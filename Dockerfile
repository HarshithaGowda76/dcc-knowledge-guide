FROM node:16
ENV TZ="Asia/Kolkata"
RUN wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
RUN apt-get install apt-transport-https
RUN echo "deb https://artifacts.elastic.co/packages/8.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-8.x.list
RUN apt-get update && apt-get install filebeat
RUN update-rc.d filebeat defaults 95 10
COPY filebeat.yml /etc/filebeat/filebeat.yml
RUN chmod go-w /etc/filebeat/filebeat.yml
WORKDIR /usr/src/app
RUN npm install pm2 -g
COPY package.json .
RUN npm install
COPY ./frontend /usr/src/app/frontend/
WORKDIR /usr/src/app/frontend/
RUN npm install --force
RUN npm run build
WORKDIR /usr/src/app
COPY . .
EXPOSE 8001
#CMD ["pm2-runtime", "server.js","filebeat -c /etc/filebeat/filebeat.yml"]
CMD filebeat -c /etc/filebeat/filebeat.yml & pm2-runtime server.js