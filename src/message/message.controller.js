const express = require("express");
const router = express.Router();
const messageService = require("./message.service");
const authorize = require("../../_middleware/authorize");

//Send message to Agent to client and client to agent for webchat
router.post("/addMessage", messageService.addMessage);

//Get current chat message list with current session ID
router.post("/getCurrentChatMessage", messageService.currentchathistory);

//Send message to Agent to client and client to agent for whatsapp
router.post("/addMessageNew", messageService.addMessageNew);

router.post("/addMessageFirst", messageService.addMessageFirst);

//to end the chat
router.post("/endchat", messageService.endChat);

//Load more message
router.post("/getMessage", messageService.getMessage);

// List of sessions for particular customer
router.post("/getSessions", messageService.getSessions);

// Summary based on session ID 
router.post("/getSummery", messageService.getSummery);

// Agent list in redis for testing
router.post("/agentInfo", messageService.agentInfo);

// to update last message for the particular session
router.post("/updateLastMessage", messageService.updateLastMessage);

// based on chat session ID , message history
router.post("/chatHistories", messageService.chatHistory);

//To check already session exists or not for whatsapp
router.post(
  "/checkSessionAvailable/:mobile_number",
  messageService.checkSessionAlreadyCreated
);
//List message based on pagination
router.post("/listmessagepagination",messageService.listMessagePagination);
//chat end by customer
router.post("/chatEndByCustomer",messageService.endChatByCustomer)
//List message based on pagination internal
router.post("/listInternalMessage",messageService.listInternalMessage);
module.exports = router;
