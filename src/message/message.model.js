const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const schema = new Schema(
  {
    message: {
      text: { type: String, required: true },
    },
    captions:{
      type: String,
      default:""
    },
    message_id:{
      type: String,
    },
    context_id:{
      type: String,
    },
    users: Array,
    sender: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    receiver: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    senderName: { type: String },
    msg_sent_type: {
      type: String,
      default: "TEXT",
    },
    receiverName: { type: String },
    session_id: {
      type: String,
    },
    status: {
      type: String,
      default: "Sent",
    },
    file_name: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

schema.set("toJSON", {
  virtuals: true,

  versionKey: false,

  transform: function (doc, ret) {
    // remove these props when object is serialized

    delete ret._id;
  },
});

module.exports = mongoose.model("Message", schema);
