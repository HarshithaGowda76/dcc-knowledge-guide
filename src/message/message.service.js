const { generateJwtToken } = require("../../_helper/helper");
const errorHandler = require("../../_helper/error.handler");
const { default: mongoose } = require("mongoose");
const db = require("../../_helper/db");
const io = require("socket.io-client");
let socket = io();
const { find } = require("./message.model");
const redis = require("redis");
const moment = require("moment");
const { redisconfig } = require("../../_helper/redis.config");
const fs = require("fs");
const axios = require("axios");
const FormData = require('form-data');
const config_file = require("../../config.json");
const config = require("../../config.json");
var logger = require("../../_helper/logger");
const { json } = require("body-parser");
let redisClient;
let lastredisclient = undefined;

(async () => {
  redisClient = redisconfig();

  redisClient.on("error", (error) => {
    lastredisclient = undefined;
    console.error(`Error : ${error}`);
  });
  redisClient.on("connect", async (data) => {
    console.error(`Redis Started`);
    lastredisclient = "Connected";
  });
  await redisClient.connect();
})();


module.exports = {
  addMessage,
  getMessage,
  addMessageNew,
  endChat,
  checkSessionAlreadyCreated,
  agentInfo,
  chatHistory,
  getSessions,
  getSummery,
  updateLastMessage,
  currentchathistory,
  listMessagePagination,
  listInternalMessage,
  endChatByCustomer,
  addMessageFirst
};

async function agentInfo(req, res) {
  let userId = req.body.userId;
  let results, total_end_chat;
  let isCached = false;
  try {
    if (lastredisclient != undefined) {
      if(config_file.redisenv=="checking"){
       // console.log('agent',(JSON.stringify(agen)))
      var ss= await redisClient.hVals(JSON.stringify("agentsDetails"))
      if(ss){
      //console.log(JSON.stringify(ss))
   console.log(typeof(ss))
    var valu1=JSON.stringify(ss)
    console.log(typeof(valu1))
    var newval=JSON.parse(valu1);
    const resData = newval.map(val => JSON.parse(val))
    logger.info(userId+":AgentInfo:Agent Details found",resData)
    res.send({
      fromCache: true,
      data: resData,
    });
  //  var newdata= newval.filter((value)=>{
  //   var parsedval=JSON.parse(value)
  //     console.log(typeof value)

  //     console.log(parsedval["status"])
  //    return parsedval["status"]==="newjoin"
      
  //  })
  //  const resData = newdata.map(val => JSON.parse(val))
      }
      }
      else{
      const cacheResults = await redisClient.get("agentsDetails");
      if (cacheResults && cacheResults.length != 2) {
        isCached = true;
        results = JSON.parse(cacheResults);
      } else {
        isCached = false;
      }
      logger.info(userId+":AgentInfo:Agent Details found",results)
      res.send({
        fromCache: isCached,
        data: results,
      });
    }
    } else {
      logger.error("AgentInfo:No Details found","")
      res.send({
        fromCache: false,
        data: [],
      });
    }
  } catch (error) {
    console.log(error);
  }
}

async function updateLastMessage(req, res) {
  let id = req.body.id;
  let lastMsg = req.body.lastMsg;
  let time = req.body.time;
  let count = req.body.count;

  const update = await db.Session.findByIdAndUpdate(
    { _id: id },
    {
      $set: {
        lastmessage: lastMsg,
        lastmessagetime: time,
        lastmessageUpdatedat:new Date()
      },
    }
  );
  await db.User.findByIdAndUpdate(
    {
     _id:mongoose.Types.ObjectId(update.agent) 
    },
    {
      $set: {
        lastactive_time:new Date()
      },
    }
  );
  logger.info(id+":UpdateLastMessage:LastMessage Updated",update)
  res.send({
    success: true,
    data: update,
  });
}

// end chat
async function endChat(req, res) {
  try{
  let session_id = req.body.chat_session_id;
  let chatendby = req.body.chatendby;
  let findDetails = await db.Session.findOne({
    chat_session_id: session_id,
    status:{$in:["Accept","CustomerDropped"]},
  });
  await db.User.findByIdAndUpdate(
    {
     _id:mongoose.Types.ObjectId(req.body.agent_id) 
    },
    {
      $set: {
        lastactive_time:new Date()
      },
    }
  );
  
 // var tenantId=req.headers.tenantId;
 var variable=JSON.stringify(req.headers);
    var variable1=JSON.parse(variable)
   // console.log(variable1.tenantid);
  
    var tenantId=variable1.tenantid;
  if (findDetails) {
    if(req.body.ticketexpired==true){
   await db.Session.findOneAndUpdate({
    chat_session_id: session_id
     },{$set:{ticket_flag:true}});
     var datas = JSON.stringify({

      "ticketID": findDetails.ticketID,
      "comment": req.body.comment,
      "ticket_autoID": findDetails.ticket_autoID,
    
    });
    
    var configemail = {
    
      method: 'post',
    
      url: config_file.email_api,
    
      headers: { 
    
        'Content-Type': 'application/json'
    
      },
    
      data : datas
    
    };
    console.log("configfile",configemail)
   var responseemail= await axios(configemail)
   console.log("responseemail",responseemail)
    }
    if(findDetails.agent==req.body.agent_id||findDetails.conference_agent==req.body.agent_id){
     
    if(findDetails.conference){
      if(findDetails.is_conferenceleft){
        console.log("finddetails",findDetails)
        let userIdd = findDetails.user_id;
        if (lastredisclient != undefined) {
          var agents=JSON.stringify(req.body.agent_id);
      var result=  await redisClient.hDel(JSON.stringify(agents), JSON.stringify(findDetails._id))
      if(findDetails.channel=="email"){
        var haongoingCount=await redisClient.HINCRBY(JSON.stringify("emailactivechatcount"), JSON.stringify(req.body.agent_id), -1)
      }else{
        var haongoingCount=await redisClient.HINCRBY(JSON.stringify("activechatcount"), JSON.stringify(req.body.agent_id), -1)
      }
        console.log("result",result)
        if(result){
          var updateUser = await db.Session.findByIdAndUpdate(
            { _id: findDetails._id },
            {
              $set: {
                is_available: false,
                status: "chatEnded",
                chatendby: chatendby,
                chat_ended_at: new Date(),
                conference_left_id:req.body.agent_id
              },
            }
          );
          console.log("Updating User");
          var updateAgent = await db.User.findByIdAndUpdate(
            { _id: req.body.agent_id },
            { $inc: { chat_count: -1, chat_end_count: 1 } }
          );

          console.log("Updating User 2");
          var agentActivity=await db.agentActivity({agent_id:req.body.agent_id,user_id:findDetails.user_id,activity_name:"USER_ENDED_CHAT",value:session_id,created_at:new Date()}).save();
          var wrapUp=await db.wrapUp({agent_id:req.body.agent_id,session_id:session_id,reason:req.body.reason,comments:req.body.comment,created_at:new Date()}).save()
          if(config_file.redisenv=="checking"){
            console.log("Updating User 3");
            var ss= await redisClient.hVals(JSON.stringify("agentsDetails"))
                //console.log(JSON.stringify(ss))
            // console.log(typeof(ss))
             if(ss){
             var agentStatusnew;
              var valu1=JSON.stringify(ss)
              console.log(typeof(valu1))
              var newval=JSON.parse(valu1);
              var agentStatusnew;
              var upadeddata=newval.map(async(value)=>{
                var parseddata=JSON.parse(value);
               if(parseddata["userId"] == userIdd){
                parseddata["chat"].active_chat_count = parseddata["chat"].active_chat_count>0 ?parseddata["chat"].active_chat_count-1:0;
                      parseddata["chat"].last_chat_end_time = new Date();
                      parseddata["chat"].total_ongoing = parseddata["chat"].total_ongoing >0?parseddata["chat"].total_ongoing-1:0;
                      parseddata["chat"].total_completed = parseddata["chat"].total_completed + 1;
              agentStatusnew=parseddata
                      var valu3=  await redisClient.hSet(JSON.stringify("agentsDetails"), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
                    //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
                    )
               }
               return parseddata;
              })
            socket = io(config.socketUrl+tenantId);
            socket.emit("agent-activity-input", agentStatusnew);
      
            let incomingreq = await db.Session.find({
              chat_session_id:  req.body.chat_session_id,
            }).populate("unique_id", "username email phonenumber");
            socket.emit("send-msg", {
              to: incomingreq[0]._id,
              session_id: incomingreq[0].chat_session_id,
              from: incomingreq[0].agent,
              senderName: updateAgent.username,
              chatType: "outbound",
              msg:"Agent "+updateAgent.username+" left the conversation",
              msgType: "web",
              userType: "external",
              msg_sent_type: "NOTIFICATIONS",
              chatdetails: incomingreq[0],
              file_name: "",
            });
            await axios.post(
              config_file.customurl+`/v1/message/addMessage`,
              {
                from: incomingreq[0].agent,
                to: incomingreq[0]._id,
                message:"Agent "+updateAgent.username+" left the conversation",
                senderName:updateUser.username,
                receiverName:incomingreq[0].username,
                msg_sent_type:"NOTIFICATIONS",
                messageFrom: "fromAgent",
                userType:"external",
                chatType:"outbound",
                file_name:"",
                chatdetails:incomingreq[0]
              }
            );
      
            // await redisClient.publish(
            //   "agent-activity",
            //   JSON.stringify(agentStatusnew)
            // );
            if(config_file.routingenv=="Redis"){
              await redisClient.publish(
                "agent-activity",
                JSON.stringify(agentStatusnew)
              );
              }else{
                if(!agentStatusnew){
                  var agentvalue="Text Message"
                }else{
                  var agentvalue=JSON.stringify(agentStatusnew)
                }
                var queuedetails=await axios.post(config_file.pushmessageapi,{ "queueName": "Test_ChatAPP_AgentActivity",
                "routingKey": "Test_ChatAPP_AgentActivity",
                "message":agentvalue,
                "priority": 1}, {
                  headers: { TenantID: tenantId },
                })
              }
           
            await redisClient.del(session_id);
             }
          }else{
            console.log("Chat End Message Sending");
                let cacheResults = await redisClient.get("agentsDetails");
          
                if (cacheResults) {
                  let cache_resp = JSON.parse(cacheResults);
                  var updatedlist= await cache_resp.filter((value) => {
                 
                    return (value["userId"] !=userIdd)
            
                  })
                  console.log("updatedlist",updatedlist)
                  await redisClient.set("agentsDetails", JSON.stringify(updatedlist));
                  var agentStatusnew = [];
                  cache_resp.forEach(async (item) => {
                    if (item["userId"] == userIdd) {
                      item["chat"].active_chat_count = item["chat"].active_chat_count>0 ?item["chat"].active_chat_count-1:0;
                      item["chat"].last_chat_end_time = new Date();
                      item["chat"].total_ongoing = item["chat"].total_ongoing >0?item["chat"].total_ongoing-1:0;
                      item["chat"].total_completed = item["chat"].total_completed + 1;
                      logger.info(req.body.agent_id + ":End Chat Tested Queued Count" + item["chat"].queued_count)
                          logger.info(req.body.agent_id + ":End Chat Tested active chat count" + item["chat"].active_chat_count)
                          logger.info(req.body.agent_id + ":End Chat Tested Total Ongoing" + item["chat"].total_ongoing)
                      agentStatusnew = item;
                    }
                  });
                  await redisClient.set("agentsDetails", JSON.stringify(cache_resp));
                
                  socket = io(config.socketUrl+tenantId);
                  socket.emit("agent-activity-input", agentStatusnew);
    
                  let incomingreq = await db.Session.find({
                    chat_session_id:  req.body.chat_session_id,
                  }).populate("unique_id", "username email phonenumber");

                  console.log("Chat End Message Sending");
                  socket.emit("send-msg", {
                    to: incomingreq[0]._id,
                    session_id: incomingreq[0].chat_session_id,
                    from: incomingreq[0].agent,
                    senderName: updateAgent.username,
                    chatType: "outbound",
                    msg:"Agent "+updateAgent.username+" left the conversation",
                    msgType: "web",
                    userType: "external",
                    msg_sent_type: "NOTIFICATIONS",
                    chatdetails: incomingreq[0],
                    file_name: "",
                  });
                  await axios.post(
                    config_file.customurl+`/v1/message/addMessage`,
                    {
                      from: incomingreq[0].agent,
                      to: incomingreq[0]._id,
                      message:"Agent "+updateAgent.username+" left the conversation",
                      senderName:updateUser.username,
                      receiverName:incomingreq[0].username,
                      msg_sent_type:"NOTIFICATIONS",
                      messageFrom: "fromAgent",
                      userType:"external",
                      chatType:"outbound",
                      file_name:"",
                      chatdetails:incomingreq[0]
                    }
                  );
          
                  // await redisClient.publish(
                  //   "agent-activity",
                  //   JSON.stringify(agentStatusnew)
                  // );
                  if(config_file.routingenv=="Redis"){
                    await redisClient.publish(
                      "agent-activity",
                      JSON.stringify(agentStatusnew)
                    );
                    }else{
                      if(agentStatusnew.length<=0){
                        var agentvalue="Text Message"
                      }else{
                        var agentvalue=JSON.stringify(agentStatusnew)
                      }
                      var queuedetails=await axios.post(config_file.pushmessageapi,{ "queueName": "Test_ChatAPP_AgentActivity",
                      "routingKey": "Test_ChatAPP_AgentActivity",
                      "message":agentvalue,
                      "priority": 1}, {
                        headers: { TenantID: tenantId },
                      })
                    }
                 
                 // await redisClient.del(session_id);
                  }
              }
              if (updateUser) {
                logger.info(session_id+":EndChat:chat session ended ",session_id)
                res.json({
                  success: true,
                  message: "chat session ended successfully",
                  data: {
                    chat_session_id: session_id,
                  },
                });
              } else {
                logger.error("EndChat: Error Occured",session_id)
                res.json({
                  success: false,
                  message: "error",
                  data: {
                    chat_session_id: session_id,
                  },
                });
              }
        }else{
          logger.error("EndChat: Error Occured",session_id)
          res.json({
            success: false,
            message: "error",
            data: {
              chat_session_id: session_id,
            },
          });
        }
         // console.log(respons)
      //     var ss= await redisClient.hVals(JSON.stringify(agents))
      //     //console.log(JSON.stringify(ss))
      //  console.log(typeof(ss))
      //   var valu1=JSON.stringify(ss)
      //   console.log(typeof(valu1))
      //   var newval=JSON.parse(valu1);
      //   var upadeddata=newval.map((value)=>{
      //     var parseddata=JSON.parse(value);
      //    if( parseddata["chat_session_id"] == session_id){
      //     parseddata["status"] = "chat Ended";
      //     parseddata["chat_type"] = "external";
      //           var valu3=  redisClient.hSet(JSON.stringify(agents), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
      //         //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
      //         )
      //         console.log("value",valu3)
      //    }
      //    return parseddata;
      //   })
         // var value = await redisClient.get(JSON.stringify(req.body.agent_id));

        //   let cachedrequestlist = JSON.parse(value);
    
        //   let filteredArray = cachedrequestlist.filter((item) => {
        //     return item.chat_session_id !== session_id;
        //   });
        //  // console.log("after Updating " + filteredArray);
        //   redisClient.set(
        //     JSON.stringify(req.body.agent_id),
        //     JSON.stringify(filteredArray)
        //   );
   
        }
    
       
      }else{
        let userIdd = findDetails.user_id;
        if (lastredisclient != undefined) {
        //   var value = await redisClient.get(JSON.stringify(req.body.agent_id));
        //   let cachedrequestlist = JSON.parse(value);
    
        //   let filteredArray = cachedrequestlist.filter((item) => {
        //     return item.chat_session_id !== session_id;
        //   });
        //  // console.log("after Updating " + filteredArray);
        //   redisClient.set(
        //     JSON.stringify(req.body.agent_id),
        //     JSON.stringify(filteredArray)
        //   );
        var haongoingCount=await redisClient.HINCRBY(JSON.stringify("activechatcount"), JSON.stringify(req.body.agent_id), -1)
        var agents=JSON.stringify(req.body.agent_id);
    //     var ss= await redisClient.hVals(JSON.stringify(agents))
    //     //console.log(JSON.stringify(ss))
    //  console.log(typeof(ss))
    //   var valu1=JSON.stringify(ss)
    //   console.log(typeof(valu1))
    //   var newval=JSON.parse(valu1);
    //   var upadeddata=newval.map((value)=>{
    //     var parseddata=JSON.parse(value);
    //    if( parseddata["chat_session_id"] == session_id){
    //     parseddata["status"] = "chat Ended";
    //     parseddata["chat_type"] = "external";
    //           var valu3=  redisClient.hSet(JSON.stringify(agents), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
    //         //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
    //         )
    //         console.log("value",valu3)
    //    }
    //    return parseddata;
    //   })
   var result= await redisClient.hDel(JSON.stringify(agents),JSON.stringify(findDetails._id)
    )
    console.log("result",result)
    if(result){
      var updateUser = await db.Session.findByIdAndUpdate(
        { _id: findDetails._id },
        {
          $set: {
            is_conferenceleft: true,
            conference_left_id:req.body.agent_id
          },
        }
      );
  
      var updateAgent = await db.User.findByIdAndUpdate(
        { _id: req.body.agent_id },
        { $inc: { chat_count: -1, chat_end_count: 1 } }
      );
      var agentActivity=await db.agentActivity({agent_id:req.body.agent_id,user_id:findDetails.user_id,activity_name:"USER_ENDED_CHAT",value:session_id,created_at:new Date()}).save();
      var wrapUp=await db.wrapUp({agent_id:req.body.agent_id,session_id:session_id,reason:req.body.reason,comments:req.body.comment,created_at:new Date()}).save()
      if(config_file.redisenv=="checking"){
        var ss= await redisClient.hVals(JSON.stringify("agentsDetails"))
        if(ss){
        var agentStatusnew;
        var valu1=JSON.stringify(ss)
        console.log(typeof(valu1))
        var newval=JSON.parse(valu1);
        var agentStatusnew;
        var upadeddata=newval.map(async(value)=>{
          var parseddata=JSON.parse(value);
         if(parseddata["userId"] == userIdd){
          parseddata["chat"].active_chat_count = parseddata["chat"].active_chat_count - 1;
          parseddata["chat"].last_chat_end_time = new Date();
          parseddata["chat"].total_ongoing = parseddata["chat"].total_ongoing - 1;
          parseddata["chat"].total_completed = parseddata["chat"].total_completed + 1;
        agentStatusnew=parseddata
                var valu3=  await redisClient.hSet(JSON.stringify("agentsDetails"), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
              //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
              )
         }
         return parseddata;
        })
        socket.current = io(config.socketUrl+tenantId);
              socket.current.emit("agent-activity-input", agentStatusnew);
      
              let incomingreq = await db.Session.find({
                chat_session_id:  req.body.chat_session_id,
              }).populate("unique_id", "username email phonenumber");
              socket.current.emit("send-msg", {
                to: incomingreq[0]._id,
                session_id: incomingreq[0].chat_session_id,
                from: incomingreq[0].agent,
                senderName: updateAgent.username,
                chatType: "outbound",
                msg:"Agent "+updateAgent.username+" left the conversation",
                msgType: "web",
                userType: "external",
                msg_sent_type: "NOTIFICATIONS",
                chatdetails: incomingreq[0],
                file_name: "",
              });
              await axios.post(
                config_file.customurl+`/v1/message/addMessage`,
                {
                  from: incomingreq[0].agent,
                  to: incomingreq[0]._id,
                  message:"Agent "+updateAgent.username+" left the conversation",
                  senderName:updateUser.username,
                  receiverName:incomingreq[0].username,
                  msg_sent_type:"NOTIFICATIONS",
                  messageFrom: "fromAgent",
                  userType:"external",
                  chatType:"outbound",
                  file_name:"",
                  chatdetails:incomingreq[0]
                }
              );
  
              // await redisClient.publish(
              //   "agent-activity",
              //   JSON.stringify(agentStatusnew)
              // );
              if(config_file.routingenv=="Redis"){
                await redisClient.publish(
                  "agent-activity",
                  JSON.stringify(agentStatusnew)
                );
                }else{
                  if(!agentStatusnew){
                    var agentvalue="Text Message"
                  }else{
                    var agentvalue=JSON.stringify(agentStatusnew)
                  }
                  var queuedetails=await axios.post(config_file.pushmessageapi,{ "queueName": "Test_ChatAPP_AgentActivity",
                  "routingKey": "Test_ChatAPP_AgentActivity",
                  "message":agentvalue,
                  "priority": 1}, {
                    headers: { TenantID: tenantId },
                  })
                }
              }
      }
      else{
        let cacheResults = await redisClient.get("agentsDetails");
      
            if (cacheResults) {
              let cache_resp = JSON.parse(cacheResults);
              var agentStatusnew = [];
              cache_resp.forEach(async (item) => {
                if (item["userId"] == userIdd) {
                  item["chat"].active_chat_count = item["chat"].active_chat_count - 1;
                  item["chat"].last_chat_end_time = new Date();
                  item["chat"].total_ongoing = item["chat"].total_ongoing - 1;
                  item["chat"].total_completed = item["chat"].total_completed + 1;
                  agentStatusnew = item;
                }
              });
              await redisClient.set("agentsDetails", JSON.stringify(cache_resp));
              socket.current = io(config.socketUrl+tenantId);
              socket.current.emit("agent-activity-input", agentStatusnew);
      
              let incomingreq = await db.Session.find({
                chat_session_id:  req.body.chat_session_id,
              }).populate("unique_id", "username email phonenumber");

              console.log("end message sending");
              socket.current.emit("send-msg", {
                to: incomingreq[0]._id,
                session_id: incomingreq[0].chat_session_id,
                from: incomingreq[0].agent,
                senderName: updateAgent.username,
                chatType: "outbound",
                msg:"Agent "+updateAgent.username+" left the conversation",
                msgType: "web",
                userType: "external",
                msg_sent_type: "NOTIFICATIONS",
                chatdetails: incomingreq[0],
                file_name: "",
              });
              await axios.post(
                config_file.customurl+`/v1/message/addMessage`,
                {
                  from: incomingreq[0].agent,
                  to: incomingreq[0]._id,
                  message:"Agent "+updateAgent.username+" left the conversation",
                  senderName:updateUser.username,
                  receiverName:incomingreq[0].username,
                  msg_sent_type:"NOTIFICATIONS",
                  messageFrom: "fromAgent",
                  userType:"external",
                  chatType:"outbound",
                  file_name:"",
                  chatdetails:incomingreq[0]
                }
              );
  
              // await redisClient.publish(
              //   "agent-activity",
              //   JSON.stringify(agentStatusnew)
              // );
              if(config_file.routingenv=="Redis"){
                await redisClient.publish(
                  "agent-activity",
                  JSON.stringify(agentStatusnew)
                );
                }else{
                  if(agentStatusnew.length<=0){
                    var agentvalue="Text Message"
                  }else{
                    var agentvalue=JSON.stringify(agentStatusnew)
                  }
                  var queuedetails=await axios.post(config_file.pushmessageapi,{ "queueName": "Test_ChatAPP_AgentActivity",
                  "routingKey": "Test_ChatAPP_AgentActivity",
                  "message":agentvalue,
                  "priority": 1}, {
                    headers: { TenantID: tenantId },
                  })
                }
              
             // await redisClient.del(session_id);
            }
      }
      if (updateUser) {
        logger.info(session_id+":EndChat:chat session ended ",session_id)
        res.json({
          success: true,
          message: "chat session ended successfully",
          data: {
            chat_session_id: session_id,
          },
        });
      } else {
        logger.error("EndChat:Error Occured",session_id)
        res.json({
          success: false,
          message: "error",
          data: {
            chat_session_id: session_id,
          },
        });
      }
    }else{
      logger.error("EndChat:Error Occured",session_id)
        res.json({
          success: false,
          message: "error",
          data: {
            chat_session_id: session_id,
          },
        });
    }
   // console.log(respons)
    
          
        }
    
        
        
      }

    }
    else{
      let userIdd = findDetails.user_id;
      if (lastredisclient != undefined) {
      //   var value = await redisClient.get(JSON.stringify(findDetails.agent));
      //   let cachedrequestlist = JSON.parse(value);
  
      //   let filteredArray = cachedrequestlist.filter((item) => {
      //     return item.chat_session_id !== session_id;
      //   });
      //  // console.log("after Updating " + filteredArray);
      //   redisClient.set(
      //     JSON.stringify(findDetails.agent),
      //     JSON.stringify(filteredArray)
      //   );
     
  //     var ss= await redisClient.hVals(JSON.stringify(agents))
  //     //console.log(JSON.stringify(ss))
  //  console.log(typeof(ss))
  //   var valu1=JSON.stringify(ss)
  //   console.log(typeof(valu1))
  //   var newval=JSON.parse(valu1);
  //   var upadeddata=newval.map((value)=>{
  //     var parseddata=JSON.parse(value);
  //    if( parseddata["chat_session_id"] == session_id){
  //     parseddata["status"] = "chat Ended";
  //     parseddata["chat_type"] = "external";
  //           parseddata["chat_started_at"] = new Date();
  //            parseddata["is_queued"] = false;
  //           var valu3=  redisClient.hSet(JSON.stringify(agents), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
  //         //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
  //         )
  //         console.log("value",valu3)
  //    }
  //    return parseddata;
  //   })
  var haongoingCount=await redisClient.HINCRBY(JSON.stringify("activechatcount"), JSON.stringify(req.body.agent_id), -1)
  var agents=JSON.stringify(findDetails.agent);
  var result=await redisClient.hDel(JSON.stringify(agents), JSON.stringify(findDetails._id))
   console.log("result test",result)
   if(result){
    var updateUser = await db.Session.findByIdAndUpdate(
      { _id: findDetails._id },
      {
        $set: {
          is_available: false,
          status: "chatEnded",
          chatendby: chatendby,
          chat_ended_at: new Date(),
        },
      }
    );

    var updateAgent = await db.User.findByIdAndUpdate(
      { _id: findDetails.agent },
      { $inc: { chat_count: -1, chat_end_count: 1 } }
    );

    // let findDetailsUSer = await db.User.findOne({
    //   _id: findDetails.agent,
    // });
    var agentActivity=await db.agentActivity({agent_id:findDetails.agent,user_id:findDetails.user_id,activity_name:"USER_ENDED_CHAT",value:session_id,created_at:new Date()}).save();
    var wrapup=await db.wrapUp({agent_id:findDetails.agent,session_id:session_id,reason:req.body.reason,comments:req.body.comment,created_at:new Date()}).save()
   //console.log("wrapUp",wrapUp.id)
  var disposition={
    "disposition_id": wrapup._id,
    "disposition_name": req.body.comment,
    "disposition_type": req.body.reason,
    "active": "true",
    "Channel": "chat"
}

const configvalue = {
  headers: { TenantID: tenantId },
  };
   axios.post(
    config.interactionApi+"dispositions/createDispositions",
    disposition,
    configvalue
    );
if(config_file.redisenv==="checking"){
  var ss= await redisClient.hVals(JSON.stringify("agentsDetails"))
  var agentStatusnew;
    var valu1=JSON.stringify(ss)
    console.log(typeof(valu1))
    var newval=JSON.parse(valu1);
    var agentStatusnew;
    var upadeddata=newval.map(async(value)=>{
      var parseddata=JSON.parse(value);
     if(parseddata["userId"] == userIdd){
      parseddata["chat"].active_chat_count = parseddata["chat"].active_chat_count - 1;
      parseddata["chat"].last_chat_end_time = new Date();
      parseddata["chat"].total_ongoing = parseddata["chat"].total_ongoing - 1;
      parseddata["chat"].total_completed = parseddata["chat"].total_completed + 1;
    agentStatusnew=parseddata
            var valu3=  await redisClient.hSet(JSON.stringify("agentsDetails"), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
          //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
          )
     }
     return parseddata;
    })

  socket.current = io(config.socketUrl+tenantId);
  socket.current.emit("agent-activity-input", agentStatusnew);
let incomingreq = await db.Session.find({
      chat_session_id:  req.body.chat_session_id,
    }).populate("unique_id", "username email phonenumber");
    socket.current.emit("send-msg", {
      to: incomingreq[0]._id,
      session_id: incomingreq[0].chat_session_id,
      from: incomingreq[0].agent,
      senderName: updateAgent.username,
      chatType: "outbound",
      msg:"Agent "+updateAgent.username+" left the conversation",
      msgType: "web",
      userType: "external",
      msg_sent_type: "NOTIFICATIONS",
      chatdetails: incomingreq[0],
      file_name: "",
    });
    await axios.post(
      config_file.customurl+`/v1/message/addMessage`,
      {
        from: incomingreq[0].agent,
        to: incomingreq[0]._id,
        message:"Agent "+updateAgent.username+" left the conversation",
        senderName:updateUser.username,
        receiverName:incomingreq[0].username,
        msg_sent_type:"NOTIFICATIONS",
        messageFrom: "fromAgent",
        userType:"external",
        chatType:"outbound",
        file_name:"",
        chatdetails:incomingreq[0]
      }
    );
  // await redisClient.publish(
  //   "agent-activity",
  //   JSON.stringify(agentStatusnew)
  // );
  if(config_file.routingenv=="Redis"){
    await redisClient.publish(
      "agent-activity",
      JSON.stringify(agentStatusnew)
    );
    }else{
      if(!agentStatusnew){
        var agentvalue="Text Message"
      }else{
        var agentvalue=JSON.stringify(agentStatusnew)
      }
      var queuedetails=await axios.post(config_file.pushmessageapi,{ "queueName": "Test_ChatAPP_AgentActivity",
      "routingKey": "Test_ChatAPP_AgentActivity",
      "message":agentvalue,
      "priority": 1}, {
        headers: { TenantID: tenantId },
      })
    }
  
  await redisClient.del(session_id);
}
else{
      let cacheResults = await redisClient.get("agentsDetails");

      if (cacheResults) {
        let cache_resp = JSON.parse(cacheResults);
        var agentStatusnew = [];
        cache_resp.forEach(async (item) => {
          if (item["userId"] == userIdd) {
            item["chat"].active_chat_count = item["chat"].active_chat_count - 1;
            item["chat"].last_chat_end_time = new Date();
            item["chat"].total_ongoing = item["chat"].total_ongoing - 1;
            item["chat"].total_completed = item["chat"].total_completed + 1;
            agentStatusnew = item;
          }
        });
        await redisClient.set("agentsDetails", JSON.stringify(cache_resp));
        socket.current = io(config.socketUrl+tenantId);
        socket.current.emit("agent-activity-input", agentStatusnew);
   let incomingreq = await db.Session.find({
            chat_session_id:  req.body.chat_session_id,
          }).populate("unique_id", "username email phonenumber");
          console.log("chat end test");
          socket.current.emit("send-msg", {
            to: incomingreq[0]._id,
            session_id: incomingreq[0].chat_session_id,
            from: incomingreq[0].agent,
            senderName: updateAgent.username,
            chatType: "outbound",
            msg:"Agent "+updateAgent.username+" left the conversation",
            msgType: "web",
            userType: "external",
            msg_sent_type: "NOTIFICATIONS",
            chatdetails: incomingreq[0],
            file_name: "",
          });
          await axios.post(
            config_file.customurl+`/v1/message/addMessage`,
            {
              from: incomingreq[0].agent,
              to: incomingreq[0]._id,
              message:"Agent "+updateAgent.username+" left the conversation",
              senderName:updateUser.username,
              receiverName:incomingreq[0].username,
              msg_sent_type:"NOTIFICATIONS",
              messageFrom: "fromAgent",
              userType:"external",
              chatType:"outbound",
              file_name:"",
              chatdetails:incomingreq[0]
            }
          );
        // await redisClient.publish(
        //   "agent-activity",
        //   JSON.stringify(agentStatusnew)
        // );
        if(config_file.routingenv=="Redis"){
          await redisClient.publish(
            "agent-activity",
            JSON.stringify(agentStatusnew)
          );
          }else{
            if(agentStatusnew.length<=0){
              var agentvalue="Text Message"
            }else{
              var agentvalue=JSON.stringify(agentStatusnew)
            }
            var queuedetails=await axios.post(config_file.pushmessageapi,{ "queueName": "Test_ChatAPP_AgentActivity",
            "routingKey": "Test_ChatAPP_AgentActivity",
            "message":agentvalue,
            "priority": 1}, {
              headers: { TenantID: tenantId },
            })
          }
        
       // await redisClient.del(session_id);
      }
    }
    if (updateUser) {
      var logdates = moment(findDetails.chat_started_at, "YYYY-M-DD HH:mm:ss");
      var dates = moment(findDetails.chat_ended_at, "YYYY-M-DD HH:mm:ss");
      var diff = dates.diff(logdates, "seconds");
      var disposition_status=findDetails.is_customer_disconnected==true?"Customer Disconnected":"Agent Disconnected"
    var updatedate=  {
        "Intraction_id":updateUser.chat_session_id,
        "start_time":updateUser.chat_started_at,
        "Ring_time":"10",
        "start_time":findDetails.chat_started_at,
        "end_time":findDetails.chat_ended_at,
        "duration":diff,
        "disposition":req.body.reason,
        "ReasoncodeInteraction":req.body.comment,
        "Disconnected_status_id":disposition_status,
        "after_call_work_notes":req.body.comment,
        "interaction_disconnected":true,
        

    }
    console.log("update",updatedate)
    const config1 = {
      headers: { TenantID: tenantId },
      };
       axios.post(
        config.interactionApi+"liveReport/updateLiveReport",
        updatedate,
        config1
        );
      logger.info(session_id+":EndChat:chat session ended ",session_id)
      res.json({
        success: true,
        message: "chat session ended successfully",
        data: {
          chat_session_id: session_id,
        },
      });
    } else {
      logger.error("EndChat:Error Occuerd",session_id)
      res.json({
        success: false,
        message: "error",
        data: {
          chat_session_id: session_id,
        },
      });
    }
  }else{
    logger.error("EndChat:Error Occuerd",session_id)
      res.json({
        success: false,
        message: "error",
        data: {
          chat_session_id: session_id,
        },
      });
  }
  //console.log(respons)
 
      }
  
    
    }
  }else{
    res.json({
      success: false,
      message: "Invalid Agent id ",
      data: {
        chat_session_id: session_id,
        agent_id:req.body.agent_id
      },
    });
  }
  } else {
    logger.error("EndChat:Error Occuerd"+session_id)
    res.json({
      success: false,
      message: "No active session available for session ",
      data: {
        chat_session_id: session_id,
      },
    });
  }
}
catch(e){
  res.json({
    success: false,
    message: "Invalid details shared",
  });
}
}
//end chat by customer
async function endChatByCustomer(req,res){
  let session_id = req.body.chat_session_id;
  let findDetails = await db.Session.findOne({
    chat_session_id: session_id,
  });

 // var tenantId=req.headers.tenantId;
 var variable=JSON.stringify(req.headers)
 var variable1=JSON.parse(variable)
 console.log(variable1.tenantid);
 
 var tenantId=variable1.tenantid;
  if (findDetails) {
    if(findDetails.status==='newjoin'){
      var dataValue={
        "crmId": "10000001",
        "phoneNumber": findDetails.phonenumber,
        "serviceRequestType": "",
        "customerLanguage": findDetails.language,
        "customerLanguageCode": "",
        "LastHandledAgentID": "",
        "routingRuleId": "",
        "skillSet": findDetails.skillset,
        "chat_session_id": findDetails.chat_session_id
      }
      if(config_file.env==='Production'){
      await axios.post(config_file.routingbaseurl+"/routingengine/engine/acceptChat",dataValue, {
        headers: { TENANTID: tenantId },
      })}
      var updateUser = await db.Session.findByIdAndUpdate(
        { _id: findDetails._id },
        {
          $set: {
            customer_disconnected:true,
            customer_drop:true,
            status:"CustomerDropped"
          }}
      );
    }else{
    var updateUser = await db.Session.findByIdAndUpdate(
      { _id: findDetails._id },
      {
        $set: {
          is_customer_disconnected:true,
    is_customer_disconnected_time: new Date(),
        }}
    );
      }

    // let findDetailsUSer = await db.User.findOne({
    //   _id: findDetails.agent,
    // });
    if (lastredisclient != undefined) {
    //   var value = await redisClient.get(JSON.stringify(findDetails.agent));
    //   let cachedrequestlist = JSON.parse(value);

    //   let filteredArray = cachedrequestlist.filter((item) => {
        
    //     if(item.chat_session_id === session_id){
    //     item.is_customer_disconnected=true;
    //         item.is_customer_disconnected_time=new Date();
    //     }
    //   });
    //  // console.log("after Updating " + filteredArray);
    //   redisClient.set(
    //     JSON.stringify(findDetails.agent),
    //     JSON.stringify(filteredArray)
    //   );
    var agents=JSON.stringify(findDetails.agent);
    var ss= await redisClient.hVals(JSON.stringify(agents))
    //console.log(JSON.stringify(ss))
 console.log(typeof(ss))
  var valu1=JSON.stringify(ss)
  console.log(typeof(valu1))
  var newval=JSON.parse(valu1);
  var upadeddata=newval.map((value)=>{
    var parseddata=JSON.parse(value);
   if( parseddata["chat_session_id"] == session_id){
    parseddata.is_customer_disconnected=true;
             parseddata.is_customer_disconnected_time=new Date();
          var valu3=  redisClient.hSet(JSON.stringify(agents), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
        //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
        )
        console.log("value",valu3)
   }
   return parseddata;
  })
     
    }

    if (updateUser) {
      logger.info(session_id+":EndChatCustomer:Chat session Ended",session_id)
      res.json({
        success: true,
        message: "chat session ended successfully",
        data: {
          chat_session_id: session_id,
        },
      });
    } else {
      logger.error("EndChatCustomer:Error Occured",session_id)
      res.json({
        success: false,
        message: "error",
        data: {
          chat_session_id: session_id,
        },
      });
    }
  
 
  }
  else {
    logger.error("EndChatCustomer:Error Occured",session_id)
    res.json({
      success: false,
      message: "error",
      data: {
        chat_session_id: session_id,
      },
    });
  }
}
// get session
async function getSessions(req, res) {
  try {
    var email = req.body.email;
    var phonenumber = req.body.phonenumber;
    if (!email) {
      email = "empty";
    }
    if (!phonenumber) {
      phonenumber = "empty";
    }

    let findSession;
    console.log(req.body.date);
    if (req.body.date) {
      var todays_date = new Date(req.body.date).toISOString();
      todays_date = todays_date.split("T");
     // var start_date = todays_date[0].toString();
     var start_date = new Date(todays_date[0] + "T23:59:59.999Z");

  
      findSession = await db.Session.find({
        $or: [
          {
            email: email,
            chat_started_at: {
              $lte: new Date(start_date),
            },
          },
          {
            phonenumber: phonenumber,
            chat_started_at: {
              $lte: new Date(start_date),
            },
          },
        ],
      }).populate("agent", "username");
    } else {
      findSession = await db.Session.find({
        $or: [
          {
            email: email,
          },
          {
            phonenumber: phonenumber,
          },
        ],
      }).populate("agent", "username");
    }

    if (findSession) {
      logger.info("GetSession:Session Feteched",findSession)
      res.json({ success: true, data: findSession });
    } else {
      logger.error("GetSession:No data","")
      res.json({ success: false, data: "" });
    }
  } catch (err) {
    res.json({ success: false, data: err, message: "Try after sometimes" });
  }
}

// get summery
async function getSummery(req, res) {
  try {
    let session_id = req.body.sessionId;
    let findSession = await db.Session.find({
      chat_session_id: session_id,
    }).populate("agent", "username");

    if (findSession) {
      let arrr = [];
      let conf_username;
      if (findSession[0].conference) {
        let findSession = await db.Session.findOne({
          chat_session_id: session_id,
        }).populate("conference_agent", "username");
        conf_username = findSession.conference_agent.username;
      }

      let result = [];

      console.log(conf_username, "conf_username");

      result.push(findSession[0], {
        confUser: conf_username ? ", " + conf_username : "",
      });
      logger.info(session_id+":GetSummary:Session Feteched",result)
      res.json({ success: true, data: result });
    } else {
      logger.error("GetSummary:No data found","")
      res.json({ success: false, data: "" });
    }
  } catch (err) {
    res.json({ success: false, data: "", message: "Try after sometimes" });
  }
}

// API to add message
async function addMessage(req, res, next) {
  try {
    const {
      from,
      to,
      message,
      senderName,
      receiverName,
      userType,
      session,
      msg_sent_type,
      file_name,
    } = req.body;

    let msgFrom = req.body.messageFrom;
    let getId, from_id, to_id, static;

    if (msgFrom == "fromClient") {
      getId = await db.Session.findOne({
        _id: from,
      });

      if (getId) {
        if(userType=="internal"){
          from_id = getId.receiver_id;
          static = from_id;
        }else{
        from_id = getId.unique_id;
        static = from_id;
        }
        if (getId.transferred) {
          to_id = String(getId.agent);
        } else {
          to_id = to;
        }
      }
    } else {
      getId = await db.Session.findOne({
        _id: to,
      });

      if (getId) {
        from_id = from;
        to_id = getId.unique_id;
        static = to_id;
      }
    }

    let findDetails = await db.Session.findOne({
      _id: to,
    });

    if (!findDetails) {
      findDetails = await db.Session.findOne({
        _id: from,
      });
    }
    var day=new Date()
    

var hours = day.getHours();
var minutes = day.getMinutes();
var seconds = day.getSeconds();
  if(hours>=12){
var dateTime =  hours + ":" + minutes + ":" + seconds+ " PM";
  }else{
    var dateTime =  hours + ":" + minutes + ":" + seconds+ " AM";
  }
console.log(dateTime)
    const update = await db.Session.findByIdAndUpdate(
      { _id: findDetails.id },
      {
        $set: {
          lastmessage: message,
          lastmessagetime: dateTime,
          lastmessageUpdatedat:new Date()
        },
      }
    );

    const data = await db.Message.create({
      message: { text: message },
      users: [from_id, to_id],
      sender: from_id,
      receiver: to_id,
      senderName,
      receiverName: receiverName,
      file_name,
      msg_sent_type: msg_sent_type,
      session_id: findDetails ? findDetails.chat_session_id : "",
    });

    var messageobj = {
      fromSelf: to_id.toString() === static.toString(),
      message: message,
      time: moment().format("lll"),
      sender: from_id,
      receiver: to_id,
      senderName: senderName,
      receiverName: receiverName,
      msg_sent_type:
        msg_sent_type != "" || msg_sent_type ? msg_sent_type : "normal_message",
      session_id: findDetails.chat_session_id,
      file_name: file_name,
    };

    if (lastredisclient != undefined) {
      var value = await redisClient.get(findDetails.chat_session_id);
      let cachedmessage = JSON.parse(value);

      if (cachedmessage) {
        cachedmessage = [...cachedmessage, messageobj];
      } else {
        cachedmessage = [messageobj];
      }
      redisClient.set(
        findDetails.chat_session_id,
        JSON.stringify(cachedmessage)
      );
    }

    
    if (data) {
      logger.info(data._id+":AddMessage:Message added",data)
      return res.json({ msg: "Message added successfully." });}
    else{ 
      logger.error("AddMessage:Failed to add message to the database","")
      return res.json({ msg: "Failed to add message to the database" });}
  } catch (ex) {
    next(ex);
  }
}

// API to chat history
async function chatHistory(req, res) {
  let sessionId = req.body.sessionId;

  let getMessages = await db.Message.find({
    session_id: sessionId,
  });

  if (getMessages && getMessages.length) {
    const messages = await db.Message.find({
      users: {
        $all: [String(getMessages[0].sender), getMessages[0].receiver],
      },
    }).sort({ updatedAt: 1 });

    const projectedMessages = getMessages.map((msg) => {
      return {
        fromSelf: msg.sender.toString() === getMessages[0].sender.toString(),
        message: msg.message.text,
        time: msg.createdAt,
        sender: msg.sender,
        receiver: msg.receiver,
        senderName: msg.senderName,
        receiverName: msg.receiverName,
        msg_sent_type: msg.msg_sent_type,
        file_name: msg.file_name,
      };
    });
    logger.info(sessionId+":ChatHistory:Message history",projectedMessages)
    res.json({ status: true, data: projectedMessages });
  } else {
    logger.error("ChatHistory:No data found","")
    res.json({ status: false, message: "no data found" });
  }
}

async function checkSessionAlreadyCreated(req, res) {
  try {
    let mobNum = req.params.mobile_number;
    let findDetails = await db.Session.findOne({
      phonenumber: mobNum,

      status: "Accept",
      // { status: { $ne: "chatEnded" } }
    });

    if (findDetails) {
      logger.info(mobNum+":CheckSessionAlreadyCreated:Session Availabile",findDetails)
      res.json({
        success: true,
        message: "session available",
        data: {
          session_id: findDetails.chat_session_id,
        },
      });
    } else {
      logger.error("CheckSessionAlreadyCreated:No Session availble","")
      res.json({
        success: false,
        message: "session not available",
      });
    }
  } catch (e) {
    res.json({
      success: false,
      message: "try after sometime",
    });
  }
}

async function addMessageNew(req, res, next) {
  try {
    console.log("new session add msh");
    var variable=JSON.stringify(req.headers)
    var variable1=JSON.parse(variable)
    console.log(variable1.tenantid);
    
    var tenantId=variable1.tenantid;
    let session_id = req.body.chat_session_id;

    let findDetails = await db.Session.findOne({
      chat_session_id: session_id,
    }).populate("unique_id", "username");

    let receiver_details = await db.User.findOne({
      _id: findDetails.agent,
    });
    let from = findDetails.unique_id;
    let to = findDetails.agent;
    let msg = req.body.message;
    let senderName = findDetails.username;
    let receiverName = receiver_details.username;
    let msg_sent_type = req.body.msg_sent_type;
    let captions = req.body.msg_sent_type;

    let findRealId = await db.Session.findOne({
      unique_id: from,
      status:"Accept"
    });
   //console.log( "body url",JSON.stringify(from.id));
     if(findRealId.channel=="from_whatsapp"){
    if (msg_sent_type != "TEXT") {
      const formData = new FormData();
      formData.append('userID',from.id);
      formData.append('file',fs.createReadStream(req.body.message));
      formData.append('clientApp',"clientApp");
      formData.append('channel',"channel");
      formData.append('sessionId',JSON.stringify(session_id));
      formData.append('sentBy',"Customer");
     // console.log("formData",formData);

      const confignew = {
        headers: { TenantID: 123456 },
      };
      axios
        .post(config_file.fileserviceUrl+"/v1/fileServer/uploadMedia",formData, confignew)
        .then(async (resp) => {
       //   console.log("Image is coming from customer", resp.data);
         

          if (resp.data) {
            fs.unlinkSync(req.body.message,function(err){
              if(err) return console.log(err);
              console.log('file deleted successfully');
            })
          }
           msg=resp.data.data.signedUrl;
          socket.current = io(config.socketUrl+tenantId);
          socket.current.emit("send-msg", {
            to: to,
            from: findRealId._id,
            msg:resp.data.data.signedUrl,
            chatType: "inbound",
            msgType: "whatsApp",
            senderName: findDetails.unique_id.username,
            msg_sent_type: msg_sent_type,
            session_id: req.body.chat_session_id,
            chatdetails: findRealId,
            chat_type: "external",
            userType: "external",
            file_name: req.body.file_name ? req.body.file_name : "",
            captions:req.body.captions
          });


          socket.current.emit("last-msg-send", {
            to: to,
            from: findRealId._id,
            chatType: "inbound",
            msg:resp.data.data.signedUrl,
            senderName: findDetails.unique_id
              ? findDetails.unique_id.username
              : "Guest",
              chatdetails: findRealId
          });
          const update = await db.Session.findByIdAndUpdate(
            { _id:findRealId._id},
            {
              $set: {
                lastmessage: resp.data.data.signedUrl,
                lastmessagetime:  moment().format("HH:MM A"),
                lastmessageUpdatedat:new Date()
              },
            }
          );
          let data = await db.Message.create({
            message: { text: resp.data.data.signedUrl },
            users: [from, String(to)],
            sender: from,
            receiver: to,
            senderName: findDetails.unique_id
              ? findDetails.unique_id.username
              : "Guest",
            receiverName: receiverName,
            session_id: req.body.chat_session_id,
            msg_sent_type: msg_sent_type,
            file_name: req.body.file_name,
            message_id:req.body.msgId,
            captions:req.body.captions,
            context_id:req.body.context_id
          });
    
          var messageobj = {
            fromSelf: false,
            message: resp.data.data.signedUrl,
            time: moment().format("lll"),
            sender: from,
            receiver: to,
            senderName: findDetails.unique_id
              ? findDetails.unique_id.username
              : "Guest",
            receiverName: receiverName,
            session_id: req.body.chat_session_id,
            senderName: findDetails.unique_id.username,
            msg_sent_type: msg_sent_type,
            file_name: req.body.file_name,
            message_id:req.body.msgId,
            captions:req.body.captions,
            context_id:req.body.context_id
          };
    
       //   console.log("object", messageobj);
          if (lastredisclient != undefined) {
            var value = await redisClient.get(session_id);
            let cachedmessage = JSON.parse(value);
    
            if (cachedmessage) {
              cachedmessage = [...cachedmessage, messageobj];
            } else {
              cachedmessage = [messageobj];
            }
            redisClient.set(
              findDetails.chat_session_id,
              JSON.stringify(cachedmessage)
            );
          }
          if (data) {
            logger.info(data._id+":AddMessage:Message added",data)
            return res.json({ msg: "Message added successfully." });}
          else {
            logger.error("AddMessage:Falied add message to db","")
            return res.json({ msg: "Failed to add message to the database" });}

        })
    } else {
      socket.current = io(config.socketUrl+tenantId);
      socket.current.emit("send-msg", {
        to: to,
        from: findRealId._id,
        msg,
        chatType: "inbound",
        msgType: "whatsApp",
        senderName: findDetails.unique_id.username,
        msg_sent_type: msg_sent_type,
        session_id: req.body.chat_session_id,
        chatdetails: findRealId,
        chat_type: "external",
        userType: "external",
        file_name: req.body.file_name ? req.body.file_name : "",
        message_id:req.body.msgId,
        context_id:req.body.context_id,
        captions:req.body.captions
    
      });

      const update = await db.Session.findByIdAndUpdate(
        { _id:findRealId._id},
        {
          $set: {
            lastmessage: msg,
            lastmessagetime:  moment().format("HH:MM A"),
          },
        }
      );

      socket.current.emit("last-msg-send", {
        to: to,
        from: findRealId._id,
        chatType: "inbound",
        msg,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
          chatdetails: findRealId
      });

      let data = await db.Message.create({
        message: { text: msg },
        users: [from, String(to)],
        sender: from,
        receiver: to,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
        receiverName: receiverName,
        session_id: req.body.chat_session_id,
        msg_sent_type: msg_sent_type,
        file_name: req.body.file_name,
        message_id:req.body.msgId,
        context_id:req.body.context_id,
        captions:req.body.captions
      });

      var messageobj = {
        fromSelf: false,
        message: msg,
        time: moment().format("lll"),
        sender: from,
        receiver: to,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
        receiverName: receiverName,
        session_id: req.body.chat_session_id,
        senderName: findDetails.unique_id.username,
        msg_sent_type: msg_sent_type,
        file_name: req.body.file_name,
        message_id:req.body.msgId,
        context_id:req.body.context_id,
        captions:req.body.captions
      };

     // console.log("object", messageobj);
      if (lastredisclient != undefined) {
        var value = await redisClient.get(session_id);
        let cachedmessage = JSON.parse(value);

        if (cachedmessage) {
          cachedmessage = [...cachedmessage, messageobj];
        } else {
          cachedmessage = [messageobj];
        }
        redisClient.set(
          findDetails.chat_session_id,
          JSON.stringify(cachedmessage)
        );
      }
      if (data) {
        logger.info(data._id+":AddMessage:Message added",data)
        return res.json({ msg: "Message added successfully." });}
      else {
        logger.error("AddMessage:Failed to add message to the db","")
        return res.json({ msg: "Failed to add message to the database" });}
    }
      }
  if(findRealId.channel=="from_facebook"){
if(msg_sent_type=="TEXT"){
  socket.current = io(config.socketUrl+tenantId);
  socket.current.emit("send-msg", {
    to: to,
    from: findRealId._id,
    msg,
    chatType: "inbound",
    msgType: "facebook",
    senderName: findDetails.unique_id.username,
    msg_sent_type: msg_sent_type,
    session_id: req.body.chat_session_id,
    chatdetails: findRealId,
    chat_type: "external",
    userType: "external",
    file_name: req.body.file_name ? req.body.file_name : ""
  });

  const update = await db.Session.findByIdAndUpdate(
    { _id:findRealId._id},
    {
      $set: {
        lastmessage: msg,
        lastmessagetime:  moment().format("HH:MM A"),
        lastmessageUpdatedat:new Date()
      },
    }
  );

  socket.current.emit("last-msg-send", {
    to: to,
    from: findRealId._id,
    chatType: "inbound",
    msg,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
      chatdetails: findRealId
  });

  let data = await db.Message.create({
    message: { text: msg },
    users: [from, String(to)],
    sender: from,
    receiver: to,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
    receiverName: receiverName,
    session_id: req.body.chat_session_id,
    msg_sent_type: msg_sent_type,
    file_name: req.body.file_name
  });

  var messageobj = {
    fromSelf: false,
    message: msg,
    time: moment().format("lll"),
    sender: from,
    receiver: to,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
    receiverName: receiverName,
    session_id: req.body.chat_session_id,
    senderName: findDetails.unique_id.username,
    msg_sent_type: msg_sent_type,
    file_name: req.body.file_name
  };

 // console.log("object", messageobj);
  if (lastredisclient != undefined) {
    var value = await redisClient.get(session_id);
    let cachedmessage = JSON.parse(value);

    if (cachedmessage) {
      cachedmessage = [...cachedmessage, messageobj];
    } else {
      cachedmessage = [messageobj];
    }
    redisClient.set(
      findDetails.chat_session_id,
      JSON.stringify(cachedmessage)
    );
  }
  if (data){ 
    logger.info(data._id+":AddMessage:Message added",data)
    return res.json({ msg: "Message added successfully." });}
  else{ 
    logger.error("AddMessage:Failed to add message db","")
    return res.json({ msg: "Failed to add message to the database" });}
}else {
  socket.current = io(config.socketUrl+tenantId);
  socket.current.emit("send-msg", {
    to: to,
    from: findRealId._id,
    msg,
    chatType: "inbound",
    msgType: "facebook",
    senderName: findDetails.unique_id.username,
    msg_sent_type: msg_sent_type,
    session_id: req.body.chat_session_id,
    chatdetails: findRealId,
    chat_type: "external",
    userType: "external",
    file_name: req.body.file_name ? req.body.file_name : "Files",
    captions:req.body.captions
  });

  const update = await db.Session.findByIdAndUpdate(
    { _id:findRealId._id},
    {
      $set: {
        lastmessage: msg,
        lastmessagetime:  moment().format("HH:MM A"),
        lastmessageUpdatedat:new Date()
      },
    }
  );

  socket.current.emit("last-msg-send", {
    to: to,
    from: findRealId._id,
    chatType: "inbound",
    msg,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
      chatdetails: findRealId
  });

  let data = await db.Message.create({
    message: { text: msg },
    users: [from, String(to)],
    sender: from,
    receiver: to,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
    receiverName: receiverName,
    session_id: req.body.chat_session_id,
    msg_sent_type: msg_sent_type,
    file_name:  req.body.file_name ? req.body.file_name : "Files",
    captions:req.body.captions
  });

  var messageobj = {
    fromSelf: false,
    message: msg,
    time: moment().format("lll"),
    sender: from,
    receiver: to,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
    receiverName: receiverName,
    session_id: req.body.chat_session_id,
    senderName: findDetails.unique_id.username,
    msg_sent_type: msg_sent_type,
    file_name:  req.body.file_name ? req.body.file_name : "Files",
    captions:req.body.captions
  };

//  console.log("object", messageobj);
  if (lastredisclient != undefined) {
    var value = await redisClient.get(session_id);
    let cachedmessage = JSON.parse(value);

    if (cachedmessage) {
      cachedmessage = [...cachedmessage, messageobj];
    } else {
      cachedmessage = [messageobj];
    }
    redisClient.set(
      findDetails.chat_session_id,
      JSON.stringify(cachedmessage)
    );
  }
  if (data) {
    logger.info(data._id+":AddMessage:Message added",data)
    return res.json({ msg: "Message added successfully." });}
  else {logger.error("AddMessage:Failed to add message to db","")
     return res.json({ msg: "Failed to add message to the database" });}
}
  }

  if(findRealId.channel=="from_twitter"){
 
      socket.current = io(config.socketUrl+tenantId);
      socket.current.emit("send-msg", {
        to: to,
        from: findRealId._id,
        msg,
        chatType: "inbound",
        msgType: "twitter",
        senderName: findDetails.unique_id.username,
        msg_sent_type: msg_sent_type,
        session_id: req.body.chat_session_id,
        chatdetails: findRealId,
        chat_type: "external",
        userType: "external",
        file_name: req.body.file_name ? req.body.file_name : ""
      });

      const update = await db.Session.findByIdAndUpdate(
        { _id:findRealId._id},
        {
          $set: {
            lastmessage: msg,
            lastmessagetime:  moment().format("HH:MM A"),
            lastmessageUpdatedat:new Date()
          },
        }
      );

      socket.current.emit("last-msg-send", {
        to: to,
        from: findRealId._id,
        chatType: "inbound",
        msg,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
          chatdetails: findRealId
      });

      let data = await db.Message.create({
        message: { text: msg },
        users: [from, String(to)],
        sender: from,
        receiver: to,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
        receiverName: receiverName,
        session_id: req.body.chat_session_id,
        msg_sent_type: msg_sent_type,
        file_name: req.body.file_name
      });

      var messageobj = {
        fromSelf: false,
        message: msg,
        time: moment().format("lll"),
        sender: from,
        receiver: to,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
        receiverName: receiverName,
        session_id: req.body.chat_session_id,
        senderName: findDetails.unique_id.username,
        msg_sent_type: msg_sent_type,
        file_name: req.body.file_name
      };

     // console.log("object", messageobj);
      if (lastredisclient != undefined) {
        var value = await redisClient.get(session_id);
        let cachedmessage = JSON.parse(value);

        if (cachedmessage) {
          cachedmessage = [...cachedmessage, messageobj];
        } else {
          cachedmessage = [messageobj];
        }
        redisClient.set(
          findDetails.chat_session_id,
          JSON.stringify(cachedmessage)
        );
      }
      if (data){ 
        logger.info(data._id+":AddMessage:Message added",data)
        return res.json({ msg: "Message added successfully." });}
      else {
        logger.error("AddMessage:Failed to add message db","")
        return res.json({ msg: "Failed to add message to the database" });}
    
      }

      if(findRealId.channel=="from_teams"){
 
        socket.current = io(config.socketUrl+tenantId);
        socket.current.emit("send-msg", {
          to: to,
          from: findRealId._id,
          msg,
          chatType: "inbound",
          msgType: "teams",
          senderName: findDetails.unique_id.username,
          msg_sent_type: msg_sent_type,
          session_id: req.body.chat_session_id,
          chatdetails: findRealId,
          chat_type: "external",
          userType: "external",
          file_name: req.body.file_name ? req.body.file_name : ""
        });
  
        const update = await db.Session.findByIdAndUpdate(
          { _id:findRealId._id},
          {
            $set: {
              lastmessage: msg,
              lastmessagetime:  moment().format("HH:MM A"),
              lastmessageUpdatedat:new Date()
            },
          }
        );
  
        socket.current.emit("last-msg-send", {
          to: to,
          from: findRealId._id,
          chatType: "inbound",
          msg,
          senderName: findDetails.unique_id
            ? findDetails.unique_id.username
            : "Guest",
            chatdetails: findRealId
        });
  
        let data = await db.Message.create({
          message: { text: msg },
          users: [from, String(to)],
          sender: from,
          receiver: to,
          senderName: findDetails.unique_id
            ? findDetails.unique_id.username
            : "Guest",
          receiverName: receiverName,
          session_id: req.body.chat_session_id,
          msg_sent_type: msg_sent_type,
          file_name: req.body.file_name
        });
  
        var messageobj = {
          fromSelf: false,
          message: msg,
          time: moment().format("lll"),
          sender: from,
          receiver: to,
          senderName: findDetails.unique_id
            ? findDetails.unique_id.username
            : "Guest",
          receiverName: receiverName,
          session_id: req.body.chat_session_id,
          senderName: findDetails.unique_id.username,
          msg_sent_type: msg_sent_type,
          file_name: req.body.file_name
        };
  
     //   console.log("object", messageobj);
        if (lastredisclient != undefined) {
          var value = await redisClient.get(session_id);
          let cachedmessage = JSON.parse(value);
  
          if (cachedmessage) {
            cachedmessage = [...cachedmessage, messageobj];
          } else {
            cachedmessage = [messageobj];
          }
          redisClient.set(
            findDetails.chat_session_id,
            JSON.stringify(cachedmessage)
          );
        }
        if (data) {
          logger.info(data._id+":AddMessage:Message added",data)
          return res.json({ msg: "Message added successfully." });}
        else{
          logger.error("AddMessage:Failed to add message to db","")
          return res.json({ msg: "Failed to add message to the database" });}
      
        }
  } catch (ex) {
    next(ex);
  }
}

async function addMessageFirst(req, res, next) {
  try {
    console.log("new session add msh");
    var variable=JSON.stringify(req.headers)
    var variable1=JSON.parse(variable)
    console.log(variable1.tenantid);
    
    var tenantId=variable1.tenantid;
    let session_id = req.body.chat_session_id;

    let findDetails = await db.Session.findOne({
      chat_session_id: session_id,
    }).populate("unique_id", "username");

    let receiver_details = await db.User.findOne({
      _id: findDetails.agent,
    });
    let from = findDetails.unique_id;
    let to = findDetails.agent;
    let msg = req.body.message;
    let senderName = findDetails.username;
    let receiverName = receiver_details.username;
    let msg_sent_type = req.body.msg_sent_type;
    let captions = req.body.msg_sent_type;

    let findRealId = await db.Session.findOne({
      unique_id: from
    });
   //console.log( "body url",JSON.stringify(from.id));
     if(findRealId.channel=="from_whatsapp"){
    if (msg_sent_type != "TEXT") {
      const formData = new FormData();
      formData.append('userID',from.id);
      formData.append('file',fs.createReadStream(req.body.message));
      formData.append('clientApp',"clientApp");
      formData.append('channel',"channel");
      formData.append('sessionId',JSON.stringify(session_id));
      formData.append('sentBy',"Customer");
     // console.log("formData",formData);

      const confignew = {
        headers: { TenantID: 123456 },
      };
      axios
        .post(config_file.fileserviceUrl+"/v1/fileServer/uploadMedia",formData, confignew)
        .then(async (resp) => {
       //   console.log("Image is coming from customer", resp.data);
         

          if (resp.data) {
            fs.unlinkSync(req.body.message,function(err){
              if(err) return console.log(err);
              console.log('file deleted successfully');
            })
          }
           msg=resp.data.data.signedUrl;
          socket.current = io(config.socketUrl+tenantId);
          socket.current.emit("send-msg", {
            to: to,
            from: findRealId._id,
            msg:resp.data.data.signedUrl,
            chatType: "inbound",
            msgType: "whatsApp",
            senderName: findDetails.unique_id.username,
            msg_sent_type: msg_sent_type,
            session_id: req.body.chat_session_id,
            chatdetails: findRealId,
            chat_type: "external",
            userType: "external",
            file_name: req.body.file_name ? req.body.file_name : "",
            captions:req.body.captions
          });


          socket.current.emit("last-msg-send", {
            to: to,
            from: findRealId._id,
            chatType: "inbound",
            msg:resp.data.data.signedUrl,
            senderName: findDetails.unique_id
              ? findDetails.unique_id.username
              : "Guest",
              chatdetails: findRealId
          });
          const update = await db.Session.findByIdAndUpdate(
            { _id:findRealId._id},
            {
              $set: {
                lastmessage: resp.data.data.signedUrl,
                lastmessagetime:  moment().format("HH:MM A"),
                lastmessageUpdatedat:new Date()
              },
            }
          );
          let data = await db.Message.create({
            message: { text: resp.data.data.signedUrl },
            users: [from, String(to)],
            sender: from,
            receiver: to,
            senderName: findDetails.unique_id
              ? findDetails.unique_id.username
              : "Guest",
            receiverName: receiverName,
            session_id: req.body.chat_session_id,
            msg_sent_type: msg_sent_type,
            file_name: req.body.file_name,
            message_id:req.body.msgId,
            captions:req.body.captions,
            context_id:req.body.context_id
          });
    
          var messageobj = {
            fromSelf: false,
            message: resp.data.data.signedUrl,
            time: moment().format("lll"),
            sender: from,
            receiver: to,
            senderName: findDetails.unique_id
              ? findDetails.unique_id.username
              : "Guest",
            receiverName: receiverName,
            session_id: req.body.chat_session_id,
            senderName: findDetails.unique_id.username,
            msg_sent_type: msg_sent_type,
            file_name: req.body.file_name,
            message_id:req.body.msgId,
            captions:req.body.captions,
            context_id:req.body.context_id
          };
    
       //   console.log("object", messageobj);
          if (lastredisclient != undefined) {
            var value = await redisClient.get(session_id);
            let cachedmessage = JSON.parse(value);
    
            if (cachedmessage) {
              cachedmessage = [...cachedmessage, messageobj];
            } else {
              cachedmessage = [messageobj];
            }
            redisClient.set(
              findDetails.chat_session_id,
              JSON.stringify(cachedmessage)
            );
          }
          if (data) {
            logger.info(data._id+":AddMessage:Message added",data)
            return res.json({ msg: "Message added successfully." });}
          else {
            logger.error("AddMessage:Falied add message to db","")
            return res.json({ msg: "Failed to add message to the database" });}

        })
    } else {
      socket.current = io(config.socketUrl+tenantId);
      socket.current.emit("send-msg", {
        to: to,
        from: findRealId._id,
        msg,
        chatType: "inbound",
        msgType: "whatsApp",
        senderName: findDetails.unique_id.username,
        msg_sent_type: msg_sent_type,
        session_id: req.body.chat_session_id,
        chatdetails: findRealId,
        chat_type: "external",
        userType: "external",
        file_name: req.body.file_name ? req.body.file_name : "",
        message_id:req.body.msgId,
        context_id:req.body.context_id,
        captions:req.body.captions
    
      });

      const update = await db.Session.findByIdAndUpdate(
        { _id:findRealId._id},
        {
          $set: {
            lastmessage: msg,
            lastmessagetime:  moment().format("HH:MM A"),
          },
        }
      );

      socket.current.emit("last-msg-send", {
        to: to,
        from: findRealId._id,
        chatType: "inbound",
        msg,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
          chatdetails: findRealId
      });

      let data = await db.Message.create({
        message: { text: msg },
        users: [from, String(to)],
        sender: from,
        receiver: to,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
        receiverName: receiverName,
        session_id: req.body.chat_session_id,
        msg_sent_type: msg_sent_type,
        file_name: req.body.file_name,
        message_id:req.body.msgId,
        context_id:req.body.context_id,
        captions:req.body.captions
      });

      var messageobj = {
        fromSelf: false,
        message: msg,
        time: moment().format("lll"),
        sender: from,
        receiver: to,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
        receiverName: receiverName,
        session_id: req.body.chat_session_id,
        senderName: findDetails.unique_id.username,
        msg_sent_type: msg_sent_type,
        file_name: req.body.file_name,
        message_id:req.body.msgId,
        context_id:req.body.context_id,
        captions:req.body.captions
      };

     // console.log("object", messageobj);
      if (lastredisclient != undefined) {
        var value = await redisClient.get(session_id);
        let cachedmessage = JSON.parse(value);

        if (cachedmessage) {
          cachedmessage = [...cachedmessage, messageobj];
        } else {
          cachedmessage = [messageobj];
        }
        redisClient.set(
          findDetails.chat_session_id,
          JSON.stringify(cachedmessage)
        );
      }
      if (data) {
        logger.info(data._id+":AddMessage:Message added",data)
        return res.json({ msg: "Message added successfully." });}
      else {
        logger.error("AddMessage:Failed to add message to the db","")
        return res.json({ msg: "Failed to add message to the database" });}
    }
      }
  if(findRealId.channel=="from_facebook"){
if(msg_sent_type=="TEXT"){
  socket.current = io(config.socketUrl+tenantId);
  socket.current.emit("send-msg", {
    to: to,
    from: findRealId._id,
    msg,
    chatType: "inbound",
    msgType: "facebook",
    senderName: findDetails.unique_id.username,
    msg_sent_type: msg_sent_type,
    session_id: req.body.chat_session_id,
    chatdetails: findRealId,
    chat_type: "external",
    userType: "external",
    file_name: req.body.file_name ? req.body.file_name : ""
  });

  const update = await db.Session.findByIdAndUpdate(
    { _id:findRealId._id},
    {
      $set: {
        lastmessage: msg,
        lastmessagetime:  moment().format("HH:MM A"),
        lastmessageUpdatedat:new Date()
      },
    }
  );

  socket.current.emit("last-msg-send", {
    to: to,
    from: findRealId._id,
    chatType: "inbound",
    msg,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
      chatdetails: findRealId
  });

  let data = await db.Message.create({
    message: { text: msg },
    users: [from, String(to)],
    sender: from,
    receiver: to,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
    receiverName: receiverName,
    session_id: req.body.chat_session_id,
    msg_sent_type: msg_sent_type,
    file_name: req.body.file_name
  });

  var messageobj = {
    fromSelf: false,
    message: msg,
    time: moment().format("lll"),
    sender: from,
    receiver: to,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
    receiverName: receiverName,
    session_id: req.body.chat_session_id,
    senderName: findDetails.unique_id.username,
    msg_sent_type: msg_sent_type,
    file_name: req.body.file_name
  };

 // console.log("object", messageobj);
  if (lastredisclient != undefined) {
    var value = await redisClient.get(session_id);
    let cachedmessage = JSON.parse(value);

    if (cachedmessage) {
      cachedmessage = [...cachedmessage, messageobj];
    } else {
      cachedmessage = [messageobj];
    }
    redisClient.set(
      findDetails.chat_session_id,
      JSON.stringify(cachedmessage)
    );
  }
  if (data){ 
    logger.info(data._id+":AddMessage:Message added",data)
    return res.json({ msg: "Message added successfully." });}
  else{ 
    logger.error("AddMessage:Failed to add message db","")
    return res.json({ msg: "Failed to add message to the database" });}
}else {
  socket.current = io(config.socketUrl+tenantId);
  socket.current.emit("send-msg", {
    to: to,
    from: findRealId._id,
    msg,
    chatType: "inbound",
    msgType: "facebook",
    senderName: findDetails.unique_id.username,
    msg_sent_type: msg_sent_type,
    session_id: req.body.chat_session_id,
    chatdetails: findRealId,
    chat_type: "external",
    userType: "external",
    file_name: req.body.file_name ? req.body.file_name : "Files",
    captions:req.body.captions
  });

  const update = await db.Session.findByIdAndUpdate(
    { _id:findRealId._id},
    {
      $set: {
        lastmessage: msg,
        lastmessagetime:  moment().format("HH:MM A"),
        lastmessageUpdatedat:new Date()
      },
    }
  );

  socket.current.emit("last-msg-send", {
    to: to,
    from: findRealId._id,
    chatType: "inbound",
    msg,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
      chatdetails: findRealId
  });

  let data = await db.Message.create({
    message: { text: msg },
    users: [from, String(to)],
    sender: from,
    receiver: to,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
    receiverName: receiverName,
    session_id: req.body.chat_session_id,
    msg_sent_type: msg_sent_type,
    file_name:  req.body.file_name ? req.body.file_name : "Files",
    captions:req.body.captions
  });

  var messageobj = {
    fromSelf: false,
    message: msg,
    time: moment().format("lll"),
    sender: from,
    receiver: to,
    senderName: findDetails.unique_id
      ? findDetails.unique_id.username
      : "Guest",
    receiverName: receiverName,
    session_id: req.body.chat_session_id,
    senderName: findDetails.unique_id.username,
    msg_sent_type: msg_sent_type,
    file_name:  req.body.file_name ? req.body.file_name : "Files",
    captions:req.body.captions
  };

//  console.log("object", messageobj);
  if (lastredisclient != undefined) {
    var value = await redisClient.get(session_id);
    let cachedmessage = JSON.parse(value);

    if (cachedmessage) {
      cachedmessage = [...cachedmessage, messageobj];
    } else {
      cachedmessage = [messageobj];
    }
    redisClient.set(
      findDetails.chat_session_id,
      JSON.stringify(cachedmessage)
    );
  }
  if (data) {
    logger.info(data._id+":AddMessage:Message added",data)
    return res.json({ msg: "Message added successfully." });}
  else {logger.error("AddMessage:Failed to add message to db","")
     return res.json({ msg: "Failed to add message to the database" });}
}
  }

  if(findRealId.channel=="from_twitter"){
 
      socket.current = io(config.socketUrl+tenantId);
      socket.current.emit("send-msg", {
        to: to,
        from: findRealId._id,
        msg,
        chatType: "inbound",
        msgType: "twitter",
        senderName: findDetails.unique_id.username,
        msg_sent_type: msg_sent_type,
        session_id: req.body.chat_session_id,
        chatdetails: findRealId,
        chat_type: "external",
        userType: "external",
        file_name: req.body.file_name ? req.body.file_name : ""
      });

      const update = await db.Session.findByIdAndUpdate(
        { _id:findRealId._id},
        {
          $set: {
            lastmessage: msg,
            lastmessagetime:  moment().format("HH:MM A"),
            lastmessageUpdatedat:new Date()
          },
        }
      );

      socket.current.emit("last-msg-send", {
        to: to,
        from: findRealId._id,
        chatType: "inbound",
        msg,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
          chatdetails: findRealId
      });

      let data = await db.Message.create({
        message: { text: msg },
        users: [from, String(to)],
        sender: from,
        receiver: to,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
        receiverName: receiverName,
        session_id: req.body.chat_session_id,
        msg_sent_type: msg_sent_type,
        file_name: req.body.file_name
      });

      var messageobj = {
        fromSelf: false,
        message: msg,
        time: moment().format("lll"),
        sender: from,
        receiver: to,
        senderName: findDetails.unique_id
          ? findDetails.unique_id.username
          : "Guest",
        receiverName: receiverName,
        session_id: req.body.chat_session_id,
        senderName: findDetails.unique_id.username,
        msg_sent_type: msg_sent_type,
        file_name: req.body.file_name
      };

     // console.log("object", messageobj);
      if (lastredisclient != undefined) {
        var value = await redisClient.get(session_id);
        let cachedmessage = JSON.parse(value);

        if (cachedmessage) {
          cachedmessage = [...cachedmessage, messageobj];
        } else {
          cachedmessage = [messageobj];
        }
        redisClient.set(
          findDetails.chat_session_id,
          JSON.stringify(cachedmessage)
        );
      }
      if (data){ 
        logger.info(data._id+":AddMessage:Message added",data)
        return res.json({ msg: "Message added successfully." });}
      else {
        logger.error("AddMessage:Failed to add message db","")
        return res.json({ msg: "Failed to add message to the database" });}
    
      }

      if(findRealId.channel=="from_teams"){
 
        socket.current = io(config.socketUrl+tenantId);
        socket.current.emit("send-msg", {
          to: to,
          from: findRealId._id,
          msg,
          chatType: "inbound",
          msgType: "teams",
          senderName: findDetails.unique_id.username,
          msg_sent_type: msg_sent_type,
          session_id: req.body.chat_session_id,
          chatdetails: findRealId,
          chat_type: "external",
          userType: "external",
          file_name: req.body.file_name ? req.body.file_name : ""
        });
  
        const update = await db.Session.findByIdAndUpdate(
          { _id:findRealId._id},
          {
            $set: {
              lastmessage: msg,
              lastmessagetime:  moment().format("HH:MM A"),
              lastmessageUpdatedat:new Date()
            },
          }
        );
  
        socket.current.emit("last-msg-send", {
          to: to,
          from: findRealId._id,
          chatType: "inbound",
          msg,
          senderName: findDetails.unique_id
            ? findDetails.unique_id.username
            : "Guest",
            chatdetails: findRealId
        });
  
        let data = await db.Message.create({
          message: { text: msg },
          users: [from, String(to)],
          sender: from,
          receiver: to,
          senderName: findDetails.unique_id
            ? findDetails.unique_id.username
            : "Guest",
          receiverName: receiverName,
          session_id: req.body.chat_session_id,
          msg_sent_type: msg_sent_type,
          file_name: req.body.file_name
        });
  
        var messageobj = {
          fromSelf: false,
          message: msg,
          time: moment().format("lll"),
          sender: from,
          receiver: to,
          senderName: findDetails.unique_id
            ? findDetails.unique_id.username
            : "Guest",
          receiverName: receiverName,
          session_id: req.body.chat_session_id,
          senderName: findDetails.unique_id.username,
          msg_sent_type: msg_sent_type,
          file_name: req.body.file_name
        };
  
     //   console.log("object", messageobj);
        if (lastredisclient != undefined) {
          var value = await redisClient.get(session_id);
          let cachedmessage = JSON.parse(value);
  
          if (cachedmessage) {
            cachedmessage = [...cachedmessage, messageobj];
          } else {
            cachedmessage = [messageobj];
          }
          redisClient.set(
            findDetails.chat_session_id,
            JSON.stringify(cachedmessage)
          );
        }
        if (data) {
          logger.info(data._id+":AddMessage:Message added",data)
          return res.json({ msg: "Message added successfully." });}
        else{
          logger.error("AddMessage:Failed to add message to db","")
          return res.json({ msg: "Failed to add message to the database" });}
      
        }
  } catch (ex) {
    next(ex);
  }
}

// API to get message from database
async function getMessage(req, res, next) {
  try {
    const { from, to, send, receive } = req.body;

    let msgFrom = req.body.messageFrom;
    let getId, from_id, to_id, checkId, static;

    if (msgFrom == "fromClient") {
      getId = await db.Session.findOne({
        _id: from,
      });

      if (getId) {
        from_id = getId.unique_id;
        to_id = to;
        static = from_id;
      }

      checkId = to_id;
    } else {
      getId = await db.Session.findOne({
        _id: to,
      });
      if (getId) {
        from_id = from;
        to_id = getId.unique_id;
        static = to_id;
      }
      checkId = from_id;
    }

    const messages = await db.Message.find({
      $or: [
        {
          sender: static,
        },
        {
          receiver: static,
        },
      ],
    }).sort({ updatedAt: 1 });

    const projectedMessages = messages.map((msg) => {
      return {
        fromSelf: msg.receiver.toString() === static.toString(),
        message: msg.message.text,
        time: msg.createdAt,
        sender: msg.sender,
        receiver: msg.receiver,
        senderName: msg.senderName,
        receiverName: msg.receiverName,
        msg_sent_type: msg.msg_sent_type,
        file_name: msg.file_name,
        captions:msg.captions,
        context_id:msg.context_id
        // author: msg.sender.username,
      };
    });
    logger.info("GetMessage:Message listed",projectedMessages)
    res.json(projectedMessages);
  } catch (ex) {
    next(ex);
  }
}

async function currentchathistory(req, res) {
  let sessionId = req.body.sessionId;

  if (lastredisclient != undefined) {
    const cacheResults = await redisClient.get(sessionId);
    var isCached;
    var results;

    // redisClient.set(sessionId, JSON.stringify(""));
    if (cacheResults && cacheResults.length != "2") {
    //  console.log("msg cache resultss", cacheResults);
      isCached = true;
      results = JSON.parse(cacheResults);
      // console.log(cacheResults, "cacheeee ");
      logger.info(sessionId+":CurrentChatHistory:chat history fetched",cacheResults)
      res.json({ cache: true, status: true, data: JSON.parse(cacheResults) });
      // await redisClient.set(userId, JSON.stringify(agentStatus));
    } else {
      let getMessages = await db.Message.find({
        session_id: sessionId,
      });

      if (getMessages && getMessages.length) {
        const messages = await db.Message.find({
          users: {
            $all: [String(getMessages[0].sender), getMessages[0].receiver],
          },
        }).sort({ updatedAt: 1 });

        const projectedMessages = messages.map((msg) => {
          return {
            fromSelf:
              msg.sender.toString() === getMessages[0].sender.toString(),
            message: msg.message.text,
            time: msg.createdAt,
            sender: msg.sender,
            receiver: msg.receiver,
            senderName: msg.senderName,
            receiverName: msg.receiverName,
            msg_sent_type: msg.msg_sent_type,
            file_name: msg.file_name,
            captions:msg.captions,
            context_id:msg.context_id
          };
        });
        if (projectedMessages.length != 0 && redisClient != undefined) {
          redisClient.set(sessionId, JSON.stringify(projectedMessages));
        }logger.info(sessionId+":CurrentChatHistory:chat history fetched",projectedMessages)
        res.json({ cache: false, status: true, data: projectedMessages });
      } else {
        logger.error("CurrentChatHistory:No data Found",)
        res.json({ status: false, message: "no data found" });
      }
    }
  } else {
    let getMessages = await db.Message.find({
      session_id: sessionId,
    });

    if (getMessages && getMessages.length) {
      const messages = await db.Message.find({
        users: {
          $all: [String(getMessages[0].sender), getMessages[0].receiver],
        },
      }).sort({ updatedAt: 1 });

      const projectedMessages = getMessages.map((msg) => {
        return {
          fromSelf:
            msg.sender.toString() == getMessages[0].sender.toString()
              ? true
              : false,
          message: msg.message.text,
          time: msg.createdAt,
          sender: msg.sender,
          receiver: msg.receiver,
          senderName: msg.senderName,
          receiverName: msg.receiverName,
          msg_sent_type: msg.msg_sent_type,
          file_name: msg.file_name,
          captions:msg.captions,
          context_id:msg.context_id
        };
      });
      if (projectedMessages.length != 0 && redisClient != undefined) {
        redisClient.set(sessionId, JSON.stringify(projectedMessages));
      }logger.info(sessionId+":CurrentChatHistory:chat history fetched",projectedMessages)
      res.json({ cache: false, status: true, data: projectedMessages });
    } else {
      logger.error("CurrentChatHistory:No data Found","")
      res.json({ status: false, message: "no data found" });
    }
  }
}
//List Message Pagination Api based on sender or reciver id 
async function listMessagePagination(req,res,next){
  try{
    var session_id=req.body.session_id;
    var messager_id=req.body.messager_id;
    var agent=req.body.agent;
    var offset=req.body.offset;
    var limit=req.body.limit;
    var messages=await db.Message.find({
      $and: [
        { session_id:{$ne:session_id} }
        ,{$or:[{$and:[
          {sender:mongoose.Types.ObjectId(messager_id)},{receiver:mongoose.Types.ObjectId(agent)}
        ]},{$and:[
          {sender:mongoose.Types.ObjectId(agent)},{receiver:mongoose.Types.ObjectId(messager_id)}
        ]}]}
      ],
    }).skip(offset)
    .limit(limit);
    var count=await db.Message.countDocuments({
      $and: [
        { session_id:{$ne:session_id} }
        ,{$or:[{$and:[
          {sender:mongoose.Types.ObjectId(messager_id)},{receiver:mongoose.Types.ObjectId(agent)}
        ]},{$and:[
          {sender:mongoose.Types.ObjectId(agent)},{receiver:mongoose.Types.ObjectId(messager_id)}
        ]}]}
      ],
    });
    if (messages && messages.length) {
    const projectedMessages = messages.map((msg) => {
      return {
        fromSelf: msg.sender.toString() === messages[0].sender.toString(),
        message: msg.message.text,
        time: msg.createdAt,
        sender: msg.sender,
        receiver: msg.receiver,
        senderName: msg.senderName,
        receiverName: msg.receiverName,
        msg_sent_type: msg.msg_sent_type,
        file_name: msg.file_name,
        captions:msg.captions
      };
    });
    logger.info(session_id+":ListMessagePagination:listed message based on sessionId",projectedMessages)
    res.json({ status: true, count:count,data: projectedMessages });
  }else {
    logger.error("ListMessagePagination:No data Found","")
    res.json({ status: false, message: "no data found" });
  }
  }
  catch(e){
    next(e)
  }
}
//List internal Message Pagination on sender or reciver
async function listInternalMessage(req,res,next){
  try{
    var session_id=req.body.session_id;
    var sender_id=req.body.sender_id;
    var receiver_id=req.body.receiver_id
    var offset=req.body.offset;
    var limit=req.body.limit;
    var messages=await db.Message.find({
      $and: [
        { session_id:{$ne:session_id} }
        ,{$or:[{$and:[
          {sender:mongoose.Types.ObjectId(sender_id)},{receiver:mongoose.Types.ObjectId(receiver_id)}
        ]},{$and:[
          {sender:mongoose.Types.ObjectId(receiver_id)},{receiver:mongoose.Types.ObjectId(sender_id)}
        ]}]}
      ],
    }).skip(offset)
    .limit(limit);
    var count=await db.Message.countDocuments({
      $and: [
        { session_id:{$ne:session_id} }
        ,{$or:[{$and:[
          {sender:mongoose.Types.ObjectId(sender_id)},{receiver:mongoose.Types.ObjectId(receiver_id)}
        ]},{$and:[
          {sender:mongoose.Types.ObjectId(receiver_id)},{receiver:mongoose.Types.ObjectId(sender_id)}
        ]}]}
      ],
    });
    if (messages && messages.length) {
    const projectedMessages = messages.map((msg) => {
      return {
        fromSelf: msg.sender.toString() === messages[0].sender.toString(),
        message: msg.message.text,
        time: msg.createdAt,
        sender: msg.sender,
        receiver: msg.receiver,
        senderName: msg.senderName,
        receiverName: msg.receiverName,
        msg_sent_type: msg.msg_sent_type,
        file_name: msg.file_name,
      };
    });
    logger.info(session_id+":ListInternalMessage:listed internal message",projectedMessages)
    res.json({ status: true, count:count,data: projectedMessages });
  }else {
    logger.error("ListInternalMessage:No data Found","")
    res.json({ status: false, message: "no data found" });
  }
  }
  catch(e){
    next(e)
  }
}
