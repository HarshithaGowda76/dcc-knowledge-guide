const express = require("express");
const router = express.Router();
const fileService = require("./fileUpload.service");
var multer = require("multer");
const storage = multer.memoryStorage()
const upload = multer({ storage: storage })

router.post("/uploadMedia", upload.single('file'),fileService.uploadMedia);
module.exports = router;