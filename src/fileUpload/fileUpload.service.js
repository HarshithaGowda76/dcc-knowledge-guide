const errorHandler = require("../../_helper/error.handler");
var axios=require('axios');
const FormData = require('form-data');
module.exports = {
    uploadMedia
  };
async function uploadMedia(req, res, next) {
    try {
      const file = req.body.file;
      const userID = req.body.userID;
      const clientApp = "chatapp";
      var sessionId = req.body.sessionId;
      var channel = req.body.channel;
      var sentBy="Agent";
      var err={}
     if (!userID) {
        err.message = "userId is missing";
        err.status = 400;
        err.errorMessage= [{ message: "userId is missing" }]
        err.data= []
      }
      if (!file) {
        err.message = "File is missing";
        err.status = 400;
        err.errorMessage= [{ message: "File is missing" }]
        err.data= []
      }
      if (!clientApp) {
        err.message = "clientApp is missing";
        err.status = 400;
        err.errorMessage= [{ message: "clientApp is missing" }]
        err.data= []
      }
      if (!channel) {
        err.message = "channel is missing";
        err.status = 400;
        err.errorMessage= [{ message: "channel is missing" }]
        err.data= []
      }
      if (!sessionId) {
        err.message = "sessionId is missing";
        err.status = 400;
        err.errorMessage= [{ message: "sessionId is missing" }]
        err.data= []
      }
      if (!sentBy) {
        err.message = "sentBy is missing";
        err.status = 400;
        err.errorMessage= [{ message: "sentBy is missing" }]
        err.data= []
      }
      if (err.message) return errorHandler(err, req, res, next);
    //  console.log(file)
      const formData = new FormData();
formData.append('userID', userID);
formData.append('file', file);
formData.append('clientApp',clientApp);
formData.append('channel',channel);
formData.append('sessionId',sessionId);
formData.append('sentBy',sentBy);
//console.log(formData)
      // const config = {
      //   headers: { TenantID: config_file.TenantID },
      // };
      // let data = { email: username, password: password };
     // console.log(formData)
     await axios
        .post("https://qacc.inaipi.ae/v1/fileServer/uploadUrl", formData, {
          headers:{ ...formData.getHeaders(),TenantID:123456}
        })
        .then(async (resp) => {
        //  console.log(resp)
          res.json({
                status: true,
                statusCode: 200,
                message: "File Uploaded Sucessfully",
                errorMessages: [],
                data: resp.data,
              });
        }).catch((err) => {
          console.log(err);
          res.json({
            status: false,
            statusCode: 200,
            message: "Invalid Details ",
            errorMessages: [{ message: "Invalid Details" }],
            data: [],
          });}
          )
     
    } catch (e) {
      res.json({
        status: false,
        statusCode: 200,
        message: "Invalid Details ",
        errorMessages: [{ message: "Invalid Details" }],
        data: [],
      });
    }
  }