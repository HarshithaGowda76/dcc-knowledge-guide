const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const schema = new Schema({
  clientApp: { type: String },
  signedUrl: { type: String },
  userID:{type:mongoose.Schema.Types.ObjectId},
  createdDate:{type:Date},
  isdeleted:{type:Boolean,default:false},
  channel:{type:String},
  sessionId:{type:String},
  mediaType:{type:String},
  path:{type:String},
  extension:{type:String},
  fileName:{type:String},
  originalName:{type:String},
  sentBy:{type:String},

});

schema.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    // remove these props when object is serialized
    delete ret._id;
  },
});

module.exports = mongoose.model("fileServer", schema);