const axios = require("axios");
const config = require("../../config.json");

const utils = require("./utils");
var embedToken = require("./embedConfigService");
module.exports = {
  getEmbedToken,
};

async function getEmbedToken(req, res, next) {
  try {
    console.log("Hi");
    // Validate whether all the required configurations are provided in config.json
    configCheckResult = utils.validateConfig();
    if (configCheckResult) {
      console.log("getEmbedTokenError");
      return res.status(400).send({
        error: configCheckResult,
      });
    }
    // Get the details like Embed URL, Access token and Expiry
    let result = await embedToken.getEmbedInfo();
    console.log("getEmbedToken", result);

    // result.status specified the statusCode that will be sent along with the result object
    res.status(result.status).send(result);
  } catch (error) {}
}
