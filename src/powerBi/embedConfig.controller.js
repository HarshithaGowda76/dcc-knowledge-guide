const express = require("express");
const router = express.Router();
const embedService = require("./embedConfig.service");

router.get("/getEmbedToken", embedService.getEmbedToken);

module.exports = router;
