// ----------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.
// ----------------------------------------------------------------------------

const auth = require("./authentication.js");
const config = require("../../config.json");
const utils = require("./utils.js");

const PowerBiReportDetails = require("./embedReportConfig");
const EmbedConfig = require("./embedConfig");

const fetch = require("node-fetch");
const axios = require("axios");

async function getEmbedInfo() {
  // Get the Report Embed details
  try {
    // Get report details and embed token
    const embedParams = await getEmbedParamsForSingleReport(
      config.workspaceId,
      config.reportId
    );

    return {
      accessToken: embedParams.embedToken.token,
      embedUrl: embedParams.reportsDetail,
      expiry: embedParams.embedToken.expiration,
      status: 200,
    };
  } catch (err) {
    return {
      status: err.status,
      error: `Error while retrieving report embed details\r\n${
        err.statusText
      }\r\nRequestId: \n${err.headers.get("requestid")}`,
    };
  }
}

async function getEmbedParamsForSingleReport(
  workspaceId,
  reportId,
  additionalDatasetId
) {
  const reportInGroupApi = `https://api.powerbi.com/v1.0/myorg/groups/${workspaceId}/reports/${reportId}`;
  const headers = await getRequestHeader();

  // Get report info by calling the PowerBI REST API
  const result = await fetch(reportInGroupApi, {
    method: "GET",
    headers: headers,
  });
  console.log("result", result);
  if (!result.ok) {
    throw result;
  }

  // Convert result in json to retrieve values
  const resultJson = await result.json();

  // Add report data for embedding
  var reportDetails = new PowerBiReportDetails(
    resultJson.id,
    resultJson.name,
    resultJson.embedUrl
  );

  // var reportdata = await reportDetails.save();
  const reportEmbedConfig = new EmbedConfig();

  // Create mapping for report and Embed URL
  reportEmbedConfig.reportsDetail = [reportDetails];

  // var reportembeddata = await reportEmbedConfig.save();

  // Create list of datasets
  let datasetIds = [resultJson.datasetId];

  // Append additional dataset to the list to achieve dynamic binding later
  if (additionalDatasetId) {
    datasetIds.push(additionalDatasetId);
  }

  // Get Embed token multiple resources
  reportEmbedConfig.embedToken =
    await getEmbedTokenForSingleReportSingleWorkspace(
      reportId,
      datasetIds,
      workspaceId
    );
  return reportEmbedConfig;
}

async function getEmbedTokenForSingleReportSingleWorkspace(
  reportId,
  datasetIds,
  targetWorkspaceId
) {
  // Add report id in the request
  let formData = {
    reports: [
      {
        id: reportId,
      },
    ],
  };

  // Add dataset ids in the request
  formData["datasets"] = [];
  for (const datasetId of datasetIds) {
    formData["datasets"].push({
      id: datasetId,
    });
  }

  // Add targetWorkspace id in the request
  if (targetWorkspaceId) {
    formData["targetWorkspaces"] = [];
    formData["targetWorkspaces"].push({
      id: targetWorkspaceId,
    });
  }

  const embedTokenApi = "https://api.powerbi.com/v1.0/myorg/GenerateToken";
  const headers = await getRequestHeader();

  // Generate Embed token for single report, workspace, and multiple datasets. Refer https://aka.ms/MultiResourceEmbedToken
  const result = await fetch(embedTokenApi, {
    method: "POST",
    headers: headers,
    body: JSON.stringify(formData),
  });
  console.log("data", result);
  if (!result.ok) throw result;
  return result.json();
}

async function getRequestHeader() {
  // Store authentication token
  let tokenResponse;

  // Store the error thrown while getting authentication token
  let errorResponse;

  // Get the response from the authentication request
  try {
    tokenResponse = await auth.getAccessToken();
  } catch (err) {
    if (
      err.hasOwnProperty("error_description") &&
      err.hasOwnProperty("error")
    ) {
      errorResponse = err.error_description;
    } else {
      // Invalid PowerBI Username provided
      errorResponse = err.toString();
    }
    return {
      status: 401,
      error: errorResponse,
    };
  }

  // Extract AccessToken from the response
  const token = tokenResponse.accessToken;
  return {
    "Content-Type": "application/json",
    Authorization: utils.getAuthHeader(token),
  };
}

module.exports = {
  getEmbedInfo: getEmbedInfo,
};
