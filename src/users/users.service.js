const { generateJwtToken } = require("../../_helper/helper");
const io = require("socket.io-client");
const db = require("../../_helper/db");
let socket = io();
const axios = require("axios");
const redis = require("redis");
const config_file = require("../../config.json");
const config = require("../../config.json");
let user_id_cache;
const { redisconfig } = require("../../_helper/redis.config");
var Mongoose = require("mongoose");
var ObjectId = Mongoose.Types.ObjectId;
let redisClient;
let lastredisclient = undefined;
const requestIp = require("request-ip");
const { default: mongoose } = require("mongoose");
var logger = require("../../_helper/logger");
var moment = require("moment");

(async () => {
  redisClient = redisconfig();

  redisClient.on("error", async (error) => {
    // redisClient=undefined;
    lastredisclient = undefined;
    console.error(`Error : ${error}`);
  });
  redisClient.on("connect", async (data) => {
    console.error(`Redis Started`);
    lastredisclient = "Connected";
    let getAvailAgents = await db.User.find({
      is_available: true,
    });

    console.error(`Getting All Agent Information`);
    // var promises = getAvailAgents.map((element) => {
    //   return new Promise(async (resolve, reject) => {
    //     const newincomingrequest = await db.Session.find({
    //       agent: element._id,
    //       status: {
    //         $in: ["newjoin", "Accept"],
    //       },
    //     }).populate("unique_id", "username email phonenumber");
    //     // console.log(newincomingrequest);
    //     redisClient.set(
    //       JSON.stringify(element._id),
    //       JSON.stringify(newincomingrequest)
    //     );
    //     console.error(`Agent Incoming Request Updation : ${element._id}`);
    //     resolve(element);
    //   });
    // });

    // Promise.all(promises).then(function (values) {
    //   console.log("All data restored to redis");
    //   lastredisclient = "Connected";
    // });
  });

  await redisClient.connect();
})();
module.exports = {
  login,
  getAllUsers,
  logOut,
  addClient,
  getClients,
  getId,
  updateAgentStatus,
  agentConfirmation,
  getIncomingUsers,
  getAgents,
  transferAgent,
  conferenceAgent,
  availableInternalAgents,
  createInternalAgentsSession,
  allInternalUsers,
  assignchattoagent,
  getInfoBasedOnNum,
  rejectchat,
  rejectchatnew,
  chatHistory,
  listUserBySkillsetIdLanguageId,
  listSupervisor,
  listagentList,
  allInternalUsersList,
  createInternalAgentsSessionNew,
  transfertoWhatsapp,
  logOutnew,
  assignchattoagentfromchatbot,
  updateClient,
  logoutall
};

// If the connection is closed or fails to be establish, it'll reconnect

// var amqpConn = null;

// function start() {
//   amqp.connect("amqp://localhost", function (err, conn) {
//     if (err) {
//       console.error("[AMQP]", err.message);
//       return setTimeout(start, 1000);
//     }
//     conn.on("error", function (err) {
//       if (err.message !== "Connection closing") {
//         console.error("[AMQP] conn error", err.message);
//       }
//     });
//     conn.on("close", function () {
//       console.error("[AMQP] reconnecting");
//       return setTimeout(start, 1000);
//     });
//     console.log("[AMQP] connected");
//     amqpConn = conn;
//     whenConnected();
//   });
// }

// function whenConnected() {
//   startPublisher();
//   startWorker();
// }

// var pubChannel = null;
// var offlinePubQueue = [];

// function startPublisher() {
//   // createConfirmChannel opens a channel that uses confirmation mode.
//   //  A channel in confirmation mode requires each published message to be acked  by the server,
//   // thereby indicating that it has been handled

//   amqpConn.createConfirmChannel(function (err, ch) {
//     if (closeOnErr(err)) return;
//     ch.on("error", function (err) {
//       console.error("[AMQP] channel error", err.message);
//     });
//     ch.on("close", function () {
//       console.log("[AMQP] channel closed");
//     });
//     pubChannel = ch;
//     while (true) {
//       var m = offlinePubQueue.shift();
//       if (!m) break;
//       publish(m);
//     }
//   });
// }

// //  method to publish messages, will queue messages internally if the connection is down and resend later

// function publish(exchange, routingKey, content) {
//   try {
//     pubChannel.publish(
//       exchange,
//       routingKey,
//       content,
//       { persistent: true },
//       function (err, ok) {
//         if (err) {
//           console.error("[AMQP] publish", err);
//           offlinePubQueue.push([exchange, routingKey, content]);
//           // console.log("queue" + offlinePubQueue.content);
//           pubChannel.connection.close();
//         }
//       }
//     );
//   } catch (e) {
//     console.error("[AMQP] publish", e.message);
//     offlinePubQueue.push([exchange, routingKey, content]);
//   }
// }

// //  Worker that acknowledge message only if processed successfully

// function startWorker() {
//   amqpConn.createChannel(function (err, ch) {
//     if (closeOnErr(err)) return;
//     ch.on("error", function (err) {
//       console.error("[AMQP] channel error", err.message);
//     });
//     ch.on("close", function () {
//       console.log("[AMQP] channel closed");
//     });
//     ch.prefetch(10);
//     ch.assertQueue("clients", { durable: true }, function (err, _ok) {
//       if (closeOnErr(err)) return;
//       ch.consume("clients", processMsg, { noAck: false });
//       console.log("Worker is started");
//     });
//     function processMsg(msg) {
//       work(msg, function (ok) {
//         try {
//           if (ok) {
//             ch.ack(msg);
//           } else {
//             console.log("reject");

//             ch.reject(msg, true);
//           }
//         } catch (e) {
//           closeOnErr(e);
//         }
//       });
//     }
//   });
// }

// async function work(msg, cb) {
//   input = JSON.parse(msg.content.toString());
//   // console.log(input);

//   let username = input.msg.name;
//   let email = input.msg.email_id;
//   let phonenumber = input.msg.phone;
//   let lattitude = input.msg.latitude;
//   let longitude = input.msg.longitute;
//   var {
//     channel,

//     client_ip,
//     device_type,
//     chat_session_id,
//     whatsapp_msg_id,
//   } = input.msg;

//   var limit = 50;

//   const aggregateOption = [
//     {
//       $match: {
//         is_available: true,
//       },
//     },
//     {
//       $group: {
//         _id: "$_id",
//         chat_count: {
//           $min: "$chat_count",
//         },
//       },
//     },
//   ];
//   let Client;
//   db.User.aggregate(aggregateOption)
//     .sort({ chat_count: 1 })
//     .exec()
//     .then(async (agents) => {
//       if (agents.length != 0) {
//         // agents.forEach(async (agent) => {
//         if (agents[0].chat_count != limit) {
//           const agent = agents[0]._id;
//           console.log("comes to add userss");
//           Client = await new db.Client({
//             username,
//             email,
//             phonenumber,
//             channel,
//             lattitude,
//             longitude,
//             client_ip,
//             device_type,
//             chat_session_id,
//             agent,
//             whatsapp_msg_id,
//           }).save();
//         }
//         // });

//         if (channel != "webchat") {
//           async function getData() {
//             const data = await axios.post(
//               `config_file.customurl/v1/users/getId/${email}`
//             );
//             console.log("this is id", data.data.user.id);
//             socket = io(config.socketUrl);
//             socket.emit("add-user", data.data.user.id);
//           }
//           if (Client) {
//             getData();
//           }
//         }
//       } else {
//         console.log("NO AGENTS");
//         // offlinePubQueue.push(
//         //   "",
//         //   "clients",
//         //   new Buffer.from(
//         //     JSON.stringify({
//         //       msg: input,
//         //     })
//         //   )
//         // );

//         // console.log("queueContent" + offlinePubQueue);

//         // res.json({ status: false, message: "no users available" });
//       }
//     });

//   cb(true);
// }

// function closeOnErr(err) {
//   if (!err) return false;
//   console.error("[AMQP] error", err);
//   amqpConn.close();
//   return true;
// }

// start();

// Login User API
async function login(req, res, next) {
  try {
    const username = req.body.email;
    const password = req.body.pass;
    var err = {};
    // check login
    var variable = JSON.stringify(req.headers)
    var variable1 = JSON.parse(variable)
    console.log(variable1.tenantid);

    var tenantId = variable1.tenantid;
    //console.log(req.rawHeaders[1])
    // console.log(tenantId)
    const config = {
      headers: { TenantID: tenantId },
    };
    let data = { email: username, password: password };

    axios
      .post(config_file.loginbaseurl + "/usermodule/login" + "/login", data, config)
      .then(async (resp) => {
        if (resp.data.status == "OK") {
          console.log("hi")
          let data = resp.data.dataList[0];
          logger.info(
            data.userId + ":Login:Login sucessfull for user " + username
          );
          let user_id = data.userId;
          const config1 = {
            headers: { TenantID: tenantId },
          };
          let datas = { userId: user_id };
          let lang_skillset = await axios.post(
            config_file.loginbaseurl + "/usermodule/clientUser/master",
            datas,
            config1
          );

          // console.log(lang_skillset.data);

          // if(lang_skillset.data.data.language !=null){

          var languageForAgent = lang_skillset.data.data
            ? lang_skillset.data.data.language
            : [{ "languageId": "", "proficiencyId": "", "languageCode": "", "languageDesc": "", "proficiencyDesc": "" }];
          var skillForAgent = lang_skillset.data.data
            ? lang_skillset.data.data.skillSet
            : [{ "skillId": "", "proficiencyId": "", "skillName": "", "proficiencyDesc": "" }];
          var channeldesc = lang_skillset.data.data ? lang_skillset.data.data.channel ? lang_skillset.data.data.channel : [{
            "channelName": "",
            "channelId": ""
          }] : [{
            "channelName": "",
            "channelId": ""
          }]
          var voiceData = lang_skillset.data.data ? lang_skillset.data.data.voiceData ? lang_skillset.data.data.voiceData : [{
            "voiceType": "",
            "avayaUserName": "",
            "avayaPassword": "",
            "avayaDomain": ""
          }] : [{
            "voiceType": "",
            "avayaUserName": "",
            "avayaPassword": "",
            "avayaDomain": ""
          }]
          // logger.info("Language Skillset value is available ");

          logger.info(
            data.userId +
            ":Skillset:Skillset for User " +
            username +
            " " +
            skillForAgent
          );

          logger.info(
            data.userId +
            ":Skillset:Skillset for User " +
            username +
            " " +
            languageForAgent
          );
          // var languageForAgent = [];
          // var skillForAgent = [];

          let userAlreadyAvailable = await db.User.findOne({
            user_id: user_id,
          });
          if (userAlreadyAvailable && userAlreadyAvailable.is_loggedIn) {
            logger.error(
              data.userId + ":Login:User is already logged In " + username
            );
            return res.json({
              status: false,
              user_id: user_id,
              message: "User Already Login",
            });
          }
          var userCount = await db.User.countDocuments({ is_loggedIn: true });
          //    console.log(userCount);
          let api_count = await axios.post(
            config_file.loginbaseurl + "/usermodule/login/getClientDetails",
            {},
            config1
          );
          if (api_count.data.status == "OK") {
            if (api_count.data.dataList[0].currentActiveUsers <= userCount) {
              logger.error(
                data.userId +
                ":Login limit Exceed Current Limit is: " +
                api_count.data.dataList[0].currentActiveUsers
              );
              return res.json({
                status: false,
                user_id: user_id,
                message: "Login limit Exceed",
              });
            }
          }
          //   console.log(api_count.data.dataList[0].currentActiveUsers);
          let userDetailsForToken;
          let doc = {
            username: data.firstName + " " + data.lastName,
            password: password,
            email: username,
            user_id: data.userId ? data.userId : "",
            mobile: data.mobileNumber,
            roles_array: data.roles,
            group_array: data.groups,
            skillSet: skillForAgent,
            language: languageForAgent,
            channel: channeldesc,
            voiceRecord: voiceData
          };
          if (userAlreadyAvailable) {
            var agent_id = userAlreadyAvailable.id;
            userDetailsForToken = userAlreadyAvailable;

            //console.log(typeof userDetailsForToken)

            // userDetailsForToken.push({role:rolename.roleName})
            let updateStatus = await db.User.findOneAndUpdate(
              {
                _id: userAlreadyAvailable._id,
              },
              {
                $set: {
                  username: data.firstName + " " + data.lastName,
                  password: password,
                  email: username,
                  user_id: data.userId ? data.userId : "",
                  mobile: data.mobileNumber,
                  roles_array: data.roles,
                  group_array: data.groups,
                  skillSet: skillForAgent,
                  language: languageForAgent,
                  channel: channeldesc,
                  voiceRecord: voiceData,
                  agent_id:agent_id,
                  queued_count:0
                },
              }
            );
            // logger.info("user already available not loggedin ");
            console.log(
              "user already available , no insertion req",
              updateStatus
            );
          } else {
            // logger.info("New User ");
            console.log("new user req");
            let saveDataToDb = new db.User(doc);
            await saveDataToDb.save();
            userDetailsForToken = saveDataToDb;
            var agent_id = saveDataToDb.id;
            var updatedvalue = await db.User.findOneAndUpdate(
              {
                _id:saveDataToDb.id,
              },
              {
                $set: {
                  agent_id:agent_id
                },
              }
            );
          }
          //  const jwtToken = generateJwtToken(userDetailsForToken);

          var cacheResults = [];
          var cache_resp = [];
          var todays_date = new Date().toISOString();
          todays_date = todays_date.replace(/\T.*/, "");
          var start_date = new Date(todays_date);
          var end_date = new Date(todays_date + "T23:59:59.999Z");
          var completedTask = await db.Session.countDocuments({
            $or: [
              { agent: mongoose.Types.ObjectId(userDetailsForToken._id) },
              {
                transfer_agent: mongoose.Types.ObjectId(
                  userDetailsForToken._id
                ),
              },
              {
                conference_agent: mongoose.Types.ObjectId(
                  userDetailsForToken._id
                ),
              },
            ],
            createdAt: { $gte: start_date, $lte: end_date },
            status: "chatEnded",
          });
          // var ongoing_chat = await db.Session.countDocuments({
          //   agent: mongoose.Types.ObjectId(userDetailsForToken._id),
          //   status: "Accept",
          //   createdAt: { $gte: start_date, $lte: end_date },
          // });
          var ongoing_chat = await db.chatCount.findOne({ created_at: start_date, agent_id: mongoose.Types.ObjectId(userDetailsForToken._id) }).select({ _id: 0, active_chat: 1, conference_basedactive_chat: 1 });

          var rolename = await db.roleManage
            .findOne({ id: data.roles })
            .select({ roleName: 1, _id: 0 });
          var rolenames = rolename?.roleName.trim();
          if (lastredisclient != undefined) {
            console.log("inside redis")
            var ongoingcount= ongoing_chat ? ongoing_chat.active_chat + ongoing_chat.conference_basedactive_chat : 0;
            var emailongoing=await db.Session.countDocuments({channel:"email",status:"Accept",agent_id: mongoose.Types.ObjectId(userDetailsForToken._id)});
            var emailcount=emailongoing?emailongoing:0
            var havalue = await redisClient.HSET(JSON.stringify("queuecount"), JSON.stringify(userDetailsForToken.id), 0)
            var haongoingCount=await redisClient.HSET(JSON.stringify("activechatcount"), JSON.stringify(userDetailsForToken.id), ongoingcount)
            var havalues = await redisClient.HSET(JSON.stringify("emailqueuecount"), JSON.stringify(userDetailsForToken.id), 0)
            var haongoingCountemail=await redisClient.HSET(JSON.stringify("emailactivechatcount"), JSON.stringify(userDetailsForToken.id), emailcount)
            console.log("havalue",havalue)
            console.log(haongoingCount,"ongoing")
            if (config_file.redisenv === "checking") {
              console.log("inside", userDetailsForToken.id)
              //var respone=await redisClient.hVals("agentsDetails")
              console.log("resp")
              var respone = await redisClient.hGet(JSON.stringify("agentsDetails"), JSON.stringify(userDetailsForToken.id))
              console.log("v", respone)

              var hashrespone = [];
              if (respone == null) {
                let users = userDetailsForToken;
                if (users) {
                  let agentStatus = [
                    {
                      id: users._id,
                      firstName: users.username,
                      lastName: null,
                      mobileNumber: users.mobile,
                      email: users.email,
                      userId: users.user_id,
                      roles: users.roles_array,
                      groups: users.group_array,
                      language: languageForAgent,
                      skillSet: skillForAgent,
                      channel: channeldesc,
                      voiceRecord: voiceData,
                      isSupvervisor: rolenames == "Supervisor" ? true : false,
                      status: "Connected",
                      chat: {
                        active_chat_count: 0,
                        last_chat_start_time: "",
                        last_chat_end_time: "",
                        total_ongoing: 0,
                        total_completed: 0,
                      },
                      activity: {
                        login_time: new Date(),
                      },
                    },
                  ];
                  //console.log("test agent status", agentStatus);
                  // socket.current = io(config.socketUrl);
                  // socket.current.emit("agent-activity-input", agentStatus);

                  // await redisClient.publish(
                  //   "agent-activity",
                  //   JSON.stringify(agentStatus)
                  // );
                  if (config_file.routingenv == "Redis") {
                    await redisClient.publish(
                      "agent-activity",
                      JSON.stringify(agentStatus)
                    );
                  } else {
                    if (agentStatus.length <= 0) {
                      var agentvalue = "Text Message"
                    } else {
                      var agentvalue = JSON.stringify(agentStatus[0])
                    }
                    var queuedetails = await axios.post(config_file.pushmessageapi, {
                      "queueName": "Test_ChatAPP_AgentActivity",
                      "routingKey": "Test_ChatAPP_AgentActivity",
                      "message": agentvalue,
                      "priority": 1
                    }, {
                      headers: { TenantID: tenantId },
                    })
                  }
                  await redisClient.hSet(
                    JSON.stringify("agentsDetails"),
                    JSON.stringify(userDetailsForToken.id), JSON.stringify(agentStatus)
                  );
                  // logger.info("New redis cache is added ");
                }
              }
              else {
                console.log("elseresp", respone)
                let users = userDetailsForToken;

                if (users) {
                  let agentStatus = {
                    id: users._id,
                    firstName: users.username,
                    lastName: null,
                    mobileNumber: users.mobile,
                    email: users.email,
                    userId: users.user_id,
                    roles: users.roles_array,
                    groups: users.group_array,
                    language: languageForAgent,
                    skillSet: skillForAgent,
                    channel: channeldesc,
                    voiceRecord: voiceData,
                    isSupvervisor: rolenames == "Supervisor" ? true : false,
                    status: "Connected",
                    chat: {
                      active_chat_count: ongoing_chat ? ongoing_chat.active_chat + ongoing_chat.conference_basedactive_chat : 0,
                      last_chat_start_time: "",
                      last_chat_end_time: "",
                      total_ongoing: ongoing_chat ? ongoing_chat.active_chat + ongoing_chat.conference_basedactive_chat : 0,
                      total_completed: completedTask ? completedTask : 0,
                      queued_count:users.queued_count?users.queued_count:0
                    },
                    activity: {
                      login_time: new Date(),
                    },
                  };
                  // socket.current = io(config.socketUrl);
                  // socket.current.emit("agent-activity-input", agentStatus);
                  if (config_file.routingenv == "Redis") {
                    await redisClient.publish(
                      "agent-activity",
                      JSON.stringify(agentStatus)
                    );
                  } else {
                    //    console.log(agentStatus)
                    if (agentStatus.length <= 0) {
                      var agentvalue = "Text Message"
                    } else {
                      var agentvalue = JSON.stringify(agentStatus)
                    }
                    var queuedetails = await axios.post(config_file.pushmessageapi, {
                      "queueName": "Test_ChatAPP_AgentActivity",
                      "routingKey": "Test_ChatAPP_AgentActivity",
                      "message": agentvalue,
                      "priority": 1
                    }, {
                      headers: { TenantID: tenantId },
                    })
                  }

                  //cache_resp.push(agentStatus);
                  // console.log(cache_resp)
                  await redisClient.hSet(
                    JSON.stringify("agentsDetails"),
                    JSON.stringify(userDetailsForToken.id), JSON.stringify(agentStatus)
                  );
                }
              }
            }
            else {
              cacheResults = await redisClient.get("agentsDetails");
              if (cacheResults) {
                cache_resp = JSON.parse(cacheResults);
              }
              if (cache_resp.length != 0) {
                const found = cache_resp.find(
                  (element) => element.userId == user_id
                );
                if (found) {
                  // logger.info("user already available redis ");
                  console.log("User already exist");
                } else {
                  console.log("new user plz append data");
                  //const users = await db.User.findOne({ user_id: user_id });

                  let users = userDetailsForToken;

                  if (users) {
                    let agentStatus = {
                      id: users._id,
                      firstName: users.username,
                      lastName: null,
                      mobileNumber: users.mobile,
                      email: users.email,
                      userId: users.user_id,
                      roles: users.roles_array,
                      groups: users.group_array,
                      language: languageForAgent,
                      skillSet: skillForAgent,
                      channel: channeldesc,
                      voiceRecord: voiceData,
                      isSupvervisor: rolenames == "Supervisor" ? true : false,
                      status: "Connected",
                      chat: {
                        active_chat_count: ongoing_chat ? ongoing_chat.active_chat + ongoing_chat.conference_basedactive_chat : 0,
                        last_chat_start_time: "",
                        last_chat_end_time: "",
                        total_ongoing: ongoing_chat ? ongoing_chat.active_chat + ongoing_chat.conference_basedactive_chat : 0,
                        total_completed: completedTask ? completedTask : 0,
                        queued_count:0,
                      },
                      activity: {
                        login_time: new Date(),
                      },
                    };
                    // socket.current = io(config.socketUrl);
                    // socket.current.emit("agent-activity-input", agentStatus);
                    if (config_file.routingenv == "Redis") {
                      await redisClient.publish(
                        "agent-activity",
                        JSON.stringify(agentStatus)
                      );
                    } else {
                      //    console.log(agentStatus)
                      if (agentStatus.length <= 0) {
                        var agentvalue = "Text Message"
                      } else {
                        var agentvalue = JSON.stringify(agentStatus)
                      }
                      var queuedetails = await axios.post(config_file.pushmessageapi, {
                        "queueName": "Test_ChatAPP_AgentActivity",
                        "routingKey": "Test_ChatAPP_AgentActivity",
                        "message": agentvalue,
                        "priority": 1
                      }, {
                        headers: { TenantID: tenantId },
                      })
                    }

                    cache_resp.push(agentStatus);
                    // console.log(cache_resp)
                    await redisClient.set(
                      "agentsDetails",
                      JSON.stringify(cache_resp)
                    );
                  }
                  // logger.info("new user added in avalible rediscache ");
                }
              } else {
                console.log("First data to redis");
                // const users = await db.User.findOne({ user_id: user_id });
                let users = userDetailsForToken;
                if (users) {
                  let agentStatus = [
                    {
                      id: users._id,
                      firstName: users.username,
                      lastName: null,
                      mobileNumber: users.mobile,
                      email: users.email,
                      userId: users.user_id,
                      roles: users.roles_array,
                      groups: users.group_array,
                      language: languageForAgent,
                      skillSet: skillForAgent,
                      channel: channeldesc,
                      voiceRecord: voiceData,
                      isSupvervisor: rolenames == "Supervisor" ? true : false,
                      status: "Connected",
                      chat: {
                        active_chat_count: ongoing_chat ? ongoing_chat.active_chat + ongoing_chat.conference_basedactive_chat : 0,
                        last_chat_start_time: "",
                        last_chat_end_time: "",
                        total_ongoing: ongoing_chat ? ongoing_chat.active_chat + ongoing_chat.conference_basedactive_chat : 0,
                        total_completed: completedTask ? completedTask : 0,
                        queued_count:0,
                      },
                      activity: {
                        login_time: new Date(),
                      },
                    },
                  ];
                  //console.log("test agent status", agentStatus);
                  // socket.current = io(config.socketUrl);
                  // socket.current.emit("agent-activity-input", agentStatus);

                  // await redisClient.publish(
                  //   "agent-activity",
                  //   JSON.stringify(agentStatus)
                  // );
                  if (config_file.routingenv == "Redis") {
                    await redisClient.publish(
                      "agent-activity",
                      JSON.stringify(agentStatus)
                    );
                  } else {
                    if (agentStatus.length <= 0) {
                      var agentvalue = "Text Message"
                    } else {
                      var agentvalue = JSON.stringify(agentStatus[0])
                    }
                    var queuedetails = await axios.post(config_file.pushmessageapi, {
                      "queueName": "Test_ChatAPP_AgentActivity",
                      "routingKey": "Test_ChatAPP_AgentActivity",
                      "message": agentvalue,
                      "priority": 1
                    }, {
                      headers: { TenantID: tenantId },
                    })
                  }
                  await redisClient.set(
                    "agentsDetails",
                    JSON.stringify(agentStatus)
                  );
                  // logger.info("New redis cache is added ");
                }
              }
            }

          }


          if (rolenames == "Supervisor") {
            let updateStatus = await db.User.findOneAndUpdate(
              {
                user_id: user_id,
              },
              {
                $set: {
                  agent_status: "Connected",
                  agent_status_real: "Connected",
                  is_loggedIn: true,
                  is_supervisor: true,
                  lastactive_time:new Date()
                },
              }
            );
          }
          else {
            let updateStatus = await db.User.findOneAndUpdate(
              {
                user_id: user_id,
              },
              {
                $set: {
                  agent_status: "Connected",
                  agent_status_real: "Connected",
                  is_loggedIn: true,
                  lastactive_time:new Date()
                },
              }
            );
          }
          // userDetailsForToken.rolename=rolename.roleName
          // console.log(rolename.roleName)
          //userDetailsForToken.rolename=rolename.roleName
          //console.log(userDetailsForToken)
          var role = rolename?.roleName.trim();
          console.log(role)
          var userDetails = {
            username: userDetailsForToken.username,
            email: userDetailsForToken.email,
            password: userDetailsForToken.password,
            user_id: userDetailsForToken.user_id,
            mobile: userDetailsForToken.mobile,
            is_available: userDetailsForToken.is_available,
            chat_count: userDetailsForToken.chat_count,
            chat_end_count: userDetailsForToken.chat_end_count,
            roles_array: userDetailsForToken.roles_array,
            group_array: userDetailsForToken.group_array,
            createdAt: userDetailsForToken.createdAt,
            updatedAt: userDetailsForToken.updatedAt,
            language: userDetailsForToken.language,
            skillSet: userDetailsForToken.skillSet,
            agent_status: userDetailsForToken.agent_status,
            is_loggedIn: userDetailsForToken.is_loggedIn,
            id: userDetailsForToken.id,
            role: role,
            channel: channeldesc,
            voiceRecord: voiceData,
            isSupvervisor: rolenames == "Supervisor" ? true : false,
            agent_id:userDetailsForToken.id,
          };
          var agentActivity = await db
            .agentActivity({
              agent_id: agent_id,
              user_id: user_id,
              activity_name: "USER_LOGGED_IN",
              value: "USER_LOGGED_IN",
              created_at: new Date(),
            })
            .save();
          var todays_date = new Date().toISOString();
          todays_date = todays_date.replace(/\T.*/, "");
          todays_date = new Date(todays_date);
          var logindata = await db.activeTime.findOne({
            agent_id: agent_id,
            created_at: todays_date,
          });
          if (!logindata) {
            await new db.activeTime({
              created_at: todays_date,
              agent_id: agent_id,
              loginDate: new Date(),
              is_login: true,
              updateDate: new Date()
            }).save();
          } else {
            var id = logindata._id;
            var countValue = await db.activeTime.findByIdAndUpdate(
              { _id: id },
              {
                loginDate: new Date(),
                is_login: true,
                updateDate: new Date()
              },
              { new: true }
            );
          }
          socket.current = io(config.socketUrl + tenantId);
          socket.current.emit("update-agent-status", user_id);
          logger.info(data.userId + ":Login:User Loggedin Successfully ");
          var agentdata={
            "agent_id":userDetails.id,
            "status":"Connected",
            "status_reason":"Connected",
            "notes":""
        }
        console.log("update",agentdata)
        const config2 = {
          headers: { TenantID:"a3dc14bd-fe70-4120-8572-461b0dc866b5"  },
          };
        await axios.post(
            config_file.interactionApi+"agentStatusChange/createAgentStatusChange",
          agentdata,
            config2
            );

          res.json({
            status: true,
            message: "Login Successfully",
            access_token_cognicx: data.accessToken,
            user: userDetails,
          });
          // }
          // else{
          //   // logger.info("No Language and Skillset Assigned user ");
          //   logger.error(data.userId+":Login:No Language and Skillset Assigned user"+username);
          //   res.json({
          //     status: false,
          //     message: "No Language and Skillset Assigned"
          //   });
          // }
        } else if (resp.data.statusCode == "500") {
          logger.error("Login:Internal Server Error " + resp.data.message);
          console.log("Internal Server Error");
          res.json({
            status: false,
            message: resp.data.message,
          });
        }
        else {
          // logger.info("Invalid username or password");
          logger.error("Login:Username or Password invalid " + username);
          console.log("username or password invalid");
          res.json({
            status: false,
            message: "Username or Password Invalid",
          });
        }
      })
      .catch((err) => {
        // logger.info("Login api  error occured");
        //logger.error("Login:User management Login Exceptions" + err);
        console.log(err);
        res.json({
          status: false,
          message: "Something Went Wrong !",
        });
      });
  } catch (e) {
    console.log(e);
    //next(e);
    // logger.info("Login went wrong");
    logger.error("Login:Chat Login Exceptions" + err);
    res.json({
      status: false,
      message: "Something Went Wrong !",
    });
  }
}

// get all clients having requested agent_id
async function getAllUsers(req, res, next) {
  try {
    if (lastredisclient != undefined) {
      var agen = JSON.stringify(req.query.agent_id)
      console.log('agent', (JSON.stringify(agen)))
      var ss = await redisClient.hVals(JSON.stringify(agen))
      //console.log(JSON.stringify(ss))
      console.log(typeof (ss))
      var valu1 = JSON.stringify(ss)
      console.log(typeof (valu1))
      var newval = JSON.parse(valu1);

      var newdata = newval.filter((value) => {
        var id = JSON.parse(value)
        console.log(typeof value)

        console.log(id["status"])
        return ((id["status"] == "Accept" &&
          id["chat_type"] == "external" &&
          id["conference_left_id"] != req.query.agent_id) ||
          (id["status"] == "Accept" &&
            id["chat_type"] == "external" &&
            id["conference_left_id"] != req.query.agent_id &&
            id["conference_agent"] == req.query.agent_id) ||
          (id["status"] == "Accept" &&
            id["chat_type"] == "external" &&
            id["conference_left_id"] == req.query.agent_id &&
            id["transfer_agent"] == req.query.agent_id &&
            id["agent"] == req.query.agent_id))

      })
      const resData = newdata.map(val => JSON.parse(val))
      //const cacheResults = await redisClient.get(req.query.agent_id);
      var isCached;
      var results;
      // if (cacheResults != '""' && cacheResults && cacheResults != "[]") {
      //   isCached = true;
      //   results = JSON.parse(cacheResults);
      //   let users = results.filter((id) => {
      //     return (
      //       (id["status"] == "Accept" &&
      //         id["chat_type"] == "external" &&
      //         id["conference_left_id"] != req.query.agent_id) ||
      //       (id["status"] == "Accept" &&
      //         id["chat_type"] == "external" &&
      //         id["conference_left_id"] != req.query.agent_id &&
      //         id["conference_agent"] == req.query.agent_id)||
      //         (id["status"] == "Accept" &&
      //         id["chat_type"] == "external" &&
      //         id["conference_left_id"] == req.query.agent_id &&
      //         id["transfer_agent"] == req.query.agent_id&&
      //         id["agent"] == req.query.agent_id)
      //     );
      //   });
      //   if (users.length >= 1) {
      //     console.log("if")
      //     logger.info("GetUsers:Getting all avalibile users ",users);
      //     res.json(users);
      //   } else {
      //     //console.log("firstelse")
      //     const users = await db.Session.find({
      //       $or: [
      //         {
      //           agent: req.query.agent_id,
      //           status: "Accept",
      //           chat_type: "external",
      //           conference_left_id: { $ne: req.query.agent_id },
      //         },
      //         {
      //           conference_agent: req.query.agent_id,
      //           status: "Accept",
      //           chat_type: "external",
      //           conference_left_id: { $ne: req.query.agent_id },
      //         },
      //         {
      //           agent: req.query.agent_id,
      //           status: "Accept",
      //           chat_type: "external",
      //           conference_left_id: { $eq: req.query.agent_id },
      //           transfer_agent:req.query.agent_id
      //         },
      //       ],
      //     })
      //     .sort({lastmessageUpdatedat: -1 })
      //       .populate("unique_id", "username email phonenumber whatsappnumber facebookId twitterId teamsId address company");
      //       logger.info("GetUsers:Getting all avalibile users ",users);
      //     return res.json(users);
      //   }
      // }
      if (resData) {
        resData.sort(function(a,b){
          // Turn your strings into dates, and then subtract them
          // to get a value that is either negative, positive, or zero.
          return new Date(b.lastmessageUpdatedat) - new Date(a.lastmessageUpdatedat);
        });
        console.log("res",resData)
        res.json(resData);
      }
      else {
        console.log("secondlastelse");
        const users = await db.Session.find({
          $or: [
            {
              agent: req.query.agent_id,
              status: "Accept",
              chat_type: "external",
              conference_left_id: { $ne: req.query.agent_id },
            },
            {
              conference_agent: req.query.agent_id,
              status: "Accept",
              chat_type: "external",
              conference_left_id: { $ne: req.query.agent_id },
            },
            {
              agent: req.query.agent_id,
              status: "Accept",
              chat_type: "external",
              conference_left_id: { $eq: req.query.agent_id },
              transfer_agent: req.query.agent_id
            },
          ],
        })
          .sort({ lastmessageUpdatedat: -1 })
          .populate("unique_id", "username email phonenumber whatsappnumber facebookId twitterId teamsId address company");
        // console.log(users)
        logger.info("GetUsers:Getting all avalibile users ", users);
        return res.json(users);
      }
    } else {
      // console.log("lastelse")
      const users = await db.Session.find({
        $or: [
          {
            agent: req.query.agent_id,
            status: "Accept",
            chat_type: "external",
            conference_left_id: { $ne: req.query.agent_id },
          },
          {
            conference_agent: req.query.agent_id,
            status: "Accept",
            chat_type: "external",
            conference_left_id: { $ne: req.query.agent_id },
          },
          {
            agent: req.query.agent_id,
            status: "Accept",
            chat_type: "external",
            conference_left_id: { $eq: req.query.agent_id },
            transfer_agent: req.query.agent_id
          },
        ],
      })
        .sort({ lastmessageUpdatedat: -1 })
        .populate("unique_id", "username email phonenumber whatsappnumber facebookId twitterId teamsId address company");
      logger.info("GetUsers:Getting all avalibile users ", users);
      return res.json(users);
    }
  } catch (error) {
    next(error);
  }
}

// get all agents having requested agent_id
async function allInternalUsers(req, res, next) {
  let create_req = req.body.create;
  var variable = JSON.stringify(req.headers)
  var variable1 = JSON.parse(variable)
  console.log(variable1.tenantid);

  var tenantId = variable1.tenantid;
  await db.User.findByIdAndUpdate(
    {
     _id:mongoose.Types.ObjectId(req.query.agent_id) 
    },
    {
      $set: {
        lastactive_time:new Date()
      },
    }
  );
  //var tenantId=req.headers.tenantId;
  try {
    var aggregateOption = [
      {
        $match: {
          status: "Accept",
          chat_type: "internal",
          $or: [
            {
              sender_id: new ObjectId(req.query.agent_id),
            },
            {
              receiver_id: new ObjectId(req.query.agent_id),
            },
          ],
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "sender_id",
          foreignField: "_id",
          as: "senderDetails",
          pipeline: [{ $project: { _id: 1, username: 1, agent_status: 1 } }],
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "receiver_id",
          foreignField: "_id",
          as: "reciverDetails",
          pipeline: [{ $project: { _id: 1, username: 1, agent_status: 1 } }],
        },
      },
      {
        $sort: {
          lastmessagetime: -1,
        },
      },
      {
        $sort: {
          updatedAt: -1,
        },
      },
      {
        $project: {
          id: "$_id",
          phonenumber: "$phonenumber",
          lastmessage: "$lastmessage",
          lastmessagetime: "$lastmessagetime",
          email: "$email",
          channel: "$channel",
          chat_type: "$chat_type",
          chat_session_id: "$chat_session_id",
          chat_duration: "$chat_duration",
          user_id: "$user_id",
          agent: "$agent",
          reciverDetails: "$reciverDetails",
          senderDetails: "$senderDetails",
          lastmessageUpdatedat: "$lastmessageUpdatedat"
          // agent_status: "$agent_status",
        },
      },
    ];
    var users = await db.Session.aggregate(aggregateOption).exec();
    // console.log(users);
    if (users) {
      if (create_req == "create") {
        socket.current = io(config.socketUrl + tenantId);
        socket.current.emit("send-internal", users);
      }
      logger.info("ListInternalAgents:Listing all available internal agents ", users);
      res.json({ success: true, data: users });
    } else {
      logger.error("ListInternalAgents:No internal users availble", "");
      res.json({ success: false, data: "" });
    }
  } catch (error) {
    res.json({ success: false, data: "try after sometimes" });
  }
}
// get all agents having agent_id
async function allInternalUsersList(req, res, next) {
  try {
    await db.User.findByIdAndUpdate(
      {
       _id:mongoose.Types.ObjectId(req.query.agent_id) 
      },
      {
        $set: {
          lastactive_time:new Date()
        },
      }
    );
    var aggregateOption = [
      {
        $match: {
          status: "Accept",
          chat_type: "internal",
          $or: [
            {
              sender_id: new ObjectId(req.query.agent_id),
            },
            {
              receiver_id: new ObjectId(req.query.agent_id),
            },
          ],
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "sender_id",
          foreignField: "_id",
          as: "senderDetails",
          pipeline: [{ $project: { _id: 1, username: 1, agent_status: 1,user_id:1 } }],
        },
      },
      {
        $lookup: {
          from: "users",
          localField: "receiver_id",
          foreignField: "_id",
          as: "reciverDetails",
          pipeline: [{ $project: { _id: 1, username: 1, agent_status: 1,user_id:1 } }],
        },
      },
      {
        $project: {
          id: "$_id",
          phonenumber: "$phonenumber",
          lastmessage: "$lastmessage",
          lastmessagetime: "$lastmessagetime",
          email: "$email",
          channel: "$channel",
          chat_type: "$chat_type",
          chat_session_id: "$chat_session_id",
          chat_duration: "$chat_duration",
          user_id: "$user_id",
          agent: "$agent",
          reciverDetails: "$reciverDetails",
          senderDetails: "$senderDetails",
          lastmessageUpdatedat: "$lastmessageUpdatedat"
          // agent_status: "$agent_status",
        },
      },
    ];
    var users = await db.Session.aggregate(aggregateOption).exec();
    if (users) {
      var user_id = [];
      users.forEach((elem) => {
        user_id.push(elem.user_id);
      });
      // console.log(user_id)
      var userlist = await db.User.find({ user_id: { $nin: user_id } }).select({
        username: 1,
        email: 1,
        user_id: 1,
        agent_status: 1,
      });
      // console.log(userlist)
      logger.info("ListInternalAgents:Listing all available internal agents ", users.concat(userlist));
      res.json({
        status: true,
        data: users.concat(userlist),
      });
    } else {
      var userlist = await db.User.find().select({
        username: 1,
        email: 1,
        user_id: 1,
        agent_status: 1,
      });
      // console.log(userlist)
      logger.info("ListInternalAgents:Listing all available internal agents ", userlist);
      res.json({
        status: true,
        data: userlist,
      });
    }
  } catch (e) {
    next(e);
  }
}
// async function getAvailAllUsers(req, res, next) {
//   try {
//     const users = await db.Session.find({
//       $or: [
//         {
//           agent: req.query.agent_id,
//           status: "Accept",
//           chat_type: "external",
//         },
//         {
//           conference_agent: req.query.agent_id,
//         },
//       ],
//     }).populate("unique_id", "username email phonenumber");

//     return res.json(users);
//   } catch (error) {
//     next(error);
//   }
// }

// get internal agents
async function availableInternalAgents(req, res) {
  try {
    let getAgents = await db.User.find();
    if (getAgents) {
      logger.info("ListInternalAgents:Listing all available internal agents ", getAgents);
      res.json({ success: true, message: "Records found", data: getAgents });
    } else {
      logger.error("ListInternalAgents:No agents are avalible", "");
      res.json({ success: false, message: " No records found" });
    }
  } catch (err) {
    res.json({ success: false, message: "try after sometime" });
  }
}

async function createInternalAgentsSession(req, res) {
  let userId = req.body.userId;
  let agentId = req.body.agentId;
  try {
    let filterAgents = await db.User.find({ user_id: userId });
    // let filterAgents = findAllAgents.filter((data) => {
    //   return userId == data.user_id;
    // });

    // check session already there or not
    await db.User.findByIdAndUpdate(
      {
       _id:mongoose.Types.ObjectId(agentId) 
      },
      {
        $set: {
          lastactive_time:new Date()
        },
      }
    );
    let checkSession = await db.AgentSession.findOne({
      user_id: userId,
      agent: new ObjectId(agentId),
    });
    let id = "session" + Math.random().toString(16).slice(2);
    let chat_session_id = id;
    let saveDoc;
    if (!checkSession) {
      saveDoc = new db.AgentSession({
        username: filterAgents[0].username,
        phonenumber: filterAgents[0].mobile,
        email: filterAgents[0].email,
        channel: "web",
        chat_session_id: chat_session_id,
        user_id: filterAgents[0].user_id,
        arrival_at: new Date(),
        chat_started_at: new Date(),
        agent: agentId,
        sender_id: agentId,
        receiver_id: filterAgents[0].id,
      });
      let save = await saveDoc.save();

      if (save) {
        // console.log("save doc data", saveDoc);
        session = await new db.Session({
          phonenumber: filterAgents[0].mobile,
          username: filterAgents[0].username,
          email: filterAgents[0].email,
          chat_session_id: chat_session_id,
          unique_id: save._id,
          channel: "web",
          user_id: filterAgents[0].user_id,
          arrival_at: new Date(),
          chat_started_at: new Date(),

          agent: agentId,
          status: "Accept",
          chat_type: "internal",
          chat_duration: "00:00:00",
          sender_id: agentId,
          receiver_id: filterAgents[0].id,
        }).save();
        var agentActivity = await db
          .agentActivity({
            agent_id: agentId,
            user_id: userId,
            activity_name: "USER_ININTITAED_INTERNAL_CHAT",
            value: chat_session_id,
            created_at: new Date(),
          })
          .save();
      }
      logger.info(userId + ":InternalAgentsSession:Created Internal agent session ", session);
      res.json({
        success: true,
        message: "session created",
        data: save,
        session: session,
      });
    } else {
      logger.error("InternalAgentsSession:Session already availble ", filterAgents);
      res.json({
        success: false,
        message: "session already available",
        data: filterAgents,
      });
    }
  } catch (err) {
    res.json({ success: false, message: "try after sometime" });
  }
}
//create internal Agent session new api
async function createInternalAgentsSessionNew(req, res, next) {
  try {
    let userId = req.body.userId;
    let agentId = req.body.agentId;
    let filterAgents = await db.User.find({ user_id: userId });
    await db.User.findByIdAndUpdate(
      {
       _id:mongoose.Types.ObjectId(agentId) 
      },
      {
        $set: {
          lastactive_time:new Date()
        },
      }
    );
    let checkSession = await db.AgentSession.findOne({
      $or: [
        {
          user_id: userId,
          sender_id: new ObjectId(agentId),
        },
        {
          user_id: userId,
          receiver_id: new ObjectId(agentId),
        },
      ],
    });
    let id = "session" + Math.random().toString(16).slice(2);
    let chat_session_id = id;
    let saveDoc;
    if (!checkSession) {
      saveDoc = new db.AgentSession({
        username: filterAgents[0].username,
        phonenumber: filterAgents[0].mobile,
        email: filterAgents[0].email,
        channel: "web",
        chat_session_id: chat_session_id,
        user_id: filterAgents[0].user_id,
        arrival_at: new Date(),
        chat_started_at: new Date(),
        agent: agentId,
        sender_id: agentId,
        receiver_id: filterAgents[0].id,
      });
      let save = await saveDoc.save();

      if (save) {
        // console.log("save doc data", saveDoc);
        session = await new db.Session({
          phonenumber: filterAgents[0].mobile,
          username: filterAgents[0].username,
          email: filterAgents[0].email,
          chat_session_id: chat_session_id,
          unique_id: save._id,
          channel: "web",
          user_id: filterAgents[0].user_id,
          arrival_at: new Date(),
          chat_started_at: new Date(),

          agent: agentId,
          status: "Accept",
          chat_type: "internal",
          chat_duration: "00:00:00",
          sender_id: agentId,
          receiver_id: filterAgents[0].id,
        }).save();
        var agentActivity = await db
          .agentActivity({
            agent_id: agentId,
            user_id: userId,
            activity_name: "USER_ININTITAED_INTERNAL_CHAT",
            value: chat_session_id,
            created_at: new Date(),
          })
          .save();
      }
      logger.info(userId + ":InternalAgentsSession:Created Internal agent session ", session);
      res.json({
        success: true,
        message: "session created",
        data: save,
        session: session,
      });
    } else {
      logger.error("InternalAgentsSession:Session already availble", filterAgents);
      res.json({
        success: false,
        message: "session already available",
        data: filterAgents,
      });
    }
  } catch (e) {
    next(e);
  }
}
// get

async function getClients(req, res, next) {
  try {
    const client = await db.Session.findById({
      _id: req.params.id,
    }).populate("agent", { username: 1 });
    // console.log("client" + client);
    //  console.log(client);
    if (client.agent === null) {
      logger.error("GetClients:No agent is avvailbile for client ", "");
      return res.json({ status: false, message: "No agent is available" });
    } else {
      var agent = [];
      if (client.agent) {
        const agent_id = client.agent._id;
        // console.log("agent_id" + agent_id);
        agent = await db.User.findById(agent_id);
      }

      // console.log("agent" + agent);
      logger.info(req.params.id + ":GetClients:Client details are got ", client);
      return res.json({ agents: agent, client: client });
    }
  } catch (error) {
    next(error);
  }
}

// get available agents
async function getAgents(req, res) {
  try {
    let results, filter;
    if (lastredisclient != undefined) {
      if (config_file.redisenv === "checking") {
        var ss = await redisClient.hVals(JSON.stringify("agentsDetails"))
        //console.log(JSON.stringify(ss))
        console.log(typeof (ss))
        var valu1 = JSON.stringify(ss)
        console.log(typeof (valu1))
        var newval = JSON.parse(valu1);

        var newdata = newval.filter((value) => {
          var parsedval = JSON.parse(value)
          console.log(typeof value)

          // console.log(parsedval["status"])
          return (parsedval["status"] == "Available" ||
            (parsedval["status"] == "Ready" && parsedval["userId"] != req.body.userId))

        })
        const resData = newdata.map(val => JSON.parse(val))
        logger.info("GetAgents:Agents list got " + JSON.stringify(resData));
        res.json({ isCached: true, success: true, data: resData });
      }
      else {
        const cacheResults = await redisClient.get("agentsDetails");

        if (cacheResults && cacheResults.length != 2) {
          isCached = true;

          results = JSON.parse(cacheResults);
          filter = results.filter((id) => {
            return (
              id["status"] == "Available" ||
              (id["status"] == "Ready" && id["userId"] != req.body.userId)
            );
          });

          // console.log(filter);
        } else {
          filter = await db.User.find({
            agent_status: {
              $in: ["Available", "Ready"],
            },
          });
          isCached = false;
        }
        logger.info("GetAgents:Agents list got " + filter);
        res.json({ isCached: isCached, success: true, data: filter });
      }
    } else {
      let getAvailAgents = await db.User.find({
        agent_status: {
          $in: ["Available", "Ready"],
        },
      });

      if (getAvailAgents) {
        logger.info("GetAgents:Agents list got ", getAvailAgents);
        res.json({ isCached: false, success: true, data: getAvailAgents });
      } else {
        logger.error("GetAgents:No agents avalible", "");
        res.json({
          success: false,
          data: "",
        });
      }
    }
  } catch (e) { }
}

// Logout API

async function logOut(req, res, next) {
  try {
    if (req.params.id) {
      var variable = JSON.stringify(req.headers)
      var variable1 = JSON.parse(variable)
      console.log(variable1.tenantid);

      var tenantId = variable1.tenantid;
      // var tenantId=req.headers.tenantId;
      if (lastredisclient != undefined) {
        if (config_file.redisenv == "checking") {
          var ss = await redisClient.hVals(JSON.stringify("agentsDetails"))
          // console.log(JSON.stringify(ss))
          if (ss) {
            console.log("type", typeof (ss))
            var valu1 = JSON.stringify(ss)
            console.log(typeof (valu1))
            var newval = JSON.parse(valu1);
            var agentStatusnew;
            var upadeddata = newval.map(async (value) => {
              var parseddata = JSON.parse(value);
              parseddata.forEach(async (parsedata) => {
                console.log("id", req.params.id, "userId", parsedata)
                if (parsedata.userId == req.params.id) {
                  parsedata.status = "Logged Out";
                  agentStatusnew = parsedata;
                  var valu3 = await redisClient.hDel(JSON.stringify("agentsDetails"), JSON.stringify(parsedata.id)
                    //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
                  )

                }
              })

              return parseddata;
            })
            if (config_file.routingenv == "Redis") {
              await redisClient.publish(
                "agent-activity",
                JSON.stringify(agentStatusnew)
              );
            } else {
              if (agentStatusnew.length <= 0) {
                var agentvalue = "Text Message"
              } else {
                var agentvalue = JSON.stringify(agentStatusnew[0])
              }
              var queuedetails = await axios.post(config_file.pushmessageapi, {
                "queueName": "Test_ChatAPP_AgentActivity",
                "routingKey": "Test_ChatAPP_AgentActivity",
                "message": agentvalue,
                "priority": 1
              }, {
                headers: { TenantID: tenantId },
              })
            }
          }
        }
        else {
          let cacheResults = await redisClient.get("agentsDetails");

          let cache_resp = JSON.parse(cacheResults);
          var agentStatus = [];
          if (cache_resp) {
            let filter = cache_resp.filter((id) => {
              return id["userId"] != req.params.id;
            });
            await redisClient.set("agentsDetails", JSON.stringify(filter));
            let agentStatusnew = cache_resp.filter((id) => {
              id["status"] = "Logged Out";
              return id["userId"] == req.params.id;
            });

            // await redisClient.publish(
            //   "agent-activity",
            //   JSON.stringify(agentStatusnew)
            // );
            if (config_file.routingenv == "Redis") {
              await redisClient.publish(
                "agent-activity",
                JSON.stringify(agentStatusnew)
              );
            } else {
              if (agentStatusnew.length <= 0) {
                var agentvalue = "Text Message"
              } else {
                var agentvalue = JSON.stringify(agentStatusnew[0])
              }
              var queuedetails = await axios.post(config_file.pushmessageapi, {
                "queueName": "Test_ChatAPP_AgentActivity",
                "routingKey": "Test_ChatAPP_AgentActivity",
                "message": agentvalue,
                "priority": 1
              }, {
                headers: { TenantID: tenantId },
              })
            }
          }
        }
      }

      let updateStatus = await db.User.findOneAndUpdate(
        {
          user_id: req.params.id,
        },
        {
          $set: {
            agent_status: "Not Available",
            agent_status_real: "Is LoggedOut",
            is_loggedIn: false,
            lastactive_time:new Date()
          },
        }
      );
      console.log("test teh session", req.params.id);
      // let updateStatusSession = await db.Session.findOneAndUpdate(
      //   {
      //     user_id: req.params.id,
      //   },
      //   {
      //     $set: {
      //       agent_status: "Not Available",
      //     },
      //   }
      // );
      // let data = { email: username, password: password };
      // axios
      // .post(config_file.loginapi+"/logout", data, config)
      // .then(async (resp) => {

      // })
      var agentActivity = await db
        .agentActivity({
          agent_id: updateStatus.id,
          user_id: req.params.id,
          activity_name: "USER_LOGGED_OUT",
          value: "USER_LOGGED_OUT",
          created_at: new Date(),
        })
        .save();
      var todays_date = new Date().toISOString();
      todays_date = todays_date.replace(/\T.*/, "");
      todays_date = new Date(todays_date);
      var logindata = await db.activeTime.findOne({
        agent_id: updateStatus.id,
        created_at: todays_date,
      });
      if (logindata) {
        var id = logindata._id;
        // var login_time = logindata.updateDate;
        // var current_time = new Date();
        // var logdates = moment(login_time, "YYYY-M-DD HH:mm:ss");
        // var dates = moment(current_time, "YYYY-M-DD HH:mm:ss");
        // var diff = dates.diff(logdates, "seconds");
        var countValue = await db.activeTime.findByIdAndUpdate(
          { _id: id },
          {
            //  duration_login: duration,
            is_login: false,
            breakvalue: false,
            // updateDate:new Date()
          },
          { new: true }
        );
      }
      socket.current = io(config.socketUrl + tenantId);
      socket.current.emit("update-agent-status", req.params.id);
      logger.info(req.params.id + ":Logout:Agent Logged out ", updateStatus);
      var agentdata={
        "agent_id":updateStatus.id,
        "status":"Not Available",
        "status_reason":"Is LoggedOut",
        "notes":""
    }
    console.log("update",agentdata)
    const config2 = {
      headers: { TenantID:"a3dc14bd-fe70-4120-8572-461b0dc866b5" },
      };
     await axios.post(
        config.interactionApi+"agentStatusChange/createAgentStatusChange",
      agentdata,
        config2
        );
      return res.json({
        success: true,
        message: "Agent Logged out",
        userId: req.params.id,
      });
    } else {
      logger.error("Logout:User id not valid ", req.params.id);
      return res.json({
        success: false,
        message: "User id not valid",
        userId: req.params.id,
      });
    }
  } catch (ex) {
    logger.error("Logout:User id not valid ", req.params.id);
    return res.json({
      success: false,
      message: "User id not valid",
      userId: req.params.id,
    });
    console.log("error", ex);
  }
}
//logoutnew api
async function logOutnew(req, res, next) {
  try {
    if (req.params.id) {
      var variable = JSON.stringify(req.headers)
      var variable1 = JSON.parse(variable)
      console.log(variable1.tenantid);

      var tenantId = variable1.tenantid;
      // var tenantId=req.headers.tenantId;
      
      var agentname = await db.User.findOne({ user_id: req.params.id }).select({
        _id: 1,
      });
      await db.User.findByIdAndUpdate(
        {
         _id:mongoose.Types.ObjectId(agentname._id) 
        },
        {
          $set: {
            lastactive_time:new Date()
          },
        }
      );
      var agents=JSON.stringify(agentname._id);
    var ss= await redisClient.hVals(JSON.stringify(agents))
   // console.log("values",ss)
 console.log(typeof(ss))
  var valu1=JSON.stringify(ss)
  console.log(typeof(valu1))
 // console.log("values",valu1)
 var newval = JSON.parse(valu1);
 //console.log("val",newval)
 var count=0;
 await newval.map(async (value) => {
  var parseddata = JSON.parse(value);
  console.log("value",parseddata)
  if (parseddata["status"] == "Accept" && parseddata["chat_type"]== "external") {
   
     count=count+1;
   // return count;
     
  } 
})
  console.log("valuecount",count)

      var activesessionCount = await db.Session.countDocuments({
        chat_type: "external",
        status: "Accept",
        $or: [
          { user_id: req.params.id, conference: false, transferred: false },
          { agent: agentname._id, transferred: true },
          {
            conference: true,
            user_id: req.params.id,
            conference_left_id: { $ne: agentname._id },
          },
          {
            conference: true,
            conference_agent: agentname._id,
            conference_left_id: { $ne: agentname._id },
          },
        ],
      });
      var value = await db.Session.find({
        chat_type: "external",
        status: "Accept",
        $or: [
          { user_id: req.params.id, conference: false, transferred: false },
          { agent: agentname._id, transferred: true },
          {
            conference: true,
            user_id: req.params.id,
            conference_left_id: { $ne: agentname._id },
          },
          {
            conference: true,
            conference_agent: agentname._id,
            conference_left_id: { $ne: agentname._id },
          },
        ],
      })
      // console.log(value)
      if (count > 0) {
        logger.error("Logout:Active session available ", req.params.id);
        return res.json({
          success: false,
          message: "Active session available",
          userId: req.params.id,
        });
      } else {
        if (lastredisclient != undefined) {
          if (config_file.redisenv == "checking") {
            var ss = await redisClient.hVals(JSON.stringify("agentsDetails"))
            //console.log(JSON.stringify(ss))
            if (ss) {
              console.log(typeof (ss))
              var valu1 = JSON.stringify(ss)
              console.log(typeof (valu1))
              var newval = JSON.parse(valu1);
              var agentStatusnew;
              var upadeddata = newval.map(async (value) => {
                var parseddata = JSON.parse(value);
                if (parseddata["userId"] == req.params.id) {
                  parseddata["status"] = "Logged Out";
                  agentStatusnew = parseddata;
                  var valu3 = await redisClient.hDel(JSON.stringify("agentsDetails"), JSON.stringify(parseddata["id"])
                    //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
                  )

                }
                return parseddata;
              })
              if (config_file.routingenv == "Redis") {
                await redisClient.publish(
                  "agent-activity",
                  JSON.stringify(agentStatusnew)
                );
              } else {
                //agentStatusnew==" []"?"Text Message":agentStatusnew
                if (!agentStatusnew) {
                  var agentvalue = "Text Message"
                } else {
                  var agentvalue = JSON.stringify(agentStatusnew[0])
                }
                // console.log("agent",agentStatusnew)
                var queuedetails = await axios.post(config_file.pushmessageapi, {
                  "queueName": "Test_ChatAPP_AgentActivity",
                  "routingKey": "Test_ChatAPP_AgentActivity",
                  "message": agentvalue,
                  "priority": 1
                }, {
                  headers: { TenantID: tenantId },
                })
              }
            }
          }
          else {
            let cacheResults = await redisClient.get("agentsDetails");

            let cache_resp = JSON.parse(cacheResults);
            var agentStatus = [];
            let filter = cache_resp.filter((id) => {
              return id["userId"] != req.params.id;
            });
            await redisClient.set("agentsDetails", JSON.stringify(filter));
            // console.log("filter", filter);
            let agentStatusnew = cache_resp.filter((id) => {
              id["status"] = "Logged Out";
              return id["userId"] == req.params.id;
            });
            // console.log("agentsstatus", agentStatusnew);
            // await redisClient.publish(
            //   "agent-activity",
            //   JSON.stringify(agentStatusnew)
            // );
            if (config_file.routingenv == "Redis") {
              await redisClient.publish(
                "agent-activity",
                JSON.stringify(agentStatusnew)
              );
            } else {
              //agentStatusnew==" []"?"Text Message":agentStatusnew
              if (agentStatusnew.length <= 0) {
                var agentvalue = "Text Message"
              } else {
                var agentvalue = JSON.stringify(agentStatusnew[0])
              }
              // console.log("agent",agentStatusnew)
              var queuedetails = await axios.post(config_file.pushmessageapi, {
                "queueName": "Test_ChatAPP_AgentActivity",
                "routingKey": "Test_ChatAPP_AgentActivity",
                "message": agentvalue,
                "priority": 1
              }, {
                headers: { TenantID: tenantId },
              })
            }
          }

        }

        let updateStatus = await db.User.findOneAndUpdate(
          {
            user_id: req.params.id,
          },
          {
            $set: {
              agent_status: "Not Available",
              agent_status_real: "Is LoggedOut",
              is_loggedIn: false,
            },
          }
        );
        console.log("test teh session", req.params.id);
        let updateStatusSession = await db.Session.findOneAndUpdate(
          {
            user_id: req.params.id,
          },
          {
            $set: {
              agent_status: "Not Available",
            },
          }
        );
        // let data = { email: username, password: password };
        // axios
        // .post(config_file.loginapi+"/logout", data, config)
        // .then(async (resp) => {

        // })
        var agentActivity = await db
          .agentActivity({
            agent_id: updateStatus.id,
            user_id: req.params.id,
            activity_name: "USER_LOGGED_OUT",
            value: "USER_LOGGED_OUT",
            created_at: new Date(),
          })
          .save();
        var todays_date = new Date().toISOString();
        todays_date = todays_date.replace(/\T.*/, "");
        todays_date = new Date(todays_date);
        var logindata = await db.activeTime.findOne({
          agent_id: updateStatus.id,
          created_at: todays_date,
        });
        if (logindata) {
          var id = logindata._id;
          var login_time = logindata.updateDate;
          var current_time = new Date();
          var logdates = moment(login_time, "YYYY-M-DD HH:mm:ss");
          var dates = moment(current_time, "YYYY-M-DD HH:mm:ss");
          var diff = dates.diff(logdates, "seconds");
          var duration = logindata.duration_login + diff;
          var countValue = await db.activeTime.findByIdAndUpdate(
            { _id: id },
            {
              duration_login: duration,
              is_login: false,
              breakvalue: false,
              updateDate: new Date()
            },
            { new: true }
          );
        }
        socket.current = io(config.socketUrl + tenantId);
        socket.current.emit("update-agent-status", req.params.id);
        logger.info(req.params.id + ":Logout:Agent Logged out ", updateStatus);
        var agentdata={
          "agent_id":updateStatus.id,
          "status":"Not Available",
          "status_reason":"Is LoggedOut",
          "notes":""
      }
      console.log("update",agentdata)
      const config2 = {
        headers: { TenantID:'a3dc14bd-fe70-4120-8572-461b0dc866b5' },
        };
      await axios.post(
        config.interactionApi+"agentStatusChange/createAgentStatusChange",
        agentdata,
          config2
          );
        return res.json({
          success: true,
          message: "Agent Logged out",
          userId: req.params.id,
        });
      }
    } else {
      logger.error("Logout:User id not valid ", req.params.id);
      return res.json({
        success: false,
        message: "User id not valid",
        userId: req.params.id,
      });
    }
  } catch (ex) {
    console.log("error", ex);
  }
}
// API to transfer agent
async function transferAgent(req, res) {
  let sessionId = req.body.sessionId;
  let agent_id = req.body.agentId;
  let old_agent_id = req.body.oldAgentId;
  // var tenantId=req.headers.tenantId;
  var variable = JSON.stringify(req.headers)
  var variable1 = JSON.parse(variable)
  console.log(variable1.tenantid);

  var tenantId = variable1.tenantid;
  var sessionavailble = await db.Session.findOne({ chat_session_id: sessionId }).select({ conference: 1, is_conferenceleft: 1 })
  if (sessionavailble.conference == true && sessionavailble.is_conferenceleft == false) {
    await db.User.findByIdAndUpdate(
      {
       _id:mongoose.Types.ObjectId(old_agent_id) 
      },
      {
        $set: {
          lastactive_time:new Date()
        },
      }
    );
    logger.error("TransferAgent:Already in Conference", sessionId);
    return res.json({
      status: false,
      message: "Transfer can not possible it is already in Conference",
    });
  } else {
    if (lastredisclient != undefined) {
      // var value = await redisClient.get(JSON.stringify(old_agent_id));
      // let cachedrequestlist = JSON.parse(value);
      // if (cachedrequestlist) {
      //   let filteredArray = cachedrequestlist.filter((item) => {
      //     return item.chat_session_id !== sessionId;
      //   });
      //   redisClient.set(
      //     JSON.stringify(old_agent_id),
      //     JSON.stringify(filteredArray)
      //   );
      // }
      var agents = JSON.stringify(old_agent_id);
      var respons = await redisClient.hDel(JSON.stringify(agents), JSON.stringify(sessionavailble._id))
      console.log(respons)
      //     var ss= await redisClient.hVals(JSON.stringify(agents))
      //     //console.log(JSON.stringify(ss))
      //  console.log(typeof(ss))
      //   var valu1=JSON.stringify(ss)
      //   console.log(typeof(valu1))
      //   var newval=JSON.parse(valu1);
      //   var upadeddata=newval.map((value)=>{
      //     var parseddata=JSON.parse(value);
      //    if( parseddata["chat_session_id"] == sessionId){
      //     parseddata["status"] = "Chat End";
      //     parseddata["chat_type"] = "external";
      //           parseddata["chat_started_at"] = new Date();
      //            parseddata["is_queued"] = false;
      //           var valu3=  redisClient.hSet(JSON.stringify(agents), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
      //         //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
      //         )
      //         console.log("value",valu3)
      //    }
      //    return parseddata;
      //   })
    }
    
    let updateAgent = await db.Session.findOneAndUpdate(
      {
        chat_session_id: sessionId,
      },
      {
        $set: {
          transferred: true,
          transfer_agent_time: new Date(),
          transfer_agent: agent_id,
          agent: agent_id,
          status: "newjoin",
          conference:false,
          is_conferenceleft:false,
          conference_agent:null,
          conference_left_id:null
        },
      }
    );
    await db.Session.findOneAndUpdate(
      {
        chat_session_id: sessionId, conference: true
      },
      {
        $set: {
          conference: false
        },
      }
    );
    var transferAgent = await db
      .chatHistory({
        agent_id: old_agent_id,
        destinationagent_id: agent_id,
        user_id: updateAgent.user_id,
        time: new Date(),
        session_id: sessionId,
        type: "Transfer",
      })
      .save();
    var agentActivity = await db
      .agentActivity({
        agent_id: agent_id,
        user_id: updateAgent.user_id,
        activity_name: "USER_TRANSFER_CHAT",
        value: sessionId,
        created_at: new Date(),
      })
      .save();
      await db.User.findByIdAndUpdate(
        {
         _id:mongoose.Types.ObjectId(old_agent_id) 
        },
        {
          $set: {
            lastactive_time:new Date()
          },
        }
      );
      var transfer_data={
  
        interaction_id:updateAgent.chat_session_id,
        Soure_Ref_id:"1",
        transfer_Estabilish_time: "10s",
        transfer_Start_time:new Date(),
        transfer_from: old_agent_id,
        transfer_to: agent_id,
        transfer_type: "attended",
        transfer_notes: "transferred and solved",
        isfinished: false
    
     }
     const config1 = {
      headers: { TenantID: tenantId },
      };
       axios.post(
        config.interactionApi+"transferDetails/createTransferDetails",
        transfer_data,
        config1
        );
    if (updateAgent) {
      socket.current = io(config.socketUrl + tenantId);

      let findAgent = await db.Session.find({
        chat_session_id: sessionId,
        status: "newjoin",
      }).populate("unique_id", "username email phonenumber");

      let agent_id_incoming = agent_id;
      var agents = JSON.stringify(agent_id_incoming)
      if (lastredisclient != undefined) {
        // var value = await redisClient.get(JSON.stringify(agent_id_incoming));
        // let cachedrequestlist = JSON.parse(value);

        // if (cachedrequestlist) {
        //   cachedrequestlist = [...cachedrequestlist, findAgent[0]];
        // } else {
        //   cachedrequestlist = [findAgent[0]];
        // }

        // redisClient.set(
        //   JSON.stringify(agent_id_incoming),
        //   JSON.stringify(cachedrequestlist)
        // );
        var v = await redisClient.hSet(JSON.stringify(agents), JSON.stringify(findAgent[0]._id), JSON.stringify(findAgent[0])
          //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
        )
        var havalue = await redisClient.HINCRBY(JSON.stringify("queuecount"), JSON.stringify(agent_id), 1)
        var haongoingCount=await redisClient.HINCRBY(JSON.stringify("activechatcount"), JSON.stringify(old_agent_id), -1)
        console.log("value", v)
      }
      socket.current.emit("send-new-req", findAgent[0]);
      logger.info(sessionId + ":TransferAgent:Chat transferred to another agent ", updateAgent);
      res.json({ status: true, message: "agent transferred" });
    } else {
      logger.error("TransferAgent:Chat not transferred ", sessionId);
      res.json({ status: false, message: "no data found" });
    }
  }
}

// API to conference agent
async function conferenceAgent(req, res) {
  let sessionId = req.body.sessionId;
  let agent_id = req.body.agentId;
  let old_agent_id = req.body.oldAgentId;
  // var tenantId=req.headers.tenantId;
  var variable = JSON.stringify(req.headers)
  var variable1 = JSON.parse(variable)
  console.log(variable1.tenantid);

  var tenantId = variable1.tenantid;
  // res.json({ status: true, message: "agent conference success" });
  // return;
  var conference = await db.Session.findOne({
    chat_session_id: sessionId,
  })
  if (conference.conference==true && conference.is_conferenceleft == false) {
    logger.error("ConferenceAgent:Conference can not possible for more than 1 user", sessionId);
    await db.User.findByIdAndUpdate(
      {
       _id:mongoose.Types.ObjectId(old_agent_id) 
      },
      {
        $set: {
          lastactive_time:new Date()
        },
      }
    );
    return res.json({
      status: false,
      message: "Conference can not possible for more than 1 user ",
    });
  } else {
    if (lastredisclient != undefined) {
      // var value = await redisClient.get(JSON.stringify(old_agent_id));
      // let cachedrequestlist = JSON.parse(value);
      // let conferencearray = [];
      // if (cachedrequestlist) {
      //   let filteredArray = cachedrequestlist.filter((item) => {
      //     if (item.chat_session_id == sessionId) {
      //       item["conference"] = true;
      //       item["conference_agent_time"] = new Date();
      //       item["conference_agent"] = agent_id;

      //       conferencearray = item;
      //     }
      //   });
      //   redisClient.set(
      //     JSON.stringify(old_agent_id),
      //     JSON.stringify(filteredArray)
      //   );
      // }
      var agents = JSON.stringify(old_agent_id);
      var agentnew = JSON.stringify(agent_id);
      var ss = await redisClient.hVals(JSON.stringify(agents))
     // var havalue = await redisClient.HINCRBY(JSON.stringify("queuecount"), JSON.stringify(agent_id), 1)
     
      await redisClient.HINCRBY(JSON.stringify("activechatcount"), JSON.stringify(agent_id), 1)
      //console.log(JSON.stringify(ss))
      console.log(typeof (ss))
      var valu1 = JSON.stringify(ss)
      console.log(typeof (valu1))
      var newval = JSON.parse(valu1);
      var upadeddata = newval.map(async (value) => {
        var parseddata = JSON.parse(value);
        if (parseddata["chat_session_id"] == sessionId) {
          parseddata["conference"] = true;
          parseddata["conference_agent_time"] = new Date();
          parseddata["conference_agent"] = agent_id;
          console.log("parsedId", parseddata["id"])
          var valu3 = await redisClient.hSet(JSON.stringify(agents), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
            //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
          )

          var valu4 = await redisClient.hSet(JSON.stringify(agentnew), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
            //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
          )
          console.log("value", valu3)
          console.log("value4", valu4)
        }
        return parseddata;
      })
      // console.log("conference array", conferencearray);
      // var value1 = await redisClient.get(JSON.stringify(agent_id));
      // let cachedrequestlist1 = JSON.parse(value1);
      // let cachedrequestlistnew = [];
      // if (cachedrequestlist1) {
      //   cachedrequestlistnew = [...cachedrequestlist1, conferencearray];
      // } else {
      //   cachedrequestlistnew = [conferencearray];
      // }
      // redisClient.set(
      //   JSON.stringify(agent_id),
      //   JSON.stringify(cachedrequestlistnew)
      // );

      //     var s= await redisClient.hVals(JSON.stringify(agentnew))
      //     //console.log(JSON.stringify(ss))
      //  console.log(typeof(s))
      //   var valu2=JSON.stringify(s)
      //   console.log(typeof(valu2))
      //   var newvalue=JSON.parse(valu2);
      //   var upadeddatas=newvalue.map((value)=>{
      //     var parseddat=JSON.parse(value);
      //    if( parseddat["chat_session_id"] == sessionId){
      //     parseddat["conference"] = true;
      //            parseddat["conference_agent_time"] = new Date();
      //           parseddat["conference_agent"] = agent_id;
      //           var value3=  redisClient.hSet(JSON.stringify(agentnew), JSON.stringify(parseddat["id"]), JSON.stringify(parseddat)
      //         //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
      //         )
      //         console.log("value",value3)
      //    }
      //    return parseddat;
      //   })
      // console.log("updates",upadeddatas)
    }
    var conference_data={
      interaction_id: conference.chat_session_id,
      agent_id: old_agent_id,
      start_time:conference.updatedAt,
      end_time: new Date(),
      duration: "10s",
      conference_number:"",
      participant_type: "internal",
      participant_id:agent_id,
      conference_notes:"Conference",
      isfinished: true
  }
  const config1 = {
    headers: { TenantID: tenantId },
    };
    await axios.post(
      config.interactionApi+"conferenceDetails/createConferenceDetails",
      conference_data,
      config1
      );
      var updatedata=  {
        "Intraction_id":conference.chat_session_id,
        "confernce_flag":true,
        "interaction_disconnected":false,
        

    }
      await axios.post(
        config_file.interactionApi+"liveReport/updateLiveReport",
        updatedata,
        config1
        );
    var updateAgent;
if(conference.conference==true){
  console.log("val",)
  console.log("inside",conference.conference_agent,conference.conference_left_id)
  if(conference.agent.equals(conference.conference_left_id)){
    console.log("insideconference")
     updateAgent = await db.Session.findOneAndUpdate(
      {
        chat_session_id: sessionId,
      },
      {
        $set: {
          conference: true,
          conference_agent_time: new Date(),
          agent: agent_id,
          conference_left_id:null,
          is_conferenceleft:false
        },
      }
    ).populate("unique_id", "username email phonenumber");
    await db.User.findByIdAndUpdate(
      {
       _id:mongoose.Types.ObjectId(old_agent_id) 
      },
      {
        $set: {
          lastactive_time:new Date()
        },
      }
    );
    await db.User.findByIdAndUpdate(
      {
       _id:mongoose.Types.ObjectId(agent_id) 
      },
      {
        $set: {
          lastactive_time:new Date()
        },
      }
    );
    var conferenceAgent = await db.chatHistory({
      agent_id: old_agent_id,
      destinationagent_id: agent_id,
      user_id: updateAgent.user_id,
      time: new Date(),
      session_id: sessionId,
      type: "Conference",
    })
    .save();
  var AgentActivity = await db.agentActivity({
      agent_id: agent_id,
      user_id: updateAgent.user_id,
      activity_name: "USER_CONFERENCE_CHAT",
      value: sessionId,
      created_at: new Date(),
    })
    .save();
  socket.current = io(config.socketUrl + tenantId);
  socket.current.emit("check-agent", "yes conf");

  if (updateAgent) {
    let findAgent = await db.Session.find({
      chat_session_id: sessionId,
    }).populate("unique_id", "username email phonenumber");
    // console.log("response from   , findAgent);
    socket.current.emit("send-new-req-conf", findAgent);
    logger.info(sessionId + ":ConferenceAgent:Chat is conference to another agent", updateAgent);
    res.json({ status: true, message: "agent conferenced" });
  } else {
    logger.error("ConferenceAgent:Conference not happend ", sessionId);
    res.json({ status: false, message: "no data found" });
  }
  }else if(conference.conference_agent.equals(conference.conference_left_id)){
    console.log("insideconference")
     updateAgent = await db.Session.findOneAndUpdate(
      {
        chat_session_id: sessionId,
      },
      {
        $set: {
          conference: true,
          conference_agent_time: new Date(),
          conference_agent: agent_id,
          conference_left_id:null,
          is_conferenceleft:false
        },
      }
    ).populate("unique_id", "username email phonenumber");
    await db.User.findByIdAndUpdate(
      {
       _id:mongoose.Types.ObjectId(old_agent_id) 
      },
      {
        $set: {
          lastactive_time:new Date()
        },
      }
    );
    await db.User.findByIdAndUpdate(
      {
       _id:mongoose.Types.ObjectId(agent_id) 
      },
      {
        $set: {
          lastactive_time:new Date()
        },
      }
    );
    var conferenceAgent = await db.chatHistory({
      agent_id: old_agent_id,
      destinationagent_id: agent_id,
      user_id: updateAgent.user_id,
      time: new Date(),
      session_id: sessionId,
      type: "Conference",
    })
    .save();
  var AgentActivity = await db.agentActivity({
      agent_id: agent_id,
      user_id: updateAgent.user_id,
      activity_name: "USER_CONFERENCE_CHAT",
      value: sessionId,
      created_at: new Date(),
    })
    .save();
  socket.current = io(config.socketUrl + tenantId);
  socket.current.emit("check-agent", "yes conf");

  if (updateAgent) {
    let findAgent = await db.Session.find({
      chat_session_id: sessionId,
    }).populate("unique_id", "username email phonenumber");
    // console.log("response from   , findAgent);
    socket.current.emit("send-new-req-conf", findAgent);
    logger.info(sessionId + ":ConferenceAgent:Chat is conference to another agent", updateAgent);
    res.json({ status: true, message: "agent conferenced" });
  } else {
    logger.error("ConferenceAgent:Conference not happend ", sessionId);
    res.json({ status: false, message: "no data found" });
  }
  }
}
else{
   updateAgent = await db.Session.findOneAndUpdate(
    {
      chat_session_id: sessionId,
    },
    {
      $set: {
        conference: true,
        conference_agent_time: new Date(),
        conference_agent: agent_id,
      },
    }
  ).populate("unique_id", "username email phonenumber");
  
  var conferenceAgent = await db.chatHistory({
    agent_id: old_agent_id,
    destinationagent_id: agent_id,
    user_id: updateAgent.user_id,
    time: new Date(),
    session_id: sessionId,
    type: "Conference",
  })
  .save();
var AgentActivity = await db.agentActivity({
    agent_id: agent_id,
    user_id: updateAgent.user_id,
    activity_name: "USER_CONFERENCE_CHAT",
    value: sessionId,
    created_at: new Date(),
  })
  .save();
  await db.User.findByIdAndUpdate(
    {
     _id:mongoose.Types.ObjectId(old_agent_id) 
    },
    {
      $set: {
        lastactive_time:new Date()
      },
    }
  );
  await db.User.findByIdAndUpdate(
    {
     _id:mongoose.Types.ObjectId(agent_id) 
    },
    {
      $set: {
        lastactive_time:new Date()
      },
    }
  );
socket.current = io(config.socketUrl + tenantId);
socket.current.emit("check-agent", "yes conf");

if (updateAgent) {
  let findAgent = await db.Session.find({
    chat_session_id: sessionId,
  }).populate("unique_id", "username email phonenumber");
  // console.log("response from   , findAgent);
  socket.current.emit("send-new-req-conf", findAgent);
  logger.info(sessionId + ":ConferenceAgent:Chat is conference to another agent", updateAgent);
  res.json({ status: true, message: "agent conferenced" });
} else {
  logger.error("ConferenceAgent:Conference not happend ", sessionId);
  res.json({ status: false, message: "no data found" });
}
}
   
  }
}

//  Client session
async function addClient(req, res, next) {
  try {
    const user = {};
    let body = req.body;
    var {
      name,
      email_id,
      phone,
      channel,
      latitude,
      longitute,
      whatsapp_msg_id,
    } = req.body;
    var clientIp = requestIp.getClientIp(req);
    var source = req.useragent;
    // var tenantId=req.headers.tenantId;
    var variable = JSON.stringify(req.headers)
    var variable1 = JSON.parse(variable)
    console.log(variable1.tenantid);

    var tenantId = variable1.tenantid;
    // console.log("soruce" + JSON.stringify(source));
    console.log("client ip" + clientIp);
    let client_ip = clientIp;
    let get_agent_userid;
    let user_agent = source;

    get_agent_userid = await db.User.find({ user_id: req.body.availableAgent });

    if (get_agent_userid[0]) {
      if (body) {
        const jwtToken = generateJwtToken(user);
        user.access_token = jwtToken;
        var id = "session" + Math.random().toString(16).slice(2);
        var chat_session_id = id;
        let isInsert = false;

        let check_user_old = await db.Client.findOne({
          // email: req.body.email_id,
          $or: [
            {
              email: req.body.email_id,
            },
            {
              phonenumber: phone,
            },
          ],
        });

        if (check_user_old) {
          // let update = await db.Client.findByIdAndUpdate(
          //   { _id: check_user_old._id },
          //   { $set: { status: "newjoin" } }
          // );
          isInsert = false;
        } else {
          isInsert = true;
        }

        let Client;

        if (isInsert) {
          let agentVAl;
          if (req.body.channel == "webchat" && req.body.availableAgent) {
            agentVAl = get_agent_userid[0]._id;
          } else {
            console.log("agent for whatsapp", get_agent_userid[0]._id);
            agentVAl = get_agent_userid[0]._id;
          }
          Client = await new db.Client({
            username: name,
            email: email_id,
            phonenumber: phone,
          }).save();
        }

        let session;

        let check_user_active = await db.Session.findOne({
          email: req.body.email_id,
          status: "Accept",
          chat_type: "external",
        });

        if (check_user_active) {
          logger.error("AddClient:Chat session already exit", req.body.email_id);
          res.json({
            status: false,
            msg: "chat session already exit",
            data: "",
          });
          return;
        }

        if (check_user_old) {
          console.log("old user");
          let agentVAl = get_agent_userid[0]._id,
            userId = req.body.availableAgent;
          console.log("okkkkk---------");
          session = await new db.Session({
            phonenumber: phone,
            username: check_user_old.username,
            email: check_user_old.email,
            channel:
              req.body.loginFrom == "web" ? "webchat" : check_user_old.channel,
            chat_session_id: chat_session_id,
            unique_id: check_user_old ? check_user_old._id : "",
            user_id: userId,
            agent: agentVAl,
            arrival_at: new Date(),
            chat_started_at: "",
            chat_ended_at: "",
            skillset: check_user_old.skillset,
            language: check_user_old.language,
            complaint: check_user_old.complaint,
            available_agent: check_user_old.available_agent,
            lattitude: latitude,
            longitude: longitute,
            client_ip,
            whatsapp_msg_id,
            user_agent: user_agent,
          }).save();
        } else {
          console.log("new user");
          var agent_id = req.body.availableAgent;
          let agentVAl, userId;
          if (req.body.channel == "webchat" && req.body.availableAgent) {
            agentVAl = get_agent_userid[0]._id;
            userId = req.body.availableAgent;
          } else {
            agentVAl = get_agent_userid[0]._id;
            userId = req.body.availableAgent;
            //  console.log("agent value for wp", agentVAl);
          }
          session = await new db.Session({
            phonenumber: phone,
            username: name,
            email: req.body.email_id,
            chat_session_id: chat_session_id,
            unique_id: Client._id,
            channel: channel,
            user_id: userId,
            arrival_at: new Date(),
            chat_started_at: "",
            chat_ended_at: "",
            agent: agentVAl,
            skillset: req.body.skillset,
            language: req.body.language,
            complaint: req.body.complaint,
            available_agent: get_agent_userid[0]._id,
            lattitude: latitude,
            longitude: longitute,
            client_ip,
            whatsapp_msg_id,
            user_agent: user_agent,
          }).save();
        }
        var agent_id_incoming = get_agent_userid[0]._id;

        socket.current = io(config.socketUrl + tenantId);

        let findAgent = await db.Session.find({
          _id: session.id,
          status: "newjoin",
        }).populate("unique_id", "username email phonenumber");
        agent_id_incoming = JSON.stringify(agent_id_incoming);

        // console.log(session);
        if (redisClient != undefined) {
          var value = await redisClient.get(agent_id_incoming);

          let cachedrequestlist = JSON.parse(value);

          if (cachedrequestlist) {
            cachedrequestlist = [...cachedrequestlist, findAgent[0]];
          } else {
            cachedrequestlist = [findAgent[0]];
          }

          redisClient.set(agent_id_incoming, JSON.stringify(cachedrequestlist));
        }
        socket.current.emit("send-new-req", findAgent);

        // save first message from whatsapp
        if (channel == "from_whatsapp") {
          await axios.post(config_file.customurl+`/v1/message/addMessage`, {
            from: session._id,
            to: get_agent_userid[0]._id,
            message: req.body.message,
            senderName: name,
            receiverName: get_agent_userid[0].username,
            messageFrom: "fromClient",
          });
        }

        if (channel != "webchat") {
          async function getData() {
            const data = await axios.post(
              config_file.customurl+`/v1/users/getId/${email_id}`
            );
            if (data && data.length) {
              socket = io(config.socketUrl + tenantId);
              socket.emit("add-user", data.data.user.id);
            }
          }
          if (Client) {
            getData();
          }
        }
        logger.info(req.body.email_id + ":AddClient:chat session created successfully", Client ? Client : check_user_old);
        res.json({
          status: true,
          message: "chat session created successfully",
          data: Client ? Client : check_user_old,
        });
      }
    } else {
      logger.info(email_id + ":AddClient:Agent Not loggedIn", "");
      res.json({
        status: true,
        message: "Agent Not loggedIn",
      });
    }
  } catch (error) {
    next(error);
  }
}

async function getId(req, res, next) {
  try {
    const client = await db.Session.findOne({
      email: req.params.email,
      chat_type: "external",
      // status: "Accept",
    }).sort({ $natural: -1 });
    // console.log("check new session", client);
    logger.info(req.params.email + ":GetSession:Finding new Session", client);
    res.json({ user: client,client_id:client.id ,chat_session_id:client.chat_session_id});
  } catch (error) { }
}

async function getInfoBasedOnNum(req, res) {
  try {
    let number = req.body.number;
    let getDetails = await db.Session.find({
      phonenumber: number,
      status: "Accept",
    }).populate("unique_id", "username email phonenumber");
    if (getDetails) {
      logger.info(number + ":GetInfoByNumber:Get information based on phonenumber", getDetails);
      res.json({ success: true, data: getDetails });
    } else {
      logger.error("GetInfoByNumber:No data found based number", number);
      res.json({ success: false, data: "no data found" });
    }
  } catch (e) {
    console.log("error");
  }
}

async function updateAgentStatus(req, res) {
  try {
    let user_id = req.body.userId;
    let status = req.body.status;
    let update_agent_status;
    //var tenantId=req.headers.tenantId;
    var variable = JSON.stringify(req.headers)
    var variable1 = JSON.parse(variable)
    console.log(variable1.tenantid);

    var tenantId = variable1.tenantid;
    if (status == "Ready") {
      update_agent_status = "Available";
    } else {
      update_agent_status = "Busy";
    }

    let updateStatus = await db.User.findOneAndUpdate(
      {
        user_id: user_id,
      },
      {
        $set: {
          agent_status: status == "Available" ? status : update_agent_status,
          agent_status_real: status == "Ready" ? update_agent_status : status,
          lastactive_time:new Date()
        },
      }
    );

    // let updateStatusSession = await db.Session.findOneAndUpdate(
    //   {
    //     user_id: user_id,
    //   },
    //   {
    //     $set: {
    //       agent_status: update_agent_status,
    //     },
    //   }
    // );
    socket.current = io(config.socketUrl + tenantId);
    socket.current.emit("update-agent-status", {
      user_id: user_id,
      status: update_agent_status,
    });
    if (lastredisclient != undefined) {
      if (config_file.redisenv == "checking") {
        var ss = await redisClient.hVals(JSON.stringify("agentsDetails"))
        if (ss) {
          //console.log(JSON.stringify(ss))
          console.log(typeof (ss))
          var valu1 = JSON.stringify(ss)
          console.log(typeof (valu1))
          var newval = JSON.parse(valu1);
          var upadeddata = newval.map(async (value) => {
            var parseddata = JSON.parse(value);
            if (parseddata["userId"] == user_id) {

              var valu3 = await redisClient.hSet(JSON.stringify("agentsDetails"), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
                //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
              )

            }
            return parseddata;
          })
        }
      }
      else {
        let cacheResults = await redisClient.get("agentsDetails");

        let cache_resp = JSON.parse(cacheResults);
        var agentStatus = [];
        cache_resp.forEach(async (item) => {
          if (item["userId"] == user_id) {
            item["status"] = status == "Ready" ? update_agent_status : status;
            agentStatus = item;
          }
        });
        await redisClient.set("agentsDetails", JSON.stringify(cache_resp));
      }
      var agentActivity = await db
        .agentActivity({
          agent_id: updateStatus.id,
          user_id: user_id,
          activity_name: "USER_CHANGED_STATUS",
          value: req.body.status,
          created_at: new Date(),
        })
        .save();
      var todays_date = new Date().toISOString();
      todays_date = todays_date.replace(/\T.*/, "");
      todays_date = new Date(todays_date);
      console.log(todays_date);
      var logindata = await db.activeTime.findOne({
        agent_id: updateStatus.id,
        created_at: todays_date,
      });
      console.log(logindata.breakvalue);
      if (req.body.status == "Ready") {
        if (logindata.breakvalue === true) {
          var login_time = logindata.notReadyDate;
          console.log(login_time);
          var current_time = new Date();
          var logdates = moment(login_time, "YYYY-MM-DD HH:mm:ss");
          var dates = moment(current_time, "YYYY-MM-DD HH:mm:ss");
          var diff = dates.diff(logdates, "seconds");
          var duration = logindata.duration_break + diff;
          var countValue = await db.activeTime.findByIdAndUpdate(
            { _id: logindata._id },
            {
              duration_break: duration,
              breakvalue: false,
            },
            { new: true }
          );
        }
      } else {
        if (!logindata.breakvalue) {
          var id = logindata._id;
          var countValue = await db.activeTime.findByIdAndUpdate(
            { _id: id },
            {
              notReadyDate: new Date(),
              breakvalue: true,
            },
            { new: true }
          );
        }
      }

      socket.current = io(config.socketUrl + tenantId);
      socket.current.emit("agent-activity-input", agentStatus);

      //await redisClient.publish("agent-activity", JSON.stringify(agentStatus));
      if (config_file.routingenv == "Redis") {
        await redisClient.publish(
          "agent-activity",
          JSON.stringify(agentStatus)
        );
      } else {
        console.log(agentStatus)
        if (agentStatus.length <= 0) {
          var agentvalue = "Text Message"
        } else {
          var agentvalue = JSON.stringify(agentStatus[0])
        }
        var queuedetails = await axios.post(config_file.pushmessageapi, {
          "queueName": "Test_ChatAPP_AgentActivity",
          "routingKey": "Test_ChatAPP_AgentActivity",
          "message": agentvalue,
          "priority": 1
        }, {
          headers: { TenantID: tenantId },
        })
      }


    }
    var agentdata={
      "agent_id":updateStatus.id,
      "status":update_agent_status == "Available" ? "Available" : "Not Available" ,
      "language":updateStatus.language[0].languageDesc,
      "status_reason":status == "Ready" ? update_agent_status : status,
      "skillset":updateStatus.skillSet[0].skillName,
      "previous_status":updateStatus.agent_status,
      "new_status":status,
      "duration_in_previous_status":"10s",
      "reason_for_change":"",
      "notes":"",
      "Cur_Ready_Status":update_agent_status == "Available" ? 1 : 0
  }
  console.log("update",agentdata)
  const config1 = {
    headers: { TenantID: 'a3dc14bd-fe70-4120-8572-461b0dc866b5' },
    };
   await axios.post(config_file.interactionApi+
      "agentStatusChange/createAgentStatusChange",
    agentdata,
      config1
      );
     // console.log(responsevalue)
    logger.info(user_id + ":UpdateAgentStatus:Agent status is Updated", user_id);
    res.json({ status: true, user_id: user_id, message: "status updated" });
  } catch (error) { 
    res.json({ status: false, message: "status not  updated",error:error });
  }
}

async function rejectchat(req, res, next) {
  var agent = req.body.agentID;
  var userID = req.body.userID;
  //var tenantId=req.headers.tenantId;
  var variable = JSON.stringify(req.headers)
  var variable1 = JSON.parse(variable)
  console.log(variable1.tenantid);

  var tenantId = variable1.tenantid;
  //console.log(JSON.stringify(agent));
  // const cacheResults = await redisClient.get(JSON.stringify(agent));

  // if (cacheResults != '""' && cacheResults && cacheResults != "[]") {
  //   isCached = true;
  //   results = JSON.parse(cacheResults);
  //   let users = results.filter((id) => {
  //     return id["chat_session_id"] != req.body.chat_session_id;
  //   });
  //  // console.log(users);
  //   redisClient.set(JSON.stringify(agent), JSON.stringify(users));
  // }
  var agents = JSON.stringify(agent);
  var ss = await redisClient.hVals(JSON.stringify(agents))
  //console.log(JSON.stringify(ss))
  if (ss) {
    console.log(typeof (ss))
    var valu1 = JSON.stringify(ss)
    console.log(typeof (valu1))
    var newval = JSON.parse(valu1);
    var upadeddata = newval.map(async (value) => {
      var parseddata = JSON.parse(value);
      if (parseddata["chat_session_id"] == req.body.chat_session_id) {
        // parseddata["status"] = "Chat End";
        // parseddata["chat_type"] = "external";
        //       parseddata["chat_started_at"] = new Date();
        //        parseddata["is_queued"] = false;
        //       var valu3=  redisClient.hSet(JSON.stringify(agents), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
        //     //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
        //     )
        //     console.log("value",valu3)
        var respons = await redisClient.hDel(JSON.stringify(agents), JSON.stringify(parseddata["id"]))
        console.log(respons)
      }
      return parseddata;
    })
  }
  var updateresult = await db.Session.findOneAndUpdate(
    { chat_session_id: req.body.chat_session_id, status: "newjoin" },
    {
      $set: {
        user_id: null,
        available_agent: null,
        agent: null,
      },
    }
  ).populate("unique_id", "username email phonenumber");
  var agentActivity = await db
    .agentActivity({
      agent_id: agent,
      user_id: userID,
      activity_name: "USER_REJECTED_CHAT",
      value: req.body.chat_session_id,
      created_at: new Date(),
    })
    .save();
  var messageobj = {
    chat_session_id: req.body.chat_session_id,
    agentID: req.body.userID,
  };
  let cachedmessage = [];
  if (lastredisclient != undefined) {
    var value = await redisClient.get("reject_chat" + req.body.chat_session_id);
    cachedmessage = JSON.parse(value);

    if (cachedmessage) {
      cachedmessage = [...cachedmessage, messageobj];
    } else {
      cachedmessage = [messageobj];
    }
    redisClient.set(
      "reject_chat" + req.body.chat_session_id,
      JSON.stringify(cachedmessage)
    );
  }
  let data = {
    phoneNumber: req.body.phone,
    serviceRequestType: "",
    customerLanguage: req.body.language,
    customerLanguageCode: "",
    LastHandledAgentID: "",
    routingRuleId: "",
    skillSet: req.body.skillset,
    chat_session_id: req.body.chat_session_id,
  };

  await axios.post(config_file.routingbaseurl + "/routingengine/engine/bestRoutev1", data, {
    headers: { TENANTID: tenantId },
  }).then(async (resp) => {
    if (resp.data.status == 1001) {
      // console.log("inside response",resp.data)
      if (resp.data.value != null || req.body.channel == "from_whatsapp") {
        if (
          req.body.channel == "from_whatsapp"
        ) {
          req.body.availableAgent = "03bbcd2f-448b-4c55-8a95-99502c166955";
        } else {
          req.body.availableAgent = resp.data.value.userId;
        }

        let findAgent = await db.User.find({
          user_id: req.body.availableAgent,
        });

        if (findAgent != null) {
          var updateresult = await db.Session.findOneAndUpdate(
            { chat_session_id: req.body.chat_session_id },
            {
              $set: {
                user_id: req.body.availableAgent,
                available_agent: findAgent[0]._id,
                agent: findAgent[0]._id,
              },
            }
          ).populate("unique_id", "username email phonenumber");

          if (updateresult) {
            let incomingreq = await db.Session.find({
              chat_session_id: req.body.chat_session_id,
            }).populate("unique_id", "username email phonenumber");
            let agent_id_incoming = JSON.stringify(findAgent[0]._id);

            if (redisClient != undefined) {
              var v = await redisClient.hSet(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0])
                //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
              )
              console.log("value", v)

              // var value = await redisClient.get(agent_id_incoming);

              // let cachedrequestlist = JSON.parse(value);
              //  console.log("cached",cachedrequestlist)
              // if (cachedrequestlist) {
              //   let filter = cachedrequestlist.filter((id) => {
              //     return id["email"] != incomingreq[0].email;
              //   });
              //   filter.push(incomingreq[0])
              //   cachedrequestlist =filter;
              // } else {
              //   cachedrequestlist = [incomingreq[0]];
              // }

              // redisClient.set(
              //   agent_id_incoming,
              //   JSON.stringify(cachedrequestlist)
              // );
            }
            var agentActivity = await db
              .agentActivity({
                agent_id: findAgent[0]._id,
                user_id: req.body.availableAgent,
                activity_name: "USER_INCOMING_REQUEST",
                value: req.body.chat_session_id,
                created_at: new Date(),
              })
              .save();
            socket.current = io(config.socketUrl + tenantId);

            socket.current.emit("send-new-req", incomingreq[0]);

            if (req.body.channel == "from_whatsapp") {
              await axios.post(config_file.customurl+`/v1/message/addMessage`, {
                from: req.body.chat_session_id,
                to: findAgent[0]._id,
                message: req.body.message,
                senderName: req.body.name,
                receiverName: findAgent[0].username,
                messageFrom: "fromClient",
                msg_sent_type: req.body.msg_sent_type,
              });

              const data = await axios.post(
                config_file.customurl+`/v1/users/getId/${email_id}`
              );
              if (data && data.length) {
                socket = io(config.socketUrl + tenantId);
                socket.emit("add-user", data.data.user.id);
              }
            }
            logger.info(req.body.chat_session_id + ":RejectChat:Chat is assign to new agent", incomingreq);
            var transfer_data={

              interaction_id:req.body.chat_session_id,
            
              agent_id:agent,
            
              rejection_reason:"",
            
              rejection_time:new Date(),
            
              notes:"",
            
              routed:"Agent",
            
              Routed_Agent:findAgent[0]._id,
            
              chat_arrival_at: incomingreq[0].arrival_at?incomingreq[0].arrival_at:new Date()
            
            }
           const config1 = {
            headers: { TenantID: tenantId },
            };
            await axios.post(
              config.interactionApi+"intractionRejection/createIntractionRejection",
              transfer_data,
              config1
              );
            res.json({
              status: true,
              message: "chat session created successfully",
              data: incomingreq,
              chat_session_id: req.body.chat_session_id,
              UserId: req.body.availableAgent,
              chat_inititaed_at: incomingreq[0].arrival_at
            });

            // console.log(req.body);
            return;
          } else {
            logger.error("RejectChat:Agent not logged in", req.body.chat_session_id);
            res.json({
              status: false,
              msg: "Agent Not logged In",
              data: "",
            });
            return;
          }
        } else {
          // console.log("availble",findAgent)
          var queuedetails = await axios.post(config_file.loginbaseurl + "/usermodule/clientMaster/chatConfig/list", { enabled: true }, {
            headers: { TenantID: tenantId },
          })
          console.log(queuedetails, "details")
          var queueMessage = queuedetails.data.data ? queuedetails.data.data.queueMessage : "Please wait, your request is in queue";
          console.log("hello")
          let incomingreq = await db.Session.find({
            chat_session_id: req.body.chat_session_id,
          }).populate("unique_id", "username email phonenumber");
          var client = await db.Client.findOne({ _id: mongoose.Types.ObjectId(incomingreq[0].unique_id) })
          await db.Session.findOneAndUpdate({ chat_session_id: req.body.chat_session_id }, { $set: { is_queued: true } });
          await axios.post(
            config_file.customurl+`/v1/message/addMessage`,
            {
              from: incomingreq[0]._id,
              to: incomingreq[0]._id,
              message: queueMessage,
              senderName: "Queue Message",
              receiverName: client.name,
              msg_sent_type: "TEXT",
              messageFrom: "fromAgent",
              userType: "external",
              file_name: "",
              chatdetails: incomingreq[0]
            }
          );

          const data = await axios.post(
            config_file.customurl+`/v1/users/getId/${client.email}`
          );
          if (data && data.length) {
            socket = io(config.socketUrl + tenantId);
            socket.emit("add-user", data.data.user.id);
          }
          logger.info(req.body.chat_session_id + ":RejectChat:Chat is in queue ", incomingreq);
          var transfer_data={

            interaction_id:req.body.chat_session_id,
          
            agent_id:agent,
          
            rejection_reason:"",
          
            rejection_time:new Date(),
          
            notes:"",
          
            routed:"Queue",
          
            Routed_Agent:"",
          
            chat_arrival_at: incomingreq[0].arrival_at?incomingreq[0].arrival_at:new Date()
          
          }
         const config1 = {
          headers: { TenantID: tenantId },
          };
          await axios.post(
            config.interactionApi+"intractionRejection/createIntractionRejection",
            transfer_data,
            config1
            );
          res.json({
            status: true,
            message: "You are in queue please wait",
            data: incomingreq,
            chat_session_id: req.body.chat_session_id,
            UserId: null,
            chat_inititaed_at: incomingreq[0].arrival_at
          });
          return;
          // res.json({
          //   status: false,
          //   msg: "Unable to create session please try after sometime",
          //   data: "",
          // });
          // return;
        }
      } else {
        var queuedetails = await axios.post(config_file.loginbaseurl + "/usermodule/clientMaster/chatConfig/list", { enabled: true }, {
          headers: { TenantID: tenantId },
        })
        var queueMessage = queuedetails.data.data ? queuedetails.data.data.queueMessage : "Please wait, your request is in queue";
        let incomingreq = await db.Session.find({
          chat_session_id: req.body.chat_session_id,
        }).populate("unique_id", "username email phonenumber");
        var client = await db.Client.findOne({ _id: mongoose.Types.ObjectId(incomingreq[0].unique_id) })
        await db.Session.findOneAndUpdate({ chat_session_id: req.body.chat_session_id }, { $set: { is_queued: true } });
        await axios.post(
          config_file.customurl+`/v1/message/addMessage`,
          {
            from: incomingreq[0]._id,
            to: incomingreq[0]._id,
            message: queueMessage,
            senderName: "Queue Message",
            receiverName: client.name,
            msg_sent_type: "TEXT",
            messageFrom: "fromAgent",
            userType: "external",
            file_name: "",
            chatdetails: incomingreq[0]
          }
        );

        const data = await axios.post(
          config_file.customurl+`/v1/users/getId/${client.email}`
        );
        if (data && data.length) {
          socket = io(config.socketUrl + tenantId);
          socket.emit("add-user", data.data.user.id);
        } logger.info(req.body.chat_session_id + ":RejectChat:Chat is in queue", incomingreq[0]);
        var transfer_data={

          interaction_id:req.body.chat_session_id,
        
          agent_id:agent,
        
          rejection_reason:"",
        
          rejection_time:new Date(),
        
          notes:"",
        
          routed:"Queue",
        
          Routed_Agent:"",
        
          chat_arrival_at: incomingreq[0].arrival_at?incomingreq[0].arrival_at:new Date()
        
        }
       const config1 = {
        headers: { TenantID: tenantId },
        };
        await axios.post(
          config.interactionApi+"intractionRejection/createIntractionRejection",
          transfer_data,
          config1
          );
        res.json({
          status: true,
          message: "You are in queue please wait",
          data: incomingreq[0],
          chat_session_id: req.body.chat_session_id,
          UserId: null,
          chat_inititaed_at: incomingreq[0].arrival_at
        });
        return;
      }
    } else {
      console.log("else issue")
      var queuedetails = await axios.post(config_file.loginbaseurl + "/usermodule/clientMaster/chatConfig/list", { enabled: true }, {
        headers: { TenantID: tenantId },
      })
      var queueMessage = queuedetails.data.data ? queuedetails.data.data.queueMessage : "Please wait, your request is in queue";
      console.log("hi")
      let incomingreq = await db.Session.find({
        chat_session_id: req.body.chat_session_id,
      }).populate("unique_id", "username email phonenumber");
      var client = await db.Client.findOne({ _id: mongoose.Types.ObjectId(incomingreq[0].unique_id) })
      await db.Session.findOneAndUpdate({ chat_session_id: req.body.chat_session_id }, { $set: { is_queued: true } });
      await axios.post(
        config_file.customurl+`/v1/message/addMessage`,
        {
          from: incomingreq[0]._id,
          to: incomingreq[0]._id,
          message: queueMessage,
          senderName: "Queue Message",
          receiverName: client.name,
          msg_sent_type: "TEXT",
          messageFrom: "fromAgent",
          userType: "external",
          file_name: "",
          chatdetails: incomingreq[0]
        }
      );

      const data = await axios.post(
        config_file.customurl+`/v1/users/getId/${client.email}`
      );
      if (data && data.length) {
        socket = io(config.socketUrl + tenantId);
        socket.emit("add-user", data.data.user.id);
      } logger.info(req.body.chat_session_id + ":RejectChat:Chat is in queue", incomingreq[0]);
      var transfer_data={

        interaction_id:req.body.chat_session_id,
      
        agent_id:agent,
      
        rejection_reason:"",
      
        rejection_time:new Date(),
      
        notes:"",
      
        routed:"Queue",
      
        Routed_Agent:"",
      
        chat_arrival_at: incomingreq[0].arrival_at?incomingreq[0].arrival_at:new Date()
      
      }
     const config1 = {
      headers: { TenantID: tenantId },
      };
      await axios.post(
        config.interactionApi+"intractionRejection/createIntractionRejection",
        transfer_data,
        config1
        );
      res.json({
        status: true,
        message: "You are in queue please wait",
        data: incomingreq[0],
        chat_session_id: req.body.chat_session_id,
        UserId: null,
        chat_inititaed_at: incomingreq[0].arrival_at
      });
      return;
      // console.log("Unable to create session please try after sometime");
      // res.json({
      //   status: false,
      //   msg: "Unable to create session please try after sometime",
      //   data: "",
      // });
      // return;
    }
  });

}
async function rejectchatnew(req, res, next) {
  var agent = req.body.agent;

  console.log(JSON.stringify(agent));
  const cacheResults = await redisClient.get(JSON.stringify(agent));

  if (cacheResults != '""' && cacheResults && cacheResults != "[]") {
    isCached = true;
    var results = JSON.parse(cacheResults);
    let users = results.filter((id) => {
      return id["chat_session_id"] != req.body.chat_session_id;
    });
    console.log(users);
    redisClient.set(JSON.stringify(agent), JSON.stringify(users));
  }
  var updateresult = await db.Session.findOneAndUpdate(
    { chat_session_id: req.body.chat_session_id, status: "newjoin" },
    {
      $set: {
        user_id: null,
        available_agent: null,
        agent: null,
      },
    }
  ).populate("unique_id", "username email phonenumber");
  console.log("hi");
  var hashagent = await redisClient.hVals(
    JSON.stringify(req.body.chat_session_id)
  );
  console.log(hashagent);

  if (hashagent.length == 0) {
    var arrData = [
      { agent: [req.body.agent], chat_session_id: req.body.chat_session_id },
    ];
    var newdata = redisClient.hSet(
      JSON.stringify(req.body.chat_session_id),
      "key",
      JSON.stringify(arrData)
    );
    console.log("newdata" + JSON.stringify(newdata));
    var data = await redisClient.hVals(
      JSON.stringify(req.body.chat_session_id)
    );
    console.log(JSON.parse(data));
  } else {
    console.log("hi");
    var results = JSON.parse(hashagent);
    console.log(results);
    var arrData = [{ agent: req.body.chat_session_id }];
    //results.find(someobject => someobject.chat_session_id == req.body.chat_session_id).chat_session_id = req.body.chat_session_id;
    let users = await results.filter((id) => {
      console.log(id);
      return id.chat_session_id == req.body.chat_session_id;
    });
    users[0].agent.push(req.body.agent);
    var data2 = JSON.stringify(users);
    console.log(data2 + "data");
    var updatedata = await redisClient.hSet(
      JSON.stringify(req.body.chat_session_id),
      "key",
      JSON.stringify(users)
    );
    console.log(updatedata);
  }

  // console.log(hashagent)

  // var data={crmId:"10000001",
  // phoneNumber:req.body.phoneNumber,
  // serviceRequestType:"",
  // customerLanguage:req.body.customerLanguage,
  // customerLanguageCode:"",
  // LastHandledAgentID:"",
  // routingRuleId:"",
  // skillSet:req.body.skillSet,
  // chat_session_id:req.body.chat_session_id}
  // axios
  // .post(config_file.rejectapi, data)
  // .then(async (resp) => {
  //  // console.log(resp)
  //  if(resp){
  //  res.json({ status: true, message:"chat rejected sucessfully"});
  //  }
  // })
}
async function agentConfirmation(req, res, next) {
  try {
    var agent_id = req.query.agent_id,
      client_id = req.query.client_id,
      status = req.query.status,
      user_id = req.query.user_id;
    //var tenantId=req.headers.tenantId;
    var variable = JSON.stringify(req.headers)
    var variable1 = JSON.parse(variable)
    console.log(variable1.tenantid);

    var tenantId = variable1.tenantid;
    await db.User.findByIdAndUpdate(
      {
       _id:mongoose.Types.ObjectId(req.query.agent_id) 
      },
      {
        $set: {
          lastactive_time:new Date()
        },
      }
    );
    var sessionvalue = await db.Session.findOne({
      _id: client_id
    });
    console.log(sessionvalue)
    if (sessionvalue.status == "Accept") {
      logger.error("AgentConfirmation:session already  accepted", user_id);
      res.json({ status: false, message: "session already  accepted" });
    } else if (sessionvalue.status == "CustomerDropped") {
      logger.error("AgentConfirmation:Customer Dropped chat", user_id);
      res.json({ status: false, message: "Customer Dropped chat" });
    } else {
      if (status === "Accept") {
        if(sessionvalue.agent!=agent_id){
          
          logger.info(user_id + "AgentConfirmation:agent id not correct notchat");
          res.json({ status: false, message: "Invalid agent id" });
        }
        socket.current = io(config.socketUrl + tenantId);
        socket.current.emit("check-agent", "yes transfer");
        let agentStatus;
        if (lastredisclient != undefined) {
          //console.log('agent',(JSON.stringify(agen)))
          var agents = JSON.stringify(agent_id);
          var ss = await redisClient.hVals(JSON.stringify(agents))
          console.log("ss",JSON.stringify(ss))
          //console.log(typeof(ss))
          var valu1 = JSON.stringify(ss)
          // console.log(typeof(valu1))
          var newval = JSON.parse(valu1);
          if(sessionvalue.channel=="email"){
            var havalue = await redisClient.HINCRBY(JSON.stringify("emailqueuecount"), JSON.stringify(agent_id), -1)
            var haongoingCount=await redisClient.HINCRBY(JSON.stringify("emailactivechatcount"), JSON.stringify(agent_id), 1)
          }else{
          var havalue = await redisClient.HINCRBY(JSON.stringify("queuecount"), JSON.stringify(agent_id), -1)
          var haongoingCount=await redisClient.HINCRBY(JSON.stringify("activechatcount"), JSON.stringify(agent_id), 1)
          }
          var upadeddata = newval.map(async (value) => {
            var parseddata = JSON.parse(value);
            if (parseddata["id"] == client_id) {
              console.log("parseddata", parseddata["id"])
              console.log("client", client_id)
              parseddata["status"] = "Accept";
              parseddata["chat_type"] = "external";
              parseddata["chat_started_at"] = new Date();
              parseddata["is_queued"] = false;

              console.log("updated_parseddata", parseddata)
              var values = await redisClient.hSet(JSON.stringify(agents), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata))
              console.log("value", values)
              if (values == 0 || values == 1) {
                if (config_file.redisenv == "checking") {
                  var ss = await redisClient.hVals(JSON.stringify("agentsDetails"))
                  //console.log(JSON.stringify(ss))
                  console.log(typeof (ss))
                  var agentStatusnew;
                  var valu1 = JSON.stringify(ss)
                  console.log(typeof (valu1))
                  var newval = JSON.parse(valu1);
                  var upadeddata = newval.map(async (value) => {
                    var parseddata = JSON.parse(value);
                    console.log("parsed", parseddata)
                    if (parseddata["userId"] == user_id) {
                      parseddata["chat"].active_chat_count =
                        parseddata["chat"].active_chat_count + 1;
                      parseddata["chat"].total_ongoing = parseddata["chat"].total_ongoing + 1;
                      parseddata["chat"].last_chat_start_time = new Date();
                      agentStatusnew = parseddata
                      var valu3 = await redisClient.hSet(JSON.stringify("agentsDetails"), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
                        //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
                      )
                    }
                    return parseddata;
                  })
                  socket.current.emit("agent-activity-input", agentStatusnew);

                  // await redisClient.publish(
                  //   "agent-activity",
                  //   JSON.stringify(agentStatusnew)
                  // );
                  if (config_file.routingenv == "Redis") {
                    await redisClient.publish(
                      "agent-activity",
                      JSON.stringify(agentStatusnew)
                    );
                  } else {
                    if (!agentStatusnew) {
                      var agentvalue = "Text Message"
                    } else {
                      var agentvalue = JSON.stringify(agentStatusnew[0])
                    }
                    var queuedetails = await axios.post(config_file.pushmessageapi, {
                      "queueName": "Test_ChatAPP_AgentActivity",
                      "routingKey": "Test_ChatAPP_AgentActivity",
                      "message": agentvalue,
                      "priority": 1
                    }, {
                      headers: { TenantID: tenantId },
                    })
                  }
                }
                else {
                  const cacheResults = await redisClient.get("agentsDetails");
                  let cache_resp = JSON.parse(cacheResults);

                  if (cache_resp) {
                     await db.User.findByIdAndUpdate({_id:mongoose.Types.ObjectId(agent_id)}, { $inc: { queued_count: -1 } })
                    var agentStatusnew = [];
                    var updatedlist= await cache_resp.filter((value) => {
                 
                      return (value["userId"] != user_id)
              
                    })
                    console.log("updatedlist",updatedlist)
                    await redisClient.set("agentsDetails", JSON.stringify(updatedlist));
                    cache_resp.forEach(async (item) => {
                      if (item["userId"] == user_id) {
                        item["chat"].active_chat_count =
                          item["chat"].active_chat_count + 1;
                        item["chat"].total_ongoing = item["chat"].total_ongoing + 1;
                        item["chat"].queued_count= item["chat"].queued_count >0?item["chat"].queued_count - 1:0;
                        item["chat"].last_chat_start_time = new Date();
                        logger.info(agent_id + ":Agent Confirmation Tested Queued Count" + item["chat"].queued_count)
                          logger.info(agent_id + ":Agent Confirmation  Tested active chat count" + item["chat"].active_chat_count)
                          logger.info(agent_id + ":Agent Confirmation  Tested Total Ongoing" + item["chat"].total_ongoing)

                        agentStatusnew = item;
                      }
                    });
                    await redisClient.set("agentsDetails", JSON.stringify(cache_resp));
                    socket.current.emit("agent-activity-input", agentStatusnew);

                    // await redisClient.publish(
                    //   "agent-activity",
                    //   JSON.stringify(agentStatusnew)
                    // );
                    if (config_file.routingenv == "Redis") {
                      await redisClient.publish(
                        "agent-activity",
                        JSON.stringify(agentStatusnew)
                      );
                    } else {
                      if (agentStatusnew.length <= 0) {
                        var agentvalue = "Text Message"
                      } else {
                        var agentvalue = JSON.stringify(agentStatusnew[0])
                      }
                      var queuedetails = await axios.post(config_file.pushmessageapi, {
                        "queueName": "Test_ChatAPP_AgentActivity",
                        "routingKey": "Test_ChatAPP_AgentActivity",
                        "message": agentvalue,
                        "priority": 1
                      }, {
                        headers: { TenantID: tenantId },
                      })
                    }

                  }
                }
                var updateUser = await db.User.findByIdAndUpdate(
                  agent_id,
                  {
                    $inc: { chat_count: 1,queued_count:-1 },
                  },
                  { new: true }
                );

                let findClient = await db.Session.findOne({ _id: client_id });

                var clientUpdate = await db.Client.findByIdAndUpdate(
                  { _id: findClient.unique_id },
                  {
                    $set: { status: "Accept" },
                  }
                );
                var dataValue = {
                  crmId: "10000001",
                  phoneNumber: findClient.phonenumber,
                  serviceRequestType: "",
                  customerLanguage: findClient.language,
                  customerLanguageCode: "",
                  LastHandledAgentID: "",
                  routingRuleId: "",
                  skillSet: findClient.skillset,
                  chat_session_id: findClient.chat_session_id,
                };
                if (config_file.env === "Production") {
                  await axios.post(config_file.routingbaseurl + "/routingengine/engine/acceptChat", dataValue, {
                    headers: { TENANTID: tenantId },
                  });
                }
                var queuedetails = await axios.post(
                  config_file.loginbaseurl + "/usermodule/clientMaster/chatConfig/list",
                  { enabled: true },
                  {
                    headers: { TenantID: tenantId },
                  }
                );
                var queueMessage = queuedetails.data.data
                  ? queuedetails.data.data.welcomeMessage
                  : "Welcome to Avaya Contact Center How can i help you?";

                let incomingreq = await db.Session.find({
                  chat_session_id: findClient.chat_session_id,
                }).populate("unique_id", "username email phonenumber");
                socket.current.emit("send-msg", {
                  to: incomingreq[0]._id,
                  session_id: incomingreq[0].chat_session_id,
                  from: incomingreq[0].agent,
                  senderName: updateUser.username,
                  chatType: "outbound",
                  msg:
                    "Hi " +
                    clientUpdate.username +
                    ", " +
                    queueMessage +
                    " this is  " +
                    updateUser.username + " how may i help you?",
                  msgType: "web",
                  userType: "external",
                  msg_sent_type: "TEXT",
                  chatdetails: incomingreq[0],
                  file_name: "",
                });
                await axios.post(config_file.customurl+`/v1/message/addMessage`, {
                  from: incomingreq[0].agent,
                  to: incomingreq[0]._id,
                  message: "Hi " +
                    clientUpdate.username +
                    ", " +
                    queueMessage +
                    " this is  " +
                    updateUser.username + " how may i help you?",
                  senderName: updateUser.username,
                  receiverName: clientUpdate.username,
                  msg_sent_type: "TEXT",
                  messageFrom: "fromAgent",
                  userType: "external",
                  chatType: "outbound",
                  file_name: "",
                  chatdetails: incomingreq[0],
                });
                var client = await db.Session.findByIdAndUpdate(
                  client_id,
                  {
                    status: "Accept",
                    chat_type: "external",
                    chat_started_at: new Date(),
                    is_queued: false,
                  },
                  { new: true }
                ).select(["email", "username", "_id"]);
                var agentActivity = await db
                  .agentActivity({
                    agent_id: agent_id,
                    user_id: user_id,
                    activity_name: "USER_ACCEPTED_CHAT",
                    value: findClient.chat_session_id,
                    created_at: new Date(),
                  })
                  .save();
                  if(sessionvalue.transferred){
                  var transfer_data={
                    interaction_id:sessionvalue.chat_session_id,
                    transfer_End_time: new Date(),
                    isfinished:true
                
                 }
                 const config1 = {
                  headers: { TenantID: tenantId },
                  };
                  await axios.post(
                    config.interactionApi+"transferDetails/updateTransferDetails",
                    transfer_data,
                    config1
                    );
                   }
                logger.info(user_id + "AgentConfirmation:agent accepeted chat", client);
                res.json({ status: true, data: client });
              } else {
                logger.info(user_id + "AgentConfirmation:agent accepeted notchat");
                res.json({ status: false, message: "not accepted" });
              }
              // console.log("value",valu3)
            }
            // return parseddata;
          })
          console.log(typeof upadeddata)
          //  var v= await redisClient.hSet(JSON.stringify(agents), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0])
          //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
          //       )
          //       console.log("value",v)

          // redisClient.HGET(JSON.stringify(agent_id), JSON.stringify(client_id), (err, result) => { 
          //   if (err) { console.error(err); return; } console.log("result"+result); 
          //   console.log("inside")
          //   const obj = JSON.parse(result[0])
          //   obj.status = "Accept";
          //     obj.chat_type = "external";
          //     obj.chat_started_at = new Date();
          //     obj.is_queued = false;
          //     redisClient.HSET(JSON.stringify(agent_id), JSON.stringify(client_id), JSON.stringify(obj), (err, result) => { if (err) { console.error(err); return; } console.log(result);}) 
          // })// logs a string containing the value of the field });
          //var value = await redisClient.get(JSON.stringify(agent_id));
          //   let cachedrequestlist = JSON.parse(value);
          //   logger.info(user_id+"AgentConfirmation:agent accepeted chat12"+value);
          //   console.log("cachedrequestlist", cachedrequestlist);
          //   console.log("test client id ", client_id);
          //   console.log("cached",cachedrequestlist)
          //   logger.info(user_id+"AgentConfirmation:agent accepeted chat123"+cachedrequestlist);
          //   var tempstorage=[];
          // await cachedrequestlist.forEach(async (item) => {
          //     console.log("item",item)
          //     logger.info(user_id+"AgentConfirmation:agent accepeted chat1234"+item);
          //     if (item["id"] == client_id) {
          //       logger.info(user_id+"AgentConfirmation:agent accepeted chat1234"+item["id"]);
          //       item["status"] = "Accept";
          //       item["chat_type"] = "external";
          //       item["chat_started_at"] = new Date();
          //       item["is_queued"] = false;
          //     }
          //      tempstorage.push(item)
          //   });
          //   logger.info(user_id+"AgentConfirmation:agent accepeted chat12345"+JSON.stringify(tempstorage));
          // console.log("after Updating " + cachedrequestlist);
          // redisClient.set(
          //   JSON.stringify(agent_id),
          //   JSON.stringify(tempstorage)
          // );
          // client.hset(JSON.stringify(agent_id), client_id, JSON.stringify(), (err, result) => { if (err) { console.error(err); return; } console.log(result);}) // logs: 0 or 1, depending on whether the field was updated });
          // var temp2=  await redisClient.get(JSON.stringify(agent_id))
          // logger.info(agent_id+"AgentConfirmation:agent accepeted chat1"+temp2);

        }


      }
    }
  } catch (error) {
    next(error);
  }
}
// function scanHash(cursor, pattern, count, results, callback) {
//     client.hscan(JSON.stringify(req.params.id), cursor, 'MATCH', pattern, 'COUNT', count, (err, reply) => {
//       if (err) {
//         callback(err);
//         return;
//       }

//    

//       const nextCursor = reply[0];
//       const fields = reply[1];

//    

//       for (let i = 0; i < fields.length; i += 2) {
//         const key = fields[i];
//         const value = fields[i + 1];

//    
//   const obj = JSON.parse(value); // Check if the object has a 'status' property and its value is equal to 1 
//   if (obj.status != 'Accept') { results[key] = obj;

//    }
//    

//       if (nextCursor === '0') {
//         // All results have been returned, call the callback with the final results object
//         callback(null, results);
//       } else {
//         // Continue scanning with the new cursor and results object
//         scanHash(nextCursor, pattern, count, results, callback);
//       }}
//     });

//   }
async function getIncomingUsers(req, res, next) {
  try {
    if (lastredisclient != undefined) {
      console.log("lastredisclient")
      var agen = JSON.stringify(req.params.id)
      console.log('agent', (JSON.stringify(agen)))
      var ss = await redisClient.hVals(JSON.stringify(agen))
      //console.log(JSON.stringify(ss))
      console.log(typeof (ss))
      var valu1 = JSON.stringify(ss)
      console.log(typeof (valu1))
      var newval = JSON.parse(valu1);

      var newdata = newval.filter((value) => {
        var parsedval = JSON.parse(value)
        console.log(typeof value)

        console.log(parsedval["status"])
        return parsedval["status"] === "newjoin"

      })
      const resData = newdata.map(val => JSON.parse(val))
      // console.log("newdata",newdata)
      logger.info(req.params.id + ":GetIncomingUsers:Incoming users got" + JSON.stringify(resData));
      res.json({ cache: true, status: true, data: resData });
      //console.log("val",JSON.parse(valu1))
      //  ss=JSON.parse(ss);
      //  console.log("redis",ss)
      // const cacheResults = await redisClient.get(JSON.stringify(req.params.id));
      // var isCached;
      // var results;
      // if (cacheResults != '""' && cacheResults && cacheResults != "[]") {
      //   isCached = true;
      //   results = JSON.parse(cacheResults);

      //   let filter = results.filter((id) => {
      //     return id["status"] != "Accept";
      //   });
      //   //console.log(filter.length);
      //   if (filter.length >= 1) {
      //     logger.info(req.params.id+":GetIncomingUsers:Incoming users got"+JSON.stringify(filter));
      //     res.json({ cache: true, status: true, data: filter });
      //   }
      //       const cursor = '0'; // start with a cursor of 0
      // const pattern = '*'; // match all fields
      // const count = '100'; // return up to 100 results per call



      // Define a function to recursively scan the hash and find matching fields




      // Call the scanHash function to scan the hash and find matching fields
      // scanHash(cursor, pattern, count, {}, (err, results) => {
      //   if (err) {
      //     console.error(err);
      //     return;
      //   }

      //  

      //   console.log(results);
      // res.json({ cache: true, status: true, data: results });
      //  // logs an object containing matching fields and their values
      // });
      // await redisClient.hGETALL(JSON.stringify(agen), (err, result) => {
      //    if (err) { 
      //     console.log("inif")
      //     console.error(err);

      //    } 
      //    console.log(result)
      //   //  else { 
      //   //   console.log("else")
      //   //   var valu1=JSON.stringify(result)
      //   //  var temp3= JSON.parse(valu1)
      //   //   const filteredResult = Object.entries(temp3) .filter(([field, value]) => field === 'status' && value !== 'Accept') .reduce((obj, [field, value]) => ({ ...obj, [field]: value }), {}); 
      //   //   console.log(filteredResult);
      //   //   res.json({ cache: true, status: true, data: filteredResult });
      //   //  }
      //    }
      //  );
    } else {
      console.log("else")
      const users = await db.Session.find({
        agent: req.params.id,
        status: "newjoin",
      }).populate("unique_id", "username email phonenumber");
      logger.info(req.params.id + ":GetIncomingUsers:Incoming users", users);
      return res.json({ cache: false, status: true, data: users });
    }

    // return res.json(users);
  } catch (error) {
    next(error);
  }
}

async function assignchattoagentfromchatbot(req, res, next) {
  try {
    //var tenantId=req.headers.tenantId;
    var variable = JSON.stringify(req.headers)
    var variable1 = JSON.parse(variable)
    console.log(variable1.tenantid);

    var tenantId = variable1.tenantid;
    if (config_file.env == "Production") {
      let data = {
        phoneNumber: req.body.phone,
        serviceRequestType: "",
        customerLanguage: req.body.language,
        customerLanguageCode: "",
        LastHandledAgentID: "",
        routingRuleId: "",
        skillSet: req.body.skillset,
        chat_session_id: req.body.chat_session_id,
      };
      await axios.post(config_file.routingapi, data).then(async (resp) => {
        if (resp.data.status == 1001) {
          if (resp.data.value != null) {
            // if (req.body.channel == "from_whatsapp" || req.body.channel == "from_facebook" || req.body.channel == "from_twitter") {
            //   req.body.availableAgent = "03bbcd2f-448b-4c55-8a95-99502c166955";
            // } else {
            req.body.availableAgent = resp.data.value.userId;
            // }

            let findAgent = await db.User.find({
              user_id: req.body.availableAgent,
            });

            if (findAgent[0] != undefined) {
              var updateresult = await db.Session.findByIdAndUpdate(
                req.body.session_id,
                {
                  $set: {
                    user_id: req.body.availableAgent,
                    available_agent: findAgent[0]._id,
                    agent: findAgent[0]._id,
                  },
                }
              ).populate("unique_id", "username email phonenumber");

              if (updateresult) {
                let incomingreq = await db.Session.find({
                  chat_session_id: req.body.chat_session_id,
                }).populate("unique_id", "username email phonenumber");
                let agent_id_incoming = JSON.stringify(findAgent[0]._id);

                if (redisClient != undefined) {
                  // var value = await redisClient.get(agent_id_incoming);

                  // let cachedrequestlist = JSON.parse(value);

                  // if (cachedrequestlist) {
                  //   cachedrequestlist = [...cachedrequestlist, incomingreq[0]];
                  // } else {
                  //   cachedrequestlist = [incomingreq[0]];
                  // }

                  // redisClient.set(
                  //   agent_id_incoming,
                  //   JSON.stringify(cachedrequestlist)
                  // );
                  //var agents=JSON.stringify(agent_id_incoming)
                  var v = await redisClient.hSet(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0])
                    //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
                  )
                  console.log("value", v)
                }
                var agentActivity = await db.agentActivity({ agent_id: findAgent[0]._id, user_id: req.body.availableAgent, activity_name: "USER_INCOMING_REQUEST", value: req.body.chat_session_id, created_at: new Date() }).save();
                socket = io(config.socketUrl + tenantId);

                socket.emit("send-new-req", incomingreq[0]);

                if (req.body.channel != "webchat") {
                  await axios.post(
                    config_file.customurl+`/v1/message/addMessage`,
                    {
                      from: req.body.chat_session_id,
                      to: findAgent[0]._id,
                      message: req.body.message,
                      senderName: req.body.name,
                      receiverName: findAgent[0].username,
                      messageFrom: "fromClient",
                      msg_sent_type: req.body.msg_sent_type
                    }
                  );

                  const data = await axios.post(
                    config_file.customurl+`/v1/users/getId/${req.body.email_id}`
                  );
                  if (data && data.length) {
                    socket = io(config.socketUrl + tenantId);
                    socket.emit("add-user", data.data.user.id);
                  }
                }
                logger.info(req.body.chat_session_id + ":CreateSession:chat session created successsfully", incomingreq)
                res.json({
                  status: true,
                  message: "chat session created successfully",
                  data: incomingreq,
                  chat_session_id: req.body.chat_session_id,
                  UserId: req.body.availableAgent,
                  chat_inititaed_at: new Date()
                });

                // console.log(req.body);
                return;
              } else {
                logger.error("CreateSession:Agent not logged in", "")
                res.json({
                  status: false,
                  msg: "Agent Not logged In",
                  data: "",
                });
                return;
              }
            } else {
              logger.error("CreateSession:Unable to create session", "")
              res.json({
                status: false,
                msg: "Unable to create session please try after sometime",
                data: "",
              });
              return;
            }
          } else {
            var queueMessage = queuedetails.data.data ? queuedetails.data.data.queueMessage : "Please wait, your request is in queue";
            let incomingreq = await db.Session.find({
              chat_session_id: req.body.chat_session_id,
            }).populate("unique_id", "username email phonenumber");
            await db.Session.findOneAndUpdate({ chat_session_id: req.body.chat_session_id }, { $set: { is_queued: true } });
            await axios.post(
              config_file.customurl+`/v1/message/addMessage`,
              {
                from: incomingreq[0]._id,
                to: incomingreq[0]._id,
                message: queueMessage,
                senderName: "Queue Message",
                receiverName: req.body.name,
                msg_sent_type: "TEXT",
                messageFrom: "fromAgent",
                userType: "external",
                file_name: "",
                chatdetails: incomingreq[0]
              }
            );

            const data = await axios.post(
              config_file.customurl+`/v1/users/getId/${req.body.email_id}`
            );
            if (data && data.length) {
              socket = io(config.socketUrl + tenantId);
              socket.emit("add-user", data.data.user.id);
            } logger.info(req.body.chat_session_id + ":CreateSession:Session creation in queue", incomingreq)
            res.json({
              status: true,
              message: queueMessage,
              data: incomingreq,
              chat_session_id: req.body.chat_session_id,
              UserId: null,
              chat_inititaed_at: new Date()
            });
            return;
          }
        } else {
          var queuedetails = await axios.post(config.queuemsgapi, { enabled: true }, {
            headers: { TenantID: tenantId },
          })
          var queueMessage = queuedetails.data.data ? queuedetails.data.data.queueMessage : "Please wait, your request is in queue";
          let incomingreq = await db.Session.find({
            chat_session_id: req.body.chat_session_id,
          }).populate("unique_id", "username email phonenumber");
          await db.Session.findOneAndUpdate({ chat_session_id: req.body.chat_session_id }, { $set: { is_queued: true } });
          await axios.post(
            config_file.customurl+`/v1/message/addMessage`,
            {
              from: incomingreq[0]._id,
              to: incomingreq[0]._id,
              message: queueMessage,
              senderName: "Queue Message",
              receiverName: req.body.name,
              msg_sent_type: "TEXT",
              messageFrom: "fromAgent",
              userType: "external",
              file_name: "",
              chatdetails: incomingreq[0]
            }
          );

          const data = await axios.post(
            config_file.customurl+`/v1/users/getId/${req.body.email_id}`
          );
          if (data && data.length) {
            socket = io(config.socketUrl + tenantId);
            socket.emit("add-user", data.data.user.id);

            socket.emit("send-msg", {
              to: incomingreq[0]._id,
              session_id: incomingreq[0].chat_session_id,
              from: incomingreq[0]._id,
              senderName: "Queue Message",
              chatType: "outbound",
              msg: queueMessage,
              msgType: "web",
              userType: "external",
              msg_sent_type: "TEXT",
              chatdetails: incomingreq[0],
              file_name: "",
            });
          }
          logger.info(req.body.chat_session_id + ":CreateSession:Session in queue", incomingreq)
          res.json({
            status: true,
            message: "You are in queue please wait",
            data: incomingreq,
            chat_session_id: req.body.chat_session_id,
            UserId: null,
            chat_inititaed_at: new Date()
          });
          return;
        }
      });
    } else {
      //var tenantId=req.headers.tenantId;
      var variable = JSON.stringify(req.headers)
      var variable1 = JSON.parse(variable)
      console.log(variable1.tenantid);

      var tenantId = variable1.tenantid;
      req.body.availableAgent = "03bbcd2f-448b-4c55-8a95-99502c166955";


      let findAgent = await db.User.find({
        user_id: req.body.availableAgent,
      });

      if (findAgent[0] != undefined) {
        var updateresult = await db.Session.findByIdAndUpdate(
          req.body.session_id,
          {
            $set: {
              user_id: req.body.availableAgent,
              available_agent: findAgent[0]._id,
              agent: findAgent[0]._id,
              ischatbot: false
            },
          }
        ).populate("unique_id", "username email phonenumber");

        if (updateresult) {
          let incomingreq = await db.Session.find({
            chat_session_id: req.body.chat_session_id,
          }).populate("unique_id", "username email phonenumber");
          let agent_id_incoming = JSON.stringify(findAgent[0]._id);

          if (redisClient != undefined) {
            // var value = await redisClient.get(agent_id_incoming);

            // let cachedrequestlist = JSON.parse(value);

            // if (cachedrequestlist) {
            //   cachedrequestlist = [...cachedrequestlist, incomingreq[0]];
            // } else {
            //   cachedrequestlist = [incomingreq[0]];
            // }

            // redisClient.set(
            //   agent_id_incoming,
            //   JSON.stringify(cachedrequestlist)
            // );

            var v = await redisClient.hSet(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0])
              //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
            )
            console.log("value", v)
          }
          var agentActivity = await db.agentActivity({ agent_id: findAgent[0]._id, user_id: req.body.availableAgent, activity_name: "USER_INCOMING_REQUEST", value: req.body.chat_session_id, created_at: new Date() }).save();
          socket = io(config.socketUrl + tenantId);

          socket.emit("send-new-req", incomingreq[0]);

          if (req.body.channel != "webchat") {
            await axios.post(
              config_file.customurl+`/v1/message/addMessage`,
              {
                from: req.body.session_id,
                to: findAgent[0]._id,
                message: req.body.message,
                senderName: req.body.name,
                receiverName: findAgent[0].username,
                messageFrom: "fromClient",
                msg_sent_type: req.body.msg_sent_type
              }
            );

            const data = await axios.post(
              config_file.customurl+`/v1/users/getId/${req.body.email_id}`
            );
            if (data && data.length) {
              socket = io(config.socketUrl + tenantId);
              socket.emit("add-user", data.data.user.id);
            }
          }
          logger.info(req.body.chat_session_id + ":CreateSession:chat session created ", incomingreq)
          res.json({
            status: true,
            message: "chat session created successfully",
            data: incomingreq,
            chat_session_id: req.body.chat_session_id,
            UserId: req.body.availableAgent,
            chat_inititaed_at: new Date()
          });

          // console.log(req.body);
          return;
        }
      }
    }
  }
  catch (error) {
    // var tenantId=req.headers.tenantId;
    var variable = JSON.stringify(req.headers)
    var variable1 = JSON.parse(variable)
    console.log(variable1.tenantid);

    var tenantId = variable1.tenantid;
    req.body.availableAgent = "03bbcd2f-448b-4c55-8a95-99502c166955";


    let findAgent = await db.User.find({
      user_id: req.body.availableAgent,
    });

    if (findAgent[0] != undefined) {
      var updateresult = await db.Session.findByIdAndUpdate(
        req.body.session_id,
        {
          $set: {
            user_id: req.body.availableAgent,
            available_agent: findAgent[0]._id,
            agent: findAgent[0]._id,
            ischatbot: false
          },
        }
      ).populate("unique_id", "username email phonenumber");

      if (updateresult) {
        let incomingreq = await db.Session.find({
          chat_session_id: req.body.chat_session_id,
        }).populate("unique_id", "username email phonenumber");
        let agent_id_incoming = JSON.stringify(findAgent[0]._id);

        if (redisClient != undefined) {
          // var value = await redisClient.get(agent_id_incoming);

          // let cachedrequestlist = JSON.parse(value);

          // if (cachedrequestlist) {
          //   cachedrequestlist = [...cachedrequestlist, incomingreq[0]];
          // } else {
          //   cachedrequestlist = [incomingreq[0]];
          // }

          // redisClient.set(
          //   agent_id_incoming,
          //   JSON.stringify(cachedrequestlist)
          // );
          var v = await redisClient.hSet(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0])
            //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
          )
          console.log("value", v)
        }
        var agentActivity = await db.agentActivity({ agent_id: findAgent[0]._id, user_id: req.body.availableAgent, activity_name: "USER_INCOMING_REQUEST", value: req.body.chat_session_id, created_at: new Date() }).save();
        socket = io(config.socketUrl + tenantId);

        socket.emit("send-new-req", incomingreq[0]);

        if (req.body.channel != "webchat") {
          await axios.post(
            config_file.customurl+`/v1/message/addMessage`,
            {
              from: req.body.session_id,
              to: findAgent[0]._id,
              message: req.body.message,
              senderName: req.body.name,
              receiverName: findAgent[0].username,
              messageFrom: "fromClient",
              msg_sent_type: req.body.msg_sent_type
            }
          );

          const data = await axios.post(
            config_file.customurl+`/v1/users/getId/${req.body.email_id}`
          );
          if (data && data.length) {
            socket = io(config.socketUrl + tenantId);
            socket.emit("add-user", data.data.user.id);
          }
        }
        logger.info(req.body.chat_session_id + ":CreateSession:chat session created ", incomingreq)
        res.json({
          status: true,
          message: "chat session created successfully",
          data: incomingreq,
          chat_session_id: req.body.chat_session_id,
          UserId: req.body.availableAgent,
          chat_inititaed_at: new Date()
        });

        // console.log(req.body);
        return;
      }
    }

  }
}


async function assignchattoagent(req, res, next) {
  console.log("Assign agent from queue", req.body.chat_session_id);
  // var tenantId=req.headers.tenantId;
  var variable = JSON.stringify(req.headers)
  var variable1 = JSON.parse(variable)
  console.log(variable1.tenantid);

  var tenantId = variable1.tenantid;
  var sessionvalue = await db.Session.findOne({
    chat_session_id: req.body.chat_session_id
  });
  // sessionvalue.available_agent
  if(sessionvalue.available_agent==null){
  let findAgent = await db.User.find({
    user_id: req.body.agentId,
  });
  var updateresult = await db.Session.findOneAndUpdate(
    { chat_session_id: req.body.chat_session_id, status: "newjoin" },
    {
      $set: {
        user_id: req.body.agentId,
        available_agent: findAgent[0]._id,
        agent: findAgent[0]._id,
        is_queued: false,
      },
    }
  ).populate("unique_id", "username email phonenumber");

  if (updateresult) {
    var updateuser=await db.User.findByIdAndUpdate(findAgent[0].id,{ $inc: { queued_count:1 } })
    let incomingreq = await db.Session.find({
      chat_session_id: req.body.chat_session_id,
      status: "newjoin",
    }).populate("unique_id", "username email phonenumber");
    if (incomingreq[0]) {
      let agent_id_incoming = JSON.stringify(findAgent[0]._id);

      if (redisClient != undefined) {
        // var value = await redisClient.get(agent_id_incoming);

        // let cachedrequestlist = JSON.parse(value);

        // if (cachedrequestlist) {
        //   cachedrequestlist = [...cachedrequestlist, incomingreq[0]];
        // } else {
        //   cachedrequestlist = [incomingreq[0]];
        // }

        // redisClient.set(agent_id_incoming, JSON.stringify(cachedrequestlist));
        var v = await redisClient.hSet(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0])
          //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
        )
        await redisClient.HINCRBY(JSON.stringify("queuecount"), JSON.stringify(findAgent[0]._id), 1)
        console.log("value", v)
        const cacheResults = await redisClient.get("agentsDetails");
        let cache_resp = JSON.parse(cacheResults);
        if (cache_resp) {
          var agentStatusnew = [];
          cache_resp.forEach(async (item) => {
            if (item["userId"] == findAgent[0].user_id) {
              item["chat"].queued_count= item["chat"].queued_count + 1;

              agentStatusnew = item;
            }
          });
          await redisClient.set("agentsDetails", JSON.stringify(cache_resp));
         }
      }
      var agentActivity = await db
        .agentActivity({
          agent_id: findAgent[0]._id,
          user_id: req.body.agentId,
          activity_name: "USER_ASSIGN_CHAT_AGENT",
          value: req.body.chat_session_id,
          created_at: new Date(),
        })
        .save();

      //  console.log("Incoming request for the agent",incomingreq[0])
      socket.current = io(config.socketUrl + tenantId);

      socket.current.emit("send-new-req", incomingreq[0]);
      var updatedata=  {
        "Intraction_id":sessionvalue.chat_session_id,
        "agent_id": req.body.agentId,
        "GroupId":findAgent[0].group_array[0]?findAgent[0].group_array[0]:"31188316-f485-4e02-9987-794ba938fef3",
        "agent_extension": findAgent[0].email,
        "interaction_disconnected":false,
        

    } 
    const config1 = {
      headers: { TenantID:"a3dc14bd-fe70-4120-8572-461b0dc866b5"  },
      };
      await axios.post(
        config_file.interactionApi+"liveReport/updateLiveReport",
        updatedata,
        config1
        );
      logger.info(req.body.chat_session_id + ":AssignChatAgent:agent assigned sucessfully", incomingreq[0]);
      res.json({
        status: true,
        message: "agent assigned sucessfully",
        data: incomingreq[0],
      });
    } else {
      logger.error("AssignChatAgent:Customer left the session", req.body.chat_session_id);
      res.json({
        status: false,
        message: "Customer left the session",
        data: [],
      });
    }
  } else {
    logger.error("AssignChatAgent:Invalid Chat Session ID", req.body.chat_session_id);
    res.json({ status: false, message: "Invalid Chat Session ID", data: [] });
  }
}else{
  logger.error("AssignChatAgent:Session Already Accepted", req.body.chat_session_id);
  res.json({ status: false, message: "Session Already Accepted", data: [] });
}
}
async function chatHistory(req, res, next) {
  try {
    var session_id = req.body.session_id;
    var history = await db.chatHistory
      .find({ session_id: session_id })
      .sort({ time: -1 })
      .populate("agent_id", { username: 1 })
      .populate("destinationagent_id", { username: 1 });
    //var transferhistory=await db.transferHistory.find({session_id:session_id}).populate("agent_id",{username:1}).populate("destinationagent_id",{username:1});
    if (history && history.length) {
      // var transferedHistory,conferenceHistory;

      var chatHistory = history.map((msg) => {
        return {
          type: msg.type,
          agent: msg.agent_id.username,
          destinationAgent: msg.destinationagent_id.username,
          agent_id: msg.agent_id._id,
          destinationagent_id: msg.destinationagent_id._id,
          user_id: msg.user_id,
          time: msg.time,
          session_id: msg.session_id,
        };
      });
      logger.info(session_id + ":ChatHistory:List of chat history", chatHistory)
      res.json({ status: true, data: chatHistory });

      // if(transferedHistory.length&&conferenceHistory.length){
      // res.json({ status: true,data:transferedHistory.concat(conferenceHistory) });
      // }else if(transferedHistory.length){
      //   res.json({ status: true,data:transferedHistory });
      // }else if(conferenceHistory.length){
      //   res.json({ status: true,data:conferenceHistory });
      // }
    } else {
      logger.error("ChatHistory:No data found", session_id)
      res.json({ status: false, message: "no data found" });
    }
  } catch (e) {
    next(e);
  }
}
//list users based on either language_id or skillset_id andaget_status avalible
async function listUserBySkillsetIdLanguageId(req, res, next) {
  try {
    var { language_id, skillset_id, agent_id } = req.body;
    if (language_id && skillset_id) {
      var data = await db.User.find({
        "language.languageId": language_id,
        "skillSet.skillId": skillset_id,
        agent_status: "Available",
        _id: { $ne: mongoose.Types.ObjectId(agent_id) },
      });
      var count = await db.User.countDocuments({
        "language.languageId": language_id,
        "skillSet.skillId": skillset_id,
        agent_status: "Available",
        _id: { $ne: mongoose.Types.ObjectId(agent_id) },
      });
      logger.info(agent_id + ":ListUserSkillsetLanguage:User details based on language skilset", data)
      res.json({
        status: true,
        message: "Data fetched Successfully",
        count: count,
        data: data,
      });
    } else if (language_id) {
      var data = await db.User.find({
        "language.languageId": language_id,
        agent_status: "Available",
        _id: { $ne: mongoose.Types.ObjectId(agent_id) },
      });
      var count = await db.User.countDocuments({
        "language.languageId": language_id,
        agent_status: "Available",
        _id: { $ne: mongoose.Types.ObjectId(agent_id) },
      });
      logger.info(agent_id + ":ListUserSkillsetLanguage:User details based on language skilset", data)
      res.json({
        status: true,
        message: "Data fetched Successfully",
        count: count,
        data: data,
      });
    } else if (skillset_id) {
      var data = await db.User.find({
        "skillSet.skillId": skillset_id,
        agent_status: "Available",
        _id: { $ne: mongoose.Types.ObjectId(agent_id) },
      });
      var count = await db.User.countDocuments({
        "skillSet.skillId": skillset_id,
        agent_status: "Available",
        _id: { $ne: mongoose.Types.ObjectId(agent_id) },
      });
      logger.info(agent_id + ":ListUserSkillsetLanguage:User details based on language skilset", data)
      res.json({
        status: true,
        message: "Data fetched Successfully",
        count: count,
        data: data,
      });
    } else {
      logger.error("ListUserSkillsetLanguage:No data found", "")
      res.json({ status: false, message: "no data found" });
    }
  } catch (e) {
    next(e);
  }
}
//list supervisor based on rolename as supervisor
async function listSupervisor(req, res, next) {
  try {
    var supervisor = await db.roleManage
      .findOne({
        $or: [{ roleName: "Supervisor" }, { roleName: "supervisor" }, { roleName: "Supervisor " }],
      })
      .select({ id: 1, _id: 0 });
    if (supervisor) {
      var listUser = await db.User.find({ roles_array: supervisor.id, agent_status: "Available" });
      var count = await db.User.countDocuments({ roles_array: supervisor.id, agent_status: "Available" });
      logger.info(listUser._id + ":ListSupervisor:List users based on supervisor", listUser)
      res.json({
        status: true,
        message: "Data fetched Successfully",
        count: count,
        data: listUser,
      });
    } else {
      logger.error("ListSupervisor:No data found", "")
      res.json({ status: false, message: "no data found" });
    }
  } catch (e) {
    next(e);
  }
}
async function listagentList(req, res, next) {
  try {
    var { offset, limit } = req.body;
    var agent = await db.roleManage
      .findOne({ $or: [{ roleName: "Agent" }, { roleName: "agent" }] })
      .select({ id: 1, _id: 0 });
    if (agent) {
      var listUser = await db.User.find({ roles_array: agent.id })
        .skip(offset)
        .limit(limit);
      var count = await db.User.countDocuments({ roles_array: agent.id });
      logger.info(listUser._id + ":ListAgentList:List users agent list", listUser)
      res.json({
        status: true,
        message: "Data fetched Successfully",
        count: count,
        data: listUser,
      });
    } else {
      logger.error("ListAgentList:No data found", "")
      res.json({ status: false, message: "no data found" });
    }
  } catch (e) {
    next(e);
  }
}
async function transfertoWhatsapp(req, res, next) {
  try {
    var { chat_session_id, phonenumber } = req.body;
    var session = await db.Session.findOneAndUpdate(
      {
        chat_session_id: chat_session_id,
        channel: { $ne: "from_whatsapp" },
        is_transfertoWhatsapp: false,
      },
      { $set: { status: "chatEnded", is_transfertoWhatsapp: true } }
    ).populate("agent", { username: 1 });

    console.log(session.phonenumber);
    console.log(phonenumber);
    if (session) {
      var payload = {
        messaging_product: "whatsapp",
        to: phonenumber ? phonenumber : session.phonenumber,
        type: "template",
        template: {
          name: "welcome_message",
          language: {
            code: "en",
          },
        },
      };

      //   console.log(payload);
      const header = {
        headers: {
          Authorization: "Bearer " + config_file.whatsapp_token,
          "Content-Type": "application/json",
        },
      };
      var whatsappapi = await axios.post(
        config_file.transferWhatsappapi,
        payload,
        header
      );
      //   console.log(whatsappapi);
      const message = await db.Message.create({
        message: { text: "welcome_message" },
        users: [session.agent._id, session.unique_id],
        sender: session.agent._id,
        receiver: session.unique_id,
        senderName: session.agent.username,
        receiverName: session.unique_id,
        session_id: session.chat_session_id,
      });
      var data = await db
        .Session({
          phonenumber: phonenumber ? phonenumber : session.phonenumber,
          channel: "from_whatsapp",
          email: session.email,
          lattitude: session.lattitude,
          client_ip: session.client_ip,
          user_agent: session.user_agent,
          is_available: session.is_available,
          chat_session_id: session.chat_session_id,
          conference: session.conference,
          is_conferenceleft: session.is_conferenceleft,
          is_customer_disconnected: session.is_customer_disconnected,
          transferred: session.transferred,
          chat_type: session.chat_type,
          arrival_at: session.arrival_at,
          chat_started_at: session.chat_started_at,
          chat_ended_at: session.chat_ended_at,
          unique_id: session.unique_id,
          skillset: session.skillset,
          language: session.language,
          complaint: session.complaint,
          agent_status: session.agent_status,
          createdAt: session.createdAt,
          updatedAt: session.updatedAt,
          agent: session.agent._id,
          available_agent: session.available_agent,
          user_id: session.user_id,
          lastmessage: session.lastmessage,
          lastmessagetime: session.lastmessagetime,
          status: session.status,
          lastmessageUpdatedat: session.lastmessageUpdatedat
        })
        .save();
      if (data) {
        logger.info(chat_session_id + ":TransferToWhatsapp:webchat transfer to whatsapp", data)
        res.json({
          status: true,
          message: "webchat transfer to whatsapp successfully",
          data: data,
        });
      }
    } else {
      logger.error("TransferToWhatsapp:No data Found", "")
      res.json({
        status: false,
        message: "No data found",
      });
    }
  } catch (e) {
    next(e);
  }
}
async function updateClient(req, res, next) {
  try {
    var { username, email, phonenumber, whatsappnumber, facebookId, twitterId, teamsId, address, company, id,agent_id } = req.body
    if (!id) {
      logger.error("CilentUpdate:Id is missing", "")
      res.json({
        status: false,
        message: "Id is missing",
      });
    } else {
      if (lastredisclient != undefined) {
        //console.log('agent',(JSON.stringify(agen)))
        var agents = JSON.stringify(agent_id);
        var ss = await redisClient.hVals(JSON.stringify(agents))
        //console.log(JSON.stringify(ss))
        //console.log(typeof(ss))
        var valu1 = JSON.stringify(ss)
        // console.log(typeof(valu1))
        var newval = JSON.parse(valu1);
        var upadeddata = newval.map(async (value) => {
          var parseddata = JSON.parse(value);
        
          if (parseddata.unique_id.id ==id) {
            
            parseddata.unique_id.username=username
            parseddata.unique_id.email=email
            parseddata.unique_id.phonenumber=phonenumber
            parseddata.unique_id.whatsappnumber=whatsappnumber
            parseddata.unique_id.facebookId=facebookId
            parseddata.unique_id.twitterId=twitterId
            parseddata.unique_id.teamsId=teamsId
            parseddata.unique_id.address=address
            parseddata.unique_id.company=company
            console.log("parsed",parseddata.unique_id)
            // parseddata["chat"].active_chat_count =
            //   parseddata["chat"].active_chat_count + 1;
            // parseddata["chat"].total_ongoing = parseddata["chat"].total_ongoing + 1;
            // parseddata["chat"].last_chat_start_time = new Date();
            // agentStatusnew = parseddata
            var valu3 = await redisClient.hSet(JSON.stringify(agents), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
              //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
            )
            console.log("val",valu3)
          }
        })
      }
      var updatedclient = await db.Client.findByIdAndUpdate(id, {
        username,
        email,
        phonenumber,
        whatsappnumber,
        facebookId,
        twitterId,
        teamsId,
        address,
        company
      }, { new: true })
      if (updatedclient != null) {
        logger.info(updatedclient.id + ":CilentUpdate:Value updated successfully", updatedclient)
        res.json({
          status: true,
          message: "Client details Updated Successfully",
          data: updatedclient,
        });
      } else {
        logger.error("CilentUpdate:Value not updated", "")
        res.json({
          status: false,
          message: "Value not updated",
        });
      }
    }

  }
  catch (e) {
    next(e);
  }
}
async function logoutall(req,res,next){
  try{
    await db.User.updateMany({is_loggedIn:true},{$set: {
      agent_status: "Not Available",
      agent_status_real: "Is LoggedOut",
      is_loggedIn: false,
    }})
    let cacheResults = await redisClient.del("agentsDetails")
    console.log("cacheResults",cacheResults)
    res.json({
      success: true,
      message: "All agents Logged out",
      
    })

  }
  catch(e){
    res.json({
      success: false,
      message: "Something went wrong",
      
    })
  }
}