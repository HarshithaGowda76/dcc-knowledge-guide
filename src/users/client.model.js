const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const schema = new Schema(
  {
    username: {
      type: String,
    },
    email: {
      type: String,
    },
     phonenumber: {
      type: String,
    },
    whatsappnumber:{
      type:String
    },
    facebookId:{
      type:String
    },
    twitterId:{
      type:String
    },
    teamsId:{
      type:String
    },
    address:{
      type:String
    },
    company:{
      type:String
    }
  },
  {
    timestamps: true,
  }
);

schema.set("toJSON", {
  virtuals: true,

  versionKey: false,

  transform: function (doc, ret) {
    // remove these props when object is serialized

    delete ret._id;
  },
});

module.exports = mongoose.model("Client", schema);
