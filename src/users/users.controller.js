const express = require("express");
const router = express.Router();
const userService = require("./users.service");
const Routingservice = require("../../_middleware/user-service");
const LoginService=require("../../_middleware/login-service");
const callSession=require("./callsessionservice");

 router.post("/login", userService.login);
 router.post("/callcreateSession",callSession.usercreation,callSession.sessioncreation)
// router.post("/login",LoginService.userlogin,
//   LoginService.skillsetGenrate,
//   LoginService.redisRoute)
router.post(
  "/createsession",
  Routingservice.usercreation,
  Routingservice.sessioncreation,
  Routingservice.RoutingApiNew
);

//Api for create session
router.post(
  "/createsessionnew",
  Routingservice.usercreation,
  Routingservice.sessioncreation,
  Routingservice.RoutingApiNew
);

//if chat went to queue agent ID updation
router.post("/updateAgent", userService.assignchattoagent);

//fromchat bot callling routing api


router.post("/assignchattoagentfromchatbot", userService.assignchattoagentfromchatbot);
//if chat went to queue agent ID updation
router.post("/rejectchat", userService.rejectchat);

router.post("/rejectchatnew", userService.rejectchatnew);

//List of available agents for conference and Transfer
router.post("/getAgents", userService.getAgents);

//Tranfser chat to agent function
router.post("/transferAgent", userService.transferAgent);

//Conference chat to agent function
router.post("/conferenceAgent", userService.conferenceAgent);

// To accept the chat
router.post("/agentConfirmation", userService.agentConfirmation);

// List of new incoming chat request for agents
router.post("/getIncomingUsers/:id", userService.getIncomingUsers);

//Update Agent Status
router.post("/updateAgentStatus", userService.updateAgentStatus);

// All available Internal Agents 
router.post("/availableInternalAgents", userService.availableInternalAgents);

//Get all incoming accepted users for chat
router.post("/allUsers", userService.getAllUsers);

// All Internal Agents with session created
router.post("/allInternalUsers", userService.allInternalUsers);


router.post("/logout/:id", userService.logOut);
//logoutnew api available chat session also checking
router.post("/logoutnew/:id",userService.logOutnew);


//Get information based on CLI
router.post("/getInfoBasedOnNum", userService.getInfoBasedOnNum);

//create session for internal chat
router.post(
  "/createInternalAgentsSession",
  userService.createInternalAgentsSession
);

//Clients Chat APIs
// Get Client Details based on email for client chat
router.post("/getId/:email", userService.getId);
//Get session details for clients
router.post("/getClients/:id", userService.getClients);

//chathistory based on session id (transferhistory&conferencehistory)
router.post("/listchatHistory",userService.chatHistory);

//list users based on either language_id or skillset_id andaget_status avalible 
router.post("/listUserBySkillsetIdLanguageId",userService.listUserBySkillsetIdLanguageId)
//list supervisor based on rolename as supervisor
router.post("/listSupervisor",userService.listSupervisor);
//list agentlist
router.post("/listagentList",userService.listagentList)
//list all internal agent userslist and avalible agents
router.post("/allInternalUsersList",userService.allInternalUsersList)
//create internal agent session new
router.post("/createInternalAgentsSessionNew",userService.createInternalAgentsSessionNew)
//transfer webchat to whatsappp chat 
router.post("/transfertoWhatsapp",userService.transfertoWhatsapp),
router.post("/updateClient",userService.updateClient)
router.get("/logoutall",userService.logoutall)
module.exports = router;
