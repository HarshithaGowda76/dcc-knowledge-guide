const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const schema = new Schema(
  {
    username: {
      type: String,
      required: true,
      min: 3,
      max: 50,
    },
    email: {
      type: String,
      required: true,

      max: 50,
    },

    password: {
      type: String,
      required: true,
      min: 8,
    },
    phonenumber: {
      type: String,
    },
    user_id: {
      type: String,
    },
    mobile: {
      type: String,
    },
    is_available: {
      type: Boolean,
      default: true,
    },
    chat_count: {
      type: Number,
      default: 0,
    },
    chat_end_count: {
      type: Number,
      default: 0,
    },
    roles_array: {
      type: Array,
    },
    group_array: {
      type: Array,
    },

    language: {
      type: Array,
    },
    skillSet: {
      type: Array,
    },
    agent_status: {
      type: String,
      default: "Available",
    },
    agent_status_real: {
      type: String,
      default: "Available",
    },
    is_loggedIn:{
      type:Boolean,
      default:false
    },  is_supervisor:{
      type:Boolean,
      default:false
          },
          channel:{
            type:Array
          },
          voiceRecord:{
            type:Array
          },
          agent_id: {  type: mongoose.Schema.Types.ObjectId },
          queued_count: {
            type: Number,
            default: 0,
          },
          lastactive_time:{type:Date},
  },
  {
    timestamps: true,
  }
);

schema.set("toJSON", {
  virtuals: true,

  versionKey: false,

  transform: function (doc, ret) {
    // remove these props when object is serialized

    delete ret._id;
  },
});

module.exports = mongoose.model("User", schema);
