const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const schema = new Schema(
  {
    username: {
      type: String,
    },
    phonenumber: {
      type: String,
    },
    email: {
      type: String,
    },
    channel: {
      type: String,
      default: "webchat",
    },

    client_ip: {
      type: String,
    },
    is_available: {
      type: Boolean,
      default: true,
    },

    chat_session_id: {
      type: String,
    },
    chat_duration: {
      type: String,
      default: "00:00:00",
    },

    user_id: {
      type: String,
    },
    device_type: {
      type: String,
      default: "web",
    },

    arrival_at: {
      type: Date,
    },
    chat_started_at: {
      type: Date,
    },
    chat_ended_at: {
      type: Date,
    },
    unique_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Client",
    },
    sender_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    receiver_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    chat_type: {
      type: String,
      default: "internal",
    },
    agent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },

    skillset: {
      type: String,
    },
    language: {
      type: String,
    },
    complaint: {
      type: String,
    },
    available_agent: {
      type: String,
    },
    lastmessage: {
      type: String,
    },
    lastmessagetime: {
      type: String,
    },
    unreadcount: {
      type: String,
    },
    status: {
      type: String,
      default: "Accept",
    },
    chatendby: {
      type: String,
    },
  },

  {
    timestamps: true,
  }
);

schema.set("toJSON", {
  virtuals: true,

  versionKey: false,

  transform: function (doc, ret) {
    // remove these props when object is serialized

    delete ret._id;
  },
});

module.exports = mongoose.model("AgentSession", schema);
