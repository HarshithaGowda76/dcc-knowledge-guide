const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const schema = new Schema(
  {
    phonenumber: {
      type: String,
    },
    email: {
      type: String,
    },
    channel: {
      type: String,
      default: "webchat",
    },
    lattitude: {
      type: String,
    },
    longitude: {
      type: String,
    },
    client_ip: {
      type: String,
    },

    user_agent: {
      type: Object,
    },
    is_available: {
      type: Boolean,
      default: true,
    },

    chat_session_id: {
      type: String,
    },
    conference: {
      type: Boolean,
      default: false,
    },
    is_conferenceleft: {
      type: Boolean,
      default: false,
    },
    conference_left_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    emailId: {
      type: mongoose.Schema.Types.ObjectId,
    },
    is_customer_disconnected: {
      type: Boolean,
      default: false,
    },
    is_customer_disconnected_time: {
      type: Date,
    },
    conference_agent_time: {
      type: Date,
    },
    conference_agent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    transferred: {
      type: Boolean,
      default: false,
    },
    transfer_agent_time: {
      type: Date,
    },
    transfer_agent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    user_id: {
      type: String,
    },
    chat_type: {
      type: String,
      default: "external",
    },
    sender_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    receiver_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },

    arrival_at: {
      type: Date,
    },
    chat_started_at: {
      type: Date,
    },
    chat_ended_at: {
      type: Date,
    },
    unique_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Client",
    },
    agent: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    whatsapp_msg_id: {
      type: String,
    },
    skillset: {
      type: String,
    },
    language: {
      type: String,
    },
    complaint: {
      type: String,
    },
    available_agent: {
      type: String,
    },
    lastmessage: {
      type: String,
    },
    lastmessagetime: {
      type: String,
    },
    unreadcount: {
      type: String,
    },
    status: {
      type: String,
      default: "newjoin",
    },
    agent_status: {
      type: String,
      default: "Available",
    },
    chatendby: {
      type: String,
    },
    is_transfertoWhatsapp: {
      type: Boolean,
      default: false,
    },
    is_queued: {
      type: Boolean,
      default: false,
    },
    customer_drop: {
      type: Boolean,
      default: false,
    },
    customer_disconnected: {
      type: Boolean,
      default: false,
    },
    ischatbot: {
      type: Boolean,
      default: false,
    },
    direction: { type: String },
    lastmessageUpdatedat: { type: Date },
    pg_sessionId: { type: String },
    category:{type:String},
    sentiment:{type:String},
    ticketID:{type:String},
   ticket_autoID:{type:String},
   ticket_flag:{type:Boolean,default:false},
   email_subject:{type:String}
  },

  {
    timestamps: true,
  }
);

schema.set("toJSON", {
  virtuals: true,

  versionKey: false,

  transform: function (doc, ret) {
    // remove these props when object is serialized

    delete ret._id;
  },
});

module.exports = mongoose.model("Session", schema);
