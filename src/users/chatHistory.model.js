const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const schema = new Schema(
  {
    
    agent_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
    },
    destinationagent_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
    },
    user_id: {
      type: String,
    },
    time: {
      type: Date,
    },
    session_id: {
      type: String,
    },
    type:{
      type:String
    }
  }
);

schema.set("toJSON", {
  virtuals: true,

  versionKey: false,

  transform: function (doc, ret) {
    // remove these props when object is serialized

    delete ret._id;
  },
});

module.exports = mongoose.model("chatHistory", schema);