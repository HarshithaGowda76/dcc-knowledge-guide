const db = require("../../_helper/db");
const requestIp = require("request-ip");
const axios = require("axios");
const config_file = require("../../config.json");
const io = require("socket.io-client");
const { default: mongoose } = require("mongoose");
let socket = io();
const { redisconfig } = require("../../_helper/redis.config");
var logger = require("../../_helper/logger");
let redisClient;
(async () => {
  redisClient = redisconfig();

  redisClient.on("error", (error) => {
    lastredisclient = undefined;
    console.error(`Error : ${error}`);
  });
  redisClient.on("connect", async (data) => {
    console.error(`Redis Started`);
    lastredisclient = "Connected";
  });
  await redisClient.connect();
})();
module.exports = {
    usercreation,
    sessioncreation,
  };
  function appendCountryCode(number) {
    console.log("number",number)
    if (number.startsWith("0091")) {
      console.log("number",number)
      const sanitizedNumber = number.replace(/^0091+/, "");
       console.log(sanitizedNumber)  
      return sanitizedNumber;
  
    }else if(number.startsWith("00230")){
      const sanitizedNumber = number.replace(/^00230/, "");
      return sanitizedNumber;
    }
  
  
    // Number does not require any changes
  else{
    return number;
  }
  }
  
async function usercreation(req, res, next) {
    try {
      let body = req.body;
      var { name, email_id, phone } = req.body;
      console.log(typeof phone)
      phone = appendCountryCode(phone);
      console.log("phone",phone)
      let check_user_old = await db.Client.findOne(
          {
            phonenumber: phone,
          }
        
      );
  
      if (check_user_old) {
        req.body.customer_id = check_user_old.id;
        req.body.name = check_user_old.username;
        req.body.email_id = check_user_old.email;
        req.body.phonenumber = check_user_old.phonenumber;
        req.body.client = check_user_old;
        next();
      } else {
        var result = await new db.Client({
          username: name,
          email: email_id,
          phonenumber: phone,
        }).save();
        req.body.customer_id = result.id;
        req.body.name = name;
        req.body.email_id = email_id;
        req.body.phonenumber = phone;
        req.body.client = result;
  
        next();
      }
    } catch (error) {
      // next(error);
      logger.error("CreateSession:Unable to create session please try after sometime",error)
      res.json({
        status: false,
        msg: "Unable to create session please try after sometime",
        data: error,
      });
      return;
    }
  }
  
  async function sessioncreation(req, res, next) {
    try {
      
      var client_ip = requestIp.getClientIp(req);
      var user_agent = req.useragent;
      var tenantId=req.headers.tenantid;
      console.log(req.headers)
      console.log(tenantId)
      let check_user_active = await db.Session.findOne({
        unique_id: req.body.customer_id,
        chat_type: "external",
        status: {
          $in: ["newjoin", "Accept"],
        },
      });
  
     
      if (check_user_active) {
        logger.error("CreateSession:Session Already availble",check_user_active)
        res.json({
          status: false,
          msg: "Session already available",
          data: check_user_active,
        });
        return;
        // next();
      } else {
        var chat_session_id = "session" + Math.random().toString(16).slice(2);
        var session = await new db.Session({
          phonenumber: req.body.phonenumber,
          username: req.body.name,
          email: req.body.email_id,
          channel: "voice",
          chat_session_id: chat_session_id,
          unique_id: req.body.customer_id,
          arrival_at: new Date(),
          chat_started_at: "",
          chat_ended_at: "",
          skillset: req.body.skillset,
          language: req.body.language,
          complaint: req.body.complaint,
          lattitude: req.body.latitude,
          longitude: req.body.longitute,
          client_ip,
          whatsapp_msg_id: req.body.whatsapp_msg_id,
          user_agent: user_agent,
          available_agent: req.body.agent_id,
          agent: req.body.agent_id,
          status: "Accept",
          chat_type: "external",
          chat_started_at: new Date(),
          is_queued: false,
          direction:req.body.direction
        }).save();
  
        req.body.chat_session_id = chat_session_id;
        req.body.session_id = session._id;
  
       // logger.info(req.body.chat_session_id+":CreateSession:chat session created successsfully",session)
       // req.body.availableAgent = "03bbcd2f-448b-4c55-8a95-99502c166955";
  
    
        let findAgent = await db.User.find({
          _id: mongoose.Types.ObjectId(req.body.agent_id),
        });
       console.log(findAgent[0])
        if (findAgent[0] != undefined) {
          var updateresult = await db.Session.findByIdAndUpdate(
            req.body.session_id,
            {
              $set: {
                user_id: findAgent[0].user_id,
               
              },
            }
          ).populate("unique_id", "username email phonenumber");
  
          if (updateresult) {
            let incomingreq = await db.Session.find({
              chat_session_id: req.body.chat_session_id,
            }).populate("unique_id", "username email phonenumber");
            let agent_id_incoming = JSON.stringify(findAgent[0]._id);
  
            if (redisClient != undefined) {
              var v = await redisClient.hSet(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0]))
                    console.log("value", v)
              var value = await redisClient.get(agent_id_incoming);
  
              let cachedrequestlist = JSON.parse(value);
  
              if (cachedrequestlist) {
                cachedrequestlist = [...cachedrequestlist, incomingreq[0]];
              } else {
                cachedrequestlist = [incomingreq[0]];
              }
             
              redisClient.set(
                agent_id_incoming,
                JSON.stringify(cachedrequestlist)
              );
              const cacheResults = await redisClient.get("agentsDetails");
              let cache_resp = JSON.parse(cacheResults);
              if (cache_resp) {
                var agentStatusnew = [];
                cache_resp.forEach(async (item) => {
                  if (item["userId"] == findAgent[0].user_id) {
                    item["chat"].active_chat_count =
                      item["chat"].active_chat_count + 1;
                    item["chat"].total_ongoing = item["chat"].total_ongoing + 1;
                    item["chat"].last_chat_start_time = new Date();
    
                    agentStatusnew = item;
                  }
                });
            }
          }
          //tenantId="a3dc14bd-fe70-4120-8572-461b0dc866b5"
          socket = io(config_file.socketUrl+tenantId);

          socket.emit("send-new-callreq", incomingreq[0]);
            var agentActivity=await db.agentActivity({agent_id:findAgent[0]._id,user_id:req.body.availableAgent,activity_name:"USER_ACCEPT_REQUEST",value:req.body.chat_session_id,created_at:new Date()}).save();
            logger.info(req.body.chat_session_id+":CreateSession:chat session created ",incomingreq)
            var incoming_data=
            {
        agent_id:findAgent[0]._id,
         Intraction_id:req.body.chat_session_id,
         channel_id:"2",
         Ring_time:"10",
         skillset:"",
         customer_id:incomingreq[0].unique_id.id,
         language:"",
         In_conference:"true",
         conference_parties:"supervisor",
         Recording_In_progress:"true",
         after_call_work:"true",
         Is_handling_transer:"true",
         agent_extension:req.body.agent_extension?req.body.agent_extension:"6002",
         Customer_Contact:req.body.phonenumber,
         Originator_time:incomingreq[0].arrival_at,
         Customer_Contact_email:"",
         call_type:"Inbound",
         call_result:"Answered",
         start_time:new Date(),
         Intraction_mode_id:incomingreq[0].channel,
         interaction_disconnected:false
          }
     console.log("incoming",incoming_data)
     const config1 = {
     headers: { TenantID: tenantId },
     };
     console.log("tenant",config1,"file",config_file.interactionApi)
     await axios.post(
     config_file.interactionApi+"liveReport/createLiveReport",
     incoming_data,
     config1
     );
            res.json({
              status: true,
              message: "chat session created successfully",
              data: incomingreq,
              chat_session_id:  req.body.chat_session_id,
              UserId: req.body.availableAgent,
              chat_inititaed_at:new Date()
            });
          
            // console.log(req.body);
            return;
          }
        }
      }
    } catch (error) {
      logger.error("CreateSession:Unable to create session please try after sometime",error)
      res.json({
        status: false,
        msg: "Unable to create session please try after sometime",
        data: error,
      });
      return;
    }
  }