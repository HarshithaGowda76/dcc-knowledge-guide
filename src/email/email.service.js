const db = require("../../_helper/db");
const requestIp = require("request-ip");
const config = require("../../config.json");
const axios = require("axios");
var logger = require("../../_helper/logger");
const io = require("socket.io-client");
let socket = io();
const { redisconfig } = require("../../_helper/redis.config");

let redisClient;
(async () => {
  redisClient = redisconfig();

  redisClient.on("error", (error) => {
    lastredisclient = undefined;
    console.error(`Error : ${error}`);
  });
  redisClient.on("connect", async (data) => {
    console.error(`Redis Started`);
    lastredisclient = "Connected";
    // await redisClient.HSET('myHash', '4', JSON.stringify({ id: 4, name: 'Bob' }), (err, result) => { if (err) { console.error(err); return; } console.log(result);}) // logs: 1 })
  });
  await redisClient.connect();
})();

module.exports = {
  emailUserCreation,
  emailSessionUpdate,
};

async function emailUserCreation(req, res, next) {
  try {
    console.log("Hi");
    var { name, email_id, agentID, emailId } = req.body;
    let check_user_old = await db.Client.findOne({
      email: email_id,
    });
    console.log("check_user_old", check_user_old);
    if (check_user_old) {
      if (check_user_old.email == "" || check_user_old.username == "") {
        var clientname = await db.Client.findByIdAndUpdate(
          { _id: check_user_old.id },
          {
            username: name,
            email: email_id,
          },
          { new: true }
        );
        req.body.name = clientname.username;
        req.body.email_id = clientname.email;
        console.log("clientname", clientname);
      } else {
        req.body.name = check_user_old.username;
        req.body.email_id = check_user_old.email;
      }
      req.body.customer_id = check_user_old.id;
      req.body.phonenumber = check_user_old.phonenumber;
      req.body.client = check_user_old;
      //   next();
    } else {
      var result = await new db.Client({
        username: name,
        email: email_id,
        phonenumber: "",
      }).save();
      console.log("result", result);
      req.body.customer_id = result.id;
      req.body.name = name;
      // req.body.email_id = email_id;
      // req.body.phonenumber = phone ? phone : "";
      req.body.client = result;
      // res.json({ data: result });
      //   next();
      console.log("result1", result);
    }
    console.log("result", result);
    var client_ip = requestIp.getClientIp(req);
    console.log("clientip", client_ip);
    var user_agent = req.useragent;
    console.log("user_agent", user_agent);
    let check_user_active = await db.Session.findOne({
      unique_id: req.body.customer_id,
      chat_type: "external",
      status: {
        $in: ["newjoin", "Accept"],
      },email_subject:req.body.subject
    });
    console.log("check_user_active", check_user_active);
    if (check_user_active) {
      logger.error("CreateSession:Session Already availble", check_user_active);
      res.json({
        status: false,
        msg: "Session already available",
        data: check_user_active,
      });
      return;
      // next();
    } else {
      var ticketdetails=await db.Session.findOne({ticketID:req.body.ticketID,ticket_flag:false})
      if(ticketdetails){
        var user = await db.User.findOne({ user_id: agentID });
        console.log("user", user);
        var chat_session_id = "session" + Math.random().toString(16).slice(2);
        var session = await new db.Session({
          phonenumber: req.body.phonenumber ? req.body.phonenumber : "",
          username: req.body.name ? req.body.name : "",
          email: req.body.email_id,
          channel: "email",
          chat_session_id: chat_session_id,
          unique_id: req.body.customer_id,
          arrival_at: new Date(),
          chat_started_at: "",
          chat_ended_at: "",
          skillset: "Customer Service",
          language: "English",
          status:"Accept",
          complaint: req.body.complaint,
          lattitude: req.body.latitude,
          longitude: req.body.longitute,
          client_ip,
          whatsapp_msg_id: req.body.whatsapp_msg_id,
          user_agent: user_agent,
          user_id: agentID,
          available_agent: user._id,
          agent: user._id,
          emailId: emailId,
          category:req.body.category?req.body.category:"",
          sentiment:req.body.sentiment?req.body.sentiment:"",
          ticketID: req.body.ticketID?req.body.ticketID:"",
          ticket_autoID: req.body.ticket_autoID?req.body.ticket_autoID:"",
          ticket_flag:false,
          email_subject:req.body.subject
        }).save();
        console.log("session", session);
        req.body.chat_session_id = chat_session_id;
        req.body.session_id = session._id;
        let agent_id_incoming = JSON.stringify(user._id);
        let incomingreq = await db.Session.find({
          chat_session_id: req.body.chat_session_id,
        }).populate("unique_id", "username email phonenumber");
        if (redisClient != undefined) {
          // var updateuser = await db.User.findByIdAndUpdate(
          //   { _id: user._id },
          //   { $inc: { queued_count: 1 } }
          // );
          var haongoingCount=await redisClient.HINCRBY(JSON.stringify("emailactivechatcount"), JSON.stringify(user._id), 1)
          // var havalue = await redisClient.HINCRBY(JSON.stringify("emailqueuecount"), JSON.stringify(user._id), 1)
          console.log("agent_id", JSON.stringify(agent_id_incoming));
          var v = await redisClient.hSet(
            JSON.stringify(agent_id_incoming),
            JSON.stringify(session._id),
            JSON.stringify(incomingreq[0])
          );
          console.log("value", v);
          const cacheResults = await redisClient.get("agentsDetails");
          let cache_resp = JSON.parse(cacheResults);
          if (cache_resp) {
            var agentStatusnew = [];
            cache_resp.forEach(async (item) => {
              if (item["userId"] == agentID) {
                item["chat"].queued_count = item["chat"].queued_count + 1;
               
  
                agentStatusnew = item;
              }
            });
            await redisClient.set("agentsDetails", JSON.stringify(cache_resp));
          }
  
          var v = await redisClient.hSet(
            JSON.stringify(agent_id_incoming),
            JSON.stringify(session._id),
            JSON.stringify(session)
          );
          console.log("value", v);
          logger.info(
            req.body.chat_session_id + ":CreateSession" + JSON.stringify(session)
          );
        }
        console.log("session", session);
       let tenantId = "a3dc14bd-fe70-4120-8572-461b0dc866b5";
        socket = io(config.socketUrl + tenantId);
        socket.emit("send-new-req", session);
        var incoming_data =
        {
          agent_id: user._id,
          Intraction_id: req.body.chat_session_id,
          channel_id: "5",
          Ring_time: "10",
          skillset: session.skillset,
          customer_id:session.unique_id,
          language:session.language,
          In_conference: "true",
          conference_parties: "supervisor",
          Recording_In_progress: "true",
          Is_handling_transer: "true",
          agent_extension: user.email,
          Chat_type: "Inbound",
          Chat_result:"Answered",
          GroupId:user.group_array[0]?user.group_array[0]:"31188316-f485-4e02-9987-794ba938fef3",
          Customer_Contact:session.email,
          start_time:new Date(),
          Originator_time:session.arrival_at,
          Intraction_mode_id:session.channel,
          to_email:req.body.to_email?req.body.to_email:session.email,
          subject:req.body.subject?req.body.subject:"", 
          interaction_disconnected: false
          
        }
        console.log("incoming", incoming_data)
        const config1 = {
          headers: { TenantID: tenantId },
        };
        await axios.post(
          config.interactionApi + "liveReport/createLiveReport",
          incoming_data,
          config1
        );
        res.json({ message: "response sent successfully" });
      }
      else{
      var user = await db.User.findOne({ user_id: agentID });
      console.log("user", user);
      var chat_session_id = "session" + Math.random().toString(16).slice(2);
      var session = await new db.Session({
        phonenumber: req.body.phonenumber ? req.body.phonenumber : "",
        username: req.body.name ? req.body.name : "",
        email: req.body.email_id,
        channel: "email",
        chat_session_id: chat_session_id,
        unique_id: req.body.customer_id,
        arrival_at: new Date(),
        chat_started_at: "",
        chat_ended_at: "",
        skillset: "Customer Service",
        language: "English",
        complaint: req.body.complaint,
        lattitude: req.body.latitude,
        longitude: req.body.longitute,
        client_ip,
        whatsapp_msg_id: req.body.whatsapp_msg_id,
        user_agent: user_agent,
        user_id: agentID,
        available_agent: user._id,
        agent: user._id,
        emailId: emailId,
        category:req.body.category?req.body.category:"",
        sentiment:req.body.sentiment?req.body.sentiment:"",
        ticketID: req.body.ticketID?req.body.ticketID:"",
        ticket_autoID: req.body.ticket_autoID?req.body.ticket_autoID:"",
        ticket_flag:false,
        email_subject:req.body.subject
      }).save();
      console.log("session", session);
      req.body.chat_session_id = chat_session_id;
      req.body.session_id = session._id;
      let agent_id_incoming = JSON.stringify(user._id);
      let incomingreq = await db.Session.find({
        chat_session_id: req.body.chat_session_id,
      }).populate("unique_id", "username email phonenumber");
      if (redisClient != undefined) {
        var updateuser = await db.User.findByIdAndUpdate(
          { _id: user._id },
          { $inc: { queued_count: 1 } }
        );
        var havalue = await redisClient.HINCRBY(JSON.stringify("emailqueuecount"), JSON.stringify(user._id), 1)
        console.log("agent_id", JSON.stringify(agent_id_incoming));
        var v = await redisClient.hSet(
          JSON.stringify(agent_id_incoming),
          JSON.stringify(session._id),
          JSON.stringify(incomingreq[0])
        );
        console.log("value", v);
        const cacheResults = await redisClient.get("agentsDetails");
        let cache_resp = JSON.parse(cacheResults);
        if (cache_resp) {
          var agentStatusnew = [];
          cache_resp.forEach(async (item) => {
            if (item["userId"] == agentID) {
              item["chat"].queued_count = item["chat"].queued_count + 1;
             

              agentStatusnew = item;
            }
          });
          await redisClient.set("agentsDetails", JSON.stringify(cache_resp));
        }

        var v = await redisClient.hSet(
          JSON.stringify(agent_id_incoming),
          JSON.stringify(session._id),
          JSON.stringify(session)
        );
        console.log("value", v);
        logger.info(
          req.body.chat_session_id + ":CreateSession" + JSON.stringify(session)
        );
      }
      console.log("session", session);
     let tenantId = "a3dc14bd-fe70-4120-8572-461b0dc866b5";
      socket = io(config.socketUrl + tenantId);
      socket.emit("send-new-req", session);
      var incoming_data =
      {
        agent_id: user._id,
        Intraction_id: req.body.chat_session_id,
        channel_id: "5",
        Ring_time: "10",
        skillset: session.skillset,
        customer_id:session.unique_id,
        language:session.language,
        In_conference: "true",
        conference_parties: "supervisor",
        Recording_In_progress: "true",
        Is_handling_transer: "true",
        agent_extension: user.email,
        Chat_type: "Inbound",
        Chat_result:"Answered",
        GroupId:user.group_array[0]?user.group_array[0]:"31188316-f485-4e02-9987-794ba938fef3",
        Customer_Contact:session.email,
        start_time:new Date(),
        Originator_time:session.arrival_at,
        Intraction_mode_id:session.channel,
        to_email:req.body.to_email?req.body.to_email:session.email,
        subject:req.body.subject?req.body.subject:"", 
        interaction_disconnected: false
        
      }
      console.log("incoming", incoming_data)
      const config1 = {
        headers: { TenantID: tenantId },
      };
      await axios.post(
        config.interactionApi + "liveReport/createLiveReport",
        incoming_data,
        config1
      );
      res.json({ message: "response sent successfully" });
    }
    }
  } catch (error) {
    // next(error);
  }
}

async function emailSessionUpdate(req, res, next) {
  try {
    console.log("inside session update");
    var { session_id, agentID, emailId } = req.body;
    var user = await db.User.findOne({ user_id: agentID });
    console.log("User", user);
    var session=await db.Session.findOne({ $and: [{ _id: session_id }, { status: "newjoin" }]})
    var updateSession = await db.Session.findOneAndUpdate(
      { $and: [{ _id: session_id }, { status: "newjoin" }] },
      {
        $set: {
          user_id: agentID,
          available_agent: user._id,
          agent: user._id,
        },
      }, { new: true }
    );
    if (redisClient != undefined) {
      var updateuser = await db.User.findByIdAndUpdate(
        { _id: user._id },
        { $inc: { queued_count: 1 } }
      );
      var agents = JSON.stringify(session.agent);
      var havalue = await redisClient.HINCRBY(JSON.stringify("emailqueuecount"), JSON.stringify(user._id), 1)
      var havalues = await redisClient.HINCRBY(JSON.stringify("emailqueuecount"), JSON.stringify(session.agent), -1)
      var respons = await redisClient.hDel(JSON.stringify(agents), JSON.stringify(session._id))
      console.log(respons)
      var agent_id_incoming=JSON.stringify(user._id)
      //console.log("agent_id", JSON.stringify(agent_id_incoming));
      var v = await redisClient.hSet(
        JSON.stringify(agent_id_incoming),
        JSON.stringify(session_id),
        JSON.stringify(updateSession)
      );
      console.log("value", v);
      const cacheResults = await redisClient.get("agentsDetails");
      let cache_resp = JSON.parse(cacheResults);
      if (cache_resp) {
        var agentStatusnew = [];
        cache_resp.forEach(async (item) => {
          if (item["userId"] == agentID) {
            item["chat"].queued_count = item["chat"].queued_count + 1;
            logger.info(
              findAgent[0]._id +
                ":Create Session Tested Queued Count" +
                item["chat"].queued_count
            );
            logger.info(
              findAgent[0]._id +
                ":Create Session Tested active chat count" +
                item["chat"].active_chat_count
            );
            logger.info(
              findAgent[0]._id +
                ":Create Session Tested Total Ongoing" +
                item["chat"].total_ongoing
            );

            agentStatusnew = item;
          }
          if(item["userId"]==session.user_id){
            item["chat"].queued_count= item["chat"].queued_count >0?item["chat"].queued_count - 1:0;
          }
        });
        await redisClient.set("agentsDetails", JSON.stringify(cache_resp));
      }
      var v = await redisClient.hSet(
        JSON.stringify(agent_id_incoming),
        JSON.stringify(session._id),
        JSON.stringify(session)
      );
      console.log("value", v);
      logger.info(
        req.body.chat_session_id + ":CreateSession" + JSON.stringify(v)
      );
    }
    console.log("session", updateSession);
    tenantId = "a3dc14bd-fe70-4120-8572-461b0dc866b5";
    socket = io(config.socketUrl + tenantId);

    socket.emit("send-new-req", updateSession);
    res.json({ message: "response sent successfully" });
  } catch (error) {}
}
