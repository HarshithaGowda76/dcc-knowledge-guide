const express = require("express");
const router = express.Router();

const emailService = require("./email.service");

router.post("/emailUserCreation", emailService.emailUserCreation);
router.post("/emailSessionUpdate", emailService.emailSessionUpdate);

module.exports = router;
