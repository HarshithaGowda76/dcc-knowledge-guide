const errorHandler = require("../../_helper/error.handler");
const db = require("../../_helper/db");
var logger = require("../../_helper/logger");
const { default: mongoose } = require("mongoose");
module.exports = {
  createsocialmedia,
  updatesocialmedia,
  listSocialmedia,
  listSocialmediaTenantid,
};

async function createsocialmedia(req, res, next) {
  try {
    var { type, channelinfo } = req.body;
    var err = {};
    var variable = JSON.stringify(req.headers);
    var variable1 = JSON.parse(variable);
    console.log(variable1.tenantid);
    console.log(channelinfo);
    var tenantId = variable1.tenantid;
    if (!tenantId) {
      err.message = "TenantId is missing";
      err.status = 400;
    }
    if (!type) {
      err.message = "Type is missing";
      err.status = 400;
    }
    var socialvalue = await db.SocialMedia.findOne({
      type: type,
      tenantId: tenantId,
    });
    if (socialvalue) {
      err.message = "Channel already exist for the tenanatId";
      err.status = 400;
    }
    if (err.message) return errorHandler(err, req, res, next);
    if (type == "Whatsapp") {
      var result = await db
        .SocialMedia({
          type: type,
          created_at: new Date(),
          channelinfo: channelinfo,
          tenantId: tenantId,
        })
        .save();
    } else if (type == "Facebook") {
      var result = await db
        .SocialMedia({
          type: type,
          created_at: new Date(),
          channelinfo: channelinfo,
          tenantId: tenantId,
        })
        .save();
    } else if (type == "Twitter") {
      var result = await db
        .SocialMedia({
          type: type,
          created_at: new Date(),
          channelinfo: channelinfo,
          tenantId: tenantId,
        })
        .save();
    } else if (type == "Teams") {
      var result = await db
        .SocialMedia({
          type: type,
          created_at: new Date(),
          channelinfo: channelinfo,
          tenantId: tenantId,
        })
        .save();
    }
    if (result) {
      res.json({
        status: true,
        message: "Value Added Successfully",
        data: result,
      });
    } else {
      res.json({ status: false, message: "No data inserted" });
    }
  } catch (e) {
    next(e);
  }
}
async function updatesocialmedia(req, res, next) {
  try {
    var { id, channelinfo } = req.body;
    var err = {};
    var variable = JSON.stringify(req.headers);
    var variable1 = JSON.parse(variable);
    console.log(variable1.tenantid);
    console.log(channelinfo);
    var tenantId = variable1.tenantid;
    if (!tenantId) {
      err.message = "TenantId is missing";
      err.status = 400;
    }
    if (err.message) return errorHandler(err, req, res, next);
    var result = await db.SocialMedia.findByIdAndUpdate(
      { _id: mongoose.Types.ObjectId(id) },
      { channelinfo: channelinfo },
      { new: true }
    );
    console.log(result);
    if (result) {
      res.json({
        status: true,
        message: "Details updated Successfully",
        data: result,
      });
    } else {
      res.json({ status: false, message: "No data updated" });
    }
  } catch (e) {
    next(e);
  }
}
async function listSocialmedia(req, res, next) {
  try {
    var { offset, limit } = req.body;
    var result = await db.SocialMedia.find().skip(offset).limit(limit);
    const count = await db.SocialMedia.countDocuments();
    if (result.length > 0) {
      res.json({
        status: true,
        message: "Details Fetched Successfully",
        data: result,
        count: count,
      });
    } else {
      res.json({ status: false, message: "No data found" });
    }
  } catch (e) {
    next(e);
  }
}
async function listSocialmediaTenantid(req, res, next) {
  try {
    var { offset, limit } = req.body;
    var err = {};
    var variable = JSON.stringify(req.headers);
    var variable1 = JSON.parse(variable);
    console.log(variable1.tenantid);
    var tenantId = variable1.tenantid;
    if (!tenantId) {
      err.message = "TenantId is missing";
      err.status = 400;
    }
    if (err.message) return errorHandler(err, req, res, next);
    var result = await db.SocialMedia.find({tenantId:tenantId}).skip(offset).limit(limit);
    const count = await db.SocialMedia.countDocuments({tenantId:tenantId});
    if (result.length > 0) {
      res.json({
        status: true,
        message: "Details Fetched Successfully",
        data: result,
        count: count,
      });
    } else {
      res.json({ status: false, message: "No data found" });
    }
  } catch (e) {
    next(e);
  }
}
