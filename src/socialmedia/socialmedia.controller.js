const express = require("express");
const router = express.Router();
const authorize = require("../../_middleware/authorize");
const socialmedia = require("./socialmedia.service");

 router.post("/createsocialmedia", socialmedia.createsocialmedia);
 router.post("/updatesocialmedia", socialmedia.updatesocialmedia);
 router.post("/listsocialmedia", socialmedia.listSocialmedia);
 router.post("/listsocialmediatenantId", socialmedia.listSocialmediaTenantid);
module.exports = router;