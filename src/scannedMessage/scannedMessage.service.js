const errorHandler = require("../../_helper/error.handler");
const db = require("../../_helper/db");
var logger = require("../../_helper/logger");

module.exports = {
  createScannedMessage,
  listScannedMessage
};

// API to create ScannedMessage

async function createScannedMessage(req, res, next) {
    try{
        const { message} = req.body;
        var err = {};
            if (!message) {
              err.message = "Message is missing";
              err.status = 400;
            }
            if (err.message) return errorHandler(err, req, res, next);
            const scannedmessage = await new db.scannedMessage({
                message,created_at:new Date()
              }).save();
              logger.info(scannedmessage.id+":CreateScannedMessage:Scanned message created Successfully"+scannedmessage)
            res.json({ status: true, message: "Value Added Successfully", scannedmessage });
        
    }
    catch(e){
        next(e)
    }
}
async function listScannedMessage(req,res,next){
    try{
        const { offset,limit} = req.body;
        const scannedmessage = await db.scannedMessage
      .find()
      .sort({ created_at: -1 })
      .skip(offset)
      .limit(limit);
    const count = await db.scannedMessage.countDocuments();
    if (scannedmessage.length > 0) {
      logger.info(scannedmessage[0]._id+":ListScannedMessage:Scanned message listed "+scannedmessage)
      res.json({
        status: true,
        message: "Details Fetched Successfully",
        data: scannedmessage,
        count: count,
      });
    } else {
      logger.error("ListScannedMessage:No data found","")
      res.json({ status: false, message: "No data found" });
    }
    }
    catch(e){
      logger.error("ListScannedMessage:Error occured"+e)
      res.json({ status: false, message: "No data found" });
        //next(e)
    }
}