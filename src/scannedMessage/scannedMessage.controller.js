const express = require("express");
const router = express.Router();
const authorize = require("../../_middleware/authorize");
const scannedMessage = require("./scannedMessage.service");

 router.post("/createMessage", scannedMessage.createScannedMessage);
 router.post("/listMessage", scannedMessage.listScannedMessage);

module.exports = router;