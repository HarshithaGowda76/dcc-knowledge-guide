const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const schema = new Schema({
  message: { type: String },
  created_at: { type: Date },
  
});

schema.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    // remove these props when object is serialized
    delete ret._id;
  },
});

module.exports = mongoose.model("ScannedMessage", schema);