const express = require("express");
const router = express.Router();
const authorize = require("../../_middleware/authorize");
const skillsetlist = require("./skillSet.service");

 router.post("/skillsetlist", skillsetlist.skillsetlist);

module.exports = router;