const express = require("express");
const router = express.Router();
const authorize = require("../../_middleware/authorize");
const userstatuslist = require("./status.service");

 router.post("/userstatuslist", userstatuslist.userstatuslist);

module.exports = router;