const express = require("express");
const router = express.Router();
const authorize = require("../../_middleware/authorize");
const customerportal = require("./customerportal.service");
const Routingservice = require("./userservice");
router.post(
    "/createsession",authorize(),
    Routingservice.usercreation,
    Routingservice.sessioncreation,
    Routingservice.RoutingApiNew
  );
module.exports = router;