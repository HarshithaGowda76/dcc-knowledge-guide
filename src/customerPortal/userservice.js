const db = require("../../_helper/db");
const requestIp = require("request-ip");
const axios = require("axios");
const config = require("../../config.json");
const io = require("socket.io-client");
let socket = io();
const { redisconfig } = require("../../_helper/redis.config");
var logger = require("../../_helper/logger");
let redisClient;
(async () => {
  redisClient = redisconfig();

  redisClient.on("error", (error) => {
    lastredisclient = undefined;
    console.error(`Error : ${error}`);
  });
  redisClient.on("connect", async (data) => {
    console.error(`Redis Started`);
    lastredisclient = "Connected";
   // await redisClient.HSET('myHash', '4', JSON.stringify({ id: 4, name: 'Bob' }), (err, result) => { if (err) { console.error(err); return; } console.log(result);}) // logs: 1 })
  });
  await redisClient.connect();
})();
module.exports = {
  usercreation,
  sessioncreation,
  RoutingApiNew,
};
async function usercreation(req, res, next) {
  try {
    let body = req.body;
    var name=body.payload.sender.sender_name,
    email_id=body.payload.sender.sender_email,
    phone=body.payload.sender.sender_number
    //var { name, email_id, phone } = req.body;
    let check_user_old = await db.Client.findOne({
      $or: [
        {
          email:email_id,
        },
        {
          phonenumber: phone,
        },
      ],
    });

    if (check_user_old) {
      if(check_user_old.email==""||check_user_old.username==""){
        var clientname=  await db.Client.findByIdAndUpdate( { _id: check_user_old.id },
            {
              username:name,
              email:email_id
            },
            { new: true })
            req.body.name = clientname.username;
            req.body.email_id = clientname.email;
          }
          else{
            req.body.name = check_user_old.username;
            req.body.email_id = check_user_old.email;
          }
          req.body.customer_id = check_user_old.id;
          req.body.phonenumber = check_user_old.phonenumber;
          req.body.client = check_user_old;
      next();
    } else {
      var result = await new db.Client({
        username: name,
        email: email_id,
        phonenumber: phone,
      }).save();
      req.body.customer_id = result.id;
      req.body.name = name;
      req.body.email_id = email_id;
      req.body.phonenumber = phone;
      req.body.client = result;

      next();
    }
  } catch (error) {
    // next(error);
    logger.error("CreateSession:Unable to create session please try after sometime",error)
    res.json({
      status: false,
      msg: "Unable to create session please try after sometime",
      data: error,
    });
    return;
  }
}

async function sessioncreation(req, res, next) {
  try {
    
    var client_ip = requestIp.getClientIp(req);
    var user_agent = req.useragent;

    let check_user_active = await db.Session.findOne({
      unique_id: req.body.customer_id,
      chat_type: "external",
      status: {
        $in: ["newjoin", "Accept"],
      },
    });

    if (check_user_active) {
      logger.error("CreateSession:Session Already availble",check_user_active)
      res.json({
        status: false,
        msg: "Session already available",
        data: check_user_active,
      });
      return;
      // next();
    } else {
      var chat_session_id = "session" + Math.random().toString(16).slice(2);
      var session = await new db.Session({
        phonenumber: req.body.phonenumber,
        username: req.body.name,
        email: req.body.email_id,
        channel: "from_purple",
        pg_sessionId:req.body.pg_sessionId,
        chat_session_id: chat_session_id,
        unique_id: req.body.customer_id,
        arrival_at: new Date(),
        chat_started_at: "",
        chat_ended_at: "",
        skillset: req.body.payload.customFields_2.value,
        language: req.body.payload.customFields.value,
        complaint: req.body.complaint,
        lattitude: "23°27N",
        longitude: "23.5",
        client_ip,
        whatsapp_msg_id: req.body.whatsapp_msg_id? req.body.whatsapp_msg_id:"",
        user_agent: user_agent,
      }).save();
      req.body.channel="from_purple";
      req.body.chat_session_id = chat_session_id;
      req.body.session_id = session._id;

      next();
    }
  } catch (error) {
    logger.error("CreateSession:Unable to create session please try after sometime",error)
    res.json({
      status: false,
      msg: "Unable to create session please try after sometime",
      data: error,
    });
    return;
  }
}
async function RoutingApiNew(req, res, next) {
  try {
    let data = {
      phoneNumber: req.body.phone,
      serviceRequestType: "",
      customerLanguage: req.body.payload.customFields.value,
      customerLanguageCode: "",
      LastHandledAgentID: "",
      routingRuleId: "",
      skillSet: req.body.payload.customFields_2.value,
      chat_session_id: req.body.chat_session_id,
    };
    // var  head = {
    //   headers: { TenantID: config_file.TenantID },
    // };
    // var value = {enabled:true};
    console.log("hi")
//var tenantId=req.headers.tenantId;
// var variable=JSON.stringify(req.headers)
//     var variable1=JSON.parse(variable)
//     console.log(variable1.tenantid);
    
    var tenantId="a3dc14bd-fe70-4120-8572-461b0dc866b5";
//console.log(queuedetails.data.data.queueMessage);
if(config.route!="Bot"){
if(config.env=="Production"){
    await axios.post(config.routingbaseurl+"/routingengine/engine/bestRoutev1", data).then(async (resp) => {
      if (resp.data.status == 1001) {
        if (resp.data.value != null) {
          // if (req.body.channel == "from_whatsapp" || req.body.channel == "from_facebook" || req.body.channel == "from_twitter") {
          //   req.body.availableAgent = "03bbcd2f-448b-4c55-8a95-99502c166955";
          // } else {
            req.body.availableAgent = resp.data.value.userId;
          // }
        
          let findAgent = await db.User.find({
            user_id: req.body.availableAgent,
          });

          if (findAgent[0] != undefined) {
            var updateresult = await db.Session.findByIdAndUpdate(
              req.body.session_id,
              {
                $set: {
                  user_id: req.body.availableAgent,
                  available_agent: findAgent[0]._id,
                  agent: findAgent[0]._id,
                },
              }
            ).populate("unique_id", "username email phonenumber");

            if (updateresult) {
              let incomingreq = await db.Session.find({
                chat_session_id: req.body.chat_session_id,
              }).populate("unique_id", "username email phonenumber");
              let agent_id_incoming = JSON.stringify(findAgent[0]._id);

              if (redisClient != undefined) {
               // var value = await redisClient.get(agent_id_incoming);
                // redisClient.hgetall(JSON.stringify(agent_id_incoming), (err, result) => {
                // if (err) {
                //   console.error(err);
                //    return;
                //    }
                  
                //   console.log(result); // logs an object with field-value pairs
                //   });
                // let cachedrequestlist = JSON.parse(value);

                // if (cachedrequestlist) {
                //   cachedrequestlist = [...cachedrequestlist, incomingreq[0]];
                // } else {
                //   cachedrequestlist = [incomingreq[0]];
                // }

                // redisClient.set(
                //   agent_id_incoming,
                //   JSON.stringify(cachedrequestlist)
                // );
               // redisClient.HSET('incoming'+agent_id, incomingreq[0]._id, JSON.stringify(incomingreq[0]), (err, result) => { 
                //   if (err) { console.error(err); return; } console.log(result);
                //  })// logs: 1 });
                var v= await redisClient.hSet(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0])
          //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
          )
          console.log("value",v)
                // await redisClient.HSET(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0]), (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);})
                logger.info(req.body.chat_session_id+":CreateSession"+JSON.stringify(cachedrequestlist))
              }
              var agentActivity=await db.agentActivity({agent_id:findAgent[0]._id,user_id:req.body.availableAgent,activity_name:"USER_INCOMING_REQUEST",value:req.body.chat_session_id,created_at:new Date()}).save();
              socket = io(config.socketUrl+tenantId);

              socket.emit("send-new-req", incomingreq[0]);

              if (req.body.channel != "from_purple") {
                await axios.post(
                  config.customurl+"/v1/message/addMessage",
                  {
                    from: req.body.chat_session_id,
                    to: findAgent[0]._id,
                    message: req.body.message,
                    senderName: req.body.name,
                    receiverName: findAgent[0].username,
                    messageFrom: "fromClient",
                    msg_sent_type:req.body.msg_sent_type
                  }
                );

                const data = await axios.post(
                  config.customurl+`/v1/users/getId/${req.body.email_id}`
                );
                if (data && data.length) {
                  socket = io(config.socketUrl+tenantId);
                  socket.emit("add-user", data.data.user.id);
                }
              }
              logger.info(req.body.chat_session_id+":CreateSession:chat session created successsfully",incomingreq)
              res.json({
                status: true,
                message: "chat session created successfully",
                error: false,
                data: {
                    session_key:req.body.chat_session_id,
                    channel:incomingreq[0].channel,
                    sender_number:incomingreq[0].phonenumber,
                    sender_name:req.body.name,
                    skillset_name:incomingreq[0].skillset
                },
              });

              // console.log(req.body);
              return;
            } else {
              logger.error("CreateSession:Agent not logged in","")
              res.json({
                status: false,
                msg: "Agent Not logged In",
                data: "",
              });
              return;
            }
          } else {
            logger.error("CreateSession:Unable to create session","")
            res.json({
              status: false,
              msg: "Unable to create session please try after sometime",
              data: "",
            });
            return;
          }
        } else {
          var queueMessage=queuedetails.data.data?queuedetails.data.data.queueMessage:"Please wait, your request is in queue";
          let incomingreq = await db.Session.find({
            chat_session_id: req.body.chat_session_id,
          }).populate("unique_id", "username email phonenumber");
          await db.Session.findOneAndUpdate({chat_session_id: req.body.chat_session_id},{$set:{is_queued:true}});
          await axios.post(
            config.customurl+"/v1/message/addMessage",
            {
              from: incomingreq[0]._id,
              to: incomingreq[0]._id,
              message:queueMessage,
              senderName:"Queue Message",
              receiverName: req.body.name,
              msg_sent_type:"TEXT",
              messageFrom: "fromAgent",
              userType:"external",
              file_name:"",
              chatdetails:incomingreq[0]
            }
          );
      
          const data = await axios.post(
            config.customurl+`/v1/users/getId/${req.body.email_id}`
          );
          if (data && data.length) {
            socket = io(config.socketUrl+tenantId);
            socket.emit("add-user", data.data.user.id);
          }logger.info(req.body.chat_session_id+":CreateSession:Session creation in queue",incomingreq)
          res.json({
            status: true,
            message: queueMessage,
            data: incomingreq,
            chat_session_id: req.body.chat_session_id,
            UserId: null,
            chat_inititaed_at:new Date()
          });
          return;
        }
      } else {
        var queuedetails=await axios.post(config.loginbaseurl+"/usermodule/clientMaster/chatConfig/list",{enabled:true}, {
          headers: { TenantID: tenantId },
        })
        var queueMessage=queuedetails.data.data?queuedetails.data.data.queueMessage:"Please wait, your request is in queue";
          let incomingreq = await db.Session.find({
            chat_session_id: req.body.chat_session_id,
          }).populate("unique_id", "username email phonenumber");
          await db.Session.findOneAndUpdate({chat_session_id: req.body.chat_session_id},{$set:{is_queued:true}});
          await axios.post(
            config.customurl+"/v1/message/addMessage",
            {
              from: incomingreq[0]._id,
              to: incomingreq[0]._id,
              message:queueMessage,
              senderName:"Queue Message",
              receiverName: req.body.name,
              msg_sent_type:"TEXT",
              messageFrom: "fromAgent",
              userType:"external",
              file_name:"",
              chatdetails:incomingreq[0]
            }
          );
      
          const data = await axios.post(
            config.customurl+`/v1/users/getId/${req.body.email_id}`
          );
          if (data && data.length) {
            socket = io(config.socketUrl+tenantId);
            socket.emit("add-user", data.data.user.id);

            socket.emit("send-msg", {
              to: incomingreq[0]._id,
              session_id: incomingreq[0].chat_session_id,
              from: incomingreq[0]._id,
              senderName:"Queue Message",
              chatType: "outbound",
              msg:queueMessage,
              msgType: "web",
              userType: "external",
              msg_sent_type: "TEXT",
              chatdetails: incomingreq[0],
              file_name: "",
            });
          }
          logger.info(req.body.chat_session_id+":CreateSession:Session in queue",incomingreq)
          res.json({
            status: true,
            message: "You are in queue please wait",
            data: incomingreq,
            chat_session_id: req.body.chat_session_id,
            UserId: null,
            chat_inititaed_at:new Date()
          });
          return;
      }
    });
  }else{
    req.body.availableAgent = "03bbcd2f-448b-4c55-8a95-99502c166955";
      
    
    let findAgent = await db.User.find({
      user_id: req.body.availableAgent,
    });

    if (findAgent[0] != undefined) {
      var updateresult = await db.Session.findByIdAndUpdate(
        req.body.session_id,
        {
          $set: {
            user_id: req.body.availableAgent,
            available_agent: findAgent[0]._id,
            agent: findAgent[0]._id,
          },
        }
      ).populate("unique_id", "username email phonenumber");

      if (updateresult) {
        let incomingreq = await db.Session.find({
          chat_session_id: req.body.chat_session_id,
        }).populate("unique_id", "username email phonenumber");
        let agent_id_incoming = JSON.stringify(findAgent[0]._id);
        console.log("inside")
        if (redisClient != undefined) {
          console.log("inside else")
          // var value = await redisClient.get(agent_id_incoming);

          // let cachedrequestlist = JSON.parse(value);

          // if (cachedrequestlist) {
          //   cachedrequestlist = [...cachedrequestlist, incomingreq[0]];
          // } else {
          //   cachedrequestlist = [incomingreq[0]];
          // }

          // redisClient.set(
          //   agent_id_incoming,
          //   JSON.stringify(cachedrequestlist)
          // );
          // redisClient.HSET('incoming'+agent_id, incomingreq[0]._id, JSON.stringify(incomingreq[0]), (err, result) => { 
          //   if (err) { console.error(err); return; } console.log(result);
          //  })
          console.log("agent_id",JSON.stringify(agent_id_incoming))
         var v= await redisClient.hSet(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0])
          //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
          )
          console.log("value",v)
          //var ss= await redisClient.hVals(JSON.stringify(agent_id_incoming))

          //ss=JSON.parse(ss);
          //console.log("redis",ss)
        }
        var agentActivity=await db.agentActivity({agent_id:findAgent[0]._id,user_id:req.body.availableAgent,activity_name:"USER_INCOMING_REQUEST",value:req.body.chat_session_id,created_at:new Date()}).save();
        socket = io(config.socketUrl+tenantId);

        socket.emit("send-new-req", incomingreq[0]);

        if (req.body.channel != "from_purple") {
          await axios.post(
            config.customurl+"/v1/message/addMessage",
            {
              from: req.body.session_id,
              to: findAgent[0]._id,
              message: req.body.message,
              senderName: req.body.name,
              receiverName: findAgent[0].username,
              messageFrom: "fromClient",
              msg_sent_type:req.body.msg_sent_type
            }
          );

          const data = await axios.post(
            config.customurl+`/v1/users/getId/${req.body.email_id}`
          );
          if (data && data.length) {
            socket = io(config.socketUrl+tenantId);
            socket.emit("add-user", data.data.user.id);
          }
        }
        logger.info(req.body.chat_session_id+":CreateSession:chat session created",incomingreq)
        res.json({
            status: true,
            message: "chat session created successfully",
            error: false,
            data: {
                session_key:req.body.chat_session_id,
                channel:incomingreq[0].channel,
                sender_number:incomingreq[0].phonenumber,
                sender_name:req.body.name,
                skillset_name:incomingreq[0].skillset
            },
          });
      
        // console.log(req.body);
        return;
      }
    }
  }
}else{
  let incomingreq = await db.Session.find({
    chat_session_id: req.body.chat_session_id,
  }).populate("unique_id", "username email phonenumber");
  res.json({
    status: true,
    message: "chat session created successfully",
    error: false,
    data: {
        session_key:req.body.chat_session_id,
        channel:incomingreq[0].channel,
        sender_number:incomingreq[0].phonenumber,
        sender_name:req.body.name,
        skillset_name:incomingreq[0].skillset
    },
  });
}
  } catch (error) {
    //  next(error);
    // res.json({
    //   status: false,
    //   msg: "Unable to create session please try after sometime",
    //   data: error,
    // });
    // return;
    // var queueMessage="Please wait, your request is in queue";
    // let incomingreq = await db.Session.find({
    //   chat_session_id: req.body.chat_session_id,
    // }).populate("unique_id", "username email phonenumber");
    // await axios.post(
    //   "config.customurl/v1/message/addMessage",
    //   {
    //     from: incomingreq[0]._id,
    //     to: incomingreq[0]._id,
    //     message:queueMessage,
    //     senderName:"Queue Message",
    //     receiverName: req.body.name,
    //     msg_sent_type:"TEXT",
    //     messageFrom: "fromAgent",
    //     userType:"external",
    //     file_name:"",
    //     chatdetails:incomingreq[0]
    //   }
    // );

    // const data = await axios.post(
    //   `config.customurl/v1/users/getId/${req.body.email_id}`
    // );
    // if (data && data.length) {
    //   socket = io(config.socketUrl);
    //   socket.emit("add-user", data.data.user.id);
    // }
    // res.json({
    //   status: true,
    //   message: "chat session created successfully",
    //   data: incomingreq,
    //   chat_session_id: req.body.chat_session_id,
    //   UserId: null,
    // });
        req.body.availableAgent = "03bbcd2f-448b-4c55-8a95-99502c166955";
      
    
      let findAgent = await db.User.find({
        user_id: req.body.availableAgent,
      });
        console.log("catch")
      if (findAgent[0] != undefined) {
        var updateresult = await db.Session.findByIdAndUpdate(
          req.body.session_id,
          {
            $set: {
              user_id: req.body.availableAgent,
              available_agent: findAgent[0]._id,
              agent: findAgent[0]._id,
            },
          }
        ).populate("unique_id", "username email phonenumber");

        if (updateresult) {
          let incomingreq = await db.Session.find({
            chat_session_id: req.body.chat_session_id,
          }).populate("unique_id", "username email phonenumber");
          let agent_id_incoming = JSON.stringify(findAgent[0]._id);

          if (redisClient != undefined) {
            // var value = await redisClient.get(agent_id_incoming);

            // let cachedrequestlist = JSON.parse(value);

            // if (cachedrequestlist) {
            //   cachedrequestlist = [...cachedrequestlist, incomingreq[0]];
            // } else {
            //   cachedrequestlist = [incomingreq[0]];
            // }

            // redisClient.set(
            //   agent_id_incoming,
            //   JSON.stringify(cachedrequestlist)
            // );
            // redisClient.HSET('incoming', incomingreq[0]._id, JSON.stringify(incomingreq[0]), (err, result) => { 
            //   if (err) { console.error(err); return; } console.log(result);
            //  })
            console.log("hellohset")
            var v= await redisClient.hSet(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0])
            //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
            )
            console.log("value",v)
           // await redisClient.HSET(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0]), (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}) // logs: 
          }
          var agentActivity=await db.agentActivity({agent_id:findAgent[0]._id,user_id:req.body.availableAgent,activity_name:"USER_INCOMING_REQUEST",value:req.body.chat_session_id,created_at:new Date()}).save();
          socket = io(config.socketUrl+tenantId);

          socket.emit("send-new-req", incomingreq[0]);
          console.log(req.body.channel)
          console.log("msg",req.body.message)
          if (req.body.channel != "from_purple") {
            await axios.post(
              config.customurl+"/v1/message/addMessage",
              {
                from: req.body.session_id,
                to: findAgent[0]._id,
                message: req.body.message,
                senderName: req.body.name,
                receiverName: findAgent[0].username,
                messageFrom: "fromClient",
                msg_sent_type:req.body.msg_sent_type
              }
            );

            const data = await axios.post(
              config.customurl+`/v1/users/getId/${req.body.email_id}`
            );
            if (data && data.length) {
              socket = io(config.socketUrl+tenantId);
              socket.emit("add-user", data.data.user.id);
            }
          }
          logger.info(req.body.chat_session_id+":CreateSession:chat session created ",incomingreq)
          res.json({
            status: true,
            message: "chat session created successfully",
            error: false,
            data: {
                session_key:req.body.chat_session_id,
                channel:incomingreq[0].channel,
                sender_number:incomingreq[0].phonenumber,
                sender_name:req.body.name,
                skillset_name:incomingreq[0].skillset
            },
          });
        
          // console.log(req.body);
          return;
        }
      }
    
  }
      
}