const { generateJwtToken } = require("../../_helper/helper");
const errorHandler = require("../../_helper/error.handler");
const db = require("../../_helper/db");
const { default: mongoose } = require("mongoose");
const io = require("socket.io-client");
let socket = io();
const redis = require("redis");
const moment = require("moment");
const { redisconfig } = require("../../_helper/redis.config");
const fs = require("fs");
const axios = require("axios");
const FormData = require('form-data');
const config_file = require("../../config.json");
const config = require("../../config.json");
var logger = require("../../_helper/logger");
const { json } = require("body-parser");
let redisClient;
let lastredisclient = undefined;

(async () => {
  redisClient = redisconfig();

  redisClient.on("error", (error) => {
    lastredisclient = undefined;
    console.error(`Error : ${error}`);
  });
  redisClient.on("connect", async (data) => {
    console.error(`Redis Started`);
    lastredisclient = "Connected";
  });
  await redisClient.connect();
})();
module.exports = {
  token,
  sendMessage,
  endChat
};

async function token(req, res, next) {
    try {
      const { email, password } = req.body;
  
      var err = {};
  
      if (!email) {
        err.message = "email  is missing";
  
        err.status = 400;
      }
  
      if (!password) {
        err.message = "password  is missing";
  
        err.status = 400;
      }
  
      if (err.message) return errorHandler(err, req, res, next);
  
      // const user = await db.admin.findOne({
      //   $or: [{ username, password }],
      // });
      var user = {};
      if (email ==  "purple@inaipi.com"&& password == "Puae@inp2202") {
        user.email = email;
        user.password = password;
      }
  
      if (user != null) {
        if (user.email) {
          const jwtToken = generateJwtToken(user);
           
          res.json({
            status: true,
            message: "Login Successfully",
            token_type: "bearer",
            access_token: jwtToken,
            expires_in: 7200,
            scope: "message"
            
          });
        } else {
          res.json({ status: false, message: "invalid username/password" });
        }
      } else {
        res.json({ status: false, message: "invalid username/password" });
      }
    } catch (e) {
      next(e);
    }
  }

async function sendMessage(req,res,next){
    try {
        const {
          
          file_name,
        } = req.body;
    
        // let msgFrom = req.body.messageFrom;
        // let getId, from_id, to_id, static;
    
        
        //   getId = await db.Session.findOne({
        //     _id: from,
        //   });
    
        //   if (getId) {
        //     if(userType=="internal"){
        //       from_id = getId.receiver_id;
        //       static = from_id;
        //     }else{
        //     from_id = getId.unique_id;
        //     static = from_id;
        //     }
        //     if (getId.transferred) {
        //       to_id = String(getId.agent);
        //     } else {
        //       to_id = to;
        //     }
        //   }
        
    
        let findDetails = await db.Session.findOne({
            chat_session_id:req.body.session_key
        });
        var day=new Date()
        
        // var variable=JSON.stringify(req.headers)
        // var variable1=JSON.parse(variable)
        // console.log(variable1.tenantid);
        
        var tenantId="a3dc14bd-fe70-4120-8572-461b0dc866b5";
    var hours = day.getHours();
    var minutes = day.getMinutes();
    var seconds = day.getSeconds();
      if(hours>=12){
    var dateTime =  hours + ":" + minutes + ":" + seconds+ " PM";
      }else{
        var dateTime =  hours + ":" + minutes + ":" + seconds+ " AM";
      }
    console.log(dateTime)
    if(findDetails){
        const update = await db.Session.findByIdAndUpdate(
          { _id: findDetails.id },
          {
            $set: {
              lastmessage: req.body.payload.value.text,
              lastmessagetime: dateTime,
              lastmessageUpdatedat:new Date()
            },
          }
        );
    
        const data = await db.Message.create({
          message: { text:req.body.payload.value.text },
          users: [findDetails.unique_id,findDetails.agent],
          sender:findDetails.unique_id,
          receiver:findDetails.agent,
          file_name,
          msg_sent_type:req.body.payload.type,
          session_id: findDetails ? findDetails.chat_session_id : "",
        });
    
        var messageobj = {
          fromSelf:false,
          message:req.body.payload.value.text,
          time: moment().format("lll"),
          sender:findDetails.unique_id,
          receiver:findDetails.agent,
          msg_sent_type:
          req.body.payload.type?req.body.payload.type : "normal_message",
          session_id: findDetails.chat_session_id,
          file_name: file_name,
        };
    
        if (lastredisclient != undefined) {
          var value = await redisClient.get(findDetails.chat_session_id);
          let cachedmessage = JSON.parse(value);
    
          if (cachedmessage) {
            cachedmessage = [...cachedmessage, messageobj];
          } else {
            cachedmessage = [messageobj];
          }
          redisClient.set(
            findDetails.chat_session_id,
            JSON.stringify(cachedmessage)
          );
        }
       // socket.current = io(config.socketUrl+tenantId);
        
      // socket.current.emit("last-msg-send", {
      //   to: currentChat.id,
      //   session_id: currentChat.chat_session_id,
      //   from: senderId,
      //   chatType: "inbound",
      //   msg,
      //   senderName: data.username,
      //   chatdetails: currentChat
      // });
      console.log("agent",String(findDetails.agent))
        // socket.current.emit("send-msg", {
        //   to:String(findDetails.agent),
        //   from:findDetails.unique_id,
        //   chatType: "inbound",
        //   msg:req.body.payload.value.text,
        //   senderName:findDetails.username?findDetails.username:"",
        //   msgType: "web",
        //   msg_sent_type:req.body.payload.type,
        //   session_id: findDetails.chat_session_id,
        //   chatdetails: findDetails,
        //   userType:"internal"
          
        // });
        var msg=req.body.payload.value.text
        socket.current = io(config.socketUrl+tenantId);
        socket.current.emit("send-msg", {
          to:findDetails.agent,
          from: findDetails._id,
          msg,
          chatType: "inbound",
          msgType: "webchat",
          senderName: findDetails.unique_id.username,
          msg_sent_type:req.body.payload.type ,
          session_id: findDetails.chat_session_id,
          chatdetails: findDetails,
          chat_type: "external",
          userType: "internal",
          file_name: req.body.file_name ? req.body.file_name : ""
        });
        if (data) {
          logger.info(data._id+":AddMessage:Message added",data)
          return res.json({
            message: "Message sent successfully",
            error: false,
            data: {
            message: req.body.payload.value.text,
            session_key: req.body.session_key,
            message_type: req.body.payload.type
            }
           }
           );}
        else{ 
          logger.error("AddMessage:Failed to add message to the database","")
          return res.json({ msg: "Failed to add message to the database" });}
        }
        else{
            logger.error("AddMessage:Failed to add message to the database","")
          return res.json({ msg: "Failed to add message to the database" });
        }
      } catch (ex) {
        next(ex);
      }
}  
async function endChat(req,res){
    let session_id = req.body.payload.session_key;
    let findDetails = await db.Session.findOne({
      chat_session_id: session_id,
    });
  
   // var tenantId=req.headers.tenantId;
  //  var variable=JSON.stringify(req.headers)
  //  var variable1=JSON.parse(variable)
  //  console.log(variable1.tenantid);
   
   var tenantId="a3dc14bd-fe70-4120-8572-461b0dc866b5";
    if (findDetails) {
      if(findDetails.status==='newjoin'){
        var dataValue={
          "crmId": "10000001",
          "phoneNumber": findDetails.phonenumber,
          "serviceRequestType": "",
          "customerLanguage": findDetails.language,
          "customerLanguageCode": "",
          "LastHandledAgentID": "",
          "routingRuleId": "",
          "skillSet": findDetails.skillset,
          "chat_session_id": findDetails.chat_session_id
        }
        if(config_file.env==='Production'){
        await axios.post(config_file.routingbaseurl+"/routingengine/engine/acceptChat",dataValue, {
          headers: { TENANTID: tenantId },
        })}
        var updateUser = await db.Session.findByIdAndUpdate(
          { _id: findDetails._id },
          {
            $set: {
              customer_disconnected:true,
              customer_drop:true,
              status:"CustomerDropped"
            }}
        );
      }else{
      var updateUser = await db.Session.findByIdAndUpdate(
        { _id: findDetails._id },
        {
          $set: {
            is_customer_disconnected:true,
      is_customer_disconnected_time: new Date(),
          }}
      );
        }
  
      // let findDetailsUSer = await db.User.findOne({
      //   _id: findDetails.agent,
      // });
      if (lastredisclient != undefined) {
      //   var value = await redisClient.get(JSON.stringify(findDetails.agent));
      //   let cachedrequestlist = JSON.parse(value);
  
      //   let filteredArray = cachedrequestlist.filter((item) => {
          
      //     if(item.chat_session_id === session_id){
      //     item.is_customer_disconnected=true;
      //         item.is_customer_disconnected_time=new Date();
      //     }
      //   });
      //  // console.log("after Updating " + filteredArray);
      //   redisClient.set(
      //     JSON.stringify(findDetails.agent),
      //     JSON.stringify(filteredArray)
      //   );
      var agents=JSON.stringify(findDetails.agent);
      var ss= await redisClient.hVals(JSON.stringify(agents))
      //console.log(JSON.stringify(ss))
   console.log(typeof(ss))
    var valu1=JSON.stringify(ss)
    console.log(typeof(valu1))
    var newval=JSON.parse(valu1);
    var upadeddata=newval.map((value)=>{
      var parseddata=JSON.parse(value);
     if( parseddata["chat_session_id"] == session_id){
      parseddata.is_customer_disconnected=true;
               parseddata.is_customer_disconnected_time=new Date();
            var valu3=  redisClient.hSet(JSON.stringify(agents), JSON.stringify(parseddata["id"]), JSON.stringify(parseddata)
          //, (err, result) => { if (err) { console.error(err); return; } console.log("redis"+result);}
          )
          console.log("value",valu3)
     }
     return parseddata;
    })
       
      }
      var msg="Customer Ended the Chat";
      socket.current = io(config.socketUrl+tenantId);
      socket.current.emit("customer-end-chat-req", {
        chat_session_id: session_id
      });
      socket.current.emit("send-msg", {
        to:findDetails.agent,
        from: findDetails._id,
        msg,
        chatType: "inbound",
        msgType: "webchat",
        senderName: findDetails.unique_id.username,
        msg_sent_type: "NOTIFICATIONS" ,
        session_id: findDetails.chat_session_id,
        chatdetails: findDetails,
        chat_type: "external",
        userType: "internal",
        file_name: req.body.file_name ? req.body.file_name : ""
      });
      socket.current.emit("last-msg-send", {
        to:findDetails.agent,
        session_id:findDetails.chat_session_id,
        from:findDetails._id,
        chatType: "external",
        msg,
        senderName:findDetails.unique_id.username,
        chatdetails:findDetails
      });
      if (updateUser) {
        logger.info(session_id+":EndChatCustomer:Chat session Ended",session_id)
        res.json({
            message: "Chat ended successfully ",
            error: false,
            }
            );
      } else {
        logger.error("EndChatCustomer:Error Occured",session_id)
        res.json({
          success: false,
          message: "error",
          data: {
            chat_session_id: session_id,
          },
        });
      }
    
   
    }
    else {
      logger.error("EndChatCustomer:Error Occured",session_id)
      res.json({
        success: false,
        message: "error",
        data: {
          chat_session_id: session_id,
        },
      });
    }
  }