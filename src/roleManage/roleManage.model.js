const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const schema = new Schema({
  id: { type: String },
  createdAt: { type: Date },
  createdBy:{type:String},
  roleName:{type:String},
  status:{type:String},
  updatedAt:{type:String},
  updatedBy:{type:String},
  description:{type:String}
  
});

schema.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    // remove these props when object is serialized
    delete ret._id;
  },
});

module.exports = mongoose.model("roleManage", schema);