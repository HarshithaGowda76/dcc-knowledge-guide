const axios = require("axios");
const errorHandler = require("../../_helper/error.handler");
const db = require("../../_helper/db");
const config_file = require("../../config.json");
module.exports = {
    languagelist
  };
  
  async function languagelist(req, res, next) {
    try{
      var tenantId=req.headers.tenantId;
        const config = {
            headers: { TenantID: tenantId},
          };
          let data = {enabled:true};
      
          axios
            .post(config_file.languagelistapi, data, config)
            .then(async (resp) => {
              if (resp.data.status == "OK") {
                res.json({
                    status: true,
                    message: "Data Fetched Successfully  ",
                    data:resp.data.dataList
                  });
                //let data = resp.data.dataList[0];
              }
              else{
                res.json({
                    status: false,
                    message: "No data Found ",
                  });
              }
            }) .catch((err) => {
                //console.log(err);
                res.json({
                  status: false,
                  message: "Something Went Wrong Api !",
                });
              });
        
    }
    catch(e){
        res.json({
            status: false,
            message: "Something Went Wrong !",
          });
    }
}