const express = require("express");
const router = express.Router();
const authorize = require("../../_middleware/authorize");
const languagelist = require("./language.service");

 router.post("/languagelist", languagelist.languagelist);

module.exports = router;