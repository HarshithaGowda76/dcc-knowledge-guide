const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const schema = new Schema({
  agent_id: {  type: mongoose.Schema.Types.ObjectId,
    ref: "User" },
  created_at: { type: Date },
 duration_login:{type:Number,default:0},
 loginDate:{type:Date},
 is_login:{type:Boolean,default:false},
 notReadyDate:{type:Date},
 duration_break:{type:Number,default:0},
 breakvalue:{type:Boolean,default:false},
 updateDate:{type:Date}





  
});

schema.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    // remove these props when object is serialized
    delete ret._id;
  },
});

module.exports = mongoose.model("activeTime", schema);