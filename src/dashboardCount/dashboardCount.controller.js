const express = require("express");
const router = express.Router();
const authorize = require("../../_middleware/authorize");
const dashboardCount = require("./dashboardCount.service");

 router.post("/dashboardCount", dashboardCount.dashboardCount);
 router.post("/taskCountByAgentId",dashboardCount.taskCountByAgentId);
 router.post("/tasklist",dashboardCount.tasklist);
 router.post("/chatCount",dashboardCount.chatCount)

module.exports = router;