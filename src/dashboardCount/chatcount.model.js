const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const schema = new Schema({
  agent_id: {  type: mongoose.Schema.Types.ObjectId,
    ref: "User" },
  created_at: { type: Date },
  internal_chat:{type: Number, default: 0 },
  active_chat:{type: Number, default: 0 },
  active_transfered_chat:{type:Number,default:0},
  active_conferece_chat:{type:Number,default:0},
  total_transferred_in:{type:Number,default:0},
  total_transferred_out:{type:Number,default:0},
  conference_basedactive_chat:{type:Number,default:0},
  conference_based_conferenceagent:{type:Number,default:0},
  agent_status:{type:String,default:"Available"}



  
});

schema.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    // remove these props when object is serialized
    delete ret._id;
  },
});

module.exports = mongoose.model("chatCount", schema);