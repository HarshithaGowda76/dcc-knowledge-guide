const errorHandler = require("../../_helper/error.handler");
const db = require("../../_helper/db");
const { default: mongoose } = require("mongoose");
const { count } = require("./totaltask.model");
var logger = require("../../_helper/logger");
module.exports = {
  dashboardCount,
  taskCountByAgentId,
  tasklist,
  chatCount
};
async function dashboardCount(req, res, next) {
  try {
    var todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var agent = await db.roleManage
      .findOne({ $or: [{ roleName: "Agent" }, { roleName: "agent" }] })
      .select({ id: 1, _id: 0 });
    if (agent) {
      var allAgentsCount = await db.User.countDocuments({
        roles_array: agent.id
      });
      var agentLoggedInCount = await db.User.countDocuments({
        roles_array: agent.id,
        is_loggedIn: true,
      });
      var agentOfflineCount = await db.User.countDocuments({
        roles_array: agent.id,
        is_loggedIn: false,
      });
      var agentActiveCount = await db.User.countDocuments({
        roles_array: agent.id,
        is_loggedIn: true,
        agent_status_real: "Available",
      });
      var agentReadyCount = await db.User.countDocuments({
        roles_array: agent.id,
        is_loggedIn: true,
        agent_status_real: "Ready",
      });
      var agentNotReadyCount = await db.User.countDocuments({
        roles_array: agent.id,
        is_loggedIn: true,
        agent_status_real: { $nin: ["Available", "Ready"] }
      });
      var ongoing_chat = await db.Session.countDocuments({
        status:"Accept",createdAt: { $gte: start_date, $lte: end_date },chat_type:"external"
      });
      var queued_chat = await db.Session.countDocuments({
        createdAt: { $gte: start_date, $lte: end_date },is_queued:true,chat_type:'external'
      });
      logger.info("DashboardCount:Dashboard count details",allAgentsCount)
      res.json({
        status: true,
        message: "Details Fetched Successfully",
        AllAgent: allAgentsCount,
        AgentLoggedIn: agentLoggedInCount,
        AgentOffline: agentOfflineCount,
        AgentActive: agentActiveCount,
        AgentReady: agentReadyCount,
        AgentNotReady: agentNotReadyCount,
        OngoingChat:ongoing_chat,
        QueuedChat:queued_chat

      });
    } else {
      logger.error("DashboardCount:No data Found","")
      res.json({ status: false, message: "no data found" });
    }
  } catch (e) {
    next(e);
  }
}
async function taskCountByAgentId(req, res, next) {
  try {
    var agent_id = req.body.agent_id;
    var todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var userDetails = await db.User.findOne({
      _id: mongoose.Types.ObjectId(agent_id),
    }).select({ username: 1, user_id: 1, email: 1,agent_status_real:1 });
    await db.User.findByIdAndUpdate(
      {
       _id:mongoose.Types.ObjectId(req.body.agent_id) 
      },
      {
        $set: {
          lastactive_time:new Date()
        },
      }
    );
    var totalTask = await db.Session.countDocuments({
      $or: [
        { agent: mongoose.Types.ObjectId(agent_id) },
        { transfer_agent: mongoose.Types.ObjectId(agent_id) },
        { conference_agent: mongoose.Types.ObjectId(agent_id) },
      ],createdAt: { $gte: start_date, $lte: end_date },chat_type:'external'
    });
    var completedTask = await db.Session.countDocuments({
      $or: [
        { agent: mongoose.Types.ObjectId(agent_id) ,createdAt: { $gte: start_date, $lte: end_date },
        status: "chatEnded",chat_type:'external'},
        { transfer_agent: mongoose.Types.ObjectId(agent_id),createdAt: { $gte: start_date, $lte: end_date },
        status: "chatEnded",chat_type:'external' },
        { conference_agent: mongoose.Types.ObjectId(agent_id),createdAt: { $gte: start_date, $lte: end_date },
        $or:[{
        status: "chatEnded",chat_type:'external' },{conference_left_id:mongoose.Types.ObjectId(agent_id),chat_type:'external'}]}
      ]
    });
    var tranferChat = await db.Session.countDocuments({
      transfer_agent: mongoose.Types.ObjectId(agent_id),createdAt: { $gte: start_date, $lte: end_date },chat_type:'external'
    });
    var conferChat = await db.Session.countDocuments({$or:[
     { conference_agent: mongoose.Types.ObjectId(agent_id),createdAt: { $gte: start_date, $lte: end_date },chat_type:'external'},
     {agent: mongoose.Types.ObjectId(agent_id),createdAt: { $gte: start_date, $lte: end_date },chat_type:'external',conference:true}
    ]});
    var is_customer_disconnected=await db.Session.countDocuments({
      $or: [
        { agent: mongoose.Types.ObjectId(agent_id) },
        { transfer_agent: mongoose.Types.ObjectId(agent_id) },
        { conference_agent: mongoose.Types.ObjectId(agent_id) },
      ],
      is_customer_disconnected:true,createdAt: { $gte: start_date, $lte: end_date },chat_type:'external'
    });
    var transferedin=await db.chatCount.findOne({created_at:start_date,agent_id:mongoose.Types.ObjectId(agent_id)}).select({_id:0,total_transferred_in:1,total_transferred_out:1,active_chat:1,conference_basedactive_chat:1});
    var totaltime=await db.totalTask.findOne({created_at:start_date,agent_id:mongoose.Types.ObjectId(agent_id)}).select({_id:0,total_active_time:1,total_break_time:1});
    console.log(totaltime)
    logger.info(agent_id+":TaskCountAgent:TaskCountAgent",userDetails)
    res.json({
      status: true,
      message: "Details Fetched Successfully",
      user: userDetails,
      totalTask: totalTask,
      completedTask: completedTask,
      transferchat: tranferChat,
      conferncechat: conferChat,
      customerDisconnected:is_customer_disconnected,
      TotalTransferedIn:transferedin?transferedin.total_transferred_in:0,
      TotalTransferedOut:transferedin?transferedin.total_transferred_out:0,
      OngoingChat:transferedin?transferedin.active_chat+transferedin.conference_basedactive_chat:0,
      TotalActiveTime:totaltime?totaltime.total_active_time:0,
      TotalBreakTime:totaltime?totaltime.total_break_time:0,

    });
  } catch (e) {
    next(e);
  }
}
async function tasklist(req, res, next) {
  try {

    var { from_date, to_date,offset,limit } = req.body;
    from_date = new Date(from_date);
    offset=parseInt(offset)
    limit=parseInt(limit)
    //console.log(from_date);
   to_date= new Date(to_date).toISOString();
    to_date = to_date.replace(/\T.*/, "");
     to_date = new Date(to_date + "T23:59:59.999Z");
     var aggregateOpts=[
      {
        '$match': {
          'created_at': {
            '$gte': from_date, 
            '$lte': to_date
          }
        }
      }, {
        '$lookup': {
          'from': 'chatcounts', 
          'let': {
            'agent_id': '$agent_id', 
            'dt_event': {
              '$dateToString': {
                'date': '$created_at', 
                'format': '%Y-%m-%d'
              }
            }
          }, 
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$$dt_event', {
                          '$dateToString': {
                            'date': '$created_at', 
                            'format': '%Y-%m-%d'
                          }
                        }
                      ]
                    }, {
                      '$eq': [
                        '$$agent_id', '$agent_id'
                      ]
                    }
                  ]
                }
              }
            }
          ], 
          'as': 'chat'
        }
      }, {
        '$unwind': {
          'path': '$chat'
        }
      }, {
        '$lookup': {
          'from': 'users', 
          'localField': 'agent_id', 
          'foreignField': '_id', 
          'as': 'agent'
        }
      }, {
        '$unwind': {
          'path': '$agent'
        }
      }, {
        '$project': {
          'id': '$_id', 
          'agent_id': {
            'username': '$agent.username', 
            'id': '$agent._id'
          }, 
          'created_at': 1, 
          'total_completed': {$add: ["$total_completed", "$total_completed_conference"],}, 
          'total_active_chat_count': {$add: ["$total_active_chat_conference_count", "$total_active_chat_count"],}, 
          'total_transfered': 1, 
          'total_break_time': 1, 
          'total_active_time': 1, 
          'total_transfered_in': '$chat.total_transferred_in', 
          'total_transfered_out': '$chat.total_transferred_out'
        }
      }, {
        '$skip': offset
      }, {
        '$limit': limit
      }
    ]
    var countaggregate=[
      {
        '$match': {
          'created_at': {
            '$gte': from_date, 
            '$lte': to_date
          }
        }
      }, {
        '$lookup': {
          'from': 'chatcounts', 
          'let': {
            'agent_id': '$agent_id', 
            'dt_event': {
              '$dateToString': {
                'date': '$created_at', 
                'format': '%Y-%m-%d'
              }
            }
          }, 
          'pipeline': [
            {
              '$match': {
                '$expr': {
                  '$and': [
                    {
                      '$eq': [
                        '$$dt_event', {
                          '$dateToString': {
                            'date': '$created_at', 
                            'format': '%Y-%m-%d'
                          }
                        }
                      ]
                    }, {
                      '$eq': [
                        '$$agent_id', '$agent_id'
                      ]
                    }
                  ]
                }
              }
            }
          ], 
          'as': 'chat'
        }
      }, {
        '$unwind': {
          'path': '$chat'
        }
      }, {
        '$lookup': {
          'from': 'users', 
          'localField': 'agent_id', 
          'foreignField': '_id', 
          'as': 'agent'
        }
      }, {
        '$unwind': {
          'path': '$agent'
        }
      }, {
        '$project': {
          'id': '$_id', 
          'agent_id': {
            'username': '$agent.username', 
            'id': '$agent._id'
          }, 
          'created_at': 1, 
          'total_completed': 1, 
          'total_active_chat_count': 1, 
          'total_transfered': 1, 
          'total_break_time': 1, 
          'total_active_time': 1, 
          'total_transfered_in': '$chat.total_transferred_in', 
          'total_transfered_out': '$chat.total_transferred_out'
        }
      },  {
        '$count': 'count'
      }
    ]
    //  var list=await db.totalTask.find({  created_at: { $gte: from_date, $lte: to_date },}).populate("agent_id", { username: 1 }).skip(offset).limit(limit);
    //  var counts=await db.totalTask.countDocuments({  created_at: { $gte: from_date, $lte: to_date },})
    var list=await db.totalTask.aggregate(aggregateOpts).exec();
    var count=await db.totalTask.aggregate(countaggregate).exec();
    if (count.length > 0) {
      var counts = count[0].count;
    } else {
      var counts = 0;
    }
     if(list.length>0){
      logger.info("TaskList:listed task details",list)
        res.json({
          success: true,
          message: "Data fetched Successfully",
          count:counts,
          Data: list,
        });
      }else{
        logger.error("TaskList:No data Found","")
        res.json({
          success: false,
          message: "No Data found"
        });}
    // console.log(to_date)
  } catch (e) {
    next(e);
  }
}
async function chatCount(req,res,next){
    try {
      var {offset,limit}=req.body;
        var { from_date, to_date } = req.body;
        from_date = new Date(from_date);
        //console.log(from_date);
       to_date= new Date(to_date).toISOString();
        to_date = to_date.replace(/\T.*/, "");
         to_date = new Date(to_date + "T23:59:59.999Z");
         var groupdetails=await db.User.findOne({_id:mongoose.Types.ObjectId(req.body.agent_id)})
         var agent = await db.roleManage
      .findOne({ $or: [{ roleName: "Agent" }, { roleName: "agent" }] })
      .select({ id: 1, _id: 0 });
      console.log(groupdetails.group_array)
      var group=groupdetails.group_array
         //var list=await db.chatCount.find({  created_at: { $gte: from_date, $lte: to_date },}).populate("agent_id", { username: 1,agent_status_real:1}).skip(offset).limit(limit);
         var aggregateOpts=[
          {
            '$match': {
              'created_at': {
                '$gte': from_date, 
                '$lte': to_date
              }
            }
          }, {
            '$lookup': {
              'from': 'users', 
              'localField': 'agent_id', 
              'foreignField': '_id', 
              'as': 'users'
            }
          }, {
            '$unwind': {
              'path': '$users'
            }
          }, {
            '$match':{
              'users.roles_array':{$eq:agent.id},
              'users.group_array':{$in:group}
            }
          }, {
            '$sort': {
              'agent_status': 1
            }
          },{
            '$project': {
              'agent_id': {
                'username': '$users.username', 
                'agent_status_real': '$users.agent_status_real', 
                'id': '$users._id'
              }, 
              'created_at': 1, 
              'internal_chat': 1, 
              'active_transfered_chat': 1, 
              'total_transferred_in': 1, 
              'total_transferred_out': 1, 
              'id': '$_id', 
              'active_chat': {
                '$add': [
                  '$active_chat', '$conference_basedactive_chat'
                ]
              }, 
              'active_conferece_chat': {
                '$add': [
                  '$active_conferece_chat', '$conference_based_conferenceagent'
                ]
              }
            }
          }
        ]
        var countaggregate=[
          {
            '$match': {
              'created_at': {
                '$gte': from_date, 
                '$lte': to_date
              }
            }
          }, {
            '$lookup': {
              'from': 'users', 
              'localField': 'agent_id', 
              'foreignField': '_id', 
              'as': 'users'
            }
          }, {
            '$unwind': {
              'path': '$users'
            }
          }, {
            '$match':{
              'users.roles_array':{$eq:agent.id},
              'users.group_array':{$in:group}
            }
          }, {
            '$sort': {
              'agent_status': 1
            }
          },{
            '$count': 'count'
          }
        ]
        var list=await db.chatCount.aggregate(aggregateOpts).exec();
        // var counts=await db.chatCount.countDocuments({  created_at: { $gte: from_date, $lte: to_date },})
        var count =await db.chatCount.aggregate(countaggregate).exec();
        if (count.length > 0) {
          var counts = count[0].count;
        } else {
          var counts = 0;
        }
         if(list.length>0){
          var filterrecord=list.filter((record,index)=>index>=offset && index < limit+offset )
          logger.info("ChatCount:listed chat Count details",list)
            res.json({
              success: true,
              message: "Data fetched Successfully",
              count:counts,
              Data: filterrecord,
            });
          }else{
            logger.error("ChatCount:No data Found","")
            res.json({
              success: false,
              message: "No Data found"
            });}
        // console.log(to_date)
      } catch (e) {
        res.json({
          success: false,
          message: "No Data found"
        })
      }
}