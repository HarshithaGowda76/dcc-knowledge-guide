const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const schema = new Schema({
  agent_id: {  type: mongoose.Schema.Types.ObjectId,
    ref: "User" },
  created_at: { type: Date },
  total_completed:{type: Number, default: 0 },
  total_active_chat_count:{type: Number, default: 0 },
  total_transfered:{type:Number,default:0},
  ready_time:{type:Date},
  total_active_time:{type:Number},
  total_break_time:{type:Number},
  total_completed_conference:{type: Number, default: 0 },
  total_active_chat_conference_count:{type: Number, default: 0 },


  
});

schema.set("toJSON", {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    // remove these props when object is serialized
    delete ret._id;
  },
});

module.exports = mongoose.model("totalTask", schema);