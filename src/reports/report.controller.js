const express = require("express");
const router = express.Router();
const authorize = require("../../_middleware/authorize");
const reportService = require("./report.service");

router.post("/viewDetails", reportService.viewDetails);
router.post("/listDetails", reportService.listDetails);
router.post("/dummyreport",reportService.dummyreport)
router.post("/listDetailsExcel", reportService.listDetailsExcel);
router.post("/updateagentactivity",reportService.updateagentactivity);
module.exports = router;
