const errorHandler = require("../../_helper/error.handler");
const db = require("../../_helper/db");
var logger = require("../../_helper/logger");
const axios = require("axios");
const config_file = require("../../config.json");
var Mongoose = require("mongoose");
var XLSX = require("xlsx");
var path = require("path");
var ObjectId = Mongoose.Types.ObjectId;
module.exports = {
  viewDetails,
  listDetails,
  dummyreport,
  listDetailsExcel,
  updateagentactivity
};

// API to list agent call details

async function viewDetails(req, res, next) {
  try {
    const chat_session_id = req.body.session_id;
    var sessionData = await db.Session.find({
      chat_session_id: chat_session_id,
    });
    var chatHistoryData = await db.chatHistory.find({
      session_id: chat_session_id,
    });

    if (sessionData || chatHistoryData) {
      logger.info(chat_session_id+":ViewReportDetails:report details based on session id", sessionData ? sessionData : [])
      res.json({
        success: true,
        message: "Data fetched",
        Data: {
          sessionData: sessionData ? sessionData : [],
          chatHistory: chatHistoryData ? chatHistoryData : [],
        },
      });
    } else {
      logger.error("ViewReportDetails:No data Found", "")
      res.json({
        success: false,
        message: "No data found",
      });
    }
  } catch (e) {
    next(e);
  }
}

// list details Api

async function listDetails(req, res, next) {
  try {
    var agent_id = req.body.agent_id;
    var status = req.body.status;
    var from_date = req.body.from_date;
    var to_date = req.body.to_date;
    var { offset, limit } = req.body;
    offset = parseInt(offset);
    limit = parseInt(limit);

    // var err = {};
    // if (!from_date) {
    //   err.message = "From date  is missing";
    //   err.status = 400;
    // }
    // if (!to_date) {
    //   err.message = "To date is missing";
    //   err.status = 400;
    // }
    // if (err.message) return errorHandler(err, req, res, next);
    if(!from_date&&!to_date){
   var start_date=new Date().toISOString();
   start_date=start_date.replace(/\T.*/, "");
   var from_date_starting = new Date(start_date);
   var to_date_end = new Date(start_date+ "T23:59:59.999Z");
    }else{
    from_date = new Date(from_date).toISOString();
    from_date = from_date.replace(/\T.*/, "");
    var from_date_starting = new Date(from_date);
    var from_date_end = new Date(from_date + "T23:59:59.999Z");
    to_date = new Date(to_date).toISOString();
    to_date = to_date.replace(/\T.*/, "");
    var to_date_starting = new Date(to_date);
    var to_date_end = new Date(to_date + "T23:59:59.999Z");
    }
    var aggregateOption, countValue;
  
    if (agent_id) {
      if (status) {
        if(status=='queued'){
          aggregateOption = [
            {
              $match: {
                is_queued:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
  
                $or: [
                  { agent: Mongoose.Types.ObjectId(agent_id) },
                  { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                  { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
                ],
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: {
                  $switch: {
                    branches: [
                      {
                        case: {
                          $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                        },
                        then: "$agentdetails.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$conference_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$conference_agents.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$transfer_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$transfer_agents.username",
                      },
                    ],
                    default: "",
                  },
                },
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
                transfer_agent_name:"$transfer_agents.username",
                conference_agent_name:"$conference_agents.username"
              },
            },
            {
              $skip: offset,
            },
            {
              $limit: limit,
            },
          ];
          countValue = [
            {
              $match: {
                is_queued:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
                $or: [
                  { agent: Mongoose.Types.ObjectId(agent_id) },
                  { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                  { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
                ],
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: {
                  $switch: {
                    branches: [
                      {
                        case: {
                          $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                        },
                        then: "$agentdetails.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$conference_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$conference_agents.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$transfer_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$transfer_agents.username",
                      },
                    ],
                    default: "",
                  },
                },
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
              },
            },
            {
              $count: "count",
            },
          ];
        }else if(status=='customerdisconnect'){
          aggregateOption = [
            {
              $match: {
                customer_disconnected:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
  
                $or: [
                  { agent: Mongoose.Types.ObjectId(agent_id) },
                  { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                  { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
                ],
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: {
                  $switch: {
                    branches: [
                      {
                        case: {
                          $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                        },
                        then: "$agentdetails.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$conference_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$conference_agents.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$transfer_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$transfer_agents.username",
                      },
                    ],
                    default: "",
                  },
                },
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
                transfer_agent_name:"$transfer_agents.username",
                conference_agent_name:"$conference_agents.username"
              },
            },
            {
              $skip: offset,
            },
            {
              $limit: limit,
            },
          ];
          countValue = [
            {
              $match: {
                customer_disconnected:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
                $or: [
                  { agent: Mongoose.Types.ObjectId(agent_id) },
                  { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                  { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
                ],
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: {
                  $switch: {
                    branches: [
                      {
                        case: {
                          $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                        },
                        then: "$agentdetails.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$conference_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$conference_agents.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$transfer_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$transfer_agents.username",
                      },
                    ],
                    default: "",
                  },
                },
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
              },
            },
            {
              $count: "count",
            },
          ];
        }
        else{
        aggregateOption = [
          {
            $match: {
              status: status,
              createdAt: {
                $gte: from_date_starting,
                $lte: to_date_end,
              },

              $or: [
                { agent: Mongoose.Types.ObjectId(agent_id) },
                { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
              ],
            },
          },
          {
            $lookup: {
              from: "clients",
              localField: "unique_id",
              foreignField: "_id",
              as: "client",
            },
          },
          {
            $unwind: {
              path: "$client",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "agent",
              foreignField: "_id",
              as: "agentdetails",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "conference_agent",
              foreignField: "_id",
              as: "conference_agents",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "transfer_agent",
              foreignField: "_id",
              as: "transfer_agents",
            },
          },
          {
            $project: {
              " _id": "$_id",
              agent_name: {
                $switch: {
                  branches: [
                    {
                      case: {
                        $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                      },
                      then: "$agentdetails.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$conference_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$conference_agents.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$transfer_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$transfer_agents.username",
                    },
                  ],
                  default: "",
                },
              },
              name: "$client.username",
              phone_number: "$phonenumber",
              email: "$email",
              channel: "$channel",
              browser: "$user_agent.browser",
              conference: "$conference",
              transferred: "$transferred",
              skillset: "$skillset",
              language: "$language",
              chat_arrival_at: "$arrival_at",
              chat_started_at: "$chat_started_at",
              chat_ended_at: "$chat_ended_at",
              status: "$status",
              transfer_agent_name:"$transfer_agents.username",
              conference_agent_name:"$conference_agents.username"
            },
          },
          {
            $skip: offset,
          },
          {
            $limit: limit,
          },
        ];
        countValue = [
          {
            $match: {
              status: status,
              createdAt: {
                $gte: from_date_starting,
                $lte: to_date_end,
              },
              $or: [
                { agent: Mongoose.Types.ObjectId(agent_id) },
                { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
              ],
            },
          },
          {
            $lookup: {
              from: "clients",
              localField: "unique_id",
              foreignField: "_id",
              as: "client",
            },
          },
          {
            $unwind: {
              path: "$client",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "agent",
              foreignField: "_id",
              as: "agentdetails",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "conference_agent",
              foreignField: "_id",
              as: "conference_agents",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "transfer_agent",
              foreignField: "_id",
              as: "transfer_agents",
            },
          },
          {
            $project: {
              " _id": "$_id",
              agent_name: {
                $switch: {
                  branches: [
                    {
                      case: {
                        $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                      },
                      then: "$agentdetails.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$conference_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$conference_agents.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$transfer_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$transfer_agents.username",
                    },
                  ],
                  default: "",
                },
              },
              name: "$client.username",
              phone_number: "$phonenumber",
              email: "$email",
              channel: "$channel",
              browser: "$user_agent.browser",
              conference: "$conference",
              transferred: "$transferred",
              skillset: "$skillset",
              language: "$language",
              chat_arrival_at: "$arrival_at",
              chat_started_at: "$chat_started_at",
              chat_ended_at: "$chat_ended_at",
              status: "$status",
            },
          },
          {
            $count: "count",
          },
        ];
      }
      } else {
        // console.log("no status");
        aggregateOption = [
          {
            $match: {
              createdAt: {
                $gte: from_date_starting,
                $lte: to_date_end,
              },
              $or: [
                { agent: Mongoose.Types.ObjectId(agent_id) },
                { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
              ],
            },
          },
          {
            $lookup: {
              from: "clients",
              localField: "unique_id",
              foreignField: "_id",
              as: "client",
            },
          },
          {
            $unwind: {
              path: "$client",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "agent",
              foreignField: "_id",
              as: "agentdetails",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "conference_agent",
              foreignField: "_id",
              as: "conference_agents",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "transfer_agent",
              foreignField: "_id",
              as: "transfer_agents",
            },
          },
          {
            $project: {
              " _id": "$_id",
              agent_name: {
                $switch: {
                  branches: [
                    {
                      case: {
                        $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                      },
                      then: "$agentdetails.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$conference_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$conference_agents.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$transfer_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$transfer_agents.username",
                    },
                  ],
                  default: "",
                },
              },
              name: "$client.username",
              phone_number: "$phonenumber",
              email: "$email",
              channel: "$channel",
              browser: "$user_agent.browser",
              conference: "$conference",
              transferred: "$transferred",
              skillset: "$skillset",
              language: "$language",
              chat_arrival_at: "$arrival_at",
              chat_started_at: "$chat_started_at",
              chat_ended_at: "$chat_ended_at",
              status: "$status",
              transfer_agent_name:"$transfer_agents.username",
              conference_agent_name:"$conference_agents.username"
            },
          },
          {
            $skip: offset,
          },
          {
            $limit: limit,
          },
        ];
        countValue = [
          {
            $match: {
              createdAt: {
                $gte: from_date_starting,
                $lte: to_date_end,
              },
              $or: [
                { agent: Mongoose.Types.ObjectId(agent_id) },
                { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
              ],
            },
          },
          {
            $lookup: {
              from: "clients",
              localField: "unique_id",
              foreignField: "_id",
              as: "client",
            },
          },
          {
            $unwind: {
              path: "$client",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "agent",
              foreignField: "_id",
              as: "agentdetails",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "conference_agent",
              foreignField: "_id",
              as: "conference_agents",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "transfer_agent",
              foreignField: "_id",
              as: "transfer_agents",
            },
          },
          {
            $project: {
              " _id": "$_id",
              agent_name: {
                $switch: {
                  branches: [
                    {
                      case: {
                        $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                      },
                      then: "$agentdetails.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$conference_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$conference_agents.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$transfer_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$transfer_agents.username",
                    },
                  ],
                  default: "",
                },
              },
              name: "$client.username",
              phone_number: "$phonenumber",
              email: "$email",
              channel: "$channel",
              browser: "$user_agent.browser",
              conference: "$conference",
              transferred: "$transferred",
              skillset: "$skillset",
              language: "$language",
              chat_arrival_at: "$arrival_at",
              chat_started_at: "$chat_started_at",
              chat_ended_at: "$chat_ended_at",
              status: "$status",
            },
          },
          {
            $count: "count",
          },
        ];
      }

      var agentData = await db.Session.aggregate(aggregateOption).exec();
      var countData = await db.Session.aggregate(countValue).exec();
      if (countData.length > 0) {
        var count = countData[0].count;
      } else {
        var count = 0;
      }
    } else {
      var Data = await db.roleManage
        .findOne({ roleName: "Agent" })
        .select({ id: 1, _id: 0 });
      var user = await db.User.find({ roles_array: Data.id }).select({
        _id: 1,
      });
      var userarray = [];
      user.forEach((el) => {
        userarray.push(el._id);
      });
      if (status) {
        // console.log("with status");
        if(status=='queued'){
          aggregateOption = [
            {
              $match: {
                is_queued:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
               // agent: { $in: userarray },
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            // {
            //   $unwind: {
            //     path: "$agentdetails",
            //   },
            // },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: ["$agentdetails.username"],
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
                transfer_agent_name:"$transfer_agents.username",
                conference_agent_name:"$conference_agents.username"
              },
            },
            {
              $skip: offset,
            },
            {
              $limit: limit,
            },
          ];
          countValue = [
            {
              $match: {
                is_queued:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
                //agent: { $in: userarray },
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            // {
            //   $unwind: {
            //     path: "$agentdetails",
            //   },
            // },
            {
              $project: {
                " _id": "$_id",
                agent_name: ["$agentdetails.username"],
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
              },
            },
            {
              $count: "count",
            },
          ];
        }else if(status=='customerdisconnect'){
          aggregateOption = [
            {
              $match: {
                customer_disconnected:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
               // agent: { $in: userarray },
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            // {
            //   $unwind: {
            //     path: "$agentdetails",
            //   },
            // },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: ["$agentdetails.username"],
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
                transfer_agent_name:"$transfer_agents.username",
                conference_agent_name:"$conference_agents.username"
              },
            },
            {
              $skip: offset,
            },
            {
              $limit: limit,
            },
          ];
          countValue = [
            {
              $match: {
                customer_disconnected:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
                //agent: { $in: userarray },
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            // {
            //   $unwind: {
            //     path: "$agentdetails",
            //   },
            // },
            {
              $project: {
                " _id": "$_id",
                agent_name: ["$agentdetails.username"],
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
              },
            },
            {
              $count: "count",
            },
          ];
        }
        else{
          aggregateOption = [
            {
              $match: {
                status: status,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
                agent: { $in: userarray },
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            {
              $unwind: {
                path: "$agentdetails",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: ["$agentdetails.username"],
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
                transfer_agent_name:"$transfer_agents.username",
                conference_agent_name:"$conference_agents.username"
              },
            },
            {
              $skip: offset,
            },
            {
              $limit: limit,
            },
          ];
          countValue = [
            {
              $match: {
                status: status,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
                agent: { $in: userarray },
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            {
              $unwind: {
                path: "$agentdetails",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: ["$agentdetails.username"],
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
              },
            },
            {
              $count: "count",
            },
          ];
        }
       
      } else {
        // console.log("no status");
        aggregateOption = [
          {
            $match: {
              createdAt: {
                $gte: from_date_starting,
                $lte: to_date_end,
              },
              agent: { $in: userarray },
            },
          },
          {
            $lookup: {
              from: "clients",
              localField: "unique_id",
              foreignField: "_id",
              as: "client",
            },
          },
          {
            $unwind: {
              path: "$client",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "agent",
              foreignField: "_id",
              as: "agentdetails",
            },
          },
          {
            $unwind: {
              path: "$agentdetails",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "conference_agent",
              foreignField: "_id",
              as: "conference_agents",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "transfer_agent",
              foreignField: "_id",
              as: "transfer_agents",
            },
          },
          {
            $project: {
              " _id": "$_id",
              agent_name: ["$agentdetails.username"],
              name: "$client.username",
              phone_number: "$phonenumber",
              email: "$email",
              channel: "$channel",
              browser: "$user_agent.browser",
              conference: "$conference",
              transferred: "$transferred",
              skillset: "$skillset",
              language: "$language",
              chat_arrival_at: "$arrival_at",
              chat_started_at: "$chat_started_at",
              chat_ended_at: "$chat_ended_at",
              status: "$status",
              transfer_agent_name:"$transfer_agents.username",
              conference_agent_name:"$conference_agents.username"
            },
          },
          {
            $skip: offset,
          },
          {
            $limit: limit,
          },
        ];
        countValue = [
          {
            $match: {
              createdAt: {
                $gte: from_date_starting,
                $lte: to_date_end,
              },
              agent: { $in: userarray },
            },
          },
          {
            $lookup: {
              from: "clients",
              localField: "unique_id",
              foreignField: "_id",
              as: "client",
            },
          },
          {
            $unwind: {
              path: "$client",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "agent",
              foreignField: "_id",
              as: "agentdetails",
            },
          },
          {
            $unwind: {
              path: "$agentdetails",
            },
          },
          {
            $project: {
              " _id": "$_id",
              agent_name: ["$agentdetails.username"],
              name: "$client.username",
              phone_number: "$phonenumber",
              email: "$email",
              channel: "$channel",
              browser: "$user_agent.browser",
              conference: "$conference",
              transferred: "$transferred",
              skillset: "$skillset",
              language: "$language",
              chat_arrival_at: "$arrival_at",
              chat_started_at: "$chat_started_at",
              chat_ended_at: "$chat_ended_at",
              status: "$status",
            },
          },
          {
            $count: "count",
          },
        ];
      }
      var agentData = await db.Session.aggregate(aggregateOption).exec();
      var countData = await db.Session.aggregate(countValue).exec();
      if (countData.length > 0) {
        var count = countData[0].count;
      } else {
        var count = 0;
      }
    }
    if (agentData.length > 0) {
      logger.info("ListReportDetails:report details based on session id",agentData)
      res.json({
        success: true,
        message: "Data fetched",
        Data: agentData,
        count: count,
      });
    } else {
      logger.error("ListReportDetails:No data Found","")
      res.json({
        success: false,
        message: "No data found",
      });
    }
  } catch (e) {
    next(e);
  }
}
async function dummyreport(req,res,next){
  try{
     var {type,phonenumber}=req.body;
    //  var variable=JSON.stringify(req.headers);
    // var variable1=JSON.parse(variable)
    // console.log(variable1.tenantid);
    
   // var tenantId=variable1.tenantid;
   

 


      if(type=="hold"){
        const configvalue = {
          headers: { TenantID: "a3dc14bd-fe70-4120-8572-461b0dc866b5" },
          };
          const result = await db.Session.findOne({phonenumber:phonenumber}).sort({ chat_started_at: -1 })
          .exec();
          const hold={
                "interaction_id":result.chat_session_id,
                "hold_start_time": new Date(),
                "hold_reason": "hold",
                "hold_notes": "hold call",
                "agent_id":result.agent,
                "isfinished": false
               }
  
        await axios.post(
          config_file.interactionApi+"holdDetails/createHoldDetails",
          hold,
          configvalue
          );
      }
      else if(type=="transfer"){
        const result = await db.Session.findOne({phonenumber:phonenumber})
  .sort({ chat_started_at: -1 })
  .exec();
        const configvalue = {
          headers: { TenantID:"a3dc14bd-fe70-4120-8572-461b0dc866b5" },
          };
          const tranfer={
            "interaction_id": result.chat_session_id,
            "Soure_Ref_id":"2",
            "transfer_Start_time": new Date(),
            "transfer_from":req.body.transfer_from?req.body.transfer_from:"",
            "transfer_to":req.body.transfer_to?req.body.transfer_to:"",
            "transfer_type": "Attended",
            "transfer_notes":"" ,
            "isfinished": true
        }
        await axios.post(
          config_file.interactionApi+"transferDetails/createTransferDetails",
          tranfer,
          configvalue
          );
      }
      else if(type=="unhold"){
        const configvalue = {
          headers: { TenantID: "a3dc14bd-fe70-4120-8572-461b0dc866b5" },
          };
          const result = await db.Session.findOne({phonenumber:phonenumber}).sort({ chat_started_at: -1 })
          .exec()
          const unhold={
            "interaction_id":result.chat_session_id,
         "hold_end_time": new Date(),
         "hold_duration":"30s",
         "hold_reason": "hold",
         "hold_notes": "hold",
         "isfinished": true
       }
  
        await axios.post(
          config_file.interactionApi+"holdDetails/updateHoldDetails",
          unhold,
          configvalue
          );
          var updatedata=  {
            "Intraction_id":result.chat_session_id,
            "agent_id":result.agent,
            "unhold_flag":true,
            "interaction_disconnected":false,
            
    
        }
          await axios.post(
            config_file.interactionApi+"liveReport/updateLiveReport",
            updatedata,
            configvalue
            );

      }
      else if(type=="conference"){
        const configvalue = {
          headers: { TenantID: "a3dc14bd-fe70-4120-8572-461b0dc866b5" },
          };
          const result = await db.Session.findOne({phonenumber:phonenumber})
          .sort({ chat_started_at: -1 })
          .exec();
                
          var conference_data={
            interaction_id:result.chat_session_id,
            start_time:result.chat_started_at,
            end_time: new Date(),
            duration: "10s",
            conference_number:req.body.confrence_number?req.body.confrence_number:req.body.phonenumber,
            participant_type: "internal",
            participant_id:result.agent,
            conference_notes:"Conference",
            isfinished: true
        }
        const config1 = {
          headers: { TenantID: tenantId },
          };
          await axios.post(
            config_file.interactionApi+"conferenceDetails/createConferenceDetails",
            conference_data,
            configvalue
            );
            var updatedata=  {
              "Intraction_id":result.chat_session_id,
              "confernce_flag":true,
              "interaction_disconnected":false,
              
      
          }
            await axios.post(
              config_file.interactionApi+"liveReport/updateLiveReport",
              updatedata,
              configvalue
              );
      }
  }
  catch(e){
    next(e)
  }
}
async function listDetailsExcel(req,res,next){
  try {
    var agent_id = req.body.agent_id;
    var status = req.body.status;
    var from_date = req.body.from_date;
    var to_date = req.body.to_date;
    // var { offset, limit } = req.body;
    // offset = parseInt(offset);
    // limit = parseInt(limit);

    // var err = {};
    // if (!from_date) {
    //   err.message = "From date  is missing";
    //   err.status = 400;
    // }
    // if (!to_date) {
    //   err.message = "To date is missing";
    //   err.status = 400;
    // }
    // if (err.message) return errorHandler(err, req, res, next);
    if(!from_date&&!to_date){
   var start_date=new Date().toISOString();
   start_date=start_date.replace(/\T.*/, "");
   var from_date_starting = new Date(start_date);
   var to_date_end = new Date(start_date+ "T23:59:59.999Z");
    }else{
    from_date = new Date(from_date).toISOString();
    from_date = from_date.replace(/\T.*/, "");
    var from_date_starting = new Date(from_date);
    var from_date_end = new Date(from_date + "T23:59:59.999Z");
    to_date = new Date(to_date).toISOString();
    to_date = to_date.replace(/\T.*/, "");
    var to_date_starting = new Date(to_date);
    var to_date_end = new Date(to_date + "T23:59:59.999Z");
    }
    var aggregateOption, countValue;
  
    if (agent_id) {
      if (status) {
        if(status=='queued'){
          aggregateOption = [
            {
              $match: {
                is_queued:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
  
                $or: [
                  { agent: Mongoose.Types.ObjectId(agent_id) },
                  { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                  { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
                ],
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: {
                  $switch: {
                    branches: [
                      {
                        case: {
                          $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                        },
                        then: "$agentdetails.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$conference_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$conference_agents.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$transfer_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$transfer_agents.username",
                      },
                    ],
                    default: "",
                  },
                },
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
                transfer_agent_name:"$transfer_agents.username",
                conference_agent_name:"$conference_agents.username"
              },
            },
          
          ];
        }else if(status=='customerdisconnect'){
          aggregateOption = [
            {
              $match: {
                customer_disconnected:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
  
                $or: [
                  { agent: Mongoose.Types.ObjectId(agent_id) },
                  { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                  { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
                ],
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: {
                  $switch: {
                    branches: [
                      {
                        case: {
                          $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                        },
                        then: "$agentdetails.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$conference_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$conference_agents.username",
                      },
                      {
                        case: {
                          $eq: [
                            "$transfer_agent",
                            Mongoose.Types.ObjectId(agent_id),
                          ],
                        },
                        then: "$transfer_agents.username",
                      },
                    ],
                    default: "",
                  },
                },
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
                transfer_agent_name:"$transfer_agents.username",
                conference_agent_name:"$conference_agents.username"
              },
            },
          
          ];
        }
        else{
        aggregateOption = [
          {
            $match: {
              status: status,
              createdAt: {
                $gte: from_date_starting,
                $lte: to_date_end,
              },

              $or: [
                { agent: Mongoose.Types.ObjectId(agent_id) },
                { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
              ],
            },
          },
          {
            $lookup: {
              from: "clients",
              localField: "unique_id",
              foreignField: "_id",
              as: "client",
            },
          },
          {
            $unwind: {
              path: "$client",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "agent",
              foreignField: "_id",
              as: "agentdetails",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "conference_agent",
              foreignField: "_id",
              as: "conference_agents",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "transfer_agent",
              foreignField: "_id",
              as: "transfer_agents",
            },
          },
          {
            $project: {
              " _id": "$_id",
              agent_name: {
                $switch: {
                  branches: [
                    {
                      case: {
                        $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                      },
                      then: "$agentdetails.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$conference_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$conference_agents.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$transfer_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$transfer_agents.username",
                    },
                  ],
                  default: "",
                },
              },
              name: "$client.username",
              phone_number: "$phonenumber",
              email: "$email",
              channel: "$channel",
              browser: "$user_agent.browser",
              conference: "$conference",
              transferred: "$transferred",
              skillset: "$skillset",
              language: "$language",
              chat_arrival_at: "$arrival_at",
              chat_started_at: "$chat_started_at",
              chat_ended_at: "$chat_ended_at",
              status: "$status",
              transfer_agent_name:"$transfer_agents.username",
              conference_agent_name:"$conference_agents.username"
            },
          },
        
        ];
      }
      } else {
        // console.log("no status");
        aggregateOption = [
          {
            $match: {
              createdAt: {
                $gte: from_date_starting,
                $lte: to_date_end,
              },
              $or: [
                { agent: Mongoose.Types.ObjectId(agent_id) },
                { conference_agent: Mongoose.Types.ObjectId(agent_id) },
                { transfer_agent: Mongoose.Types.ObjectId(agent_id) },
              ],
            },
          },
          {
            $lookup: {
              from: "clients",
              localField: "unique_id",
              foreignField: "_id",
              as: "client",
            },
          },
          {
            $unwind: {
              path: "$client",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "agent",
              foreignField: "_id",
              as: "agentdetails",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "conference_agent",
              foreignField: "_id",
              as: "conference_agents",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "transfer_agent",
              foreignField: "_id",
              as: "transfer_agents",
            },
          },
          {
            $project: {
              " _id": "$_id",
              agent_name: {
                $switch: {
                  branches: [
                    {
                      case: {
                        $eq: ["$agent", Mongoose.Types.ObjectId(agent_id)],
                      },
                      then: "$agentdetails.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$conference_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$conference_agents.username",
                    },
                    {
                      case: {
                        $eq: [
                          "$transfer_agent",
                          Mongoose.Types.ObjectId(agent_id),
                        ],
                      },
                      then: "$transfer_agents.username",
                    },
                  ],
                  default: "",
                },
              },
              name: "$client.username",
              phone_number: "$phonenumber",
              email: "$email",
              channel: "$channel",
              browser: "$user_agent.browser",
              conference: "$conference",
              transferred: "$transferred",
              skillset: "$skillset",
              language: "$language",
              chat_arrival_at: "$arrival_at",
              chat_started_at: "$chat_started_at",
              chat_ended_at: "$chat_ended_at",
              status: "$status",
              transfer_agent_name:"$transfer_agents.username",
              conference_agent_name:"$conference_agents.username"
            },
          },
        ];
      }

      var agentData = await db.Session.aggregate(aggregateOption).exec();
      // var countData = await db.Session.aggregate(countValue).exec();
      // if (countData.length > 0) {
      //   var count = countData[0].count;
      // } else {
      //   var count = 0;
      // }
    } else {
      var Data = await db.roleManage
        .findOne({ roleName: "Agent" })
        .select({ id: 1, _id: 0 });
        console.log(Data)
      var user = await db.User.find({ roles_array: Data.id }).select({
        _id: 1,
      });
      var userarray = [];
      user.forEach((el) => {
        userarray.push(el._id);
      });
      if (status) {
        // console.log("with status");
        if(status=='queued'){
          aggregateOption = [
            {
              $match: {
                is_queued:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
               // agent: { $in: userarray },
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            // {
            //   $unwind: {
            //     path: "$agentdetails",
            //   },
            // },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: ["$agentdetails.username"],
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
                transfer_agent_name:"$transfer_agents.username",
                conference_agent_name:"$conference_agents.username"
              },
            },
          ];
         
        }else if(status=='customerdisconnect'){
          aggregateOption = [
            {
              $match: {
                customer_disconnected:true,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
               // agent: { $in: userarray },
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            // {
            //   $unwind: {
            //     path: "$agentdetails",
            //   },
            // },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: ["$agentdetails.username"],
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
                transfer_agent_name:"$transfer_agents.username",
                conference_agent_name:"$conference_agents.username"
              },
            },
          ];
        }
        else{
          aggregateOption = [
            {
              $match: {
                status: status,
                createdAt: {
                  $gte: from_date_starting,
                  $lte: to_date_end,
                },
                agent: { $in: userarray },
              },
            },
            {
              $lookup: {
                from: "clients",
                localField: "unique_id",
                foreignField: "_id",
                as: "client",
              },
            },
            {
              $unwind: {
                path: "$client",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "agent",
                foreignField: "_id",
                as: "agentdetails",
              },
            },
            {
              $unwind: {
                path: "$agentdetails",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "conference_agent",
                foreignField: "_id",
                as: "conference_agents",
              },
            },
            {
              $lookup: {
                from: "users",
                localField: "transfer_agent",
                foreignField: "_id",
                as: "transfer_agents",
              },
            },
            {
              $project: {
                " _id": "$_id",
                agent_name: ["$agentdetails.username"],
                name: "$client.username",
                phone_number: "$phonenumber",
                email: "$email",
                channel: "$channel",
                browser: "$user_agent.browser",
                conference: "$conference",
                transferred: "$transferred",
                skillset: "$skillset",
                language: "$language",
                chat_arrival_at: "$arrival_at",
                chat_started_at: "$chat_started_at",
                chat_ended_at: "$chat_ended_at",
                status: "$status",
                transfer_agent_name:"$transfer_agents.username",
                conference_agent_name:"$conference_agents.username"
              },
            },
          ];
        }
       
      } else {
        // console.log("no status");
        aggregateOption = [
          {
            $match: {
              createdAt: {
                $gte: from_date_starting,
                $lte: to_date_end,
              },
              agent: { $in: userarray },
            },
          },
          {
            $lookup: {
              from: "clients",
              localField: "unique_id",
              foreignField: "_id",
              as: "client",
            },
          },
          {
            $unwind: {
              path: "$client",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "agent",
              foreignField: "_id",
              as: "agentdetails",
            },
          },
          {
            $unwind: {
              path: "$agentdetails",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "conference_agent",
              foreignField: "_id",
              as: "conference_agents",
            },
          },
          {
            $lookup: {
              from: "users",
              localField: "transfer_agent",
              foreignField: "_id",
              as: "transfer_agents",
            },
          },
          {
            $project: {
              " _id": "$_id",
              agent_name: ["$agentdetails.username"],
              name: "$client.username",
              phone_number: "$phonenumber",
              email: "$email",
              channel: "$channel",
              browser: "$user_agent.browser",
              conference: "$conference",
              transferred: "$transferred",
              skillset: "$skillset",
              language: "$language",
              chat_arrival_at: "$arrival_at",
              chat_started_at: "$chat_started_at",
              chat_ended_at: "$chat_ended_at",
              status: "$status",
              transfer_agent_name:"$transfer_agents.username",
              conference_agent_name:"$conference_agents.username"
            },
          },
        ];
      }
      var agentData = await db.Session.aggregate(aggregateOption).exec();
      // var countData = await db.Session.aggregate(countValue).exec();
      // if (countData.length > 0) {
      //   var count = countData[0].count;
      // } else {
      //   var count = 0;
      // }
    }
    if (agentData.length > 0) {
      var newcallhistory=[];
      agentData.forEach(async (call) => {
        console.log(call)
        var newdata = {};
        newdata.id= call._id?call._id:"";
           newdata.agentname= call.agent_name?call.agent_name[0]:"";
               newdata.name=call.name?call.name:"";
                newdata.phonenumber=call.phone_number?call.phone_number:""
                newdata.email= call.email?call.email:"";
                newdata.channel= call.channel?call.channel:""
                newdata.browser=call.browser?call.browser:""
                newdata.conference=call.conference?call.conference:false
                newdata.transferred =call.transferred?call.transferred:false
                newdata.skillset= call.skillset?call.skillset:""
                newdata.language= call.language?call.language:""
                newdata.chat_arrival_at=call.chat_arrival_at?call.chat_arrival_at:""
                newdata.chat_started_at =call.chat_started_at?call.chat_started_at:""
                newdata.chat_ended_at=call.chat_ended_at?call.chat_ended_at:""
                newdata.status= call.status?call.status:""
                newdata.transfer_agent_name=call.transfer_agent_name?call.transfer_agent_name[0]:""
                newdata.conference_agent_name=call.conference_agent_name?call.conference_agent_name[0]:""

        newcallhistory.push(newdata);
      });
      logger.info("ListReportDetails:report details based on session id",agentData)
      // res.json({
      //   success: true,
      //   message: "Data fetched",
      //   Data: agentData,
      //  // count: count,
      // });
      if(newcallhistory.length>0){
      var temp = JSON.stringify(newcallhistory);
      temp = JSON.parse(temp);
      console.log(temp)
      var workSheet = XLSX.utils.json_to_sheet(temp);

      const workBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(workBook, workSheet, "Details");

      // Generate buffer

      XLSX.write(workBook, {
        bookType: "xlsx",
        bookSST: false,
        type: "array",
      });

      // Binary string

      XLSX.write(workBook, { bookType: "xlsx", type: "binary" });
      var randomNum = Math.round(Math.random() * 99999);
      XLSX.writeFile(
        workBook,
        path.join(
          __dirname,

          "../../public/report/" + "calldetails" + "_" + randomNum + ".xlsx"
        )
      );
      res.json({
        status: true,
        message: "/report/" + "calldetails" + "_" + randomNum + ".xlsx",
      });}
      else {
        res.json({ status: false, message: "No data found" });
      }
    } else {
      logger.error("ListReportDetails:No data Found","")
      res.json({
        success: false,
        message: "No data found",
      });
    }
  } catch (e) {
    next(e);
  }
}
async function updateagentactivity(req,res,next){
try{
         var {agent_id}=req.body;
         var data=await db.agentlastupdate.findOne({agent_id:agent_id});
         if(data){
          var value=await db.agentlastupdate.findOneAndUpdate({agent_id:agent_id},{ $set: {created_at:new Date()}})
          res.json({
            status:true,
            data:"Agent status updated successfully"
          })
         }else{
          
    const data = await db.agentlastupdate.create({
     agent_id:agent_id,
     created_at:new Date()
    });
    res.json({
      status:true,
      data:"Agent status updated successfully"
    })
         }
}catch(e){
  next(e)
}
}