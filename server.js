const http = require("http");
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
const axios = require("axios");
const path = require("path");
const configData = require("./config.json");
var useragent = require("express-useragent");
const fs = require("fs");
const crypto = require("crypto");
app.use(useragent.express());
app.use(express.json());
app.use(bodyParser.json());
const eurekaHelper = require("./_helper/eureka-helper");
const cron = require("./cron_job/cron_job");

cron.roleManage();
cron.totalCompleted();
cron.totalTransfered();
cron.totalActiveChatCount();
cron.activeinternalChat();
cron.activeChat();
cron.activeTransferedChat();
cron.activeConfernceChat();
cron.chatcountDataCreate();
cron.totalBreakTime();
cron.totalAvaliblieTime();
cron.tranferChatIncount();
cron.tranferChatOutcount();
cron.totalLoginTime();
cron.agentstatusupdate();
cron.sessionUpdate();

app.use(
  cors({
    origin: (origin, callback) => callback(null, true),
    credentials: true,
  })
);
const customerportal = require("./src/customerPortal/customerportal.service");
const authorize = require("./_middleware/authorize");
app.use("/token", customerportal.token);
app.use("/sendmessage", authorize(), customerportal.sendMessage);
app.use("/endchat", authorize(), customerportal.endChat);
var Routingservice = require("./src/customerPortal/userservice");
app.use(
  "/createsession",
  authorize(),
  Routingservice.usercreation,
  Routingservice.sessioncreation,
  Routingservice.RoutingApiNew
);
app.use("/v1/users", require("./src/users/users.controller"));
app.use("/v1/email", require("./src/email/email.controller"));
app.use("/v1/message", require("./src/message/message.controller"));
app.use(
  "/v1/scannedMessage",
  require("./src/scannedMessage/scannedMessage.controller")
);
app.use("/v1/language", require("./src/language/language.controller"));
app.use("/v1/skillset", require("./src/skillSet/skillSet.controller"));
app.use("/v1/userstatus", require("./src/status/status.controller"));
app.use("/v1/fileUpload", require("./src/fileUpload/fileUpload.controller"));
app.use(
  "/v1/dashboard",
  require("./src/dashboardCount/dashboardCount.controller")
);
app.use("/v1/inaipichat", require("./src/users/users.controller"));
app.use("/v1/inaipichat", require("./src/message/message.controller"));
app.use("/v1/reports", require("./src/reports/report.controller"));
app.use("/v1/socialmedia", require("./src/socialmedia/socialmedia.controller"));
app.use("/v1/powerBi", require("./src/powerBi/embedConfig.controller"));
var port = 8001;
app.set("port", port);
var server = http.createServer(app);
server.listen(port);

console.log("application is running on port " + port);

// Creates the endpoint for our webhook whatsapp
app.post("/webhook", async (req, res) => {
  console.log("webhook triggered from whatsapp", req.body);
  try {
    let body = req.body;
    let msg_sent_type;

    let messages = body.entry[0].changes[0].value.messages
      ? body.entry[0].changes[0].value.messages[0]
      : "";
    let contacts = body.entry[0].changes[0].value.contacts
      ? body.entry[0].changes[0].value.contacts[0]
      : "";
    //console.log(messages);
    let msgType, userMsg, msgId, userName, userNumber, file_name;
    // Regular expression to match emoji
    const regexExp =
      /(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/gi;
    let typeOfMsg = messages.type;
    console.log(typeOfMsg);
    if (messages.type == "text") {
      msgType = "text";
      msg_sent_type = "TEXT";
      userMsg = messages.text.body;
      msgId = messages.id;

      let checkNameHavingEmoji = regexExp.test(contacts.profile.name);
      if (checkNameHavingEmoji) {
        userName = messages.from;
      } else {
        userName = contacts.profile.name;
      }

      userNumber = messages.from;
    } else if (typeOfMsg == "image") {
      msg_sent_type = "IMAGE";
      let bToken = configData.whatsapp_token;
      const config = {
        headers: { Authorization: `Bearer ${bToken}` },
      };

      console.log("media id", messages.image.id);

      let getImgUrl = await axios.get(
        "https://graph.facebook.com/v15.0/" + messages.image.id,
        config
      );

      const config1 = {
        method: "get",
        url: getImgUrl.data.url, //PASS THE URL HERE, WHICH YOU RECEIVED WITH THE HELP OF MEDIA ID
        headers: {
          Authorization: `Bearer ${bToken}`,
        },
        responseType: "arraybuffer",
      };
      let response = await axios(config1);
      let fileNameMedia = "whatsappMedia_" + new Date().getTime();
      const ext = response.headers["content-type"].split("/")[1];
      fs.writeFileSync(
        path.join(__dirname, "src/whatsapp-media", `${fileNameMedia}.${ext}`),
        response.data
      );

      console.log(
        "file path",
        path.join(__dirname, "src/whatsapp-media", `${fileNameMedia}.${ext}`)
      );

      userMsg = path.join(
        __dirname,
        "src/whatsapp-media",
        `${fileNameMedia}.${ext}`
      );
      msgId = messages.id;

      let checkNameHavingEmoji = regexExp.test(contacts.profile.name);
      if (checkNameHavingEmoji) {
        userName = messages.from;
      } else {
        userName = contacts.profile.name;
      }
      userNumber = messages.from;
    } else if (typeOfMsg == "audio") {
      msg_sent_type = "AUDIO";
      let bToken = configData.whatsapp_token;
      const config = {
        headers: { Authorization: `Bearer ${bToken}` },
      };

      console.log("media id", messages.audio.id);

      let getImgUrl = await axios.get(
        "https://graph.facebook.com/v15.0/" + messages.audio.id,
        config
      );

      const config1 = {
        method: "get",
        url: getImgUrl.data.url, //PASS THE URL HERE, WHICH YOU RECEIVED WITH THE HELP OF MEDIA ID
        headers: {
          Authorization: `Bearer ${bToken}`,
        },
        responseType: "arraybuffer",
      };
      let response = await axios(config1);
      let fileNameMedia = "whatsappMedia_" + new Date().getTime();
      const ext = response.headers["content-type"].split("/")[1];
      fs.writeFileSync(
        path.join(__dirname, "src/whatsapp-media", `${fileNameMedia}.${ext}`),
        response.data
      );

      console.log(
        "file path",
        path.join(__dirname, "src/whatsapp-media", `${fileNameMedia}.${ext}`)
      );

      userMsg = path.join(
        __dirname,
        "src/whatsapp-media",
        `${fileNameMedia}.${ext}`
      );
      msgId = messages.id;

      let checkNameHavingEmoji = regexExp.test(contacts.profile.name);
      if (checkNameHavingEmoji) {
        userName = messages.from;
      } else {
        userName = contacts.profile.name;
      }
      userNumber = messages.from;
    } else if (typeOfMsg == "video") {
      console.log("message type working" + typeOfMsg);
      msg_sent_type = "VIDEO";
      let bToken = configData.whatsapp_token;
      const config = {
        headers: { Authorization: `Bearer ${bToken}` },
      };

      console.log("media id", messages.video.id);

      let getImgUrl = await axios.get(
        "https://graph.facebook.com/v15.0/" + messages.video.id,
        config
      );

      const config1 = {
        method: "get",
        url: getImgUrl.data.url, //PASS THE URL HERE, WHICH YOU RECEIVED WITH THE HELP OF MEDIA ID
        headers: {
          Authorization: `Bearer ${bToken}`,
        },
        responseType: "arraybuffer",
      };
      let response = await axios(config1);
      let fileNameMedia = "whatsappMedia_" + new Date().getTime();
      const ext = response.headers["content-type"].split("/")[1];
      fs.writeFileSync(
        path.join(__dirname, "src/whatsapp-media", `${fileNameMedia}.${ext}`),
        response.data
      );

      console.log(
        "file path",
        path.join(__dirname, "src/whatsapp-media", `${fileNameMedia}.${ext}`)
      );

      userMsg = path.join(
        __dirname,
        "src/whatsapp-media",
        `${fileNameMedia}.${ext}`
      );
      msgId = messages.id;

      let checkNameHavingEmoji = regexExp.test(contacts.profile.name);
      if (checkNameHavingEmoji) {
        userName = messages.from;
      } else {
        userName = contacts.profile.name;
      }
      userNumber = messages.from;
    } else {
      msg_sent_type = "APPLICATION";
      let bToken = configData.whatsapp_token;
      const config = {
        headers: { Authorization: `Bearer ${bToken}` },
      };

      console.log("media id", messages.document.id);

      let getImgUrl = await axios.get(
        "https://graph.facebook.com/v15.0/" + messages.document.id,
        config
      );

      const config1 = {
        method: "get",
        url: getImgUrl.data.url, //PASS THE URL HERE, WHICH YOU RECEIVED WITH THE HELP OF MEDIA ID
        headers: {
          Authorization: `Bearer ${bToken}`,
        },
        responseType: "arraybuffer",
      };
      let response = await axios(config1);
      let fileNameMedia = "whatsappMedia_" + new Date().getTime();
      const ext = response.headers["content-type"].split("/")[1];
      fs.writeFileSync(
        path.join(__dirname, "src/whatsapp-media", `${fileNameMedia}.${ext}`),
        response.data
      );

      file_name = `${fileNameMedia}.${ext}`;

      console.log(
        "file path",
        path.join(__dirname, "src/whatsapp-media", `${fileNameMedia}.${ext}`)
      );

      userMsg = path.join(
        __dirname,
        "src/whatsapp-media",
        `${fileNameMedia}.${ext}`
      );
      msgId = messages.id;

      let checkNameHavingEmoji = regexExp.test(contacts.profile.name);
      if (checkNameHavingEmoji) {
        userName = messages.from;
      } else {
        userName = contacts.profile.name;
      }
      userNumber = messages.from;
    }

    if (userNumber) {
      let check_session = await axios.post(
        configData.customurl+"/v1/inaipichat/checkSessionAvailable/" +
          userNumber
      );

      if (check_session.data.success) {
        console.log("session already available");
        let data = {
          chat_session_id: check_session.data.data.session_id,
          message: userMsg,
          msg_sent_type: msg_sent_type,
          file_name: file_name,
        };

        // Sending post data to API URL
        axios
          .post(configData.customurl+"/v1/inaipichat/addMessageNew", data)
          .then((res) => {
            console.log("Body: ", res.data);
          })
          .catch((err) => {
            console.error(err);
          });
      } else {
        console.log("create new session");
        let data = {
          name: userName,
          email_id: userName + "@whatsapp.com",
          phone: userNumber,
          channel: "from_whatsapp",
          latitude: "",
          longitute: "",
          client_ip: "",
          device_type: "api",
          whatsapp_msg_id: msgId,
          message: userMsg,
          msg_sent_type: msg_sent_type,
          file_name: file_name,
          skillset: "Insurance",
          language: "Arabic",
        };

        // Sending post data to API URL
        axios
          .post(configData.customurl+"/v1/inaipichat/createsession", data)
          .then((res) => {
            console.log(`Status: ${res.data}`);
          })
          .catch((err) => {
            console.error(err);
          });
      }
    }

    res.status(200).send("EVENT_RECEIVED");
  } catch (e) {
    res.status(200).send("comes catch block");
  }
});

app.post("/webhookfacebook", async (req, res) => {
  console.log("webhook triggered from whatsapp");
  try {
    let body = req.body;

    // console.log(body.entry[0].messaging[0].message.attachments[0].payload);
    let msg_sent_type;

    let messages = body.entry[0].messaging[0].message
      ? body.entry[0].messaging[0].message
      : "";
    let contacts = body.entry[0].messaging[0]
      ? body.entry[0].messaging[0].message
      : "";

    let msgType, userMsg, msgId, userName, userNumber, file_name;
    // Regular expression to match emoji
    const regexExp =
      /(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff])/gi;
    let typeOfMsg = body.entry[0].messaging[0].message.attachments
      ? body.entry[0].messaging[0].message.attachments[0].type
      : "text";
    console.log(typeOfMsg);
    if (typeOfMsg == "text") {
      msgType = "text";
      msg_sent_type = "TEXT";
      userMsg = messages.text;
      msgId = messages.mid;

      //let checkNameHavingEmoji = regexExp.test(contacts.profile.name);
      // if (checkNameHavingEmoji) {
      userName = body.entry[0].messaging[0].sender.id;
      // } else {
      //   userName = contacts.profile.name;
      // }

      userNumber = body.entry[0].messaging[0].sender.id;
    } else if (typeOfMsg == "image") {
      msg_sent_type = "IMAGE";
      userMsg = messages.attachments[0].payload.url;
      msgId = messages.mid;
      userName = body.entry[0].messaging[0].sender.id;
      userNumber = body.entry[0].messaging[0].sender.id;
    } else if (typeOfMsg == "audio") {
      msg_sent_type = "AUDIO";
      userMsg = messages.attachments[0].payload.url;
      msgId = messages.mid;
      userName = body.entry[0].messaging[0].sender.id;
      userNumber = body.entry[0].messaging[0].sender.id;
    } else if (typeOfMsg == "video") {
      console.log("message type working" + typeOfMsg);
      msg_sent_type = "VIDEO";
      userMsg = messages.attachments[0].payload.url;
      msgId = messages.mid;
      userName = body.entry[0].messaging[0].sender.id;
      userNumber = body.entry[0].messaging[0].sender.id;
    } else {
      msg_sent_type = "APPLICATION";
      userMsg = messages.attachments[0].payload.url;
      msgId = messages.mid;
      userName = body.entry[0].messaging[0].sender.id;
      userNumber = body.entry[0].messaging[0].sender.id;
    }

    if (userNumber) {
      let check_session = await axios.post(
        configData.customurl+"/v1/inaipichat/checkSessionAvailable/" +
          userNumber
      );

      if (check_session.data.success) {
        console.log("session already available");
        let data = {
          chat_session_id: check_session.data.data.session_id,
          message: userMsg,
          msg_sent_type: msg_sent_type,
          file_name: file_name,
        };

        // Sending post data to API URL
        axios
          .post(configData.customurl+"/v1/inaipichat/addMessageNew", data)
          .then((res) => {
            console.log("Body: ", res);
          })
          .catch((err) => {
            console.error(err);
          });
      } else {
        console.log("create new session");
        let data = {
          name: userNumber,
          email_id: userNumber + "@facebook.com",
          phone: userNumber,
          channel: "from_facebook",
          latitude: "",
          longitute: "",
          client_ip: "",
          device_type: "api",
          whatsapp_msg_id: msgId,
          message: userMsg,
          msg_sent_type: msg_sent_type,
          file_name: file_name,
          skillset: "Insurance",
          language: "Arabic",
        };

        console.log("new", data);
        // Sending post data to API URL
        axios
          .post(configData.customurl+"/v1/inaipichat/createsession", data)
          .then((res) => {
            console.log(`Status: ${JSON.stringify(res.data)}`);
          })
          .catch((err) => {
            console.error(err);
          });
      }
    }

    res.status(200).send("EVENT_RECEIVED");
  } catch (e) {
    res.status(200).send("comes catch block");
  }
});
// Adds support for GET requests to our webhook
app.get("/webhook", (req, res) => {
  let VERIFY_TOKEN = "inaipifacebooktoken";

  // Parse the query params
  let mode = req.query["hub.mode"];
  let token = req.query["hub.verify_token"];
  let challenge = req.query["hub.challenge"];
  res.status(200).send(challenge);
});

app.get("/webhookfacebook", (req, res) => {
  let VERIFY_TOKEN = "inaipifacebooktoken";

  // Parse the query params
  let mode = req.query["hub.mode"];
  let token = req.query["hub.verify_token"];
  let challenge = req.query["hub.challenge"];
  res.status(200).send(challenge);
});

//Webhook for twiiter

app.get("/webhooktwitter", (req, res) => {
  //console.log("crc",req.query["crc_token"]);
  const secret = "tFKZxMJUxofypRoIiOnJvi7B28FjUl65OE3AciP1QD1c8yEvIY";
  const crc_token = req.query["crc_token"];
  const hmac = crypto.createHmac("sha256", secret).update(crc_token).digest();
  const encoded = "sha256=" + Buffer.from(hmac).toString("base64");
  const response = {
    response_token: encoded,
  };
  console.log(response);
  res.status(200).send(JSON.stringify(response));
  //res.json(response);
  //  res.status(200).send({"response_token"=>""});
});

app.post("/webhooktwitter", async (req, res) => {
  try {
    var array = req.body;
    console.log(req.body);
    var array = req.body;
    if (array.direct_message_events && array.direct_message_events[0]) {
      if (
        array.direct_message_events[0].message_create &&
        array.direct_message_events[0].message_create.message_data
      ) {
        const message_data = array.direct_message_events[0].message_create;
        const sender_id = message_data.sender_id;
        const recipient_id = message_data.target.recipient_id;
        let message = message_data.message_data.text;
        const sender_email = array.users[sender_id].name;
        const sender_name = array.users[sender_id].screen_name;

        let msg_sent_type = "TEXT";
        let userMsg = message;
        let msgId = "1234";
        let userName = sender_name;
        let userNumber = sender_id;
        let file_name = "";
        //  console.log();
        if (array.users[sender_id].screen_name != "inaipiapp") {
          if (userNumber) {
            let check_session = await axios.post(
              configData.customurl+"/v1/inaipichat/checkSessionAvailable/" +
                userNumber,
              {},
              {
                headers: {
                  "Content-Type": "application/json",
                  tenant: "tenant-1",
                },
              }
            );

            if (check_session.data.success) {
              console.log("session already available");
              let data = {
                chat_session_id: check_session.data.data.session_id,
                message: userMsg,
                msg_sent_type: msg_sent_type,
                file_name: file_name,
              };

              // Sending post data to API URL
              axios
                .post(
                  configData.customurl+"/v1/inaipichat/addMessageNew",
                  data,
                  {
                    headers: {
                      "Content-Type": "application/json",
                      tenant: "tenant-1",
                    },
                  }
                )
                .then((res) => {
                  console.log("Body: ", res);
                })
                .catch((err) => {
                  console.error(err);
                });
            } else {
              console.log("create new session");
              let data = {
                name: userName,
                email_id: sender_email + "@twitter.com",
                phone: userNumber,
                channel: "from_twitter",
                latitude: "",
                longitute: "",
                client_ip: "",
                device_type: "api",
                whatsapp_msg_id: msgId,
                message: userMsg,
                msg_sent_type: msg_sent_type,
                file_name: file_name,
                skillset: "Insurance",
                language: "Arabic",
              };

              console.log("new", data);
              // Sending post data to API URL
              axios
                .post(
                  configData.customurl+"/v1/inaipichat/createsession",
                  data,
                  {
                    headers: {
                      "Content-Type": "application/json",
                      tenant: "tenant-1",
                    },
                  }
                )
                .then((res) => {
                  console.log(`Status: ${JSON.stringify(res.data)}`);
                })
                .catch((err) => {
                  console.error(err);
                });
            }
          }
          // Do something with the variables
        } else {
          console.log("Old session");
        }
      }

      res.status(200).send("EVENT_RECEIVED");
    }
  } catch (e) {
    res.status(200).send("comes catch block");
  }
});

app.post("/webhookteams", async (req, res) => {
  console.log("webhook triggered from whatsapp", req.body);

  const conversationId = req.body.conversation.id;
  const userId = req.body.from.id;
  const sender_name = req.body.from.name;
  const message = req.body.text;

  if (req.body.text) {
    let msg_sent_type = "TEXT";
    let userMsg = message;
    let msgId = conversationId;
    let userName = sender_name;
    let userNumber = userId;
    let file_name = "";

    if (userNumber) {
      let check_session = await axios.post(
        configData.customurl+"/v1/inaipichat/checkSessionAvailable/" +
          userNumber,
        {},
        {
          headers: {
            "Content-Type": "application/json",
            tenant: "tenant-1",
          },
        }
      );

      if (check_session.data.success) {
        console.log("session already available");
        let data = {
          chat_session_id: check_session.data.data.session_id,
          message: userMsg,
          msg_sent_type: msg_sent_type,
          file_name: file_name,
        };

        // Sending post data to API URL
        axios
          .post(configData.customurl+"/v1/inaipichat/addMessageNew", data, {
            headers: {
              "Content-Type": "application/json",
              tenant: "tenant-1",
            },
          })
          .then((res) => {
            console.log("Body: ", res);
          })
          .catch((err) => {
            console.error(err);
          });
      } else {
        console.log("create new session");
        let data = {
          name: userName,
          email_id: userName + "@teams.com",
          phone: userNumber,
          channel: "from_teams",
          latitude: "",
          longitute: "",
          client_ip: "",
          device_type: "api",
          whatsapp_msg_id: msgId,
          message: userMsg,
          msg_sent_type: msg_sent_type,
          file_name: file_name,
          skillset: "Insurance",
          language: "Arabic",
        };

        console.log("new", data);
        // Sending post data to API URL
        axios
          .post(configData.customurl+"/v1/inaipichat/createsession", data, {
            headers: {
              "Content-Type": "application/json",
              tenant: "tenant-1",
            },
          })
          .then((res) => {
            console.log(`Status: ${JSON.stringify(res.data)}`);
          })
          .catch((err) => {
            console.error(err);
          });
      }
    }
  }
  res.status(200).send("EVENT_RECEIVED");
});

app.use("/files", express.static(path.join(__dirname, "public")));

app.use(express.static(path.join(__dirname, "frontend/build")));
app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "frontend/build", "index.html"));
});

if (configData.env == "Production") {
  eurekaHelper.registerWithEureka("chat-service", port);
}
