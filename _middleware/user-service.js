const db = require("../_helper/db");
const requestIp = require("request-ip");
const axios = require("axios");
const config = require("../config.json");
const io = require("socket.io-client");
let socket = io();
const { redisconfig } = require("../_helper/redis.config");
var logger = require("../_helper/logger");
let redisClient;
(async () => {
  redisClient = redisconfig();

  redisClient.on("error", (error) => {
    lastredisclient = undefined;
    console.error(`Error : ${error}`);
  });
  redisClient.on("connect", async (data) => {
    console.error(`Redis Started`);
    lastredisclient = "Connected";
    // await redisClient.HSET('myHash', '4', JSON.stringify({ id: 4, name: 'Bob' }), (err, result) => { if (err) { console.error(err); return; } console.log(result);}) // logs: 1 })
  });
  await redisClient.connect();
})();
module.exports = {
  usercreation,
  sessioncreation,
  RoutingApiNew,
};
async function usercreation(req, res, next) {
  try {
    let body = req.body;
    var { name, email_id, phone } = req.body;
    let check_user_old = await db.Client.findOne({
      $or: [
        {
          email: req.body.email_id,
        },
        {
          phonenumber: phone,
        },
      ],
    });

    if (check_user_old) {
      if (check_user_old.email == "" || check_user_old.username == "") {
        var clientname = await db.Client.findByIdAndUpdate({ _id: check_user_old.id },
          {
            username: name,
            email: email_id
          },
          { new: true })
        req.body.name = clientname.username;
        req.body.email_id = clientname.email;
      }
      else {
        req.body.name = check_user_old.username;
        req.body.email_id = check_user_old.email;
      }
      req.body.customer_id = check_user_old.id;
      req.body.phonenumber = check_user_old.phonenumber;
      req.body.client = check_user_old;
      next();
    } else {
      var result = await new db.Client({
        username: name,
        email: email_id,
        phonenumber: phone,
      }).save();
      req.body.customer_id = result.id;
      req.body.name = name;
      req.body.email_id = email_id;
      req.body.phonenumber = phone;
      req.body.client = result;

      next();
    }
  } catch (error) {
    // next(error);
    logger.error("CreateSession:Unable to create session please try after sometime", error)
    res.json({
      status: false,
      msg: "Unable to create session please try after sometime",
      data: error,
    });
    return;
  }
}

async function sessioncreation(req, res, next) {
  try {

    var client_ip = requestIp.getClientIp(req);
    var user_agent = req.useragent;

    let check_user_active = await db.Session.findOne({
      unique_id: req.body.customer_id,
      chat_type: "external",
      status: {
        $in: ["newjoin", "Accept"],
      },
    });

    if (check_user_active) {
      logger.error("CreateSession:Session Already availble", check_user_active)
      res.json({
        status: false,
        msg: "Session already available",
        data: check_user_active,
      });
      return;
      // next();
    } else {
      var chat_session_id = "session" + Math.random().toString(16).slice(2);
      var session = await new db.Session({
        phonenumber: req.body.phonenumber,
        username: req.body.name,
        email: req.body.email_id,
        channel: req.body.channel,
        chat_session_id: chat_session_id,
        unique_id: req.body.customer_id,
        arrival_at: new Date(),
        chat_started_at: "",
        chat_ended_at: "",
        skillset: req.body.skillset,
        language: req.body.language,
        complaint: req.body.complaint,
        lattitude: req.body.latitude,
        longitude: req.body.longitute,
        client_ip,
        whatsapp_msg_id: req.body.whatsapp_msg_id,
        user_agent: user_agent,
      }).save();

      req.body.chat_session_id = chat_session_id;
      req.body.session_id = session._id;

      next();
    }
  } catch (error) {
    logger.error("CreateSession:Unable to create session please try after sometime", error)
    res.json({
      status: false,
      msg: "Unable to create session please try after sometime",
      data: error,
    });
    return;
  }
}
async function RoutingApiNew(req, res, next) {
  try {
    let data = {
      phoneNumber: req.body.phone,
      serviceRequestType: "",
      customerLanguage: req.body.language,
      customerLanguageCode: "",
      LastHandledAgentID: "",
      routingRuleId: "",
      skillSet: req.body.skillset,
      chat_session_id: req.body.chat_session_id,
    };
    // var  head = {
    //   headers: { TenantID: config_file.TenantID },
    // };
    // var value = {enabled:true};
    console.log("hi")
    //var tenantId=req.headers.tenantId;
    var variable = JSON.stringify(req.headers)
    var variable1 = JSON.parse(variable)
    console.log(variable1.tenantid);

    var tenantId = variable1.tenantid;
    //console.log(queuedetails.data.data.queueMessage);
    if (config.route != "Bot") {
      if (config.env == "Production") {
        await axios.post(config.routingbaseurl + "/routingengine/engine/bestRoutev1", data).then(async (resp) => {
          if (resp.data.status == 1001) {
            if (resp.data.value != null) {

              req.body.availableAgent = resp.data.value.userId;


              let findAgent = await db.User.find({
                user_id: req.body.availableAgent,
              });

              if (findAgent[0] != undefined) {
                var updateresult = await db.Session.findByIdAndUpdate(
                  req.body.session_id,
                  {
                    $set: {
                      user_id: req.body.availableAgent,
                      available_agent: findAgent[0]._id,
                      agent: findAgent[0]._id,
                    },
                  }
                ).populate("unique_id", "username email phonenumber");

                if (updateresult) {
                  let incomingreq = await db.Session.find({
                    chat_session_id: req.body.chat_session_id,
                  }).populate("unique_id", "username email phonenumber");
                  let agent_id_incoming = JSON.stringify(findAgent[0]._id);

                  if (redisClient != undefined) {
                    var updateuser = await db.User.findByIdAndUpdate({_id:findAgent[0]._id}, { $inc: { queued_count: 1 } })
                    
                    const cacheResults = await redisClient.get("agentsDetails");
                    let cache_resp = JSON.parse(cacheResults);
                    if (cache_resp) {
                      var agentStatusnew = [];
                   var updatedlist= await cache_resp.filter((value) => {
                 
                    return (value["userId"] != req.body.availableAgent)
            
                  })
                  console.log("updatedlist",updatedlist)
                  await redisClient.set("agentsDetails", JSON.stringify(updatedlist));
                      cache_resp.forEach(async (item) => {
                        if (item["userId"] == req.body.availableAgent) {
                          item["chat"].queued_count = item["chat"].queued_count + 1;
                          logger.info(findAgent[0]._id + ":Create Session Tested Queued Count" + item["chat"].queued_count)
                          logger.info(findAgent[0]._id + ":Create Session Tested active chat count" + item["chat"].active_chat_count)
                          logger.info(findAgent[0]._id + ":Create Session Tested Total Ongoing" + item["chat"].total_ongoing)
                          
                          agentStatusnew = item;
                        }
                      });
                      await redisClient.set("agentsDetails", JSON.stringify(cache_resp));
                    }
                    var havalue = await redisClient.HINCRBY(JSON.stringify("queuecount"), JSON.stringify(findAgent[0]._id), 1)
                    var v = await redisClient.hSet(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0]))
                    console.log("value", v)
                    logger.info(req.body.chat_session_id + ":CreateSession" + JSON.stringify(v))
                  }
                  var agentActivity = await db.agentActivity({ agent_id: findAgent[0]._id, user_id: req.body.availableAgent, activity_name: "USER_INCOMING_REQUEST", value: req.body.chat_session_id, created_at: new Date() }).save();
                  socket = io(config.socketUrl + tenantId);

                  socket.emit("send-new-req", incomingreq[0]);
                  if (req.body.channel != "webchat") {

                    let datatomessage = {
                      chat_session_id: req.body.chat_session_id,
                      message: req.body.message,
                      msg_sent_type: req.body.msg_sent_type,
                      context_id: req.body.context_id,
                      captions: req.body.captions,
                      msgId: req.body.msgId
                    };
      
                    console.log("data before calling api",datatomessage)
      
                    // Sending post data to API URL
                    await axios
                      .post(config.customurl+'/v1/inaipichat/addMessageFirst', datatomessage, {
                        headers: {
                          tenantId: tenantId,
      
                        },
                      })
                      .then((res) => {
                        console.log("Body: ", res.data);
                      })
                      .catch((err) => {
                        console.error(err);
                      });
                  
                    const data = await axios.post(
                      config.customurl+`/v1/users/getId/${req.body.email_id}`
                    );
                    if (data && data.length) {
                      socket = io(config.socketUrl + tenantId);
                      socket.emit("add-user", data.data.user.id);
                    }
                  }
                  // if (req.body.channel != "webchat") {
                  //   await axios.post(
                  //     "config.customurl/v1/message/addMessageNew",
                  //     {
                  //       from: req.body.chat_session_id,
                  //       chat_session_id: req.body.chat_session_id,
                  //       to: findAgent[0]._id,
                  //       message: req.body.message,
                  //       senderName: req.body.name,
                  //       receiverName: findAgent[0].username,
                  //       messageFrom: "fromClient",
                  //       msg_sent_type: req.body.msg_sent_type,
                  //       context_id: req.body.context_id,
                  //       captions: req.body.captions,
                  //       msgId: req.body.msgId,
                  //       context_id: context_id
                  //     }
                  //   );

                  //   const data = await axios.post(
                  //     `config.customurl/v1/users/getId/${req.body.email_id}`
                  //   );
                  //   if (data && data.length) {
                  //     socket = io(config.socketUrl + tenantId);
                  //     socket.emit("add-user", data.data.user.id);
                  //   }
                  // }
                  logger.info(req.body.chat_session_id + ":CreateSession:chat session created successsfully", incomingreq)
                  var incoming_data =
                  {
                    agent_id: findAgent[0]._id,
                    Intraction_id: req.body.chat_session_id,
                    channel_id: "1",
                    Ring_time: "10s",
                    skillset: req.body.skillset,
                    customer_id:incomingreq[0].unique_id.id,
                    language:req.body.language,
                    In_conference: "true",
                    conference_parties: "supervisor",
                    Recording_In_progress: "true",
                    Is_handling_transer: "true",
                    agent_extension: findAgent[0].email,
                    Chat_type: "Inbound",
                    Chat_result:"Answered",
                    GroupId:findAgent[0].group_array[0]?findAgent[0].group_array[0]:"31188316-f485-4e02-9987-794ba938fef3",
                    Customer_Contact:req.body.phonenumber,
                    start_time:new Date(),
                    Originator_time:incomingreq[0].arrival_at,
                    Customer_Contact_email:req.body.email_id,
                    Intraction_mode_id:incomingreq[0].channel,
                    interaction_disconnected: false
                    
                  }
                  console.log("incoming", incoming_data)
                  const config1 = {
                    headers: { TenantID: tenantId },
                  };
                  await axios.post(
                    config.interactionApi + "liveReport/createLiveReport",
                    incoming_data,
                    config1
                  );
                  res.json({
                    status: true,
                    message: "chat session created successfully",
                    data: incomingreq,
                    chat_session_id: req.body.chat_session_id,
                    UserId: req.body.availableAgent,
                    chat_inititaed_at: new Date()
                  });

                  // console.log(req.body);
                  return;
                } else {
                  logger.error("CreateSession:Agent not logged in", "")
                  res.json({
                    status: false,
                    msg: "Agent Not logged In",
                    data: "",
                  });
                  return;
                }
              } else {
                logger.error("CreateSession:Unable to create session", "")
                res.json({
                  status: false,
                  msg: "Unable to create session please try after sometime",
                  data: "",
                });
                return;
              }
            } else {
              var queueMessage = queuedetails.data.data ? queuedetails.data.data.queueMessage : "Please wait, your request is in queue";
              let incomingreq = await db.Session.find({
                chat_session_id: req.body.chat_session_id,
              }).populate("unique_id", "username email phonenumber");
              await db.Session.findOneAndUpdate({ chat_session_id: req.body.chat_session_id }, { $set: { is_queued: true } });
              await axios.post(
                config.customurl+`/v1/message/addMessage`,
                {
                  from: incomingreq[0]._id,
                  to: incomingreq[0]._id,
                  message: queueMessage,
                  senderName: "Queue Message",
                  receiverName: req.body.name,
                  msg_sent_type: "TEXT",
                  messageFrom: "fromAgent",
                  userType: "external",
                  file_name: "",
                  chatdetails: incomingreq[0]
                }
              );

              const data = await axios.post(
                config.customurl+`/v1/users/getId/${req.body.email_id}`
              );
              var incoming_data =
              {
                Intraction_id: req.body.chat_session_id,
                channel_id: "1",
                Ring_time: "10s",
                skillset: req.body.skillset,
                customer_id:incomingreq[0].unique_id.id,
                language:req.body.language,
                In_conference: "true",
                conference_parties: "supervisor",
                Recording_In_progress: "true",
                Is_handling_transer: "true",
                Chat_type: "Inbound",
                Chat_result:"Queued",
                Customer_Contact:req.body.phonenumber,
                start_time:new Date(),
                Originator_time:incomingreq[0].arrival_at,
                Customer_Contact_email:req.body.email_id,
                interaction_disconnected: false
                
              }
              console.log("incoming", incoming_data)
              const config1 = {
                headers: { TenantID: tenantId },
              };
              await axios.post(
                config.interactionApi + "liveReport/createLiveReport",
                incoming_data,
                config1
              );
              if (data && data.length) {
                socket = io(config.socketUrl + tenantId);
                socket.emit("add-user", data.data.user.id);
              } logger.info(req.body.chat_session_id + ":CreateSession:Session creation in queue", incomingreq)
              res.json({
                status: true,
                message: queueMessage,
                data: incomingreq,
                chat_session_id: req.body.chat_session_id,
                UserId: null,
                chat_inititaed_at: new Date()
              });
              return;
            }
          } else {
            var queuedetails = await axios.post(config.loginbaseurl + "/usermodule/clientMaster/chatConfig/list", { enabled: true }, {
              headers: { TenantID: tenantId },
            })
            var queueMessage = queuedetails.data.data ? queuedetails.data.data.queueMessage : "Please wait, your request is in queue";
            let incomingreq = await db.Session.find({
              chat_session_id: req.body.chat_session_id,
            }).populate("unique_id", "username email phonenumber");
            await db.Session.findOneAndUpdate({ chat_session_id: req.body.chat_session_id }, { $set: { is_queued: true } });
            await axios.post(
              config.customurl+`/v1/message/addMessage`,
              {
                from: incomingreq[0]._id,
                to: incomingreq[0]._id,
                message: queueMessage,
                senderName: "Queue Message",
                receiverName: req.body.name,
                msg_sent_type: "TEXT",
                messageFrom: "fromAgent",
                userType: "external",
                file_name: "",
                chatdetails: incomingreq[0]
              }
            );

            const data = await axios.post(
              config.customurl+`/v1/users/getId/${req.body.email_id}`
            );
            var incoming_data =
              {
                Intraction_id: req.body.chat_session_id,
                channel_id: "1",
                Ring_time: "10s",
                skillset: req.body.skillset,
                customer_id:incomingreq[0].unique_id.id,
                language:req.body.language,
                In_conference: "true",
                conference_parties: "supervisor",
                Recording_In_progress: "true",
                Is_handling_transer: "true",
                Chat_type: "Inbound",
                Chat_result:"Queued",
                Customer_Contact:req.body.phonenumber,
                start_time:new Date(),
                Originator_time:incomingreq[0].arrival_at,
                Customer_Contact_email:req.body.email_id,
                interaction_disconnected: false
                
              }
              console.log("incoming", incoming_data)
              const config1 = {
                headers: { TenantID: tenantId },
              };
              await axios.post(
                config.interactionApi + "liveReport/createLiveReport",
                incoming_data,
                config1
              );
            if (data && data.length) {
              socket = io(config.socketUrl + tenantId);
              socket.emit("add-user", data.data.user.id);

              socket.emit("send-msg", {
                to: incomingreq[0]._id,
                session_id: incomingreq[0].chat_session_id,
                from: incomingreq[0]._id,
                senderName: "Queue Message",
                chatType: "outbound",
                msg: queueMessage,
                msgType: "web",
                userType: "external",
                msg_sent_type: "TEXT",
                chatdetails: incomingreq[0],
                file_name: "",
              });
            }
            logger.info(req.body.chat_session_id + ":CreateSession:Session in queue", incomingreq)
            res.json({
              status: true,
              message: "You are in queue please wait",
              data: incomingreq,
              chat_session_id: req.body.chat_session_id,
              UserId: null,
              chat_inititaed_at: new Date()
            });
            return;
          }
        });
      } else {
        req.body.availableAgent = "03bbcd2f-448b-4c55-8a95-99502c166955";


        let findAgent = await db.User.find({
          user_id: req.body.availableAgent,
        });

        if (findAgent[0] != undefined) {
          var updateresult = await db.Session.findByIdAndUpdate(
            req.body.session_id,
            {
              $set: {
                user_id: req.body.availableAgent,
                available_agent: findAgent[0]._id,
                agent: findAgent[0]._id,
              },
            }
          ).populate("unique_id", "username email phonenumber");

          if (updateresult) {
            let incomingreq = await db.Session.find({
              chat_session_id: req.body.chat_session_id,
            }).populate("unique_id", "username email phonenumber");
            let agent_id_incoming = JSON.stringify(findAgent[0]._id);
            console.log("inside")
            if (redisClient != undefined) {
              console.log("inside else")
              var havalue = await redisClient.HINCRBY(JSON.stringify("queuecount"), JSON.stringify(findAgent[0]._id), 1)
              console.log("agent_id", JSON.stringify(agent_id_incoming))
              var v = await redisClient.hSet(JSON.stringify(agent_id_incoming), JSON.stringify(incomingreq[0]._id), JSON.stringify(incomingreq[0])
              )
              console.log("value", v)

            }
            var agentActivity = await db.agentActivity({ agent_id: findAgent[0]._id, user_id: req.body.availableAgent, activity_name: "USER_INCOMING_REQUEST", value: req.body.chat_session_id, created_at: new Date() }).save();
            socket = io(config.socketUrl + tenantId);

            socket.emit("send-new-req", incomingreq[0]);

            if (req.body.channel != "webchat") {

              let datatomessage = {
                chat_session_id: req.body.chat_session_id,
                message: req.body.message,
                msg_sent_type: req.body.msg_sent_type,
                context_id: req.body.context_id,
                captions: req.body.captions,
                msgId: req.body.msgId
              };

              console.log("data before calling api",datatomessage)

              // Sending post data to API URL
              await axios
                .post(config.customurl+'/v1/inaipichat/addMessageFirst', datatomessage, {
                  headers: {
                    tenantId: tenantId,

                  },
                })
                .then((res) => {
                  console.log("Body: ", res.data);
                })
                .catch((err) => {
                  console.error(err);
                });
              // await axios.post(
              //   "config.customurl/v1/message/addMessagenew",
              //   {
              //     // from: req.body.session_id,
              //     // to: findAgent[0]._id,
              //     message: req.body.message,
              //     // senderName: req.body.name,
              //     // receiverName: findAgent[0].username,
              //     // messageFrom: "fromClient",
              //     msg_sent_type:req.body.msg_sent_type,
              //     context_id:req.body.context_id,
              //     captions:req.body.captions,
              //     msgId:req.body.msgId,
              //     chat_session_id:req.body.session_id,
              //     file_name:""
              //   }
              // );

              const data = await axios.post(
                config.customurl+`/v1/users/getId/${req.body.email_id}`
              );
              if (data && data.length) {
                socket = io(config.socketUrl + tenantId);
                socket.emit("add-user", data.data.user.id);
              }
            }
            logger.info(req.body.chat_session_id + ":CreateSession:chat session created", incomingreq)
            var incoming_data =
            {
              agent_id: findAgent[0]._id,
              Intraction_id: req.body.chat_session_id,
              channel_id: "1",
              Ring_time: "10s",
              skillset: "customer service",
              customer_id: "3002",
              language: "Arabic",
              In_conference: "true",
              conference_parties: "supervisor",
              Recording_In_progress: "true",
              after_call_work: "true",
              Is_handling_transer: "true",
              agent_extension: findAgent[0].email,
              Chat_type: "Assign Agent",
              Skill_set_name: "customer service",
              interaction_disconnected: false
            }
            console.log("incoming", incoming_data)
            const config1 = {
              headers: { TenantID: tenantId },
            };
            await axios.post(
              config.interactionApi + "liveReport/createLiveReport",
              incoming_data,
              config1
            );
            res.json({
              status: true,
              message: "chat session created successfully",
              data: incomingreq,
              chat_session_id: req.body.chat_session_id,
              UserId: req.body.availableAgent,
              chat_inititaed_at: new Date()
            });

            // console.log(req.body);
            return;
          }
        }
      }
    } else {
      let incomingreq = await db.Session.find({
        chat_session_id: req.body.chat_session_id,
      }).populate("unique_id", "username email phonenumber");
      res.json({
        status: true,
        message: "chat session created successfully",
        data: incomingreq,
        chat_session_id: req.body.chat_session_id,
        chat_inititaed_at: new Date()
      });
    }
  } catch (error) {
    logger.info(req.body.chat_session_id + ":CreateSession:Error in create session" + error)

    res.json({
      status: true,
      message: "Problem in chat session API",
      data: JSON.stringify(error),
      chat_session_id: req.body.chat_session_id,
      UserId: null,
    });
  }

}
