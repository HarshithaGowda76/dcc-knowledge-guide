const axios = require("axios");
const config = require("../config.json");

module.exports = RoutingApi;

function RoutingApi() {

    try {
        return [
            async (req, res, next) => {

                let data = { phoneNumber: req.body.phone, serviceRequestType: "", customerLanguage:  req.body.language, customerLanguageCode: "", LastHandledAgentID: "", routingRuleId: "", skillSet:  req.body.skillset };

                await axios
                    .post(config.routingbaseurl+"/routingengine/engine/bestRoutev1", data)
                    .then(async (resp) => {

                        if (resp.data.status == 1001) {

                            req.body.availableAgent = resp.data.value.agentId;
                            console.log(req.body);
                            next();
                        } else {

                            let data = { value: "03bbcd2f-448b-4c55-8a95-99502c166955" };
                            req.body.availableAgent = data.value;
                            next();
                        }
                    })

                    

            }
        ];
    }
    catch (error) {
      
        req.body.availableAgent = config.agent_id;
        next();
        
        // next(error);
    }

}
