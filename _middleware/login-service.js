const { generateJwtToken } = require("../_helper/helper");
const io = require("socket.io-client");
const db = require("../_helper/db");
let socket = io();
const axios = require("axios");
const redis = require("redis");
const config_file = require("../config.json");
let user_id_cache;
var Mongoose = require("mongoose");
var ObjectId = Mongoose.Types.ObjectId;
let redisClient;
let lastredisclient = undefined;
const { redisconfig } = require("../_helper/redis.config");
(async () => {
  redisClient = redisconfig();

  redisClient.on("error", (error) => {
    lastredisclient = undefined;
    console.error(`Error : ${error}`);
  });
  redisClient.on("connect", async (data) => {
    console.error(`Redis Started`);
    lastredisclient = "Connected";
  });
  await redisClient.connect();
})();
module.exports = {
  userlogin,
  skillsetGenrate,
  redisRoute
};
async function userlogin(req,res,next){
    try{
        
    const username = req.body.email;
    const password = req.body.pass;
    //var tenantId=req.headers.tenantId;
    var variable=JSON.stringify(req.headers)
    var variable1=JSON.parse(variable)
    console.log(variable1.tenantid);
    
    var tenantId=variable1.tenantid;
    var err = {};
    // check login
    const config = {
      headers: { TenantID: tenantId },
    };
    let data = { email: username, password: password };
    
    axios
      .post(config_file.loginbaseurl+"/usermodule/login" + "/login", data, config)
      .then(async (resp) => {
        if (resp.data.status == "OK") {
           req.body.data = resp.data.dataList[0];
           next();
         // req.body.user_id = data.userId;
        }else{
            console.log("username or password invalid");
          res.json({
            status: false,
            message: "Username or Password Invalid",
          });
          return;
        }
    }).catch((err) => {
        console.log(err);
        res.json({
          status: false,
          message: "Something Went Wrong login !",
        });
        return;
      });
    }
    catch(e){
        res.json({
            status: false,
            message: "Something Went Wrong while login !",
          });
          return;
    }
}
async function skillsetGenrate(req,res,next){
    try{
      var data=req.body.data;
      var user_id=data.userId;
     // var tenantId=req.headers.tenantId;
     var variable=JSON.stringify(req.headers)
    var variable1=JSON.parse(variable)
    console.log(variable1.tenantid);
    
    var tenantId=variable1.tenantid;
      const config = {
        headers: { TenantID: tenantId },
      };
      let datas = { userId:user_id };
      let lang_skillset = await axios.post(
        config_file.loginbaseurl+"/usermodule/clientUser/master",datas, config
      );
      req.body.languageForAgent = lang_skillset.data?lang_skillset.data.language:[{"languageId": "","proficiencyId": "","languageCode": "","languageDesc": "","proficiencyDesc": ""}];
      req.body.skillForAgent = lang_skillset.data?lang_skillset.data.skillSet:[{"skillId":"","proficiencyId":"","skillName":"","proficiencyDesc":""}];
      let userAlreadyAvailable = await db.User.findOne({
        user_id: user_id,
      });
    
     // let userDetailsForToken;
      let doc = {
        username: data.firstName + " " + data.lastName,
        password: req.body.pass,
        email: req.body.email,
        user_id: data.userId ? data.userId : "",
        mobile: data.mobileNumber,
        roles_array: data.roles,
        group_array: data.groups,
      };
      if (userAlreadyAvailable) {
        console.log("user already available , no insertion req");
        req.body.userDetailsForToken = userAlreadyAvailable;
        next();
      } else {
        console.log("new user req");
        let saveDataToDb = new db.User(doc);
        await saveDataToDb.save();
        req.body.userDetailsForToken = saveDataToDb;
        next();
      }

    }
    catch(e){
        res.json({
            status: false,
            message: "Something Went Wrong skillset!",
          });
          return;
    }
}
async function redisRoute(req,res,next){
    try{
        var userDetailsForToken=req.body.userDetailsForToken;
        var user_id= req.body.data.userId;
       // const jwtToken = generateJwtToken(userDetailsForToken);
      
        var cacheResults = [];
        var cache_resp = [];

        if (lastredisclient != undefined) {
          cacheResults = await redisClient.get("agentsDetails");
          if (cacheResults) {
            cache_resp = JSON.parse(cacheResults);
          }
          if (cache_resp.length != 0) {
            const found = cache_resp.find(
              (element) => element.userId == user_id
            );
            if (found) {
              console.log("User already exist");
            } else {
              console.log("new user plz append data");
              //const users = await db.User.findOne({ user_id: user_id });

              let users = userDetailsForToken;
              if (users) {
                let agentStatus = {
                  id: users._id,
                  firstName: users.username,
                  lastName: null,
                  mobileNumber: users.mobile,
                  email: users.email,
                  userId: users.user_id,
                  roles: users.roles_array,
                  groups: users.group_array,
                  language: req.body.languageForAgent,
                  skillSet: req.body.skillForAgent,
                  status: "Available",
                  chat: {
                    active_chat_count: 0,
                    last_chat_start_time: "",
                    last_chat_end_time: "",
                    total_ongoing: 0,
                    total_completed: 0,
                  },
                  activity: {
                    login_time: new Date(),
                  },
                };
                // socket.current = io("http://localhost:8001");
                // socket.current.emit("agent-activity-input", agentStatus);

                await redisClient.publish(
                  "agent-activity",
                  JSON.stringify(agentStatus)
                );

                cache_resp.push(agentStatus);
                // console.log(cache_resp)
                await redisClient.set(
                  "agentsDetails",
                  JSON.stringify(cache_resp)
                );
              }
            }
          } else {
            console.log("First data to redis");
            // const users = await db.User.findOne({ user_id: user_id });
            let users = userDetailsForToken;
            if (users) {
              let agentStatus = [
                {
                  id: users._id,
                  firstName: users.username,
                  lastName: null,
                  mobileNumber: users.mobile,
                  email: users.email,
                  userId: users.user_id,
                  roles: users.roles_array,
                  groups: users.group_array,
                  language: req.body.languageForAgent,
                  skillSet: req.body.skillForAgent,
                  status: "Available",
                  chat: {
                    active_chat_count: 0,
                    last_chat_start_time: "",
                    last_chat_end_time: "",
                    total_ongoing: 0,
                    total_completed: 0,
                  },
                  activity: {
                    login_time: new Date(),
                  },
                },
              ];
              //console.log("test agent status", agentStatus);
              // socket.current = io("http://localhost:8001");
              // socket.current.emit("agent-activity-input", agentStatus);

              await redisClient.publish(
                "agent-activity",
                JSON.stringify(agentStatus)
              );

              await redisClient.set(
                "agentsDetails",
                JSON.stringify(agentStatus)
              );
            }
          }
        }

        let updateStatus = await db.User.findOneAndUpdate(
          {
            user_id: user_id,
          },
          {
            $set: {
              agent_status: "Available",
            },
          }
        );

        //status available for internal chat
        let updateStatusSession = await db.Session.findOneAndUpdate(
          {
            user_id: user_id,
          },
          {
            $set: {
              agent_status: "Available",
            },
          }
        );
        socket.current = io("http://localhost:8001");
        socket.current.emit("update-agent-status", user_id);
        res.json({
          status: true,
          message: "Login Successfully",
          access_token_cognicx: req.body.data.accessToken,
          user: userDetailsForToken,
        });
    }
    catch(e){
        res.json({
            status: false,
            message: "Something Went Wrong !",
          });
          return;
    }
}