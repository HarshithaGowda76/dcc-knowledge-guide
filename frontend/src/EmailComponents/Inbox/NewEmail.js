import React, { Component } from "react";
import { Link } from "react-router-dom";
// import shortid from "shortid";
import moment from "moment";
import Button from "react-bootstrap/Button";
import SplitButton from "react-bootstrap/SplitButton";
import Dropdown from "react-bootstrap/Dropdown";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import pdfdownload from "../../assets/img/pdf.png";
import { connect } from "react-redux";

import {
  Grid,
  Container,
  Box,
  Tabs,
  Tab,
  Paper,
  IconButton,
  InputBase,
  Divider,
  Autocomplete,
  Card,
  CardHeader,
  CardContent,
  Typography,
  CardActions,
  Chip,
  Popover,
  TextField,
  Stack,
  MenuList,
  MenuItem,
  ListItemIcon,
  ListItemText,
  Fade,
  CircularProgress,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Avatar,
  ToggleButton,
  ListItem,
} from "@mui/material";
import FormatBoldIcon from "@mui/icons-material/FormatBold";
import FormatItalicIcon from "@mui/icons-material/FormatItalic";
import FormatUnderlinedIcon from "@mui/icons-material/FormatUnderlined";
import FormatColorFillIcon from "@mui/icons-material/FormatColorFill";
import FormatColorTextIcon from "@mui/icons-material/FormatColorText";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import FormatListNumberedIcon from "@mui/icons-material/FormatListNumbered";
import FormatIndentDecreaseIcon from "@mui/icons-material/FormatIndentDecrease";
import FormatIndentIncreaseIcon from "@mui/icons-material/FormatIndentIncrease";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import DownloadIcon from "@mui/icons-material/Download";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import AttachFileIcon from "@mui/icons-material/AttachFile";
import InsertPhotoIcon from "@mui/icons-material/InsertPhoto";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import Email from "../Resources/images/firstemail.png";
import Mail_write from "../Resources/images/new_email.png";
import inbox from "../Resources/images/inbox_second.png";
import drafts from "../Resources/images/draft_new.jpg";
import sentitems from "../Resources/images/dm.png";
import deleteitems from "../Resources/images/trash.png";
import junkemail from "../Resources/images/error.png";
import bold from "../Resources/images/bold1.png";
import italic from "../Resources/images/italic.png";
import underline from "../Resources/images/line.png";
import avatar from "../Resources/images/imageicon1.png";
import highlight from "../Resources/images/highlight.png";
import fontcolour from "../Resources/images/fontcolour.png";
import bullets from "../Resources/images/bullet.png";
import numberBullets from "../Resources/images/number.png";
import leftIndent from "../Resources/images/leftindent.png";
import rightIndent from "../Resources/images/rightindent.png";
import moreOptions from "../Resources/images/moreoptions.png";
import search from "../Resources/images/search.png";
import reply from "../Resources/images/reply2.png";
import replyall from "../Resources/images/replyall.png";
import forward from "../Resources/images/forward.png";
import pdf from "../Resources/images/pdf.png";
import txt from "../Resources/images/txt.png";
import word from "../Resources/images/word.png";
import "../Resources/css/email.scss";
import { CTooltip } from "@coreui/react";
import SidebarComp from "./SidebarComp";
import NavBarHeader from "../../containers/Page/NavBarHeader";
import SidebarHeader from "../../containers/Page/SidebarHeader";

import {
  Editor,
  EditorState,
  RichUtils,
  Modifier,
  convertToRaw,
  convertFromRaw,
  ContentState,
  AtomicBlockUtils,
} from "draft-js";
import { stateFromHTML } from "draft-js-import-html";
import "draft-js/dist/Draft.css";
import { ChromePicker } from "react-color";
import draftToHtml from "draftjs-to-html";
import { ToastContainer, toast } from "react-toastify";
import { ticketUrl } from "../../containers/Page/Constants/BaseUrl";
import axios from "axios";
import { event } from "jquery";

export default class NewEmail extends Component {
  constructor(props) {
    super(props);
    console.log("knowledegecontentttttt", props.action.knowledgeCategory);


    this.state = {
      nameObj: { first: "Belvin" },
      name: "Bell ",
      emails: [],
      from: { mailId: "Acct.user@gmail.com" },
      to: props.toSendlist ? props.toSendlist : [],
      toInputValue: "",
      cc: [],
      ccInputValue: "",
      bcc: [],
      bccInputValue: "",
      emailSubject: props.emailSubject,
      toError: false,
      ccError: false,
      bccError: false,
      inputText: "",
      formattedText: "",
      color: "#000000",
      iscolor: false,
      editorState: EditorState.createEmpty(),
      selectedtempFile: [],
      selectedFile: [],
      emailSubject: props.inboxContent ? props.inboxContent.subject : "",
      templateDropdownList: [],
      selectedTemplateMessage: "",
      selectedEmailUrl: props.attachmentUrl,
      knowledgeCategory: props.action.knowledgeCategory,
      myTogglechat: props.action.togglechat,
    };
    this.inputRef = React.createRef();

    // this.editorState=EditorState.createEmpty();
  }

  getFileNameFromURL = (attachmentUrl) => {
    if (!attachmentUrl) return "";
    try {
      const pathname = new URL(attachmentUrl).pathname;
      const fileName = pathname.split("/").pop();
      return fileName;
    } catch (error) {
      console.error("Invalid URL:", error);
      return ""; // Return an empty string in case of an invalid URL
    }
  };


  componentDidUpdate(prevProps) {
    if (
      this.props.attachmentUrl &&
      this.props.attachmentUrl !== prevProps.attachmentUrl
    ) {
      const fileName = this.getFileNameFromURL(this.props.attachmentUrl);
      const htmlContent = `<p><a href=${this.props.attachmentUrl} download='${fileName}' target='_blank'> ${fileName} </a></p>`;
      const contentState = stateFromHTML(htmlContent);
      const newEditorState = EditorState.createWithContent(contentState);
      this.setState({ editorState: newEditorState });
      // this.handelChangeTemplate();
    }
  }

  

  appendAnchorTagToURL = (url, anchorTag) => {
    return url + "#" + anchorTag;
  };

  componentDidMount = () => {
    // console.log('props.attachmentUrl',this.props.attachmentUrl)
    this.props.attachmentUrl;
    this.SelectTemplate();
    // this.handelChangeTemplate();
    this.props.action.knowledgeCategory;
    console.log(
      this.props.action.knowledgeCategory,
      "knowledgeCategory@@@@@@@@@"
    );
  };

  // Check if the state has changed (e.g., count has changed)

  handleChangeColor = (newColor) => {
    this.setState({ color: newColor.hex });
    console.log(newColor.hex);
    this.setState({
      editorState: RichUtils.toggleInlineStyle(
        this.state.editorState,
        "BACKGROUND-COLOR",
        newColor.hex
      ),
    });
  };

  handleTextColorChange = (color) => {
    this.setState({ color: color.hex });
    const selection = this.state.editorState.getSelection();
    const contentState = this.state.editorState.getCurrentContent();

    // Apply the text color to the selected text
    const contentStateWithColor = Modifier.applyInlineStyle(
      contentState,
      selection,
      "red"
    );
    console.log("updated", contentStateWithColor);
    // Update the editor state with the new content
    const newEditorState = EditorState.push(
      this.state.editorState,
      contentStateWithColor,
      "change-inline-style"
    );

    this.setState({ editorState: newEditorState });
  };

  handleFontSizeChange = (fontSize) => {
    const { editorState } = this.state;
    const selection = editorState.getSelection();
    const nextContentState = RichUtils.toggleInlineStyle(
      editorState.getCurrentContent(),
      `FONT-SIZE-${fontSize}`
    );
    const nextEditorState = EditorState.push(
      editorState,
      nextContentState,
      "change-inline-style"
    );
    this.setState({
      editorState: RichUtils.toggleInlineStyle(nextEditorState, fontSize),
    });
  };

  handleEditorChange = (newEditorState) => {
    console.log("editor<<<<<>>>>", newEditorState);
    this.setState({ editorState: newEditorState });
  };

  handleBoldClick = () => {
    this.setState({
      editorState: RichUtils.toggleInlineStyle(this.state.editorState, "BOLD"),
    });
    console.log(
      "EditorState",
      RichUtils.toggleInlineStyle(this.state.editorState, "BOLD")
    );
    console.log("EditorOriginal", this.state.editorState);
  };

  handleItalicClick = () => {
    this.setState({
      editorState: RichUtils.toggleInlineStyle(
        this.state.editorState,
        "ITALIC"
      ),
    });
  };

  handlePasetImage = (files) => {
    console.log("Pasted File", files);
    const file = files[0];
    const reader = new FileReader();
    const { editorState } = this.state;
    reader.onload = (e) => {
      const contentState = editorState.getCurrentContent();

      const contentStateWithEntity = contentState.createEntity(
        "IMAGE",
        "IMMUTABLE",
        { src: e.target.result }
      );

      const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
      const newContentState = AtomicBlockUtils.insertAtomicBlock(
        contentStateWithEntity,
        entityKey,
        " "
      );

      const newEditorState = EditorState.push(
        editorState,
        newContentState,
        "insert-fragment"
      );
      this.handleEditorChange(newEditorState);
    };
    reader.readAsDataURL(file);
  };
  insertImage = (url) => {
    const { editorState } = this.state;
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      "IMAGE",
      "IMMUTABLE",
      { src: url }
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, {
      currentContent: contentStateWithEntity,
    });
    return AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, "");
  };

  handleButtonClickUnderline = () => {
    this.setState({
      editorState: RichUtils.toggleInlineStyle(
        this.state.editorState,
        "UNDERLINE"
      ),
    });
  };
  onToChange = (e, value) => {
    const regex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let errorEmail = value.find((email) => !regex.test(email));
    if (errorEmail) {
      this.setState({ toInputValue: errorEmail, toError: true });
    } else {
      this.setState({ toError: false });
    }

    this.setState({ to: value.filter((email) => regex.test(email)) });
  };
  onCcChange = (e, value) => {
    const regex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let errorEmail = value.find((email) => !regex.test(email));
    if (errorEmail) {
      this.setState({ ccInputValue: errorEmail, ccError: true });
    } else {
      this.setState({ ccError: false });
    }
    this.setState({ cc: value.filter((email) => regex.test(email)) });
  };
  onBccChange = (e, value) => {
    const regex =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let errorEmail = value.find((email) => !regex.test(email));
    if (errorEmail) {
      this.setState({ bccInputValue: errorEmail, bccError: true });
    } else {
      this.setState({ bccError: false });
    }
    this.setState({ bcc: value.filter((email) => regex.test(email)) });
  };
  onToInputChange = (e, newValue) => {
    this.setState({ toInputValue: newValue });
  };
  onCcInputChange = (e, newValue) => {
    this.setState({ ccInputValue: newValue });
  };
  onBccInputChange = (e, newValue) => {
    this.setState({ bccInputValue: newValue });
  };
  handleEmailSubject = (e) => {
    this.setState({ emailSubject: this.props.inboxContent.subject });
  };
  openActionView = (e, value) => {
    this.setState({
      actionView: true,
      anchorEl: e.currentTarget,
      valueIndex: value,
    });
  };
  closeActionView = () => {
    this.setState({ actionView: false, anchorEl: null, valueIndex: {} });
  };

  filesizes = (bytes, decimals = 2) => {
    if (bytes === 0) return "0 Bytes";
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  };
  FileInputChange = (e) => {
    let images = [];
    let tempDoc = this.state.selectedtempFile;
    _.map(e.target.files, (val, i) => {
      images.push(val);
      let reader = new FileReader();
      let file = val;
      reader.onloadend = () => {
        let tempValue = {
          // id: shortid.generate(),
          filename: val.name,
          filetype: val.type,
          fileimage: reader.result,
          datetime: val.lastModifiedDate.toLocaleString("en-IN"),
          filesize: this.filesizes(val.size),
        };
        tempDoc.push(tempValue);
        this.setState({ selectedtempFile: tempDoc });
      };
      if (val) {
        reader.readAsDataURL(file);
      }
    });
    this.setState({ selectedFile: e.target.files });
    // this.setState({ selectedtempFile : tempDoc });
  };

  DeleteSelectFile = (id) => {
    if (window.confirm("Are you sure you want to delete this Image?")) {
      const result = this.state.selectedtempFile.filter(
        (data) => data.id !== id
      );
      this.setState({ selectedtempFile: result });
    } else {
      // alert('No');
    }
  };

  SelectTemplate = () => {
    const Username = "imperium";
    const Password = "Indesk@$123#";

    const credentials = btoa(`${Username}:${Password}`);

    let data = {
      organization_id: "13",
      userid: 505,
      pagename: "Masters_viewmasters",
      role_id: 66,
    };

    let payload = {
      method: "post",
      maxBodyLength: Infinity,
      url: ticketUrl + "/api_new/index.php/emailtemplatelist",
      headers: {
        Authorization: `Basic ${credentials}`,
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios
      .request(payload)
      .then((response) => {
        if (response) {
          console.log("TemplateEmail", response.data.data.emailtp_list);
          this.setState({
            templateDropdownList: response.data.data.emailtp_list,
          });
        } else {
          console.log("error");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  handleSendEmail = () => {
    console.log("clicked from newmail ");
    const { selectedFile, editorState, to, cc, bcc, emailSubject } = this.state;
    console.log("newemailAllState", this.state);
    const { inboxContent, agentId } = this.props;
    const contentState = editorState.getCurrentContent();
    const html = draftToHtml(convertToRaw(contentState));

    const urlRegex = /href="([^"]+)"/;
    const fileNameRegex = />([^<]+)</;

    const urlMatch = html.match(urlRegex);
    const fileNameMatch = html.match(fileNameRegex);
    const url = urlMatch ? urlMatch[1] : null;
    const fileName = fileNameMatch ? fileNameMatch[1] : null;

    const newhtml = html;

    console.log("newhtml", newhtml);
    let requestID = "";
    let uid = 0;
    if (inboxContent) {
      let tempStr = inboxContent.subject;
      requestID = tempStr.slice(
        tempStr.indexOf("-") + 1,
        tempStr.lastIndexOf("]")
      );
      uid = inboxContent.uid;
    }

    if (html) {
      let formdata = new FormData();
      formdata.append("files", selectedFile);
      formdata.append("email", to.toString());
      formdata.append("subject", emailSubject);
      formdata.append("ccData", cc.toString());
      formdata.append("bccData", bcc.toString());
      formdata.append("html", newhtml);
      formdata.append("requestID", requestID);
      formdata.append("agentID", agentId.user_id);
      formdata.append("receivedTime", moment().format());
      formdata.append("uid", uid);

      const config = {
        headers: {
          "content-type": "multipart/form-data",
          tenantId: "123456",
        },
      };

      const url = "https://gway.release.inaipi.ae/routeemail/email/sendEmail";
      axios
        .post(url, formdata, config)
        // setLoading(true)
        .then((response) => {
          if (response.data.status) {
            setLoading(false);
            let pic_url = response.data.data.signedUrl;
            let mediaType = response.data.data.mediaType.toUpperCase();
            props.handleSendMsg(pic_url, "AUDIO", "");
            setFile("");
            setFileTypeStore("");
          }
        });
      console.log("allFormadastsaconsorl", formdata);
      this.props.action.SendEmail(formdata);
      this.props.action.closeWriteEmail();
      // this.props.sendMail(formdata);
      // this.props.closeReplaypage();
      toast.success("Message Sent Successfully", {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  handelChangeTemplate = (event) => {
    console.log("selectedOptionIdEmailtemplate<<<<>>>>>>", event.target.value);
    this.setState({ selectedTemplateMessage: event.target.value });

    // const content = ContentState.createFromText(event.target.value);
    // const newEditorState =EditorState.push(this.state.editorState, content, 'insert-characters');

    //this.setState({EditorState})

    const htmlContent = "<p>This is <b>HTML</b> content.</p>";
    const contentState = stateFromHTML(event.target.value);
    const newEditorState = EditorState.createWithContent(contentState);

    this.setState({ editorState: newEditorState });
  };

  handelSendUrl = (event) => {
    this.setState({ selectedTemplateMessage: this.props.attachmentUrl });

    // const content = ContentState.createFromText(event.target.value);
    // const newEditorState =EditorState.push(this.state.editorState, content, 'insert-characters');

    //this.setState({EditorState})

    // const htmlContent = '<p>This is <b>HTML</b> content.</p>';
    // const contentState = stateFromHTML(event.target.value);
    // const newEditorState = EditorState.createWithContent(contentState);

    this.setState({ editorState: newEditorState });
  };

  EmailAttachmentContent(data) {
    const imageFormat = [
      "png",
      "jpg",
      "jpeg",
      "jfif",
      "pjpeg",
      "pjp",
      "svg",
      "gif",
    ];
    if (!_.isEmpty(data)) {
      return (
        <>
          <Grid container spacing={1}>
            {_.map(data, (val, i) => {
              if (val.filename.match(/.(jpg|jpeg|png|gif|svg)$/i)) {
                return (
                  <Grid item xs={3}>
                    <div
                      className="attachment_container"
                      onClick={(e) => this.openActionView(e, val)}
                    >
                      <img
                        src={val.fileimage}
                        alt="Avatar"
                        className="image_attachment"
                      />
                      <div className="attachment_overlay">
                        <div className="attachment_text">
                          {val.filename}
                          {/* <span> <IconButton className='text-white align-right' onClick={(e)=>this.openActionView(e,val)} > <ExpandMoreIcon /> </IconButton>  </span> */}
                        </div>
                      </div>
                    </div>
                  </Grid>
                );
              }
            })}
          </Grid>

          <Grid container spacing={1}>
            {_.map(data, (val, i) => {
              if (!val.filename.match(/.(jpg|jpeg|png|gif|svg)$/i)) {
                return (
                  <Grid item xs={4}>
                    <Paper>
                      <MenuItem>
                        <ListItemIcon className="attachment_Preview_image">
                          <img
                            src={
                              val.filename.match(/.(pdf)$/i)
                                ? pdf
                                : val.filename.match(/.(docx)$/i)
                                ? word
                                : val.filename.match(/.(xlsx)$/i)
                                ? word
                                : txt
                            }
                            height="20px"
                            weight="20px"
                          />
                        </ListItemIcon>
                        <ListItemText>
                          <span className="attachment_el_text">
                            {val.filename}
                          </span>
                        </ListItemText>
                        <Typography variant="body2" color="text.secondary">
                          <IconButton
                            onClick={(e) => this.openActionView(e, val)}
                          >
                            {" "}
                            <ExpandMoreIcon />{" "}
                          </IconButton>
                        </Typography>
                      </MenuItem>
                    </Paper>
                  </Grid>
                );
              }
            })}
          </Grid>
        </>
      );
    }
  }

  handleBulletPointsToggle = () => {
    //   alert("work");
    this.setState((prevState) => ({
      editorState: RichUtils.toggleBlockType(
        prevState.editorState,
        "unordered-list-item"
      ),
    }));
  };

  handleNumberedListToggle = () => {
    this.setState((prevState) => ({
      editorState: RichUtils.toggleBlockType(
        prevState.editorState,
        "ordered-list-item"
      ),
    }));
  };

  renderCard() {
    const { attachmentUrl } = this.props;
    const fileName = this.getFileNameFromURL(attachmentUrl);

    if (attachmentUrl && fileName) {
      return (
        <Grid item xs={3} spacing={1}>
          <div
            className="card  attachment_container"
            style={{ height: "30px", width: "240px" }}
          >
            <img src={pdfdownload} alt="Avatar" className="pdf_attachment" />

            <div className="card-body attachment_overlay_filename mx-1">
              <div className="attachment_text">
                <a href={attachmentUrl} download={fileName} target="_blank">
                  {fileName}
                </a>
              </div>
            </div>
          </div>
        </Grid>
      );
    } else {
      return null; // Return nothing if attachmentUrl is not available
    }
  }

  render() {
    const { attachmentUrl } = this.props;
    const { knowledgeCategory } = this.props;
    const anchorTag = "section1";
    // const url = "https://gway.release.inaipi.ae/kmportal/static/lucene/files/ForexExchangePolicy.pdf";
    const fileName = this.getFileNameFromURL(attachmentUrl);

    const urlWithAnchor = this.appendAnchorTagToURL(attachmentUrl, anchorTag);

    const {
      from,
      to,
      toInputValue,
      toError,
      cc,
      ccInputValue,
      ccError,
      bcc,
      bccInputValue,
      bccError,
      selectedFile,
      selectedtempFile,
      actionView,
      anchorEl,
      emailSubject,
      templateDropdownList,
    } = this.state;
    const mailAddress = [
      {
        title: "rangitha.s@cognicx.com",
        mailid: "rangitha.s@cognicx.com",
      },
      { title: "belvin.d@cognicx.com" },
      { title: "vinoth.s@cognicx.com" },
      { title: "test@gmail.com" },
      { title: "nandhini.e@cognicx.com" },
      { title: "indumathi.p@cognicx.com" },
      { title: "hinduja.d@cognicx.com" },
    ];

    console.log("This state", this.state);
    console.log(
      "This props<<<<<<<<>>>>>.After render",
      this.props.action.togglechat
    );

    console.log(this.props.action.knowledgeCategory, "knowledgeCategory");

    return (
      <>
        <ToastContainer />
        <div className="email-desc-wrapper ">
          <div className="mailcontent" style={{ width: "95%" }}>
            <>
              <InputGroup size="sm" className="mb-3">
                <InputGroup.Text id="inputGroup-sizing-sm">To</InputGroup.Text>

                <Stack spacing={{}} sx={{ width: 740 }}>
                  <Autocomplete
                    multiple
                    size="small"
                    onChange={this.onToChange}
                    value={to}
                    inputValue={toInputValue}
                    onInputChange={this.onToInputChange}
                    options={mailAddress.map((option) => option.title)}
                    freeSolo
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        name="toAddr"
                        variant="standard"
                        type="email"
                        error={toError}
                      />
                    )}
                  />
                </Stack>
              </InputGroup>
              <InputGroup size="sm" className="mb-3">
                <InputGroup.Text id="inputGroup-sizing-sm">Cc</InputGroup.Text>

                <Stack spacing={{}} sx={{ width: 740 }}>
                  <Autocomplete
                    multiple
                    size="small"
                    onChange={this.onCcChange}
                    value={cc}
                    inputValue={ccInputValue}
                    onInputChange={this.onCcInputChange}
                    options={mailAddress.map((option) => option.title)}
                    freeSolo
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        name="ccAddr"
                        variant="standard"
                        type="email"
                        // error={ccError}
                      />
                    )}
                  />
                </Stack>
              </InputGroup>
              <InputGroup size="sm" className="mb-3">
                <InputGroup.Text id="inputGroup-sizing-sm">Bcc</InputGroup.Text>

                <Stack spacing={{}} sx={{ width: 740 }}>
                  <Autocomplete
                    multiple
                    size="small"
                    onChange={this.onBccChange}
                    value={bcc}
                    inputValue={bccInputValue}
                    onInputChange={this.onBccInputChange}
                    options={mailAddress.map((option) => option.title)}
                    freeSolo
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        name="bccAddr"
                        variant="standard"
                        type="email"
                        error={bccError}
                      />
                    )}
                  />
                </Stack>
              </InputGroup>
              <InputGroup className="mb-3">
                <Form.Control
                  placeholder="Add a Subject"
                  aria-label="Add a Subject"
                  aria-describedby="basic-addon2"
                  value={
                    this.props.inboxContent
                      ? this.props.inboxContent.subject
                      : "subject"
                  }
                  onChange={this.handleEmailSubject}
                />
              </InputGroup>

              {this.props.action.togglechat ? (
                <select
                  className="form-select form-select-sm w-50"
                  aria-label="Default select example"
                  defaultValue={this.props.action.knowledgeCategory || ""}
                  onChange={this.handelChangeTemplate}
                >
                  <option value="">Select Template</option>
                  {this.state.templateDropdownList.map((item) => (
                    <option
                      key={item.id}
                      value={item.description}
                      selected={
                        this.props.action.knowledgeCategory ===
                        item.emailtemplate_name
                      }
                    >
                      {item.emailtemplate_name}
                    </option>
                  ))}
                </select>
              ) : (
                <select
                  className="form-select form-select-sm w-25"
                  aria-label="Default select example"
                  onChange={this.handelChangeTemplate}
                >
                  <option value="">Select Template</option>
                  {this.state.templateDropdownList.map((item) => (
                    <option key={item.id} value={item.description}>
                      {item.emailtemplate_name}
                    </option>
                  ))}
                </select>
              )}
              <br />
            </>

            <div
              className="email-body-content-write"
              style={{ height: this.props.action.togglechat ? "15vh" : "25vh" }}
            >
              <div className="email-body-content-write-inside">
                {/* <div dangerouslySetInnerHTML={{ __html: this.state.inputText }} /> */}
                {/* <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1"> */}
                {/* <Form.Control as="textarea" rows={10}

                                    type="text"
                                    value={this.state.inputText}
                                    onChange={this.handleInputChange}
                                    ref={this.inputRef}
                                    onMouseUp={this.handleMouseUp}

                                /> */}

                {!_.isEmpty(selectedtempFile)
                  ? this.EmailAttachmentContent(selectedtempFile)
                  : null}

                {/* </Form.Group> */}

                <div>
                  {this.renderCard()}

                  <Editor
                    editorState={this.state.editorState}
                    onChange={this.handleEditorChange}
                    customStyleMap={customStyleMap}
                    handlePastedFiles={this.handlePasetImage}
                  />
                </div>
              </div>
            </div>

            <div className="email-action-newEmail">
              {this.props.action.togglechat ? (
                <>
                  <div className="d-flex">
                    <SplitButton
                      onClick={this.handleSendEmail}
                      variant="primary"
                      title="Send"
                      id="segmented-button-dropdown-1"
                    >
                      <Dropdown.Item href="#">Send</Dropdown.Item>
                      <Dropdown.Item href="#">Schedule Send</Dropdown.Item>
                    </SplitButton>
                    &nbsp;
                    <button variant="light" className="btn btn-warning btn-sm">
                      Discard
                    </button>
                    &nbsp;
                  </div>

                  <div className="d-flex">
                    <div className="palette-option">
                      <label class="attachment">
                        <input
                          type="file"
                          id=" "
                          className="file-upload-input"
                          onChange={this.FileInputChange}
                          multiple
                        />
                        <AttachFileIcon />
                      </label>
                    </div>
                    &nbsp;
                    <div className="palette-option">
                      <label class="attachment">
                        <input type="file" />
                        <InsertPhotoIcon />
                      </label>
                    </div>
                    <div className="palette-option1">
                      <select className="font-list">
                        <option>Arial</option>
                        <option>Calibri</option>
                        <option>Times New Roman</option>
                      </select>
                    </div>
                    <div className="palette-option1">
                      <select className="font-list">
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                      </select>
                    </div>
                    <button onClick={() => this.handleFontSizeChange("30")}>
                      Small
                    </button>
                    <div
                      className="d-flex"
                      style={{
                        position: "relative",
                        top: "43px",
                        right: "32.8rem",
                      }}
                    >
                      <div className="palette-option1">
                        <ToggleButton
                          value="bold"
                          aria-label="bold"
                          size="small"
                          id="fnt-style"
                        >
                          <FormatBoldIcon onClick={this.handleBoldClick} />
                        </ToggleButton>
                      </div>
                      <div className="palette-option1">
                        <ToggleButton
                          value="italic"
                          aria-label="italic"
                          size="small"
                          id="fnt-style"
                        >
                          <FormatItalicIcon onClick={this.handleItalicClick} />
                        </ToggleButton>
                      </div>
                      <div className="palette-option1">
                        <ToggleButton
                          value="underlined"
                          aria-label="underlined"
                          size="small"
                          id="fnt-style"
                        >
                          <FormatUnderlinedIcon
                            onClick={this.handleButtonClickUnderline}
                          />
                        </ToggleButton>
                      </div>
                      {this.state.iscolor ? (
                        <ChromePicker
                          color={this.state.color}
                          onChange={this.handleTextColorChange}
                        />
                      ) : (
                        ""
                      )}

                      <div className="palette-option1">
                        <ToggleButton
                          value="highlight"
                          aria-label="highlight"
                          size="small"
                          id="fnt-style"
                        >
                          <BorderColorIcon
                            onClick={() => {
                              this.setState({ iscolor: !this.state.iscolor });
                            }}
                          />
                        </ToggleButton>
                      </div>

                      <div className="palette-option1">
                        <ToggleButton
                          value="colortext"
                          aria-label="colortext"
                          size="small"
                          id="fnt-style"
                        >
                          <FormatColorTextIcon
                            onClick={() => {
                              this.setState({ iscolor: !this.state.iscolor });
                            }}
                          />
                        </ToggleButton>
                      </div>
                      <div className="palette-option1">
                        <ToggleButton
                          value="listbulleted"
                          aria-label="listbulleted"
                          size="small"
                          id="fnt-style"
                        >
                          <FormatListBulletedIcon />
                        </ToggleButton>
                      </div>
                      <div className="palette-option1">
                        <ToggleButton
                          value="listnumbered"
                          aria-label="listnumbered"
                          size="small"
                          id="fnt-style"
                        >
                          <FormatListNumberedIcon />
                        </ToggleButton>
                      </div>
                      <div className="palette-option1">
                        <ToggleButton
                          value="indentincrease"
                          aria-label="indentincrease"
                          size="small"
                          id="fnt-style"
                        >
                          <FormatIndentIncreaseIcon />
                        </ToggleButton>
                      </div>
                      <div className="palette-option1">
                        <ToggleButton
                          value="indentdecrease"
                          aria-label="indentdecrease"
                          size="small"
                          id="fnt-style"
                        >
                          <FormatIndentDecreaseIcon />
                        </ToggleButton>
                      </div>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <SplitButton
                    onClick={this.handleSendEmail}
                    variant="primary"
                    title="Send"
                    id="segmented-button-dropdown-1"
                  >
                    <Dropdown.Item href="#">Send</Dropdown.Item>
                    <Dropdown.Item href="#">Schedule Send</Dropdown.Item>
                  </SplitButton>
                  &nbsp;
                  <button variant="light" className="btn btn-warning">
                    Discard
                  </button>
                  &nbsp;
                  <div className="palette-option">
                    <label class="attachment">
                      <input
                        type="file"
                        id="fileupload"
                        className="file-upload-input"
                        onChange={this.FileInputChange}
                        multiple
                      />
                      <AttachFileIcon />
                    </label>
                  </div>
                  &nbsp;
                  <div className="palette-option">
                    <label class="attachment">
                      <input type="file" />
                      <InsertPhotoIcon />
                    </label>
                  </div>
                  <div className="palette-option1">
                    <select className="font-list">
                      <option>Arial</option>
                      <option>Calibri</option>
                      <option>Times New Roman</option>
                    </select>
                  </div>
                  <div className="palette-option1">
                    <select className="font-list">
                      <option>8</option>
                      <option>9</option>
                      <option>10</option>
                    </select>
                  </div>
                  <button onClick={() => this.handleFontSizeChange("30")}>
                    Small
                  </button>
                  <div className="palette-option1">
                    <ToggleButton
                      value="bold"
                      aria-label="bold"
                      size="small"
                      id="fnt-style"
                    >
                      <FormatBoldIcon onClick={this.handleBoldClick} />
                    </ToggleButton>
                  </div>
                  <div className="palette-option1">
                    <ToggleButton
                      value="italic"
                      aria-label="italic"
                      size="small"
                      id="fnt-style"
                    >
                      <FormatItalicIcon onClick={this.handleItalicClick} />
                    </ToggleButton>
                  </div>
                  <div className="palette-option1">
                    <ToggleButton
                      value="underlined"
                      aria-label="underlined"
                      size="small"
                      id="fnt-style"
                    >
                      <FormatUnderlinedIcon
                        onClick={this.handleButtonClickUnderline}
                      />
                    </ToggleButton>
                  </div>
                  {this.state.iscolor ? (
                    <ChromePicker
                      color={this.state.color}
                      onChange={this.handleTextColorChange}
                    />
                  ) : (
                    ""
                  )}
                  <div className="palette-option1">
                    <ToggleButton
                      value="highlight"
                      aria-label="highlight"
                      size="small"
                      id="fnt-style"
                    >
                      <BorderColorIcon
                        onClick={() => {
                          this.setState({ iscolor: !this.state.iscolor });
                        }}
                      />
                    </ToggleButton>
                  </div>
                  <div className="palette-option1">
                    <ToggleButton
                      value="colortext"
                      aria-label="colortext"
                      size="small"
                      id="fnt-style"
                    >
                      <FormatColorTextIcon
                        onClick={() => {
                          this.setState({ iscolor: !this.state.iscolor });
                        }}
                      />
                    </ToggleButton>
                  </div>
                  <div className="palette-option1">
                    <ToggleButton
                      value="listbulleted"
                      aria-label="listbulleted"
                      size="small"
                      id="fnt-style"
                    >
                      <FormatListBulletedIcon />
                    </ToggleButton>
                  </div>
                  <div className="palette-option1">
                    <ToggleButton
                      value="listnumbered"
                      aria-label="listnumbered"
                      size="small"
                      id="fnt-style"
                    >
                      <FormatListNumberedIcon />
                    </ToggleButton>
                  </div>
                  <div className="palette-option1">
                    <ToggleButton
                      value="indentincrease"
                      aria-label="indentincrease"
                      size="small"
                      id="fnt-style"
                    >
                      <FormatIndentIncreaseIcon />
                    </ToggleButton>
                  </div>
                  <div className="palette-option1">
                    <ToggleButton
                      value="indentdecrease"
                      aria-label="indentdecrease"
                      size="small"
                      id="fnt-style"
                    >
                      <FormatIndentDecreaseIcon />
                    </ToggleButton>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>

        <Popover
          id="simple-popover"
          open={actionView}
          anchorEl={anchorEl}
          onClose={this.closeActionView}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
        >
          <Paper sx={{ width: 200, maxWidth: "100%" }}>
            <MenuList>
              <MenuItem onClick={this.openDocument}>
                <ListItemIcon>
                  <VisibilityIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText>Preview</ListItemText>
              </MenuItem>
              <Divider />
              <MenuItem onClick={this.downloadDoc}>
                <ListItemIcon>
                  <DownloadIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText>Download</ListItemText>
              </MenuItem>
              <Divider />
              {/* <MenuItem>
                    <ListItemIcon>
                        <ClearIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText>Remove Attachment</ListItemText>
                    </MenuItem> */}
            </MenuList>
          </Paper>
        </Popover>
      </>
    );
  }
}
const customStyleMap = {
  FONT_SIZE_40: {
    fontSize: "40px",
  },
};
