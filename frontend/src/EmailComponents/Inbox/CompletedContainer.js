import { connect } from "react-redux";
import Completed from "./Completed";
import { InboxList, CompletedMailList, InboxMailShow, DownloadAttachment, SendEmail, openWriteEmail, closeWriteEmail, InboxErrorMessageClose } from "../../redux/actions/spaceActions";

const mapStateToProps = (state) => {
    return {

        emailShowMessage: state.data.emailShowMessage,
        emaiIsPending: state.data.emaiIsPending,
        emailMessage: state.data.emailMessage,
        emailShowColor: state.data.emailShowColor,
        InboxDataList: state.data.InboxDataList,
        completedDataList: state.data.completedDataList,
        InboxEmailContent: state.data.InboxEmailContent,
        InboxEmailBody: state.data.InboxEmailBody,
        writeEmail: state.data.writeEmail,
        isReplyEmail: state.data.isReplyEmail,
    }
}

const mapDispatchToProps = (dispatch) => ({
    InboxList: () => dispatch(InboxList()),
    CompletedMailList: (a) => dispatch(CompletedMailList(a)),
    InboxMailShow: (a) => dispatch(InboxMailShow(a)),
    DownloadAttachment: (a, b) => dispatch(DownloadAttachment(a, b)),
    SendEmail:(a) => dispatch(SendEmail(a)),
    openWriteEmail:() => dispatch(openWriteEmail()),
    closeWriteEmail:() => dispatch(closeWriteEmail()),
    InboxErrorMessageClose: () => dispatch(InboxErrorMessageClose())
})

const CompletedContainer = connect(mapStateToProps, mapDispatchToProps)(Completed)

export default CompletedContainer