import React from "react";
import ReactDOM from "react-dom";
import Sdk from '../src/DialerComponent/Sdk';
import "./styles/index.scss";
import "./styles/neo-icons.css";
import "./styles/neo.css";
import "react-quill/dist/quill.snow.css";
import "./styles/quill.mention.css";
import App from "./containers/App";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-day-picker/lib/style.css";
import { Provider } from "react-redux";
import store from "./redux/store";
import jQuery from "jquery";
import Konva from "konva";
import xmlToJSON from "xmltojson";
import Login from "../src/components/login/Login";
import { BrowserRouter, Routes, Route } from "react-router-dom";
// import SubmitInterest from "./containers/Page/submitInterest";
import Dashboard from "./containers/Page/Dashboard";
// import Contacts from "./containers/Page/Contacts";
// import ShowDetails from "./containers/Page/showDetails";
import SidebarHeader from "./containers/Page/SidebarHeader";
// import ClientLogin from "./containers/ClientLogin";
// import ChatClient from "./containers/ChatClient";
import Calendars from './containers/Page/Calender'

import ChatForClient from "./components/clientChat";
import Bank from "./containers/Page/bankPage";
import Sessionreport from "./containers/Page/reports/sessionreports/Sessionreport";
import AgentReport from './containers/Page/reports/agentreports/Agentreport';

/**Email Components */
import DashboardContainer from "./EmailComponents/Dashboard/DashboardContainer";
import InboxContainer from "./EmailComponents/Inbox/InboxContainer";
import NewEmailContainer from "./EmailComponents/Inbox/NewEmailContainer";
import SentItemsContainer from "./EmailComponents/Inbox/SentItemsContainer";
import CompletedContainer from "./EmailComponents/Inbox/CompletedContainer";
import Powerbi from "./containers/Page/Powerbi";
import Powerbidashboard from "./containers/Page/powerbidashboard";


/******************** */




// import './Resources/css/email.scss';
// import './Resources/css/images.scss';
// import Loginwithdomain from "./components/loginwithdomain/Loginwithdomain";



window.xmlToJSON = xmlToJSON;
window.Konva = Konva;
window.$ = jQuery;
window.jQuery = jQuery;



ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path="chat" element={<App />} />
          <Route path="" element={<Login />} />
          <Route path="/cli" element={<Login />} />


          <Route path="dashboard" element={<Dashboard />} />
          <Route path="Sdk" element={<Sdk />} />
          <Route path="sessionreports" element={<Sessionreport />} />
          <Route path="agentreports" element={<AgentReport />} />
          <Route path="/powerbi" element={<Powerbi/>} />
          <Route path="/PowerbiDashboard" element={<Powerbidashboard/>} />
          

          {/* <Route path="submitInterest" element={<SubmitInterest />} /> */}
          <Route path="sidebarHeader" element={<SidebarHeader />} />
          <Route path="/clientLoginNew" element={<ChatForClient />} />
          <Route path="/customerPortal" element={<Bank />} />
          <Route path="/Calendars" element={<Calendars />} />
          <Route path="/mail" element={<DashboardContainer/>} />
          <Route path="/inbox" element={<InboxContainer/>} />
          <Route path="/newemail" element={<NewEmailContainer/>} />
          <Route path="/sentitems" element={<SentItemsContainer/>} />
          <Route path="/completeemail" element={<CompletedContainer/>} />


        </Routes>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
// });
