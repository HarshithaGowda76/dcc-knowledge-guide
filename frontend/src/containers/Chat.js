import React, { useEffect, useRef, useState } from "react";
import Message from "../components/Message";
import { Button, Modal } from "react-bootstrap";
import { GoRequestChanges, GoPlus, GoBookmark } from "react-icons/go";
import { Tooltip } from "bootstrap";
import Tabs from "react-bootstrap/Tabs";
import { Tab } from "react-bootstrap";
import { toUTCDate } from "../utils";
import { connect } from "react-redux";
import moment from "moment";
import axios from "axios";
import {
  setAddTopicToggle,
  setAddTopicInput,
  setchatmessage,
  setcontactlist,
  setcontactlist1,
  setcontactlist2,
  setrefreshtogglechat,
  setconfchatrefresh,
  setAgentList,
  setinternalchatrefresh,
  setchatdata,
  setchatid,
  setselectedmobile,
  setselectedemail,
  setselectedusername,
  setchattype,
  setinternalchatnotify,
  setchatdataforinternal,
  setConferenceNotification,
  setavailagent,
  setloadmore,
  setSelectedchannel
} from "../redux/actions/spaceActions";
import io from "socket.io-client";
import ChatInput from "../components/chatInput";
import { BaseUrl, SocketUrl, errorApi } from "./Page/Constants/BaseUrl";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { coBrowseApi } from "./Page/apis/CobrowserApi";
const mapStateToProps = (state) => {
  return {
    spaceContent: state.data.spaceContent,
    togglechat: state.data.togglechat,
    refreshtogglechat: state.data.refreshtogglechat,
    chat: state.data.chat,
    chatType: state.data.chatType,
    chatMessage: state.data.chatMessage,
    contactList: state.data.contactList,
    contactList1: state.data.contactList1,
    contactList2: state.data.contactList2,
    chatid: state.data.chatid,
    confchatrefresh: state.data.confchatrefresh,
    agentList: state.data.agentList,
    internalchatrefresh: state.data.internalchatrefresh,
    internalchatnotify: state.data.internalchatnotify,
    chatdataforinternal: state.data.chatdataforinternal,
    conferenceNotification: state.data.conferenceNotification,
    conferenceNotificationVal: state.data.conferenceNotificationVal,
    availAgent: state.data.availAgent,
    loadmoretoggle: state.data.loadmoretoggle,
    addTopicInput: state.data.addTopicInput,
    addTopicToggle: state.data.addTopicToggle,
    selectedchannel: state.data.selectedchannel,

  };
};

function Chat(props) {
  const { chat, chatid, agentList } = props;
  const tenantId = localStorage.getItem("TenantId");
  const socket = useRef();
  const contentRef = useRef();
  const { spaceContent } = props;
  const [currentUserName, setCurrentUserName] = useState(undefined);
  const [currentUserid, setCurrentUserid] = useState("");
  const [message, setMessage] = useState([]);
  const [lastmessage, setLastmessage] = useState([]);
  const [arrivalMessage, setArrivalMessage] = useState(null);
  const [CustomerTyping, setCustomerTyping] = useState(false);
  const [eventKey, setEventKey] = useState("chatContainer");
  const [initiate, setInitiate] = useState("true");
  const [showcobrowse, setShowcobrowse] = useState(false);
  const [iframeopen, setIframeopen] = useState(false);

  const [showAddTopic, setShowAddTopic] = useState(false);
  const handleCobrowseShow = () => setShowcobrowse(true);
  const handleCobrowseClose = () => setShowcobrowse(false);

  const handleAddTopicOpen = () => setShowAddTopic(true);
  const handleAddTopicClose = () => setShowAddTopic(false);
  const hideInitiate = () => {
    setInitiate(!initiate);
  };
  const [chatEmail, setChatEmail] = useState("");
  const [cobrowswerinput, setCobrowserInput] = useState([]);
  const [cobrowse, setCobrowse] = useState([]);
  const [session_id, setsession_id] = useState("");
  const [sender_id, setsender_id] = useState("");
  const [reciver_id, setreciver_id] = useState("");
  const [loadmoredata, setLoadmoredata] = useState([]);
  const [openiframemodal, setOpeniframemodal] = useState("");
  const [addTopicToggle , setAddTopicToggle] = useState(false); 



  const errorHandel = async (error,endpoint) => {
    const tenantID = localStorage.getItem("TenantId");
    try {
      const payload = {
        servicename: 'DCCCHAT',
        logs:error,
        description:endpoint

      };

      const header = {
        headers: {
          // Authorization: "Bearer " + token,
          // "Content-Type": "application/json",
          tenantid: tenantID,
        },
      };
      const { data } = await axios.post(
        errorApi + "/UIlogs/createLogs",
        payload,
        header
      );
      if (data.status) {
        console.log("from error api", data);
      }
    } catch (error) {
      console.log('error',error)
    }
  };



  useEffect(() => {
    async function getData() {
      const datas = await JSON.parse(localStorage.getItem("tokenAgent"));
      setCurrentUserid(datas.id);
      setCurrentUserName(datas.username);
      try {
        const response = await axios.post(
          BaseUrl + "/message/getCurrentChatMessage",
          {
            sessionId: chatid,
          },{
            headers:{
              tenantId: tenantId,

            }
          }
        );

        if (chat.chat_type == "internal") {
          let sender_id = chat.senderDetails[0]._id;
          let current_agent_id = datas.id;
          if (sender_id != current_agent_id) {
            if (response.data.data) {
              response.data.data.forEach((element) => {
                element["fromSelf"] = element["fromSelf"] ? false : true;
              });
              setMessage(response.data.data);
            } else {
              setMessage([]);
            }
          } else {
            if (response.data.data) {
              setMessage(response.data.data);
            } else {
              setMessage([]);
            }
          }
        } else {
          // console.log(response);
          if (response.data.data) {
            setMessage(response.data.data);
            console.log("chat api response", response.data.data);
          } else {
            setMessage([]);
          }
        }
      } catch (error) {
        setMessage([]);
        errorHandel(error,'/message/getCurrentChatMessage')
      }
    }
    getData();
    setCustomerTyping(false);
  }, [chat, chatid]);
  // console.log(message);
  const loadmore = async () => {
    
    if (chat.chat_type == "external") {
      var payload = {
        messager_id: chat.unique_id.id,
        session_id: chat.chat_session_id,
        agent:chat.agent,
        offset: 0,
        limit: 1000,
      };
      var url = "/message/listmessagepagination";
    } else if (chat.chat_type == "internal") {
      var payload = {
        sender_id: chat.senderDetails[0]._id,
        receiver_id: chat.reciverDetails[0]._id,
        session_id: chat.chat_session_id,
        offset: 0,
        limit: 1000,
      };
      var url = "/message/listInternalMessage";
    }
    console.log("chat", chat);

    const token = localStorage.getItem("access_token");

    const header = {
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
        tenantId: tenantId,

      },
    };

    const { data } = await axios.post(BaseUrl + url, payload, header);

    console.log("from loadmore api", data.data);

    if (data.status == true) {
      setLoadmoredata(data.data);
      props.setloadmore(true);
      props.setrefreshtogglechat(false);
    } else {
      setLoadmoredata([]);
      props.setloadmore(true);
      props.setrefreshtogglechat(false);
    }
  };

  useEffect(async () => {
    if (props.conferenceNotification) {
      const data = await JSON.parse(localStorage.getItem("tokenAgent"));
      const msgs = [...message];
      msgs.push({
        fromSelf: true,
        message: props.conferenceNotificationVal,
        senderName: data.username,
        receiverName: chat.username,
        to: chat.id,
        sender: data.id,
        time: moment().format(),
        msg_sent_type: "NOTIFICATIONS",
      });
      setMessage(msgs);
      setConferenceNotification(false);
    }
  }, [props.conferenceNotification, props.conferenceNotificationVal]);

  useEffect(async () => {
    setCustomerTyping(false);
    const datas = await JSON.parse(localStorage.getItem("tokenAgent"));
    // if (currentUserName) {
    socket.current = io(SocketUrl+`/${localStorage.getItem('TenantId')}`);
    socket.current.on("disconnect", (msg) => {
      console.log("Disconnected");
    });
    socket.current.on("reconnect", function () {
      console.log("Reconnecting");
    });
    socket.current.on("connect", function () {
      console.log("Connected");
      if (chat.chat_type != undefined)
        if (chat.chat_type == "internal") {
          console.log("its internal chat");
          let sender_id = chat.senderDetails[0]._id;
          let current_agent_id = datas.id;
          console.log(sender_id, current_agent_id);
          if (sender_id == current_agent_id) {
            console.log("its a sender data",datas.id);
            socket.current.emit("add-user", datas.id);
          } else {
            console.log("its a receiver data",chat.id);
            socket.current.emit("add-user", chat.id);
          }
        } else {
          console.log("its a external chat",datas.id);
          socket.current.emit("add-user", datas.id);
        }
    });

    socket.current.on("msg-receive", (msg) => {
      let updateChat;
      if (chat.chat_type == "internal") {
        setsession_id("");
        setsender_id("");
        setreciver_id("");

        // socket.current.emit("add-user", msg.from);
        if (msg.chatType == "inbound") {
          var i = 0;
          let setActive = props.agentList.filter((lis) => {
            i = i + 1;
            return lis._id == msg.from;
          });

          // updateChat = setActive[0];

          if (chat.id == msg.from) {
            props.setinternalchatnotify(true);
            setCustomerTyping(false);
            setArrivalMessage({
              fromSelf: false,
              session_id: msg.session_id,
              message: msg.msg,
              senderName: msg.senderName,
              sender: msg.to,
              receiver: msg.from,
              time: moment().format(),
              msg_sent_type: msg.msg_sent_type,
              file_name: msg.file_name,
              captions:msg.captions
            });
          }
          console.log("this is inbound");

          console.log("chat session id", setActive[0].chat_session_id);
        } else {
          let setActive = props.agentList.filter((lis) => {
            return lis.agent == msg.from;
          });
          console.log("New message from" + JSON.stringify(setActive));
          // updateChat = setActive[0];

          if (msg.from == chat.agent) {
            props.setinternalchatnotify(true);
            setCustomerTyping(false);
            setArrivalMessage({
              fromSelf: false,
              session_id: msg.session_id,
              message: msg.msg,
              senderName: msg.senderName,
              sender: msg.to,
              receiver: msg.from,
              time: moment().format(),
              msg_sent_type: msg.msg_sent_type,
              file_name: msg.file_name,
              captions:msg.captions
            });
          }
        }
      } else {
        setLastmessage(msg);
        // console.log("last-msg testing",chat.id);
        // console.log("last-msg testing",msg.from);
        if (chat.id == msg.from) {
          console.log("last-msg testing", {
            fromSelf: false,
            message: msg.msg,
            session_id: msg.session_id,
            senderName: msg.senderName,
            sender: msg.from,
            receiver: msg.to,
            time: moment().format(),
            msg_sent_type: msg.msg_sent_type,
            file_name: msg.file_name,
            captions:msg.captions
          });
          setArrivalMessage({
            fromSelf: false,
            message: msg.msg,
            session_id: msg.session_id,
            senderName: msg.senderName,
            sender: msg.from,
            receiver: msg.to,
            time: moment().format(),
            msg_sent_type: msg.msg_sent_type,
            file_name: msg.file_name,
            captions:msg.captions
          });
          setCustomerTyping(false);
        }
        if (
          (msg.conference && chat.id == msg.conference_id) ||
          (msg.conference && chat.id == msg.to)
        ) {
          setCustomerTyping(false);

          setArrivalMessage({
            fromSelf: true,
            message: msg.msg,
            session_id: msg.session_id,
            senderName: msg.senderName,
            sender: msg.from,
            receiver: msg.to,
            msg_sent_type: msg.msg_sent_type,
            file_name: msg.file_name,
            captions:msg.captions
          });
        }
      }
      // console.log(setActive, "setActive");
    });

    socket.current.emit("typing-msg-receive", async (msg) => {
      console.log('typing-msg-receive<>><<>><<>',msg)
      if (msg.chatdetails.chat_type == "internal") {
         

        setsession_id(msg.chatdetails.chat_session_id);
        setsender_id(msg.chatdetails.senderDetails[0]._id);
        setreciver_id(msg.chatdetails.senderDetails[0]._id);
        setCustomerTyping(true);
      } else {
        if (chat.id == msg.from) {
          setCustomerTyping(true);
        }
      }
    });

    socket.current.on("update-agent-status-recv", (data) => {
      props.setinternalchatrefresh(true);
      getAvailableAgents();
    });

    socket.current.on("customer-end-chat", (data) => {
      var contactlist = props.contactList;
      var newcontactlist = [];

      contactlist.map((client, index) => {
        if (client.chat_session_id == data.chat_session_id) {
          client.is_customer_disconnected = true;
        }
        newcontactlist.push(client);
      });
      props.setcontactlist(newcontactlist);
    });
    socket.current.on("get-new-callreq", (data) => {
      if (datas.id == data.agent) {
      var contactlist = props.contactList;
     // var newcontactlist = [];
     var newcontactlist=contactlist.filter((item) => {
      return item.id != data.id;
    });

      newcontactlist.push(data);
      props.setcontactlist(newcontactlist);
     }
    });


    socket.current.on("get-new-req", (newUser) => { 

      var newReqAgent = [];
      var listrequest = [];

      if (datas.id == newUser.agent) {
        window.location.reload(true)
        listrequest = newUser;
        console.log("New request", listrequest);

        let filteredArray = props.contactList.filter((item) => {
          return item.id != listrequest.id;
        });

        let filteredArraynew = props.contactList2.filter((item) => {
          return item.id != listrequest.id;
        });
        console.log("find accepted", filteredArray);
        console.log("find new request", filteredArraynew);
        if (filteredArray.length < 1 && filteredArraynew.length<1) {
          newReqAgent.push(listrequest);
          props.setcontactlist2(newReqAgent);
        }
      }
    });

    socket.current.on("recv-internal", async (newUser) => {
      let lastElement = newUser.pop();
      var oldReqAgent = props.agentList;
      props.setinternalchatrefresh(true);

      oldReqAgent = oldReqAgent.filter((item) => {
        item.user_id != lastElement.user_id;
      });
      var listrequest = [];
      props.setAgentList(oldReqAgent);
      if (lastElement) {
        if (datas.id == lastElement.reciverDetails[0]._id) {
          listrequest = lastElement;
          oldReqAgent.push(listrequest);
          props.setchatdata(listrequest);
        }
      }
    });

    socket.current.on("get-new-req-conf", (newUser) => {
      console.log("conf list came", newUser);
      props.setconfchatrefresh(true);
    });
    showLastMsg();
    showLastMsgInternal();
    setChatEmail(chat.email);
  }, [currentUserName, chat, chatid]);

  const getAvailableAgents = async () => {

     try {
       const access_token = localStorage.getItem("access_token");
       let userId = JSON.parse(localStorage.getItem("tokenAgent"));
       let passUserId = userId.user_id;
       const data = await axios.post(
         BaseUrl + "/users/getAgents",
         { userId: passUserId },
         {
           headers: {
             Authorization: "Bearer " + access_token,
             "Content-Type": "application/json",
             tenantId: tenantId,
   
           },
         }
       );
       if (data.data.success) {
         props.setavailagent(data.data.data);
       }
      
     } catch (error) {
      errorHandel(error,'/users/getAgents')
     }
  };



  const showLastMsg = () => {
    socket.current.on("last-msg-receive", (msg) => {
      var contactlist = props.contactList;
      var newcontactlist = [];
      var unread_count = 0;
      var contactList1 = props.contactList1;
      var newcontactlist1 = [];
      var i=0;
      var find=0;
      contactlist.map((client, index) => {
        if (client.id == msg.from) {
            find=i;
          if (client.unreadcount) {
            unread_count = client.unreadcount + 1;
          } else {
            unread_count = 1;
          }

          client.lastmessage = msg.msg;
          client.lastmessagetime = moment().format("h:mm A");
          client.lastmessageUpdatedat=moment();
          client.unreadcount = unread_count;
          client.currentChat = client.id == msg.from ? true : false;

          updateLastMes(
            client.id,
            msg,
            moment().format("h:mm A"),
            unread_count
          );

          newcontactlist1 = {
            username: msg.senderName,
            client_msg: msg.msg,
            msg_time: moment().format("lll"),
            from_id: msg.from,
          };
          contactList1.push(newcontactlist1);
        }

        if (
          (msg.conference && client.id == msg.conference_id) ||
          (msg.conference && client.id == msg.to)
        ) {
          if (client.unreadcount) {
            unread_count = client.unreadcount + 1;
          } else {
            unread_count = 1;
          }

          client.lastmessage = msg.msg;
          client.lastmessagetime = moment().format("h:mm A");
          client.unreadcount = unread_count;
          client.currentChat = client.id == msg.from ? true : false;

          updateLastMes(
            client.id,
            msg,
            moment().format("h:mm A"),
            unread_count
          );

          newcontactlist1 = {
            username: msg.senderName,
            client_msg: msg.msg,
            msg_time: moment().format("lll"),
            from_id: msg.from,
          };
          contactList1.push(newcontactlist1);
        }

        newcontactlist.push(client);
        i++;
      });
      let newupdatedlist = insertAndShift(newcontactlist, find, 0);
 
      props.setcontactlist(newupdatedlist);
      props.setcontactlist1(contactList1);
    });
  };

  const showLastMsgInternal = () => {
    let count = 1;
    socket.current.on("last-msg-receive", (msg) => {
      agentList.forEach(function (item, i) {
        if (msg.chatType == "inbound") {
          if (item._id === msg.from) {
            props.agentList.splice(i, 1);
            props.agentList.unshift(item);
          }
        } else {
          if (item.agent === msg.from) {
            props.agentList.splice(i, 1);
            props.agentList.unshift(item);
          }
        }
      });

      props.setrefreshtogglechat(true);
      console.log("props.agentList", props.agentList);

      count++;
      var contactlist = props.agentList;
      var newcontactlist = [];
      var unread_count = 0;
      var contactList1 = props.contactList1;
      var newcontactlist1 = [];
      agentList.map((client, index) => {
        if (msg.chatType == "inbound") {
          if (client.id == msg.from) {
            if (client.unreadcount) {
              unread_count = client.unreadcount + 1;
            } else {
              unread_count = 1;
            }

            client.lastmessage = msg.msg;
            client.lastmessagetime = moment().format("h:mm A");
            client.unreadcount = unread_count;
            client.currentChat = client.id == msg.from ? true : false;

            updateLastMes(
              client.id,
              msg,
              moment().format("h:mm A"),
              unread_count
            );
          }

          newcontactlist.push(client);
        } else {
          if (client.agent == msg.from) {
            if (client.unreadcount) {
              unread_count = client.unreadcount + 1;
            } else {
              unread_count = 1;
            }

            client.lastmessage = msg.msg;
            client.lastmessagetime = moment().format("h:mm A");
            client.unreadcount = unread_count;
            client.currentChat = client.id == msg.from ? true : false;

            updateLastMes(
              client.id,
              msg,
              moment().format("h:mm A"),
              unread_count
            );
          }

          newcontactlist.push(client);
        }
      });
      props.setAgentList(newcontactlist);
    });
  };

  const updateLastMes = async (id, msg, date, count) => {
    const access_token = localStorage.getItem("access_token");
    try {
      let data_to_pass = {
        id: id,
        lastMsg: msg.msg,
        time: date,
        count: count,
      };
  
      await axios.post(BaseUrl + "/message/updateLastMessage", data_to_pass,{
        headers:{
          tenantId: tenantId,
          'Authorization': 'Bearer ' + access_token,
          'Content-Type': 'application/json',
  
        }
      });
      
    } catch (error) {
      errorHandel(error,'/message/updateLastMessage')
    }
  };

  const handleTyping = async () => {
    const data = await JSON.parse(localStorage.getItem("tokenAgent"));
    if (chat.chat_type == "internal") {
      console.log("typing event Internal<<<<>>>>",chat);
      let sender_id = chat.senderDetails[0]._id;
      let current_agent_id = data.id;
      if (sender_id == current_agent_id) {
        console.log("outbound");
        console.log('typing-send-msg-To',chat.id)
        console.log('typing-send-msg-from',data.id)
        console.log('typing-send-msg-senderName', data.username)
        console.log('typing-send-msg-chatdetails',chat)

        socket.current.emit("typing-send-msg", {
          to: chat.id,
          from: data.id,
          senderName: data.username,
          chatType: "outbound",
          chatdetails: chat,
        });
      } else {
        console.log("inbound");
        socket.current.emit("typing-send-msg", {
          to: chat.agent,
          from: data.id,
          senderName: data.username,
          chatType: "inbound",
          chatdetails: chat,
        });
      }
    } else {
      console.log('to_chat.idExternal<<<<<<>>>>',chat.id)
      console.log('from_chat.idExternal<<<<<<>>>>',data.id)
      console.log('senderName.idExternal<<<<<<>>>>',data.username)
      console.log('chatdetailsExternal<<<<<<>>>>',chat)

      chatType: "outbound",

      socket.current.emit("typing-send-msg", {
        to: chat.id,
        from: data.id,
        senderName: data.username,
        chatType: "outbound",
        chatdetails: chat,
      });
    }
  };


  function insertAndShift(arr, from, to) {
    let cutOut = arr.splice(from, 1)[0]; // cut the element at index 'from'
    arr.splice(to, 0, cutOut);
    return arr;
    // insert it at index 'to'
  }
  const handleSendMsg = async (msg, msg_type, fileNameForDoc) => {
   
   
    if (chat.chat_type == "internal") {
    var contactlist1 = props.agentList;
    var newcontactlist1 = [];
    var i = 0;
    var find = 0;
    contactlist1.map((client1, index) => {
      if (client1.id == chat.id) {
        find = i;
        client1.lastmessage = msg;
        client1.lastmessagetime = moment().format("h:mm A");
        client1.currentChat = client1.id == msg.from ? true : false;
        client1.unreadcount = 0;
      }
      newcontactlist1.push(client1);
      i++;
    });

    let newagent = insertAndShift(newcontactlist1, find, 0);
 
    props.setAgentList(newagent);
  }else{
    // alert("work");
    var contactlist = props.contactList;
    var newcontactlist = [];
    var i = 0;
    var find = 0;
    contactlist.map((client, index) => {
      if (client.id == chat.id) {
        find = i;
     
        client.lastmessage = msg;
        client.lastmessagetime = moment().format("h:mm A");
        client.lastmessageUpdatedat=moment();
        client.currentChat = client.id == msg.from ? true : false;
        client.unreadcount=0;
      }
      newcontactlist.push(client);
      i++;
    });

  
    let newexternalcontact = insertAndShift(newcontactlist, find, 0);

    console.log(newexternalcontact);
    props.setcontactlist(newexternalcontact);
  }
    const data = await JSON.parse(localStorage.getItem("tokenAgent"));

    if (chat.chat_type == "internal") {
      let sender_id = chat.senderDetails[0]._id;
      let current_agent_id = data.id;
      if (sender_id == current_agent_id) {
        await axios.post(BaseUrl + "/message/addMessage", {
          from: data.id,
          to: chat.id,
          message: msg,
          senderName: data.username,
          receiverName: chat.username,
          messageFrom: "fromAgent",
          userType: chat.chat_type,
          session: chat.chat_session_id,
          msg_sent_type: msg_type,
          chatdetails: chat,
          file_name: fileNameForDoc,
          captions:""
        },{
          headers:{
            tenantId: tenantId,

          }
        });

   
      
        socket.current.emit("send-msg", {
          to: chat.id,
          session_id: chat.chat_session_id,
          from: data.id,
          senderName: data.username,
          chatType: "outbound",
          msg,
          msgType: "web",
          userType: chat.chat_type,
          msg_sent_type: msg_type,
          chatdetails: chat,
          file_name: fileNameForDoc,
          captions:""
        }
        );


    console.log('First Send_Message',{
      to: chat.id,
      session_id: chat.chat_session_id,
      from: data.id,
      senderName: data.username,
      chatType: "outbound",
      msg,
      msgType: "web",
      userType: chat.chat_type,
      msg_sent_type: msg_type,
      chatdetails: chat,
      file_name: fileNameForDoc,
      captions:""
    }) 
   

        socket.current.emit("last-msg-send", {
          to: chat.id,
          from: data.id,
          chatType: "outbound",
          msg,
          senderName: data.username,
          userType: chat.chat_type,
          chatdetails: chat,
          file_name: fileNameForDoc,
          captions:""
        });
      } else {
        await axios.post(BaseUrl + "/message/addMessage", {
          from: chat.id,
          to: chat.agent,
          message: msg,
          senderName: data.username,
          receiverName: chat.username,
          messageFrom: "fromClient",
          userType: chat.chat_type,
          session: chat.chat_session_id,
          msg_sent_type: msg_type,
          chatdetails: chat,
          file_name: fileNameForDoc,
          captions:""
        },{
          headers:{
            tenantId: tenantId,

          }
        });


        console.log('ExternalChat_TO<><><>',chat.agent)
        console.log('ExternalChat_session_id<><><>',chat.chat_session_id)
        console.log('ExternalChat_FROM<><><>',chat.id)
        console.log('ExternalChat_Sendername<><><>',data.username)
        console.log('ExternalChat_<><><>',msg)
        console.log('ExternalChat_usertype<><><>',chat.chat_type)
        console.log('ExternalChat_messageType<><><>',msg_type)
        console.log('ExternalChat_chatDetails<><><>',chat)
        console.log('ExternalChat_fileName<><><>',fileNameForDoc)
     

        socket.current.emit("send-msg", {
          to: chat.agent,
          session_id: chat.chat_session_id,
          from: chat.id,
          senderName: data.username,
          chatType: "inbound",
          msg,
          msgType: "web",
          userType: chat.chat_type,
          msg_sent_type: msg_type,
          chatdetails: chat,
          file_name: fileNameForDoc,
          captions:""
        });


        console.log('Second send message',{
          to: chat.agent,
          session_id: chat.chat_session_id,
          from: chat.id,
          senderName: data.username,
          chatType: "inbound",
          msg,
          msgType: "web",
          userType: chat.chat_type,
          msg_sent_type: msg_type,
          chatdetails: chat,
          file_name: fileNameForDoc,
          captions:""
          
        })

        socket.current.emit("last-msg-send", {
          to: chat.agent,
          from: chat.id,
          chatType: "inbound",
          msg,
          senderName: data.username,
          userType: chat.chat_type,
          chatdetails: chat,
        });
      }
      setCustomerTyping(false);
    } else {
      await axios.post(BaseUrl + "/message/addMessage", {
        from: data.id,
        to: chat.id,
        message: msg,
        senderName: data.username,
        receiverName: chat.username,
        messageFrom: "fromAgent",
        userType: chat.chat_type,
        session: chat.chat_session_id,
        msg_sent_type: msg_type,
        file_name: fileNameForDoc,
        chatdetails: chat,
        captions:""
      },{
        headers:{
          tenantId: tenantId,
        }
      });


 

      socket.current.emit("send-msg", {
        to: chat.id,
        from: data.id,
        session_id: chat.chat_session_id,
        senderName: data.username,
        chatType: "outbound",
        msg,
        msgType: "web",
        userType: chat.chat_type,
        msg_sent_type: msg_type,
        chatdetails: chat,
        file_name: fileNameForDoc,
        captions:""
      });


      console.log('Third send-message',
      {
        to: chat.id,
        session_id: chat.chat_session_id,
        from: data.id,
        senderName: data.username,
        chatType: "outbound",
        msg,
        msgType: "web",
        userType: chat.chat_type,
        msg_sent_type: msg_type,
        chatdetails: chat,
        file_name: fileNameForDoc,
        captions:""
      }
      )

      socket.current.emit("last-msg-send", {
        to: chat.id,
        from: data.id,
        chatType: "outbound",
        msg,
        senderName: data.username,
        userType: chat.chat_type,
        chatdetails: chat,
      });
    }

    const msgs = [...message];
    msgs.push({
      fromSelf: true,
      message: msg,
      session_id: chat.chat_session_id,
      senderName: data.username,
      receiverName: chat.username,
      to: chat.id,
      sender: data.id,
      time: moment().format(),
      msg_sent_type: msg_type,
      file_name: fileNameForDoc,
      captions:""
    });

    setCustomerTyping(false);
    setMessage(msgs);
  };

  useEffect(() => {
    if (chat && arrivalMessage) {
      if (chat.chat_session_id == arrivalMessage.session_id) {
        arrivalMessage && setMessage((prev) => [...prev, arrivalMessage]);
      }
    }
  }, [arrivalMessage]);

  useEffect(() => {
    contentRef.current.scrollTop =
      contentRef.current.scrollHeight - contentRef.current.clientHeight;
  }, [spaceContent, arrivalMessage, message, agentList]);

  const CobrosweData = () => {
    try {
      let data = {
        url: cobrowswerinput,
        agent_id: "313007",
        tags: ["tag1", "tag2"],
        hide_selector:
          "#input1,#input2,#emailID,#empAadhaar,#uan,#cust_PAN, #mobileinfo_Pan",
      };
      coBrowseApi(data).then(function (response) {
        console.log("leadlist", response.leader_link);
        setCobrowse(response.data);
        setOpeniframemodal(response.leader_link);
        setIframeopen(true);
        window.open(response.leader_link);
        var sender_link = response.follower_link;
        handleSendMsg(sender_link, "TEXT", "");
      });
      
    } catch (error) {
      errorHandel(error,'/surfly.com/v2/sessions/')
    }
  };
// console.log("value......................",props.addTopicInput)
// console.log('tags................................',props.addTopicToggle)

  return (
  
    <>
    <div>
      <div>
        {props.contactList.length>0 && chat.chat_type=='external'?
    
        <Tabs
          defaultActiveKey="chatContainer"
          transition={false}
          activeKey={eventKey}
          onSelect={(k) => {
            setEventKey(k);
          }}
        >
          <Tab
            eventKey="requests"
            data-bs-toggle="tooltip"
            title={
              <GoRequestChanges
                className="top-tab-icons"
                onClick={handleCobrowseShow}
                style={{ width: "16px" }}
                data-placement="top"
                title="Co-Browser"
              />
            }
          ></Tab>
          
          <Tab
            eventKey="Topic"
            title={
              <GoPlus
                className="top-tab-icons"
                onClick={handleAddTopicOpen}
                style={{ width: "16px" }}
                data-toggle="tooltip"
                data-placement="top"
                title="Add Topic"
              />
            }
          ></Tab>
        </Tabs>:
        <div className="mt-3">
          <Tabs>
        </Tabs>
        </div>
       
        
      }
      </div>


      {props.addTopicToggle && (
        <div
          className="alert alert-secondary"
          role="alert"
          style={{
            position: "absolute",
            overflowa: "auto",
            width: props.togglechat ? "49%" : "100%",
          }}
        >
          <GoBookmark />
          {props.addTopicInput}
        </div>
      )}

      <div
        className="chat-container"
        style={{
          width: props.togglechat ? "49%" : "100%",
          marginLeft: 0,
        }}
      >
        <div className="log d-flex flex-column" ref={contentRef}>
       

          {props.loadmoretoggle &&
            loadmoredata.map((msg, index) => {
              return (
                <Message
                  userid={currentUserid}
                  message={msg}
                  file_name={msg.file_name}
                  message_sent_type={msg.msg_sent_type}
                  messageTxt={msg.message}
                  isSelf={msg.fromSelf}
                  createdOn={toUTCDate(msg.time)}
                  key={index}
                  sender={msg.sender}
                  data={msg}
                  displayname={msg.senderName}
                  captions={msg.captions?msg.captions:""}
                  created={moment(msg.time).format("LLL")}
                />
              );
            })}


            {chat.chat_type=='external' &&  !props.loadmoretoggle && props.contactList.length>0 ?
            
             <div>

             <div
              className="agentSideLoad "
              onClick={loadmore}
              style={{
                marginLeft: props.togglechat ? "-5rem" : "5rem",
              }}
            >
              load more
            </div>
             </div>:
          chat.chat_type=='internal' && !props.loadmoretoggle?
           <div
           className="agentSideLoad "
           onClick={loadmore}
           style={{
             marginLeft: props.togglechat ? "-5rem" : "5rem",
           }}
         >
           load more
           </div>:''
          }

{/* 
          {!props.loadmoretoggle ? (
            <div
              className="agentSideLoad "
              onClick={loadmore}
              style={{
                marginLeft: props.togglechat ? "-5rem" : "5rem",
              }}
            >
              load more
            </div>
          ) : (
            <div></div>
          )} */}

          {chat &&
            message.map((msg, index) => {
              // console.log(msg, chat);
              if (chat.chat_session_id == msg.session_id) {
                return (
                  <Message
                    userid={currentUserid}
                    message={msg}
                    file_name={msg.file_name}
                    message_sent_type={msg.msg_sent_type}
                    messageTxt={msg.message}
                    isSelf={msg.fromSelf}
                    createdOn={toUTCDate(msg.time)}
                    key={index}
                    sender={msg.sender}
                    data={msg}
                    displayname={msg.senderName}
                    created={moment(msg.time).format("LLL")}
                    captions={msg.captions?msg.captions:""}
                  />
                );
              }
            })}

          {chat &&
          session_id == chat.chat_session_id &&
          chat.chat_type == "internal" &&
          sender_id == chat.senderDetails[0]._id &&
          sender_id != "" &&
          CustomerTyping ? (
            <div>
              <div className="bot-chat-main d-flex justify-content-start">
                <div className="bot-chat text-break">
                  <div className="typing agentSide-typing typing-1"></div>
                  <div className="typing agentSide-typing typing-2"></div>
                  <div className="typing agentSide-typing typing-3"></div>
                </div>
              </div>
            </div>
          ) : (
            <div></div>
          )}

          {chat && chat.chat_type != "internal" && CustomerTyping ? (
            <div>
              <div className="bot-chat-main d-flex justify-content-start">
                <div className="bot-chat text-break">
                  <div className="typing agentSide-typing typing-1"></div>
                  <div className="typing agentSide-typing typing-2"></div>
                  <div className="typing agentSide-typing typing-3"></div>
                </div>
              </div>
            </div>
          ) : (
            <div></div>
          )}
        </div>
      </div>

      {/* {chat && chat.is_customer_disconnected ? (
        <ChatInput
          handleSendMsg={handleSendMsg}
          handleTyping={handleTyping}
          data={props}
          disconnect={true}
        />
      ) : (
        <ChatInput
          handleSendMsg={handleSendMsg}
          handleTyping={handleTyping}
          data={props}
          disconnect={false}
        />
      )} */}

{chat.chat_type=='external' && props.contactList.length>0?
    <ChatInput
    handleSendMsg={handleSendMsg}
    handleTyping={handleTyping}
    data={props}
    disconnect={false}
  />
  
:chat.chat_type=='internal'?  <ChatInput
handleSendMsg={handleSendMsg}
handleTyping={handleTyping}
data={props}
disconnect={false}
/>:''
}
      <Modal show={showcobrowse} className="transferCall-modal">
        <Modal.Header
          style={{
            padding: "6px 12px",
            margin: 0,
            fontSize: "12px",
            height: "45px",
            backgroundColor: "#294e9f",
            color: "white",
          }}
        >
          <div className="d-flex justify-content-between align-items-center w-100">
            <div>
              <Modal.Title
                style={{
                  fontSize: 15,
                  margin: "6px 0 0 0",
                  textTransform: "capitalize",
                }}
              >
                Request Co-Browser
              </Modal.Title>
            </div>
            <div>
              <AiOutlineCloseCircle onClick={handleCobrowseClose} />
            </div>
          </div>
        </Modal.Header>
        <Modal.Body>
          {" "}
          <div className="my-3">
            <div>
              <input
                className="form-control"
                onChange={(e) => setCobrowserInput(e.target.value)}
                placeholder="Ex: www.setUrl.com"
                style={{ height: "40px" }}
              ></input>
            </div>
          </div>
        </Modal.Body>

        <div className="transferOkDiv">
          <Button
            variant="primary"
            className="transferOkBtn"
            onClick={() => {
              setShowcobrowse(false);
              CobrosweData(props.follower_link);
            }}
          >
            Yes Request Co-Browser
          </Button>
        </div>
      </Modal>

      <Modal show={showAddTopic} className="transferCall-modal">
        <Modal.Header
          style={{
            padding: "6px 12px",
            margin: 0,
            fontSize: "12px",
            height: "45px",
            backgroundColor: "#294e9f",
            color: "white",
          }}
        >
          <div className="d-flex justify-content-between align-items-center w-100">
            <div>
              <Modal.Title
                style={{
                  fontSize: 15,
                  margin: "6px 0 0 0",
                  textTransform: "capitalize",
                }}
              >
                Add Topic
              </Modal.Title>
            </div>
            <div>
              <AiOutlineCloseCircle onClick={handleAddTopicClose} />
            </div>
          </div>
        </Modal.Header>
        <Modal.Body>
          {" "}
          <div className="my-3">
            <div>
              <input
                className="form-control"
                onChange={(e) => props.setAddTopicInput(e.target.value)}
                placeholder="Add Topic"
                style={{ height: "40px" }}
              ></input>
            </div>
          </div>
        </Modal.Body>

        <div className="transferOkDiv">
          <Button
            variant="primary"
            className="transferOkBtn"
            onClick={() => {
              if (props.addTopicInput.length === "") {
                props.setAddTopicToggle(false);
              } else {
                setShowAddTopic(false);
                props.setAddTopicToggle(true);
              }
            }}
          >
            Add
          </Button>
        </div>
      </Modal>
    </div>
    </>

  );
}

export default connect(mapStateToProps, {
  setAddTopicToggle,
  setAddTopicInput,
  setchatmessage,
  setcontactlist,
  setcontactlist1,
  setcontactlist2,
  setconfchatrefresh,
  setAgentList,
  setinternalchatrefresh,
  setchatdata,
  setchatid,
  setrefreshtogglechat,
  setselectedmobile,
  setselectedemail,
  setselectedusername,
  setchattype,
  setinternalchatnotify,
  setchatdataforinternal,
  setConferenceNotification,
  setavailagent,
  setloadmore,
  setSelectedchannel,
})(Chat);
