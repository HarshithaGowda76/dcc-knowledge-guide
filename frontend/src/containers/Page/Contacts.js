import React, { useState, useEffect } from "react";
import { IoCallOutline } from "react-icons/io5";
import { BiMessageDots } from "react-icons/bi";
import { useNavigate } from "react-router-dom";
import plusIcon from "../../assets/img/plus-icon.svg";
import "./contact.scss";
import { Modal } from "react-bootstrap";
import SidebarHeader from "./SidebarHeader";
import { ImCross } from "react-icons/im";
import {FaBriefcase} from "react-icons/fa";
import Tabs from "react-bootstrap/Tabs";
import { Tab, Button, Form } from "react-bootstrap";
import axios from "axios";
import { BaseUrl } from "./Constants/BaseUrl";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { AiOutlineMail } from "react-icons/ai";
import NavBarHeader from "./NavBarHeader";
const Contact = () => {
  const tenantId = localStorage.getItem("TenantId");

  const [incomingCall, setIncomingCall] = useState(false);

  useEffect(() => {
    localStorage.setItem("makecall", "no");
    let refreshIntervalId = setInterval(() => {
      let check_incoming_call = localStorage.getItem("incomingCall");
      let incomingCall_number = localStorage.getItem("incomingCall_number");
      if (check_incoming_call == "yes") {
        setIncomingCall(true);
      } else {
        setIncomingCall(false);
      }
    }, 1000);
    retrieveContacts();
  }, []);

  const [key, setKey] = useState("contact");
  const navigate = useNavigate();

  const [show, setShow] = useState(false);

  const [contactUserName, setContactUserName] = useState("");
  const [contactPhoneNumber, setContactPhoneNumber] = useState("");
  const [contactEmail, setContactEmail] = useState("");
  const [contactInterest, setContactInterest] = useState("");
  const [contactOccupation, setContactOccupation] = useState("");
  const [contactIncome, setContactIncome] = useState("");

  const [contactList, setContactList] = useState([]);

  const [showContactName, setShowContactName] = useState("");
  const [showContactNameSlice, setShowContactNameSlice] = useState("");
  const [showContactEmail, setShowContactEmail] = useState("");
  const [showContactPhone, setShowContactPhone] = useState("");

  const [showContactInterest, setShowContactInterest] = useState("");

  const [showContactIncome, setShowContactIncome] = useState("");
  const [showContactOcc, setShowContactOcc] = useState("");
  const [active, setActive] = useState("1");
  const [search, setSearch] = useState(null);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const placeCall = () => {
    localStorage.setItem("makecall", "yes");
    localStorage.setItem("makecallNumber", showContactPhone);
    if (showContactPhone) {
      navigate("/chat", { state: { phonenumber: showContactPhone } });
    } else {
      alert("phone number empty");
    }

    // myRef_new.current.childMethod();
  };

  const searchSpace = (event) => {
    let keyword = event.target.value;
    // console.log(keyword);
    setSearch(keyword);
  };

  // function randomColor() {
  //   let hex = Math.floor(Math.random() * 0xffffff);
  //   let color = "#" + hex.toString(16);

  //   return color;
  // }

  const goToChatPage = () => {
    navigate("/chat");
  };

  Array.prototype.getRandom = function (cut) {
    var i = Math.floor(Math.random() * this.length);
    if (cut && i in this) {
      return this.splice(i, 1)[0];
    }
    return this[i];
  };

  const retrieveContacts = () => {
    let data = {
      offset: "0",
      limit: "15",
    };

    axios
      .post(BaseUrl + "/contact/listContact", data,{
        headers: {
          tenantId: tenantId,
        }
      })
      .then((res) => {
        if (res.status) {
          let resp = res.data.data;

          let details = [];
          let count_id = 1;
          let randomColors = [
            "aqua",
            "black",
            "blue",
            "fuchsia",
            "gray",
            "green",
            "lime",
            "maroon",
            "navy",
            "olive",
            "orange",
            "purple",
            "red",
            "silver",
            "teal",
            "yellow",
          ];

          resp.forEach((element, index) => {
            details.push({
              name: element["name"],
              phonenumber: element["phonenumber"],
              email: element["email"],
              interest: element["interest"],
              occupation: element["occupation"],
              account_status: element["account_status"],
              income: element["income"],
              created_at: element["created_at"],
              id: element["id"],
              count_id: count_id,
              b_color: randomColors.getRandom(),
            });
            count_id++;
          });
          setContactList(details);
          setShowContactNameSlice(
            res.data.data[0].name.slice(0, 2).toUpperCase()
          );
          setShowContactName(res.data.data[0].name);
          setShowContactEmail(res.data.data[0].email);
          setShowContactIncome(res.data.data[0].income);
          setShowContactInterest(res.data.data[0].interest);
          setShowContactPhone(res.data.data[0].phonenumber);
          setShowContactOcc(res.data.data[0].occupation);
        }
      })
      .catch((err) => {
        console.log("try after sometimes");
      });
  };

  const createContact = () => {
    if (
      !contactUserName ||
      !contactUserName ||
      !contactEmail ||
      !contactInterest ||
      !contactOccupation ||
      !contactIncome
    ) {
      alert("Enter all required field");
      return;
    }

    let bodyParameters = {
      name: contactUserName,
      phonenumber: contactUserName,
      email: contactEmail,
      interest: contactInterest,
      occupation: contactOccupation,
      account_status: "Available",
      income: contactIncome,
    };

    axios
      .post(BaseUrl + "/contact/createContact", bodyParameters,{
        headers:{
          tenantId: tenantId,

        }
      })
      .then((res) => {
        if (res.status) {
          retrieveContacts();
          successToast();
          handleClose();
        }
      })
      .catch((err) => {
        console.log("try after sometimes");
      });
  };

  const successToast = () => {
    toast.success("Contact Created", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };

  const getContactDetails = (e) => {
    setActive(e.count_id);

    setShowContactNameSlice(e.name.slice(0, 2).toUpperCase());
    setShowContactName(e.name);
    setShowContactEmail(e.email);
    setShowContactIncome(e.income);
    setShowContactInterest(e.interest);
    setShowContactPhone(e.phonenumber);
    setShowContactOcc(e.occupation);
  };

  const customStyles = {
    headCells: {
      style: {
        border: "1px solid #dee2e6",
        fontWeight: "600",
        padding: "0px 5px",
        color: "#21479D",
      },
    },
    cells: {
      style: {
        borderRight: "1px solid #dee2e6",
        borderBottom: "1px solid #dee2e6",
      },
    },
    rows: {
      style: {
        borderLeft: "1px solid #dee2e6",
      },
    },
  };



  let name = localStorage.getItem("display");

  return (
    <>
      <ToastContainer />
      <NavBarHeader></NavBarHeader>
      <SidebarHeader />
      {!incomingCall && (
        <div className="container contact_content">
          <div className="rel-action">
            {/* <button title="Add User " className="btn add-btn "> */}
            <img
              src={plusIcon}
              onClick={handleShow}
              style={{ height: "65px" }}
            />
            {/* </button> */}
          </div>
          <div className="row">
            <div
              className="col-md-3"
              style={{ borderRight: "1px solid grey", height: "540px" }}
            >
              <h5>Contacts</h5>

              <hr />

              <div className="search " style={{ margin: "0 0 15px 0" }}>
                <input
                  type="text"
                  placeholder=" Search"
                  name="search"
                  style={{ width: "100%", padding: "5px 0" }}
                  onChange={(e) => searchSpace(e)}
                />
              </div>
              <div
                className="scroll"
                style={{ overflow: "scroll", height: "100%" }}
              >
                {contactList
                  .filter((item) => {
                    if (search == null) return item;
                    else if (
                      item.name.toLowerCase().includes(search.toLowerCase())
                    ) {
                      return item;
                    }
                  })
                  .map((item) => (
                    <div
                      className={`d-flex justify-content-start list-chat-contact ${
                        active == item.count_id ? "active-contact" : ""
                      }`}
                      onClick={() => getContactDetails(item)}
                      id={item.count_id}
                    >
                      <div
                        className="text-center d-flex justify-content-center align-items-center"
                        style={{
                          borderRadius: "50%",
                          backgroundColor: "grey",
                          width: "40px",
                          height: "40px",
                          backgroundColor: item.b_color,
                        }}
                      >
                        {item.name.slice(0, 2).toUpperCase()}
                      </div>
                      <div className="mt-1 ml-2">
                        <h6>{item.name}</h6>
                        <p
                          style={{
                            textOverflow: "ellipsis",
                            width: "165px",
                            overflow: "hidden",
                          }}
                        >
                          {item.email}
                        </p>
                      </div>
                    </div>
                  ))}
              </div>
            </div>

            <div className="col-md-9">
              <div className="row user-profile-page">
                <div className="col-md-6">
                  <div
                    className="row"
                    style={{ marginTop: "20px", padding: "15px 10px" }}
                  >
                    <div className="col-md-3 d-flex justify-content-start">
                      <div
                        className="text-center d-flex justify-content-center align-items-center"
                        style={{
                          borderRadius: "50%",
                          backgroundColor: "grey",
                          width: "70px",
                          height: "70px",
                        }}
                      >
                        {showContactNameSlice}
                      </div>
                    </div>
                    <div className="col-md-9">
                      <h4>{showContactName}</h4>
                      <p className="mb-2">Imperium Software Technologies</p>
                      <div className="d-flex justify-content-start align-items-center">
                        <p onClick={placeCall} style={{ cursor: "pointer" }}>
                          <IoCallOutline
                            size="15"
                            style={{ cursor: "pointer" }}
                          />{" "}
                          Call
                        </p>
                        <p className="ml-4">
                          <BiMessageDots
                            size="15"
                            style={{ cursor: "pointer" }}
                            onClick={goToChatPage}
                          />
                        </p>
                      
                      </div>
                    </div>
                  </div>
                  <div className="mt-4">
                    <Tabs
                      id="controlled-tab-example"
                      activeKey={key}
                      onSelect={(k) => setKey(k)}
                      className="ml-3"
                    >
                      
                      <Tab eventKey="contact" title="Contact" className="ml-3">
                        <div
                          className="tab_content_contact mb-4 mt-4"
                          style={{
                            backgroundColor: "#f8f9fd ",
                            marginLeft: "0px",
                            color: "#000",
                          }}
                        >
                          <div className="row">
                            <div className="col-lg-12">
                              <AiOutlineMail
                                style={{
                                  position: "absolute",
                                  top: "2px",
                                  fontSize: "22px",
                                }}
                              />{" "}
                              <div style={{ marginLeft: "32px" }}>
                                <span style={{ fontWeight: "500" }}>Email</span>{" "}
                                <p>{showContactEmail}</p>
                              </div>
                            </div>
                          </div>
                          <div className="row mt-4">
                            <div className="col-lg-12">
                              <IoCallOutline
                                style={{
                                  position: "absolute",
                                  top: "2px",
                                  fontSize: "20px",
                                }}
                              />
                              <div style={{ marginLeft: "32px" }}>
                                <span style={{ fontWeight: "500" }}>
                                  Phone Number
                                </span>{" "}
                                <p>{showContactPhone}</p>
                              </div>
                            </div>
                          </div>

                          <div className="row mt-4">
                            <div className="col-lg-12">
                              <FaBriefcase
                                style={{
                                  position: "absolute",
                                  top: "2px",
                                  fontSize: "20px",
                                }}
                              />
                              <div style={{ marginLeft: "32px" }}>
                                <span style={{ fontWeight: "500" }}>
                                  Occupation
                                </span>{" "}
                                <p>{showContactOcc}</p>
                              </div>
                            </div>
                           
                          </div>

                          
                        </div>
                      </Tab>
                      
                    </Tabs>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}

      <Modal show={show} onHide={handleClose} size="xl">
        <Modal.Header className="bg-dark text-white">
          <Modal.Title>Add contacts</Modal.Title>
          <span className="mt-2">
            <ImCross onClick={handleClose} />
          </span>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            <div className="col-md-6">
              <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label className="requiredLabel"> Name</Form.Label>
                  <Form.Control
                    type="name"
                    placeholder="Enter name"
                    onChange={(e) => setContactUserName(e.target.value)}
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label className="requiredLabel">
                    Email address
                  </Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    onChange={(e) => setContactEmail(e.target.value)}
                  />
                </Form.Group>

              
              </Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="requiredLabel">Occupation</Form.Label>

                <select
                  className="form-control form-select"
                  onChange={(e) => setContactOccupation(e.target.value)}
                >
                  <option selected>Select Occupation</option>
                  <option value="Employed">Employed</option>
                  <option value="Self Employed">Self Employed</option>
                </select>
              </Form.Group>
            </div>
            <div className="col-md-6">
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="requiredLabel">Phone Number</Form.Label>
                <Form.Control
                  type="text"
                  onChange={(e) => setContactPhoneNumber(e.target.value)}
                  placeholder="Enter phone number"
                />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicEmail">
                {" "}
                <Form.Label className="requiredLabel">Interest</Form.Label>
                <select
                  className="form-control form-select"
                  onChange={(e) => setContactInterest(e.target.value)}
                >
                  <option selected>Select Interest</option>
                  <option value="Account Open">Account Open</option>
                  <option value="Loan">Loan</option>
                  <option value="Credit Card">Credit Card</option>
                </select>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label className="requiredLabel">Income</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter Income"
                  onChange={(e) => setContactIncome(e.target.value)}
                />
              </Form.Group>

        
            </div>
          </div>
        </Modal.Body>
        <div className="d-flex  justify-content-end">
          <Button className="btn btn-warning" onClick={handleClose}>
            Cancel
          </Button>
          <Button onClick={createContact}>Submit</Button>
        </div>
      </Modal>
    </>
  );
};

export default Contact;
