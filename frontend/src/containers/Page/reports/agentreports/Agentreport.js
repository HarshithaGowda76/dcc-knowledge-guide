import React, { useState, useEffect } from "react";
import SidebarHeader from "../../SidebarHeader";
import NavBarHeader from "../../NavBarHeader";
import "../../../../assets/assets_newchat/library/bootstrap-5.1.3/css/bootstrap.min.css";
import "../../../../assets/assets_newchat/assets/fonts/fontawesome-free-6.1.1-web/css/all.css";
import "./agentreport.css";
import "../../../../assets/assets_newchat/library/animate/animate.min.css";
import "../../../../assets/assets_newchat/library/slimselect/slimselect.min.css";
import { Pagination } from "@mui/material";
import { BaseUrl, errorApi, frontendBaseurl } from "../../Constants/BaseUrl";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import axios from "axios";
import moment from "moment";
import { Modal } from "react-bootstrap";
import { AiFillCloseCircle } from "react-icons/ai";

const Agentreport = () => {
  const [showfilter, setshowfilter] = useState("sidebar_filter");
  const tenantId = localStorage.getItem("TenantId");
  var someDate = new Date();
  var date = someDate.setDate(someDate.getDate());
  var defaultValue = new Date(date).toISOString().split("T")[0];
  const [agentdata, setAgentdata] = useState([]);
  const [fromdate, setFromdate] = useState(defaultValue);
  const [todate, setTodate] = useState(defaultValue);
  const [currentfromdate, setCurrentFromdate] = useState(defaultValue);
  const [currenttodate, setCurrentTodate] = useState(defaultValue);
  const [pagination, setPagination] = useState(0);
  const [page, setPage] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    if(page>1){
    agentReportData();
    }
  }, [page]);



  const errorHandel = async (error, endpoint) => {
    const tenantID = localStorage.getItem("TenantId");
    try {
      const payload = {
        servicename: 'DCCCHAT',
        logs: error,
        description: endpoint

      };

      const header = {
        headers: {
          // Authorization: "Bearer " + token,
          // "Content-Type": "application/json",
          tenantid: tenantID,
        },
      };
      const { data } = await axios.post(
        errorApi + "/UIlogs/createLogs",
        payload,
        header
      );
      if (data.status) {
        console.log("from error api", data);
      }
    } catch (error) {
      console.log('error', error)
    }
  };



  const logout = async () => {
    const access_token = localStorage.getItem("access_token");
    let data = await JSON.parse(localStorage.getItem("tokenAgent"));
    const id = localStorage.getItem('TenantId');
    let userID = data.user_id;
    try {
      if (data) {
        const update = await axios.post(
          BaseUrl + "/users/logout/" + userID,
          {},
          {
            headers: {
              Authorization: "Bearer " + access_token,
              "Content-Type": "application/json",
              tenantId: tenantId,
            },
          }
        );
        // Clear localStorage items
        console.log("ertyuytdslogout", update.data.success);
        if (update.data.success == true) {
          localStorage.removeItem('AvayaUsername')
          localStorage.removeItem('tab')
          localStorage.removeItem('timer_connect_sec')
          localStorage.removeItem('AvayaPassword')
          localStorage.removeItem('AvayaDomain')
          localStorage.removeItem('client')
          localStorage.removeItem('statusValue')
          localStorage.removeItem('emailDisplay')
          localStorage.removeItem('timer_connect_min')
          localStorage.removeItem('tokenAgent')
          localStorage.removeItem('timer_status')
          localStorage.removeItem('NameDisplay')
          localStorage.removeItem('access_token')
          localStorage.removeItem('timer_connect_hour')

          if (!id) {
            window.location.href = 'error.html';
            console.error('ID not found in local storage');
          } else {
            const loginUrl = `${frontendBaseurl}/?tenantID=${encodeURIComponent(id)}`;
            window.location.href = loginUrl;
          }

          // navigate("/");
        }
      }
    } catch (error) {
      errorHandel(error, "/users/logout/")
      console.log(error);
    }
  };

  const agentReportData = () => {

    setIsLoading(true)
    const token = localStorage.getItem("access_token");
     setCurrentFromdate(fromdate)
     setCurrentTodate(todate)
    let data = {
      from_date: fromdate,
      to_date: todate,
      offset: page == 1 ? 0 : (page - 1) * 5,
      limit: 5,
    };

    const header = {
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
        tenantId: tenantId,
      },
    };
    axios
      .post(BaseUrl + "/dashboard/tasklist", data, header, {})
      .then((res) => {
        if (res.data.success == true) {
          setIsLoading(false)
          console.log("agentReport", res.data.Data);
          setAgentdata(res.data.Data);
          setPagination(res.data.count);
        }else if(res.data.success==false){
          setIsLoading(false)
          setAgentdata([])
          setPagination(0);

          toast.warn("No Data Found", {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }else{
          console.log('error')
        }
      })
      .catch((error) => {
        errorHandel(error, "/dashboard/tasklist/")

        // setShow(true);
      });
  };


  const oldDateAgentReport =()=>{
    setIsLoading(true)
    const token = localStorage.getItem("access_token");
     setFromdate(currentfromdate)
     setTodate(currenttodate)
    let data = {
      from_date: currentfromdate,
      to_date: currenttodate,
      offset: page == 1 ? 0 : (page - 1) * 5,
      limit: 5,
    };

    const header = {
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
        tenantId: tenantId,
      },
    };
    axios
      .post(BaseUrl + "/dashboard/tasklist", data, header, {})
      .then((res) => {
        if (res.data.success == true) {
          setIsLoading(false)
          console.log("agentReport", res.data.Data);
          setAgentdata(res.data.Data);
          setPagination(res.data.count);
        }else if(res.data.success==false){
          setIsLoading(false)
          setAgentdata([])
          setPagination(0);

          toast.warn("No Data Found", {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }else{
          console.log('error')
        }
      })
      .catch((error) => {
        errorHandel(error, "/dashboard/tasklist/")

        // setShow(true);
      });

  }

  

  const handleChange = (event, value) => {
    setPage(value);
  };

  return (
    <>
      <NavBarHeader />
      <SidebarHeader />
      <ToastContainer/>
      <nav className={showfilter} style={{ top: '59px' }}>
        <div className="filter col-md-12 shadow p-4 ">
          <div className="d-flex justify-content-between align-items-center mb-3">
            <p className="mb-0">Filters</p>
            <div className="dismiss ">
              <span
                className="material-symbols-outlined"
                onClick={() => {
                  setshowfilter("sidebar_filter");
                  oldDateAgentReport()
                }}
              >
                cancel
              </span>
            </div>
          </div>

          <div className="filter_form">
            <form className=" fillter_form2 " style={{ height: '68vh' }}>
              <div className="custom_div">
                <div className="mb-3 d-flex flex-column text-start">
                  <label for="fromdate" className="form-label">
                    From
                  </label>
                  <input
                    type="date"
                    className="form-control"
                    id=""
                    aria-describedby="date"
                    value={fromdate} 
                    onChange={(e) => {setFromdate(e.target.value);localStorage.setItem('fromdate',e.target.value);setPage(1)}}
                  />
                </div>
                <div className="mb-3 d-flex flex-column text-start">
                  <label for="todate" className="form-label">
                    To
                  </label>
                  <input
                    type="date"
                    className="form-control"
                    id=""
                    aria-describedby="date"
                    onChange={(e) => {setTodate(e.target.value);localStorage.setItem('todate',e.target.value)}} 
                    value={todate}
                  />
                </div>
              </div>
            </form>
            <div className="filter_submit">
              <div className="d-flex">
                <div className="col-md-12 ps-2">
                  <button type="button" className="btn btn-primary w-100 " onClick={() => { agentReportData(); setshowfilter('sidebar_filter') }}>
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <header className="header_role ">
        <div className="conatiner-fluid">
          <nav className="navbar " style={{ paddingTop: "0px", paddingBottom: "0px" }}>
            <div className="container-fluid p-0">
              <div className="hed_left d-flex"></div>
              <div className="hed_right d-flex ">
                <button
                  type="button"
                  className="btn btn-primary-1 me-2 d-flex-p btn-sm open-filter"
                  style={{ background: "#1473e6", color: "white" }}
                  onClick={() => {
                    setshowfilter("sidebar_filter active-r");
                  }}
                >
                  <span className="material-symbols-outlined me-1">
                    {" "}
                    filter_list
                  </span>
                  Filters
                </button>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <section className="dashboard mt-3">
        <div className="container-fluid">
          <div className="wapper_sub">
            <div className="sub_wapper">
              <div className="container-fluid p-0">
                <div className="row">
                  <div className="col-md-12 p-0 animate__animated animate__fadeInUp all-ticket">
                    <div className="tickt-table container-fluid">
                      <table className="table table-sm  table-hover align-middle">
                        <thead>
                          <tr>
                            <th scope="col">Agent Name</th>
                            <th scope="col">Break Time</th>
                            <th scope="col">Active Time</th>
                            <th scope="col">Completed</th>
                            <th scope="col">Active Chat</th>
                            <th scope="col">Date</th>
                            <th scope="col">Transfered Out</th>
                            <th scope="col">Transfered In</th>
                          </tr>
                        </thead>
                        {
                          isLoading ?
                            <tr className="h-100">
                              <td colSpan="10" style={{ textAlign: 'center',height:'100vh' }}>
                                <div class="spinner-border text-secondary" role="status" style={{marginTop:'13vh'}}>
                                  <span class="sr-only">Loading...</span>
                                </div>
                              </td>
                            </tr> 
                            :
                            <tbody>
                              {agentdata.map((item) => {

                                return (
                                  <>




                                    <tr>
                                      <td>
                                        {item.agent_id == null ||
                                          item.agent_id == undefined ||
                                          item.agent_id == ""
                                          ? "--"
                                          : item.agent_id.username}
                                      </td>
                                      <td>
                                        {moment
                                          .duration(item.total_break_time, "seconds")
                                          .format("hh:mm:ss")}
                                      </td>
                                      <td>
                                        {" "}
                                        {moment
                                          .duration(item.total_active_time, "seconds")
                                          .format("hh:mm:ss")}
                                      </td>
                                      <td>{item.total_completed}</td>
                                      <td>
                                        {item.total_active_chat_count == null ||
                                          item.total_active_chat_count == undefined ||
                                          item.total_active_chat_count == "" ||
                                          item.total_active_chat_count == 0
                                          ? 0
                                          : item.total_active_chat_count}
                                      </td>
                                      <td>{moment(item.created_at).format("ll")}</td>
                                      <td>
                                        {item.total_transfered_out == null ||
                                          item.total_transfered_out == undefined ||
                                          item.total_transfered_out == "" ||
                                          item.total_transfered_out == 0
                                          ? 0
                                          : item.total_transfered_out}
                                      </td>
                                      <td>
                                        {item.total_transfered_in == null ||
                                          item.total_transfered_in == undefined ||
                                          item.total_transfered_in == "" ||
                                          item.total_transfered_in == 0
                                          ? 0
                                          : item.total_transfered_in}
                                      </td>
                                    </tr>


                                  </>

                                );
                              })}
                            </tbody>
                        }
                      </table>
                      {/* pagination */}
                      <div className="mt-3 mb-3">
                        <Pagination
                          count={Math.ceil(pagination / 5)}
                          page={page}
                          color="info"
                          shape="rounded"
                          type="number"
                          style={{ float: "right" }}
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Modal
        show={show}
        onHide={handleClose}
        centered
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header style={{ height: "3rem", backgroundColor: "#294e9f" }}>
          <div className="d-flex justify-content-center align-items-center color-white fw-bold">
            <span>Alert</span>
          </div>
        </Modal.Header>
        <Modal.Body className="fw-bold">
          Session Expired Please Login Again !
        </Modal.Body>

        <button className="btn btn-danger w-25 btn-sm ms-auto" onClick={logout}>
          Logout
        </button>
      </Modal>
    </>
  );
};

export default Agentreport;
