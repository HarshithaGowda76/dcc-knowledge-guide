import React, { useState, useEffect } from "react";
import SidebarHeader from "../../SidebarHeader";
import NavBarHeader from "../../NavBarHeader";
import "../../../../assets/assets_newchat/library/bootstrap-5.1.3/css/bootstrap.min.css";
import "../../../../assets/assets_newchat/assets/fonts/fontawesome-free-6.1.1-web/css/all.css";
import "./sessionreport.css";
import { Pagination } from "@mui/material";
import facebook from "../../../../assets/img/facebook.png";
import whatsapp from "../../../../assets/img/whatsapp.png";
import webchat from "../../../../assets/img/chat-icon.png";
import twitter from "../../../../assets/img/twitter.png";
import teams from "../../../../assets/img/teams.png";
import voice from "../../../../assets/img/voice.png";
import "../../../../assets/assets_newchat/library/animate/animate.min.css";
import "../../../../assets/assets_newchat/library/slimselect/slimselect.min.css";
import { BaseUrl, excelDownloadUrl, frontendBaseurl } from "../../Constants/BaseUrl";
import axios from "axios";
import moment from "moment";
import { AiFillCloseCircle } from "react-icons/ai";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Badge, Modal } from "react-bootstrap";
import FileSaver from "file-saver";

const Sessionreport = () => {
  const [showfilter, setshowfilter] = useState("sidebar_filter");
  const tenantId = localStorage.getItem("TenantId");
  var someDate = new Date();
  var date = someDate.setDate(someDate.getDate());
  var defaultValue = new Date(date).toISOString().split("T")[0];

  const [fromdate, setFromdate] = useState(defaultValue);
  const [todate, setTodate] = useState(defaultValue);

  const [currentfromdate, setCurrentFromdate] = useState(defaultValue);
  const [currenttodate, setCurrentTodate] = useState(defaultValue);
  const [status, setStatus] = useState("");
  const [agentid, setAgentid] = useState("");
  const [sessionreport, setSessionreport] = useState([]);
  const [agentlist, setAgentlist] = useState([]);
  const [pagination, setPagination] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [show, setShow] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const basedOnRole = JSON.parse(localStorage.getItem("tokenAgent"));

  useEffect(() => {
    agentListdropdown();
  }, []);


  useEffect(() => {
    if(page>1){
      sessionReports()
    }
  }, [page]);


  


  const errorHandel = async (error,endpoint) => {
    const tenantID = localStorage.getItem("TenantId");
    try {
      const payload = {
        servicename: 'DCCCHAT',
        logs:error,
        description:endpoint

      };

      const header = {
        headers: {
          // Authorization: "Bearer " + token,
          // "Content-Type": "application/json",
          tenantid: tenantID,
        },
      };
      const { data } = await axios.post(
        errorApi + "/UIlogs/createLogs",
        payload,
        header
      );
      if (data.status) {
        console.log("from error api", data);
      }
    } catch (error) {
      console.log('error',error)
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setIsSubmitted(true);
  };

  const logout = async () => {
    const access_token = localStorage.getItem("access_token");
    let data = await JSON.parse(localStorage.getItem("tokenAgent"));
    const id = localStorage.getItem('TenantId');
    let userID = data.user_id;
    try {
      if (data) {
        const update = await axios.post(
          BaseUrl + "/users/logout/" + userID,
          {},
          {
            headers: {
              Authorization: "Bearer " + access_token,
              "Content-Type": "application/json",
              tenantId: tenantId,
            },
          }
        );
        // Clear localStorage items
        console.log("ertyuytdslogout", update.data.success);
        if (update.data.success == true) {
          localStorage.removeItem('AvayaUsername')
          localStorage.removeItem('tab')
          localStorage.removeItem('timer_connect_sec')
          localStorage.removeItem('AvayaPassword')
          localStorage.removeItem('AvayaDomain')
          localStorage.removeItem('client')
          localStorage.removeItem('statusValue')
          localStorage.removeItem('emailDisplay')
          localStorage.removeItem('timer_connect_min')
          localStorage.removeItem('tokenAgent')
          localStorage.removeItem('timer_status')
          localStorage.removeItem('NameDisplay')
          localStorage.removeItem('access_token')
          localStorage.removeItem('timer_connect_hour')

          if (!id) {
            window.location.href = 'error.html';
            console.error('ID not found in local storage');
          } else {
            const loginUrl = `${frontendBaseurl}/?tenantID=${encodeURIComponent(id)}`;
            window.location.href = loginUrl;
          }

          // navigate("/");
        }
      }
    } catch (error) {
      errorHandel(error,"/users/logout/")
      console.log(error);
    }
  };

  const getId = (item) => {
    console.log(item);
    sessionReports(item.id);
  };

  const sessionReports = () => {
    setIsLoading(true)
    const token = localStorage.getItem("access_token");
     console.log('fromdateFromSessionFunction',fromdate)
     console.log('todateFromSessionFunction',todate)
     setCurrentFromdate(fromdate)
     setCurrentTodate(todate)
     console.log('fromdateFromSessionFunction',currentfromdate)
     console.log('todateFromSessionFunction',currenttodate)
    let data = {
      agent_id: basedOnRole.role == "Supervisor" ? agentid : basedOnRole.id,
      status: status,
      from_date: fromdate,
      to_date: todate,
      limit: 5,
      offset: page == 1 ? 0 : (page - 1) * 5,
    };



    const header = {
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
        tenantId: tenantId,
      },
    };
    axios
      .post(BaseUrl + "/reports/listDetails ", data, header, {})
      .then((res) => {
        if (res.data.success == true) {
          setIsLoading(false)
          console.log("resskillllllsessionReports", res.data.count);
          setSessionreport(res.data.Data);
          setPagination(res.data.count);
        } else if(res.data.success == false){
            toast.warn("No Data Found", {
              position: "top-right",
              autoClose: 1000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "light",
            });
            
     
          setSessionreport([]);
          setPagination(0);
          setIsLoading(false)
        }else{
        console.log('hello')
        }
      })
      .catch((error) => {
      errorHandel(error,"/reports/listDetails/")
        setShow(true);
      });
  };


  //Download Session Report

  const DownloadSessionReports = () => {
    const token = localStorage.getItem("access_token");

    let data = {
      agent_id: basedOnRole.role == "Supervisor" ? agentid : basedOnRole.id,
      status: status,
      from_date: fromdate,
      to_date: todate,
    };

    const header = {
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
        tenantId: localStorage.getItem('TenantId'),
      },
    };

    axios
      .post(BaseUrl + "/reports/listDetailsExcel", data, header, {})
      .then((res) => {
        if (res.data.status == true) {
            let file = excelDownloadUrl + res.data.message;
            FileSaver.saveAs(file);
        
        } else {
          toast.warn("No Data Found", {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
         
        }
      })
      .catch((error) => {
    console.log(error)
      });
  };

  const handleChange = (event, value) => {
    setPage(value);
  };

  const agentListdropdown = () => {
    const token = localStorage.getItem("access_token");

    const header = {
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
        tenantId: tenantId,
      },
    };

    axios
      .post(BaseUrl + "/users/listagentList", {}, header, {})
      .then((res) => {
        console.log("resskillllllagentListdropdown", res.data.data);
        setAgentlist(res.data.data);
      })
      .catch((err) => {
        setShow(true);
      });
  };


  const oldDate =()=>{
    console.log('currentfromdateOldDate',currentfromdate)
    console.log('currenttodateFromoldDate',currenttodate)
   setFromdate(currentfromdate)
   setTodate(currenttodate) 
   setIsLoading(true)
   const token = localStorage.getItem("access_token");
    console.log('fromdateFromSessionFunction',currentfromdate)
    console.log('todateFromSessionFunction',currenttodate)
   let data = {
     agent_id: basedOnRole.role == "Supervisor" ? agentid : basedOnRole.id,
     status: status,
     from_date: currentfromdate,
     to_date: currenttodate,
     limit: 5,
     offset: page == 1 ? 0 : (page - 1) * 5,
   };
   const header = {
     headers: {
       Authorization: "Bearer " + token,
       "Content-Type": "application/json",
       tenantId: tenantId,
     },
   };
   axios
     .post(BaseUrl + "/reports/listDetails ", data, header, {})
     .then((res) => {
       if (res.data.success == true) {
         setIsLoading(false)
         console.log("resskillllllsessionReports", res.data.count);
         setSessionreport(res.data.Data);
         setPagination(res.data.count);
       } else if(res.data.success == false){
           toast.warn("No Data Found", {
             position: "top-right",
             autoClose: 1000,
             hideProgressBar: false,
             closeOnClick: true,
             pauseOnHover: true,
             draggable: true,
             progress: undefined,
             theme: "light",
           });
           
    
         setSessionreport([]);
         setPagination(0);
         setIsLoading(false)
       }else{
       console.log('hello')
       }
     })
     .catch((error) => {
     errorHandel(error,"/reports/listDetails/")
       setShow(true);
     });
  }

  return (
    <>
      <NavBarHeader />
      <SidebarHeader />
      <ToastContainer/>
      <nav className={showfilter} style={{top:'59px'}}>
        <div className="filter col-md-12 shadow p-4">
          <div className="d-flex justify-content-between align-items-center mb-3">
            <p className="mb-0">Filters</p>
            <div className="dismiss ">
              <span
                className="material-symbols-outlined"
                onClick={() => {
                  setshowfilter("sidebar_filter");
                 oldDate();
                }}
              >
                cancel
              </span>
            </div>
          </div>

          <div className="filter_form">
            <form className=" fillter_form2 "style={{height:'68vh'}} onSubmit={handleSubmit}>
              <div className="custom_div">
                <div className="mb-3 d-flex flex-column text-start">
                  <label for="fromdate" className="form-label">
                    From
                  </label>
                  <input
                    type="date"
                    className="form-control"
                    id=""
                    aria-describedby="date"
                    value={fromdate}
                    onChange={(e) => {setFromdate(e.target.value);setPage(1)}}
                  />
                </div>
                <div className="mb-3 d-flex flex-column text-start">
                  <label for="todate" className="form-label">
                    To
                  </label>
                  <input
                    type="date"
                    className="form-control"
                    id=""
                    aria-describedby="date"
                    value={todate}
                    onChange={(e) => setTodate(e.target.value)}
                  />
                </div>
              </div>
              <div class="mb-3 d-flex flex-column text-start">
                <label for="Select" class="form-label">
                  Status
                </label>
                <select
                  class="form-select"
                  aria-label="Default select example"
                  onChange={(e) => setStatus(e.target.value)}
                >
                  <option value="">Select Status</option>
                  <option value="Accept">Chat Accepted</option>
                  <option value="chatEnded">Chat Ended</option>
                  <option value="queued">Queued</option>
                  <option value="customerdisconnect">
                    Customer Disconnect
                  </option>
                </select>
              </div>

              <div class="mb-3 d-flex flex-column text-start">
                <label for="Select" class="form-label">
                  {basedOnRole == "Supervisor" ? (
                    <p className="mx-1">Agent Name : </p>
                  ) : (
                    ""
                  )}
                </label>
                {basedOnRole.role == 'Supervisor' ?
                  <select
                    class="form-select"
                    aria-label="Default select example"
                    onChange={(e) => setAgentid(e.target.value)}
                  >
                    <option value="">Select Agent Name</option>
                    {agentlist.map((item) => {
                      return <option value={item.id}>{item.username}</option>;
                    })}
                  </select>:''
                }
              </div>

                <div className="col-md-12 ps-2">
                  <button type="button" className="btn btn-primary w-100 text-white" onClick={()=>{sessionReports();setshowfilter('sidebar_filter')}}>
                    Submit
                  </button>
                </div>
            </form>
            <div className="filter_submit">
              <div className="d-flex">
               
              
              </div>
            </div>
          </div>
        </div>
      </nav>
      <header className="header_role">
        <div className="conatiner-fluid">
          <nav
            className="navbar"
            style={{ paddingTop: "0px", paddingBottom: "0px" }}
          >
            <div className="container-fluid p-0">
              <div className="hed_left d-flex"></div>
              <div className="hed_right d-flex ">
                <button className="btn btn-primary"
                  style={{ background: "#1473e6", color: "white" }}
                  onClick={()=>DownloadSessionReports()}
                >
                <span className="material-symbols-outlined me-1">
                    {" "}
                    download
                  </span>
                  Session Report
                </button>
                <button
                  type="button"
                  className="btn btn-primary-1 me-2 d-flex-p btn-sm open-filter"
                  style={{ background: "#1473e6", color: "white" }}
                  onClick={() => {
                    setshowfilter("sidebar_filter active-r");
                  }}
                >
                  <span className="material-symbols-outlined me-1">
                    {" "}
                    filter_list
                  </span>
                  Filters
                </button>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <section className="dashboard mt-2">
        <div className="container-fluid" style={{paddingLeft:'7px'}}>
          <div className="wapper_sub">
            <div className="sub_wapper">
              <div className="container-fluid p-0">
                <div className="row">
                  <div className="col-md-12 p-0 animate__animated animate__fadeInUp all-ticket">
                    <div className="tickt-table container-fluid">
                      <table className="table table-sm  table-hover align-middle">
                        <thead>
                          <tr>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Phone Number</th>
                            <th scope="col">Email</th>
                            <th scope="col">Channel</th>
                            <th scope="col">Status</th>
                            <th scope="col">Conference</th>
                            <th scope="col">Transfered</th>
                            <th scope="col">Skillset</th>
                            <th scope="col">Language</th>
                            <th scope="col">Agent Name</th>
                            <th scope="col">Chat InitiatedAt</th>
                            <th scope="col">Chat StartedAt</th>
                            <th scope="col">Chat EndedAt</th>
                            <th scope="col">Transfered Agent Name</th>
                            <th scope="col">Conferance Agent Name</th>
                          </tr>
                        </thead>

                        {
                          isLoading ?
                            <tr className="h-100">
                              <td colSpan="15" style={{ textAlign: 'center',height:'100vh' }}>
                                <div class="spinner-border text-secondary" role="status" style={{marginTop:'13vh'}}>
                                  <span class="sr-only">Loading...</span>
                                </div>
                              </td>
                            </tr> :
                        <tbody>
                          {sessionreport.map((item) => {
                            return (
                              <tr>
                                <td>{item.name}</td>
                                <td>
                                  {item.phone_number}
                                </td>
                                <td >{item.email}</td>
                                <td >
                                  {item.channel == "webchat" ? (
                                    <img
                                      className="icons-size"
                                      src={webchat}
                                      alt="webchat"
                                    />
                                  ) : item.channel == "from_whatsapp" ? (
                                    <img
                                      className="icons-size"
                                      src={whatsapp}
                                      alt="webchat"
                                    />
                                  ) : item.channel == "from_twitter" ? (
                                    <img
                                      className="icons-size"
                                      src={twitter}
                                      alt="webchat"
                                    />
                                  ) : item.channel == "voice" ? (
                                    <img
                                      className="icons-size"
                                      src={voice}
                                      alt="webchat"
                                    />
                                  ) : item.channel == "from_facebook" ? (
                                    (
                                      <img
                                        className="icons-size"
                                        src={facebook}
                                        alt="webchat"
                                      />
                                    ) || item.channel == "from_teams"
                                  ) : (
                                    <img
                                      className="icons-size"
                                      src={teams}
                                      alt="webchat"
                                    />
                                  )}
                                </td>
                                <td>
                                  {item.status == "chatEnded" ? (
                                    <Badge variant="secondary">Completed</Badge>
                                  ) : (
                                    <Badge variant="success">Active </Badge>
                                  )}
                                </td>
                                <td>
                                  {item.conference == true ? "true" : "false"}
                                </td>
                                <td>
                                  {item.transferred == true ? "true" : "false"}
                                </td>
                                <td>{item.skillset}</td>
                                <td>{item.language}</td>
                                {/* <td>{item.agent_name[0]?item.agent_name[0]:item.agent_name}</td> */}
                                <td>
                                  {item.agent_name ? item.agent_name : ""}
                                </td>
                                {/* agent_name */}
                                <td>
                                  {moment(item.chat_arrival_at).format(
                                    "L hh:mm:ss A"
                                  )}
                                </td>
                                <td>
                                  {moment(item.chat_started_at).format(
                                    "L hh:mm:ss A"
                                  )}
                                </td>
                                <td>
                                  {item.chat_ended_at == null ||
                                  item.chat_ended_at == undefined ||
                                  item.chat_ended_at == ""
                                    ? "--"
                                    : moment(item.chat_ended_at).format(
                                        "L hh:mm:ss A"
                                      )}
                                </td>
                                <td>
                                  {item.transfer_agent_name == null ||
                                  item.transfer_agent_name == undefined ||
                                  item.transfer_agent_name == ""
                                    ? "--"
                                    : item.transfer_agent_name[0]}
                                </td>
                                <td>
                                  {item.conference_agent_name == null ||
                                  item.conference_agent_name == undefined ||
                                  item.conference_agent_name == ""
                                    ? "--"
                                    : item.conference_agent_name[0]}
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
}
                      </table>
                      {/* pagination */}
                      <div className="mt-3 mb-3">
                        <Pagination
                          count={Math.ceil(pagination / 5)}
                          page={page}
                          color="info"
                          shape="rounded"
                          type="number"
                          style={{ float: "right" }}
                          onChange={handleChange}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Modal 
      show={show} 
      onHide={handleClose} 
      centered
      backdrop="static"
      keyboard={false}
      >
        <Modal.Header style={{ height: "3rem", backgroundColor: "#294e9f" }}>
          <div className="d-flex justify-content-center align-items-center color-white fw-bold">
            <span>Alert</span>
          </div>
        </Modal.Header>
        <Modal.Body className="fw-bold">
          Session Expired Please Login Again !
        </Modal.Body>

        <button className="btn btn-danger w-25 btn-sm ms-auto" onClick={logout}>
          Logout
        </button>
      </Modal>
    </>
  );
};

export default Sessionreport;
