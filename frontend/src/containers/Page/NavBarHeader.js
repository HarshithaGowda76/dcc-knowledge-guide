import React, { useEffect, useState } from "react";
import inaipi from "../../assets/img/Inaipi_Logo-1.2.png";
import "./NavBarHeader.scss";
import { useNavigate } from "react-router-dom";
import $ from "jquery";
import { connect } from "react-redux";
import {
  setcontactlist,
  setcontactlist1,
} from "../../redux/actions/spaceActions";
import { BaseUrl, AvcUrl, frontendBaseurl, errorApi } from "./Constants/BaseUrl";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Sdk from "../../DialerComponent/Sdk";

const mapStateToProps = (state) => {
  return {
    contactList: state.data.contactList,
    contactList1: state.data.contactList1,
  };
};
const NavBarHeader = (props) => {
  const tenantId = localStorage.getItem("TenantId");
  const [showNotification, setShowNotification] = useState(false);
  const [showUserProfile, setShowUserProfile] = useState(false);
  const [showReadyButton, setReadyButton] = useState(true);
  const [showFinishButton, setFinishButton] = useState(false);

  const [showConnectedStatus, setConnectedStatus] = useState(true);
  const [showReadyStatus, setReadyStatus] = useState(false);
  const [hideLogout, setHideLogout] = useState(true);
  const [showingStatus, setShowingStatus] = useState("Ready");
  const [showingStatusClass, setShowingStatusClass] = useState("showStatus");
  const [rmState, setRmState] = useState(true);
  const [showStatusChannel, setShowStatusChannel] = useState("Logged Out");
  const [timerOne, setTimerOne] = useState(0);
  const [timerTwo, setTimerTwo] = useState(0);
  const [userstatus, setUserstatus] = useState([]);

  const show_notification = () => {
    setShowNotification(true);
    setShowUserProfile(false);
  };
  const close_notification = () => {
    setShowNotification(false);
  };
  const show_userprofile = () => {
    setShowUserProfile(true);
    setShowNotification(false);
  };
  const close_userprofile = () => {
    setShowUserProfile(false);
  };


  const errorHandel = async (error,endpoint) => {
    const tenantID = localStorage.getItem("TenantId");
    try {
      const payload = {
        servicename: 'DCCCHAT',
        logs:error,
        description:endpoint

      };

      const header = {
        headers: {
          // Authorization: "Bearer " + token,
          // "Content-Type": "application/json",
          tenantid: tenantID,
        },
      };
      const { data } = await axios.post(
        errorApi + "/UIlogs/createLogs",
        payload,
        header
      );
      if (data.status) {
        console.log("from error api", data);
      }
    } catch (error) {
      console.log('error',error)
    }
  };

  useEffect(() => {
    // startTimer();
    // start()
    userStatus();

    if (localStorage.getItem("statusValue") == "Ready") {
      goReady("Ready");
      updateStatus("Ready");
      setShowStatusChannel("Ready");
      localStorage.setItem("statusValue", "Ready");
    }
  }, []);

  // const {
  //   seconds,
  //   minutes,
  //   hours,
  //   start,
  // } = useStopwatch({ autoStart: false });



  const updateStatus = async (value) => {
    const access_token = localStorage.getItem("access_token");
    let data = await JSON.parse(localStorage.getItem("tokenAgent"));
    try {
      if (data) {
        let datas = {
          status: value,
          userId: data.user_id,
        };
        const update = await axios.post(
          BaseUrl + "/users/updateAgentStatus/",
          datas,
          {
            headers: {
              Authorization: "Bearer " + access_token,
              "Content-Type": "application/json",
              tenantId: tenantId,

            },
          }
        );
        console.log('update Status>>>>>>>>>>>>>>>>>', update)
      }

    } catch (error) {
      errorHandel(error,'/users/updateAgentStatus/')
    }
  };

  const navigate = useNavigate();

  const goReady = async (val) => {
    clearTimeout(timerOne);
    clearTimeout(timerTwo);
    setFinishButton(true);
    setReadyButton(false);
    setReadyStatus(true);
    setConnectedStatus(false);
    setHideLogout(false);
    setShowingStatusClass("showStatus");
    setShowingStatus(val);
    localStorage.setItem("timer_status", false);
    localStorage.setItem("timer_connect_hour", 0);
    localStorage.setItem("timer_connect_min", 0);
    localStorage.setItem("timer_connect_sec", 0);
  };

  const goFinish = () => {
    clearTimeout(timerOne);
    clearTimeout(timerTwo);
    setShowStatusChannel("Logged Out");
    updateStatus("Connected");
    localStorage.removeItem("readystatus");
    setHideLogout(true);
    setTimeout(() => {
      setFinishButton(false);
      setReadyButton(true);
      setReadyStatus(false);
      setConnectedStatus(true);
    }, 1000);
  };

  let name = localStorage.getItem("NameDisplay");
  let email = localStorage.getItem("emailDisplay");
  let firstTwoletter;
  if (name) {
    firstTwoletter = name.substring(0, 2).toLocaleUpperCase();
  }

  const logout = async () => {
    const access_token = localStorage.getItem("access_token");
    let data = await JSON.parse(localStorage.getItem("tokenAgent"));
    let userID = data.user_id;
    const id = localStorage.getItem('TenantId');

    try {
      if (data) {
        const update = await axios.post(
          BaseUrl + "/users/logoutnew/" + userID,
          {},
          {
            headers: {
              Authorization: "Bearer " + access_token,
              "Content-Type": "application/json",
              tenantId: tenantId,

            },
          }
        );
        // Clear localStorage items
        console.log("ertyuytdslogout", update.data.success);
        if (update.data.success == true) {


          localStorage.removeItem('AvayaUsername')
          localStorage.removeItem('tab')
          localStorage.removeItem('timer_connect_sec')
          localStorage.removeItem('AvayaPassword')
          localStorage.removeItem('AvayaDomain')
          localStorage.removeItem('client')
          localStorage.removeItem('statusValue')
          localStorage.removeItem('emailDisplay')
          localStorage.removeItem('timer_connect_min')
          localStorage.removeItem('tokenAgent')
          localStorage.removeItem('timer_status')
          localStorage.removeItem('NameDisplay')
          localStorage.removeItem('access_token')
          localStorage.removeItem('timer_connect_hour')


          if (!id) {
            window.location.href = 'error.html';
            console.error('ID not found in local storage');
          } else {
            const loginUrl = `${frontendBaseurl}/?tenantID=${encodeURIComponent(id)}`;
            window.location.href = loginUrl;
          }


          logoutByDomainFirst();
          navigate("/");
        } else {
          toast.warn("Please Close all Active Sessions", {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }
      }
    } catch (error) {
      errorHandel(error,'users/logoutnew/')

      // toast.warn('Server is down please try after sometime', {
      //   position: "top-right",
      //   autoClose: 1000,
      //   hideProgressBar: false,
      //   closeOnClick: true,
      //   pauseOnHover: true,
      //   draggable: true,
      //   progress: undefined,
      //   theme: "light",
      //   });
      console.log(error);
    }
  };

  const loginbyDomainforAgent = () => {
    let agentLoginTerminalId = localStorage.getItem('agentLoginTerminalId')
    let ssToken = localStorage.getItem("ssoToken");
    axios
      .post(
        AvcUrl + `/voice/cct/agent/${agentLoginTerminalId}/login/${agentLoginTerminalId}?status=Ready`,
        {},
        {
          headers: {
            ssoToken: ssToken,
            tenantId: tenantId,

          },
        }
      )
      .then((res) => {
        if ((res.data.status = "OK")) {
        }
      })
      .catch((error) => {
      errorHandel(error,'agent/login/status')
        console.error(err);
      });
  };

  const loginbyDomain = () => {
    let avayauser = localStorage.getItem("AvayaUsername");
    let avayapass = localStorage.getItem("AvayaPassword");
    let avayaDomain = localStorage.getItem("AvayaDomain");
    axios
      .post(
        `${AvcUrl}/voice/cct/login?username=${avayauser}&password=${avayapass}&domain=${avayaDomain}`,
        {},
        {
          headers: {
            tenantId: tenantId,
          }
        }
      )
      .then((res) => {
        if ((res.data.status = "OK")) {
          localStorage.setItem("ssoToken", res.data.data.ssoToken);
          localStorage.setItem("agentLoginTerminalId", res.data.data.agents[0].agentLoginId);
          console.log("resPdf", res.data.data);

          if (res.data.data.agents[0].loggedIn == false) {
            loginbyDomainforAgent();
          } else {
          }
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const logoutByDomainSecond = () => {
    let ssToken = localStorage.getItem("ssoToken");
    axios
      .post(
        AvcUrl + "/voice/cct/logout?username=Agent20",
        {},
        {
          headers: {
            ssoToken: ssToken,
            tenantId: tenantId,

          },
        }
      )
      .then((res) => {
        if ((res.data.status = "OK")) {
          console.log("logout", res);
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  const logoutByDomainFirst = () => {
    let ssToken = localStorage.getItem("ssoToken");
    let agentLoginTerminalId = localStorage.getItem('agentLoginTerminalId')
    axios
      .post(
        AvcUrl + `/voice/cct/agent/${agentLoginTerminalId}/logout/${agentLoginTerminalId}`,
        {},
        {
          headers: {
            ssoToken: ssToken,
            tenantId: tenantId,

          },
        }
      )
      .then((res) => {
        if (res.data.status == "OK") {

          logoutByDomainSecond();

          // localStorage.clear();
        }
      })
      .catch((error) => {
      errorHandel(error,'/cct/agent/logout')

        console.error(err);
      });
  };

  const changeStatusnew = (status) => {
    let ssToken = localStorage.getItem("ssoToken");
    let agentLoginTerminalId = localStorage.getItem('agentLoginTerminalId')
    axios
      .post(
        AvcUrl + `/voice/cct/agent/${agentLoginTerminalId}/status?status=` + status,
        {},
        {
          headers: {
            ssoToken: ssToken,
            tenantId: tenantId,

          },
        }
      )
      .then((res) => {
        if (res.data.status == "OK") {
          console.log("res", res);
        }
      })
      .catch((error) => {
      errorHandel(error,'agent/status')
        console.error(err);
      });
  };

  const showBreaks = () => {
    setRmState(false);
  };

  const showOldScreen = () => {
    setRmState(true);
  };

  const mealUpdate = (item) => {
    goReady(item);
    setShowingStatusClass("showStatusRed");
    updateStatus(item);
    setRmState(true);
  };

  const userStatus = () => {
    const access_token = localStorage.getItem("access_token");
    try {
      axios
        .post(
          BaseUrl + "/userstatus/userstatuslist",
          {},
          {
            headers: {
              Authorization: "Bearer " + access_token,
              "Content-Type": "application/json",
              tenantId: tenantId,

            },
          }
        )
        .then(function (response) {
          console.log("userstatus", response.data.data);
          setUserstatus(response.data.data);
        });
    } catch (error) {
      errorHandel(error,'/userstatus/userstatuslist')

      // toast.warn('Server is down please try after sometime', {
      //   position: "top-right",
      //   autoClose: 1000,
      //   hideProgressBar: false,
      //   closeOnClick: true,
      //   pauseOnHover: true,
      //   draggable: true,
      //   progress: undefined,
      //   theme: "light",
      //   });
    }
  };

  const closeNotify = (msg, id) => {
    let filteredArray = props.contactList1.filter((item) => {
      return item.client_msg !== msg;
    });
    props.setcontactlist1([]);
    props.setcontactlist1(filteredArray);
  };

  return (

    <div className="header">
      <nav className="neo-navbar shadow">
        <div className="neo-nav--left">
          <img
            src={inaipi}
            style={{ height: "auto", width: "3.5rem" }}
            className="nav_logo"
          />
        </div>

        <div>
          <h4
            style={{
              fontWeight: "bold",
              fontSize: "16px",
              fontFamily: "Noto-Sans",
              marginLeft: "9rem",
            }}
          >
            INAIPI UCX
          </h4>
        </div>
        <div className="neo-nav">
          {/* <CustomDialer /> */}
          <div className="neo-badge__navbutton neo-badge__navbutton--active">
            <Sdk/>
          </div>
          <div>
          </div>
          <div className="neo-badge__navbutton">
            <button
              className="neo-badge__navbutton--content neo-btn neo-icon-notifications-on"
              onClick={show_notification}
            ></button>
            <span
              className="neo-badge__icon"
              data-badge={props.contactList1.length}
            ></span>
          </div>
          <div className="neo-badge__navbutton">
            <button className="neo-badge__navbutton--content neo-btn neo-icon-settings"></button>
          </div>
          {showReadyStatus && (
            <div
              className="neo-nav-status neo-nav-status--ready"
              tabIndex="0"
              onClick={show_userprofile}
            >
              <div className="neo-nav-status-info">
                <p className="mx-1 font-weight-bold">
                  {localStorage.getItem("NameDisplay")}
                </p>

                <span className={`neo-label timer-ready ${showingStatusClass}`}>
                  {showingStatus}
                </span>
              </div>
              <figure
                className="neo-avatar"
                data-initials={firstTwoletter}
              ></figure>
            </div>
          )}

          {showConnectedStatus && (
            <div
              className={`neo-nav-status ${localStorage.getItem("statusValue") == "Connected" ||
                  localStorage.getItem("statusValue") == "" ||
                  localStorage.getItem("statusValue") == null ||
                  localStorage.getItem("statusValue") == undefined
                  ? `neo-nav-status--connected`
                  : `neo-nav-status--ready`
                } `}
              tabIndex="0"
              onClick={show_userprofile}
            >
              <div className="neo-nav-status-info ">
                <p className="mx-1 font-weight-bold">
                  {localStorage.getItem("NameDisplay")}
                </p>
                {localStorage.getItem("statusValue") == "Connected" ||
                  localStorage.getItem("statusValue") == null ||
                  localStorage.getItem("statusValue") == undefined ||
                  localStorage.getItem("statusValue") == "" ? (
                  <>
                    <span className="neo-label neo-label--connected timer-connected">
                      Connected
                      {/* <span id="hours">{hours >=10 ? hours:'0'+hours}:</span>
                      <span id="mins">{minutes >=10 ? minutes: '0'+ minutes}:</span>
                      <span id="seconds">{seconds >=10? seconds:'0'+seconds}</span> */}
                    </span>
                  </>
                ) : (
                  <span
                    className={`${localStorage.getItem(
                      "statusValue" == "Ready"
                    )}`}
                  >
                    <span
                      className={`${localStorage.getItem("statusValue") == "Ready" ||
                          localStorage.getItem("statusValue") == "Connected"
                          ? "neo-label neo-label--ready"
                          : "neo-label neo-label--not-ready"
                        }`}
                    >
                      {localStorage.getItem("statusValue")}
                    </span>
                  </span>
                )}
              </div>
              <figure
                className="neo-avatar"
                data-initials={firstTwoletter}
              ></figure>
            </div>
          )}
        </div>
      </nav>

      {/* Notification Contant */}
      {showNotification && (
        <div className="main-notify-cont">
          <div className="content">
            <span className="head">Notifications</span>
            <span
              className="neo-icon-close"
              onClick={close_notification}
              aria-label="close notification"
            ></span>
            {!props.contactList1 && (
              <div className="notify-body">
                <span
                  className="neo-icon-info"
                  aria-label="info notification"
                ></span>
                <span>No notifications to display</span>
              </div>
            )}

            <div className="notify-content-data">
              {props.contactList1 &&
                props.contactList1
                  .slice(0)
                  .reverse()
                  .map((data) => {
                    return (
                      <div className="notifications-div">
                        <span
                          className="notify-close"
                          onClick={() => {
                            closeNotify(data.client_msg, data.from_id);
                          }}
                        >
                          X
                        </span>
                        <div className="noti-username">{data.username}</div>
                        <div className="noti-msg">
                          <span
                            className="notify-message"
                            title={data.client_msg}
                          >
                            {data.client_msg}
                          </span>{" "}
                          <span className="notify-duration">
                            {data.msg_time}
                          </span>
                        </div>
                      </div>
                    );
                  })}
            </div>
          </div>
        </div>
      )}

      {showUserProfile && (
        <div className="main-notify-cont">
          {/* <div className="content"> */}
          {rmState && (
            <div className="content">
              <span className="head">Agent State</span>
              <span className="head_agent-details">Agent Id - {email}</span>
              <span
                className="neo-icon-close user-close"
                onClick={close_userprofile}
                aria-label="close notification"
              ></span>

              <div className="user-body">
                {showReadyButton && (
                  <button
                    className="btn btn-success work-start"
                    style={{ fontFamily: "Noto-Sans" }}
                    onClick={() => {
                      goReady("Ready");
                      updateStatus("Ready");
                      loginbyDomain();
                      setShowStatusChannel("Ready");
                      localStorage.setItem("statusValue", "Ready");
                    }}
                  >
                    {" "}
                    <span
                      className="neo-icon-work-start"
                      aria-label="start work"
                    ></span>{" "}
                    Go Ready
                  </button>
                )}

                {showFinishButton && (
                  <button
                    className="btn btn-primary"
                    style={{ fontFamily: "Noto-Sans" }}
                    onClick={() => {
                      goFinish();
                      localStorage.setItem("statusValue", "Connected");
                    }}
                  >
                    {" "}
                    <span
                      className="neo-icon-work-end"
                      aria-label="end work"
                    ></span>{" "}
                    Finish Work
                  </button>
                )}

                {showFinishButton && (
                  <div className="status">
                    <div className="status-head">Status</div>
                    <div className="go-not-ready" onClick={showBreaks}>
                      <span
                        className="neo-icon-do-not-disturb"
                        aria-label="end work"
                      ></span>
                      Go Not Ready{" "}
                      <span
                        className="neo-icon-chevron-right"
                        aria-label="end work"
                      ></span>
                    </div>
                    <div
                      className="go-ready"
                      style={{ fontFamily: "Noto-Sans" }}
                      onClick={() => {
                        goReady("Ready");
                        updateStatus("Ready");
                        setShowStatusChannel("Ready");
                        changeStatusnew("Ready");
                        localStorage.setItem("statusValue", 'Ready')

                      }}
                    >
                      <span
                        className="neo-icon-go-ready"
                        aria-label="end work"
                      ></span>
                      Go Ready
                    </div>

                    <div
                      className="go-ready"
                      style={{ fontFamily: "Noto-Sans" }}
                      onClick={() => {
                        mealUpdate("Not Ready");
                        localStorage.setItem("statusValue", "Not Ready");
                        changeStatusnew("Not Ready");
                      }}
                    >
                      <span
                        className="neo-icon-go-ready"
                        aria-label="end work"
                      ></span>
                      Not Ready
                    </div>
                  </div>
                )}

                <div className="status">
                  <div className="status-head">Channel</div>
                  <div className="go-not-ready">Chat - {showStatusChannel}</div>
                  <div className="go-ready">Voice - {showStatusChannel}</div>
                </div>
              </div>
              <a>
                {" "}
                {hideLogout && (
                  <div className="sign-out" onClick={logout}>
                    <span
                      className="neo-icon-exit-left"
                      aria-label="sign out"
                    ></span>
                    <span
                      className="signout-text"
                      style={{ fontFamily: "Noto-Sans" }}
                    >
                      Sign Out
                    </span>
                  </div>
                )}
                {!hideLogout && (
                  <div className="sign-out-1">
                    <span
                      className="neo-icon-exit-left"
                      aria-label="sign out"
                    ></span>
                    <span
                      className="signout-text"
                      style={{ fontFamily: "Noto-Sans" }}
                    >
                      Sign Out
                    </span>
                  </div>
                )}
              </a>
            </div>
          )}
          {!rmState && (
            <div className="contents">
              <div className="status">
                <div className="reason-head">
                  {" "}
                  <span
                    className="neo-icon-chevron-left"
                    aria-label="end work"
                    onClick={showOldScreen}
                  ></span>
                  Reason Codes
                </div>
                <div className="content1">
                  {userstatus.map((item) => (
                    <div
                      className="go-not-ready"
                      onClick={() => {
                        mealUpdate(item.statusName);
                        localStorage.setItem("statusValue", item.statusName);
                      }}
                    >
                      {item.statusName}
                    </div>
                  ))}
                </div>
              </div>
            </div>
          )}
        </div>
        // </div>
      )}
    </div>
  );
};

export default connect(mapStateToProps, {
  setcontactlist,
  setcontactlist1,
})(NavBarHeader);
