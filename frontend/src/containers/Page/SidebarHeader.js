import "../App.scss";
import "../stylenew1.css";
import "../style.css";
import "../../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../../assets/dist/css/neo/neo.min.css";
import axios from "axios";
import React, { Component, useEffect, useState } from "react";
import { CTooltip } from "@coreui/react";
import { SiDgraph } from 'react-icons/si'
import { BaseUrl } from "./Constants/BaseUrl";
import { Link, NavLink } from "react-router-dom";
import { BiCalendar, BiChat, BiFile, BiMailSend } from "react-icons/bi";
import { TbReportSearch } from "react-icons/tb";
import { MdOutlineDashboardCustomize } from "react-icons/md";
import { BsFillFileBarGraphFill } from "react-icons/bs";



const SidebarHeader = (props) => {
  const [isActive, setIsActive] = useState(false);


  const handleClick = (event) => {
    setIsActive((current) => !current);
  };
  const logout = async () => {
    let data = await JSON.parse(localStorage.getItem("tokenAgent"));

    if (data) {
      const update = await axios.post(
        BaseUrl + "/users/logout/" + data.user_id
      );
      // Clear localStorage items
      localStorage.clear();
      window.location.reload();
    }
  };

  let agentRole = JSON.parse(localStorage.getItem("tokenAgent"));
  let permission = JSON.parse(localStorage.getItem("permission"));


  return (
    <div className="sidebar">
      {agentRole.role == "Supervisor" ? (
        <div className="chat-main">

          {permission.find((element) => element.moduleName === "Dashboard") ?
            <CTooltip content="Dashboard" placement="right" arrow>
              <Link
                to="/dashboard"
                id="dashboard_chat_supervisor"
                style={{ marginTop: "10px" }}
                onClick={() => localStorage.setItem("tab", "dashboard")}
              >
                <button
                  className={
                    localStorage.getItem("tab") == "dashboard" ||
                      localStorage.getItem("tab") == null ||
                      localStorage.getItem("tab") == undefined ||
                      localStorage.getItem("tab") == ""
                      ? "tablinks_active"
                      : "tablinks"
                  }
                  onClick={() => {
                    localStorage.setItem("tab", "chatagent");
                    permission.find((element, index) => {
                      if (element.moduleName == 'Dashboard') {
                        localStorage.setItem('indexOf', index)
                      }
                    })
                  }}
                >
                  <MdOutlineDashboardCustomize
                    size={20}
                    color="white"
                    style={{ marginTop: "-20px" }}
                  />

                  <br></br>
                </button>
              </Link>
            </CTooltip> : ''

          }

          {permission.find((element) => element.moduleName === "Chat") ?

            <CTooltip content="Chat" placement="right">
              <Link
                to="/chat"
                id="chat_chat_supervisor"
                style={{ padding: " 0px" }}
                onClick={() => localStorage.setItem("tab", "chat")}
              >
                <button
                  className={
                    localStorage.getItem("tab") == "chat"
                      ? "tablinks_active"
                      : "tablinks"
                  }
                  onClick={() => {
                    localStorage.setItem("tab", "chatagent");
                    permission.find((element, index) => {
                      if (element.moduleName == 'Chat') {
                        localStorage.setItem('indexOf', index)

                      }
                    })
                  }}
                >
                  <BiChat
                    size={20}
                    color="white"
                    style={{ marginTop: "-20px" }}
                  />

                  <br></br>
                </button>
              </Link>
            </CTooltip> : ''

          }

          {permission.find((element) => element.moduleName === "Calender") ?

            <CTooltip content="Calender" placement="right">
              <Link
                to="/Calendars"
                id="agentreports_chat_supervisor"
                style={{ padding: " 0px" }}
                onClick={() => {
                  localStorage.setItem("tab", "Calender");
                  permission.find((element, index) => {
                    if (element.moduleName == 'Calender') {
                      localStorage.setItem('indexOf', index)

                    }
                  })

                }
                }
              >
                <button
                  className={
                    localStorage.getItem("tab") == "Calender"
                      ? "tablinks_active"
                      : "tablinks"
                  }
                >
                  <BiCalendar
                    size={20}
                    color="white"
                    style={{ marginTop: "-20px" }}
                  />

                  <br></br>
                </button>
              </Link>
            </CTooltip> : ''
          }


          {permission.find((element) => element.moduleName === "Sessionreport") ?

            <CTooltip content="Session Reports" placement="right">
              <Link
                to="/sessionreports"
                id="sessionreports_chat_supervisor"
                style={{ padding: " 0px" }}
                onClick={() => {
                  localStorage.setItem("tab", "sessionreports");
                  permission.find((element, index) => {
                    if (element.moduleName == 'Sessionreport') {
                      localStorage.setItem('indexOf', index)

                    }
                  })
                }}
              >
                <button
                  className={
                    localStorage.getItem("tab") == "sessionreports"
                      ? "tablinks_active"
                      : "tablinks"
                  }
                >
                  <BiFile
                    size={20}
                    color="white"
                    style={{ marginTop: "-20px" }}
                  />

                  <br></br>
                </button>
              </Link>
            </CTooltip> : ''
          }


          {permission.find((element) => element.moduleName === "Agentreport") ?

            <CTooltip content="Agent Reports" placement="right">
              <Link
                to="/agentreports"
                id="agentreports_chat_supervisor"
                style={{ padding: " 0px" }}
                onClick={() => {
                  localStorage.setItem("tab", "agentreports");
                  permission.find((element, index) => {
                    if (element.moduleName == 'Agentreport') {
                      localStorage.setItem('indexOf', index)

                    }
                  })
                }}
              >
                <button
                  className={
                    localStorage.getItem("tab") == "agentreports"
                      ? "tablinks_active"
                      : "tablinks"
                  }
                >
                  <TbReportSearch
                    size={20}
                    color="white"
                    style={{ marginTop: "-20px" }}
                  />

                  <br></br>
                </button>
              </Link>
            </CTooltip> : ''

          }

          <CTooltip content="Mail" placement="right">
            <Link
              to="/mail"
              id="sessionreports_chat_supervisor"
              style={{ padding: " 0px" }}
              onClick={() => localStorage.setItem("tab", "email")}


            >
              <button
                className={
                  localStorage.getItem("tab") == "email"
                    ? "tablinks_active"
                    : "tablinks"
                }
              >
                <BiMailSend
                  size={20}
                  color="white"
                  style={{ marginTop: "-20px" }}
                />

                <br></br>
              </button>
            </Link>
          </CTooltip>

          <CTooltip content="Power Bi" placement="right">
            <Link
              to="/powerbi"
              id="sessionreports_chat_supervisor"
              style={{ padding: " 0px" }}
              onClick={() => localStorage.setItem("tab", "powerBi")}


            >
              <button
                className={
                  localStorage.getItem("tab") == "powerBi"
                    ? "tablinks_active"
                    : "tablinks"
                }
              >
                <BsFillFileBarGraphFill
                  size={20}
                  color="white"
                  style={{ marginTop: "-20px" }}
                />


                <br></br>
              </button>
            </Link>
          </CTooltip>

          <CTooltip content="Power Bi" placement="right">
            <Link
              to="/PowerbiDashboard"
              id="sessionreports_chat_supervisor"
              style={{ padding: " 0px" }}
              onClick={() => localStorage.setItem("tab", "PowerbiDashboard")}


            >
              <button
                className={
                  localStorage.getItem("tab") == "PowerbiDashboard"
                    ? "tablinks_active"
                    : "tablinks"
                }
              >
                <BsFillFileBarGraphFill
                  size={20}
                  color="white"
                  style={{ marginTop: "-20px" }}
                />


                <br></br>
              </button>
            </Link>
          </CTooltip>


          

        </div>
      ) : (

        <div className="chat-main">


          {permission.find((element) => element.moduleName === "Dashboard") ?

            <CTooltip content="Dashboard" placement="right">
              <Link
                to="/dashboard"
                id="dashboard_agent_chat"
                style={{ marginTop: "10px" }}
                onClick={() => {
                  localStorage.setItem("tab", "dashboardagent");
                  permission.find((element, index) => {
                    if (element.moduleName == 'Dashboard')
                      localStorage.setItem('indexOf', index)
                  })
                }}


              >
                <button
                  className={
                    localStorage.getItem("tab") == "dashboardagent" ||
                      localStorage.getItem("tab") == null ||
                      localStorage.getItem("tab") == undefined ||
                      localStorage.getItem("tab") == ""
                      ? "tablinks_active"
                      : "tablinks"
                  }


                >
                  <MdOutlineDashboardCustomize
                    size={20}
                    color="white"
                    style={{ marginTop: "-20px" }}
                  />

                  <br></br>
                </button>
              </Link>
            </CTooltip> : ''
          }


          {permission.find((element) => element.moduleName === "Chat") ?

            <CTooltip content="Chat" placement="right">
              <Link
                to="/chat"
                id="chat_chat_agent"
                style={{ padding: " 0px" }}
                onClick={() => {
                  localStorage.setItem("tab", "chatagent");
                  permission.find((element, index) => {
                    if (element.moduleName == 'Chat') {
                      localStorage.setItem('indexOf', index)

                    }

                  })

                }}
              >
                <button
                  className={
                    localStorage.getItem("tab") == "chatagent"
                      ? "tablinks_active"
                      : "tablinks"
                  }

                >
                  <BiChat
                    size={20}
                    color="white"
                    style={{ marginTop: "-20px" }}
                  />
                  <br></br>
                </button>
              </Link>
            </CTooltip> : ''

          }


          {permission.find((element) => element.moduleName === "Calender") ?

            <CTooltip content="Calender" placement="right">
              <Link
                to="/Calendars"
                id="agentreports_chat_supervisor"
                style={{ padding: " 0px" }}
                onClick={() => {
                  localStorage.setItem("tab", "Calender");

                  permission.find((element, index) => {
                    if (element.moduleName == 'Calender') {
                      localStorage.setItem('indexOf', index)

                    }

                  })


                }}
              >
                <button
                  className={
                    localStorage.getItem("tab") == "Calender"
                      ? "tablinks_active"
                      : "tablinks"
                  }
                >
                  <BiCalendar
                    size={20}
                    color="white"
                    style={{ marginTop: "-20px" }}
                  />

                  <br></br>
                </button>
              </Link>
            </CTooltip> : ''
          }


          {/* {permission.find((element) => element.moduleName === "Session Reports") ?

            <CTooltip content="Session Reports" placement="right">
              <Link
                to="/sessionreports"
                id="sessionreports_chat_agent"
                style={{ padding: " 0px" }}
                onClick={() => {
                  localStorage.setItem("tab", "sessionreportsagent");

                  permission.find((element, index) => {
                    if (element.moduleName == 'Sessionreport') {
                      localStorage.setItem('indexOf', index)

                    }

                  })
                }}
              >
                <button
                  className={
                    localStorage.getItem("tab") == "sessionreportsagent"
                      ? "tablinks_active"
                      : "tablinks"
                  }
                >
                  <BiFile
                    size={20}
                    color="white"
                    style={{ marginTop: "-20px" }}
                  />

                  <br></br>
                </button>
              </Link>
            </CTooltip> : ''
          } */}



          {permission.find((element) => element.moduleName === "Sessionreport") ?

            <CTooltip content="Session Reports" placement="right">
              <Link
                to="/sessionreports"
                id="sessionreports_chat_supervisor"
                style={{ padding: " 0px" }}
                onClick={() => {
                  localStorage.setItem("tab", "sessionreports");

                  permission.find((element, index) => {
                    if (element.moduleName == 'Agentreport') {
                      localStorage.setItem('indexOf', index)

                    }

                  })
                }}
              >
                <button
                  className={
                    localStorage.getItem("tab") == "sessionreports"
                      ? "tablinks_active"
                      : "tablinks"
                  }
                >
                  <BiFile
                    size={20}
                    color="white"
                    style={{ marginTop: "-20px" }}
                  />

                  <br></br>
                </button>
              </Link>
            </CTooltip> : ''
          }

          <CTooltip content="Mail" placement="right">
            <Link
              to="/mail"
              id="sessionreports_chat_supervisor"
              style={{ padding: " 0px" }}
              onClick={() => localStorage.setItem("tab", "email")}


            >
              <button
                className={
                  localStorage.getItem("tab") == "email"
                    ? "tablinks_active"
                    : "tablinks"
                }
              >
                <BiMailSend
                  size={20}
                  color="white"
                  style={{ marginTop: "-20px" }}
                />

                <br></br>
              </button>
            </Link>
          </CTooltip>


      
        </div>
      )}
    </div>
  );
};

export default SidebarHeader;
