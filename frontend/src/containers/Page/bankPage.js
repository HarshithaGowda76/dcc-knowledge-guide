import "../../assets/assets_clientpage/css/style.css";
import "../../assets/assets_clientpage/plugins/bootstrap/css/bootstrap.min.css";
import "../../assets/assets_clientpage/plugins/icofont/icofont.min.css";
import mybank_logo from "../../assets/assets_clientpage/images/mybank_logo.png";
import ChatForClient from "../../components/clientChat";

function Bank() {
  return (
    <div id="top" className="client-web-body">
      <header className="header-btm ">
        <div className="header-top-bar">
          <div className="container">
            <div className="row align-items-center">
              <div className="col-lg-6">
                <ul className="top-bar-info list-inline-item pl-0 mb-0">
                  <li className="list-inline-item">
                    <a href="/#">
                      <i className="icofont-support-faq mr-2"></i>
                      support@mybank.com
                    </a>
                  </li>
                  <li className="list-inline-item">
                    <i className="icofont-location-pin mr-2"></i>Madipakkam,
                    Chennai, IND{" "}
                  </li>
                </ul>
              </div>
              <div className="col-lg-6">
                <div className="text-lg-right top-right-bar mt-2 mt-lg-0">
                  <a href="/#">
                    <span>Call Now : </span>
                    <span className="h4">(+91) 9600062925</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <nav className="navbar navbar-expand-lg navigation" id="navbar">
          <div className="container">
            <a className="navbar-brand" href="/#">
              <img
                src={mybank_logo}
                alt=""
                height="50%"
                width="50%"
                className="img-fluid"
              ></img>
            </a>

            <button
              className="navbar-toggler collapsed"
              type="button"
              data-toggle="collapse"
              data-target="/#"
              aria-controls="navbarmain"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="icofont-navigation-menu"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarmain">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item active">
                  <a className="nav-link" href="/#">
                    Home
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/#">
                    About
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/#">
                    Services
                  </a>
                </li>

                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="/#"
                    id="dropdown02"
                    data-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Loan Products{" "}
                  </a>
                  <ul className="dropdown-menu" aria-labelledby="dropdown02">
                    <li>
                      <a className="dropdown-item" href="/#">
                        Loan Department
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="/#">
                        Loan Eligibility
                      </a>
                    </li>
                  </ul>
                </li>

                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="/#"
                    id="dropdown03"
                    data-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Deposits{" "}
                  </a>
                  <ul className="dropdown-menu" aria-labelledby="dropdown03">
                    <li>
                      <a className="dropdown-item" href="/#">
                        Admin Deposits
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="/#">
                        Loan Deposits
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="/#">
                        Saving Deposits
                      </a>
                    </li>
                  </ul>
                </li>

                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="/#"
                    id="dropdown05"
                    data-toggle="dropdown"
                    aria-expanded="false"
                  >
                    Investors{" "}
                  </a>
                  <ul className="dropdown-menu" aria-labelledby="dropdown05">
                    <li>
                      <a className="dropdown-item" href="/#">
                        Indian Investors
                      </a>
                    </li>

                    <li>
                      <a className="dropdown-item" href="/#">
                        NRI Investors
                      </a>
                    </li>
                  </ul>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/#">
                    Contact
                  </a>
                </li>
              </ul>
              <div className="btn-container ">
                <a href="/#" className="btn-nav btn-main-2 btn-icon">
                  <i className="icofont-ui-user ml-2  "> </i>Login
                </a>
              </div>
            </div>
          </div>
        </nav>
      </header>

      {/* <!-- Slider Start --> */}
      <section className="banner">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-md-12 col-xl-7">
              <div className="block">
                <div className="divider mb-3"></div>
                <span className="text-uppercase text-sm letter-spacing ">
                  MY BANK
                </span>
                {/* <h1 className="mb-0 mt-3">Fulfill Your dream with an</h1>
                  <h1 className="mb-3 mt-0">MY BANK home loan</h1> */}
                <h1 className="mb-0 mt-3">Welcome joy and prosperity</h1>
                <h1 className="mb-3 mt-0">in your dream home</h1>

                <p className="mb-4 pr-5">Plan Your dream home</p>
                <div className="btn-container ">
                  <a
                    href="/#"
                    className="btn-main btn-main-2 btn-icon btn-round-full"
                  >
                    Apply Now <i className="icofont-simple-right ml-2  "></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="section service gray-bg">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-7 text-center">
              <div className="section-title">
                <h2>Plan Your home</h2>
                <div className="divider mx-auto my-4"></div>
                <p>Lets help you plane the finance for your dream home</p>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-4 col-md-6 col-sm-6">
              <div className="service-item mb-4">
                <div className="icon d-flex align-items-center">
                  <i className="icofont-chart-line-alt text-lg"></i>
                  <h4 className="mt-3 mb-3">EMI</h4>
                </div>

                <div className="content">
                  <p className="mb-4">Calculte Your monthly EMI</p>
                  <br />
                </div>
                <div className="content">
                  <a href="/#">
                    {" "}
                    <i className="icofont-arrow-right">Calculate EMI</i>
                  </a>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 col-sm-6">
              <div className="service-item mb-4">
                <div className="icon d-flex align-items-center">
                  <i className="icofont-handshake-deal text-lg"></i>
                  <h4 className="mt-3 mb-3">Eligiblity</h4>
                </div>
                <div className="content">
                  <p className="mb-4">
                    Known your eligibility and maximum loan you can avail
                  </p>
                </div>
                <div className="content">
                  <a href="/#">
                    {" "}
                    <i className="icofont-arrow-right">Check Eligibility</i>
                  </a>
                </div>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 col-sm-6">
              <div className="service-item mb-4">
                <div className="icon d-flex align-items-center">
                  <i className="icofont-chart-histogram text-lg"></i>
                  <h4 className="mt-3 mb-3">Plane</h4>
                </div>
                <div className="content">
                  <p className="mb-4">Calculate your home buying budget</p>
                  <br />
                </div>
                <div className="content">
                  <a href="/#">
                    {" "}
                    <i className="icofont-arrow-right">Plan Your Loan</i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <ChatForClient />
      {/* <!-- footer Start --> */}
      <footer className="footer">
        <div className="container ">
          <div className="row">
            <div className="col-lg-3 mr-auto col-sm-6">
              <div className="widget mb-5 mb-lg-0">
                <h4 className="text-capitalize mb-3">Loans</h4>
                <div className="divider mb-4"></div>

                <ul className="list-unstyled footer-menu lh-35">
                  <li>
                    <a href="/#">Home Loans </a>
                  </li>
                  <li>
                    <a href="/#">Plot Loans</a>
                  </li>
                  <li>
                    <a href="/#">House Renovation Loans</a>
                  </li>
                  <li>
                    <a href="/#">Loan Against Propertyr</a>
                  </li>
                </ul>
              </div>
            </div>

            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="widget mb-5 mb-lg-0">
                <h4 className="text-capitalize mb-3">Calculators</h4>
                <div className="divider mb-4"></div>

                <ul className="list-unstyled footer-menu lh-35">
                  <li>
                    <a href="/#">Home Loan Interest Rates </a>
                  </li>
                  <li>
                    <a href="/#">Home Loan EMI Calculator</a>
                  </li>
                  <li>
                    <a href="/#">Home Loan Eligibility Calculator</a>
                  </li>
                  <li>
                    <a href="/#">Home Loan Balance Transfer</a>
                  </li>
                </ul>
              </div>
            </div>

            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="widget mb-5 mb-lg-0">
                <h4 className="text-capitalize mb-3">Contact Us</h4>
                <div className="divider mb-4"></div>

                <ul className="list-unstyled footer-menu lh-35">
                  <li>
                    <a href="/#">Service Request /Queries</a>
                  </li>
                  <li>
                    <a href="/#">Helpline Numbers</a>
                  </li>
                  <li>
                    <a href="/#">Locate Us </a>
                  </li>
                </ul>
              </div>
            </div>

            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="widget widget-contact mb-5 mb-lg-0">
                <h4 className="text-capitalize mb-3">Get in Touch</h4>
                <div className="divider mb-4"></div>

                <div className="footer-contact-block mb-4">
                  <div className="icon d-flex align-items-center">
                    <i className="icofont-email mr-3"></i>
                    <span className="h6 mb-0">Support Available for 24/7</span>
                  </div>
                  <h4 className="mt-2">
                    <a href="/#">Support@mybank.com</a>
                  </h4>
                </div>

                <div className="footer-contact-block">
                  <div className="icon d-flex align-items-center">
                    <i className="icofont-support mr-3"></i>
                    <span className="h6 mb-0">Mon to Fri : 09:30 - 18:00</span>
                  </div>
                  <h4 className="mt-2">
                    <a href="/#">+91-9289200017</a>
                  </h4>
                </div>
              </div>
            </div>
          </div>

          <div className="footer-btm py-4 mt-5">
            <div className="row align-items-center justify-content-between">
              <div className="col-lg-6">
                <div className="copyright">
                  &copy; Copyright Reserved to{" "}
                  <span className="text-color">MyBank Ltd.</span> All Rights
                  Reserved
                </div>
              </div>
              <div className="col-lg-6">
                <div className="subscribe-form text-lg-right mt-5 mt-lg-0">
                  <ul className="list-inline footer-socials mt-4">
                    <li className="list-inline-item">
                      <a href="/#">
                        <i className="icofont-facebook"></i>
                      </a>
                    </li>
                    <li className="list-inline-item">
                      <a href="/#">
                        <i className="icofont-twitter"></i>
                      </a>
                    </li>
                    <li className="list-inline-item">
                      <a href="/#">
                        <i className="icofont-linkedin"></i>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-lg-4">
                <a className="backtop js-scroll-trigger">
                  <i className="icofont-long-arrow-up"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Bank;
