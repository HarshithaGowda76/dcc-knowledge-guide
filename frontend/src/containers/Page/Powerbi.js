import React, { useEffect, useRef } from "react";
import NavBarHeader from "./NavBarHeader";
import SidebarHeader from "./SidebarHeader";
import { BaseUrl } from "./Constants/BaseUrl";
import * as pbi from "powerbi-client";
import axios from "axios";
import '../App.scss'
// const Powerbi = () => {
//   return (
//     <>
//       <NavBarHeader />
//       <SidebarHeader />

//       <div className="d-flex justify-content-center my-2 h-100 w-100 m-0">
//         <iframe
//           src="https://app.powerbi.com/reportEmbed?reportId=e5bf4639-9d88-4f24-b372-137a38b3096c&groupId=6168414c-ebe8-4249-863f-f77dec44f59a&w=2&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly9XQUJJLUlORElBLUNFTlRSQUwtQS1QUklNQVJZLXJlZGlyZWN0LmFuYWx5c2lzLndpbmRvd3MubmV0IiwiZW1iZWRGZWF0dXJlcyI6eyJtb2Rlcm5FbWJlZCI6dHJ1ZSwidXNhZ2VNZXRyaWNzVk5leHQiOnRydWV9fQ%3d%3d"
//           width="100%"
//           height="90%"
//         ></iframe>
//       </div>
//     </>
//   );
// };

const Powerbi = () => {
  const reportContainerRef = useRef(null);


  const fetchEmbedData = async () => {
    const access_token = localStorage.getItem("access_token");
    const tenantId = localStorage.getItem("TenantId");
    try {


      const response = await axios.get(
        BaseUrl + "/powerBi/getEmbedToken",
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,

          },

        }

      );


 console.log("<<<<<response>>>>>",response)
  const embedData = response
  console.log("embedData", embedData.data);


  const models = pbi.models;

  const reportLoadConfig = {
    type: "report",
    tokenType: models.TokenType.Embed,
    accessToken: embedData.data.accessToken,
    embedUrl: embedData.data.embedUrl[0].embedUrl,

  };
console.log("reportconfig",reportLoadConfig)
  const report = window.powerbi.embed(
    reportContainerRef.current,
    reportLoadConfig
  );

  report.off("loaded");
  report.on("loaded", () => {
    console.log("Report load successful");
  });

  report.off("rendered");

  report.on("rendered", () => {
    console.log("Report render successful");
  });

  report.off("error");

  report.on("error", (event) => {
    const errorMsg = event.detail;
    return;
  });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchEmbedData();
  }, []);

  return (
    <>
      {/* <NavBarHeader /> */}
      <SidebarHeader />
      <div ref={reportContainerRef} id="report-container" />
    </>
  );
  // return <div ref={reportContainerRef} id="report-container" />;
};

export default Powerbi;
