import React, { useState } from "react";
import { BiSend } from "react-icons/bi";
import { RiAttachment2 } from "react-icons/ri";
import styled from "styled-components";

export default function ChatInput({ handleSendMsg,handleTyping }) {
  const [msg, setMsg] = useState("");


  const sendChat = (event) => {
    event.preventDefault();
    if (msg.length > 0) {
      handleSendMsg(msg);
      setMsg("");
    }
  };

  const handleTyping1=async()=>{
    handleTyping();
  }

  return (
    <Container >
      <div className="button-container" >
        <form className="input-container" onSubmit={(event) => sendChat(event)}>
          <input
            type="text"
            autoFocus
            placeholder="Enter your message"
            onChange={(e) => {setMsg(e.target.value);handleTyping1()}}
            value={msg}
          />
          <button className="attachment-icon">
            <RiAttachment2 style={{ fontSize: "1.5rem" }} />
          </button>
          <button className="send-icon" type="submit">
            <BiSend style={{ color: " #00498f", fontSize: "1.5rem" }} />
          </button>
        </form>
      </div>
    </Container>
  );
}

const Container = styled.div`
  display: grid;
  align-items: center;
  grid-template-columns: 100%;
  background-color: white;

  padding: 0 1rem;
  @media screen and (min-width: 720px) and (max-width: 1080px) {
    padding: 0 1rem;
    gap: 1rem;
  }
  .button-container {
    display: flex;
    /* align-items: center; */
    color: black;
    gap: 1rem;
    /* width: 800px */
    /* .emoji {
      position: relative;
      svg {
        font-size: 1.5rem;
        color: #ffff00c8;
        cursor: pointer;
      }
      .emoji-picker-react {
        position: absolute;
        top: -350px;
        background-color: #080420;
        box-shadow: 0 5px 10px #004;
        border-color: #004;
        .emoji-scroll-wrapper::-webkit-scrollbar {
          background-color: #004;
          width: 5px;
          &-thumb {
            background-color: #black;
          }
        }
        .emoji-categories {
          button {
            filter: contrast(0);
          }
        }
        .emoji-search {
          background-color: transparent;
          border-color: #9a86f3;
        }
        .emoji-group:before {
          background-color: #080420;
        }
      }
    } */
    /* } */
    .input-container {
      width: 90%;
      border-radius: 0.5rem;
      display: flex;
      align-items: flex-end;
      gap: 0.5rem;
      background-color: #fff;

      input {
        width: 90%;
        height: 30px;
        background-color: transparent;
        color: black;
        padding-left: 1rem;
        font-size: 0.8rem;
        border: 1px solid #cfd2cf;
        border-radius: 4px;
        &::selection {
          background-color: #004;
        }
        &:focus {
          outline: none;
        }
      }
      .attachment-icon {
        background-color: #00498f;
        color: white;
        border-radius: 8px;
        border: none;
      }
      .send-icon {
        /* margin-top: 1rem; */
        /* padding: 0.3rem 2rem;
      border-radius: 2rem;
      display: flex;
      justify-content: center;
      align-items: center; */
        /* background-color: #004; */
        background-color: white;
        border: none;
        @media screen and (min-width: 720px) and (max-width: 1080px) {
          padding: 0.3rem 1rem;
          svg {
            font-size: 1rem;
          }
        }
        /* svg {
          font-size: 2rem;
          color: white;
        } */
      }
    }
  }
`;
