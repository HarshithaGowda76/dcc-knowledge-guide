import "./App.scss";
import "./stylenew1.css";
import "./style.css";
import "../assets/dist/css/neo/neo.min.css";
import React, { Component } from "react";
import SpaceContent from "./SpaceContent";
import SidebarHeader from "../containers/Page/SidebarHeader";
import NavBarHeader from "./Page/NavBarHeader";
import Contacts from "../components/chatContact";
import Sdk from "../DialerComponent/Sdk";



export class App extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  state = {
    callingNumber: "",
  };

  displayErrorAlert = (event) => {
    this.setState({
      showErrorAlert: true,
      errorMessage: event.message,
      errorData: JSON.stringify(event.data, null, 2),
    });
  };

  
  render() {
    return (
      <div style={{height:'100vh'}}>
        <div>
          <NavBarHeader />
          <SidebarHeader />
          
        </div>
        <div className="container-fluid main-content p-0">
          <div className="chat-content">
            <div className="row">
              <div
                className="col pr-0"
                style={{ maxWidth: " 280px", padding: "0 0 0 3px" }}
              >
                <div className="sidebar-content">
                  <div className="chatlist-main">
                    <div id="chat" className="tabcontent">
                    {/* Sidebar contact list */}
                      <Contacts />   
                    </div>
                  </div>
                </div>
              </div>
              <div className="col p-0 m-0">
              <div>
                  <SpaceContent />
              </div>                
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App
 

