import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import ChatInput from "./ChatInput";
import { v4 as uuidv4 } from "uuid";
import axios from "axios";
import moment from "moment";
import { BaseUrl } from "./Page/Constants/BaseUrl";
export default function ChatContainer({ currentChat, socket }) {
  const [messages, setMessages] = useState([]);
  const scrollRef = useRef();
  const [arrivalMessage, setArrivalMessage] = useState(null);
  const [AgentTyping, setAgentTyping] = useState(false);
  const [senderId, setSenderId] = useState(undefined);

  useEffect(() => {
    async function getData() {
      const data = await JSON.parse(localStorage.getItem("token"));

      var axios = require("axios");
      const Data = await axios.post(BaseUrl + `/users/getId/${data.email}`);

      setSenderId(Data.data.user.id);

      const response = await axios.post(BaseUrl + "/message/getMessage", {
        from: Data.data.user.id,
        to: currentChat.id,
        send: Data.data.user.username,
        receive: currentChat.username,
        messageFrom: "fromClient",
      });
      setMessages(response.data);
    }
    getData();
  }, [currentChat]);

  useEffect(() => {
    const getCurrentChat = async () => {
      if (currentChat) {
        // await JSON.parse(localStorage.getItem("token")).id;
        const data = await JSON.parse(localStorage.getItem("token"));
        var axios = require("axios");
        const Data = await axios.post(BaseUrl + `/users/getId/${data.email}`);
      }
    };
    getCurrentChat();
  }, [currentChat]);
  var seconds = 0;
  var mins = 0;
  var hours = 0;

  let timerHour = localStorage.getItem("timer-hr");
  let timerMin = localStorage.getItem("timer-mins");
  let timerSec = localStorage.getItem("timer-seconds");

  if (timerHour) {
    hours = timerHour;
  }
  if (timerMin) {
    mins = timerMin;
  }
  if (timerSec) {
    seconds = timerSec;
  }

  const startTimer = async () => {
    var data = await JSON.parse(localStorage.getItem("token"));
    var axios = require("axios");
    var Data = await axios.post(BaseUrl + `/users/getId/${data.email}`);
    setTimeout(function () {
      seconds++;
      if (seconds > 59) {
        seconds = 0;
        mins++;
        if (mins > 59) {
          mins = 0;
          hours++;
          if (hours < 10) {
            hours = "0" + hours;
            $("#hours").text("0" + hours + ":");
          } else {
            hours = hours;
            $("#hours").text(hours + ":");
          }
        }
        if (mins < 10) {
          $("#mins").text("0" + mins + ":");
          mins = "0" + mins;
        } else {
          mins = mins;
          $("#mins").text(mins + ":");
        }
      }
      if (seconds < 10) {
        seconds = "0" + seconds;
        $("#seconds").text("0" + seconds);
      } else {
        seconds = seconds;
        $("#seconds").text(seconds);
      }
      startTimer();

      localStorage.setItem("timer-hr", hours);
      localStorage.setItem("timer-mins", mins);
      localStorage.setItem("timer-seconds", seconds);

      socket.current.emit("chat-duration", {
        from: Data.data.user.id,
        to: currentChat.id,
        mins: mins,
        seconds: seconds,
        hours: hours,
      });
    }, 1000);
  };

  const handleTyping = async () => {
    const data = await JSON.parse(localStorage.getItem("token"));
    socket.current.emit("typing-send-msg", {
      to: currentChat.id,
      from: senderId,
      senderName: data.username,
      chatType: "Inbound",
    });
  };
  const handleSendMsg = async (msg) => {
    const data = await JSON.parse(localStorage.getItem("token"));

    socket.current.emit("last-msg-send", {
      to: currentChat.id,
      from: senderId,
      chatType: "inbound",
      msg,
      senderName: data.username,
      // createdAt: new Date(),
    });

    socket.current.emit("send-msg", {
      to: currentChat.id,
      from: senderId,
      chatType: "inbound",
      msg,
      senderName: data.username,
      msgType: "web",
      // createdAt: new Date(),
    });
    await axios.post(BaseUrl + "/message/addMessage", {
      from: senderId,
      to: currentChat.id,
      message: msg,
      senderName: data.username,
      messageFrom: "fromClient",
      // timestamp: data.createdAt,
    });

    const msgs = [...messages];
    msgs.push({ fromSelf: false, message: msg, senderName: data.username });
    // console.log(msgs);
    setMessages(msgs);
  };

  useEffect(() => {
    console.log(socket.current, "test");
    if (socket.current) {
      socket.current.on("msg-receive", (msg) => {
        setAgentTyping(false);
        setArrivalMessage({
          fromSelf: true,
          message: msg.msg,
          senderName: msg.senderName,
        });
      });

      socket.current.on("typing-msg-receive", (msg) => {
        setAgentTyping(true);
      });
    }
    startTimer();
  }, []);

  useEffect(() => {
    arrivalMessage && setMessages((prev) => [...prev, arrivalMessage]);

    console.log(arrivalMessage, "check");
  }, [arrivalMessage]);

  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages]);

  return (
    <Container>
      <div className="chat-header">
        <div className="user-details">
          <div className="username">
            <h3>{currentChat.username}</h3>
          </div>
        </div>
        {/* <Logout /> */}
      </div>
      <div className="chat-messages">
        {messages.map((message) => {
          return (
            <div ref={scrollRef} key={uuidv4()}>
              <div
                className={`message ${
                  !message.fromSelf ? "sended" : "received"
                }`}
              >
                <div className="content ">
                  <p
                    className="client-username"
                    style={{
                      color: "gray",
                      fontSize: "0.8rem",
                      textTransform: "capitalize",
                    }}
                  >
                    {message.senderName}
                  </p>
                  <p>{message.message}</p>
                  <p id="time">{moment(message.time).format("h:mm A")}</p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      {AgentTyping ? (
        <div className="typing">
          <p>Agent is Typing ....</p>
        </div>
      ) : (
        <div></div>
      )}
      <ChatInput handleSendMsg={handleSendMsg} handleTyping={handleTyping} />
    </Container>
  );
}

const Container = styled.div`
  display: grid;
  grid-template-rows: 10% 80% 10%;
  gap: 0.1rem;
  overflow: hidden;
  @media screen and (min-width: 720px) and (max-width: 1080px) {
    grid-template-rows: 15% 70% 15%;
  }
  .chat-header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 2rem;
    .user-details {
      display: flex;
      align-items: center;
      gap: 1rem;

      .username {
        h3 {
          color: white;
        }
      }
    }
  }
  .chat-messages {
    padding: 1rem 2rem;
    display: flex;
    flex-direction: column;
    gap: 1rem;
    overflow: auto;
    &::-webkit-scrollbar {
      width: 0.2rem;
      &-thumb {
        background-color: #ffffff39;
        width: 0.1rem;
        border-radius: 1rem;
      }
    }
    .message {
      display: flex;
      align-items: center;
      .content {
        max-width: 40%;
        overflow-wrap: break-word;
        padding: 8px 8px;
        font-size: 1.1rem;
        border-radius: 1rem;
        color: #000;
        @media screen and (min-width: 720px) and (max-width: 1080px) {
          max-width: 70%;
        }
      }
      #time {
        color: gray;
        font-size: 0.8rem;
      }
    }
    .sended {
      justify-content: flex-end;
      .content {
        background-color: #4f04ff21;
      }
    }

    .received {
      justify-content: flex-start;
      .content {
        background-color: #9900ff20;
      }
    }
  }
`;
