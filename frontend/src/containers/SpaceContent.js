import React, { useState, useEffect } from "react";
import "../styles/index.scss";
import Chat from "../containers/Chat";
import { Button, Modal, Form, Card } from "react-bootstrap";
import { connect } from "react-redux";
import { SiMicrosoftteams } from "react-icons/si";
import Draggable from "react-draggable";
import { Tooltip } from "bootstrap";
import avatar from "../EmailComponents/Resources/images/imageicon1.png";
import reply from "../EmailComponents/Resources/images/reply2.png";
import replyall from "../EmailComponents/Resources/images/replyall.png";
import forward from "../EmailComponents/Resources/images/forward.png";
import pdf from "../EmailComponents/Resources/images/pdf.png";
import txt from "../EmailComponents/Resources/images/txt.png";
import word from "../EmailComponents/Resources/images/word.png";
import { useDispatch, useSelector } from "react-redux";
import { openReplyEmail, SendEmail } from "../redux/actions/spaceActions";
import parse from "html-react-parser";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import DownloadIcon from "@mui/icons-material/Download";
import DoNotTouchIcon from "@mui/icons-material/DoNotTouch";
import { GiBlackBook } from "react-icons/gi";

import {
  Grid,
  Paper,
  IconButton,
  Divider,
  Typography,
  Popover,
  MenuList,
  MenuItem,
  ListItemIcon,
  ListItemText,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@mui/material";

import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import {
  settogglechat,
  setrefreshtogglechat,
  sendMessage,
  setConferenceNotification,
  setConferenceNotificationVal,
  setSelectedColor,
  setchatid,
  setselectedemail,
  setselectedusername,
  setselectedcompany,
  setselectedadress,
  setselectedmobile,
  setselectedwhatsapp,
  setselectedfacebook,
  setselectedtwitter,
  setselectedteams,
  setselectedid,
  setSelectedchannel,
  setSelectedchanneldata,
  setUpdatechanneldata,
  DownloadAttachment,
  setcontactlist,
  setSendattachmentUrl,
  setshowKnowlegeguidecomponent,
  setIswrite,
} from "../redux/actions/spaceActions";

import {
  FiArrowDownLeft,
  FiArrowUpLeft,
  FiEdit,
  FiEye,
  FiShare2,
} from "react-icons/fi";
import { MdOutlineCallMerge } from "react-icons/md";
import "../components/UserInfo/contactdetails.scss";
import { FaArrowUp, FaHandHolding, FaWhatsapp } from "react-icons/fa";
import {
  BiTransferAlt,
  BiTransfer,
  BiGitMerge,
  BiArrowFromRight,
  BiHistory,
} from "react-icons/bi";
import {
  BsArrowLeftCircle,
  BsWhatsapp,
  BsChatSquareText,
  BsArrowRightCircle,
} from "react-icons/bs";
import "./App.scss";
import {
  AiFillEdit,
  AiFillEye,
  AiOutlineCloseCircle,
  AiOutlineFile,
} from "react-icons/ai";
import { coBrowseApi } from "./Page/apis/CobrowserApi";
import axios from "axios";
import {
  baseUrl,
  BaseUrl,
  newBaseUrlLang,
  TenantID,
  langskillTenantId,
  frontendBaseurl,
  errorApi,
  ticketUrl,
} from "./Page/Constants/BaseUrl";

import { setavailagent } from "../redux/actions/spaceActions";
import { toast, ToastContainer } from "react-toastify";
import moment from "moment";
import {
  RiErrorWarningLine,
  RiFileHistoryLine,
  RiMailForbidLine,
} from "react-icons/ri";
import { CTooltip } from "@coreui/react";
import { useNavigate } from "react-router-dom";
import { HiOutlineTicket, HiOutlineUserCircle } from "react-icons/hi";
import NewEmail from "../EmailComponents/Inbox/NewEmail";
import Inbox from "../EmailComponents/Inbox/Inbox";
import { SEND_MAIL_CONTENT } from "../EmailComponents/appList";
import { TbHandOff, TbListDetails } from "react-icons/tb";
import IFramesecondContainer from "../components/IFrame/IFrameContainer";

const mapStateToProps = (state) => {
  console.log("SpaceContent", state.data.attachmentUrl);
  return {
    chatid: state.data.chatid,
    refreshtogglechat: state.data.refreshtogglechat,
    chatdata: state.data.chatdata,
    selecteduserimage: state.data.selecteduserimage,
    togglechat: state.data.togglechat,
    usercolor: state.data.usercolor,
    uploadedFile: state.data.uploadedFile,
    internalExternal: state.data.internalExternal,
    chat: state.data.chat,
    availAgent: state.data.availAgent,
    selectedusername: state.data.selectedusername,
    selectedemail: state.data.selectedemail,
    selectedmobile: state.data.selectedmobile,
    selectedwhatsapp: state.data.selectedwhatsapp,
    selectedfacebook: state.data.selectedfacebook,
    selectedtwitter: state.data.selectedtwitter,
    selectedteams: state.data.selectedteams,
    selectedcompany: state.data.selectedcompany,
    selectedaddress: state.data.selectedaddress,
    selectedid: state.data.selectedid,
    selectedchannel: state.data.selectedchannel,
    selectedchanneldata: state.data.selectedchanneldata,
    updatechanneldata: state.data.updatechanneldata,
    InboxEmailBody: state.data.InboxEmailBody,
    attachmentUrl: state.data.attachmentUrl,
    showKnowlegeguidecomponent: state.data.showKnowlegeguidecomponent,
    iswrite: state.data.iswrite,
    knowledgeCategory:state.data.knowledgeCategory
  };
};

function SpaceContent(props) {
  const dispatch = useDispatch();

  const counterReplay = useSelector((state) => state.counter);
  const tenantId = localStorage.getItem("TenantId");

  const percentage = 50;

  var tooltipTriggerList = [].slice.call(
    document.querySelectorAll('[data-bs-toggle="tooltip"]')
  );
  var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new Tooltip(tooltipTriggerEl);
  });

  const navigate = useNavigate();
  const basedOnRole = JSON.parse(localStorage.getItem("tokenAgent"));

  var someDate = new Date();
  var date = someDate.setDate(someDate.getDate());
  var defaultValue = new Date(date).toISOString().split("T")[0];

  // const [todaydate, setDatetoday] = useState(defaultValue)
  const [eventKey, setEventKey] = useState("chatContainer");
  const [show_details, setShow_details] = useState(false);
  const [show, setShow] = useState(false);

  const handleOpenKnowledgeGuide = () => {
    props.setshowKnowlegeguidecomponent(true);
  };

  const [showTransferMod, setShowTransferMod] = useState(false);
  const [showTransferWhatsapp, setShowTransferWhatsapp] = useState(false);
  const [esignshow, setesignShow] = useState(false);
  const [agentTransferId, setAgentTransferId] = useState("agent");

  const [initiate, setInitiate] = useState("true");
  const [key, setKey] = useState("Userdetails");

  const [skilldropdown, setSkilldropdown] = useState([]);
  const [languagedropdown, setLanguagedropdown] = useState([]);
  const [skilldropdownid, setSkilldropdownid] = useState("");
  const [languagedropdownid, setLanguagedropdownid] = useState("");

  const [showfiltersupervisor, setShowfiltersupervisor] = useState(false);
  const [showfilterconferencesupervisor, setShowfilterconferencesupervisor] =
    useState(false);

  const [chatHistory, setChatHistory] = useState([]);

  const [filename, setFilename] = useState([]);
  const [opensidebarticket, setOpensidebarticket] = useState(false);
  const [opensidebarfilter, setOpenSidebarfilter] = useState(false);
  const [editbutton, seteditbutton] = useState(false);
  const [UserStartName, setUserStartName] = useState(true);
  const [sessionexpiredshow, setSessionexpiredshow] = useState(false);

  const [editemail, seteditemail] = useState();
  const [editusername, seteditusername] = useState();
  const [editmobile, seteditmobile] = useState();
  const [editwhatsapp, seteditwhatsapp] = useState();
  const [editfacebook, seteditfacebook] = useState();
  const [edittwitter, setedittwitter] = useState();
  const [editteams, seteditteams] = useState();
  const [editcompany, seteditcompany] = useState();
  const [editaddress, seteditaddress] = useState();
  const [availableAgent, setAvailableAgent] = useState([]);
  const [actionView, setActionView] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [valueIndex, setValueIndex] = useState({});
  const [conditionpage, setConditionpage] = useState(<Chat />);

  const [writeEmail, setWriteEmail] = useState(false);
  const [inboxContent, setInboxContent] = useState(null);
  const [inboxContentAttchment, setInboxContentAttachment] = useState([]);
  const [isReplyEmail, setIsReplyEmail] = useState(false);
  const [toSendlist, setToSendlist] = useState([]);
  const [toCCSendlist, setToCCSendlist] = useState([]);
  const [toBCCSendlist, setToBCCSendlist] = useState([]);
  const [escalationModal, setEscalationModal] = useState(false);
  const [value, selectedValue] = useState("");
  const [selectedtemplate, setSelectedtemplate] = useState([]);
  const [selectedFile, setSelectedFile] = useState([]);

  const handleOpenEscalationModal = () => {
    setEscalationModal(true);
  };

  const handleCloseEscalationModal = () => {
    setEscalationModal(false);
  };

  let permission = JSON.parse(localStorage.getItem("permission"));
  let indexNum = localStorage.getItem("indexOf");
  // console.log('indexNum', indexNum)

  const SendEmail = (obj) => {
    let myHeaders = new Headers();
    const access_token = localStorage.getItem("access_token");
    myHeaders.append("Authorization", "Bearer " + access_token);

    let data = obj;
    let requestOptions = {
      method: "POST",
      body: data,
      headers: myHeaders,
    };
    fetch(SEND_MAIL_CONTENT, requestOptions)
      .then((res) => res.json())
      .then((json) => {
        console.log("All json", json);
      })
      .catch((error) => {
        // dispatch(InboxErrorMessage(error, 'red'))
        console.log("error><><><><><><><><", error);
      });
  };

  const handleSendEmail = () => {
    console.log("clicked from newmail ");
    // const { selectedFile, editorState, to, cc, bcc, emailSubject } = this.state;
    // console.log("newemailAllState", this.state);
    // const { inboxContent, agentId } = this.props;
    const contentState = editorState.getCurrentContent();
    const html = draftToHtml(convertToRaw(contentState));
    let requestID = "";
    let uid = 0;
    if (inboxContent) {
      let tempStr = inboxContent.subject;
      requestID = tempStr.slice(
        tempStr.indexOf("-") + 1,
        tempStr.lastIndexOf("]")
      );
      uid = inboxContent.uid;
    }
    if (html) {
      let formdata = new FormData();
      formdata.append("files", props.attachmentUrl);
      console.log("attachmentUrlllllll", props.attachmentUrl);
      formdata.append("email", to.toString());
      formdata.append("subject", emailSubject);
      formdata.append("ccData", cc.toString());
      formdata.append("bccData", bcc.toString());
      formdata.append("html", props.attachmentUrl);
      formdata.append("requestID", requestID);
      formdata.append("agentID", agentId.user_id);
      console.log(
        "agentId.user_id===============================",
        agentId.user_id
      );
      formdata.append("receivedTime", moment().format());
      formdata.append("uid", uid);

      const config = {
        headers: {
          "content-type": "multipart/form-data",
          tenantId: "123456",
        },
      };
      const url = "https://gway.release.inaipi.ae/routeemail/email/sendEmail";
      axios
        .post(url, formdata, config)
        // setLoading(true)
        .then((response) => {
          if (response.data.status) {
            setLoading(false);
            let pic_url = response.data.data.signedUrl;
            let mediaType = response.data.data.mediaType.toUpperCase();
            props.handleSendMsg(pic_url, "AUDIO", "");
            setFile("");
            setFileTypeStore("");
          }
        });
      console.log("allFormadastsaconsorl", formdata);
      // this.props.action.SendEmail(formdata);
      // this.props.action.closeWriteEmail();
      props.sendMail(formdata);
      props.closeReplaypage();
      toast.success("Message Sent Successfully", {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  const handleOpenURL = (item) => {
    console.log("itemId<<<<<<<>>>>", item);
    const url = `https://spacesreporter.inaipiapp.com/helpdesk/ticket_view/${item.id}`;
    window.open(url, "_blank");
  };

  const cannotHandel = () => {
    console.log("cannotHandel Clicked");

    axios
      .put(
        `https://gway.release.inaipi.ae/routeemail/email/reassignEmails/${
          props.selectedchanneldata.Email
            ? props.selectedchanneldata.Email._id
            : ""
        }`
      )
      .then((response) => {
        console.log("cannotHandel response", response);
        if (response) {
          props.contactList();
          toast.success(response.data.message, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          console.log("handelOnclickInboxMessage", response.data);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const completedEmail = () => {
    const data = {
      id: props.selectedchanneldata.Email._id,
      status: "completed",
      requestID: props.selectedchanneldata.Email.requestID,
    };
    axios
      .post(
        "https://gway.release.inaipi.ae/routeemail/email/completedEmails",
        data
      )
      .then((response) => {
        if (response) {
          toast.success(response.data.message, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          console.log("handelOnclickInboxMessage", response.data);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const escalationSubmitButton = () => {
    const data = {
      agentID: value,
    };
    axios
      .put(
        `https://gway.release.inaipi.ae/routeemail/email/escalate/${
          props.selectedchanneldata.Email
            ? props.selectedchanneldata.Email._id
            : ""
        }`,
        data
      )
      .then((response) => {
        if (response) {
          this.props.InboxList();
          toast.success(response.data.message, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          console.log("handelOnclickInboxMessage", response.data);
          handleCloseEscalationModal();
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const openActionView = (e, value) => {
    setActionView(true);
    setAnchorEl(e.currentTarget);
    setValueIndex(value);
  };

  const closeActionView = () => {
    setActionView(false);
    setAnchorEl(null);
    setValueIndex({});
  };

  const handleClick = (value) => {
    console.log("value<>>>>>>>>>>><>Inbox", value);
    if (!_.isEmpty(value)) {
      let obj = {
        id: value._id,
      };
      setInboxContent(value);
    }
  };

  const handelOpenReplayEmail = () => {
    props.setIswrite(true);
  };

  const handelCloseReplayEmail = () => {
    props.setIswrite(false);
  };

  useEffect(() => {
    if (props.attachmentUrl) {
      onClickReply();
    }
  }, [props.attachmentUrl]);

  console.log("props.attachmentUrl", props.attachmentUrl);
  const onClickReply = () => {
    const temp = [];
    const tempCC = [];
    const tempBCC = [];
    if (!_.isEmpty(props.selectedchanneldata.Email)) {
      let tempStr = props.selectedchanneldata.Email.fromList;
      let tempArr = tempStr.split(",");
      _.map(tempArr, (val, i) => {
        const fromEmail = val.slice(val.indexOf("<") + 1, val.lastIndexOf(">"));
        temp.push(fromEmail);
      });
    }
    console.log("tempFrom|Inbox", temp);
    setToSendlist(temp);
    setToCCSendlist(tempCC);
    setToBCCSendlist(tempBCC);
    props.openReplyEmail();
    setIsReplyEmail(true);
    handelOpenReplayEmail();
    // handleCloseInboxDetailsDiv();
  };

  const onClickReplyAll = () => {
    const temp = [];
    const tempCC = [];
    const tempBCC = [];
    if (!_.isEmpty(inboxContent)) {
      let tempStr = inboxContent.fromList;
      let tempStrcc = inboxContent.fromList;
      let tempStrbcc = inboxContent.fromList;
      let tempArr = tempStr.split(",");
      let tempArrcc = tempStrcc.split(",");
      let tempArrbcc = tempStrbcc.split(",");
      _.map(tempArr, (val, i) => {
        const fromEmail = val.slice(val.indexOf("<") + 1, val.lastIndexOf(">"));
        temp.push(fromEmail);
      });
      _.map(tempArrcc, (val, i) => {
        const fromEmail = val.slice(val.indexOf("<") + 1, val.lastIndexOf(">"));
        tempCC.push(fromEmail);
      });
      _.map(tempArrbcc, (val, i) => {
        const fromEmail = val.slice(val.indexOf("<") + 1, val.lastIndexOf(">"));
        tempBCC.push(fromEmail);
      });
    }
    // Rest of the code for onClickReplyAll function goes here...
  };

  const downloadDoc = () => {
    if (!_.isEmpty(valueIndex)) {
      let obj = {
        url: valueIndex.url,
        extension: valueIndex.extension,
      };
      // Call the DownloadAttachment function from props passing the obj and filename
      DownloadAttachment(obj, valueIndex.filename);

      // Rest of the download logic goes here...
    }
  };

  const openDocument = () => {
    if (!_.isEmpty(valueIndex)) {
      window.open(valueIndex.url, "_blank");
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault(); // Prevent the default form submission behavior
    console.log("triggered>>>>>>>>>>>>>>>>>>>>>>>>>>>.");
  };

  const handleClosesessionexpired = () => {
    setSessionexpiredshow(false);
  };

  const handelopenSidebarfilter = () => {
    setOpenSidebarfilter(true);
  };
  const closeopenSidebarfilter = () => {
    setOpenSidebarfilter(false);
  };

  const handelopenSidebar = () => {
    setOpensidebarticket(true);
  };
  const closeopenSidebar = () => {
    setOpensidebarticket(false);
  };

  const handleTransfersupervisorClose = () => setShowfiltersupervisor(false);
  const handleTransfersupervisorOpen = (val) => {
    setShowfiltersupervisor(true);
    setCheckWhichCall(val);
  };

  const handleConferencesupervisorClose = () =>
    setShowfilterconferencesupervisor(false);
  const handleConferencesupervisorOpen = () =>
    setShowfilterconferencesupervisor(true);

  const {
    selectedusername,
    selectedemail,
    selectedmobile,
    selectedwhatsapp,
    selectedfacebook,
    selectedtwitter,
    selectedteams,
    selectedcompany,
    selectedaddress,
    selectedid,
    chatid,
    internalExternal,
    chat,
    availAgent,
  } = props;

  console.log("propsssssssssssssssssssssssssssssss<<<<<<<<<<<<<", props.chat);
  const [conferencemodal, setConferencemodal] = useState(false);
  const [cobrowswerinput, setCobrowserInput] = useState([]);

  // End Chat Modal
  const [endchatshow, setEndchatshow] = useState(false);
  const handleEndChatClose = () => setEndchatshow(false);
  const handleEndChatOpen = () => setEndchatshow(true);
  const handletransferWhatsappOpen = () => setShowTransferWhatsapp(true);
  const handlechatClose = () => setChatmodal(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleWhatsappClose = () => {
    setShowTransferWhatsapp(false);
  };
  const handleTransferClose = () => {
    setAgentTransferId("agent");
    setShowTransferMod(false);
  };

  const handleesignClose = () => setesignShow(false);
  const handleesignShow = () => setesignShow(true);

  const [oldChatId, setOldChatId] = useState("");
  const [checkWhichCall, setCheckWhichCall] = useState("");
  const [cobrowse, setCobrowse] = useState([]);
  const [agentbasedonfilter, setAgentbasedonfilter] = useState([]);
  const [sessionId, setSessionId] = useState("");
  const [checkedvalue, setCheckedvalue] = useState(false);
  const [comments, setComments] = useState("");
  const [reason, setReason] = useState("");
  const [phonenumber, setPhonenumber] = useState(chat.phonenumber);
  const [reasonerror, setReasonerror] = useState(false);
  const [commentserror, setCommentserror] = useState(false);
  const [phonenumbererror, setPhonenumbererror] = useState(false);
  const [sessionVal, setSessionValues] = useState([]);

  const [chatmodal, setChatmodal] = useState(false);
  const [summaryArrivalDate, setSummaryArrivalDate] = useState("");
  const [summaryChannel, setSummaryChannel] = useState("");
  const [summaryAgent, setSummaryAgent] = useState("");
  const [summaryStartedDate, setSummaryStartedDate] = useState("");
  const [summaryEndDate, setSummaryEndDate] = useState(undefined);
  const [ticketlist, setTicketlist] = useState([]);
  // const [date, setDate] = useState('')

  const [userchathistory, setUserchathistory] = useState([]);
  const [getsessionsbydate, setGetsessionsbydate] = useState(defaultValue);

  const [editUserModal, setEditUserModal] = useState(false);

  const errorHandel = async (error, endpoint) => {
    const tenantID = localStorage.getItem("TenantId");
    try {
      const payload = {
        servicename: "DCCCHAT",
        logs: error,
        description: endpoint,
      };

      const header = {
        headers: {
          // Authorization: "Bearer " + token,
          // "Content-Type": "application/json",
          tenantid: tenantID,
        },
      };
      const { data } = await axios.post(
        errorApi + "/UIlogs/createLogs",
        payload,
        header
      );
      if (data.status) {
        console.log("from error api", data);
      }
    } catch (error) {
      console.log("error", error);
    }
  };

  const updateEditvalue = async () => {
    // window.location.reload(true);
    const tokenAgent = JSON.parse(localStorage.getItem("tokenAgent"));
    const agent_ID = tokenAgent.id;
    console.log("agent_ID<<<<<<<<<<>>>>>>>>>>", agent_ID);
    const access_token = localStorage.getItem("access_token");
    console.log("access_token", access_token);
    let body = {
      username: editusername,
      email: editemail,
      phonenumber: editmobile,
      whatsappnumber: editwhatsapp,
      facebookId: editfacebook,
      twitterId: edittwitter,
      teamsId: editteams,
      address: editaddress,
      company: editcompany,
      id: selectedid,
      agent_id: agent_ID,
    };

    axios
      .post(BaseUrl + "/users/updateClient", body, {
        headers: {
          Authorization: "Bearer " + access_token,
          "Content-Type": "application/json",
          tenantId: tenantId,
        },
      })
      .then((res) => {
        console.log("updatevalue res>>>>>>>>>>>>>", res);
        window.location.reload(true);
      })

      .catch((error) => {
        errorHandel(error, "/users/updateClient");
        console.log(err);
      });
  };

  const getChatHistoryDetails = async () => {
    try {
      const access_token = localStorage.getItem("access_token");
      let session_id = props.chat.chat_session_id;
      let data = {
        session_id: session_id,
      };
      const sessions = await axios.post(
        BaseUrl + "/users/listchatHistory",
        data,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      if (sessions.data.status) {
        console.log("data new", sessions.data.data);
        setUserchathistory(sessions.data.data);
      } else {
        setUserchathistory([]);
      }
    } catch (error) {
      errorHandel(error, "/users/listchatHistory");

      // setSessionexpiredshow(true)
    }
  };

  const logout = async () => {
    const access_token = localStorage.getItem("access_token");
    let data = await JSON.parse(localStorage.getItem("tokenAgent"));
    const id = localStorage.getItem("TenantId");
    let userID = data.user_id;
    try {
      if (data) {
        const update = await axios.post(
          BaseUrl + "/users/logout/" + userID,
          {},
          {
            headers: {
              Authorization: "Bearer " + access_token,
              "Content-Type": "application/json",
              tenantId: tenantId,
            },
          }
        );
        // Clear localStorage items
        console.log("ertyuytdslogout", update.data.success);
        if (update.data.success == true) {
          localStorage.removeItem("AvayaUsername");
          localStorage.removeItem("tab");
          localStorage.removeItem("timer_connect_sec");
          localStorage.removeItem("AvayaPassword");
          localStorage.removeItem("AvayaDomain");
          localStorage.removeItem("client");
          localStorage.removeItem("statusValue");
          localStorage.removeItem("emailDisplay");
          localStorage.removeItem("timer_connect_min");
          localStorage.removeItem("tokenAgent");
          localStorage.removeItem("timer_status");
          localStorage.removeItem("NameDisplay");
          localStorage.removeItem("access_token");
          localStorage.removeItem("timer_connect_hour");

          if (!id) {
            window.location.href = "error.html";
            console.error("ID not found in local storage");
          } else {
            const loginUrl = `${frontendBaseurl}/?tenantID=${encodeURIComponent(
              id
            )}`;
            window.location.href = loginUrl;
          }

          // navigate("/");
        }
      }
    } catch (error) {
      errorHandel(error, "/users/logout/");
      console.log(error);
    }
  };

  const TicketHistory = () => {
    const Username = "imperium";
    const Password = "Indesk@$123#";

    const credentials = btoa(`${Username}:${Password}`);

    let data = {
      organization_id: "13",
      customer_id: "",
      ticket_id: "",
      role_id: 66,
      pagename: "Customers_viewcustomer",
      mobile: "",
      customer_name: "",
      email: props.selectedemail,
      type: "customer_tickets",
      page: 1,
    };

    let payload = {
      method: "post",
      maxBodyLength: Infinity,
      url: ticketUrl + "/api_new/index.php/getticketbyemail",
      headers: {
        Authorization: `Basic ${credentials}`,
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios
      .request(payload)
      .then((response) => {
        console.log("tickethistoryDate", response.data.data);
        setTicketlist(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // const SelectTemplate = () => {

  //   const Username = 'imperium';
  //   const Password = 'Indesk@$123#';

  //   const credentials = btoa(`${Username}:${Password}`);

  //   let data = {
  //       "organization_id": "13",
  //       "userid": 505,
  //       "pagename": "Masters_viewmasters",
  //       "role_id": 66
  //   };

  //   let payload = {
  //     method: "post",
  //     maxBodyLength: Infinity,
  //     url: ticketUrl + "/api_new/index.php/emailtemplatelist",
  //     headers: {
  //       Authorization: `Basic ${credentials}`,
  //       "Content-Type": "application/json",
  //     },
  //     data: data,
  //   };

  //   axios
  //     .request(payload)
  //     .then((response) => {
  //       console.log('tickethistoryDate',response.data.data);
  //       setSelectedtemplate(response.data.data)

  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // };

  const EmailAttachmentContent = (data) => {
    const imageFormat = [
      "png",
      "jpg",
      "jpeg",
      "jfif",
      "pjpeg",
      "pjp",
      "svg",
      "gif",
    ];
    if (!_.isEmpty(data.attachments)) {
      return (
        <>
          <Grid container spacing={1}>
            {_.map(
              _.filter(data.attachments, { location: "attachment" }),
              (val, i) => {
                if (imageFormat.includes(val.extension)) {
                  return (
                    <Grid item xs={3}>
                      <div
                        className="attachment_container"
                        onClick={(e) => openActionView(e, val)}
                      >
                        <img
                          src={val.url}
                          alt="Avatar"
                          className="image_attachment"
                        />
                        <div className="attachment_overlay">
                          <div className="attachment_text">
                            {val.filename}
                            {/* <span className='float-r'> <IconButton className='text-white' onClick={(e)=>this.openActionView(e,val)} > <ExpandMoreIcon /> </IconButton>  </span> */}
                          </div>
                        </div>
                      </div>
                    </Grid>
                  );
                }
              }
            )}
          </Grid>

          <Grid container spacing={1}>
            {_.map(
              _.filter(data.attachments, { location: "attachment" }),
              (val, i) => {
                if (!imageFormat.includes(val.extension)) {
                  return (
                    <Grid item xs={4}>
                      <Paper>
                        <MenuItem>
                          <ListItemIcon className="attachment_Preview_image">
                            <img
                              src={
                                val.extension === "pdf"
                                  ? pdf
                                  : val.extension === "docx"
                                  ? word
                                  : val.extension === "xls"
                                  ? word
                                  : txt
                              }
                              height="20px"
                              weight="20px"
                            />
                          </ListItemIcon>
                          <ListItemText>
                            <span className="attachment_el_text">
                              {val.filename}
                            </span>
                          </ListItemText>
                          <Typography variant="body2" color="text.secondary">
                            <IconButton onClick={(e) => openActionView(e, val)}>
                              {" "}
                              <ExpandMoreIcon />{" "}
                            </IconButton>
                          </Typography>
                        </MenuItem>
                      </Paper>
                    </Grid>
                  );
                }
              }
            )}
          </Grid>
        </>
      );
    }
  };

  const getIcon = (extension) => {
    switch (extension) {
      case "pdf":
        return pdf;
      case "docx":
        return word;
      case "xls":
        return excel;
      default:
        return txt;
    }
  };

  useEffect(() => {
    TicketHistory();

    getChatHistoryDetails();
  }, [props.selectedemail]);

  useEffect(() => {
    if (
      props.chat.unique_id == null ||
      props.chat.unique_id == undefined ||
      props.chat.unique_id == ""
    ) {
      // console.log("no call");
    } else {
      allfile();
    }
  }, [props.selectedemail]);

  useEffect(() => {
    seteditemail(selectedemail);
    seteditusername(selectedusername);
    seteditmobile(selectedmobile);
    seteditwhatsapp(selectedwhatsapp);
    seteditfacebook(selectedfacebook);
    setedittwitter(selectedtwitter);
    seteditteams(selectedteams);
    seteditcompany(selectedcompany);
    seteditaddress(selectedaddress);
    var h = window.innerHeight;
    // console.log('ScreenHeight>>>>>', h)
  }, [props.selectedemail]);

  const handlechatShow = async () => {
    try {
      const access_token = localStorage.getItem("access_token");
      let data = {
        sessionId: sessionId,
      };
      const chatHistory = await axios.post(
        BaseUrl + "/message/chatHistories/",
        data,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      if (chatHistory.data.status) {
        // console.log('allData',chatHistory.data.data)
        setChatHistory(chatHistory.data.data);
        setChatmodal(true);
      }
    } catch (error) {
      errorHandel(error, "/message/chatHistories/");
    }
  };

  const getSessionsDetails = async (value) => {
    try {
      const access_token = localStorage.getItem("access_token");

      let email = props.selectedemail;
      let mobile = props.selectedmobile;
      let data = {
        email: email,
        phonenumber: mobile,
        date: value,
      };

      const sessions = await axios.post(
        BaseUrl + "/message/getSessions/",
        data,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      if (sessions.data.success) {
        let data = sessions.data.data;
        setSessionValues(data);
      } else {
        setSessionValues([]);
      }
    } catch (error) {
      errorHandel(error, "/message/getSessions/");
    }
  };

  const allfile = () => {
    if (props.chat.unique_id.id) {
      let data = {
        userId: props.chat.unique_id ? props.chat.unique_id.id : "",
      };
      axios
        .post("https://qacc.inaipi.ae/v1/fileServer/getMediaUserId", data, {
          headers: {
            TenantId: "123456",
          },
        })
        .then((res) => {
          console.log("resPdf", res.data.data);
          setFilename(res.data.data);
        })
        .catch((err) => {
          errorHandel(err, "/fileServer/getMediaUserId");
          console.log("err");
        });
    }
  };

  useEffect(() => {
    getSessionsDetails(defaultValue);

    // console.log('alldataedvgshbh',props)
  }, [selectedemail]);

  const handleShowDetails = async (session_id) => {
    try {
      const access_token = localStorage.getItem("access_token");
      setSessionId(session_id);
      let data = {
        sessionId: session_id,
      };
      const sessions = await axios.post(
        BaseUrl + "/message/getSummery/",
        data,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      if (sessions.data.success) {
        let datas = sessions.data.data;
        setSummaryArrivalDate(datas[0].arrival_at);
        setSummaryChannel(datas[0].channel);
        let agent_one = datas[0].agent.username;
        let agent_two = datas[1].confUser;
        setSummaryAgent(agent_one + agent_two);
        setSummaryStartedDate(datas[0].chat_started_at);
        setSummaryEndDate(datas[0].chat_ended_at);
      }
      setShow(true);
    } catch (error) {
      errorHandel(error, "/message/getSummery/");
    }
  };

  useEffect(() => {
    languageDropdownList();
    skillsetDropdown();
    setOldChatId(chatid);
    if (props.refreshtogglechat) {
      setKey("Userdetails");
    }
    getAvailableAgents();
  }, [eventKey, chat]);

  const endChat = (session_id) => () => {
    if (reason == "") {
      setReasonerror(true);
      setCommentserror(false);
    } else if (comments == "") {
      setReasonerror(false);
      setCommentserror(true);
    } else {
      // const response = confirm("Are you sure you want end the chat?");
      // if (response) {
      let userId = JSON.parse(localStorage.getItem("tokenAgent"));
      let agentId = userId.id;
      // console.log('agentId',userId)
      let data = {
        chat_session_id: session_id,
        chatendby: "Agent",
        reason: reason,
        comment: comments,
        agent_id: agentId,
        ticketexpired: checkedvalue,
      };
      //let userId = JSON.parse(localStorage.getItem("tokenAgent"));
      // let passUserId = userId.user_id;

      axios
        .post(BaseUrl + "/message/endchat", data, {
          headers: {
            tenantId: tenantId,
          },
        })
        .then((res) => {
          console.log("chat ended");
          window.location.reload(true);
        })
        .catch((error) => {
          errorHandel(error, "/message/endchat");
          console.log(error);
        });
    }
    // }
  };

  const handleKeyPress = (chatid, event) => {
    if (event.keyCode === 13 || event.which === 13) {
      console.log("Clicked>>>>>>>>>>>>>>>>..", chatid);
      endChat(chatid);
    }
  };

  const imageFormat = [
    "png",
    "jpg",
    "jpeg",
    "jfif",
    "pjpeg",
    "pjp",
    "svg",
    "gif",
  ];

  //Skillset Dropdown for agent Filter

  const skillsetDropdown = () => {
    let data = {
      enabled: true,
    };
    axios
      .post(newBaseUrlLang + "usermodule/clientMaster/skills/list", data, {
        headers: {
          tenantId: tenantId,
        },
      })
      .then((res) => {
        // console.log("res", res.data.dataList);
        setSkilldropdown(res.data.dataList);
      })
      .catch((error) => {
        errorHandel(error, "/skills/list");
      });
  };

  const languageDropdownList = () => {
    const access_token = localStorage.getItem("access_token");
    let data = {
      enabled: true,
    };
    axios
      .post(newBaseUrlLang + "usermodule/clientMaster/languages/list", data, {
        headers: {
          tenantId: tenantId,

          // 'Authorization': 'Bearer ' + access_token,
          "Content-Type": "application/json",
        },
      })
      .then((res) => {
        // console.log("resLanguage", res);
        setLanguagedropdown(res.data.dataList);
      })
      .catch((error) => {
        errorHandel(error, "/languages/list");
      });
  };

  const userBasedonLanguageSkillset = (skillId, languageid) => {
    let userId = JSON.parse(localStorage.getItem("tokenAgent"));
    // alert('id',id)
    const access_token = localStorage.getItem("access_token");
    let data = {
      language_id: languageid ? languageid : languagedropdownid,
      skillset_id: skillId ? skillId : skilldropdownid,
      agent_id: userId.id,
    };
    axios
      .post(BaseUrl + "/users/listUserBySkillsetIdLanguageId", data, {
        headers: {
          Authorization: "Bearer " + access_token,
          "Content-Type": "application/json",
          tenantId: tenantId,
        },
      })
      .then((res) => {
        if (res.data.status == true) {
          console.log("datacominfg", res.data);
          setAgentbasedonfilter(res.data.data);
        }
      })
      .catch((error) => {
        errorHandel(error, "/users/listUserBySkillsetIdLanguageId");
        // toast.warn('Server is down,please try after some time', {
        //   position: "top-right",
        //   autoClose: 1000,
        //   hideProgressBar: false,
        //   closeOnClick: true,
        //   pauseOnHover: true,
        //   draggable: true,
        //   progress: undefined,
        //   theme: "light",
        //   });
        //   if (err.response.status == 401) {
        //     window.location.href = "/";
        //   }

        console.log(err);
      });
  };

  const sendEsignDoc = (eventKey) => {
    setEventKey("chatContainer");
    let esign_doc_link =
      "<a target='_blank' href='https://whatsapp.inaipiapp.com/crp_esign_signature/index.php'>https://whatsapp.inaipiapp.com/crp_esign_signature/index.php;</a>";
    props.sendMessage({ message: esign_doc_link });
    handleesignClose();
  };

  const CobrosweData = () => {
    if (chat.is_customer_disconnected) {
      toast.error("Customer is disconnected");
    } else {
      let data = {
        url: cobrowswerinput,
        agent_id: "313007",
        tags: ["tag1", "tag2"],
        hide_selector:
          "#input1,#input2,#emailID,#empAadhaar,#uan,#cust_PAN, #mobileinfo_Pan",
      };
      coBrowseApi(data)
        .then(function (response) {
          console.log("leadlist", response.leader_link);
          setCobrowse(response.data);
          window.open(response.leader_link);
          var sender_link = response.follower_link;
          props.sendMessage({ message: sender_link, data: props.uploadedFile });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const conferenceModel = () => {
    setConferencemodal(!conferenceModel);
  };

  const hideInitiate = () => {
    setInitiate(!initiate);
  };

  const transferAgent = (val) => {
    setCheckWhichCall(val);
    setShowTransferMod(true);
  };

  const getAvailableAgents = async () => {
    const access_token = localStorage.getItem("access_token");

    try {
      let userId = JSON.parse(localStorage.getItem("tokenAgent"));
      let passUserId = userId.user_id;
      const data = await axios.post(
        BaseUrl + "/users/listSupervisor",
        {},
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      if (data.data.status) {
        // props.setavailagent(data.data.data);
        setAvailableAgent(data.data.data);
      }
    } catch (error) {
      errorHandel(error, "/users/listSupervisor");
      console.log(error);
    }
  };

  const handleTransferWhatsapp = async () => {
    if (chat.is_customer_disconnected) {
      toast.error("Customer is disconnected");
    } else {
      const access_token = localStorage.getItem("access_token");
      try {
        let dataToPass = {
          chat_session_id: chatid,
          phonenumber: phonenumber,
        };

        const data = await axios.post(
          BaseUrl + "/users/transfertoWhatsapp",
          dataToPass,
          {
            headers: {
              Authorization: "Bearer " + access_token,
              "Content-Type": "application/json",
              tenantId: tenantId,
            },
          }
        );
        // if (confirm("Chat Transferred success")) {

        if (data.data.status) {
          await axios.post(
            BaseUrl + "/message/addMessage",
            {
              from: old_agent_id.id,
              to: chat.id,
              message: message_to_pass,
              senderName: old_agent_id.username,
              receiverName: chat.username,
              messageFrom: "fromAgent",
              userType: chat.chat_type,
              session: chat.chat_session_id,
              msg_sent_type: "NOTIFICATIONS",
            },
            {
              headers: {
                tenantId: tenantId,
              },
            }
          );

          toast.success("Chat Transferred to whatsapp sucessfully");
          handleTransferClose();
          window.location.reload();
        } else {
          toast.error("Enter a Valid Number");
          // handleTransferClose();
          window.location.reload();
        }
      } catch (error) {
        errorHandel(error, "/users/transfertoWhatsapp");
        console.log(error);
      }
    }
  };

  const transferAgentSubmit = async () => {
    try {
      if (chat.is_customer_disconnected) {
        toast.error("Customer is disconnected");
      } else {
        const access_token = localStorage.getItem("access_token");
        let selected_name =
          agentTransferId.options[agentTransferId.selectedIndex].text;
        let selected_val = agentTransferId.value;

        if (selected_name === "agent") {
          alert("select agent");
          return false;
        }

        let old_agent_id = JSON.parse(localStorage.getItem("tokenAgent"));

        let dataToPass = {
          sessionId: chatid,
          agentId: selected_val,
          oldAgentId: old_agent_id.id,
        };

        let message_to_pass;
        if (checkWhichCall == "transfer") {
          message_to_pass =
            "Chat Transferred from " +
            old_agent_id.username +
            " to " +
            selected_name;
          const data = await axios.post(
            BaseUrl + "/users/transferAgent",
            dataToPass,
            {
              headers: {
                Authorization: "Bearer " + access_token,
                "Content-Type": "application/json",
                tenantId: tenantId,
              },
            }
          );
          // if (confirm("Chat Transferred success")) {

          if (data.data.status) {
            await axios.post(
              BaseUrl + "/message/addMessage",
              {
                from: old_agent_id.id,
                to: chat.id,
                message: message_to_pass,
                senderName: old_agent_id.username,
                receiverName: chat.username,
                messageFrom: "fromAgent",
                userType: chat.chat_type,
                session: chat.chat_session_id,
                msg_sent_type: "NOTIFICATIONS",
              },
              {
                headers: {
                  tenantId: tenantId,
                },
              }
            );

            toast.success("Chat Transferred success");
            handleTransferClose();
            window.location.reload();
          } else {
            // toast.error(data.data.message);
            // handleTransferClose();
            // window.location.reload();
            toast.warn(data.data.message, {
              position: "top-right",
              autoClose: 1000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              theme: "light",
            });
          }

          // }
        } else {
          message_to_pass =
            "Chat conference with " +
            old_agent_id.username +
            " and " +
            selected_name;
          props.setConferenceNotificationVal(message_to_pass);
          const data = await axios.post(
            BaseUrl + "/users/conferenceAgent",
            dataToPass,
            {
              headers: {
                Authorization: "Bearer " + access_token,
                "Content-Type": "application/json",
                tenantId: tenantId,
              },
            }
          );
          if (data.data.status) {
            await axios.post(
              BaseUrl + "/message/addMessage",
              {
                from: old_agent_id.id,
                to: chat.id,
                message: message_to_pass,
                senderName: old_agent_id.username,
                receiverName: chat.username,
                messageFrom: "fromAgent",
                userType: chat.chat_type,
                session: chat.chat_session_id,
                msg_sent_type: "NOTIFICATIONS",
              },
              {
                headers: {
                  tenantId: tenantId,
                },
              }
            );

            console.log("conference Data", data.data);
            toast.success("Chat Conference success");
            handleTransferClose();
            props.setConferenceNotification(true);
            window.location.reload();
          } else {
            toast.warn(data.data.message);
            props.setConferenceNotification(false);
          }
        }
      }
    } catch (error) {
      errorHandel(error, "/users/conferenceAgent");
    }
  };
  const handelChecked = (event) => {
    setCheckedvalue(event.target.checked);

    console.log("checked Value", event.target.checked);
  };

  // let processedHtml = ''
  // if (props.selectedchanneldata.Email.attachments) {
  //     processedHtml = props..replace(/\s+/g, ' ').trim();
  // }

  console.log("mailinformation", props.selectedchanneldata.Email);

  return (
    <>
      <div>
        <ToastContainer></ToastContainer>
        {props.togglechat ? (
          <div
            className="recive-hed"
            style={{
              height: "4rem",
              backgroundColor: "white !important",
              width: "55%",
            }}
          >
            <div className="user-new" style={{ marginLeft: "-16px" }}>
              <div className="space-member space-member-dm">
                <div className="chatlist-hed-name d-flex align-items-center">
                  {selectedusername ? (
                    <div
                      className="sidebar_user_internal mx-1 text-truncate"
                      style={{
                        height: "40px",
                        width: "40px",
                        borderRadius: "50%",
                        background: props.usercolor,
                      }}
                    >
                      <p
                        className="my-auto chat_name  text-truncate"
                        style={{
                          color: "#7e7272",
                          textAlign: "center",
                          textTransform: "capitalize",
                        }}
                      >
                        <b
                          className="user_text_trans  text-truncate"
                          style={{
                            color: "#7e7272",
                            fontSize: "15px",
                            textTransform: "capitalize",
                          }}
                        >
                          {selectedusername
                            ? selectedusername.slice(0, 2).toUpperCase()
                            : ""}
                        </b>
                      </p>
                    </div>
                  ) : (
                    ""
                  )}
                  <p
                    className="text-truncate"
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      textTransform: "capitalize",
                      fontSize: "14px",
                      width: "90px",
                      fontFamily: "poppins",
                      wordBreak: "break-all",
                    }}
                  >
                    {selectedusername}
                  </p>
                </div>
              </div>
            </div>

            {chatid && internalExternal == 0 && (
              <div className="d-flex justify-content-between p-0 header_button">
                {basedOnRole.role == "Agent" ? (
                  <div className="mr-2 button_container-div">
                    {permission[indexNum].screen.find(
                      (element) => element.screenId === "TRANSFER"
                    ) ? (
                      <>
                        {props.selectedchannel == "email" ? (
                          <CTooltip content="Cannot Handel" placement="bottom">
                            <button
                              type="button"
                              className="btn btn-outline-secondary border-0 btn-sm "
                              onClick={() => cannotHandel()}
                            >
                              <TbHandOff className="btn_hover" size={15} />
                            </button>
                          </CTooltip>
                        ) : (
                          <CTooltip
                            content="Transfer to whatsapp"
                            placement="bottom"
                          >
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={handletransferWhatsappOpen}
                              className="btn btn-outline-secondary border-0 btn-sm show_hover"
                            >
                              <BsWhatsapp
                                className="icon_top-btn_small"
                                size={15}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}

                        {props.selectedchannel == "email" ? (
                          <CTooltip content="Escalation" placement="bottom">
                            <button
                              onClick={handleOpenEscalationModal}
                              type="button"
                              className="btn btn-outline-secondary border-0 btn-sm "
                            >
                              <FaArrowUp className="btn_hover" size={15} />
                            </button>
                          </CTooltip>
                        ) : (
                          <CTooltip content="Transfer Chat" placement="bottom">
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={() => {
                                handleTransfersupervisorOpen("transfer");
                              }}
                              className="btn btn-outline-secondary border-0 btn-sm"
                            >
                              <BiTransfer
                                className="icon_top-btn_small"
                                size={15}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}

                        {props.selectedchannel == "email" ? (
                          <CTooltip content="NoAction Email" placement="bottom">
                            <button
                              type="button"
                              className="btn btn-outline-secondary border-0 btn-sm "
                              onClick={completedEmail}
                            >
                              <RiMailForbidLine
                                className="btn_hover"
                                size={15}
                              />
                            </button>
                          </CTooltip>
                        ) : (
                          <CTooltip
                            content="Transfer Chat with Supervisor"
                            placement="bottom"
                          >
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={() => {
                                transferAgent("transfer");
                              }}
                              className="btn btn-outline-secondary border-0 btn-sm"
                            >
                              <BiTransferAlt
                                className="icon_top-btn_small"
                                size={15}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}
                      </>
                    ) : (
                      ""
                    )}

                    {permission[indexNum].screen.find(
                      (element) => element.screenId === "CONF"
                    ) ? (
                      <>
                        {props.selectedchannel == "email" ? (
                          ""
                        ) : (
                          <CTooltip
                            content="Conferance Chat"
                            placement="bottom"
                          >
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={() => {
                                handleTransfersupervisorOpen("conference");
                              }}
                              className="btn btn-outline-secondary border-0 btn-sm"
                            >
                              <BiGitMerge
                                className="icon_top-btn_small"
                                size={15}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}

                        {props.selectedchannel == "email" ? (
                          ""
                        ) : (
                          <CTooltip
                            content="Conferance Chat with Supervisor"
                            placement="bottom"
                          >
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={() => {
                                transferAgent("conference");
                              }}
                              className="btn btn-outline-secondary border-0 btn-sm"
                            >
                              <MdOutlineCallMerge
                                className="icon_top-btn_small"
                                size={15}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}
                      </>
                    ) : (
                      ""
                    )}

                    <CTooltip content="End Chat" placement="bottom">
                      <button
                        type="button"
                        onClick={handleEndChatOpen}
                        className="btn btn-outline-secondary border-0 btn-sm"
                      >
                        <AiOutlineCloseCircle
                          className="icon_top-btn_small"
                          size={15}
                          color="gray"
                        />
                      </button>
                    </CTooltip>

                    {props.selectedchannel == "email" ? (
                      <button
                        title="Close More Details"
                        type="button"
                        data-placement="bottom"
                        onClick={() => props.settogglechat(!props.togglechat)}
                        className="btn btn-outline-secondary border-0 btn-sm"
                        data-tippy-content=" Screen Share"
                      >
                        <BsArrowRightCircle
                          className="icon_top-btn_small"
                          size={15}
                          color="gray"
                        />
                      </button>
                    ) : (
                      <button
                        title="Close More Details"
                        type="button"
                        data-placement="bottom"
                        onClick={() => props.settogglechat(!props.togglechat)}
                        className="btn btn-outline-secondary border-0 btn-sm"
                        data-tippy-content=" Screen Share"
                      >
                        <BsArrowRightCircle
                          className="icon_top-btn_small"
                          size={15}
                          color="gray"
                        />
                      </button>
                    )}

                    <button
                      onClick={handleShow}
                      data-placement="bottom"
                      style={{ color: "red" }}
                      className="conference_modal"
                    ></button>
                  </div>
                ) : (
                  <div className="mr-2 button_container-div">
                    {permission[indexNum].screen.find(
                      (element) => element.screenId === "TRANSFER"
                    ) ? (
                      <>
                        {props.selectedchannel == "email" ? (
                          <CTooltip content="Cannot Handel" placement="bottom">
                            <button
                              type="button"
                              className="btn btn-outline-secondary border-0 btn-sm "
                              onClick={() => cannotHandel()}
                            >
                              <DoNotTouchIcon
                                className="btn_hover"
                                size={10}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        ) : (
                          <CTooltip
                            content="Transfer to whatsapp"
                            placement="bottom"
                          >
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={handletransferWhatsappOpen}
                              className="btn btn-outline-secondary border-0 btn-sm show_hover"
                            >
                              <BsWhatsapp
                                className="icon_top-btn_small"
                                size={15}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}

                        {props.selectedchannel == "email" ? (
                          <CTooltip content="Escalation" placement="bottom">
                            <button
                              type="button"
                              className="btn btn-outline-secondary border-0 btn-sm "
                              onClick={handleOpenEscalationModal}
                            >
                              <FaArrowUp
                                className="btn_hover"
                                size={20}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        ) : (
                          <CTooltip content="Transfer Chat" placement="bottom">
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={() => {
                                handleTransfersupervisorOpen("transfer");
                              }}
                              className="btn btn-outline-secondary border-0 btn-sm"
                            >
                              <BiTransfer
                                className="icon_top-btn_small"
                                size={15}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}
                      </>
                    ) : (
                      ""
                    )}

                    {props.selectedchannel == "email" ? (
                      <CTooltip content="NoAction Email" placement="bottom">
                        <button
                          type="button"
                          className="btn btn-outline-secondary border-0 btn-sm "
                          onClick={completedEmail}
                        >
                          <RiMailForbidLine className="btn_hover" size={23} />
                        </button>
                      </CTooltip>
                    ) : (
                      ""
                    )}

                    {permission[indexNum].screen.find(
                      (element) => element.screenId === "CONF"
                    ) ? (
                      <>
                        {props.selectedchannel == "email" ? (
                          ""
                        ) : (
                          <CTooltip
                            content="Conferance Chat"
                            placement="bottom"
                          >
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={() => {
                                handleTransfersupervisorOpen("conference");
                              }}
                              className="btn btn-outline-secondary border-0 btn-sm"
                            >
                              <BiGitMerge
                                className="icon_top-btn_small"
                                size={15}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}
                      </>
                    ) : (
                      ""
                    )}

                    {props.selectedchannel == "email" ? (
                      ""
                    ) : (
                      <CTooltip content="End Chat" placement="bottom">
                        <button
                          type="button"
                          onClick={handleEndChatOpen}
                          className="btn btn-outline-secondary border-0 btn-sm"
                        >
                          <AiOutlineCloseCircle
                            className="icon_top-btn_small"
                            size={15}
                            color="gray"
                          />
                        </button>
                      </CTooltip>
                    )}

                    {props.selectedchannel == "email" ? (
                      <button
                        title="Close More Details"
                        type="button"
                        data-placement="bottom"
                        onClick={() => props.settogglechat(!props.togglechat)}
                        className="btn btn-outline-secondary border-0 btn-sm"
                        data-tippy-content=" Screen Share"
                      >
                        <BsArrowRightCircle
                          className="icon_top-btn_small"
                          size={15}
                          color="gray"
                        />
                      </button>
                    ) : (
                      <button
                        title="Close More Details"
                        type="button"
                        data-placement="bottom"
                        onClick={() => props.settogglechat(!props.togglechat)}
                        className="btn btn-outline-secondary border-0 btn-sm"
                        data-tippy-content=" Screen Share"
                      >
                        <BsArrowRightCircle
                          className="icon_top-btn_small"
                          size={15}
                          color="gray"
                        />
                      </button>
                    )}

                    <button
                      onClick={handleShow}
                      data-placement="bottom"
                      style={{ color: "red" }}
                      className="conference_modal"
                    ></button>
                  </div>
                )}
              </div>
            )}
          </div>
        ) : (
          <div
            className="recive-hed"
            style={{
              height: "4rem",
              backgroundColor: "white !important",
              width: "100%",
            }}
          >
            <div className="user-new" style={{ marginLeft: "-16px" }}>
              <div className="space-member space-member-dm">
                <div className="chatlist-hed-name d-flex align-items-center">
                  {selectedusername ? (
                    <div
                      className="sidebar_user_internal mx-1"
                      style={{
                        height: "40px",
                        width: "40px",
                        borderRadius: "50%",
                        background: props.usercolor,
                      }}
                    >
                      <p
                        className="my-auto chat_name"
                        style={{
                          color: "#7e7272",
                          textAlign: "center",
                          textTransform: "capitalize",
                        }}
                      >
                        <b
                          className="user_text_trans"
                          style={{
                            color: "#7e7272",
                            fontSize: "15px",
                            textTransform: "capitalize",
                          }}
                        >
                          {selectedusername
                            ? selectedusername.slice(0, 2).toUpperCase()
                            : ""}
                        </b>
                      </p>
                    </div>
                  ) : (
                    ""
                  )}
                  <p
                    className="text-truncate"
                    style={{
                      fontWeight: "bold",
                      color: "black",
                      textTransform: "capitalize",
                      fontSize: "14px",
                      width: "30rem",
                    }}
                  >
                    {selectedusername}
                  </p>
                </div>
              </div>
            </div>

            {chatid && internalExternal == 0 && (
              <div className="call">
                {basedOnRole.role == "Agent" ? (
                  <div className="">
                    {permission[indexNum].screen.find(
                      (element) => element.screenId === "TRANSFER"
                    ) ? (
                      <>
                        {props.selectedchannel == "email" ? (
                          <CTooltip content="Cannot Handel" placement="bottom">
                            <button
                              type="button"
                              className="btn btn-outline-secondary border-0 btn-sm "
                              onClick={() => cannotHandel()}
                            >
                              <TbHandOff
                                className="btn_hover"
                                size={17}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        ) : (
                          <CTooltip
                            content="Transfer to whatsapp"
                            placement="bottom"
                          >
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={handletransferWhatsappOpen}
                              className="btn btn-outline-secondary border-0 btn-sm "
                            >
                              <FaWhatsapp
                                className="btn_hover"
                                size={20}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}

                        {props.selectedchannel == "email" ? (
                          <CTooltip content="Escalation" placement="bottom">
                            <button
                              onClick={handleOpenEscalationModal}
                              type="button"
                              className="btn btn-outline-secondary border-0 btn-sm "
                            >
                              <FaArrowUp
                                className="btn_hover"
                                size={18}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        ) : (
                          <CTooltip content="Transfer Chat" placement="bottom">
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={() => {
                                handleTransfersupervisorOpen("transfer");
                              }}
                              className="btn btn-outline-secondary border-0 btn-sm"
                            >
                              <BiTransfer
                                className="icon_top-btn"
                                size={18}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}

                        {props.selectedchannel == "email" ? (
                          <CTooltip content="NoAction Email" placement="bottom">
                            <button
                              type="button"
                              className="btn btn-outline-secondary border-0 btn-sm "
                              onClick={completedEmail}
                            >
                              <RiMailForbidLine
                                className="btn_hover"
                                size={18}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        ) : (
                          <CTooltip
                            content="Transfer Chat with Supervisor"
                            placement="bottom"
                          >
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={() => {
                                transferAgent("transfer");
                              }}
                              className="btn btn-outline-secondary border-0 btn-sm"
                            >
                              <BiTransferAlt
                                className="icon_top-btn"
                                size={20}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}
                      </>
                    ) : (
                      ""
                    )}

                    {permission[indexNum].screen.find(
                      (element) => element.screenId === "CONF"
                    ) ? (
                      <>
                        {props.selectedchannel == "email" ? (
                          ""
                        ) : (
                          <CTooltip
                            content="Conferance Chat"
                            placement="bottom"
                          >
                            <button
                              type="button"
                              color="gray"
                              disabled={chat.is_customer_disconnected}
                              onClick={() => {
                                handleTransfersupervisorOpen("conference");
                              }}
                              className="btn btn-outline-secondary border-0 btn-sm"
                            >
                              <FiShare2
                                className="icon_top-btn"
                                size={20}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}

                        {props.selectedchannel == "email" ? (
                          ""
                        ) : (
                          <CTooltip
                            content="Conferance Chat with Supervisor"
                            placement="bottom"
                          >
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={() => {
                                transferAgent("conference");
                              }}
                              className="btn btn-outline-secondary border-0 btn-sm"
                            >
                              <MdOutlineCallMerge
                                className="icon_top-btn"
                                size={20}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}
                      </>
                    ) : (
                      ""
                    )}

                    {props.selectedchannel == "email" ? (
                      <CTooltip content="End Chat" placement="bottom">
                        <button
                          type="button"
                          onClick={handleEndChatOpen}
                          className="btn btn-outline-secondary border-0 btn-sm"
                        >
                          <AiOutlineCloseCircle
                            className="icon_top-btn"
                            size={20}
                            color="gray"
                          />
                        </button>
                      </CTooltip>
                    ) : (
                      <CTooltip content="End Chat" placement="bottom">
                        <button
                          type="button"
                          onClick={handleEndChatOpen}
                          className="btn btn-outline-secondary border-0 btn-sm"
                        >
                          <AiOutlineCloseCircle
                            className="icon_top-btn"
                            size={20}
                            color="gray"
                          />
                        </button>
                      </CTooltip>
                    )}
                    {props.selectedchannel == "email" ? (
                      <button
                        type="button"
                        onClick={() => props.settogglechat(!props.togglechat)}
                        className="btn btn-outline-secondary border-0 btn-sm"
                      >
                        <BsArrowLeftCircle
                          className="icon_top-btn"
                          size={20}
                          color="gray"
                        />
                      </button>
                    ) : (
                      <button
                        type="button"
                        onClick={() => props.settogglechat(!props.togglechat)}
                        className="btn btn-outline-secondary border-0 btn-sm"
                      >
                        <BsArrowLeftCircle
                          className="icon_top-btn"
                          size={20}
                          color="gray"
                        />
                      </button>
                    )}

                    {props.selectedchannel == "email" ? (
                      ""
                    ) : (
                      <button
                        onClick={handleShow}
                        style={{ color: "red" }}
                        className="conference_modal"
                      ></button>
                    )}
                  </div>
                ) : (
                  <div className="">
                    {permission[indexNum].screen.find(
                      (element) => element.screenId === "TRANSFER"
                    ) ? (
                      <>
                        {props.selectedchannel == "email" ? (
                          ""
                        ) : (
                          <CTooltip
                            content="Transfer to whatsapp"
                            placement="bottom"
                          >
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={handletransferWhatsappOpen}
                              className="btn btn-outline-secondary border-0 btn-sm "
                            >
                              <FaWhatsapp
                                className="btn_hover"
                                size={20}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}

                        {props.selectedchannel == "email" ? (
                          ""
                        ) : (
                          <CTooltip content="Transfer Chat" placement="bottom">
                            <button
                              type="button"
                              disabled={chat.is_customer_disconnected}
                              onClick={() => {
                                handleTransfersupervisorOpen("transfer");
                              }}
                              className="btn btn-outline-secondary border-0 btn-sm"
                            >
                              <BiTransfer
                                className="icon_top-btn"
                                size={20}
                                color="gray"
                              />
                            </button>
                          </CTooltip>
                        )}
                      </>
                    ) : (
                      ""
                    )}

                    {/* <CTooltip
                          content="Transfer Chat with Supervisor"
                          placement="bottom"
                        >
                          <button
                            type="button"
                            disabled={chat.is_customer_disconnected}
                            onClick={() => {
                              transferAgent("transfer");
                            }}
                            className="btn btn-outline-secondary border-0 btn-sm"
                          >
                            <BiTransferAlt
                              className="icon_top-btn"
                              size={20}
                              color="gray"
                            />
                          </button>
                        </CTooltip> */}

                    {permission[indexNum].screen.find(
                      (element) => element.screenId === "CONF"
                    ) ? (
                      <CTooltip content="Conferance Chat" placement="bottom">
                        <button
                          type="button"
                          color="gray"
                          disabled={chat.is_customer_disconnected}
                          onClick={() => {
                            handleTransfersupervisorOpen("conference");
                          }}
                          className="btn btn-outline-secondary border-0 btn-sm"
                        >
                          <FiShare2
                            className="icon_top-btn"
                            size={20}
                            color="gray"
                          />
                        </button>
                      </CTooltip>
                    ) : (
                      ""
                    )}

                    {/* <CTooltip
                          content="Conferance Chat with Supervisor"
                          placement="bottom"
                        >
                          <button
                            type="button"
                            disabled={chat.is_customer_disconnected}
                            onClick={() => {
                              transferAgent("conference");
                            }}
                            className="btn btn-outline-secondary border-0 btn-sm"
                          >
                            <MdOutlineCallMerge
                              className="icon_top-btn"
                              size={20}
                              color="gray"
                            />
                          </button>
                        </CTooltip> */}
                    {/* } */}

                    <CTooltip content="End Chat" placement="bottom">
                      <button
                        type="button"
                        onClick={handleEndChatOpen}
                        className="btn btn-outline-secondary border-0 btn-sm"
                      >
                        <AiOutlineCloseCircle
                          className="icon_top-btn"
                          size={20}
                          color="gray"
                        />
                      </button>
                    </CTooltip>

                    <button
                      type="button"
                      onClick={() => props.settogglechat(!props.togglechat)}
                      className="btn btn-outline-secondary border-0 btn-sm"
                    >
                      <BsArrowLeftCircle
                        className="icon_top-btn"
                        size={20}
                        color="gray"
                      />
                    </button>

                    <button
                      onClick={handleShow}
                      style={{ color: "red" }}
                      className="conference_modal"
                    ></button>
                  </div>
                )}
              </div>
            )}
          </div>
        )}

        <div style={{ width: "100%", display: "flex", justifyContent: "end" }}>
          {props.togglechat && chatid && internalExternal == 0 ? (
            <>
              {/* SidebarFilter component */}

              {opensidebarticket && (
                <nav
                  className="sidebar_filter"
                  style={{
                    position: "absolute",
                    zIndex: 1,
                    left: "40rem",
                    width: "84rem",
                    top: 0,
                  }}
                >
                  <div className="filter col-md-3 shadow p-4 ">
                    <div className="d-flex justify-content-between align-items-center mb-3">
                      <p className="mb-0">Create Ticket</p>
                      <div className="dismiss ">
                        <span
                          className="material-symbols-outlined"
                          onClick={closeopenSidebar}
                        >
                          close
                        </span>
                      </div>
                    </div>

                    <div className="filter_form">
                      <form className=" fillter_form2 ">
                        <div className="mb-1 d-flex flex-column text-start">
                          <label for="Select" className="form-label">
                            Department
                          </label>
                          <select
                            className="form-select facility_select"
                            aria-label="Default select example"
                          >
                            <option value="System">System</option>
                            <option value="IT">IT</option>
                          </select>
                        </div>

                        <div className="mb-1 d-flex flex-column text-start ">
                          <label for="Select" className="form-label">
                            Facility
                          </label>
                          <select
                            className="form-select facility_select "
                            aria-label="Default select example"
                          >
                            <option value="ADC">ADC</option>
                            <option value="ZETDC">ZETDC</option>
                            <option value="MOE">MOE</option>
                            <option value="MOCD">MOCD</option>
                            <option value="MOECP">MOECP</option>
                            <option value="HCT">HCT</option>
                          </select>
                        </div>

                        <div className="mb-3 d-flex flex-column text-start">
                          <label for="Select" className="form-label">
                            Priority
                          </label>
                          <select
                            className="form-select facility_select"
                            aria-label="Default select example"
                          >
                            <option value="High">High</option>
                            <option value="Medium">Medium</option>
                            <option value="Low">Low</option>
                          </select>
                        </div>

                        <div className="mb-1 d-flex flex-column text-start">
                          <label for="Select" className="form-label">
                            Status
                          </label>
                          <select
                            className="form-select facility_select"
                            aria-label="Default select example"
                          >
                            <option value="Open">Open</option>
                            <option value="Inprogress">Inprogress</option>
                            <option value="Onhold">Onhold</option>
                            <option value="Closed">Closed</option>
                          </select>
                        </div>

                        <div className="mb-3">
                          <label
                            for="exampleFormControlTextarea1"
                            className="form-label"
                          >
                            Example textarea
                          </label>
                          <textarea
                            className="form-control facility_select"
                            id="exampleFormControlTextarea1"
                            rows="3"
                          ></textarea>
                        </div>
                      </form>
                      <div className="filter_submit">
                        <div className="d-flex">
                          <div className="col-md-6   pe-2 ">
                            <button
                              type="button"
                              className="btn btn-outline-danger w-100"
                              onClick={closeopenSidebar}
                            >
                              Reset
                            </button>
                          </div>

                          <div className="col-md-6 ps-2">
                            <button
                              type="button"
                              className="btn btn-primary2 w-100 apply-btn"
                            >
                              Apply
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </nav>
              )}

              {opensidebarfilter && (
                <nav
                  className="sidebar_filter"
                  style={{
                    position: "absolute",
                    zIndex: 1,
                    left: "40rem",
                    width: "84rem",
                    top: 0,
                  }}
                >
                  <div className="filter col-md-3 shadow p-4 ">
                    <div className="d-flex justify-content-between align-items-center mb-3">
                      <p className="mb-0">Filter Ticket</p>
                      <div className="dismiss ">
                        <span
                          className="material-symbols-outlined"
                          onClick={closeopenSidebarfilter}
                        >
                          close
                        </span>
                      </div>
                    </div>

                    <div className="filter_form">
                      <form className=" fillter_form2 ">
                        <div className="mb-3 d-flex flex-column text-start">
                          <label for="Select" className="form-label">
                            Filter by
                          </label>
                          <select
                            className="form-select facility_select"
                            aria-label="Default select example"
                          >
                            <option value="3">Custom</option>
                            <option value="1">Date created</option>
                            <option value="2">Due by time</option>
                            <option value="3">Last modified</option>
                            <option value="3">Priority</option>
                            <option value="3">Status</option>
                            <option value="3">Ascending</option>
                            <option value="3">Descending</option>
                          </select>
                        </div>
                        <div className="custom_div">
                          <div className="mb-3 d-flex flex-column text-start">
                            <label for="fromdate" className="form-label">
                              From
                            </label>
                            <input
                              type="date"
                              className="form-control facility_select"
                              id=""
                              aria-describedby="date"
                            />
                          </div>
                          <div className="mb-3 d-flex flex-column text-start">
                            <label for="todate" className="form-label">
                              To
                            </label>
                            <input
                              type="date"
                              className="form-control facility_select"
                              id=""
                              aria-describedby="date"
                            />
                          </div>
                        </div>

                        <div className="mb-3 d-flex flex-column text-start">
                          <label for="Select" className="form-label">
                            Search By Ticket ID
                          </label>
                          <select
                            className="form-select facility_select"
                            aria-label="Default select example"
                          >
                            <option selected>Open this select ID</option>
                            <option value="1">#123456</option>
                            <option value="2">12345226</option>
                            <option value="3">123452216</option>
                          </select>
                        </div>
                      </form>
                      <div className="filter_submit">
                        <div className="d-flex">
                          <div className="col-md-6   pe-2 ">
                            <button
                              type="button"
                              className="btn btn-outline-danger w-100"
                              onClick={closeopenSidebarfilter}
                            >
                              Reset
                            </button>
                          </div>

                          <div className="col-md-6 ps-2">
                            <button
                              type="button"
                              className="btn btn-primary2 w-100 apply-btn"
                            >
                              Apply
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </nav>
              )}

              {/* sidebar */}

              <div className="sider_div">
                <div
                  className="card"
                  style={{ height: "100vh", width: "100%" }}
                >
                  <div className="container">
                    <div className="row">
                      <div className="col-md-12  px-0">
                        <div className="details-user-chat border-bottom p-0 mt-0">
                          <div
                            className="chat-profile d-flex justify-content-between align-items-center"
                            style={{ padding: "12px" }}
                          >
                            <div className="d-flex mx-2 w-100">
                              <div
                                className="card userInfo_div ms-2"
                                style={{
                                  height: "15rem",
                                  width: "100%",
                                  overflowY: "auto",
                                }}
                              >
                                <div
                                  className="row w-100"
                                  style={{ height: "45vh" }}
                                >
                                  <div
                                    className="w-100 col-md-6 align-items-center d-flex  my-1"
                                    style={{ flexDirection: "column" }}
                                  >
                                    <div style={{ width: 100, height: 100 }}>
                                      <CircularProgressbar
                                        value={percentage}
                                        text={`${percentage}%`}
                                        background
                                        backgroundPadding={6}
                                        styles={buildStyles({
                                          backgroundColor: "#3e98c7",
                                          textColor: "#fff",
                                          pathColor: "#fff",
                                          trailColor: "transparent",
                                        })}
                                      />
                                    </div>

                                    <div className="d-flex align-items-center w-100 my-2">
                                      <div className="d-flex mx-auto">
                                        <div className="mx-2 text-truncate">
                                          <p
                                            style={{ wordBreak: "break-all", }}
                                            className="name fw-bold d-flex mx-auto"
                                          >
                                            {selectedusername}
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <FiEdit
                                            color="blue"
                                            className=""
                                            style={{ cursor: "pointer" }}
                                            onClick={() =>
                                              setEditUserModal(true)
                                            }
                                          />
                                        </div>
                                      </div>
                                      <div></div>
                                    </div>

                                    <div className="use-cont d-flex w-75">
                                      <div className="d-flex">
                                        <div className="">
                                          <i
                                            className="fa-solid fa-envelope me-2 my-2"
                                            style={{ color: "#0c87ef" }}
                                          ></i>
                                        </div>
                                        <p style={{ wordBreak: "break-all" }}>
                                          {selectedemail}
                                        </p>
                                      </div>
                                    </div>

                                    <div className=" use-cont d-flex w-75">
                                      <div className="d-flex align-items-start">
                                        <div>
                                          <i
                                            className="fa-solid fa-mobile me-2 my-2"
                                            style={{ color: "#0c87ef" }}
                                          ></i>
                                        </div>
                                        <p
                                          style={{ wordBreak: "break-all" }}
                                          className="mx-1"
                                        >
                                          {selectedmobile == ""
                                            ? "--"
                                            : selectedmobile}
                                        </p>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-md-6 ">
                                    <div className="my-4">
                                      <div className="d-flex"></div>
                                      <div className="use-cont d-flex">
                                        <div className="d-flex align-items-start">
                                          <div>
                                            <i
                                              className="fa-solid fa-location-dot my-2"
                                              style={{ color: "#dd4440" }}
                                            ></i>
                                          </div>
                                          <p
                                            style={{ wordBreak: "break-all" }}
                                            className="location_div"
                                          >
                                            {selectedaddress == ""
                                              ? "--"
                                              : selectedaddress}
                                          </p>
                                        </div>
                                      </div>

                                      <div className=" use-cont d-flex">
                                        <div className="d-flex align-items-start">
                                          <div>
                                            <i
                                              className="fa-brands fa-facebook-f my-2"
                                              style={{ color: "#0c87ef" }}
                                            ></i>
                                          </div>
                                          <p style={{ wordBreak: "break-all" }}>
                                            {selectedfacebook == ""
                                              ? "--"
                                              : selectedfacebook}
                                          </p>
                                        </div>
                                      </div>

                                      <div className=" use-cont d-flex">
                                        <div className="d-flex align-items-start">
                                          <div>
                                            <i
                                              className="fa-brands fa-whatsapp my-2"
                                              style={{ color: "#4cc659" }}
                                            ></i>
                                          </div>
                                          <p style={{ wordBreak: "break-all" }}>
                                            {selectedwhatsapp == ""
                                              ? "--"
                                              : selectedwhatsapp}
                                          </p>
                                        </div>
                                      </div>

                                      <div className="use-cont d-flex">
                                        <div className="d-flex align-items-start">
                                          <div>
                                            {/* <i class="fa-brands fa-teamspeak my-2"></i> */}
                                            <SiMicrosoftteams
                                              className="my-2"
                                              style={{ color: "#7980e5" }}
                                            />
                                          </div>
                                          <p
                                            style={{ wordBreak: "break-all" }}
                                            className="mx-1"
                                          >
                                            {selectedteams == ""
                                              ? "--"
                                              : selectedteams}
                                          </p>
                                        </div>
                                      </div>

                                      <div className=" use-cont d-flex">
                                        <div className="d-flex align-items-start">
                                          <div>
                                            <i
                                              class="fa-brands fa-twitter my-2"
                                              style={{ color: "#1da1f2" }}
                                            ></i>
                                          </div>
                                          <p style={{ wordBreak: "break-all" }}>
                                            {selectedtwitter == ""
                                              ? "--"
                                              : selectedtwitter}
                                          </p>
                                        </div>
                                      </div>

                                      <div className=" use-cont d-flex">
                                        <div className="d-flex align-items-start">
                                          <div>
                                            <i
                                              class="fa-regular fa-building my-2"
                                              style={{ color: "#dd4440" }}
                                            ></i>
                                          </div>
                                          <p style={{ wordBreak: "break-all" }}>
                                            {selectedcompany == ""
                                              ? "--"
                                              : selectedcompany}
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div
                          className="chat-tabs ml-3"
                          style={{ height: "37vh" }}
                        >
                          <ul
                            className="nav nav-pills mb-2 border-bottom d-flex justify-content-between"
                            id="pills-tab"
                            role="tablist"
                          >
                            <CTooltip
                              content="Interaction History"
                              placement="top"
                            >
                              <li className="nav-item" role="presentation">
                                <button
                                  className="nav-link active "
                                  id="pills-home-tab"
                                  data-bs-toggle="pill"
                                  data-bs-target="#pills-home"
                                  type="button"
                                  role="tab"
                                  aria-controls="pills-home"
                                  aria-selected="true"
                                  style={{ fontSize: "12px" }}
                                >
                                  <RiFileHistoryLine size={17} />
                                </button>
                              </li>
                            </CTooltip>

                            <CTooltip content="Session History" placement="top">
                              <li className="nav-item" role="presentation">
                                <button
                                  className="nav-link"
                                  id="pills-interaction-tab"
                                  data-bs-toggle="pill"
                                  data-bs-target="#pills-interaction"
                                  type="button"
                                  role="tab"
                                  aria-controls="pills-interaction"
                                  aria-selected="false"
                                  style={{ fontSize: "12px" }}
                                >
                                  <BiHistory size={17} />
                                </button>
                              </li>
                            </CTooltip>

                            <CTooltip content="Smart-X" placement="top">
                              <li className="nav-item" role="presentation">
                                <button
                                  className="nav-link"
                                  id="pills-loan-tab"
                                  data-bs-toggle="pill"
                                  data-bs-target="#pills-loan"
                                  type="button"
                                  role="tab"
                                  aria-controls="pills-loan"
                                  aria-selected="false"
                                  style={{ fontSize: "12px" }}
                                >
                                  <TbListDetails size={17} />
                                </button>
                              </li>
                            </CTooltip>

                            <CTooltip content="Files" placement="top">
                              <li className="nav-item" role="presentation">
                                <button
                                  className="nav-link"
                                  id="pills-profile-tab"
                                  data-bs-toggle="pill"
                                  data-bs-target="#pills-profile"
                                  type="button"
                                  role="tab"
                                  aria-controls="pills-profile"
                                  aria-selected="false"
                                  style={{ fontSize: "12px" }}
                                >
                                  <AiOutlineFile size={17} />
                                </button>
                              </li>
                            </CTooltip>

                            <CTooltip content="Ticket" placement="top">
                              <li className="nav-item" role="presentation">
                                <button
                                  className="nav-link"
                                  id="pills-ticket-tab"
                                  data-bs-toggle="pill"
                                  data-bs-target="#pills-ticket"
                                  type="button"
                                  role="tab"
                                  aria-controls="pills-ticket"
                                  aria-selected="false"
                                  style={{ fontSize: "12px" }}
                                >
                                  <HiOutlineTicket size={18} />
                                </button>
                              </li>
                            </CTooltip>

                            <CTooltip content="Knowledge Guide" placement="top">
                              <li className="nav-item" role="presentation">
                                <button
                                  className="nav-link"
                                  id="pills-ticket-tab"
                                  data-bs-toggle="pill"
                                  data-bs-target="#pills-reservation"
                                  type="button"
                                  role="tab"
                                  aria-controls="pills-ticket"
                                  aria-selected="false"
                                  style={{ fontSize: "8px" }}
                                  onClick={handleOpenKnowledgeGuide}
                                >
                                  <GiBlackBook size={17} />
                                </button>
                              </li>
                            </CTooltip>
                          </ul>
                          <div className="tab-content" id="pills-tabContent">
                            <div
                              className="tab-pane fade show active"
                              id="pills-home"
                              role="tabpanel"
                              aria-labelledby="pills-home-tab"
                            >
                              <div className="d-flex container-fluid w-100">
                                <div className="ms-auto">
                                  <input
                                    type="Date"
                                    className="form-control form-control-sm w-100"
                                    style={{ cursor: "pointer" }}
                                    onChange={(e) => {
                                      getSessionsDetails(e.target.value);
                                      setGetsessionsbydate(e.target.value);
                                    }}
                                    value={getsessionsbydate}
                                  />
                                </div>
                              </div>
                              <div className="historynew m-2">
                                {sessionVal.length > 0 ? (
                                  sessionVal.map((item) => {
                                    return (
                                      <div className="card-chat pt-2 pb-2 mb-2">
                                        <div className="container-fluid">
                                          <div className="row">
                                            {item.channel == "webchat" ? (
                                              <div className="col-md-1  box d-flex align-items-center justify-content-center sms-bg">
                                                <i className="fa-brands fa-rocketchat"></i>
                                              </div>
                                            ) : item.channel ==
                                              "from_whatsapp" ? (
                                              <div className="col-md-1  box d-flex align-items-center justify-content-center what-bg">
                                                <i className="fa-brands fa-whatsapp"></i>
                                              </div>
                                            ) : item.channel ==
                                              "from_facebook" ? (
                                              <div className="col-md-1  box d-flex align-items-center justify-content-center fb-bg">
                                                <i className="fa-brands fa-facebook-f"></i>
                                              </div>
                                            ) : item.channel ==
                                              "from_twitter" ? (
                                              <div className="col-md-1  box d-flex align-items-center justify-content-center twit-bg">
                                                <i className="fa-brands fa-twitter"></i>
                                              </div>
                                            ) : item.channel == "from_teams" ? (
                                              <div className="col-md-1  box d-flex align-items-center justify-content-center teams-bg">
                                                {/* <i className="fa-brands fa-teams">T</i> */}
                                                <SiMicrosoftteams
                                                  className="btn_hover"
                                                  size={20}
                                                  color="white"
                                                />
                                              </div>
                                            ) : (
                                              <div className="col-md-1  box d-flex  align-items-center justify-content-center call-bg">
                                                <i className="fa-brands fa-call">
                                                  C
                                                </i>
                                                {/* <MdOutlineCall
                                          
                                            size={20}
                                            color="white"
                                          /> */}
                                              </div>
                                            )}
                                            <div className="col-md-11 d-flex flex-wrap">
                                              <div className="box2 me-2   badge-bg d-flex align-items-center justify-content-center">
                                                <strong className="change_font_size">
                                                  <i className="fa-solid fa-user-tie me-1 "></i>
                                                  Agent :
                                                </strong>
                                                <p className="mb-0 ms-1 ">
                                                  {item.agent?.username}
                                                </p>
                                              </div>
                                              <div className="box2 me-2   badge-bg d-flex align-items-center justify-content-center">
                                                <strong className="change_font_size">
                                                  <i className="fa-solid fa-hourglass-half me-1"></i>
                                                  Language :
                                                </strong>
                                                <p className="mb-0 ms-1">
                                                  {item.language}
                                                </p>
                                              </div>
                                              <div className="box2 me-2   badge-bg d-flex align-items-center justify-content-center">
                                                <strong className="change_font_size">
                                                  <i className="fa-solid fa-calendar-days me-1"></i>
                                                  Date :
                                                </strong>
                                                <p className="mb-0 ms-1">
                                                  {" "}
                                                  {moment(
                                                    props.chat.chat_started_at
                                                  ).format("L")}
                                                </p>
                                              </div>
                                              <div className="box2 me-2   badge-bg d-flex align-items-center justify-content-center">
                                                <strong className="change_font_size">
                                                  <i className="fa-solid fa-suitcase me-1"></i>
                                                  Skill :
                                                </strong>
                                                <p className="mb-0 ms-1">
                                                  {" "}
                                                  {item.skillset}
                                                </p>
                                              </div>

                                              <div className="box2 me-2   badge-bg d-flex align-items-center justify-content-center">
                                                <strong className="change_font_size">
                                                  <i
                                                    className="fa-solid fa-eye"
                                                    style={{
                                                      cursor: "pointer",
                                                    }}
                                                    onClick={() =>
                                                      handleShowDetails(
                                                        item.chat_session_id
                                                      )
                                                    }
                                                  ></i>
                                                </strong>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    );
                                  })
                                ) : (
                                  <div className="d-flex justify-content-center align-items-center h-100 my-auto">
                                    <div
                                      className="text-muted d-flex"
                                      style={{ flexDirection: "column" }}
                                    >
                                      <span className="mx-auto">
                                        <RiErrorWarningLine size={30} />
                                      </span>
                                      <span className="mx-auto">
                                        <h6
                                          className="text-muted mt-2"
                                          style={{ fontFamily: "poppins" }}
                                        >
                                          No Data Found
                                        </h6>
                                      </span>
                                    </div>
                                  </div>
                                )}
                              </div>
                              {/* <div className="border-bottom mb-2"></div> */}
                              {/* <div className="d-flex justify-content-between align-items-center m-2">
                                <div className="tickt-name">TICKET HISTORY</div>
                                <div className="d-flex justify-content-center align-items-center">
                                  <div>
                                    <input
                                      type="date"
                                      className="form-control form-control-sm"
                                    />
                                  </div>
                                  <div className="d-flex">
                                    <button
                                      type="button"
                                      onClick={() => handelopenSidebar()}
                                      className="btn btn-primary d-flex-p btn-sm open-filter d-flex justify-content-center align-items-center"
                                    >
                                      <div>
                                        <span className="material-symbols-outlined">
                                          {" "}
                                          add
                                        </span>
                                      </div>
                                    </button>
                                    <button
                                      type="button"
                                      onClick={handelopenSidebarfilter}
                                      className="btn btn-primary d-flex-p btn-sm open-filter"
                                    >
                                      <span className="material-symbols-outlined">
                                        {" "}
                                        filter_list
                                      </span>
                                    </button>
                                  </div>
                                </div>
                              </div>
                              <div className="time-line-main m-2 p-2">
                                <ul className="timeline">
                                  <li className="timeline-item">
                                    <div className="timeline-marker"></div>
                                    <div className="timeline-content">
                                      <div className="d-flex flex-wrap">
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Agent :</strong>
                                          <p className="mb-0 ms-1">Janani,</p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Ticket ID :</strong>
                                          <p className="mb-0 ms-1">
                                            D1234567ED,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Created Time :</strong>
                                          <p className="mb-0 ms-1">
                                            {" "}
                                            10:10 AM,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Date:</strong>
                                          <p className="mb-0 ms-1">
                                            10/01/2023,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Status:</strong>
                                          <p className="mb-0 ms-1 text-success">
                                            Open
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                  <li className="timeline-item">
                                    <div className="timeline-marker"></div>
                                    <div className="timeline-content">
                                      <div className="d-flex flex-wrap">
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Agent :</strong>
                                          <p className="mb-0 ms-1">Janani,</p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Ticket ID :</strong>
                                          <p className="mb-0 ms-1">
                                            D1234567ED,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Created Time :</strong>
                                          <p className="mb-0 ms-1">
                                            {" "}
                                            10:10 AM,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Date:</strong>
                                          <p className="mb-0 ms-1">
                                            10/01/2023,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Status:</strong>
                                          <p className="mb-0 ms-1 text-success">
                                            Open
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                  <li className="timeline-item">
                                    <div className="timeline-marker"></div>
                                    <div className="timeline-content">
                                      <div className="d-flex flex-wrap">
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Agent :</strong>
                                          <p className="mb-0 ms-1">Janani,</p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Ticket ID :</strong>
                                          <p className="mb-0 ms-1">
                                            D1234567ED,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Created Time :</strong>
                                          <p className="mb-0 ms-1">
                                            {" "}
                                            10:10 AM,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Date:</strong>
                                          <p className="mb-0 ms-1">
                                            10/01/2023,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Status:</strong>
                                          <p className="mb-0 ms-1 text-success">
                                            Open
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                  <li className="timeline-item">
                                    <div className="timeline-marker"></div>
                                    <div className="timeline-content">
                                      <div className="d-flex flex-wrap">
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Agent :</strong>
                                          <p className="mb-0 ms-1">Janani,</p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Ticket ID :</strong>
                                          <p className="mb-0 ms-1">
                                            D1234567ED,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Created Time :</strong>
                                          <p className="mb-0 ms-1">
                                            {" "}
                                            10:10 AM,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Date:</strong>
                                          <p className="mb-0 ms-1">
                                            10/01/2023,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Status:</strong>
                                          <p className="mb-0 ms-1 text-success">
                                            Open
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                </ul>
                              </div> */}
                            </div>

                            <div
                              className="tab-pane fade"
                              id="pills-profile"
                              role="tabpanel"
                              aria-labelledby="pills-profile-tab"
                            >
                              <div className="m-2 file-main">
                                <div className="chat-r-file">
                                  {filename.length > 0 ? (
                                    filename.map((item) => {
                                      return (
                                        <div className="row m-2 border p-2 justify-content-center align-items-center bg-white">
                                          <div className="col-2">
                                            <i className="fas fa-file fs-31 text-primary"></i>
                                          </div>
                                          <div className="col-7">
                                            <div>
                                              <p className="mb-0 fw-bold fs-14">
                                                {item.path.split("/")}
                                              </p>
                                            </div>
                                            <div>
                                              <p className="fs-12 mb-0 fw-light text-secondary">
                                                {moment(
                                                  item.createdDate
                                                ).format("l   hh:mm:s A")}
                                              </p>
                                            </div>
                                          </div>
                                          <div className="col-2">
                                            <button
                                              type="button"
                                              className="btn btn-outline-secondary border-0"
                                              data-tippy-content="Download"
                                            >
                                              <i className="fas fa-download"></i>
                                            </button>
                                          </div>
                                        </div>
                                      );
                                    })
                                  ) : (
                                    <div>
                                      <div
                                        className="text-muted d-flex"
                                        style={{
                                          flexDirection: "column",
                                          marginTop: "15%",
                                        }}
                                      >
                                        <span className="mx-auto">
                                          <RiErrorWarningLine size={30} />
                                        </span>
                                        <span className="mx-auto">
                                          <h6
                                            className="text-muted mt-2"
                                            style={{ fontFamily: "poppins" }}
                                          >
                                            No Files Found
                                          </h6>
                                        </span>
                                      </div>
                                    </div>
                                  )}
                                </div>
                              </div>
                            </div>

                            {/* Interaction History */}

                            <div
                              className="tab-pane fade"
                              id="pills-interaction"
                              role="tabpanel"
                              aria-labelledby="pills-interaction-tab"
                            >
                              <div className="m-2 file-main">
                                <div className="chat-r-file">
                                  <div className="tab_content_userinfo">
                                    {userchathistory.length > 0 ? (
                                      userchathistory.map((item) => (
                                        <Card
                                          style={{
                                            width: "90%",
                                            height: "4rem",
                                            marginLeft: "20px",
                                          }}
                                          className="mt-3 pl-3 shadow"
                                        >
                                          <div
                                            className="text-center d-flex mt-3 "
                                            style={{ overflowY: "auto" }}
                                          >
                                            {item.type == "Transfer" ? (
                                              <p>
                                                <BiTransferAlt size={25} />
                                              </p>
                                            ) : (
                                              <p>
                                                <MdOutlineCallMerge size={25} />
                                              </p>
                                            )}
                                            {item.type == "Transfer" ? (
                                              <p
                                                style={{
                                                  fontWeight: "bold",
                                                  paddingLeft: 10,
                                                  fontSize: 12,
                                                  fontFamily: "poppins",
                                                }}
                                              >
                                                Chat Transfered From{" "}
                                                {item.agent} to{" "}
                                                {item.destinationAgent}
                                              </p>
                                            ) : (
                                              <p
                                                style={{
                                                  fontWeight: "bold",
                                                  paddingLeft: 10,
                                                  fontSize: 12,
                                                  fontFamily: "poppins",
                                                }}
                                              >
                                                {item.agent} conferenced with{" "}
                                                {item.destinationAgent}
                                              </p>
                                            )}
                                            <span
                                              style={{
                                                fontSize: 10,
                                                color: "#000",
                                                fontWeight: "400",
                                                position: "absolute",
                                                top: 31,
                                                left: "50px",
                                                fontFamily: "poppins",
                                              }}
                                            >
                                              {moment(item.time).format("LLL")}
                                            </span>
                                          </div>
                                        </Card>
                                      ))
                                    ) : (
                                      <div>
                                        <div
                                          className="text-muted d-flex"
                                          style={{
                                            flexDirection: "column",
                                            marginTop: "15%",
                                          }}
                                        >
                                          <span className="mx-auto">
                                            <RiErrorWarningLine size={30} />
                                          </span>
                                          <span className="mx-auto">
                                            <h6
                                              className="text-muted mt-2"
                                              style={{ fontFamily: "poppins" }}
                                            >
                                              No Session History Found
                                            </h6>
                                          </span>
                                        </div>
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>

                            {/* Loan Eligibility */}

                            <div
                              className="tab-pane fade"
                              id="pills-loan"
                              role="tabpanel"
                              aria-labelledby="pills-loan-tab"
                            >
                              {/* <div className="m-2 file-main-loan">
                                <div className="chat-r-file container-fluid h-50">
                                  <div>
                                    <h6
                                      className="mt-1 text-center"
                                      style={{ fontFamily: "poppins" }}
                                    >
                                      Loan Eligibility
                                    </h6>
                                  </div>
                                  <div className="d-flex justify-content-between mt-2 ml-2">
                                    <span
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      userId
                                    </span>
                                    <span
                                      className="mr-2"
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      true
                                    </span>
                                  </div>

                                  <div className="d-flex justify-content-between mt-2 ml-2">
                                    <span
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      userId
                                    </span>
                                    <span
                                      className="mr-2"
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      true
                                    </span>
                                  </div>
                                  <div className="d-flex justify-content-between mt-1 ml-2">
                                    <span
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      userName
                                    </span>
                                    <span
                                      className="mr-2"
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      --
                                    </span>
                                  </div>
                                  <div className="d-flex justify-content-between mt-1 ml-2">
                                    <span
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      domainId
                                    </span>
                                    <span
                                      className="mr-2"
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      0
                                    </span>
                                  </div>
                                  <div className="d-flex justify-content-between mt-1 ml-2">
                                    <span
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      buId
                                    </span>
                                    <span
                                      className="mr-2"
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      true
                                    </span>
                                  </div>
                                  <div className="d-flex justify-content-between mt-1 ml-2">
                                    <span
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      channel
                                    </span>
                                    <span
                                      className="mr-2"
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      62
                                    </span>
                                  </div>
                                  <div className="d-flex justify-content-between mt-1 ml-2">
                                    <span
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      RuleName
                                    </span>
                                    <span
                                      className="mr-2"
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      Loan Eligibility
                                    </span>
                                  </div>
                                  <div className="d-flex justify-content-between mt-1 ml-2">
                                    <span
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      Reason
                                    </span>
                                    <span
                                      className="mr-2"
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      Success
                                    </span>
                                  </div>
                                  <div className="d-flex justify-content-between mt-1 ml-2">
                                    <span
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      Status
                                    </span>
                                    <span
                                      className="mr-2"
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      Success
                                    </span>
                                  </div>
                                  <div className="d-flex justify-content-between mt-1 ml-2">
                                    <span
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      ActionType
                                    </span>
                                    <span
                                      className="mr-2"
                                      style={{
                                        fontSize: "12px",
                                        fontFamily: "poppins",
                                      }}
                                    >
                                      Static Value
                                    </span>
                                  </div>
                                </div>
                              </div> */}
                              <div className="time-line-main m-2 p-2 h-10">
                                <ul className="timeline">
                                  <div className="m-2 file-main-loan">
                                    <div className="chat-r-file container-fluid h-50">
                                      <div>
                                        <h6
                                          className="mt-1 text-center"
                                          style={{ fontFamily: "poppins" }}
                                        >
                                          Loan Eligibility
                                        </h6>
                                      </div>
                                      <div className="d-flex justify-content-between mt-2 ml-2">
                                        <span
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          userId
                                        </span>
                                        <span
                                          className="mr-2"
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          true
                                        </span>
                                      </div>

                                      <div className="d-flex justify-content-between mt-2 ml-2">
                                        <span
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          userId
                                        </span>
                                        <span
                                          className="mr-2"
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          true
                                        </span>
                                      </div>
                                      <div className="d-flex justify-content-between mt-1 ml-2">
                                        <span
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          userName
                                        </span>
                                        <span
                                          className="mr-2"
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          --
                                        </span>
                                      </div>
                                      <div className="d-flex justify-content-between mt-1 ml-2">
                                        <span
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          domainId
                                        </span>
                                        <span
                                          className="mr-2"
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          0
                                        </span>
                                      </div>
                                      <div className="d-flex justify-content-between mt-1 ml-2">
                                        <span
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          buId
                                        </span>
                                        <span
                                          className="mr-2"
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          true
                                        </span>
                                      </div>
                                      <div className="d-flex justify-content-between mt-1 ml-2">
                                        <span
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          channel
                                        </span>
                                        <span
                                          className="mr-2"
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          62
                                        </span>
                                      </div>
                                      <div className="d-flex justify-content-between mt-1 ml-2">
                                        <span
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          RuleName
                                        </span>
                                        <span
                                          className="mr-2"
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          Loan Eligibility
                                        </span>
                                      </div>
                                      <div className="d-flex justify-content-between mt-1 ml-2">
                                        <span
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          Reason
                                        </span>
                                        <span
                                          className="mr-2"
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          Success
                                        </span>
                                      </div>
                                      <div className="d-flex justify-content-between mt-1 ml-2">
                                        <span
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          Status
                                        </span>
                                        <span
                                          className="mr-2"
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          Success
                                        </span>
                                      </div>
                                      <div className="d-flex justify-content-between mt-1 ml-2">
                                        <span
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          ActionType
                                        </span>
                                        <span
                                          className="mr-2"
                                          style={{
                                            fontSize: "12px",
                                            fontFamily: "poppins",
                                          }}
                                        >
                                          Static Value
                                        </span>
                                      </div>
                                    </div>
                                  </div>
                                </ul>
                              </div>
                            </div>

                            {/* Ticket */}

                            <div
                              className="tab-pane fade"
                              id="pills-ticket"
                              role="tabpanel"
                              aria-labelledby="pills-ticket-tab"
                              style={{ height: "40vh" }}
                            >
                              <div className="d-flex justify-content-between align-items-center m-2">
                                <div className="tickt-name">TICKET HISTORY</div>
                                {/* <div className="d-flex justify-content-center align-items-center">
                                  <div>
                                    <input
                                      type="date"
                                      className="form-control form-control-sm"
                                    />
                                  </div>
                                  <div className="d-flex">
                                    <button
                                      type="button"
                                      onClick={() => handelopenSidebar()}
                                      className="btn btn-primary d-flex-p btn-sm open-filter d-flex justify-content-center align-items-center"
                                    >
                                      <div>
                                        <span className="material-symbols-outlined">
                                          {" "}
                                          add
                                        </span>
                                      </div>
                                    </button>
                                    <button
                                      type="button"
                                      onClick={handelopenSidebarfilter}
                                      className="btn btn-primary d-flex-p btn-sm open-filter"
                                    >
                                      <span className="material-symbols-outlined">
                                        {" "}
                                        filter_list
                                      </span>
                                    </button>
                                  </div>
                                </div> */}
                              </div>
                              <div className="time-line-main m-2 p-2">
                                <ul className="timeline">
                                  {ticketlist.map((item) => {
                                    return (
                                      <li className="timeline-item">
                                        <div className="timeline-marker"></div>
                                        <div className="timeline-content">
                                          <div className="d-flex flex-wrap">
                                            <div className="d-flex justify-content-center align-items-center">
                                              <strong>Agent :</strong>
                                              <p className="mb-0 ms-1">
                                                {item.username}
                                              </p>
                                            </div>
                                            <div className="d-flex justify-content-center align-items-center">
                                              <strong>Ticket ID :</strong>
                                              <p className="mb-0 ms-1">
                                                {item.ticket_id}
                                              </p>
                                            </div>
                                            <div className="d-flex justify-content-center align-items-center">
                                              <strong>Created at :</strong>
                                              <p className="mb-0 ms-1">
                                                {moment(item.created_at).format(
                                                  "ll"
                                                )}
                                              </p>
                                            </div>
                                            <div className="d-flex justify-content-center align-items-center">
                                              <strong>Priority:</strong>
                                              <p className="mb-0 ms-1">
                                                {item.priority}
                                              </p>
                                            </div>
                                            <div className="d-flex justify-content-center align-items-center">
                                              <strong>Status:</strong>
                                              <p className="mb-0 ms-1 text-success">
                                                {item.status}
                                              </p>
                                            </div>

                                            <div className="d-flex justify-content-center align-items-center">
                                              <p className="mb-0 ms-1 text-primary">
                                                <FiEye
                                                  onClick={() =>
                                                    handleOpenURL(item)
                                                  }
                                                />
                                              </p>
                                            </div>
                                          </div>
                                        </div>
                                      </li>
                                    );
                                  })}

                                  {/* <li className="timeline-item">
                                    <div className="timeline-marker"></div>
                                    <div className="timeline-content">
                                      <div className="d-flex flex-wrap">
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Agent :</strong>
                                          <p className="mb-0 ms-1">Janani,</p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Ticket ID :</strong>
                                          <p className="mb-0 ms-1">
                                            D1234567ED,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Created Time :</strong>
                                          <p className="mb-0 ms-1">
                                            {" "}
                                            10:10 AM,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Date:</strong>
                                          <p className="mb-0 ms-1">
                                            10/01/2023,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Status:</strong>
                                          <p className="mb-0 ms-1 text-success">
                                            Open
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <p className="mb-0 ms-1 text-primary">
                                            <FiEye />
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                  <li className="timeline-item">
                                    <div className="timeline-marker"></div>
                                    <div className="timeline-content">
                                      <div className="d-flex flex-wrap">
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Agent :</strong>
                                          <p className="mb-0 ms-1">Janani,</p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Ticket ID :</strong>
                                          <p className="mb-0 ms-1">
                                            D1234567ED,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Created Time :</strong>
                                          <p className="mb-0 ms-1">
                                            {" "}
                                            10:10 AM,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Date:</strong>
                                          <p className="mb-0 ms-1">
                                            10/01/2023,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Status:</strong>
                                          <p className="mb-0 ms-1 text-success">
                                            Open
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <p className="mb-0 ms-1 text-primary">
                                            <FiEye />
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </li>
                                  <li className="timeline-item">
                                    <div className="timeline-marker"></div>
                                    <div className="timeline-content">
                                      <div className="d-flex flex-wrap">
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Agent :</strong>
                                          <p className="mb-0 ms-1">Janani,</p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Ticket ID :</strong>
                                          <p className="mb-0 ms-1">
                                            D1234567ED,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Created Time :</strong>
                                          <p className="mb-0 ms-1">
                                            {" "}
                                            10:10 AM,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Date:</strong>
                                          <p className="mb-0 ms-1">
                                            10/01/2023,
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <strong>Status:</strong>
                                          <p className="mb-0 ms-1 text-success">
                                            Open
                                          </p>
                                        </div>
                                        <div className="d-flex justify-content-center align-items-center">
                                          <p className="mb-0 ms-1 text-primary">
                                            <FiEye />
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </li> */}
                                </ul>
                              </div>
                            </div>

                            {/* Reservation */}

                            {/* <iframe
           src="https://release.edayaapp.com/receptionistDashboard"
           width="100%"
           height="90%"
         ></iframe> */}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          ) : (
            <div></div>
          )}
        </div>

        {props.showKnowlegeguidecomponent && (
          <div
            className="knowledge-guide"
            style={{
              background: "white",
              maxWidth: "31rem",
              width: "489px",
              height: "82%",
              position: "absolute",
              transform: "translateX(-50%)",
              left: "45rem",
              top: "0rem",
              zIndex: "999",
              overflow: "scroll",
              boxShadow: "3px 3px 8px gray",
            }}
          >
            <IFramesecondContainer />
          </div>
        )}

        {editUserModal && (
          <Draggable handle=".handle">
            <div
              className="card"
              style={{
                position: "absolute",
                width: "50%",
                zIndex: "999",
                backgroundColor: "white",
                borderRadius: "10px",
                boxShadow: "2px 2px 20px grey",
                marginRight: "200px",
                marginTop: "-40px",
                top: "43px",
                left: "60px",
              }}
            >
              <div
                className="card-header handle"
                style={{ backgroundColor: "#0b3363" }}
              >
                <h4 style={{ fontSize: "50px", color: "white" }}>
                  <strong>Edit User</strong>
                </h4>
                <div>
                  <AiOutlineCloseCircle
                    onClick={() => {
                      setEditUserModal(false);
                    }}
                    size="20px"
                    style={{
                      float: "right",
                      marginTop: "-25px",
                      color: "white",
                      cursor: "pointer",
                    }}
                  />
                </div>
              </div>

              <div className="card-body">
                <div className="row g-2">
                  <div className="col-md-6">
                    <div className="form-floating">
                      <input
                        type="text"
                        className="form-control"
                        id="floatingInputGrid"
                        value={editusername}
                        onChange={(e) => seteditusername(e.target.value)}
                      />
                      <label for="floatingInputGrid">Name</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-floating">
                      <input
                        type="email"
                        className="form-control"
                        id="floatingInputGrid"
                        value={editemail}
                        onChange={(e) => seteditemail(e.target.value)}
                      />
                      <label for="floatingInputGrid">Email address</label>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <div className="form-floating">
                      <input
                        type="text"
                        className="form-control"
                        id="floatingInputGrid"
                        value={editmobile}
                        onChange={(e) => seteditmobile(e.target.value)}
                      />
                      <label for="floatingInputGrid">Phone</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-floating">
                      <input
                        type="phone"
                        onkeydown="return event.keyCode !== 69"
                        className="form-control"
                        id="floatingInputGrid"
                        value={editwhatsapp}
                        onChange={(e) =>
                          seteditwhatsapp(e.target.value.replace(/[^\d]/, ""))
                        }
                      />
                      <label for="floatingInputGrid">Whatsapp</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-floating">
                      <input
                        type="text"
                        className="form-control"
                        id="floatingInputGrid"
                        value={editfacebook}
                        onChange={(e) => seteditfacebook(e.target.value)}
                      />
                      <label for="floatingInputGrid">facebook</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-floating">
                      <input
                        type="email"
                        className="form-control"
                        id="floatingInputGrid"
                        value={edittwitter}
                        onChange={(e) => setedittwitter(e.target.value)}
                      />
                      <label for="floatingInputGrid">Twitter</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-floating">
                      <input
                        type="text"
                        className="form-control"
                        id="floatingInputGrid"
                        value={editteams}
                        onChange={(e) => seteditteams(e.target.value)}
                      />
                      <label for="floatingInputGrid">Teams</label>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-floating">
                      <input
                        type="text"
                        className="form-control"
                        id="floatingInputGrid"
                        value={editcompany}
                        onChange={(e) => seteditcompany(e.target.value)}
                      />
                      <label for="floatingInputGrid">Company</label>
                    </div>
                  </div>
                  <div className="col-md-12">
                    <div className="form-floating">
                      <input
                        type="text"
                        className="form-control"
                        id="floatingInputGrid"
                        value={editaddress}
                        onChange={(e) => seteditaddress(e.target.value)}
                      />
                      <label for="floatingInputGrid">Address</label>
                    </div>
                  </div>
                </div>
                <div>
                  <Button
                    className="btn btn-primary btn-md float-end"
                    onClick={() => {
                      setEditUserModal(false);
                      updateEditvalue();
                    }}
                  >
                    Update
                  </Button>
                </div>
              </div>
            </div>
          </Draggable>
        )}

        {props.selectedchannel == "email" ? (
          <div
            className="p-1 shadow h-100"
            style={{
              width: props.togglechat ? "49%" : "98%",
              paddingLeft: props.togglechat ? "20px" : "",
            }}
          >
            {props.iswrite ? (
              <>
                <div className="">
                  <div className="float-right my-1">
                    <BiArrowFromRight
                      size={25}
                      onClick={handelCloseReplayEmail}
                      style={{ cursor: "pointer" }}
                    />
                  </div>
                </div>

                <NewEmail
                  action={props}
                  inboxContent={
                    isReplyEmail ? props.selectedchanneldata.Email : null
                  }
                  agentId={
                    props.selectedchanneldata.Email.agentID
                      ? props.selectedchanneldata.Email.agentID
                      : []
                  }
                  toSendlist={isReplyEmail ? toSendlist : []}
                  toCCSendlist={isReplyEmail ? toCCSendlist : []}
                  toBCCSendlist={isReplyEmail ? toBCCSendlist : []}
                  sendMail={SendEmail}
                  closeReplaypage={handelCloseReplayEmail}
                  attachmentUrl={isReplyEmail ? props.attachmentUrl : ""}
                />
              </>
            ) : (
              <div style={{ width: props.togglechat ? "65%" : "100%" }}>
                {props.selectedchanneldata.length != 0 ? (
                  <div
                    className="email-desc-wrapper"
                    style={{ minWidth: "55rem" }}
                  >
                    <div className="title-subject">
                      <h6>{props.selectedchanneldata.subject}</h6>
                    </div>
                    <Accordion
                      style={{ width: props.togglechat ? "53%" : "100%" }}
                    >
                      <AccordionSummary
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                      >
                        <Typography>
                          <div className="email-header d-flex w-100">
                            <img src={avatar} alt="Profile Picture" />
                            <div className="address mx-2 my-2 ">
                              <p className="recipient" id="from">
                                {" "}
                                {console.log(
                                  "fromlist<<<<<<<<<>>>>",
                                  props.selectedchanneldata.Email.fromList
                                )}{" "}
                              </p>
                              <p className="recipient" id="to">
                                <span>
                                  To:{props.selectedchanneldata.Email.toList}
                                </span>
                              </p>
                              {props.selectedchanneldata.ccList ? (
                                <p className="recipient" id="cc">
                                  <span>
                                    Cc:{props.selectedchanneldata.Email.ccList}
                                  </span>
                                </p>
                              ) : null}
                              <p className="recipient" id="date">
                                <span>
                                  {moment(
                                    props.selectedchanneldata.Email.receivedTime
                                  ).format("MMMM Do YYYY, h:mm A")}
                                </span>
                              </p>
                            </div>
                          </div>
                        </Typography>
                      </AccordionSummary>
                      <AccordionDetails>
                        {!_.isEmpty(
                          props.selectedchanneldata.Email.attachments
                        ) ? (
                          <>
                            <Grid container spacing={1}>
                              {_.map(
                                _.filter(
                                  props.selectedchanneldata.Email.attachments,
                                  { location: "attachment" }
                                ),
                                (val, i) => {
                                  if (imageFormat.includes(val.extension)) {
                                    return (
                                      <Grid item xs={3} key={i}>
                                        <div
                                          className="attachment_container"
                                          onClick={(e) =>
                                            openActionView(e, val)
                                          }
                                        >
                                          <img
                                            src={val.url}
                                            alt="Avatar"
                                            className="image_attachment"
                                          />
                                          <div className="attachment_overlay">
                                            <div className="attachment_text">
                                              {val.filename}
                                            </div>
                                          </div>
                                        </div>
                                      </Grid>
                                    );
                                  }
                                  return null;
                                }
                              )}
                            </Grid>

                            <Grid container spacing={1}>
                              {_.map(
                                _.filter(
                                  props.selectedchanneldata.Email.attachments,
                                  { location: "attachment" }
                                ),
                                (val, i) => {
                                  if (!imageFormat.includes(val.extension)) {
                                    return (
                                      <Grid item xl={2} key={i}>
                                        <Paper>
                                          <MenuItem>
                                            <ListItemIcon className="attachment_Preview_image">
                                              <img
                                                src={getIcon(val.extension)}
                                                height="20px"
                                                weight="20px"
                                                alt="Icon"
                                              />
                                            </ListItemIcon>
                                            <ListItemText>
                                              <span className="attachment_el_text">
                                                {val.filename}
                                              </span>
                                            </ListItemText>
                                            <Typography
                                              variant="body2"
                                              color="text.secondary"
                                            >
                                              <IconButton
                                                onClick={(e) =>
                                                  openActionView(e, val)
                                                }
                                              >
                                                <ExpandMoreIcon />
                                              </IconButton>
                                            </Typography>
                                          </MenuItem>
                                        </Paper>
                                      </Grid>
                                    );
                                  }
                                  return null;
                                }
                              )}
                            </Grid>
                          </>
                        ) : (
                          console.log("errorconsole..........")
                        )}
                        <Typography>
                          <div className="mailcontent">
                            <div className="email-body">
                              {props.selectedchanneldata.EmailBody
                                ? parse(
                                    props.selectedchanneldata.EmailBody.replace(
                                      /\s+/g,
                                      " "
                                    ).trim()
                                  )
                                : ""}
                            </div>
                            <div className="email-action">
                              <button
                                className="btn btn-base"
                                onClick={onClickReply}
                              >
                                {" "}
                                <img src={reply} /> Reply &nbsp;
                              </button>
                              <button className="btn btn-base">
                                {" "}
                                <img src={replyall} /> Reply All &nbsp;
                              </button>
                              <button className="btn btn-base">
                                {" "}
                                <img src={forward} /> Forward
                              </button>
                            </div>
                          </div>
                        </Typography>
                      </AccordionDetails>
                    </Accordion>

                    <Popover
                      id="simple-popover"
                      open={actionView}
                      anchorEl={anchorEl}
                      onClose={closeActionView}
                      anchorOrigin={{
                        vertical: "bottom",
                        horizontal: "left",
                      }}
                    >
                      <Paper sx={{ width: 200, maxWidth: "100%" }}>
                        <MenuList>
                          <MenuItem onClick={openDocument}>
                            <ListItemIcon>
                              <VisibilityIcon fontSize="small" />
                            </ListItemIcon>
                            <ListItemText>Preview</ListItemText>
                          </MenuItem>
                          <Divider />
                          <MenuItem onClick={openDocument}>
                            <ListItemIcon>
                              <DownloadIcon fontSize="small" />
                            </ListItemIcon>
                            <ListItemText>Download</ListItemText>
                          </MenuItem>
                          <Divider />
                        </MenuList>
                      </Paper>
                    </Popover>
                  </div>
                ) : (
                  <div className="recive-hed" style={{ width: "100%" }}></div>
                )}
              </div>
            )}
          </div>
        ) : (
          <Chat />
        )}

        <Modal
          show={showTransferWhatsapp}
          className="transferCall-modal"
          onHide={handleWhatsappClose}
        >
          <Modal.Header
            // closeButton
            // <AiOutlineCloseCircle  />

            style={{
              padding: "6px 12px",
              margin: 0,
              fontSize: "12px",
              height: "45px",
              backgroundColor: "#294e9f",
              color: "white",
            }}
          >
            <div className="d-flex justify-content-between align-items-center w-100">
              <div>
                <Modal.Title
                  style={{
                    fontSize: 15,
                    margin: "6px 0 0 0",
                    textTransform: "capitalize",
                  }}
                >
                  Transfer to Whatsapp
                </Modal.Title>
              </div>
              <div>
                <AiOutlineCloseCircle onClick={handleWhatsappClose} />
              </div>
            </div>
          </Modal.Header>
          <Modal.Body>
            {" "}
            <div className="my-3">
              <div>
                <input
                  className="form-control form-control-sm"
                  value={phonenumber}
                  onChange={(e) => setPhonenumber(e.target.value)}
                  placeholder="Enter Phonenumber"
                ></input>

                {phonenumbererror ? (
                  <span className="text-danger">Fill the required field *</span>
                ) : (
                  ""
                )}
              </div>
            </div>
          </Modal.Body>
          <div className="transferOkDiv">
            <Button
              variant="primary"
              className="transferOkBtn"
              onClick={() => {
                handleTransferWhatsapp();
                handleWhatsappClose();
              }}
            >
              Transfer
            </Button>
          </div>
        </Modal>

        {/* transfer call */}
        <Modal
          show={showTransferMod}
          className="transferCall-modal"
          onHide={handleTransferClose}
        >
          <Modal.Header
            style={{
              padding: "6px 12px",
              margin: 0,
              fontSize: "12px",
              height: "45px",
              backgroundColor: "#294e9f",
              color: "white",
            }}
          >
            <div className="d-flex justify-content-between align-items-center w-100">
              <div>
                <Modal.Title
                  style={{
                    fontSize: 15,
                    margin: "6px 0 0 0",
                    textTransform: "capitalize",
                  }}
                >
                  {checkWhichCall} Chat to Supervisor
                </Modal.Title>
              </div>
              <div>
                <AiOutlineCloseCircle onClick={handleTransferClose} />
              </div>
            </div>
          </Modal.Header>
          <Modal.Body>
            {" "}
            <select
              className="form-select form-select-sm"
              name="availableAgent"
              onChange={(e) => setAgentTransferId(e.target)}
            >
              <option value="agent" selected>
                Available Supervisor
              </option>
              {availableAgent.map((agents) => {
                return (
                  <option value={agents.id}>
                    {agents.username ? agents.username : agents.firstName}
                  </option>
                );
              })}
            </select>
          </Modal.Body>

          {/* <Button variant="secondary" onClick={handleTransferClose}>
              Close
            </Button> */}
          <div className="transferOkDiv">
            <Button
              variant="primary"
              className="transferOkBtn"
              onClick={transferAgentSubmit}
            >
              {checkWhichCall}
            </Button>
          </div>
        </Modal>

        {/* Transfer to supervisor based on filter value */}

        <Modal
          show={showfiltersupervisor}
          className="transferCall-modal"
          onHide={handleTransfersupervisorClose}
          size="md"
        >
          <Modal.Header
            style={{
              padding: "6px 12px",
              margin: 0,
              fontSize: "12px",
              height: "45px",
              backgroundColor: "#294e9f",
              color: "white",
            }}
          >
            <div className="d-flex justify-content-between align-items-center w-100">
              <div>
                <Modal.Title
                  style={{
                    fontSize: 15,
                    margin: "6px 0 0 0",
                    textTransform: "capitalize",
                  }}
                >
                  {checkWhichCall}
                </Modal.Title>
              </div>
              <div>
                <AiOutlineCloseCircle onClick={handleTransfersupervisorClose} />
              </div>
            </div>
          </Modal.Header>
          <Modal.Body>
            {" "}
            <div className="d-flex justify-content-between">
              <div className="mx-1">
                <select
                  className="form-select form-select-sm"
                  aria-label="Default select example"
                  onChange={(e) => {
                    setSkilldropdownid(e.target.value);
                    userBasedonLanguageSkillset(e.target.value, "");
                  }}
                >
                  <option selected disabled value="">
                    Choose Skillset
                  </option>
                  {skilldropdown.length > 0 ? (
                    skilldropdown.map((item) => {
                      return (
                        <option id={item.skillId} value={item.skillId}>
                          {item.skillName}
                        </option>
                      );
                    })
                  ) : (
                    <option>No Skillset</option>
                  )}
                </select>
              </div>

              <div>
                <select
                  className="form-select form-select-sm"
                  aria-label="Default select example"
                  onChange={(e) => {
                    setLanguagedropdownid(e.target.value);
                    userBasedonLanguageSkillset("", e.target.value);
                  }}
                >
                  <option selected disabled value="">
                    Choose language
                  </option>
                  {languagedropdown.length > 0 ? (
                    languagedropdown.map((item, id) => {
                      return (
                        <option id={item.languageId} value={item.languageId}>
                          {item.languageDesc}
                        </option>
                      );
                    })
                  ) : (
                    <option>No Language</option>
                  )}
                </select>
              </div>
            </div>
            <select
              className="form-select form-select-sm mt-4"
              name="availableAgent"
              onChange={(e) => setAgentTransferId(e.target)}
            >
              <option value="" selected disabled>
                Available Agent
              </option>

              {agentbasedonfilter.map((item) => {
                return (
                  <option id={item.id} value={item.id}>
                    {item.username}
                  </option>
                );
              })}

              {/* agentbasedonfilter */}
            </select>
          </Modal.Body>

          {/* <Button variant="secondary" onClick={handleTransferClose}>
              Close
            </Button> */}
          <div className="transferOkDiv">
            <Button
              variant="primary"
              className="transferOkBtn"
              onClick={transferAgentSubmit}
            >
              {checkWhichCall}
            </Button>
          </div>
        </Modal>

        {/* Conference to supervisor based on filter value */}

        <Modal
          show={showfilterconferencesupervisor}
          className="transferCall-modal"
          onHide={handleConferencesupervisorClose}
          size="lg"
        >
          <Modal.Header
            // closeButton
            // <AiOutlineCloseCircle  />

            style={{
              padding: "6px 12px",
              margin: 0,
              fontSize: "12px",
              height: "45px",
              backgroundColor: "#294e9f",
              color: "white",
            }}
          >
            <div className="d-flex justify-content-between align-items-center w-100">
              <div>
                <Modal.Title
                  style={{
                    fontSize: 15,
                    margin: "6px 0 0 0",
                    textTransform: "capitalize",
                  }}
                >
                  Conference
                </Modal.Title>
              </div>
              <div>
                <AiOutlineCloseCircle
                  onClick={handleConferencesupervisorClose}
                />
              </div>
            </div>
          </Modal.Header>
          <Modal.Body>
            {" "}
            <div className="d-flex justify-content-between">
              <div>
                <select
                  className="form-select"
                  aria-label="Default select example"
                >
                  <option selected disabled value="">
                    Choose Skillset
                  </option>
                  {skilldropdown.map((item) => {
                    return (
                      <option id={item.skillId} value={item.skillName}>
                        {item.skillName}
                      </option>
                    );
                  })}
                </select>
              </div>

              <div>
                <select
                  className="form-select"
                  aria-label="Default select example"
                >
                  <option selected disabled value="">
                    Choose Skillset
                  </option>
                  {skilldropdown.map((item, id) => {
                    return (
                      <option id={item.skillId} value={item.skillName}>
                        {item.skillName}
                      </option>
                    );
                  })}
                </select>
              </div>
            </div>
            <select className="clientChatLogin mt-4" name="availableAgent">
              <option value="agent" selected>
                Available Agent
              </option>
            </select>
          </Modal.Body>

          <div className="transferOkDiv">
            <Button variant="primary" className="transferOkBtn">
              CONFERENCE
            </Button>
          </div>
        </Modal>

        {/* Conference */}
        <Modal
          show={show}
          onHide={handleClose}
          className="mt-5 modal-content-new"
        >
          <Modal.Header
            style={{
              height: "70px",
              color: "white",
              backgroundColor: "#294e9f",
              alignItems: "center",
              display: "flex",
            }}
          >
            <Modal.Title className="">Conference</Modal.Title>
            <Modal.Title>
              <AiOutlineCloseCircle size={20} onClick={handleClose} />
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Control type="text" placeholder="Select Any Agent" />
            </Form.Group>
          </Modal.Body>
          <div className="d-flex  justify-content-end">
            <Button onClick={handleClose} variant="light">
              May Be later
            </Button>
            <Button variant="primary">Conference</Button>
          </div>
        </Modal>

        {/* Esign Modal */}

        <Modal
          show={esignshow}
          onHide={handleesignShow}
          className="mt-5 modal-content-new "
        >
          <Modal.Header
            style={{
              height: "70px",
              color: "white",
              backgroundColor: "#294e9f",
              alignItems: "center",
              display: "flex",
            }}
          >
            <Modal.Title
              className=""
              style={{ color: "#fff", fontFamily: "Noto-Sans" }}
            >
              E sign
            </Modal.Title>
            <Modal.Title>
              <AiOutlineCloseCircle
                size={20}
                color="white"
                onClick={handleesignClose}
              />
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <label style={{ fontFamily: "Noto-Sans" }}>Title</label>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Control type="text" placeholder="Enter the title" />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Control type="file" placeholder="Select Any File" />
            </Form.Group>
          </Modal.Body>
          <div className="d-flex  justify-content-end">
            <Button
              variant="primary"
              onClick={sendEsignDoc}
              style={{ fontFamily: "Noto-Sans" }}
            >
              Send
            </Button>
          </div>
        </Modal>

        {/* End Chat Modal */}

        <Modal
          size="lg"
          className="end_chat_modal contained-modal-title-vcenter"
          centered
          show={endchatshow}
          onHide={handleEndChatClose}
        >
          <div className="">
            <div>
              <Modal.Header
                className="text-white"
                style={{ background: "#294e9f" }}
              >
                <Modal.Title className="text-white">Wrap Up</Modal.Title>
                <AiOutlineCloseCircle onClick={handleEndChatClose} />
              </Modal.Header>
            </div>
          </div>
          <Modal.Body className="" style={{ height: "15rem" }}>
            <div
              className="form"
              onSubmit={(event) => {
                handleSubmit(event);
              }}
            >
              <select
                className="form-select"
                aria-label="Default select example"
                onChange={(e) => setReason(e.target.value)}
              >
                <option selected>Select reason</option>
                <option value="Disconnected">Disconnected</option>
                <option value="Follow Up">Follow Up</option>
                <option value="Technical Issue">Technical Issue</option>
              </select>
              {reasonerror ? (
                <span className="text-danger">Choose a reason*</span>
              ) : (
                ""
              )}
            </div>
            <div className="my-3">
              <div className="form-floating">
                <input
                  className="form-control"
                  onChange={(e) => setComments(e.target.value)}
                  placeholder="Leave a comment here"
                  id="floatingTextarea2"
                  onKeyPress={(event) => {
                    handleKeyPress(chatid, event);
                  }}
                  style={{ height: "100px" }}
                ></input>
                <label for="floatingTextarea2">Comments</label>
                {commentserror ? (
                  <span className="text-danger">Fill the required field *</span>
                ) : (
                  ""
                )}
              </div>
              {props.selectedchannel == "email" ? (
                <div class="form-check my-2">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    onChange={() => handelChecked(event)}
                    value=""
                    id="flexCheckDefault"
                  />

                  <label class="form-check-label" for="flexCheckDefault">
                    Close Ticket
                  </label>
                </div>
              ) : (
                ""
              )}
            </div>
          </Modal.Body>
          <div className="container">
            <div className="ms-auto d-flex justify-content-end">
              <Button variant="secondary" onClick={handleEndChatClose}>
                Close
              </Button>
              <Button
                variant="primary"
                // onKeyPress={handleKeyPress}
                onClick={endChat(chatid)}
              >
                Submit
              </Button>
            </div>
          </div>
        </Modal>

        <Modal
          show={show}
          onHide={handleClose}
          className="mt-5 chat_modal"
          size="xl"
          aria-labelledby="example-modal-sizes-title-lg"
        >
          <Modal.Header
            style={{
              height: "42px",
              color: "white",
              position: "relative",
              top: "-1px",
              backgroundColor: "#294e9f",
              alignItems: "center",
              display: "flex",
              zIndex: "99999",
            }}
          >
            <Modal.Title style={{ color: "#fff" }}>Summary</Modal.Title>
            <Modal.Title>
              <AiOutlineCloseCircle
                size={20}
                style={{ color: "#fff", cursor: "pointer" }}
                onClick={handleClose}
              />
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div
              className="d-flex justify-content-between"
              style={{ height: "5rem" }}
            >
              <div className="text-primary font-weight-bold">
                Chat Arrival At
                <div className="mt-4  font-weight-normal text-dark">
                  {moment(summaryArrivalDate).format("lll")}
                </div>
              </div>
              <div className="text-primary font-weight-bold">
                Chat Started At
                <div className="mt-4  font-weight-normal text-dark">
                  {moment(summaryStartedDate).format("lll")}
                </div>
              </div>
              <div className="text-primary font-weight-bold">
                Chat End At
                <div className="mt-4  font-weight-normal text-dark">
                  {summaryEndDate ? moment(summaryEndDate).format("lll") : "--"}
                </div>
              </div>

              <div className="text-primary font-weight-bold">
                Channel
                <div className="mt-4 font-weight-normal text-dark">
                  {summaryChannel}
                </div>
              </div>
              <div className="text-primary font-weight-bold">
                Agent
                <div className="mt-4  font-weight-normal text-dark">
                  {summaryAgent}
                </div>
              </div>

              <div className="text-primary font-weight-bold">
                View
                <div className="mt-4 d-flex mx-2">
                  <BsChatSquareText
                    onClick={() => {
                      handlechatShow();
                    }}
                    style={{ cursor: "pointer" }}
                  />
                </div>
              </div>
            </div>
          </Modal.Body>
          <div className="d-flex  justify-content-end">
            <Button onClick={handleClose} variant="primary">
              Close
            </Button>
          </div>
        </Modal>

        <Modal show={chatmodal} onHide={handlechatClose}>
          <Modal.Header
            style={{
              height: "55px",
              backgroundColor: "#294e9f",
              color: "white",
              marginTop: "-1px",
              width: "349px",
            }}
          >
            <div className="d-flex w-100 justify-content-between align-items-center">
              <Modal.Title style={{ border: "black", color: "#fff" }}>
                Conversation
              </Modal.Title>
              <span>
                <AiOutlineCloseCircle
                  size={18}
                  onClick={handlechatClose}
                  style={{ cursor: "pointer" }}
                />
              </span>
            </div>
          </Modal.Header>
          <Modal.Body
            style={{ height: "25rem", backgroundColor: "#efeae2" }}
            className="chat_div"
          >
            <div className="">
              <div>
                {chatHistory.map((message, index) => {
                  // console.log('chatHistory',message)

                  return (
                    <div className="w-100" style={{ overflow: "hidden" }}>
                      <div className="w-100">
                        {message.msg_sent_type == "NOTIFICATIONS" && (
                          <div className="msgNotify_client msgNotify_sum">
                            <span
                              style={{
                                position: "relative",
                                bottom: "8px",
                                fontSize: "10px",
                              }}
                            >
                              {message.message}
                            </span>
                          </div>
                        )}

                        {message.msg_sent_type != "NOTIFICATIONS" && (
                          <div>
                            {" "}
                            <div
                              className={`message ${
                                !message.fromSelf ? "left_chat" : "right_chat"
                              }`}
                              style={{
                                marginTop: `${index != 0 ? "20px" : ""}`,
                              }}
                            >
                              <span
                                style={{
                                  fontSize: "10px",
                                  fontWeight: "bolder",
                                }}
                              >
                                {message.senderName}
                              </span>

                              <p>
                                {message.msg_sent_type == "TEXT" ? (
                                  <p
                                    className={`message ${
                                      !message.fromSelf
                                        ? "left_chat_text"
                                        : "right_chat_text"
                                    }`}
                                  >
                                    {message.message}
                                  </p>
                                ) : message.msg_sent_type == "VIDEO" ? (
                                  <video width="140" height="100" controls>
                                    <source
                                      src={message.message}
                                      type="video/mp4"
                                    ></source>
                                  </video>
                                ) : message.msg_sent_type == "AUDIO" ? (
                                  <audio
                                    controls
                                    src={message.message}
                                    style={{ width: "100%" }}
                                  ></audio>
                                ) : message.msg_sent_type == "APPLICATION" ? (
                                  <a
                                    href={message.message}
                                    style={{ color: "blue" }}
                                    target="_blank"
                                  >
                                    {message.file_name}
                                  </a>
                                ) : message.msg_sent_type == "IMAGE" ? (
                                  <div className="d-flex">
                                    <img
                                      className="mx-auto"
                                      src={message.message}
                                      alt="no img"
                                    />
                                  </div>
                                ) : (
                                  <div></div>
                                )}
                              </p>
                            </div>
                            <span
                              className={` ${
                                !message.fromSelf
                                  ? "left_chat-time"
                                  : "right_chat-time"
                              }`}
                            >
                              {moment(message.time).format("lll")}
                            </span>
                          </div>
                        )}
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </Modal.Body>
        </Modal>

        {/* logout modal for session expired */}

        <Modal
          show={sessionexpiredshow}
          onHide={handleClosesessionexpired}
          centered
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header style={{ height: "3rem", backgroundColor: "#294e9f" }}>
            <div className="d-flex justify-content-center align-items-center color-white fw-bold">
              <span>Alert</span>
            </div>
          </Modal.Header>
          <Modal.Body className="fw-bold">
            Session Expired Please Login Again !
          </Modal.Body>

          <button
            className="btn btn-danger w-25 btn-sm ms-auto"
            onClick={logout}
          >
            Logout
          </button>
        </Modal>

        {/* escalationModal  */}

        <Modal
          show={escalationModal}
          className="transferCall-modal"
          onHide={handleCloseEscalationModal}
          size="md"
        >
          <Modal.Header
            style={{
              padding: "6px 12px",
              margin: 0,
              fontSize: "12px",
              height: "45px",
              backgroundColor: "#294e9f",
              color: "white",
            }}
          >
            <div className="d-flex justify-content-between align-items-center w-100">
              <div>
                <Modal.Title
                  style={{
                    fontSize: 15,
                    margin: "6px 0 0 0",
                    textTransform: "capitalize",
                  }}
                >
                  Reassign To Supervisor
                </Modal.Title>
              </div>
              <div>
                <AiOutlineCloseCircle
                  onClick={handleCloseEscalationModal}
                  style={{ cursor: "pointer" }}
                />
              </div>
            </div>
          </Modal.Header>
          <Modal.Body>
            {" "}
            <div className="d-flex justify-content-between"></div>
            <select
              className="form-select form-select-sm mt-2"
              name="availableAgent"
              value={selectedValue}
              onChange={(e) => setAvailableAgent(e.target.value)}
            >
              <option value="" selected disabled>
                Available Supervisor
              </option>

              {availableAgent.map((item) => {
                return (
                  <option id={item.id} value={item.user_id}>
                    {item.username}
                  </option>
                );
              })}
            </select>
          </Modal.Body>

          <div className="transferOkDiv">
            <Button
              variant="primary"
              className="transferOkBtn"
              onClick={escalationSubmitButton}
            >
              Reassign To Supervisor
            </Button>
          </div>
        </Modal>
      </div>
    </>
  );
}

export default connect(mapStateToProps, {
  settogglechat,
  setrefreshtogglechat,
  sendMessage,
  setConferenceNotification,
  setConferenceNotificationVal,
  setSelectedColor,
  setchatid,
  setselectedemail,
  setselectedusername,
  setselectedcompany,
  setselectedadress,
  setselectedmobile,
  setselectedwhatsapp,
  setselectedfacebook,
  setselectedtwitter,
  setselectedteams,
  setselectedid,
  setSelectedchannel,
  setSelectedchanneldata,
  openReplyEmail,
  setUpdatechanneldata,
  setcontactlist,
  setSendattachmentUrl,
  setshowKnowlegeguidecomponent,
  setIswrite,
})(SpaceContent);
