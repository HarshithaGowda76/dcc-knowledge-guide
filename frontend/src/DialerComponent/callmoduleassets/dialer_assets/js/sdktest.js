var callObj1 = null,
  callObj2 = null;
var callmap = {};
var cli;
//1. Initializing the SDK
try {
  cli = new AWL.client();
} catch (err) {
  location.reload(true);
}

var audio = new Audio("https://apps.inaipi.in/dailer/js/telephone_ring.mp3");
$(document).ready(function () {
  cli.enableLogging();
  var appInstanceId = cli.generateAppInstanceID();
  console.log("The ApplicationInstanceID: " + appInstanceId);

  var cfg = {
    serviceType: "phone",
    enableVideo: true,
    Gateway: { ip: "clouducc.inaipi.me", port: "5061" },
    Stunserver: { ip: "", port: "3478" },
    Turnserver: { ip: "", port: "3478", user: "5104", pwd: "Avaya1234$" },
    AppData: {
      applicationID: "xxxx12345",
      applicationUA: "sdktestclient-3.0.0",
      appInstanceID: appInstanceId,
    },
    disableResiliency: false,
  };

  var tags = {
    localVideo: "lclVideo",
    remoteVideo: "rmtVideo",
  };

  var onCallListener = new CallListener();
  /*if(cli.setConfiguration(cfg, onConfigChanged, onRegistrationStateChanged)==="AWL_MSG_SETCONFIG_SUCCESS"){
            console.log("\nSETCONFIG SUCCESS");
        }*/

  if (
    cli.setConfiguration(
      cfg,
      onConfigChanged,
      onRegistrationStateChanged,
      onCallListener,
      onAuthTokenRenewed
    ) === "AWL_MSG_SETCONFIG_SUCCESS"
  ) {
    console.log("\nSETCONFIG SUCCESS");
  }

  if (cli.setDomElements(tags) === "AWL_MSG_SETDOM_FAILED") {
    console.log("\nSETDOM FAILED");
  } else {
    console.log("\nSETDOM PASS");
  }

  cli.logIn("5104", "Avaya1234$", "true");

  $("#deviceList").click(function () {
    cli.getDeviceList(onDeviceListFound);
  });

  $("#deviceSelected").click(function () {
    console.log("Devices Selected ");
    var selectedDeviceIds = {
      audioInputID: $("input[name=audioInput]:checked").val(),
      videoInputID: $("input[name=videoInput]:checked").val(),
      audioOutputID: $("input[name=audioOutput]:checked").val(),
      defaultId: true,
    };

    cli.setDeviceIds(selectedDeviceIds);

    $("#deviceListTable").hide();
    $("#audioInputTable,#videoInputTable,#audioOutputTable")
      .children()
      .remove();
  });

  $("#deviceListClose").click(function () {
    $("#deviceListTable").hide();
    $("#audioInputTable,#videoInputTable,#audioOutputTable")
      .children()
      .remove();
  });

  //first call
  $("#call").click(function () {
    var html = $("#numDisplay").val();

    var dailed_number = html.trim();
    //\swalert(dailed_number);
    if (dailed_number == "") {
      alert("Add number to dial..");
      return false;
    }
    // console.log(dailed_number);
    callObj1 = cli.makeCall(dailed_number, "audio");
    if (callObj1 !== null) {
      console.log("callobj1 callId : " + callObj1.getCallId());
      console.log("callobj1 callState : " + callObj1.getCallState());
      callmap[callObj1.getCallId()] = callObj1;

      // $('.container-wa').hide();
      // $('.container-asw').show();
      // $('#dialed_num').html(dailed_number);
      // startTimer();
    } else {
      alert("Error! Cannot make calls, try after some time");
    }
  });

  $("#end").click(function () {
    cli.dropCall(callObj1.getCallId());
    clearTimeout(timex);
    $(".container-asw").hide();
    $(".container-wa").removeClass("is-opened");
    $("#numDisplay").val("");
    $(".container-wa").show();
    var number = $("#dialed_num").text();
    var timer = $("#timer").text().trim();
    insert_call("outgoing", number, timer);
    location.reload(true);
    location.reload(true);
  });

  $("#end_inc").click(function () {
    cli.dropCall(callObj1.getCallId());
    clearTimeout(timex);
    $(".container-asw-inc").hide();
    $(".container-asw").hide();
    $(".container-wa").removeClass("is-opened");
    $("#numDisplay").val("");
    $(".container-wa").show();
    var number = $("#inc_callednumber").text();
    var timer = $("#timer_inc").text().trim();
    insert_call("incoming", number, timer);
    location.reload(true);
  });

  $("#reject").click(function () {
    audio.pause();
    $("#reject_val").val("1");
    cli.dropCall(callObj1.getCallId());
    // var number = $('#inc_callednumber').text();
  });

  $("#mute").click(function () {
    if (cli.doMute(callObj1.getCallId())) {
      console.log("Mute SUCCESS");
    }
    $("#mute").hide();
    $("#unmute").show();
  });

  $("#mute_inc").click(function () {
    if (cli.doMute(callObj1.getCallId())) {
      console.log("Mute SUCCESS");
    }
    $("#mute_inc").hide();
    $("#unmute_inc").show();
  });

  $("#unmute").click(function () {
    if (cli.doUnMute(callObj1.getCallId())) {
      console.log("UNmute SUCCESS");
    }
    $("#unmute").hide();
    $("#mute").show();
  });

  $("#unmute_inc").click(function () {
    if (cli.doUnMute(callObj1.getCallId())) {
      console.log("UNmute SUCCESS");
    }
    $("#unmute_inc").hide();
    $("#mute_inc").show();
  });

  $("#hold").click(function () {
    cli.doHold(callObj1.getCallId());
    $("#hold").hide();
    $("#unhold").show();
  });

  $("#unhold").click(function () {
    cli.doUnHold(callObj1.getCallId());
    $("#unhold").hide();
    $("#hold").show();
  });

  $("#hold_inc").click(function () {
    cli.doHold(callObj1.getCallId());
    $("#hold_inc").hide();
    $("#unhold_inc").show();
  });

  $("#unhold_inc").click(function () {
    cli.doUnHold(callObj1.getCallId());
    $("#unhold_inc").hide();
    $("#hold_inc").show();
  });

  $("#pauseVid").click(function () {
    if (cli.pauseVideo(callObj1.getCallId())) {
      console.log("video pause SUCCESS");
    }
  });

  $("#resumeVid").click(function () {
    if (cli.playVideo(callObj1.getCallId())) {
      console.log("video resume SUCCESS");
    }
  });

  $("#unattendedTransfer").click(function () {
    cli.transferCall("7006", callObj1.getCallId(), "unAttended");
  });

  $("#attendedTransfer").click(function () {
    cli.transferCall(callObj1.getCallId(), callObj2.getCallId(), "attended");
  });

  $("#attendedTransfer1").click(function () {
    cli.transferCall(callObj1.getCallId(), callObj3.getCallId(), "attended");
  });

  $("#attendedTransfer2").click(function () {
    cli.transferCall(callObj2.getCallId(), callObj3.getCallId(), "attended");
  });

  $("#d1").click(function () {
    cli.sendDTMF(callObj1.getCallId(), 1);
  });

  $("#d9").click(function () {
    cli.sendDTMF(callObj1.getCallId(), 9);
  });

  $("#dA").click(function () {
    cli.sendDTMF(callObj1.getCallId(), "A");
  });

  $("#answer").click(function () {
    audio.pause();
    cli.answerCall(callObj1.getCallId());
    $(".container-inc").hide();
    $(".container-asw-inc").show();
    startTimer_inc();
    $("#incoming_dailed_timer").html(callObj1.getFarEndNumber());
  });

  $("#callUpgrade").click(function () {
    cli.addVideo(callObj1.getCallId());
  });

  $("#callDowngrade").click(function () {
    cli.removeVideo(callObj1.getCallId());
  });

  //second call
  $("#call2").click(function () {
    callObj2 = cli.makeCall("7004", "video");
    if (callObj2 !== null) {
      console.log("callObj2 callId : " + callObj2.getCallId());
      callmap[callObj2.getCallId()] = callObj2;
    } else {
      alert("Error! Cannot make calls, try after sometime");
    }
  });

  $("#end2").click(function () {
    cli.dropCall(callObj2.getCallId());
  });

  $("#mute2").click(function () {
    if (cli.doMute(callObj2.getCallId())) {
      console.log("Mute SUCCESS");
    }
  });

  $("#unmute2").click(function () {
    if (cli.doUnMute(callObj2.getCallId())) {
      console.log("Unmute SUCCESS");
    }
  });

  $("#hold2").click(function () {
    cli.doHold(callObj2.getCallId());
  });

  $("#unhold2").click(function () {
    cli.doUnHold(callObj2.getCallId());
  });

  $("#answer2").click(function () {
    cli.answerCall(callObj2.getCallId());
  });

  //third call
  $("#call3").click(function () {
    callObj3 = cli.makeCall("4006", "video");
    if (callObj3 !== null) {
      console.log("callObj3 callId : " + callObj3.getCallId());
      callmap[callObj3.getCallId()] = callObj3;
    } else {
      alert("Error! Cannot make calls, try after sometime");
    }
  });

  $("#end3").click(function () {
    cli.dropCall(callObj3.getCallId());
  });

  $("#mute3").click(function () {
    if (cli.doMute(callObj3.getCallId())) {
      console.log("Mute SUCCESS");
    }
  });

  $("#unmute3").click(function () {
    if (cli.doUnMute(callObj3.getCallId())) {
      console.log("Unmute SUCCESS");
    }
  });

  $("#hold3").click(function () {
    cli.doHold(callObj3.getCallId());
  });

  $("#unhold3").click(function () {
    cli.doUnHold(callObj3.getCallId());
  });

  $("#answer3").click(function () {
    cli.answerCall(callObj3.getCallId());
  });

  $("#logout").click(function () {
    cli.logOut();
  });
});

$(".container-asw").css("display", "none");
$(".container-inc").hide();
$(".container-asw-inc").css("display", "none");
$(".container-wa-out").hide();

$("#call_details_1").hide();
$("#unmute").hide();
$("#unmute_inc").hide();
$("#unhold_inc").hide();
$("#unhold").hide();
//$('#call_details_1').hide();
$("#reject_val").val("");
var hours = 0;
var hours_inc = 0;
var mins = 0;
var mins_inc = 0;
var seconds = 0;
var seconds_inc = 0;
function startTimer() {
  timex = setTimeout(function () {
    seconds++;
    if (seconds > 59) {
      seconds = 0;
      mins++;
      if (mins > 59) {
        mins = 0;
        hours++;
        if (hours < 10) {
          $("#hours").text("0" + hours + ":");
        } else $("#hours").text(hours + ":");
      }
      if (mins < 10) {
        $("#mins").text("0" + mins + ":");
      } else $("#mins").text(mins + ":");
    }
    if (seconds < 10) {
      $("#seconds").text("0" + seconds);
    } else {
      $("#seconds").text(seconds);
    }
    startTimer();
  }, 1000);
}

function startTimer_inc() {
  timex = setTimeout(function () {
    seconds_inc++;
    if (seconds_inc > 59) {
      seconds_inc = 0;
      mins_inc++;
      if (mins_inc > 59) {
        mins_inc = 0;
        hours_inc++;
        if (hours_inc < 10) {
          $("#hours_inc").text("0" + hours_inc + ":");
        } else $("#hours_inc").text(hours_inc + ":");
      }
      if (mins_inc < 10) {
        $("#mins_inc").text("0" + mins_inc + ":");
      } else $("#mins_inc").text(mins_inc + ":");
    }
    if (seconds_inc < 10) {
      $("#seconds_inc").text("0" + seconds_inc);
    } else {
      $("#seconds_inc").text(seconds_inc);
    }
    startTimer_inc();
  }, 1000);
}

var count = 0;

$(".num").on("click", function () {
  var num = $(this).clone().children().remove().end().text();
  if (count < 11) {
    // $("#numDisplay").append(num.trim());
    var dialed = $("#numDisplay").val();
    $("#numDisplay")
      .val(dialed + num.trim())
      .css({ float: "right" });
    count++;
  }
});

$(".del").on("click", function () {
  var num = $("#numDisplay").val().slice(0, -1);
  $("#numDisplay").val(num);
  // $('#numDisplay last-child').remove();
  // count--;
});

function onConfigChanged(resp) {
  console.log("\n onConfigChanged :: RESULT = " + resp.result);
  console.log("\n onConfigChanged :: reason = " + resp.reason);
}

function onRegistrationStateChanged(resp) {
  console.log("\n onRegistrationStateChange :: RESULT = " + resp.result);
  console.log("\n onRegistrationStateChange :: reason = " + resp.reason);
  if (resp.result === "AWL_MSG_LOGIN_SUCCESS") {
    $("#login").hide();
    $("#1stcall").show();
    $("#1stcall").css("display", "inline-block");
    $("#dtmfpad").show();
    $("#dtmfpad").css("display", "inline-block");
    $("#deviceList").show();
    $("#deviceList").css("display", "block");
    var alternateServer = cli.getAlternateServerConfig();
    if (alternateServer !== null) {
      console.log(
        "Alternate Server:: IP : " +
          alternateServer.address +
          "\tDomain: " +
          alternateServer.domain +
          "\tPort: " +
          alternateServer.port +
          "\tServer Type: " +
          alternateServer.serverType
      );
    } else {
      console.log(
        "Resiliency is either not supported or not enabled at the server"
      );
    }
  } else if (
    resp.result === "AWL_MSG_LOGIN_WEBSOCKET_FAILURE" ||
    resp.result === "AWL_MSG_LINK_ISSUE_DETECTED"
  ) {
    location.reload(true);
  } else if (
    resp.result === "AWL_MSG_FAILING_OVER" ||
    resp.result === "AWL_MSG_FAILING_BACK"
  ) {
    $("#1stcall").hide();
    $("#2ndcall").hide();
    $("#3rdcall").hide();
    alert(resp.reason);
  } else if (
    resp.result === "AWL_MSG_FAIL_OVER_SUCCESS" ||
    resp.result === "AWL_MSG_FAIL_BACK_SUCCESS" ||
    resp.result === "AWL_MSG_RELOGGED_IN"
  ) {
    $("#1stcall").show();
    $("#1stcall").css("display", "inline-block");
    $("#disconnectText").text("");
  } else if (
    resp.result === "AWL_MSG_FAIL_OVER_FAILED" ||
    resp.result === "AWL_MSG_FAIL_BACK_FAILED" ||
    resp.result === "AWL_MSG_LOGIN_FAILED"
  ) {
    $("#1stcall").hide();
    $("#2ndcall").hide();
    $("#3rdcall").hide();
    $("#dtmfpad").hide();
    $("#deviceList").hide();
    alert(resp.reason);
    $("#login").show();
  } else {
    $("#1stcall").hide();
    $("#2ndcall").hide();
    $("#3rdcall").hide();
  }
}

function onAuthTokenRenewed(resp) {
  if (resp.result == "AWL_MSG_TOKEN_RENEW_SUCCESS") {
    console.log("\n onAuthTokenRenewed:: Token is successfully renewed");
  } else {
    console.log(
      "\n onAuthTokenRenewed:: Token renewal failed. reason: " + resp.reason
    );
  }
}

function onDeviceListFound(deviceList) {
  console.log("\n onDeviceListFound: " + deviceList);
  $("#deviceListTable").show();
  $("#deviceListTable").css("display", "block");
  $("#audioInputTable,#videoInputTable,#audioOutputTable").children().remove();
  if (deviceList.length !== 0) {
    $.each(deviceList, function (index, value) {
      if (value[0] === "audioinput") {
        $("#audioInputTable").append(
          "<tr><td>" +
            value[1] +
            "</td><td><input type='radio' class='radioButton' style='float:right;' name='audioInput' value=" +
            value[2] +
            " /></td></tr>"
        );
      } else if (value[0] === "videoinput") {
        $("#videoInputTable").append(
          "<tr><td>" +
            value[1] +
            "</td><td><input type='radio' class='radioButton' name='videoInput' value=" +
            value[2] +
            " /></td></tr>"
        );
      } else if (value[0] === "audiooutput") {
        $("#audioOutputTable").append(
          "<tr><td>" +
            value[1] +
            "</td><td><input type='radio' class='radioButton' name='audioOutput' value=" +
            value[2] +
            " /></td></tr>"
        );
      }
    });
  }
}

var CallListener = function () {
  var _onNewIncomingCall = function (callId, callObj, autoAnswer) {
    console.log("onCallListener : onNewIncomingCall");
    console.log(
      "onNewIncomingCall : getFarEndNumber = " + callObj.getFarEndNumber()
    );
    console.log(
      "onNewIncomingCall : getFarEndName = " + callObj.getFarEndName()
    );
    console.log("onNewIncomingCall : getSipUri = " + callObj.getSipUri());
    console.log("onNewIncomingCall : autoAnswer = " + autoAnswer);

    $(".container-inc").show();

    //         var audioElement = document.createElement('AUDIO');
    //         audioElement.setAttribute('src', 'https://apps.inaipi.in/ticketing_demo/public/assets/js/telephone_ring.mp3');
    //         audioElement.addEventListener('ended', function() {
    //             this.play();
    //             console.log("ringing");
    //         }, false);

    audio.play();
    var access_token = localStorage.getItem("token");
    var settings = {
      url: "http://campaign.inaipi.ae/v1/campaignContact/namebyphonenumber",
      method: "POST",
      timeout: 0,
      headers: {
        Authorization:
          "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoic3VwZXJhZG1pbiIsInBhc3N3b3JkIjoic3VwZXJhZG1pbiIsImlkIjoiNjJhYWRmODM2YmJlYzcwNzExNGYxYmRhIn0sImlhdCI6MTY1NjU4OTQzMiwiZXhwIjoxNjU3MTk0MjMyfQ.0ytZPxyctOerblJRIcfhQezkfailOk53sLKgQSCxjVM",
        "Content-Type": "application/json",
      },
      data: JSON.stringify({
        phonenumber: callObj.getFarEndNumber(),
      }),
    };

    $.ajax(settings).done(function (response) {
      if (response.status) {
        $("#incoming_number").html(response.data);
        $("#dialed_num").val(response.data);
      } else {
        $("#incoming_number").html("No Name");
        $("#dialed_num").val("No Name");
      }

      console.log(response);
    });

    //  window.open("https://mycloudcx.com/Salesforce/?phone="+callObj.getFarEndNumber());

    $(".container-wa").hide();

    if (typeof callmap[callId] === "undefined") {
      console.log("\n onCallStateChanged : New incoming CALL OBJECT ADDED");
      if (callObj1 === null) {
        console.log("\n onCallStateChanged : CallObj assigned to callObj1");
        callObj1 = callObj;
        callmap[callObj1.getCallId()] = callObj1;
      } else if (callObj2 === null) {
        console.log("\n onCallStateChanged : CallObj assigned to callObj2");
        callObj2 = callObj;
        callmap[callObj2.getCallId()] = callObj;
      } else {
        console.log("\n onCallStateChanged : ALL LINES BUSY!!");
      }
    }
  };
  var _onCallStateChange = function (callId, callObj, event) {
    console.log("\nSDK TEST: onCallStateChanged: ");
    console.log("\nSDK TEST: call Id " + callObj.getCallId());

    for (var key in callmap) {
      console.log("callMap[" + key + "]");
    }

    if (callObj1 != null && callObj.getCallId() === callObj1.getCallId()) {
      console.log("\nSDK TEST: callObj1: Call ID Matched");
      console.log("\n callObj1: callstate: " + callObj1.getCallState());
      switch (callObj1.getCallState()) {
        case "AWL_MSG_CALL_IDLE":
          break;
        case "AWL_MSG_CALL_CONNECTED":
          var dailed_number = $("#numDisplay").val().trim();
          var dailed_name = $("#nameDisplay").val().trim();
          // $('.container-wa').hide();
          $(".container-wa-out").hide();
          $(".container-asw").show();
          if (dailed_name) {
            $("#dialed_num").html(dailed_name);
          } else {
            var value = $("#incoming_number").html();
            $("#dialed_num").html(value);
          }

          startTimer();

          $("#2ndcall").show();
          $("#2ndcall").css("display", "inline-block");
          if ($("#2ndcall").css("display") != "none") {
            $("#3rdcall").css("display", "inline-block");
            $("#3rdcall").show();
          }
          break;
        case "AWL_MSG_CALL_RINGING":
          console.log("Call Update Presence API");
          var access_token = localStorage.getItem("token");
          var settings = {
            url: "http://campaign.inaipi.ae/v1/user/updateUserpresenceStatus",
            method: "POST",
            timeout: 0,
            headers: {
              Authorization:
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoiQWdlbnRUZXN0IiwicGFzc3dvcmQiOiIxMjM0IiwicGhvbmVudW1iZXIiOjExMTExMTExMTEsImV4dGVuc2lvbiI6MjMwOCwiZXh0ZW5zaW9ucGFzc3dvcmQiOiJxd2VydHkiLCJyb2xlX2lkIjoiNjJiMjk1OTJhZGI0ZjA0MDc3ZjE2OTAzIiwiaXNsb2dnZWRpbiI6dHJ1ZSwicHJlc2VuY2VzdGF0dXMiOiJJbiBDYWxsIiwiaXNkZWxldGVkIjpmYWxzZSwiY3JlYXRlZF9BdCI6IjIwMjItMDYtMjFUMDU6NTU6MDIuNDg1WiIsInN1cGVydmlzb3JfaWQiOiI2MmIyOTVmYWFkYjRmMDQwNzdmMTY5MDciLCJpZCI6IjYyYjI5NjMwYWRiNGYwNDA3N2YxNjkwOSJ9LCJpYXQiOjE2NTY0ODM3MzEsImV4cCI6MTY1NzA4ODUzMX0.So5E5O0DQSB7v1Qx8ELX04yqpyPW4qR24IRgWyMkdZQ",
              "Content-Type": "application/json",
            },
            data: JSON.stringify({
              presencestatus: "In Call",
            }),
          };

          $.ajax(settings).done(function (response) {
            console.log(response);
          });

          var dailed_number_out = $("#numDisplay").val().trim();

          var dailed_name_out = $("#nameDisplay").val().trim();
          $(".container-wa").hide();
          $(".container-wa-out").show();
          $("#outgoing_number").html(dailed_name_out);
          break;
        case "AWL_MSG_CALL_DISCONNECTED":
          //hided

          if ($("#reject_val").val() == 1) {
            //reject
            var number = $("#incoming_number").text();
            insert_call("reject", number, "");
          }
          $(".container-inc").hide();
          $(".container-asw").hide();
          $(".container-wa").show();
          var access_token = localStorage.getItem("token");
          var settings = {
            url: "http://campaign.inaipi.ae/v1/user/updateUserpresenceStatus",
            method: "POST",
            timeout: 0,
            headers: {
              Authorization:
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoiQWdlbnRUZXN0IiwicGFzc3dvcmQiOiIxMjM0IiwicGhvbmVudW1iZXIiOjExMTExMTExMTEsImV4dGVuc2lvbiI6MjMwOCwiZXh0ZW5zaW9ucGFzc3dvcmQiOiJxd2VydHkiLCJyb2xlX2lkIjoiNjJiMjk1OTJhZGI0ZjA0MDc3ZjE2OTAzIiwiaXNsb2dnZWRpbiI6dHJ1ZSwicHJlc2VuY2VzdGF0dXMiOiJJbiBDYWxsIiwiaXNkZWxldGVkIjpmYWxzZSwiY3JlYXRlZF9BdCI6IjIwMjItMDYtMjFUMDU6NTU6MDIuNDg1WiIsInN1cGVydmlzb3JfaWQiOiI2MmIyOTVmYWFkYjRmMDQwNzdmMTY5MDciLCJpZCI6IjYyYjI5NjMwYWRiNGYwNDA3N2YxNjkwOSJ9LCJpYXQiOjE2NTY0ODM3MzEsImV4cCI6MTY1NzA4ODUzMX0.So5E5O0DQSB7v1Qx8ELX04yqpyPW4qR24IRgWyMkdZQ",
              "Content-Type": "application/json",
            },
            data: JSON.stringify({
              presencestatus: "Available",
            }),
          };

          $.ajax(settings).done(function (response) {
            $("#numDisplay").val("");

            $("#nameDisplay").val("");
            $("#dialed_num").val("");
            location.reload(true);
          });

          break;
        case "AWL_MSG_CALL_FAILED":
          if (callmap[callObj1.getCallId()] !== null) {
            delete callmap[callObj1.getCallId()];
            callObj1 = "";
          }
          var access_token = localStorage.getItem("token");
          var settings = {
            url: "http://campaign.inaipi.ae/v1/user/updateUserpresenceStatus",
            method: "POST",
            timeout: 0,
            headers: {
              Authorization:
                "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJuYW1lIjoiQWdlbnRUZXN0IiwicGFzc3dvcmQiOiIxMjM0IiwicGhvbmVudW1iZXIiOjExMTExMTExMTEsImV4dGVuc2lvbiI6MjMwOCwiZXh0ZW5zaW9ucGFzc3dvcmQiOiJxd2VydHkiLCJyb2xlX2lkIjoiNjJiMjk1OTJhZGI0ZjA0MDc3ZjE2OTAzIiwiaXNsb2dnZWRpbiI6dHJ1ZSwicHJlc2VuY2VzdGF0dXMiOiJJbiBDYWxsIiwiaXNkZWxldGVkIjpmYWxzZSwiY3JlYXRlZF9BdCI6IjIwMjItMDYtMjFUMDU6NTU6MDIuNDg1WiIsInN1cGVydmlzb3JfaWQiOiI2MmIyOTVmYWFkYjRmMDQwNzdmMTY5MDciLCJpZCI6IjYyYjI5NjMwYWRiNGYwNDA3N2YxNjkwOSJ9LCJpYXQiOjE2NTY0ODM3MzEsImV4cCI6MTY1NzA4ODUzMX0.So5E5O0DQSB7v1Qx8ELX04yqpyPW4qR24IRgWyMkdZQ",
              "Content-Type": "application/json",
            },
            data: JSON.stringify({
              presencestatus: "Available",
            }),
          };

          $.ajax(settings).done(function (response) {
            location.reload(true);
          });
          break;
        case "AWL_MSG_CALL_INCOMING":
          break;
        case "AWL_MSG_CALL_HELD":
          break;
        case "AWL_MSG_CALL_FAREND_UPDATE":
          console.log(
            "onCallStateChange  : getFarEndNumber = " +
              callObj1.getFarEndNumber()
          );
          console.log(
            "onCallStateChange  : getFarEndName = " + callObj1.getFarEndName()
          );
          console.log(
            "onCallStateChange  : getSipUri = " + callObj1.getSipUri()
          );
          break;
        default:
          console.log("\n CallState doesn't match");
      }
    } else if (
      callObj2 != null &&
      callObj.getCallId() === callObj2.getCallId()
    ) {
      console.log("\ncallObj2: Call ID Matched");
      console.log("\n callObj2: callstate: " + callObj2.getCallState());
      switch (callObj2.getCallState()) {
        case "AWL_MSG_CALL_IDLE":
          break;
        case "AWL_MSG_CALL_RINGING":
          break;
        case "AWL_MSG_CALL_CONNECTED":
          $("#attendedTransfer").show();
          break;
        case "AWL_MSG_CALL_DISCONNECTED":
          $("#attendedTransfer").hide();
          $("#2ndcall").hide();
          break;
        case "AWL_MSG_CALL_FAILED":
          break;
        case "AWL_MSG_CALL_INCOMING":
          break;
        case "AWL_MSG_CALL_HELD":
          break;
        case "AWL_MSG_CALL_FAREND_UPDATE":
          console.log(
            "onCallStateChange : getFarEndNumber = " +
              callObj2.getFarEndNumber()
          );
          console.log(
            "onCallStateChange : getFarEndName = " + callObj2.getFarEndName()
          );
          console.log(
            "onCallStateChange : getSipUri = " + callObj2.getSipUri()
          );
          break;
        default:
          console.log("\n CallState doesn't match");
      }
    } else if (
      callObj3 != null &&
      callObj.getCallId() === callObj3.getCallId()
    ) {
      console.log("\ncallObj3: Call ID Matched");
      console.log("\n callObj3: callstate: " + callObj3.getCallState());
      switch (callObj3.getCallState()) {
        case "AWL_MSG_CALL_IDLE":
          break;
        case "AWL_MSG_CALL_RINGING":
          break;
        case "AWL_MSG_CALL_CONNECTED":
          $("#attendedTransfer1").show();
          $("#attendedTransfer2").show();
          break;
        case "AWL_MSG_CALL_DISCONNECTED":
          $(".container-asw-inc").hide();
          $("#attendedTransfer1").hide();
          $("#attendedTransfer2").hide();
          $("#3rdcall").hide();
          //location.reload(true);
          break;
        case "AWL_MSG_CALL_FAILED":
          break;
        case "AWL_MSG_CALL_INCOMING":
          break;
        case "AWL_MSG_CALL_HELD":
          break;
        case "AWL_MSG_CALL_FAREND_UPDATE":
          console.log(
            "onCallStateChange : getFarEndNumber = " +
              callObj3.getFarEndNumber()
          );
          console.log(
            "onCallStateChange : getFarEndName = " + callObj3.getFarEndName()
          );
          console.log(
            "onCallStateChange : getSipUri = " + callObj3.getSipUri()
          );
          break;
        default:
          console.log("\n CallState doesn't match");
      }
    }
    console.log(
      "\nonCallStateChanged: Total Calls = " + Object.keys(callmap).length
    );
  };

  var _onCallTerminate = function (callId, reason) {
    if (callObj1 != null && callObj1.getCallId() === callId) {
      delete callmap[callObj1.getCallId()];
      callObj1 = null;
    } else if (callObj2 != null && callObj2.getCallId() === callId) {
      delete callmap[callObj2.getCallId()];
      callObj2 = null;
    } else if (callObj3 != null && callObj3.getCallId() === callId) {
      delete callmap[callObj3.getCallId()];
      callObj3 = null;
    } else {
      console.log("Call Id doesn't match ");
    }
    console.log("\n callTerminate Reason: " + reason);
    $("#disconnectText").text(reason);
  };

  /*var _onVideoStreamsAvailable = function(callId, localStream, remoteStream){
        console.log("\n onVideoStreamsAvailable: callId: "+ callId);
        if(callId == callObj1.getCallId()){
            cli.setMediaStream("lclVideo",localStream,callId,"localVideo");
            cli.setMediaStream("rmtVideo",remoteStream,callId,"remoteVideo");
        }
    };

    var _onAudioStreamsAvailable = function(callId, localStream, remoteStream){
        console.log("\n onAudioStreamsAvailable: callId: "+ callId);
    };*/
  return {
    onNewIncomingCall: _onNewIncomingCall,
    onCallStateChange: _onCallStateChange,
    onCallTerminate: _onCallTerminate,
    /*onCallTerminate: _onCallTerminate,
        onVideoStreamsAvailable: _onVideoStreamsAvailable,
        onAudioStreamsAvailable: _onAudioStreamsAvailable*/
  };
};
