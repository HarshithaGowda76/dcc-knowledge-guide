import React, { useEffect, useState, useRef } from "react";
import { Avatar, Box, Stack, Typography } from "@mui/material";
import { useStopwatch } from "react-timer-hook";
import PersonIcon from "@mui/icons-material/Person";
import "./callmoduleassets/styles/dialer2.css";
import "./callmoduleassets/styles/style2.css";
import "./callmoduleassets/fonts/fontawesome-free-6.1.1-web/css/all.css";
import "./callmoduleassets/fonts/fonts2.css";
import Draggable from "react-draggable";
import $ from "jquery";
import SockJS from "sockjs-client";
import Stomp from "stompjs";
import axios from "axios";

import { BaseUrl, AvcUrl } from "../containers/Page/Constants/BaseUrl";

const Sdk = () => {
  const tenantId = localStorage.getItem("tenantid");
  // timer
  const { seconds, minutes, hours, start, reset } = useStopwatch({
    autoStart: false,
  });

  // Dialer toggle icons
  const [togglebutton, settogglebutton] = useState(true);

  // numbers
  const [dailedNumber, setDialedNumber] = useState("");
  const [agentDialedNumber, setagentDialedNumber] = useState("");
  const [dailedNumberInc, setDialedNumberInc] = useState("");

  // dialpad
  const [showphonedialer, setshowphonedialer] = useState(false);
  const [showphonedialer2, setShowphonedialer2] = useState(false);
  const [showphonedialer3, setShowphonedialer3] = useState(false);

  // call screens
  const [outgoingscreen, setoutgoingscreen] = useState(false);
  const [outgoingNxtscreen, setoutgoingNxtscreen] = useState(false);

  const [showIncommingscreen, setshowIncommingscreen] = useState(false);

  const [callscreen, setcallscreen] = useState(false);
  const [showIncommingNxtscreen, setshowIncommingNxtscreen] = useState(false);
  const [calledagentscreen, setcalledagentscreen] = useState(false);
  const [confrencescreen, setconfrencescreen] = useState(false);

  const [conferanceCustomerScreen, setconferanceCustomerScreen] =
    useState(false);
  const [conferanceAgentScreen, setconferanceAgentScreen] = useState(false);

  // call Functionality
  const [showmute, setShowmute] = useState(false);
  const [showUnmute, setShowUnmute] = useState(false);
  const [showHold, setShowHold] = useState(false);
  const [showUnHold, setShowUnHold] = useState(false);
  const [agentCallHold, setagentCallHold] = useState(false);
  const [agentCallUnHold, setagentCallUnHold] = useState(false);
  const [transferCall, setTransferCall] = useState(false);
  const [makingTransferCall, setmakingTransferCall] = useState(false);
  const [confrence, setConfrence] = useState(false);
  const [mergecall, setMergeCall] = useState(false);

  // incoming caller contact ID
  const [contactId, setcontactId] = useState();
  const [agentContactId, setagentContactId] = useState();
  const [consultedContactId, setconsultedContactId] = useState();

  const [avayaplatform, setavayaplatform] = useState("ACR");
  const [callId, setCallId] = useState("");
  const [callState, setCallState] = useState("");

  let agentLoginTerminalId = localStorage.getItem("agentLoginTerminalId");

  const [audioStatus, changeAudioStatus] = useState(false);
  const myRef = useRef();

  var soundfile = "https://apps.inaipi.in/dailer/js/telephone_ring.mp3";

  const startAudio = () => {
    myRef.current.play();
    changeAudioStatus(true);
  };

  const pauseAudio = () => {
    console.log("here");
    myRef.current.pause();
    changeAudioStatus(false);
  };

  // use Audio constructor to create HTMLAudioElement
  const audioTune = new Audio(soundfile);

  // variable to play audio in loop
  const [playInLoop, setPlayInLoop] = useState(false);

  useEffect(() => {
    // configuration();
    initiateWebSocket();
  }, []);

  var stompClient;

  const initiateWebSocket = () => {
    let ssToken = localStorage.getItem("ssoToken");
    const customHeaders = {
      terminal: ssToken, //ssoToken
    };

    let socket = new SockJS(AvcUrl + "/ws");
    stompClient = Stomp.over(socket);

    stompClient.connect(customHeaders, onConnected, onError);
  };

  var i = 0;
  const onConnected = () => {
    console.log("Stomp onConnected");
    stompClient.subscribe("/user/terminal/messages", (payload) => {
      const responseData = JSON.parse(payload.body);

      console.log("responseData", responseData);
      if (responseData.event == "RINGING") {
        console.log("RINGING EVENT", responseData);

        if (responseData.callType == "Inbound") {
          setavayaplatform("AACC");
          setDialedNumberInc(responseData.callingDeviceID);
          localStorage.setItem("contactId", responseData.contactId);
          setcontactId(responseData.contactId);
          setshowIncommingscreen(true);
          setshowphonedialer(false);
        } else if (responseData.callType == "Outbound") {
          setavayaplatform("AACC");
          setDialedNumberInc(responseData.callingDeviceID);
          localStorage.getItem("agentContactId", responseData.contactId);
          setagentContactId(responseData.contactId);
          setshowIncommingscreen(true);
          setshowphonedialer(false);
        } else if (responseData.callType == "CONSULT_INIT") {
          setavayaplatform("AACC");
          setDialedNumberInc(responseData.callingDeviceID);
          localStorage.setItem("agentContactId", responseData.contactId);
          setagentContactId(responseData.contactId);
          setshowIncommingscreen(true);
          setshowphonedialer(false);
        }
      }

      if (responseData.event == "ACTIVE") {
        console.log("RINGING EVENT", responseData);
        if (responseData.callType == "Inbound") {
          setavayaplatform("AACC");
          localStorage.setItem("contactId", responseData.contactId);
          setcontactId(responseData.contactId);
        } else if (responseData.callType == "ConsultConference") {
          setavayaplatform("AACC");
          localStorage.setItem("agentContactId", responseData.contactId);
        }
      }

      if (responseData.event == "HELD") {
        console.log("HELD", responseData);
        if (responseData.callType == "Inbound") {
          setavayaplatform("AACC");
          setDialedNumberInc(responseData.callingDeviceID);
          localStorage.getItem("contactId", responseData.contactId);
          setcontactId(responseData.contactId);
          setcallscreen(true);
          setshowIncommingNxtscreen(true);
          setShowHold(false);
          setShowUnHold(true);
          setagentCallHold(true);
          setagentCallUnHold(false);
        } else if (responseData.callType == "ConsultConference") {
          setavayaplatform("AACC");
          setcallscreen(true);
          setshowIncommingNxtscreen(true);
          settransferUnhold(true);
        }
      }

      if (responseData.event == "DROPPED") {
        if (
          localStorage.getItem("AgentContactID") ||
          localStorage.getItem("contactId")
        ) {
          if (responseData.contactId == consultedContactId) {
            setcalledagentscreen(false);
            localStorage.removeItem("AgentContactID");
          } else if (responseData.contactId == contactId) {
            localStorage.removeItem("contactId");
            setshowIncommingNxtscreen(false);
            reset();
            onDisconnectWebSocket();
          }
        } else {
          setcallscreen(false);
          reset();
          window.location.reload(true);
        }
      }
    });
  };

  const onError = (error) => {
    console.log("Stomp Error", error);
  };

  const onDisconnectWebSocket = () => {
    stompClient.disconnect();
  };

  var callObj1 = null,
    callObj2 = null;
  var callmap = {};
  var cli;
  //1. Initializing the SDK
  cli = new AWL.client();
  cli.enableLogging();

  const configuration = () => {
    var appInstanceId = cli.generateAppInstanceID();
    console.log("The ApplicationInstanceID: " + appInstanceId);

    var cfg = {
      serviceType: "phone",
      enableVideo: true,
      Gateway: { ip: "clouducc.inaipi.me", port: "5061" },

      Stunserver: { ip: "", port: "3478" },
      Turnserver: { ip: "", port: "3478", user: "5104", pwd: "Avaya1234$" },
      AppData: {
        applicationID: "xxxx12345",
        applicationUA: "sdktestclient-3.0.0",
        appInstanceID: appInstanceId,
      },
      disableResiliency: false,
    };

    var tags = {
      localVideo: "lclVideo",
      remoteVideo: "rmtVideo",
    };

    setShowDialer(false);
    $("#call_details_1").hide();
    setShowDialerNextScr(false);

    // var onCallListener = onCallListeners;
    var onCallListener = new CallListener();
    // console.log(onCallListener);

    function onAuthTokenRenewed(resp) {
      if (resp.result == "AWL_MSG_TOKEN_RENEW_SUCCESS") {
        console.log("\n onAuthTokenRenewed:: Token is successfully renewed");
      } else {
        console.log(
          "\n onAuthTokenRenewed:: Token renewal failed. reason: " + resp.reason
        );
      }
    }

    function onConfigChanged(resp) {
      console.log("\n onConfigChanged :: RESULT = " + resp.result);
      console.log("\n onConfigChanged :: reason = " + resp.reason);
    }

    function onRegistrationStateChanged(resp) {
      console.log("\n onRegistrationStateChange :: RESULT = " + resp.result);
      console.log("\n onRegistrationStateChange :: reason = " + resp.reason);
      if (resp.result === "AWL_MSG_LOGIN_SUCCESS") {
        var alternateServer = cli.getAlternateServerConfig();

        if (alternateServer !== null) {
          console.log(
            "Alternate Server:: IP : " +
              alternateServer.address +
              "\tDomain: " +
              alternateServer.domain +
              "\tPort: " +
              alternateServer.port +
              "\tServer Type: " +
              alternateServer.serverType
          );
        } else {
          console.log(
            "Resiliency is either not supported or not enabled at the server"
          );
        }
      } else if (
        resp.result === "AWL_MSG_LOGIN_WEBSOCKET_FAILURE" ||
        resp.result === "AWL_MSG_LINK_ISSUE_DETECTED"
      ) {
      }
    }

    if (
      cli.setConfiguration(
        cfg,
        onConfigChanged,
        onRegistrationStateChanged,
        onCallListener,
        onAuthTokenRenewed
      ) === "AWL_MSG_SETCONFIG_SUCCESS"
    ) {
      console.log("\nSETCONFIG SUCCESS");
    }

    if (cli.setDomElements(tags) === "AWL_MSG_SETDOM_FAILED") {
      console.log("\nSETDOM FAILED");
    } else {
      console.log("\nSETDOM PASS");
    }

    cli.logIn("5100", "Avaya1234$", "true");
  };

  //Making Call
  const makeCall = () => {
    if (dailedNumber == "") {
      return alert("Please Enter Valied Number");
    }
    settogglebutton(false);
    setshowphonedialer(false);
    setoutgoingscreen(true);
    let ssToken = localStorage.getItem("ssoToken");
    axios
      .post(
        AvcUrl +
          `/voice/terminal/${agentLoginTerminalId}/createContact/sip:${agentLoginTerminalId}@demoaccs.com/sip:${dailedNumber}@demoaccs.com`,
        {},
        {
          headers: {
            ssoToken: ssToken,
          },
        }
      )
      .then((res) => {
        console.log("MAKECALL", res);
        setcontactId(res.data.data.contactId);
        setavayaplatform("AACC");
      })
      .catch((err) => {
        console.log(err);
      });

    // callObj1 = cli.makeCall(dailedNumber, "audio");

    // if (callObj1 !== null) {
    //   console.log("callobj1 callId : " + callObj1.getCallId());
    //   let call_id = callObj1.getCallId();
    //   setCallId(call_id);
    //   console.log("callobj1 callState : " + callObj1.getCallState());
    //   setCallState(callObj1.getCallState());
    //   callmap[callObj1.getCallId()] = callObj1;
    //   console.log("call connected");
    // } else {
    //   alert("Error! Cannot make calls, try after some time");
    // }
  };
  const endMakeCall = () => {
    axios.post(
      AvcUrl +
        `/voice/terminal/${agentLoginTerminalId}/disconnect/${contactId}`,

      {},
      {
        headers: {
          ssoToken: localStorage.getItem("ssoToken"),
        },
      }
    );
  };

  var CallListener = function () {
    var _onNewIncomingCall = function (callId, callObj, autoAnswer) {
      console.log("onCallListener : onNewIncomingCall");
      console.log(
        "onNewIncomingCall : getFarEndNumber = " + callObj.getFarEndNumber()
      );
      console.log(
        "onNewIncomingCall : getFarEndName = " + callObj.getFarEndName()
      );
      console.log("onNewIncomingCall : getSipUri = " + callObj.getSipUri());
      console.log("onNewIncomingCall : autoAnswer = " + autoAnswer);

      // $(".container-inc").show();
      // alert(callObj.getFarEndNumber());
      setshowphonedialer(false);
      setShowDialerInc(true);
      settogglebutton(true);
      setShowDialerNextScrInc(false);
      setShowDialer(false);
      setShowDialerNextScr(false);
      setCallState(callObj.getCallState());
      let call_id = callObj.getCallId();
      setCallId(call_id);
      startAudio();

      $(".container-wa").hide();
      // $("#incoming_number").html("kjjjj");
      setDialedNumberInc(callObj.getFarEndNumber());

      if (typeof callmap[callId] === "undefined") {
        console.log("\n onCallStateChanged : New incoming CALL OBJECT ADDED");
        if (callObj1 === null) {
          console.log("\n onCallStateChanged : CallObj assigned to callObj1");
          callObj1 = callObj;
          callmap[callObj1.getCallId()] = callObj1;
        } else {
          console.log("\n onCallStateChanged : ALL LINES BUSY!!");
        }
      }
    };

    var _onCallStateChange = function (callId, callObj, event) {
      console.log("\nSDK TEST: onCallStateChanged: ");
      console.log("\nSDK TEST: call Id " + callObj.getCallId());

      for (var key in callmap) {
        console.log("callMap[" + key + "]");
      }

      if (callObj1 != null && callObj.getCallId() === callObj1.getCallId()) {
        console.log("\nSDK TEST: callObj1: Call ID Matched");
        console.log("\n callObj1: callstate: " + callObj1.getCallState());
        switch (callObj1.getCallState()) {
          case "AWL_MSG_CALL_IDLE":
            break;
          case "AWL_MSG_CALL_CONNECTED":
            // window.open(
            //   "https://pnbhflv300.simplecrmdemo.com/app/customer360/Accounts/" +
            //     callObj.getFarEndNumber()
            // );

            var dailed_number = dailedNumber.trim();

            setShowDialer(false);

            // setShowDialerNextScr(true);
            setDialedNumberInc(callObj.getFarEndNumber());
            startTimer();
            $("#dialed_num").html(dailed_number);
            break;
          case "AWL_MSG_CALL_RINGING":
            var dailed_number_out = dailedNumber.trim();
            $(".container-wa").hide();
            // $(".container-wa-out").show();
            setShowDialer(true);
            $("#outgoing_number").html(dailed_number_out);
            break;
          case "AWL_MSG_CALL_DISCONNECTED":
            //hided
            if ($("#reject_val").val() == 1) {
              //reject
              var number = dailedNumberInc.text();
              insert_call("reject", number, "");
            }
            // $(".container-inc").hide();
            setShowDialerInc(false);
            // $(".container-asw").hide();
            setShowDialerNextScr(false);
            $(".container-wa").show();
            window.location.reload();

            break;
          case "AWL_MSG_CALL_FAILED":
            if (callmap[callObj1.getCallId()] !== null) {
              delete callmap[callObj1.getCallId()];
              callObj1 = "";
            }
            break;
          case "AWL_MSG_CALL_INCOMING":
            break;
          case "AWL_MSG_CALL_HELD":
            break;
          case "AWL_MSG_CALL_FAREND_UPDATE":
            console.log(
              "onCallStateChange  : getFarEndNumber = " +
                callObj1.getFarEndNumber()
            );
            console.log(
              "onCallStateChange  : getFarEndName = " + callObj1.getFarEndName()
            );
            console.log(
              "onCallStateChange  : getSipUri = " + callObj1.getSipUri()
            );
            break;
          default:
            console.log("\n CallState doesn't match");
        }
      }
      console.log(
        "\nonCallStateChanged: Total Calls = " + Object.keys(callmap).length
      );
    };

    var _onCallTerminate = function (callId, reason) {
      if (callObj1 != null && callObj1.getCallId() === callId) {
        delete callmap[callObj1.getCallId()];
        callObj1 = null;
      } else {
        console.log("Call Id doesn't match ");
      }
      console.log("\n callTerminate Reason: " + reason);
      $("#disconnectText").text(reason);
    };

    return {
      onNewIncomingCall: _onNewIncomingCall,
      onCallStateChange: _onCallStateChange,
      onCallTerminate: _onCallTerminate,
    };
  };

  const rejectIncCall = () => {
    if (avayaplatform == "AACC") {
      let ssToken = localStorage.getItem("ssoToken");
      axios
        .post(
          AvcUrl +
            `/voice/terminal/${agentLoginTerminalId}/disconnect/${dailedNumberInc}`,
          {},
          {
            headers: {
              ssoToken: ssToken,
            },
          }
        )
        .then((res) => {
          console.log(res);
        })
        .catch((err) => {
          console.log(err);
        });
    }
    pauseAudio();
    cli.dropCall(callId);
    setShowphonedialer2(false);
  };
  const answerIncCall = async (e) => {
    if (avayaplatform == "AACC") {
      pauseAudio();
      let ssToken = localStorage.getItem("ssoToken");
      axios
        .post(
          `${AvcUrl}/voice/terminal/${agentLoginTerminalId}/answer/${contactId}`,
          {},
          {
            headers: {
              ssoToken: ssToken,
            },
          }
        )
        .then(async (res) => {
          if ((res.data.status = "OK")) {
            setshowIncommingscreen(false);
            setavayaplatform("AACC");
            setcallscreen(true);
            setshowIncommingNxtscreen(true);
            setShowHold(true);
            setShowUnHold(false);
            setShowUnHold(false);
            setagentCallUnHold(false);
            setTransferCall(true);
            setmakingTransferCall(false);
            setConfrence(true);
            setMergeCall(false);
            setcalledagentscreen(false);
            setconfrencescreen(false);
            start();
          }
        })
        .catch((err) => {
          console.error(err);
        });
    } else {
      pauseAudio();
      $("#outGoingDialer").css("display", "none !important");
      cli.answerCall(callId);
      setShowDialerNextScrInc(true);
      setShowDialerInc(false);
      startTimer_inc();
      $("#incoming_dailed_timer").html(showDialerNextScrInc);
    }
    let tokenagent = await JSON.parse(localStorage.getItem("tokenAgent"));
    const access_token = localStorage.getItem("access_token");
    var agent_extension = localStorage.getItem("AvayaUsername");
    console.log("hi", tokenagent.id);
    let datas = {
      name: "",
      email_id: "",
      phone: dailedNumberInc,
      channel: "voice",
      latitude: "",
      longitute: "",
      skillset: "Customer Service",
      language: "English",
      complaint: "",
      agent_id: tokenagent.id,
      agent_extension: agent_extension,
      direction: "Incoming",
    };

    console.log("test", datas);

    const { data } = await axios.post(
      BaseUrl + "/users/callcreateSession",
      datas,
      {
        headers: {
          Authorization: "Bearer " + access_token,
          "Content-Type": "application/json",
          tenantid: localStorage.getItem("TenantId"),
        },
      }
    );
    //console.log("data",data)

    // audio.pause();
  };

  // const mute_call = () => {
  //   setShowmute(false);
  //   setShowUnmute(true);
  //   if (cli.doMute(callId)) {
  //     console.log("Mute SUCCESS");
  //   }
  // };

  // const unmute_call = () => {
  //   setShowmute(true);
  //   setShowUnmute(false);
  //   if (cli.doUnMute(callId)) {
  //     console.log("Unmute SUCCESS");
  //   }
  // };

  const unhold_call = (value) => {
    if (avayaplatform == "AACC") {
      if (value == "transferhold") {
        let ssToken = localStorage.getItem("ssoToken");
        axios
          .post(
            AvcUrl +
              `/voice/terminal/${agentLoginTerminalId}/unhold/` +
              consultedContactId,
            {},
            {
              headers: {
                ssoToken: ssToken,
              },
            }
          )
          .then(async (res) => {
            if ((res.data.status = "OK")) {
              console.log("Agent call in Hold...");
              setavayaplatform("AACC");
              setagentCallHold(true);
              setagentCallUnHold(false);
            }
          })
          .catch((err) => {
            console.error(err);
          });
        const access_token = localStorage.getItem("access_token");

        var datas = {
          type: "unhold",
          phonenumber: agentDialedNumber,
        };
        axios.post(BaseUrl + "/reports/dummyreport", datas, {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantid: localStorage.getItem("TenantId"),
          },
        });
      } else if (value == "incomingcallhold") {
        let ssToken = localStorage.getItem("ssoToken");
        axios
          .post(
            AvcUrl +
              `/voice/terminal/${agentLoginTerminalId}/unhold/` +
              contactId,
            {},
            {
              headers: {
                ssoToken: ssToken,
              },
            }
          )
          .then(async (res) => {
            if ((res.data.status = "OK")) {
              console.log("Customer call in unHold...");
              setavayaplatform("AACC");
              setShowHold(true);
              setShowUnHold(false);
            }
          })
          .catch((err) => {
            console.error(err);
          });
        var datas = {
          type: "unhold",
          phonenumber: dailedNumberInc,
        };
        const access_token = localStorage.getItem("access_token");
        axios.post(BaseUrl + "/reports/dummyreport", datas, {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantid: localStorage.getItem("TenantId"),
          },
        });
      }
    } else {
      setShowUnHold(true);
      cli.doUnHold(callId);
    }
  };

  const hold_call = (value) => {
    if (avayaplatform == "AACC") {
      if (value == "transferhold") {
        let ssToken = localStorage.getItem("ssoToken");
        axios
          .post(
            AvcUrl +
              `/voice/terminal/${agentLoginTerminalId}/hold/` +
              consultedContactId,
            {},
            {
              headers: {
                ssoToken: ssToken,
              },
            }
          )
          .then(async (res) => {
            if ((res.data.status = "OK")) {
              console.log("Agent call in Hold...");
              setavayaplatform("AACC");
              setagentCallHold(false);
              setagentCallUnHold(true);
              unhold_call("incomingcallhold");
            }
          })
          .catch((err) => {
            console.error(err);
          });
        const access_token = localStorage.getItem("access_token");
        var datas = {
          type: "hold",
          phonenumber: agentDialedNumber,
        };
        axios.post(BaseUrl + "/reports/dummyreport", datas, {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantid: localStorage.getItem("TenantId"),
          },
        });
      } else if (value == "incomingcallhold") {
        let ssToken = localStorage.getItem("ssoToken");
        axios
          .post(
            AvcUrl +
              `/voice/terminal/${agentLoginTerminalId}/hold/` +
              contactId,
            {},
            {
              headers: {
                ssoToken: ssToken,
              },
            }
          )
          .then(async (res) => {
            if ((res.data.status = "OK")) {
              console.log("Customer call in Hold...");
              setavayaplatform("AACC");
              setShowHold(false);
              setShowUnHold(true);
              unhold_call("transferhold");
            }
          })
          .catch((err) => {
            console.error(err);
          });
        const access_token = localStorage.getItem("access_token");
        var datas = {
          type: "hold",
          phonenumber: dailedNumberInc,
        };

        axios.post(BaseUrl + "/reports/dummyreport", datas, {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantid: localStorage.getItem("TenantId"),
          },
        });
      }
    } else {
      setShowHold(true);
      cli.doHold(callId);
    }
  };

  const end_call = (value) => {
    if (avayaplatform == "AACC") {
      if (value == "transercallcut") {
        let ssToken = localStorage.getItem("ssoToken");
        axios
          .post(
            AvcUrl +
              `/voice/terminal/${agentLoginTerminalId}/disconnect/` +
              consultedContactId,
            {},
            {
              headers: {
                ssoToken: ssToken,
              },
            }
          )
          .then((res) => {
            console.log(res);
            if ((res.data.status = "OK")) {
              setShowHold(false);
              setShowUnHold(true);
              setagentCallUnHold(false);
              setTransferCall(true);
              setmakingTransferCall(false);
              setConfrence(true);
              setMergeCall(false);
              setcallscreen(true);
              setshowIncommingNxtscreen(true);
              setcalledagentscreen(false);
              setagentDialedNumber("");
              setconferanceAgentScreen(false);
              setconfrencescreen(false);
              reset1();
            }
          })
          .catch((err) => {
            console.error(err);
          });
      } else if (value == "contactId") {
        console.log("hiiii");
        console.log("ContactId", contactId);
        let ssToken = localStorage.getItem("ssoToken");
        axios
          .post(
            AvcUrl +
              `/voice/terminal/${agentLoginTerminalId}/disconnect/` +
              contactId,
            {},
            {
              headers: {
                ssoToken: ssToken,
              },
            }
          )
          .then((res) => {
            console.log(res);
            if ((res.data.status = "OK")) {
              setconferanceCustomerScreen(false);
              setcallscreen(false);
              setcalledagentscreen(false);
              setconfrencescreen(false);
            }
          })
          .catch((err) => {
            console.error(err);
          });
      } else {
        let ssToken = localStorage.getItem("ssoToken");
        axios
          .post(
            AvcUrl +
              `/voice/terminal/${agentLoginTerminalId}/disconnect/` +
              consultedContactId,
            {},
            {
              headers: {
                ssoToken: ssToken,
              },
            }
          )
          .then((res) => {
            axios
              .post(
                AvcUrl +
                  `/voice/terminal/${agentLoginTerminalId}/disconnect/` +
                  contactId,
                {},
                {
                  headers: {
                    ssoToken: ssToken,
                  },
                }
              )
              .then((res) => {
                console.log("res");
                setcallscreen(false);
                setconfrencescreen(false);
                window.location.reload(true);
              });
          });
      }
    } else {
      cli.dropCall(callId);
      setShowDialerNextScr(false);
      window.location.reload();
    }
  };
  const transfer_call = () => {
    setShowphonedialer2(true);
    setcallscreen(false);
  };

  const making_transfer_call = () => {
    if (avayaplatform == "AACC") {
      let ssToken = localStorage.getItem("ssoToken");
      axios
        .post(
          `${AvcUrl}/voice/terminal/${agentLoginTerminalId}/transfer/${agentDialedNumber}/${contactId}`,
          {},
          {
            headers: {
              ssoToken: ssToken,
            },
          }
        )
        .then(async (res) => {
          console.log("makeing transfer call", res);
          if ((res.data.status = "OK")) {
            setconsultedContactId(res.data.data.contactId);
            setShowphonedialer2(false);
            setcallscreen(true);
            setshowIncommingNxtscreen(true);
            setShowHold(false);
            setShowUnHold(true);
            setConfrence(false);
            setTransferCall(false);
            setmakingTransferCall(true);
            setcalledagentscreen(true);
            setagentCallHold(true);
            setagentCallUnHold(false);
          }
        })
        .catch((err) => {
          console.error(err);
        });
    } else {
      //alert(dailedNumber);
      cli.transferCall(dailedNumber, callId, "unAttended");
    }
  };
  const makeTransferCall = () => {
    let ssToken = localStorage.getItem("ssoToken");
    if (avayaplatform == "AACC") {
      axios
        .post(
          `${AvcUrl}/voice/terminal/${agentLoginTerminalId}/transfer/complete/${consultedContactId}/${contactId}`,
          {},
          {
            headers: {
              ssoToken: ssToken,
            },
          }
        )
        .then((res) => {
          if ((res.data.status = "OK")) {
            console.log("transfer call completed ", res);
            setcallscreen(false);
            setshowIncommingNxtscreen(false);
            setcalledagentscreen(false);
            end_call("contactId");
          }
        })
        .catch((err) => {
          console.error(err);
        });
      const access_token = localStorage.getItem("access_token");
      var datas = {
        type: "transfer",
        phonenumber: dailedNumberInc,
        transfer_to: consultedContactId,
        transfer_from: agentLoginTerminalId,
      };

      axios.post(BaseUrl + "/reports/dummyreport", datas, {
        headers: {
          Authorization: "Bearer " + access_token,
          "Content-Type": "application/json",
          tenantid: localStorage.getItem("TenantId"),
        },
      });
    } else {
      setConfrence(true);
    }
  };
  const confrenceCall = () => {
    setShowphonedialer3(true);
    setcallscreen(false);
  };

  const make_Confrence_Call = () => {
    if (avayaplatform == "AACC") {
      let ssToken = localStorage.getItem("ssoToken");
      axios
        .post(
          `${AvcUrl}/voice/terminal/${agentLoginTerminalId}/conference/${agentDialedNumber}/${contactId}`,
          {},
          {
            headers: {
              ssoToken: ssToken,
            },
          }
        )
        .then(async (res) => {
          console.log("confrenece", res);
          if ((res.data.status = "OK")) {
            localStorage.setItem("AgentContactID", res.data.data.contactId);
            setconsultedContactId(res.data.data.contactId);
            setShowphonedialer3(false);
            setcallscreen(true);
            setshowIncommingNxtscreen(true);
            setShowHold(false);
            setShowUnHold(true);
            setTransferCall(false);
            setmakingTransferCall(false);
            setConfrence(false);
            setMergeCall(true);
            setcalledagentscreen(true);
            setagentCallHold(true);
            setagentCallUnHold(false);
          }
        })
        .catch((err) => {
          console.error(err);
        });
    } else {
      setConfrence(true);
    }
  };
  const mergeCall = () => {
    console.log("agentLoginTerminalId", agentLoginTerminalId);
    console.log("consultedContactId", consultedContactId);
    console.log("contactId", contactId);

    if (avayaplatform == "AACC") {
      let ssToken = localStorage.getItem("ssoToken");
      axios
        .post(
          `${AvcUrl}/voice/terminal/${agentLoginTerminalId}/conference/complete/${consultedContactId}/${contactId}`,
          {},
          {
            headers: {
              ssoToken: ssToken,
            },
          }
        )
        .then(async (res) => {
          console.log("mergeCall", res);
          if ((res.data.status = "OK")) {
            setcallscreen(true);
            setshowIncommingNxtscreen(false);
            setcalledagentscreen(false);
            setconfrencescreen(true);
            setlistOfConfrenceScreen(false);
            setconferanceCustomerScreen(true);
            setconferanceAgentScreen(true);
          }
        })
        .catch((err) => {
          console.error(err);
        });
      const access_token = localStorage.getItem("access_token");
      var datas = {
        type: "conference",
        phonenumber: dailedNumberInc,
        confrence_number: consultedContactId,
      };
      axios.post(BaseUrl + "/reports/dummyreport", datas, {
        headers: {
          Authorization: "Bearer " + access_token,
          "Content-Type": "application/json",
          tenantid: localStorage.getItem("TenantId"),
        },
      });
    } else {
      setConfrence(true);
    }
  };

  const toggle = () => {
    if (showphonedialer == false) {
      setshowphonedialer(true);
      settogglebutton(true);
    }
  };
  const toggleclose = () => {
    if (showphonedialer == true) {
      setshowphonedialer(false);
      settogglebutton(true);
    }
  };

  return (
    <>
      <audio
        ref={myRef}
        src="https://apps.inaipi.in/dailer/js/telephone_ring.mp3"
      />

      {togglebutton && (
        <span
          className="material-symbols-outlined"
          onClick={toggle}
          style={{ cursor: "pointer" }}
        >
          dialpad
        </span>
      )}
      {/* Dialer interFace */}
      {showphonedialer && (
        <Draggable handle=".handle">
          <nav className="dialer-main">
            <div className="filter shadow">
              <div className="icons-dialer">
                <div className="d-flex justify-content-between align-items-center mb-3">
                  <div className="user-dial position-relative handle">
                    <span className="dialer-badge position-absolute  start-100"></span>
                    <span className="avatar-side-letter">
                      <p style={{ fontSize: "13px" }}>
                        Ext:{localStorage.getItem("AvayaUsername")}
                      </p>
                    </span>
                  </div>
                  <div className="dismiss ">
                    <span
                      className="material-symbols-outlined"
                      onClick={toggleclose}
                      style={{ fontSize: "20px" }}
                    >
                      cancel
                    </span>
                  </div>
                </div>
              </div>
              <div className="dialer-sub">
                <div className="inputHolder  d-flex">
                  <input
                    className="form-control dialerInput"
                    type="text"
                    id=""
                    value={dailedNumber}
                    onChange={(e) =>
                      setDialedNumber(e.target.value.replace(/[^\d]/, ""))
                    }
                    onKeyDown={(event) => {
                      return /[0-9]/i.test(event.key);
                    }}
                  />
                  <button
                    className="dial-bck"
                    onClick={() =>
                      setDialedNumber((dailedNumber) =>
                        dailedNumber.slice(0, dailedNumber.length - 1)
                      )
                    }
                  >
                    <i
                      className="fa-solid fa-delete-left"
                      style={{ fontSize: "21px" }}
                    ></i>
                  </button>
                </div>
                <div className="dial-pad">
                  <div className="dial-pad-sub">
                    <ul className="icons p-0 m-0">
                      <li
                        id="one"
                        value="1"
                        className="num"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}1`)
                        }
                      >
                        1
                      </li>
                      <li
                        id="two"
                        value="2"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}2`)
                        }
                      >
                        2
                      </li>
                      <li
                        id="three"
                        value="3"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}3`)
                        }
                      >
                        3
                      </li>
                      <li
                        id="four"
                        value="4"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}4`)
                        }
                      >
                        4
                      </li>
                      <li
                        id="five"
                        value="5"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}5`)
                        }
                      >
                        5
                      </li>
                      <li
                        id="six"
                        value="6"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}6`)
                        }
                      >
                        6
                      </li>
                      <li
                        id="seven"
                        value="7"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}7`)
                        }
                      >
                        7
                      </li>
                      <li
                        id="eight"
                        value="8"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}8`)
                        }
                      >
                        8
                      </li>
                      <li
                        id="nine"
                        value="9"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}9`)
                        }
                      >
                        9
                      </li>
                      <li
                        className="star2"
                        id="*"
                        value="astrick"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}*`)
                        }
                      >
                        *
                      </li>
                      <li
                        className="d-flex flex-column"
                        id="zero"
                        value="0"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}0`)
                        }
                      >
                        0
                      </li>
                      <li
                        className="hash0"
                        id="hash"
                        value="#"
                        onClick={() =>
                          setDialedNumber((dailedNumber) => `${dailedNumber}#`)
                        }
                      >
                        #
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <nav className="d-flex justify-content-center dial-nav">
                <button
                  className="dialer-btn-main"
                  id="call"
                  onClick={makeCall}
                >
                  <i
                    className="fa-solid fa-phone"
                    style={{ fontSize: "17px" }}
                  ></i>
                </button>

                <ul className="nav nav-pills position-relative">
                  <li className="nav-item">
                    <a
                      href="#"
                      className="nav-link active d-flex flex-column"
                      aria-current="page"
                    >
                      <i className="fa-solid fa-house"></i>
                      Home
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="#" className="nav-link d-flex flex-column">
                      <i className="fa-solid fa-magnifying-glass"></i>
                      Search
                    </a>
                  </li>
                  <li className="nav-item nav-width">
                    <a></a>
                  </li>
                  <li className="nav-item">
                    <a href="#" className="nav-link d-flex flex-column">
                      <i className="fa-solid fa-clock-rotate-left"></i>History
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="#" className="nav-link d-flex flex-column">
                      <i className="fa-solid fa-address-book"></i> Contact
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </nav>
        </Draggable>
      )}
      {/* transfer interFace */}
      {showphonedialer2 && (
        <Draggable handle=".handle">
          <nav className="dialer-main">
            <div className="filter shadow">
              <div className="icons-dialer">
                <div className="d-flex justify-content-between align-items-center mb-3">
                  <div className="user-dial position-relative handle">
                    <span className="dialer-badge position-absolute  start-100"></span>
                    <span className="avatar-side-letter">
                      <p style={{ fontSize: "13px" }}>
                        Ext:{localStorage.getItem("AvayaUsername")}
                      </p>
                    </span>
                  </div>
                  <div className="dismiss ">
                    <span
                      className="material-symbols-outlined"
                      onClick={() => {
                        settogglebutton(false);
                        setshowphonedialer(false);
                        setShowphonedialer2(false);
                        setShowphonedialer3(false);
                        setcallscreen(true);
                      }}
                      style={{ fontSize: "20px" }}
                    >
                      cancel
                    </span>
                  </div>
                </div>
              </div>
              <div className="dialer-sub">
                <div className="inputHolder  d-flex">
                  <input
                    className="form-control dialerInput"
                    type="text"
                    id=""
                    value={agentDialedNumber}
                    onChange={(e) =>
                      setagentDialedNumber(e.target.value.replace(/[^\d]/, ""))
                    }
                    onKeyDown={(event) => {
                      return /[0-9]/i.test(event.key);
                    }}
                  />
                  <button
                    className="dial-bck"
                    onClick={() =>
                      setagentDialedNumber((transferNum) =>
                        transferNum.slice(0, transferNum.length - 1)
                      )
                    }
                  >
                    <i
                      className="fa-solid fa-delete-left"
                      style={{ fontSize: "21px" }}
                    ></i>
                  </button>
                </div>
                <div className="dial-pad">
                  <div className="dial-pad-sub">
                    <ul className="icons p-0 m-0">
                      <li
                        id="one"
                        value="1"
                        className="num"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}1`
                          )
                        }
                      >
                        1
                      </li>
                      <li
                        id="two"
                        value="2"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}2`
                          )
                        }
                      >
                        2
                      </li>
                      <li
                        id="three"
                        value="3"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}3`
                          )
                        }
                      >
                        3
                      </li>
                      <li
                        id="four"
                        value="4"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}4`
                          )
                        }
                      >
                        4
                      </li>
                      <li
                        id="five"
                        value="5"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}5`
                          )
                        }
                      >
                        5
                      </li>
                      <li
                        id="six"
                        value="6"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}6`
                          )
                        }
                      >
                        6
                      </li>
                      <li
                        id="seven"
                        value="7"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}7`
                          )
                        }
                      >
                        7
                      </li>
                      <li
                        id="eight"
                        value="8"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}8`
                          )
                        }
                      >
                        8
                      </li>
                      <li
                        id="nine"
                        value="9"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}9`
                          )
                        }
                      >
                        9
                      </li>
                      <li
                        className="star2"
                        id="*"
                        value="astrick"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}*`
                          )
                        }
                      >
                        *
                      </li>
                      <li
                        className="d-flex flex-column"
                        id="zero"
                        value="0"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}0`
                          )
                        }
                      >
                        0
                      </li>
                      <li
                        className="hash0"
                        id="hash"
                        value="#"
                        onClick={() =>
                          setagentDialedNumber(
                            (transferNum) => `${transferNum}#`
                          )
                        }
                      >
                        #
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <nav className="d-flex justify-content-center dial-nav">
                <button
                  className="dialer-btn-main"
                  id="call"
                  onClick={making_transfer_call}
                >
                  <span className="material-symbols-outlined">
                    phone_forwarded
                  </span>
                </button>

                <ul className="nav nav-pills position-relative">
                  <li className="nav-item">
                    <a
                      href="#"
                      className="nav-link active d-flex flex-column"
                      aria-current="page"
                    >
                      <i className="fa-solid fa-house"></i>
                      Home
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="#" className="nav-link d-flex flex-column">
                      <i className="fa-solid fa-magnifying-glass"></i>
                      Search
                    </a>
                  </li>
                  <li className="nav-item nav-width">
                    <a></a>
                  </li>
                  <li className="nav-item">
                    <a href="#" className="nav-link d-flex flex-column">
                      <i className="fa-solid fa-clock-rotate-left"></i>History
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="#" className="nav-link d-flex flex-column">
                      <i className="fa-solid fa-address-book"></i> Contact
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </nav>
        </Draggable>
      )}
      {/* Confrence interFace */}
      {showphonedialer3 && (
        <Draggable handle=".handle">
          <nav className="dialer-main">
            <div className="filter shadow">
              <div className="icons-dialer">
                <div className="d-flex justify-content-between align-items-center mb-3">
                  <div className="user-dial position-relative handle">
                    <span className="dialer-badge position-absolute  start-100"></span>
                    <span className="avatar-side-letter">
                      <p style={{ fontSize: "13px" }}>
                        Ext:{localStorage.getItem("AvayaUsername")}
                      </p>
                    </span>
                  </div>
                  <div className="dismiss ">
                    <span
                      className="material-symbols-outlined"
                      onClick={() => {
                        setShowphonedialer3(false);
                        setcallscreen(true);
                        setshowIncommingNxtscreen(true);
                      }}
                      style={{ fontSize: "20px" }}
                    >
                      cancel
                    </span>
                  </div>
                </div>
              </div>
              <div className="dialer-sub">
                <div className="inputHolder  d-flex">
                  <input
                    className="form-control dialerInput"
                    type="text"
                    id=""
                    value={agentDialedNumber}
                    onChange={(e) =>
                      setagentDialedNumber(e.target.value.replace(/[^\d]/, ""))
                    }
                    onKeyDown={(event) => {
                      return /[0-9]/i.test(event.key);
                    }}
                  />
                  <button
                    className="dial-bck"
                    onClick={() =>
                      setagentDialedNumber((conferenceNum) =>
                        conferenceNum.slice(0, conferenceNum.length - 1)
                      )
                    }
                  >
                    <i
                      className="fa-solid fa-delete-left"
                      style={{ fontSize: "21px" }}
                    ></i>
                  </button>
                </div>
                <div className="dial-pad">
                  <div className="dial-pad-sub">
                    <ul className="icons p-0 m-0">
                      <li
                        id="one"
                        value="1"
                        className="num"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}1`
                          )
                        }
                      >
                        1
                      </li>
                      <li
                        id="two"
                        value="2"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}2`
                          )
                        }
                      >
                        2
                      </li>
                      <li
                        id="three"
                        value="3"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}3`
                          )
                        }
                      >
                        3
                      </li>
                      <li
                        id="four"
                        value="4"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}4`
                          )
                        }
                      >
                        4
                      </li>
                      <li
                        id="five"
                        value="5"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}5`
                          )
                        }
                      >
                        5
                      </li>
                      <li
                        id="six"
                        value="6"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}6`
                          )
                        }
                      >
                        6
                      </li>
                      <li
                        id="seven"
                        value="7"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}7`
                          )
                        }
                      >
                        7
                      </li>
                      <li
                        id="eight"
                        value="8"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}8`
                          )
                        }
                      >
                        8
                      </li>
                      <li
                        id="nine"
                        value="9"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}9`
                          )
                        }
                      >
                        9
                      </li>
                      <li
                        className="star2"
                        id="*"
                        value="astrick"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}*`
                          )
                        }
                      >
                        *
                      </li>
                      <li
                        className="d-flex flex-column"
                        id="zero"
                        value="0"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}0`
                          )
                        }
                      >
                        0
                      </li>
                      <li
                        className="hash0"
                        id="hash"
                        value="#"
                        onClick={() =>
                          setagentDialedNumber(
                            (conferenceNum) => `${conferenceNum}#`
                          )
                        }
                      >
                        #
                      </li>
                    </ul>
                  </div>
                </div>
              </div>

              <nav className="d-flex justify-content-center dial-nav">
                <button
                  className="dialer-btn-main"
                  id="call"
                  onClick={make_Confrence_Call}
                >
                  <span className="material-symbols-outlined">
                    phone_forwarded
                  </span>
                </button>

                <ul className="nav nav-pills position-relative">
                  <li className="nav-item">
                    <a
                      href="#"
                      className="nav-link active d-flex flex-column"
                      aria-current="page"
                    >
                      <i className="fa-solid fa-house"></i>
                      Home
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="#" className="nav-link d-flex flex-column">
                      <i className="fa-solid fa-magnifying-glass"></i>
                      Search
                    </a>
                  </li>
                  <li className="nav-item nav-width">
                    <a></a>
                  </li>
                  <li className="nav-item">
                    <a href="#" className="nav-link d-flex flex-column">
                      <i className="fa-solid fa-clock-rotate-left"></i>History
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="#" className="nav-link d-flex flex-column">
                      <i className="fa-solid fa-address-book"></i> Contact
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </nav>
        </Draggable>
      )}

      {/* Outgoing call screen */}
      {outgoingscreen && (
        <Draggable>
          <div className="callattend_main  p-2">
            <Box
              sx={{
                width: "100%",
                height: 60,
                borderRadius: 1,
                // background: "#5b9acc",
              }}
              p={0.5}
            >
              <Stack
                direction="row"
                alignItems={"center"}
                justifyContent="space-between"
              >
                <Stack className="handle" direction="row" spacing={2}>
                  <Avatar src={PersonIcon} />
                  <Stack spacing={0.4}>
                    <Typography sx={{ color: "#ebeaeac9" }} variant="subtitle1">
                      {dailedNumber}
                    </Typography>
                    <Typography sx={{ color: "#ebeaeac9" }} variant="subtitle1">
                      Ringing...
                    </Typography>
                  </Stack>
                </Stack>

                <Stack direction="row" spacing={0.9}>
                  <button
                    type="button"
                    className="call-end border-0 d-flex justify-content-center align-items-center me-1"
                    onClick={() => endMakeCall()}
                  >
                    <span className="material-symbols-outlined">call_end</span>
                  </button>
                </Stack>
              </Stack>
            </Box>
          </div>
        </Draggable>
      )}
      {/* Outgoing call attend screen */}
      {outgoingNxtscreen && (
        <Draggable>
          <div className="callattend_main  p-1">
            <Box
              sx={{
                width: "100%",
                height: 60,
                borderRadius: 1,
                // background: "#5b9acc",
              }}
              p={0.5}
            >
              <Stack
                direction="row"
                alignItems={"center"}
                justifyContent="space-between"
              >
                <Stack className="handle" direction="row" spacing={2}>
                  <Avatar src={PersonIcon} />
                  <Stack spacing={0.9}>
                    <Typography sx={{ color: "#ebeaeac9" }} variant="subtitle1">
                      8072679551
                    </Typography>
                    <Typography sx={{ color: "#ebeaeac9" }} variant="caption">
                      00.00.00
                    </Typography>
                  </Stack>
                </Stack>

                <Stack direction="row" spacing={0.9}>
                  <button
                    type="button"
                    className="call-end border-0 d-flex justify-content-center align-items-center me-1"
                    onClick={() => rejectInCall()}
                  >
                    <span className="material-symbols-outlined">call_end</span>
                  </button>
                  <button
                    type="button"
                    className=" call-start border-0 d-flex justify-content-center align-items-center"
                    onClick={() => answerIncCall()}
                  >
                    <span className="material-symbols-outlined">call</span>
                  </button>
                </Stack>
              </Stack>
            </Box>
          </div>
        </Draggable>
      )}

      {/* incoming call screen */}
      {showIncommingscreen && (
        <Draggable handle=".handle">
          <div className="callattend_main  p-2">
            <Box
              sx={{
                width: "100%",
                height: 60,
                borderRadius: 1,
                // background: "#5b9acc",
              }}
              p={0.5}
            >
              <Stack
                direction="row"
                alignItems={"center"}
                justifyContent="space-between"
              >
                <Stack className="handle" direction="row" spacing={2}>
                  <Avatar src={PersonIcon} />
                  <Stack spacing={0.4}>
                    <Typography sx={{ color: "#ebeaeac9" }} variant="subtitle1">
                      {dailedNumberInc}
                    </Typography>
                    <Typography sx={{ color: "#ebeaeac9" }} variant="subtitle1">
                      IncomingCall...
                    </Typography>
                  </Stack>
                </Stack>

                <Stack direction="row" spacing={0.9}>
                  <button
                    type="button"
                    className="call-end border-0 d-flex justify-content-center align-items-center me-1"
                    onClick={() => rejectIncCall()}
                  >
                    <span className="material-symbols-outlined">call_end</span>
                  </button>
                  <button
                    type="button"
                    className=" call-start border-0 d-flex justify-content-center align-items-center"
                    onClick={() => answerIncCall()}
                  >
                    <span className="material-symbols-outlined">call</span>
                  </button>
                </Stack>
              </Stack>
            </Box>
          </div>
        </Draggable>
      )}

      {/* Incoming call attend screen */}
      {callscreen && (
        <Draggable handle=".handle">
          <div className="callattend_main p-2">
            {showIncommingNxtscreen && (
              <Box
                sx={{
                  width: "100%",
                  height: 60,
                  borderRadius: 1,
                  // background: "#5b9acc",
                }}
                p={0.5}
              >
                <Stack
                  direction="row"
                  alignItems={"center"}
                  justifyContent="space-between"
                >
                  <Stack className="handle" direction="row" spacing={2}>
                    <Avatar src={PersonIcon} />
                    <Stack spacing={0.9}>
                      <Typography
                        sx={{ color: "#ebeaeac9" }}
                        variant="subtitle1"
                      >
                        {dailedNumberInc}
                      </Typography>
                      <Typography sx={{ color: "#ebeaeac9" }} variant="caption">
                        {hours}:{minutes}:{seconds}
                      </Typography>
                    </Stack>
                  </Stack>

                  <Stack
                    direction="row"
                    sx={{ position: "absolute", top: "36px", left: "150px" }}
                  >
                    {/* {showmute && (
                      <button
                        type="button"
                        className="call-btn border-0 d-flex justify-content-center align-items-center me-1"
                        onClick={mute_call}
                      >
                        <i className="fa-solid fa-microphone call-btns"></i>
                      </button>
                    )}
                    {showUnmute && (
                      <button
                        type="button"
                        className="call-btn border-0 d-flex justify-content-center align-items-center me-1"
                        onClick={unmute_call}
                      >
                        <i
                          className="fa-solid fa-microphone-slash call-btns"
                          style={{ color: "#1d1f20" }}
                        ></i>
                      </button>
                    )} */}

                    {showHold && (
                      <button
                        type="button"
                        id="hold"
                        className="call-btn border-0 d-flex justify-content-center align-items-center me-1 "
                        onClick={() => {
                          hold_call("incomingcallhold");
                        }}
                      >
                        <i className="fa-solid fa-pause call-btns"></i>
                      </button>
                    )}

                    {showUnHold && (
                      <button
                        type="button"
                        id="unhold"
                        className="call-btn border-0 d-flex justify-content-center align-items-center me-1"
                        onClick={() => {
                          unhold_call("incomingcallhold");
                        }}
                      >
                        <i
                          className="fa-solid fa-circle-pause call-btns"
                          style={{ color: "#1d1f20" }}
                        ></i>
                      </button>
                    )}
                    {transferCall && (
                      <button
                        type="button"
                        id="unhold"
                        className="call-btn border-0 d-flex justify-content-center align-items-center me-1"
                        onClick={transfer_call}
                      >
                        <span
                          className="material-symbols-outlined call-btns"
                          style={{ fontWeight: 600 }}
                        >
                          phone_forwarded
                        </span>
                      </button>
                    )}
                    {makingTransferCall && (
                      <button
                        type="button"
                        id="unhold"
                        className="call-btn border-0 d-flex justify-content-center align-items-center me-1"
                        onClick={makeTransferCall}
                      >
                        <span
                          class="material-symbols-outlined call-btns"
                          style={{ fontWeight: 600 }}
                        >
                          swap_calls
                        </span>
                      </button>
                    )}
                    {confrence && (
                      <button
                        type="button"
                        id="unhold"
                        className="call-btn border-0 d-flex justify-content-center align-items-center me-1"
                        onClick={confrenceCall}
                      >
                        <span
                          class="material-symbols-outlined call-btns"
                          style={{ fontWeight: 600 }}
                        >
                          add_call
                        </span>
                      </button>
                    )}
                    {mergecall && (
                      <button
                        type="button"
                        id="unhold"
                        className="call-btn border-0 d-flex justify-content-center align-items-center me-1"
                        onClick={mergeCall}
                      >
                        <span
                          className="material-symbols-outlined call-btns"
                          style={{ fontWeight: 600 }}
                        >
                          call_merge
                        </span>
                      </button>
                    )}
                  </Stack>

                  <Stack direction="row">
                    <button
                      type="button"
                      className="call-end border-0 d-flex justify-content-center align-items-center"
                      onClick={() => {
                        end_call("contactId");
                      }}
                    >
                      <span className="material-symbols-outlined">
                        call_end
                      </span>
                    </button>
                  </Stack>
                </Stack>
              </Box>
            )}

            {calledagentscreen && (
              <Box
                sx={{
                  width: "100%",
                  height: 60,
                  borderRadius: 1,
                  // background: "#5b9acc",
                }}
                p={0.5}
              >
                <Stack
                  direction="row"
                  alignItems={"center"}
                  justifyContent="space-between"
                >
                  <Stack className="handle" direction="row" spacing={2}>
                    <Avatar src={PersonIcon} />
                    <Stack spacing={0.9}>
                      <Typography
                        sx={{ color: "#ebeaeac9" }}
                        variant="subtitle1"
                      >
                        {agentDialedNumber}
                      </Typography>
                      <Typography
                        sx={{ color: "#ebeaeac9" }}
                        variant="caption"
                      ></Typography>
                    </Stack>
                  </Stack>

                  <Stack
                    direction="row"
                    sx={{ position: "absolute", top: "95px", left: "150px" }}
                  >
                    {agentCallHold && (
                      <button
                        type="button"
                        id="hold"
                        className="call-btn border-0 d-flex justify-content-center align-items-center me-1 "
                        onClick={() => {
                          hold_call("transferhold");
                        }}
                      >
                        <i className="fa-solid fa-pause call-btns"></i>
                      </button>
                    )}
                    {agentCallUnHold && (
                      <button
                        type="button"
                        id="unhold"
                        className="call-btn border-0 d-flex justify-content-center align-items-center me-1"
                        onClick={() => {
                          unhold_call("transferhold");
                        }}
                      >
                        <i
                          className="fa-solid fa-circle-pause call-btns"
                          style={{ color: "#1d1f20" }}
                        ></i>
                      </button>
                    )}
                  </Stack>

                  <Stack direction="row">
                    <button
                      type="button"
                      className="call-end border-0 d-flex justify-content-center align-items-center"
                      onClick={() => {
                        end_call("transercallcut");
                      }}
                    >
                      <span className="material-symbols-outlined">
                        call_end
                      </span>
                    </button>
                  </Stack>
                </Stack>
              </Box>
            )}

            {confrencescreen && (
              <Box
                sx={{
                  width: "100%",
                  height: 60,
                  borderRadius: 1,
                  // background: "#5b9acc",
                }}
                p={0.5}
              >
                <Stack
                  direction="row"
                  alignItems={"center"}
                  justifyContent="space-between"
                >
                  <Stack className="handle" direction="row" spacing={2}>
                    <Avatar src={PersonIcon} />
                    <Stack spacing={0.9}>
                      <Typography
                        sx={{ color: "#ebeaeac9" }}
                        variant="subtitle1"
                      >
                        Call In Confrence
                      </Typography>
                      <Typography sx={{ color: "#ebeaeac9" }} variant="caption">
                        {hours}:{minutes}:{seconds}
                      </Typography>
                    </Stack>
                  </Stack>

                  <Stack direction="row">
                    <button
                      type="button"
                      className="call-end border-0 d-flex justify-content-center align-items-center"
                      onClick={() => {
                        end_call();
                      }}
                    >
                      <span className="material-symbols-outlined">
                        call_end
                      </span>
                    </button>
                  </Stack>
                </Stack>
              </Box>
            )}
          </div>
        </Draggable>
      )}
    </>
  );
};

export default Sdk;
