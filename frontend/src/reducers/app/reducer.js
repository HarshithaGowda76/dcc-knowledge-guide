import {
  SEND_MESSAGE,
  TOGGLE_RECORDING,
  SET_UPLOADED_FILE,
  SET_SELECTED_EMAIL,
  SET_SELECTED_MOBILE,
  SET_CHAT_ID,
  SET_CHAT_DURATION,
  SET_SELECTED_USERIMAGE,
  TOOGLE_CHAT,
  REFRESH_TOOGLE_CHAT,
  CONF_CHAT_REFRESH,
  INTERNAL_CHAT_REFRESH,
  CHAT_DATA,
  CHAT_DATA_FOR_INTERNAL,
  INTERNAL_CHAT_NOTIFY,
  TYPE_CHAT,
  CHAT_MESSAGE,
  SET_CONTACT_LIST,
  SET_AGENT_LIST,
  SET_CONTACT_LIST1,
  SET_INTERNAL_EXTERNAL,
  SET_CONF_NOTIFI,
  SET_CONF_NOTIFI_VAL,
  ONLOAD_CONTACT_REFRESH,
  SET_CONTACT_LIST2,
  SET_AGENT_AVAIL,
  SET_SELECTED_COLOR,
  SET_LOADMORE,
  SET_SELECTED_USERNAME,
  SET_ADD_TOPIC_INPUT,
  SET_ADD_TOPIC_TOGGLE,
  SET_SELECTED_WHATSAPP,
  SET_SELECTED_FACEBOOK,
  SET_SELECTED_TWITTER,
  SET_SELECTED_TEAMS,
  SET_SELECTED_COMPANY,
  SET_SELECTED_ADDRESS,
  SET_SELECTED_ID,
  SET_SELECTED_CHANNEL,
  SET_SELECTED_CHANNEL_DATA,
  UPDATED_SELECTED_CHANNEL_DATA,
  SEND_ATTACHMENT_URL,
  SEND_ATTACHMENT_URL_BOLLEAN,
  KNOWLEDGE_GUIDE,
KNOWLEDGE_CATEGORY,
IS_WRITE,



  IFRAME_MESSAGE,
  IFRAME_SHOWMESSAGE,
  IFRAME_ISPENDING,
  IFRAME_SHOW_COLOR,
  IFRAME_FULLSCREEN,
  IFRAME_CONTACT_NUMBER,
  IFRAME_SESSION_ID,
  IFRAME_ARTICLE_LIST,
  IFRAME_FA_QURIES




} from "../../redux/actions/types";

const initialState = {
  updatechanneldata:'',
  selectedchannel:'',
  selectedchanneldata:[],
  spaceContent: [],
  availAgent:[],
  conferenceType: "",
  client: null,
  selecteduserimage: "",
  uploadedFile: [],
  chatid: "",
  chatduration: "",
  cannedMessage:[],
  showErrorAlert: false,
  errorMessage: "",
  errorData: "",
  showvideocallModal: false,
  internalchatnotify: false,
  onloadContactRefresh: true,
  recording: false,
  togglechat: false,
  refreshtogglechat: false,
  confchatrefresh: false,
  internalchatrefresh: false,
  chat: [],
  chatdataforinternal: [],
  chatType: "",
  chatMessage: [],
  contactList: [],
  contactList1: [],
  internalExternal: 0,
  contactList2: [],
  agentList: [],
  conferenceNotification: false,
  conferenceNotificationVal: "",
  usercolor:'',
  loadmoretoggle: false,
  addTopicInput:'',
  addTopicToggle: false,
  selectedusername: "",
  selectedemail: "",
  selectedmobile: "",
  selectedwhatsapp:'',
  selectedfacebook:'',
  selectedtwitter:'',
  selectedteams:'',
  selectedcompany:'',
  selectedaddress:'',
  selectedid:'',
  
  /**Email Channel Reducers Variables */
  emaiIsPending:false,
  emailShowMessage:false,
  emailMessage:"",
  emailShowColor:"",
  InboxDataList:[],
  completedDataList:[],
  InboxEmailContent:{},
  InboxEmailBody: "",
  writeEmail:false,
  isReplyEmail:false,
  NewEmailDataList:[],
  SentItemList:[],


  // FOR IFRAME

  isPending:false,
  showMessage:false,
  message:"",
  fullScreen:true,
  contactNumber:'',
  sessionId:'',
  schArticalList :[],
  
  attachmentUrl:'',
  attachmentUrlbollian:false,
  showKnowlegeguidecomponent:false,
  knowledgeCategory:"",
  iswrite:false,




  schArticalList02 : [
    {
        'fileName' : 'The Welcome to dark world.PDF',
        'articalContent' : 'AEA000101013',
        'articalId' : 'ar001',
        'favorite': false,
        'artLiked': false,
        'totalLikes': 20,
        "fileDownloadPath": "http://43.241.62.118:8080/RuleEngine/static/files/Credit_Debit_Card.PDF",
        'fileIndexedContents' : '\nRBI/2022-23/92 \nDoR.AUT.REC.No.27/24.01.041/2022-23 April 21, 2022 \n \nMaster Direction – Credit Card and Debit Card – Issuance and  \nConduct Directions, 2022 \nIn exercise of the powers conferred by Sections 35A and Section 56 of the Banking \nRegulation Act, 1949 and Chapter IIIB of the Reserve Bank of India Act, 1934, the Reserve \nBank of India being satisfied that it is necessary and expedient in the public interest so to do, \nhereby, issues the Directions hereinafter specified.'
    },
    {
      'fileName' : 'The Welcome to dark world.PDF',
      'articalContent' : 'AEA000101013',
      'articalId' : 'ar001',
      'favorite': false,
      'artLiked': false,
      'totalLikes': 20,
      "fileDownloadPath": "http://43.241.62.118:8080/RuleEngine/static/files/Credit_Debit_Card.PDF",
      'fileIndexedContents' : '\nRBI/2022-23/92 \nDoR.AUT.REC.No.27/24.01.041/2022-23 April 21, 2022 \n \nMaster Direction – Credit Card and Debit Card – Issuance and  \nConduct Directions, 2022 \nIn exercise of the powers conferred by Sections 35A and Section 56 of the Banking \nRegulation Act, 1949 and Chapter IIIB of the Reserve Bank of India Act, 1934, the Reserve \nBank of India being satisfied that it is necessary and expedient in the public interest so to do, \nhereby, issues the Directions hereinafter specified.'
    },
    {
      'fileName' : 'The Welcome to dark world.PDF',
      'articalContent' : 'AEA000101013',
      'articalId' : 'ar001',
      'favorite': false,
      'artLiked': false,
      'totalLikes': 20,
      "fileDownloadPath": "http://43.241.62.118:8080/RuleEngine/static/files/Credit_Debit_Card.PDF",
      'fileIndexedContents' : '\nRBI/2022-23/92 \nDoR.AUT.REC.No.27/24.01.041/2022-23 April 21, 2022 \n \nMaster Direction – Credit Card and Debit Card – Issuance and  \nConduct Directions, 2022 \nIn exercise of the powers conferred by Sections 35A and Section 56 of the Banking \nRegulation Act, 1949 and Chapter IIIB of the Reserve Bank of India Act, 1934, the Reserve \nBank of India being satisfied that it is necessary and expedient in the public interest so to do, \nhereby, issues the Directions hereinafter specified.'
    },
    {
      'fileName' : 'The Welcome to dark world.PDF',
      'articalContent' : 'AEA000101013',
      'articalId' : 'ar001',
      'favorite': false,
      'artLiked': false,
      'totalLikes': 20,
      "fileDownloadPath": "http://43.241.62.118:8080/RuleEngine/static/files/Credit_Debit_Card.PDF",
      'fileIndexedContents' : '\nRBI/2022-23/92 \nDoR.AUT.REC.No.27/24.01.041/2022-23 April 21, 2022 \n \nMaster Direction – Credit Card and Debit Card – Issuance and  \nConduct Directions, 2022 \nIn exercise of the powers conferred by Sections 35A and Section 56 of the Banking \nRegulation Act, 1949 and Chapter IIIB of the Reserve Bank of India Act, 1934, the Reserve \nBank of India being satisfied that it is necessary and expedient in the public interest so to do, \nhereby, issues the Directions hereinafter specified.'
    },
    {
      'fileName' : 'The Welcome to dark world.PDF',
      'articalContent' : 'AEA000101013',
      'articalId' : 'ar001',
      'favorite': false,
      'artLiked': false,
      'totalLikes': 20,
      "fileDownloadPath": "http://43.241.62.118:8080/RuleEngine/static/files/Credit_Debit_Card.PDF",
      'fileIndexedContents' : '\nRBI/2022-23/92 \nDoR.AUT.REC.No.27/24.01.041/2022-23 April 21, 2022 \n \nMaster Direction – Credit Card and Debit Card – Issuance and  \nConduct Directions, 2022 \nIn exercise of the powers conferred by Sections 35A and Section 56 of the Banking \nRegulation Act, 1949 and Chapter IIIB of the Reserve Bank of India Act, 1934, the Reserve \nBank of India being satisfied that it is necessary and expedient in the public interest so to do, \nhereby, issues the Directions hereinafter specified.'
    },
],


  faQuries:[
    {
      'id': 1,
      'question' : 'How to change/update the mobile number?',
      'answer' : 'Kindly place a request at the branch for changing the Mobile number'
    },
    {
      'id': 2,
      'question' : 'How to change/update the mailing address?',
      'answer' : 'Kindly place a request at the nearest branch for changing the Mailing address .'
    },
    {
      'id': 3,
      'question' : 'I am a NRE customer want to update the contact details',
      'answer' : 'You may send a Email or Fax the request incase Fax/email Indemnity is at place . Refer official site . Else you may send the orginial request through courier at your home branch.'
    },
    {
      'id': 4,
      'question' : 'Are there any charges for non maintenance of minimum balance in my account?',
      'answer' : 'Refer the schedule of charges.'
    },
    {
      'id': 5,
      'question' : 'How can I stop a cheque payment?',
      'answer' : 'It can be done through Branch , IVR, Netbanking, SMS Banking, Mobile Banking and also by calling at Contact center .'
    },
    {
      'id': 6,
      'question' : 'Is there any charges applicable for initiating cheque stop payment ?',
      'answer' : 'Cheque stop payment can be initiated free of charges through NetBanking, SMS, Mobile as well through IVR but would be charged if done through Branch or by speaking to Contact Center officer.'
    },
    {
      'id': 7,
      'question' : 'How long will it take to update a change of address?',
      'answer' : 'It takes 7 Working days for bank to process the request from the time complete request is given'
    },
    {
      'id': 8,
      'question' : 'Can I use my existing salary account if I have changed employers?',
      'answer' : 'Yes , only if the new employer is registred as Corporate account holder with BANK'
    },
    {
      'id': 9,
      'question' : 'What are the annual charges of debit card?',
      'answer' : 'Refer the schedule of charges'
    },
    {
      'id': 10,
      'question' : 'Is there any charges applicable for ATM insufficient transaction?',
      'answer' : 'Yes , Please refer schedule of charges'
    },
    
  ],



  /***************** */

};

export function appReducer(state = initialState, action) {
  switch (action.type) {




    case IS_WRITE:
      return {
        ...state,
        iswrite: action.val,
      };

    case KNOWLEDGE_CATEGORY:
      return {
        ...state,
        knowledgeCategory: action.val,
      };

    case KNOWLEDGE_GUIDE:
      return {
        ...state,
        showKnowlegeguidecomponent: action.val,
      };
      case SEND_MESSAGE:
      return {
        ...state,
        cannedMessage: action.val,
      };
    case CHAT_DATA:
      return {
        ...state,
        chat: action.val,
      };
      case SET_AGENT_AVAIL:
        return {
          ...state,
          availAgent: action.val,
        };
      
    case CHAT_DATA_FOR_INTERNAL:
      return {
        ...state,
        chatdataforinternal: action.val,
      };
    case INTERNAL_CHAT_NOTIFY:
      return {
        ...state,
        internalchatnotify: action.val,
      };

    case ONLOAD_CONTACT_REFRESH:
      return {
        ...state,
        onloadContactRefresh: action.val,
      };
    case TYPE_CHAT:
      return {
        ...state,
        chatType: action.val,
      };
    case SET_CONTACT_LIST:
      return {
        ...state,
        contactList: action.val,
      };
    case SET_AGENT_LIST:
      return {
        ...state,
        agentList: action.val,
      };
    case SET_CONF_NOTIFI:
      return {
        ...state,
        conferenceNotification: action.val,
      };
    case SET_CONF_NOTIFI_VAL:
      return {
        ...state,
        conferenceNotificationVal: action.val,
      };
    case SET_INTERNAL_EXTERNAL:
      return {
        ...state,
        internalExternal: action.val,
      };
    case SET_CONTACT_LIST1:
      return {
        ...state,
        contactList1: action.val,
      };

    case SET_CONTACT_LIST2:
      return {
        ...state,
        contactList2: action.val,
      };
    case CHAT_MESSAGE:
      return {
        ...state,
        chatMessage: action.val,
      };
    case SET_SELECTED_EMAIL:
      return {
        ...state,
        selectedemail: action.val,
      };
    case SET_SELECTED_MOBILE:
      return {
        ...state,
        selectedmobile: action.val,
      };
    case SET_CHAT_ID:
      return {
        ...state,
        chatid: action.val,
      };
    case SET_CHAT_DURATION:
      return {
        ...state,
        chatduration: action.val,
      };
    case SET_SELECTED_USERIMAGE:
      return {
        ...state,
        selecteduserimage: action.val,
      }; 
       case TOGGLE_RECORDING: {
      return {
        ...state,
        recording: action.val,
      };
    }
     case SET_UPLOADED_FILE: {
      return {
        ...state,
        uploadedFile: [action.val],
      };
    } 
    case TOOGLE_CHAT: {
      return {
        ...state,
        togglechat: action.val,
      };
    }
    case SET_LOADMORE: {
      return {
        ...state,
        loadmoretoggle: action.val,
      };
    }
    case SET_SELECTED_USERNAME:
      return {
        ...state,
        selectedusername: action.val,
      };

    case REFRESH_TOOGLE_CHAT: {
      return {
        ...state,
        refreshtogglechat: action.val,
      };
    }
    case CONF_CHAT_REFRESH: {
      return {
        ...state,
        confchatrefresh: action.val,
      };
    }
    case INTERNAL_CHAT_REFRESH: {
      return {
        ...state,
        internalchatrefresh: action.val,
      };
    }
    case SET_SELECTED_COLOR: {
      return {
        ...state,
        usercolor: action.val,
      };
    }
    case SET_ADD_TOPIC_INPUT: {
      return {
        ...state,
        addTopicInput: action.val,
      };
    }
    case SET_ADD_TOPIC_TOGGLE: {
      return {
        ...state,
        addTopicToggle: action.val,
      };
    }
    case SET_SELECTED_WHATSAPP: {
      return {
        ...state,
        selectedwhatsapp: action.val,
      };
    }
    case SET_SELECTED_FACEBOOK: {
      return {
        ...state,
        selectedfacebook: action.val,
      };
    }
    case SET_SELECTED_TWITTER: {
      return {
        ...state,
        selectedtwitter: action.val,
      };
    }
    case SET_SELECTED_CHANNEL: {
      return {
        ...state,
        selectedchannel: action.val,
      };
    }
    case SET_SELECTED_TEAMS: {
      return {
        ...state,
        selectedteams: action.val,
      };
    }
    case SET_SELECTED_COMPANY: {
      return {
        ...state,
        selectedcompany: action.val,
      };
    }
    case SET_SELECTED_ADDRESS: {
      return {
        ...state,
        selectedaddress: action.val,
      };
    }
    case SET_SELECTED_ID: {
      return {
        ...state,
        selectedid: action.val,
      };
    }

    case SET_SELECTED_CHANNEL_DATA: {
      return {
        ...state,
        selectedchanneldata: action.val,
      };
    }

    case UPDATED_SELECTED_CHANNEL_DATA: {
      return {
        ...state,
        updatechanneldata: action.val,
      };
    }


    case SEND_ATTACHMENT_URL: {
      return {
        ...state,
        attachmentUrl: action.val,
      };
    }


    case SEND_ATTACHMENT_URL_BOLLEAN: {
      return {
        ...state,
        attachmentUrlbollian: action.val,
      };
    }







    



    // IFRAME 


    // case IFRAME_MESSAGE: {
    //   return {
    //     ...state,
    //     message: action.val,
    //   };
    // }
    

    // case IFRAME_SHOWMESSAGE: {
    //   return {
    //     ...state,
    //     showMessage: action.val,
    //   };
    // }

    // case IFRAME_ISPENDING: {
    //   return {
    //     ...state,
    //     isPending: action.val,
    //   };
    // }

    // case IFRAME_FULLSCREEN: {
    //   return {
    //     ...state,
    //     fullScreen: action.val,
    //   };
    // }

    // case IFRAME_CONTACT_NUMBER: {
    //   return {
    //     ...state,
    //     contactNumber: action.val,
    //   };
    // }

    // case IFRAME_SESSION_ID: {
    //   return {
    //     ...state,
    //     sessionId: action.val,
    //   };
    // }

    // case IFRAME_ARTICLE_LIST: {
    //   return {
    //     ...state,
    //     schArticalList: action.val,
    //   };
    // }


    // case IFRAME_FA_QURIES: {
    //   return {
    //     ...state,
    //     faQuries: action.val,
    //   };
    // }


    case 'ACTION_PENDING':
      return Object.assign({}, state, {
        isPending: action.isPending
      })
    case 'GET_SEARCHED_ARTICLES':
      return Object.assign({}, state, {  
        schArticalList: action.schArticalList,
        isPending: action.isPending
      })

    case 'AUTHENTICATION_FAILED':
        return Object.assign({}, state, {
        message: action.message,
        showMessage: action.showMessage,
        isPending: action.isPending,
        showColor:action.showColor
        })  
    case "AUTHENTICATION_FAILED_CLOSE":
      return Object.assign({}, state, {
        showMessage: action.showMessage,
        message:action.message
      })


    

    /**Email Channel Reucer case */
    case 'INBOX_PENDING':
      return Object.assign({}, state, {
        emaiIsPending: action.emaiIsPending
      })
    case 'INBOX_MAIL_LIST_GET_SUCCESS':
      return Object.assign({}, state, {      
        InboxDataList: action.InboxDataList,
        emaiIsPending: action.emaiIsPending,
      })
    case 'INBOX_MAIL_COMPLETE_SUCCESS':
      return Object.assign({}, state, {      
        completedDataList: action.completedDataList,
        emaiIsPending: action.emaiIsPending,
      })
    case 'INBOX_MAIL_CONTENT_SUCCESS':
      return Object.assign({}, state, {      
        InboxEmailContent: action.InboxEmailContent,
        InboxEmailBody: action.InboxEmailBody,
        emaiIsPending: action.emaiIsPending,
      })
    case 'INBOX_WRITE_MAIL':
      return Object.assign({}, state, {      
        writeEmail: action.writeEmail,
        isReplyEmail: action.isReplyEmail,
      })
    case 'INBOX_FAILED':
      return Object.assign({}, state, {
        emailMessage: action.emailMessage,
        emailShowMessage: action.emailShowMessage,
        emaiIsPending: action.emaiIsPending,
        emailShowColor: action.emailShowColor
      })  
    case "INBOX_FAILED_CLOSE":
      return Object.assign({}, state, {
        emailShowMessage: action.emailShowMessage,
        emailMessage: action.emailMessage
      })




    /***************************** */

    default:
      return state;
  }
}
