import React, { useState, useEffect } from 'react';
import { Grid, Container, Fab, Box, IconButton, SwipeableDrawer } from '@mui/material';
import { blue } from '@mui/material/colors';
import _ from 'lodash';
import moment from 'moment';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import TipsAndUpdatesIcon from '@mui/icons-material/TipsAndUpdates';
import Knowledge from './Knowledge';
import queryString from 'query-string';

const Home = (props) => {
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(0);
  const [searchval, setSearchval] = useState('');
  const [activeIndex, setActiveIndex] = useState('');
  const [adxList, setAdxList] = useState([]);
  const [kaView, setKaView] = useState(false);
  const [searchQueryRoute, setSearchQueryRoute] = useState(false);
  const [searchQueryValue, setSearchQueryValue] = useState('');

  useEffect(() => {
    checkQuery();
  }, []);

  const checkQuery = () => {
    let searchArticle = queryString.parse(window.location.search).searchArticle;
    if (searchArticle) {
      setSearchQueryRoute(true);
      setSearchQueryValue(searchArticle);
    }
  };

  const openKaView = () => {
    setKaView(true);
  };

  const closeKaView = () => {
    setKaView(false);
  };

  const toggleDrawer = (anchor, open) => (event) => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setKaView(open);
  };

  const redirectAdmin = () => {
    props.history.push('/admin');
  };

  return (
    <div className='Content'>
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Box sx={{ height: '100vh' }}>
            <Knowledge
              action={props}
              schArticalList={props.schArticalList}
              closekaView={closeKaView}
              faQuries={props.faQuries}
              searchQueryRoute={searchQueryRoute}
              searchQueryValue={searchQueryValue}
            />
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};

export default Home;

/**Colors */
/**amber, blue, blueGrey, brown, common, cyan, deepOrange, deepPurple, green, grey, indigo, lightBlue, lightGreen, lime, orange, pink, purple, red, teal, yellow */