import { connect } from "react-redux";
import { IsErrorClose, getSearchedArtical, uploadArtical} from "../../redux/actions/spaceActions";
import Home from "./Home";

const mapStateToProps = (state) => {
  console.log('developmentiframe',state.data)
  return {
    message: state.data.message,
    showMessage: state.data.showMessage,    
    isPending: state.data.isPending,
    showColor: state.data.showColor,
    fullScreen: state.data.fullScreen,
    contactNumber: state.data.contactNumber,
    sessionId: state.data.sessionId,
    schArticalList: state.data.schArticalList,
    faQuries: state.data.faQuries,
  }
}
const mapDispatchToProps = (dispatch) => ({
  IsErrorClose: () => dispatch(IsErrorClose()),
  getSearchedArtical: (a) => dispatch(getSearchedArtical(a)),
  uploadArtical: (a) => dispatch(uploadArtical(a))
})
const IFramesecondContainer = connect(mapStateToProps, mapDispatchToProps)(Home)

export default IFramesecondContainer