import React, { useState, useEffect } from "react";
import {
  Grid,
  Container,
  Box,
  Tabs,
  Tab,
  Paper,
  IconButton,
  InputBase,
  Divider,
  Card,
  CardHeader,
  CardContent,
  Typography,
  CardActions,
  Chip,
  Popover,
  MenuList,
  MenuItem,
  ListItemIcon,
  ListItemText,
  Fade,
  CircularProgress,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Alert,
  Collapse,
  Slide,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  DialogTitle,
} from "@mui/material";
import { pink } from "@mui/material/colors";
import { Link, withRouter } from "react-router-dom";
import AddIcon from "@mui/icons-material/Add";
import SettingsIcon from "@mui/icons-material/Settings";
import LoopIcon from "@mui/icons-material/Loop";
import SearchIcon from "@mui/icons-material/Search";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import ShareIcon from "@mui/icons-material/Share";
import QuizIcon from "@mui/icons-material/Quiz";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import MenuIcon from "@mui/icons-material/Menu";
import DirectionsIcon from "@mui/icons-material/Directions";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ThumbUpOffAltIcon from "@mui/icons-material/ThumbUpOffAlt";
import ThumbUpAltIcon from "@mui/icons-material/ThumbUpAlt";
import ContentPasteSearchIcon from "@mui/icons-material/ContentPasteSearch";
import QueryStatsIcon from "@mui/icons-material/QueryStats";
import SendAndArchiveIcon from "@mui/icons-material/SendAndArchive";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import CloseIcon from "@mui/icons-material/Close";
import AddToDriveIcon from "@mui/icons-material/AddToDrive";
import CastForEducationIcon from "@mui/icons-material/CastForEducation";
import _ from "lodash";
import moment from "moment";
import { connect } from "react-redux";

import {
  setSendattachmentUrl,
  setshowKnowlegeguidecomponent,
  setIswrite,
  openReplyEmail,

} from "../../redux/actions/spaceActions";

const mapStateToProps = (state) => {
  console.log("knowledgeComponent", state.data.knowledgeCategory);
  return {
    attachmentUrl: state.data.chatid,
    knowledgeCategory: state.data.knowledgeCategory,
    selectedchanneldata: state.data.selectedchanneldata,
  };
};

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const Knowledge = (props) => {
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [page, setPage] = useState(0);
  const [searchval, setSearchval] = useState("");
  const [activeIndex, setActiveIndex] = useState("");
  const [schArticalList, setSchArticalList] = useState([]);
  const [adxList, setAdxList] = useState([]);
  const [valueIndex, setValueIndex] = useState({});
  const [selectedTab, setSelectedTab] = useState(1);
  const [artView, setArtView] = useState(false);
  const [actionView, setActionView] = useState(false);
  const [txtArtical, setTxtArtical] = useState(props.knowledgeCategory);
  const [anchorEl, setAnchorEl] = useState(null);
  const [isAlertMail, setIsAlertMail] = useState(false);
  const [isAlertTag, setIsAlertTag] = useState(false);


  
  const handleClickOpen = (event) => {
    setAnchorEl(event.currentTarget);
    setActionView(true);
  };

  useEffect(() => {
    setInitialValue(props.schArticalList);
    console.log("rrrrrrrrrrrrrr", props.schArticalList);
  }, []);

  useEffect(() => {
    if (!_.isEqual(props.schArticalList, schArticalList)) {
      setInitialValue(props.schArticalList);
    }
    if (!_.isEqual(props.searchQueryRoute, props.searchQueryRoute)) {
      if (props.searchQueryRoute) {
        handleSubmitQuery(props.searchQueryValue);
        console.log("valueeeeeeeequeryyyyyy", props.searchQueryValue);

        handleSearchQuery(props.searchQueryValue);
      }
    }

    // handleSubmitQuery("Credit Card");
    // handleSearchQuery("Credit Card");
  }, [props.schArticalList, props.searchQueryRoute]);

  const setInitialValue = (data) => {
    if (!_.isEmpty(data)) {
      _.map(data, (val, i) => {
        val["favorite"] = false;
        val["liked"] = false;
      });
      setSchArticalList(data);
    } else {
      setSchArticalList([]);
    }
  };

  const handleSearch = (e) => {
    setTxtArtical(e.target.value);
  };

  const handleSearchQuery = (data) => {
    setTxtArtical(data);
  };

  const handleChangeTab = (e, newValue) => {
    setSelectedTab(newValue);
  };

  const openArtView = () => {
    setArtView(true);
  };

  const closeArtView = () => {
    setArtView(false);
  };

  const handelClickForurl = (obj) => {
    console.log("clickd", obj);
  };
  const openActionView = (e, value) => {
    setActionView(true);
    setAnchorEl(e.currentTarget);
    setValueIndex(value);
  };

  const closeActionView = () => {
    setActionView(false);
    setAnchorEl(null);
    setValueIndex({});
  };

  const handleSubmitQuery = (data) => {
    console.log("Url hit");
    if (data && data.length > 0) {
      const obj = {
        searchQuery: data,
      };
      props.action.getSearchedArtical(obj);
    }
  };

  useEffect(() => {
    if (txtArtical && txtArtical.length > 0) {
      const obj = {
        searchQuery: txtArtical,
      };
      props.action.getSearchedArtical(obj);
    }
  }, [props.knowledgeCategory]);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("search hit");
    if (txtArtical && txtArtical.length > 0) {
      const obj = {
        searchQuery: txtArtical,
      };
      props.action.getSearchedArtical(obj);
    }
  };

  const makeFavorite = (index) => {
    const updatedArticals = [...schArticalList];
    updatedArticals[index]["favorite"] = !updatedArticals[index]["favorite"];
    setSchArticalList(updatedArticals);
  };

  const makeLike = (index) => {
    const updatedArticals = [...schArticalList];
    updatedArticals[index]["liked"] = !updatedArticals[index]["liked"];
    setSchArticalList(updatedArticals);
  };

  const openDocument = () => {
    if (!_.isEmpty(valueIndex)) {
      window.open(valueIndex.fileDownloadPath, "_blank");
    }
  };

  const redirectAdmin = () => {
    document.getElementById("redirectLink").click();
  };

  const openDocumentSideScreen = (data) => {
    if (!_.isEmpty(data)) {
      props.openDocumentFullView(data);
    }
  };

  const openAlertMail = () => {
    if (!_.isEmpty(valueIndex)) {
      props.setSendattachmentUrl(valueIndex.fileDownloadPath);

    }
    closeActionView();
  };

  const openAlertTag = () => {
    setIsAlertTag(true);
    closeActionView();
  };

  const closeAlert = () => {
    setIsAlertMail(false);
    setIsAlertTag(false);
  };

  const handleCloseKnowledgeGuide = () => {
    props.setshowKnowlegeguidecomponent(false);
  };

  return (
    <div className="text-left fs-12">
      <li style={{ visibility: "hidden" }}>
        <Link id="redirectLink" to={"/admin"} />
      </li>
      <Box sx={{ height: "100vh" }}>
        <Container fixed>
          <div className="m-t-10 d-flex ">
            <p className="text-content fs-16 fw-600">Knowledge Search</p>
            <i
              class="fa-solid fa-circle-xmark"
              style={{
                cursor: "pointer",
                marginLeft: "274px",
                fontSize: "15px",
              }}
              onClick={handleCloseKnowledgeGuide}
            ></i>
          </div>
          <div>
            <nav>
              <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <button
                  class="nav-link active"
                  id="nav-search-tab"
                  data-bs-toggle="tab"
                  data-bs-target="#nav-search"
                  type="button"
                  role="tab"
                  aria-controls="nav-search"
                  aria-selected="true"
                >
                  <SearchIcon style={{ fontSize: "15px" }} /> Search
                </button>
                <button
                  class="nav-link"
                  id="nav-favorite-tab"
                  data-bs-toggle="tab"
                  data-bs-target="#nav-favorite"
                  type="button"
                  role="tab"
                  aria-controls="nav-favorite"
                  aria-selected="false"
                >
                  <FavoriteIcon style={{ fontSize: "15px" }} /> My Favorite
                </button>
                <button
                  class="nav-link"
                  id="nav-faq-tab"
                  data-bs-toggle="tab"
                  data-bs-target="#nav-faq"
                  type="button"
                  role="tab"
                  aria-controls="nav-faq"
                  aria-selected="false"
                >
                  <QuizIcon style={{ fontSize: "15px" }} /> FAQs
                </button>
              </div>
            </nav>
            <div class="tab-content m-t-10" id="nav-tabContent">
              <div
                class="tab-pane fade show active"
                id="nav-search"
                role="tabpanel"
                aria-labelledby="nav-search-tab"
                tabindex="0"
              >
                <Paper
                  elevation={4}
                  component="form"
                  sx={{
                    p: "2px 4px",
                    display: "flex",
                    alignItems: "center",
                    width: "100%",
                  }}
                  onSubmit={handleSubmit}
                >
                  <IconButton
                    sx={{ p: "10px" }}
                    aria-label="menu"
                    onDoubleClick={redirectAdmin}
                  >
                    <MenuIcon />
                  </IconButton>
                  <InputBase
                    sx={{ ml: 1, flex: 1 }}
                    placeholder="Search Articles..."
                    id="txtArtical"
                    value={txtArtical}
                    onChange={handleSearch}
                  />
                  <IconButton
                    type="button"
                    sx={{ p: "10px" }}
                    aria-label="search"
                    onClick={handleSubmit}
                  >
                    <SearchIcon />
                  </IconButton>
                  <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
                  <IconButton
                    color="primary"
                    sx={{ p: "10px" }}
                    aria-label="directions"
                    onClick={handleSubmit}
                  >
                    <DirectionsIcon />
                  </IconButton>
                </Paper>


                {!_.isEmpty(schArticalList) ? (
                  // {(artView)?
                  <div className="fixed-content knowledge-guide-content">
                    {_.map(schArticalList, (obj, i) => {
                      console.log('obj',obj)
                      return (
                        
                        <Box sx={{ display: "flex",height:'180px' }} className="m-t-5">
                          <Paper elevation={3}>
                            <Card className="knowledge-guide-content " sx={{height:'180px' }}>
                              <CardHeader
                              sx={{paddingBottom:'0px'}}
                                action={
                                  <IconButton
                                    aria-label="settings"
                                    onClick={(e) => openActionView(e, obj)}
                                  >
                                    <MoreVertIcon />
                                  </IconButton>
                                }
                                title={
                                  <Typography variant="body1">
                                    {obj.fileName.split(".").shift()}
                                  </Typography>
                                }
                              />
                              <CardContent >
                                <Typography
                                  className="text-ellipsis--2 text-truncate"
                                  variant="body2"
                                  color="text.secondary"
                                  style={{width:"410px",marginBottom:"-50px",cursor:"pointer",WebkitBoxOrient:"vertical",WebkitLineClamp:"5"}}
                                >
                                  {obj.fileIndexedContents}
                                </Typography>
                                <div className="m-t-5 m-b-0 fs-8 mt-2">
                                  <Chip label="External" className="m-r-5" />
                                  <Chip label="Published" variant="outlined" />
                                </div>
                              </CardContent>
                              <CardActions disableSpacing
                              sx={{padding:"0px 10px 20px 10px"}}>
                                <IconButton
                                  aria-label="add to favorites "
                                  onClick={() => makeFavorite(i)}
                                  
                                >
                                  {!obj.favorite ? (
                                    <FavoriteBorderIcon />
                                  ) : (
                                    <FavoriteIcon sx={{ color: pink[500] }} />
                                  )}
                                </IconButton>
                                <IconButton
                                  aria-label="like"
                                  onClick={() => makeLike(i)}
                                  
                                >
                                  {!obj.liked ? (
                                    <ThumbUpOffAltIcon />
                                  ) : (
                                    <ThumbUpAltIcon color="primary" />
                                  )}
                                </IconButton>
                                <IconButton aria-label="like">
                                  <VisibilityIcon />
                                </IconButton>
                              </CardActions>
                            </Card>
                          </Paper>
                        </Box>
                      );
                    })}
                  </div>
                ) : (
                  <div className="text-center p-t-20">
                    <ContentPasteSearchIcon
                      sx={{ fontSize: 75 }}
                      color="action"
                    />
                    <Typography
                      className="text-content"
                      variant="overline"
                      display="block"
                      gutterBottom
                    >
                      You haven't searched anything yet.
                    </Typography>

                    {/* <Fade in={true}
                            style={{
                              transitionDelay: true ? '800ms' : '0ms',
                            }}
                            sx={{ color: 'grey.500' }}
                            unmountOnExit
                          >
                            <CircularProgress color="inherit" />
                          </Fade> */}
                  </div>
                )}
              </div>

              <div
                class="tab-pane fade"
                id="nav-favorite"
                role="tabpanel"
                aria-labelledby="nav-favorite-tab"
                tabindex="0"
              >
                {!_.isEmpty(_.filter(schArticalList, { favorite: true })) ? (
                  <div className="fixed-content">
                    {_.map(
                      _.filter(schArticalList, { favorite: true }),
                      (val, i) => {
                        return (
                          <Box sx={{ display: "flex" }} className="m-t-5">
                            <Paper elevation={3}>
                              <Card>
                                <CardHeader
                                  action={
                                    <IconButton
                                      aria-label="settings"
                                      onClick={(e) => openActionView(e, val)}
                                    >
                                      <MoreVertIcon />
                                    </IconButton>
                                  }
                                  title={
                                    <Typography variant="body1">
                                      {val.fileName.split(".").shift()}
                                    </Typography>
                                  }
                                  // subheader="September 14, 2016"
                                />
                                <CardContent>
                                  <Typography
                                    className="text-ellipsis--2"
                                    variant="body2"
                                    color="text.secondary"
                                  >
                                    {val.fileIndexedContents}
                                  </Typography>
                                  <div className="m-t-5 m-b-0 fs-8">
                                    <Chip label="External" className="m-r-5" />
                                    <Chip
                                      label="Published"
                                      variant="outlined"
                                    />
                                  </div>
                                </CardContent>
                                <CardActions disableSpacing>
                                  <IconButton
                                    aria-label="add to favorites"
                                    onClick={() => makeFavorite(i)}
                                  >
                                    {!val.favorite ? (
                                      <FavoriteBorderIcon />
                                    ) : (
                                      <FavoriteIcon sx={{ color: pink[500] }} />
                                    )}
                                  </IconButton>
                                  <IconButton
                                    aria-label="like"
                                    onClick={() => makeLike(i)}
                                  >
                                    {!val.liked ? (
                                      <ThumbUpOffAltIcon />
                                    ) : (
                                      <ThumbUpAltIcon color="primary" />
                                    )}
                                  </IconButton>
                                  <IconButton
                                    aria-label="like"
                                    onClick={() => openDocumentSideScreen(val)}
                                  >
                                    <VisibilityIcon />
                                  </IconButton>
                                </CardActions>
                              </Card>
                            </Paper>
                          </Box>
                        );
                      }
                    )}
                  </div>
                ) : (
                  <div className="text-center p-t-20">
                    <QueryStatsIcon
                      className="p-b-20"
                      sx={{ fontSize: 75 }}
                      color="action"
                    />
                    <Typography
                      className="text-content"
                      variant="overline"
                      display="block"
                      gutterBottom
                    >
                      You have not added any artical to favorite list.
                    </Typography>
                    <Typography
                      mt={0}
                      className="text-content"
                      variant="overline"
                      display="block"
                      gutterBottom
                    >
                      Once you added any artical as favorite it will appear
                      here.
                    </Typography>
                  </div>
                )}
              </div>
              <div
                class="tab-pane fade"
                id="nav-faq"
                role="tabpanel"
                aria-labelledby="nav-faq-tab"
                tabindex="0"
              >
                {!_.isEmpty(props.faQuries) ? (
                  <div className="fixed-content">
                    {_.map(props.faQuries, (val, i) => {
                      return (
                        <Accordion>
                          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                            <Typography variant="subtitle2">
                              {val.question}
                            </Typography>
                          </AccordionSummary>
                          <AccordionDetails>
                            <Typography variant="caption">
                              {val.answer}
                            </Typography>
                          </AccordionDetails>
                        </Accordion>
                      );
                    })}
                  </div>
                ) : (
                  <div className="text-center p-t-20">
                    <QueryStatsIcon
                      className="p-b-20"
                      sx={{ fontSize: 75 }}
                      color="action"
                    />
                    <Typography
                      className="text-content"
                      variant="overline"
                      display="block"
                      gutterBottom
                    >
                      NO FAQ's Found
                    </Typography>
                  </div>
                )}
              </div>
            </div>
          </div>
        </Container>

        <div>
          <Popover
            id="simple-popover"
            open={actionView}
            anchorEl={anchorEl}
            onClose={closeActionView}
            //  onClick={props.setSendattachmentUrl}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left",
            }}
            style={{ height: actionView ? "100%" : "50%" }}
          >
            <Paper sx={{ width: 200, maxWidth: "100%" }}>
              <MenuList>
                <MenuItem onClick={openDocument}>
                  <ListItemIcon>
                    <CastForEducationIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText>Open Documet</ListItemText>
                </MenuItem>
                <Divider />
                <MenuItem onClick={openAlertMail}>
                  <ListItemIcon>
                    <SendAndArchiveIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText>Attach Email</ListItemText>
                </MenuItem>
                <Divider />
                <MenuItem onClick={openAlertTag}>
                  <ListItemIcon>
                    <AddToDriveIcon fontSize="small" />
                  </ListItemIcon>
                  <ListItemText>Tag to complaint</ListItemText>
                </MenuItem>
              </MenuList>
            </Paper>
          </Popover>
        </div>

        <Dialog
          open={isAlertMail}
          onClose={closeAlert}
          TransitionComponent={Transition}
          keepMounted
        >
          <DialogContent>
            <DialogContentText>
              <span className="fw-600">
                The referred document has been mailed to the customer !!
              </span>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={closeAlert}>close</Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={isAlertTag}
          onClose={closeAlert}
          TransitionComponent={Transition}
          keepMounted
        >
          <DialogContent>
            <DialogContentText>
              <span className="fw-600">
                The referred document has been tagged to the complaint history
              </span>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={closeAlert}>close</Button>
          </DialogActions>
        </Dialog>
      </Box>
    </div>
  );
};

// export default Knowledge;

export default connect(mapStateToProps, {
  setSendattachmentUrl,
  setshowKnowlegeguidecomponent,
  setIswrite,
  openReplyEmail,


})(Knowledge);
