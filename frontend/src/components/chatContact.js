import React, { useState, useEffect, useRef } from "react";
import chatIcon from "../assets/img/chat-icon.png";
import emailIcon from "../assets/img/email.png";
import chatIconWhatsapp from "../assets/img/whatsapp.png";
import chatIconfacebook from "../assets/img/facebook.png";
import chatIcontwitter from "../assets/img/twitter.png";
import chatIconteams from "../assets/img/teams.png";
import reply from "../EmailComponents/Resources/images/reply2.png";
import replyall from "../EmailComponents/Resources/images/replyall.png";
import forward from "../EmailComponents/Resources/images/forward.png";
import { SiMicrosoftteams } from "react-icons/si";
import { MdOutlineCall, MdOutlineClose } from "react-icons/md";
import { CSpinner } from "@coreui/react";
import { useNavigate } from "react-router-dom";
import { connect } from "react-redux";
import {
  BaseUrl,
  errorApi,
  frontendBaseurl,
} from "../containers/Page/Constants/BaseUrl";
import "./chatStyle.css";
import { Button, Modal, Badge } from "react-bootstrap";
import "../assets/assets_newchat/assets/fonts/fontawesome-free-6.1.1-web/css/all.css";
import "../assets/assets_newchat/library/animate/animate.min.css";
import "../assets/assets_newchat/assets/styles/chat.css";
import "../assets/assets_newchat/assets/scripts/jquery-3.6.0.min.js";
import "../assets/assets_newchat/assets/scripts/popper.min.js";
import "../assets/assets_newchat/library/tippy/js/tippy-bundle.umd.min.js";
import "../assets/assets_newchat/library/bootstrap-5.1.3/css/bootstrap.min.css";
import "../assets/assets_newchat/assets/scripts/jquery-3.6.0.min.js";
import "../assets/assets_newchat/assets/scripts/popper.min.js";
import "../assets/assets_newchat/library/bootstrap-5.1.3/js/bootstrap.min.js";
import "../assets/assets_newchat/library/tippy/js/tippy-bundle.umd.min.js";
import DoNotTouchIcon from "@mui/icons-material/DoNotTouch";

import TimeAgo from "react-timeago";
import frenchStrings from "react-timeago/lib/language-strings/fr";
import buildFormatter from "react-timeago/lib/formatters/buildFormatter";

import { Tooltip } from "bootstrap";
import {
  setselectedusername,
  setselectedemail,
  setselectedmobile,
  setselectedwhatsapp,
  setselectedfacebook,
  setselectedtwitter,
  setselectedteams,
  setselectedcompany,
  setselectedadress,
  setselectedid,
  setselecteduserimage,
  setchatdata,
  setchattype,
  setchatid,
  setchatduration,
  setrefreshtogglechat,
  setcontactlist,
  setcontactlist2,
  setonloadContactRefresh,
  setconfchatrefresh,
  setinternalexternal,
  settogglechat,
  setAgentList,
  setinternalchatnotify,
  setinternalchatrefresh,
  setSelectedColor,
  setloadmore,
  setSelectedchannel,
  setSelectedchanneldata,
  setUpdatechanneldata,
  setKnowledgeCategory,
} from "../redux/actions/spaceActions";

import Paper from "@material-ui/core/Paper";
import TabNew from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import { RiErrorWarningLine, RiMailForbidLine } from "react-icons/ri";
import { toast } from "react-toastify";
import { AiFillCloseCircle, AiOutlineCloseCircle } from "react-icons/ai";
import { IoMdOpen } from "react-icons/io";
import { FaArrowUp } from "react-icons/fa";
import Draggable from "react-draggable";

var axios = require("axios");
var moment = require("moment");
var moment = require("moment-timezone");
var momentDurationFormatSetup = require("moment-duration-format");
momentDurationFormatSetup(moment);
typeof moment.duration.fn.format === "function";
// true
typeof moment.duration.format === "function";

const mapStateToProps = (state) => {
  const { data } = state;
  return {
    selectedusername: data.selectedusername,
    contactList: data.contactList,
    chatduration: data.chatduration,
    chatid: data.chatid,
    contactList2: data.contactList2,
    chat: data.chat,
    onloadContactRefresh: data.onloadContactRefresh,
    confchatrefresh: data.confchatrefresh,
    internalExternal: data.internalExternal,
    agentList: data.agentList,
    internalchatrefresh: data.internalchatrefresh,
    internalchatnotify: data.internalchatnotify,
    usercolor: state.data.usercolor,
    loadmoretoggle: state.data.loadmoretoggle,
    selectedchannel: state.data.selectedchannel,
    selectedchanneldata: state.data.selectedchanneldata,
    updatechanneldata: state.data.updatechanneldata,
  };
};

function Contacts(props) {
  let permission = JSON.parse(localStorage.getItem("permission"));
  let result = permission.find(
    (item) => item === "EXT_CHAT" || item === "IN_CHAT"
  );
  let indexNum = localStorage.getItem("indexOf");
  const tenantId = localStorage.getItem("TenantId");
  // console.log("props>>>>>>>>>>>>>>", props)
  const navigate = useNavigate();
  const socket = useRef();
  const [availAgent, setAvailAgent] = useState([]);
  const [sessionId, setSessionId] = useState("");
  const [agentTransferId, setAgentTransferId] = useState("agent");
  const { selectedusername, chat, chatid, onloadContactRefresh } = props;
  const handleTransferClose = () => setShowTransferMod(false);
  const handleTransferOpen = (session) => {
    setSessionId(session);
    setShowTransferMod(true);
  };
  const [showTransferMod, setShowTransferMod] = useState(false);

  const [currentSelected, setCurrentSelected] = useState(undefined);
  const [oldId, setOldId] = useState("");
  const [currentuser, setCurrentUser] = useState();
  const [chatAsActive, setChatAsActive] = useState("active");
  const [overflow, setOverflow] = useState("scroll");
  const [alignChatIfCall, setAlignChatIfCall] = useState("0px");
  const [visible, setVisible] = useState(true);
  const [accept, setAccept] = useState(false);
  const [internalClient, setInternalClient] = useState([]);
  const [internalClientFiltered, setInternalClientFiltered] = useState([]);
  const [intervalId, setIntervalId] = useState(0);
  const [value, setValue] = useState(0);
  const [agentContactList, setAgentContactList] = useState([]);
  const [nonActiveAgents, setNonActiveAgents] = useState([]);
  const [agentId, setAgentId] = useState("");
  const [internalspinner, setInternalspinner] = useState(false);
  const [searchterm, setSearchterm] = useState([]);
  const [show, setShow] = useState(false);
  const [internalagentloader, setInternalagentloader] = useState(false);
  const [openmail, setOpenmail] = useState(false);
  const [escalationModal, setEscalationModal] = useState(false);
  const [emailid, setEmailid] = useState("");
  const [emaiddata, setEmaildata] = useState([]);

  const basedOnRole = JSON.parse(localStorage.getItem("tokenAgent"));

  moment.locale("en", {
    relativeTime: {
      future: "in %s",
      past: "%s ",
      s: "just now",
      ss: "%ss",
      m: "1 min",
      mm: "%d min",
      h: "1 hour",
      hh: "%dh",
      d: "1 day",
      dd: "%dd",
      M: "1 month",
      MM: "%d M",
      y: "1 year",
      yy: "%d Y",
    },
  });

  // const isBothPresent = permission[indexNum].screen.includes('IN_CHAT');
  // console.log('isBothPresent', isBothPresent)

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handelEscalationModalOpen = () => {
    setEscalationModal(true);
  };

  const handelEscalationModalClose = () => {
    setEscalationModal(false);
  };

  const permissions = JSON.parse(localStorage.getItem("permission"));
  // console.log({ permissions })

  // console.log("permission[indexNum].screen.includes('IN_CHAT')", permission[indexNum].screen.includes('IN_CHAT'))
  useEffect(() => {
    if (!localStorage.getItem("tokenAgent")) {
      navigate("/");
    } else {
      getDataOnload();
      getInternalAgents();
    }
    getData();
  }, []);

  useEffect(() => {
    if (props.confchatrefresh) {
      clearInterval(intervalId);
      getDataAfterAcceptClient();
    }
  }, [props.confchatrefresh]);

  useEffect(() => {
    if (props.internalchatrefresh) {
      getAllInternalAgents("not_create");
    }
  }, [props.internalchatrefresh]);

  const handelOpenmail = () => {
    setOpenmail(true);
  };

  const handelClosemail = () => {
    setOpenmail(false);
  };

  const errorHandel = async (error, endpoint) => {
    const tenantID = localStorage.getItem("TenantId");
    try {
      const payload = {
        servicename: "DCCCHAT",
        logs: error,
        description: endpoint,
      };

      const header = {
        headers: {
          // Authorization: "Bearer " + token,
          // "Content-Type": "application/json",
          tenantid: tenantID,
        },
      };
      const { data } = await axios.post(
        errorApi + "/UIlogs/createLogs",
        payload,
        header
      );
      if (data.status) {
        console.log("from error api", data);
      }
    } catch (error) {
      console.log("error", error);
    }
  };

  const fetchSingleEmail = (emailId) => {
    console.log(emailId);
    const token = localStorage.getItem("access_token");

    const data = {
      id: emailId,
    };
    const header = {
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    };
    axios
      .post(
        "https://gway.release.inaipi.ae/routeemail/email/fetchEmail",
        data,
        header
      )
      .then((response) => {
        if (response) {
          console.log("response<<<<<<<>>>>>>>>>>>>>>chatcontact", response);
          props.setSelectedchanneldata(response.data);
          props.setUpdatechanneldata(response.data.EmailBody);
          setEmaildata(response.data);
          toast.success(response.data.message, {
            position: "top-right",
            autoClose: 1000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          console.log("handelOnclickInboxMessage", response.data);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  async function getDataOnload() {
    try {
      const access_token = localStorage.getItem("access_token");
      const datas = await JSON.parse(localStorage.getItem("tokenAgent"));
      setCurrentUser(JSON.stringify(datas));
      setAgentId(datas.id);
      const data = await axios.post(
        BaseUrl + `/users/allUsers?agent_id=${datas.id}`,
        {},
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      if (data.data.length > 0 && data.data) {
        props.setcontactlist(data.data);
        console.log(props.contactList);
      }
    } catch (error) {
      errorHandel(error, "/users/allUsers");
    }
  }

  const logout = async () => {
    const access_token = localStorage.getItem("access_token");
    let data = await JSON.parse(localStorage.getItem("tokenAgent"));
    const id = localStorage.getItem("TenantId");
    let userID = data.user_id;
    try {
      if (data) {
        const update = await axios.post(
          BaseUrl + "/users/logout/" + userID,
          {},
          {
            headers: {
              Authorization: "Bearer " + access_token,
              "Content-Type": "application/json",
              tenantId: tenantId,
            },
          }
        );
        // Clear localStorage items
        console.log("ertyuytdslogout", update.data.success);
        if (update.data.success == true) {
          localStorage.removeItem("AvayaUsername");
          localStorage.removeItem("tab");
          localStorage.removeItem("timer_connect_sec");
          localStorage.removeItem("AvayaPassword");
          localStorage.removeItem("AvayaDomain");
          localStorage.removeItem("client");
          localStorage.removeItem("statusValue");
          localStorage.removeItem("emailDisplay");
          localStorage.removeItem("timer_connect_min");
          localStorage.removeItem("tokenAgent");
          localStorage.removeItem("timer_status");
          localStorage.removeItem("NameDisplay");
          localStorage.removeItem("access_token");
          localStorage.removeItem("timer_connect_hour");

          if (!id) {
            window.location.href = "error.html";
            console.error("ID not found in local storage");
          } else {
            const loginUrl = `${frontendBaseurl}/?tenantID=${encodeURIComponent(
              id
            )}`;
            window.location.href = loginUrl;
          }

          // navigate("/");
        }
      }
    } catch (error) {
      // console.log(error);
      errorHandel(error, "/users/logout/");
    }
  };

  async function getDataAfterAcceptClient() {
    try {
      const access_token = localStorage.getItem("access_token");
      props.setconfchatrefresh(false);
      const datas = await JSON.parse(localStorage.getItem("tokenAgent"));
      setCurrentUser(JSON.stringify(datas));
      const data = await axios.post(
        BaseUrl + `/users/allUsers?agent_id=${datas.id}`,
        {},
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      console.log("contactlist................>>>>>>>>>>>>>>>>", data);
      props.setcontactlist(data.data);
      let acceptClient = JSON.parse(localStorage.getItem("client"));
      onClickContactChatActive(acceptClient, 0);

      props.setonloadContactRefresh(false);
    } catch (error) {
      setShow(true);
      errorHandel(error, "/users/allUsers");
    }
  }

  const rejectchat = async (
    chat_session_id,
    agentid,
    id,
    skillset,
    language,
    phone,
    channel
  ) => {
    const access_token = localStorage.getItem("access_token");

    let filteredArray = props.contactList2.filter((item) => {
      return item.id !== id;
    });
    console.log("filteredArray..................", filteredArray);
    props.setcontactlist2(filteredArray);
    let userId = await JSON.parse(localStorage.getItem("tokenAgent"));
    let passUserId = userId.user_id;
    let data = {
      chat_session_id: chat_session_id,
      userID: passUserId,
      agentID: agentid,
      skillset: skillset,
      language: language,
      phone: phone,
      channel: channel,
      session_id: id,
    };
    await axios
      .post(BaseUrl + "/users/rejectchat", data, {
        headers: {
          Authorization: "Bearer " + access_token,
          "Content-Type": "application/json",
          tenantId: tenantId,
        },
      })
      .then((res) => {
        console.log("chat rejected");
        window.location.reload(true);
      })
      .catch((error) => {
        window.location.reload(true);
        errorHandel(error, "/users/rejectchat");
        if (error.status == 401) {
          window.location.href = "/";
        }
      });
  };

  async function getData() {
    try {
      const access_token = localStorage.getItem("access_token");
      const datas = await JSON.parse(localStorage.getItem("tokenAgent"));

      const data = await axios.post(
        BaseUrl + `/users/getIncomingUsers/${datas.id}`,
        {},
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      console.log("alldata", data.data.data);
      props.setcontactlist2(data.data.data);

      var value = true;
    } catch (error) {
      errorHandel(error, "/users/getIncomingUsers/");
      setShow(true);
    }
  }

  const acceptClient = async (client) => {
    try {
      const uniqueid = Math.floor(Math.random() * 90000) + 10000;
      window.open(`https://apps.inaipi.in/avayacms_ocbc/public/index.php/inaipi/livecall?mobilenumber=919740687413&uniqueid=${uniqueid}&survey_opt=1&type=Web&agentname=admin`, '_blank');
      localStorage.setItem("client", JSON.stringify(client));
      const access_token = localStorage.getItem("access_token");

      let filteredArray = props.contactList2.filter((item) => {
        return item.id !== client.id;
      });

      console.log("after accepting", filteredArray);
      props.setcontactlist2(filteredArray);
      const data = await JSON.parse(localStorage.getItem("tokenAgent"));

      const datas = await axios.post(
        BaseUrl +
          `/users/agentConfirmation?agent_id=${data.id}&client_id=${client.id}&status=Accept&user_id=${data.user_id}`,
        {},
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      if (datas.data.status) {
        clearInterval(intervalId);
        let acceptClient = JSON.parse(localStorage.getItem("client"));
        onClickContactChatActive(acceptClient, 0);
        getDataAfterAcceptClient();
        window.location.reload(true);
      }
      console.log("id", data);
      setAccept((prev) => !prev);
    } catch (error) {
      errorHandel(error, "/users/agentConfirmation");
      setShow(true);
    }
  };

  const transferAgentSubmit = async () => {
    try {
      const access_token = localStorage.getItem("access_token");
      let dataToPass = {
        sessionId: sessionId,
        agentId: agentTransferId,
      };

      const data = await axios.post(
        BaseUrl + "/users/transferAgent",
        dataToPass,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      if (data.data.status) {
        setShowTransferMod(false);

        getDataOnload();
      }
    } catch (error) {
      errorHandel(error, "/users/transferAgent");
    }
  };

  const updateCount = (id) => {
    props.contactList.map((contact, index) => {
      if (contact.id === id) {
        contact.unreadcount = 0;
      }
      if (contact.id === oldId) {
        contact.unreadcount = 0;
      }
    });
  };

  const updateCount1 = (id) => {
    props.agentList.map((contact, index) => {
      if (contact.id === id) {
        contact.unreadcount = undefined;
      }
      if (contact.id === oldId) {
        contact.unreadcount = undefined;
      }
    });
  };

  const getInternalAgents = async () => {
    setInternalagentloader(true);
    try {
      const access_token = localStorage.getItem("access_token");
      const data = await axios.post(
        BaseUrl + "/users/availableInternalAgents",
        {},
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      if (data) {
        if (data.data.success) {
          setInternalagentloader(false);
          let internalAgents = data.data.data;
          let getCurrentAgent = JSON.parse(localStorage.getItem("tokenAgent"));
          let UserId = getCurrentAgent.user_id;
          let filterAgents = internalAgents.filter((data) => {
            return UserId != data.user_id;
          });
          setInternalClient(filterAgents);

          getAllInternalAgents("not_create");
        } else {
          setInternalClient([]);
        }
      }
    } catch (error) {
      setShow(true);
      errorHandel(error, "/users/availableInternalAgents");
    }
  };

  const filterBySearch = (event) => {
    const query = event.target.value;
    var updatedList = [...nonActiveAgents];
    updatedList = updatedList.filter((item) => {
      console.log("nonActiveAgents............", item);
      return item.username.toLowerCase().indexOf(query.toLowerCase()) !== -1;
    });
    if (query != "") {
      setInternalClientFiltered(updatedList);
    } else {
      const query = event.target.value;
      var updatedList = [...props.agentList];
      updatedList = updatedList.filter((item) => {
        console.log("ActiveAgents............", item);

        return item.username.toLowerCase().indexOf(query.toLowerCase()) !== -1;
      });
      setInternalClientFiltered(updatedList);
    }
  };

  const createInternalChat = async (UserId, clientId) => {
    try {
      setInternalspinner(true);
      const access_token = localStorage.getItem("access_token");
      const getAgent = await JSON.parse(localStorage.getItem("tokenAgent"));
      let agentId = getAgent.id;
      const data = await axios.post(
        BaseUrl + "/users/createInternalAgentsSession",
        { userId: UserId, agentId: agentId },
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );

      //  if (data.data.message=='session already available' || data.data.message=='session created') {
      setInternalspinner(false);

      let details = data.data.session;
      let arr = {
        _id: details.id,
        id: details.id,
        phonenumber: details.phonenumber,
        email: details.email,
        channel: details.channel,
        chat_type: "internal",
        chat_session_id: details.chat_session_id,
        user_id: details.user_id,
        agent: details.agent,
        reciverDetails: [
          {
            _id: details.receiver_id,
            username: "",
          },
        ],
        senderDetails: [
          {
            _id: details.sender_id,
            username: "",
          },
        ],
      };
      props.setinternalchatnotify(false);
      props.setchatdata(arr);
      props.setchatid(data.data.data.chat_session_id);
      props.setrefreshtogglechat(true);
      props.setselectedmobile(data.data.data.phonenumber);
      props.setselectedemail(data.data.data.email);
      props.setselectedusername(data.data.data.username);
      props.setchattype("WEBCHAT");

      setInternalClientFiltered([]);

      setTimeout(() => {
        getAllInternalAgents("create");
      }, 1000);
    } catch (error) {
      errorHandel(error, "/users/createInternalAgentsSession");
      setTimeout(() => {
        navigate("/");
      }, 2000);
    }
  };

  const getAllInternalAgents = async (val) => {
    try {
      const access_token = localStorage.getItem("access_token");
      props.setinternalchatrefresh(false);
      const datas = await JSON.parse(localStorage.getItem("tokenAgent"));
      let body = {
        create: val,
      };
      const data = await axios.post(
        BaseUrl + `/users/allInternalUsers?agent_id=${datas.id}`,
        body,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );

      let agents = data.data.data;
      setAgentId(datas.id);

      const data1 = await axios.post(
        BaseUrl + "/users/availableInternalAgents",
        {},
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json",
            tenantId: tenantId,
          },
        }
      );
      if (data1) {
        if (data1.data.success) {
          let internalAgentss = data1.data.data;
          let getCurrentAgent = JSON.parse(localStorage.getItem("tokenAgent"));
          let UserId = getCurrentAgent.user_id;
          let filterAgents = internalAgentss.filter((data) => {
            return UserId != data.user_id;
          });
          const nonActiveAgents = filterAgents.filter(
            (f) =>
              !agents.some(
                (d) => d.user_id == f.user_id || d.senderDetails[0]._id == f.id
              )
          );
          setNonActiveAgents(nonActiveAgents);
        }
      }

      props.setAgentList(agents);
      setAgentContactList(agents);
    } catch (error) {
      setShow(true);
      errorHandel(error, "/users/availableInternalAgents");
    }
  };

  // useEffect(() => {
  //   setInterval(() => {
  //     getDataAfterAcceptClient();
  //   }, 1000);
  // }, [props.contactList])

  let randomColors = [
    "#E1F2FB",
    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",
    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",
    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",
    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#E1F2FB",

    "#F3DFE3",
    "#E9B2BC",
    "#F6E5F5",
    "#FBF4F9",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
    "#F3DFE4",
    "#E9B2D4",
    "#F6E5L3",
    "#FBF4T6",
    "#B9CCED",
    "#FEFCF3",
    "#F5EBEO",
    "#FODBDB",
    "#DBA39A",
  ];

  useEffect(() => {
    var tooltipTriggerList = [].slice.call(
      document.querySelectorAll('[data-bs-toggle="tooltip"]')
    );
    tooltipTriggerList.map(function (tooltipTriggerEl) {
      return new Tooltip(tooltipTriggerEl);
    });
  }, []);

  const onClickContactChatActive = (contact, index) => {
    setOldId(contact.id);
    updateCount(contact.id);
    props.setchatid(contact.chat_session_id);
    props.setselectedusername(contact.unique_id.username);
    props.setchatdata(contact);
    props.setchattype("WEBCHAT");
    props.setKnowledgeCategory(contact.category);
    props.setSelectedColor(randomColors[index]);
    props.setselectedmobile(contact.unique_id.phonenumber);
    if(contact.unique_id.email){
      props.setselectedemail(contact.unique_id.email);
    }else{
      props.setselectedemail(contact.email);
      
    }
    props.setselectedwhatsapp(contact.unique_id.whatsappnumber);
    props.setselectedfacebook(contact.unique_id.facebookId);
    props.setselectedtwitter(contact.unique_id.twitterId);
    props.setselectedteams(contact.unique_id.teamsId);
    props.setselectedcompany(contact.unique_id.company);
    props.setselectedadress(contact.unique_id.address);
    props.setselectedid(contact.unique_id.id);
  };

  return (
    <div>
      <div>
        {basedOnRole.role == "Agent" ? (
          <Paper square>
            <Tabs
              value={value}
              textColor="primary"
              indicatorColor="primary"
              onChange={(event, newValue) => {
                setValue(newValue);
                props.setinternalexternal(newValue);
                if (newValue == 1) {
                  console.log("props.agentList", props.agentList);
                  if (props.agentList.length != 0) {
                    props.setSelectedchannel("email");
                    props.setrefreshtogglechat(true);
                    props.setinternalchatnotify(false);
                    props.settogglechat(false);
                    props.setchatdata(props.agentList[0]);
                    props.setchattype("WEBCHAT");
                    props.setchatid(props.agentList[0].chat_session_id);
                    props.setselectedmobile(props.agentList[0].phonenumber);
                    props.setselectedemail(props.agentList[0].email);
                    props.setselectedusername(
                      props.agentList[0].reciverDetails[0]._id != agentId
                        ? props.agentList[0].reciverDetails[0].username
                        : props.agentList[0].senderDetails[0].username
                    );

                    props.setloadmore(false);
                  } else {
                    props.setselectedusername("");
                    props.setchatid("");
                  }
                } else {
                  if (props.contactList[0]) {
                    props.setrefreshtogglechat(true);
                    props.setinternalchatnotify(false);
                    props.settogglechat(false);
                    props.setchatdata(props.contactList[0]);
                    props.setchattype("WEBCHAT");
                    props.setSelectedchannel("email");

                    props.setchatid(props.contactList[0].chat_session_id);
                    props.setselectedmobile(
                      props.contactList[0].unique_id.phonenumber
                    );
                    props.setselectedemail(
                      props.contactList[0].unique_id.email
                    );

                    props.setselectedusername(
                      props.contactList[0].unique_id.username
                    );

                    props.setrefreshtogglechat(true);
                    props.setinternalchatnotify(false);
                    props.settogglechat(true);
                    props.setloadmore(false);
                  } else {
                    props.setselectedusername("");
                    props.setselectedemail("");
                    props.setchatid("");
                    props.setchatdata([]);
                  }
                }
              }}
            >
              {permission[indexNum].screen.map((item) => {
                if (item.screenId == "EXT_CHAT") {
                  return (
                    <TabNew
                      label="External"
                      onClick={props.settogglechat(false)}
                    />
                  );
                }
              })}

              {permission[indexNum].screen.map((item) => {
                if (item.screenId == "IN_CHAT") {
                  return <TabNew label="Internal" />;
                }
              })}

              {props.internalchatnotify && (
                <span className="notifyNewChat"></span>
              )}
            </Tabs>
          </Paper>
        ) : (
          <Paper square>
            <Tabs
              value={value}
              cla
              textColor="primary"
              indicatorColor="primary"
              onChange={(event, newValue) => {
                setValue(newValue);
                props.setinternalexternal(newValue);
                if (newValue == 1) {
                  console.log("props.agentList", props.agentList);
                  if (props.agentList.length != 0) {
                    props.setrefreshtogglechat(true);
                    props.setinternalchatnotify(false);
                    props.settogglechat(false);
                    props.setchatdata(props.agentList[0]);
                    props.setchattype("WEBCHAT");

                    props.setchatid(props.agentList[0].chat_session_id);
                    props.setselectedmobile(props.agentList[0].phonenumber);
                    props.setselectedemail(props.agentList[0].email);

                    props.setselectedusername(
                      props.agentList[0].reciverDetails[0]._id != agentId
                        ? props.agentList[0].reciverDetails[0].username
                        : props.agentList[0].senderDetails[0].username
                    );

                    props.setloadmore(false);
                  } else {
                    props.setselectedusername("");
                    props.setchatid("");
                  }
                } else {
                  if (props.contactList[0]) {
                    props.setrefreshtogglechat(true);
                    props.setinternalchatnotify(false);
                    props.settogglechat(false);
                    props.setchatdata(props.contactList[0]);
                    props.setchattype("WEBCHAT");

                    props.setchatid(props.contactList[0].chat_session_id);
                    props.setselectedmobile(
                      props.contactList[0].unique_id.phonenumber
                    );
                    props.setselectedemail(
                      props.contactList[0].unique_id.email
                    );

                    props.setselectedusername(
                      props.contactList[0].unique_id.username
                    );

                    props.setrefreshtogglechat(true);
                    props.setinternalchatnotify(false);
                    props.settogglechat(true);
                    props.setloadmore(false);
                  } else {
                    props.setselectedusername("");
                    props.setselectedemail("");
                    props.setchatid("");
                    props.setchatdata([]);
                  }
                }
              }}
            >
              {permission[indexNum].screen.map((item) => {
                if (item.screenId == "EXT_CHAT") {
                  return <TabNew label="External" />;
                }
              })}

              {permission[indexNum].screen.map((item) => {
                if (item.screenId == "IN_CHAT") {
                  return <TabNew label="Internal" />;
                }
              })}

              {props.internalchatnotify && (
                <span className="notifyNewChat"></span>
              )}
            </Tabs>
          </Paper>
        )}
      </div>
      {value == 0 && (
        <div
          className="chat-contacts mt-2"
          style={{ overflowY: "scroll", height: "78vh" }}
        >
          {props.contactList2
            .filter((val) => {
              return val.id !== props.contactList2.id;
            })
            .map((client, index) => {
              console.log('incoming request',client)
              return (
                <>
                  <div className="incomecall_details p-2" key={client.id}>
                    <div class="image-paragraph">
                      <div
                        className=" d-flex justify-content-center align-items-center"
                        style={{
                          background: "#fdfdfd",
                          width: "30px",
                          height: "30px",
                          borderRadius: "50%",
                          color: "aliceblue",
                          fontSize: "18px",
                          fontWeight: "bold",
                          position: "relative",
                          boxShadow:
                            "rgb(0 0 0 / 0%) 0px 1px 3px, rgb(0 0 0 / 14%) 0px 1px 2px",
                        }}
                      >
                        <img
                          src={`${
                            client.channel == "webchat"
                              ? chatIcon
                              : client.channel == "from_whatsapp"
                              ? chatIconWhatsapp
                              : client.channel == "from_facebook"
                              ? chatIconfacebook
                              : client.channel == "from_twitter"
                              ? chatIcontwitter
                              : client.channel == "from_teams"
                              ? chatIconteams
                              : emailIcon
                          }`}
                          className={`chat_avaya_img ${
                            index === currentSelected ? "iconActive" : ""
                          }`}
                          style={{
                            height: "23px",
                            width: "23px",
                            margin: "auto",
                          }}
                        />
                      </div>
                      <p>
                        {client.channel == "email"
                          ? `Email Request From ${client.email}`
                          : `Chat Request From ${
                              client.unique_id.username
                                ? client.unique_id.username
                                : client.unique_id.email
                            }`}
                      </p>
                    </div>
                    <div className="row mt-2 list-details">
                      <div className="col-6">
                        <p>Skillset :</p>
                      </div>
                      <div className="col-6">
                        <p>{client.skillset}</p>
                      </div>
                      <div className="col-6">
                        <p>Language :</p>
                      </div>
                      <div className="col-6">
                        <p>{client.language}</p>
                      </div>
                      <div className="col-6">
                        <p>Category :</p>
                      </div>
                      <div className="col-6">
                        <p>{client.category}</p>
                      </div>
                      <div className="col-6">
                        <p>Sentiment :</p>
                      </div>
                      <div className="col-6">
                        <p>{client.sentiment}</p>
                      </div>
                      <div className="col-6">
                        <p>Wait Time :</p>
                      </div>
                      <div className="col-6">
                        <p>{moment(client.arrival_at).fromNow()}</p>
                      </div>
                    </div>
                    <div className="request-button">
                      <button
                        type="button "
                        className="btn btn-success button"
                        id="accept_client_chat"
                        onClick={() => acceptClient(client, index)}
                        style={{ width: "120px" }}
                      >
                        <p style={{ color: "#ffff" }}>Accept</p>
                      </button>
                      <button
                        type="button"
                        className="btn btn-danger button"
                        id="reject_client_chat"
                        onClick={() =>
                          rejectchat(
                            client.chat_session_id,
                            client.agent,
                            client.id,
                            client.skillset,
                            client.language,
                            client.phonenumber,
                            client.channel
                          )
                        }
                        style={{ width: "120px" }}
                      >
                        <p style={{ color: "#ffff" }}>Reject</p>
                      </button>
                    </div>
                  </div>
                </>
              );
            })}

          {props.contactList.length > 0 ? (
            props.contactList.map(
              (contact, index) => {
              console.log('contactwwwwwwwwwwwww',contact)
                if (contact.channel == "email") {
                  return (
                    <div
                      key={contact.id}
                      className={
                        contact != undefined
                          ? chatid == contact.chat_session_id
                            ? "chat-contact d-flex p-2 justify-content-evenly align-items-center border-bottom chat-active"
                            : "chat-contact d-flex p-2 justify-content-evenly align-items-center border-bottom"
                          : ""
                      }
                      onClick={() => {
                        props.setSelectedchannel(contact.channel);
                        onClickContactChatActive(contact, index);
                        props.setinternalchatnotify(false);
                        props.setrefreshtogglechat(true);
                        props.setloadmore(false);
                        setEmailid(contact.emailId);
                        fetchSingleEmail(contact.emailId);
                        props.settogglechat(false);
                        // props.setSelectedchannel('email')
                      }}
                    >
                      <div className="d-flex justify-content-between w-100">
                        <div className="d-flex  me-1">
                          <div
                            className="me-2 position-relative"
                            style={{
                              width: "40px",
                              height: "40px",
                              background: randomColors[index],
                              borderRadius: "50%",
                            }}
                          >
                            <p
                              className="my-auto chat_name"
                              style={{ color: "#7e7272", textAlign: "center" }}
                            >
                              <b
                                className="h-100"
                                style={{
                                  color: "#7e7272",
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center",
                                }}
                              >
                                {contact.email === ""
                                  ? contact.email.slice(1, 3)
                                  : contact.email
                                  ? contact.email.slice(0, 2).toUpperCase()
                                  : ""}
                              </b>
                            </p>

                            {contact.channel === "email" ? (
                              <div className="chat-social mail-bg d-flex justify-content-center align-items-center">
                                <i
                                  style={{ fontSize: 9 }}
                                  className="fa-solid fa-envelope"
                                ></i>
                              </div>
                            ) : (
                              ""
                            )}
                          </div>
                          <div className="chat-content-main d-flex">
                            <div className="chat-details me-1">
                              <div className="d-flex align-items-center">
                                <div
                                  className="name d-inline-block text-truncate me-1"
                                  style={{ width: "110px" }}
                                >
                                  {contact.email === ""
                                    ? contact.email
                                    : contact.email}
                                </div>

                                {/* <div>
                                <span>
                                  <IoMdOpen style={{ cursor: 'pointer' }} onClick={handelOpenmail} />
                                </span>
                              </div> */}
                              </div>
                              <div className="d-flex align-items-center">
                                <p
                                  className="d-inline-block text-truncate mb-0 chat_mesge"
                                  style={{
                                    width: "100px",
                                    fontFamily: "poppins",
                                    fontSize: "12px",
                                  }}
                                >
                                  {contact.lastmessage}
                                </p>
                              </div>

                              {openmail ? (
                                <Draggable>
                                  <div
                                    className="card"
                                    style={{
                                      height: "30rem",
                                      width: "35rem",
                                      position: "absolute",
                                      left: "10rem",
                                      top: "3rem",
                                      zIndex: 9999,
                                    }}
                                  >
                                    <div className="header p-1 d-flex justify-content-between align-items-center">
                                      <div>
                                        {/* <h1>{this.state.id}</h1> */}
                                      </div>
                                      <div
                                        className=""
                                        style={{ paddingTop: "10px" }}
                                      >
                                        <MdOutlineClose
                                          style={{ cursor: "pointer" }}
                                          onClick={handelClosemail}
                                        />
                                      </div>
                                    </div>
                                    <hr />
                                    <div
                                      className="sub_header d-flex w-100 justify-content-around align-items-center"
                                      style={{ height: "40px", width: "100vw" }}
                                    >
                                      <div>Add-Ins</div>

                                      <div>
                                        <div
                                          className="d-flex"
                                          style={{
                                            width: "15rem",
                                            height: "50px",
                                          }}
                                        >
                                          <table
                                            style={{
                                              border: "1px solid black",
                                            }}
                                          >
                                            <tr
                                              style={{
                                                border: "1px solid black",
                                              }}
                                            >
                                              <td className="popup_div">
                                                <FaArrowUp
                                                  size={18}
                                                  className="d-flex mx-auto"
                                                  onClick={
                                                    handelEscalationModalOpen
                                                  }
                                                />
                                                <div
                                                  style={{ marginTop: "3px" }}
                                                  className="table_data"
                                                >
                                                  Escalation
                                                </div>
                                              </td>

                                              <td className="popup_div">
                                                <DoNotTouchIcon
                                                  size={10}
                                                  className="d-flex mx-auto"
                                                />
                                                <div className="table_data">
                                                  Cannot Handle
                                                </div>
                                              </td>

                                              <td className="popup_div">
                                                <RiMailForbidLine
                                                  size={20}
                                                  className="d-flex mx-auto"
                                                />
                                                <div
                                                  className="table_data"
                                                  style={{ marginTop: "3px" }}
                                                >
                                                  NoAction Email
                                                </div>
                                              </td>
                                            </tr>
                                          </table>
                                        </div>
                                      </div>
                                    </div>
                                    <hr />
                                    <div
                                      className=""
                                      style={{
                                        height: "90vh",
                                        overflowY: "auto",
                                        marginBottom: "",
                                      }}
                                    >
                                      <div className="p-3">
                                        <div>
                                          {/* {!_.isEmpty(inboxContentAttchment) ?
                                                            this.EmailAttachmentContent(inboxContentAttchment)
                                                            : null} */}

                                          <div className="mailcontent">
                                            <div className="email-body">
                                              {/* {parse(processedHtml)} */}
                                            </div>
                                            <div className="email-action">
                                              <button className="btn btn-base">
                                                {" "}
                                                <img src={reply} /> Reply &nbsp;
                                              </button>
                                              <button className="btn btn-base">
                                                {" "}
                                                <img src={replyall} /> Reply All
                                                &nbsp;
                                              </button>
                                              <button className="btn btn-base">
                                                {" "}
                                                <img src={forward} /> Forward
                                              </button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </Draggable>
                              ) : (
                                ""
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                } else {
                  return (
                    <div
                      key={contact.id}
                      className={
                        contact != undefined
                          ? chatid == contact.chat_session_id
                            ? "chat-contact d-flex p-2 justify-content-evenly align-items-center border-bottom chat-active"
                            : "chat-contact d-flex p-2 justify-content-evenly align-items-center border-bottom"
                          : ""
                      }
                      onClick={() => {
                        onClickContactChatActive(contact, index);
                        props.setinternalchatnotify(false);
                        props.setrefreshtogglechat(true);
                        props.setloadmore(false);
                        props.setSelectedchannel(contact.channel);
                      }}
                    >
                      <div className="d-flex justify-content-between w-100">
                        <div className="d-flex  me-1">
                          <div
                            className="me-2 position-relative"
                            style={{
                              width: "40px",
                              height: "40px",
                              background: randomColors[index],
                              borderRadius: "50%",
                            }}
                          >
                            <p
                              className="my-auto chat_name"
                              style={{ color: "#7e7272", textAlign: "center" }}
                            >
                              <b
                                className="h-100"
                                style={{
                                  color: "#7e7272",
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center",
                                }}
                              >
                                {contact.unique_id.username === ""
                                  ? contact.unique_id.phonenumber.slice(1, 3)
                                  : contact.unique_id.username
                                  ? contact.unique_id.username
                                      .slice(0, 2)
                                      .toUpperCase()
                                  : ""}
                              </b>
                            </p>

                            {contact.channel === "webchat" ? (
                              <div className="chat-social web-bg d-flex justify-content-center align-items-center">
                                <i
                                  style={{ fontSize: 9 }}
                                  className="fa-brands fa-rocketchat"
                                ></i>
                              </div>
                            ) : contact.channel === "instagram" ? (
                              <div className="inst-bg chat-social  d-flex justify-content-center align-items-center">
                                <i
                                  style={{ fontSize: 9 }}
                                  className="fa-brands fa-instagram"
                                ></i>
                              </div>
                            ) : contact.channel === "from_whatsapp" ? (
                              <div className="what-bg chat-social  d-flex justify-content-center align-items-center">
                                <i
                                  style={{ fontSize: 9 }}
                                  className="fa-brands fa-whatsapp"
                                ></i>
                              </div>
                            ) : contact.channel === "voice" ? (
                              <div className="what-bg chat-social  d-flex justify-content-center align-items-center">
                                <i
                                  style={{ fontSize: 9 }}
                                  className="fa-sharp fa-solid fa-phone-volume"
                                ></i>
                              </div>
                            ) : contact.channel === "from_facebook" ? (
                              <div className="fb-bg chat-social d-flex justify-content-center align-items-center">
                                <i
                                  style={{ fontSize: 9 }}
                                  className="fa-brands fa-facebook-f"
                                ></i>
                              </div>
                            ) : contact.channel === "from_twitter" ? (
                              <div className="twit-bg chat-social  d-flex justify-content-center align-items-center">
                                <i
                                  style={{ fontSize: 9 }}
                                  className="fa-brands fa-twitter"
                                ></i>
                              </div>
                            ) : contact.channel === "voice" ? (
                              <div className="twit-bg chat-social  d-flex justify-content-center align-items-center">
                                <i
                                  style={{ fontSize: 9 }}
                                  className="fa-solid fa-phone"
                                ></i>
                              </div>
                            ) : (
                              <div className="teams-bg chat-social  d-flex justify-content-center align-items-center">
                                <SiMicrosoftteams
                                  className="btn_hover"
                                  size={9}
                                  color="white"
                                />
                              </div>
                            )}
                          </div>
                          <div className="chat-content-main d-flex">
                            <div className="chat-details me-1">
                              <div className="d-flex align-items-center">
                                <div
                                  className="name d-inline-block text-truncate me-1"
                                  style={{ width: "110px" }}
                                >
                                  {contact.unique_id.username === ""
                                    ? contact.unique_id.phonenumber
                                    : contact.unique_id.username}
                                </div>
                                {contact.is_customer_disconnected === true ? (
                                  <div className="discont"></div>
                                ) : (
                                  <div className="online"></div>
                                )}
                                {/* <div className="discont"></div> */}
                              </div>
                              <div className="d-flex align-items-center">
                                <p
                                  className="d-inline-block text-truncate mb-0 chat_mesge"
                                  style={{
                                    width: "100px",
                                    fontFamily: "poppins",
                                    fontSize: "12px",
                                  }}
                                >
                                  {contact.lastmessage}
                                </p>
                                <div className="position-relative top container">
                                  {contact.unreadcount != 0 && (
                                    <Badge
                                      pill
                                      style={{ fontSize: 9 }}
                                      variant="danger"
                                    >
                                      {contact.unreadcount}
                                    </Badge>
                                  )}
                                </div>
                              </div>
                              {/* <div className="position-relative top container" style={{left:140,bottom:20}}><Badge pill variant='danger'>{contact.unreadcount}</Badge></div> */}
                            </div>
                          </div>
                        </div>

                        <div className="chat-time d-flex justify-content-start flex-column align-items-end">
                          <p className="mb-0">
                            {/* Last seen: <ReactTimeAgo data={contact.lastmessagetime}/> */}
                            {moment(contact.lastmessageUpdatedat).fromNow()}
                            {/* {contact.lastmessagetime} */}
                            {/* <TimeAgo size={'10px'} date={contact.lastmessageUpdatedat} /> */}
                          </p>

                          <span
                            className="d-flex "
                            style={{ flexDirection: "column" }}
                          >
                            {contact.conference ? (
                              <span
                                className="chat_type"
                                style={{
                                  fontSize: 7,
                                  fontWeight: "bold",
                                  color: "gray",
                                  marginLeft: "",
                                }}
                              >
                                Conference
                              </span>
                            ) : (
                              ""
                            )}
                            {contact.transferred ? (
                              <span
                                className="chat_type"
                                style={{
                                  fontSize: 7,
                                  fontWeight: "bold",
                                  color: "gray",
                                  marginLeft: "",
                                }}
                              >
                                Transfered
                              </span>
                            ) : (
                              ""
                            )}
                          </span>
                        </div>
                      </div>
                    </div>
                  );
                }
              }

              //WEB CHAT CONDITION STARTED
            )
          ) : (
            <div className="" style={{ marginTop: "70%" }}>
              <div
                className="text-muted d-flex justify-content-center"
                style={{ flexDirection: "column" }}
              >
                <span className="mx-auto">
                  <RiErrorWarningLine size={30} />
                </span>
                <span className="mx-auto">
                  <h6
                    className="text-muted mt-2"
                    style={{ fontFamily: "poppins" }}
                  >
                    No Chat Found
                  </h6>
                </span>
              </div>
            </div>
          )}
        </div>
      )}

      {value == 1 && (
        <div>
          {internalspinner && (
            <div
              className="d-flex justify-content-center position-absolute h-100"
              style={{
                background: "white",
                opacity: "0.8",
                zIndex: 999,
                width: "92%",
              }}
            >
              <CSpinner
                color="primary"
                style={{ top: "25%", position: "relative" }}
              />
            </div>
          )}
          <div className="searchInternalAgent">
            <input
              type="text"
              placeholder="Search available agents"
              onChange={(e) => {
                setSearchterm(e.target.value);
              }}
            ></input>
          </div>

          {internalagentloader ? (
            <div className="d-flex justify-content-center align-items-center h-75">
              <CSpinner color="primary" className="my-auto" />{" "}
            </div>
          ) : (
            <div>
              {internalClientFiltered.length > 0 && (
                <span className="search-text">Search Results:</span>
              )}
              <div
                className="chat-contacts mt-2"
                style={{ overflowY: "scroll", height: "76vh" }}
              >
                <div style={{ marginLeft: "40%" }}></div>
                {internalClientFiltered.length > 0 && (
                  <div style={{ borderBottom: "1px solid #fff" }}>
                    {/* {internalClientFiltered
                      .slice(0)
                      .reverse()
                      .map((client, index) => {
                        return (
                          <div
                            key={client.id}
                            className={`list-chat internal-list-chat
                                ${index === currentSelected ? chatAsActive : ""}
                                `}
                            style={{
                              marginTop: `${index === currentSelected ? alignChatIfCall : ""
                                }`,
                            }}
                            onClick={() => {
                              setInternalspinner(true)
                              setTimeout(() => {
                                createInternalChat(client.user_id, client.id);
                            
                              }, 2000);
                            }}
                          >
                            <div className="space-member internal-space-mem space-member-dm m-0">
                              <span
                                style={{
                                  width: 35,
                                  height: 35,
                                  background: randomColors[index],
                                  borderRadius: 35,
                                  justifyContent: "center",
                                  display: "flex",
                                }}
                              >
                                <p
                                  style={{
                                    display: "flex",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    fontWeight: "bold",
                                    color: "white",
                                  }}
                                  className="my-auto"
                                >
                                  {client
                                    ? client.username.slice(0, 2).toUpperCase()
                                    : ""}
                                </p>
                              </span>

                              <span
                                data-id={`${client.agent_status}`}
                                className={`${client.agent_status == "Available"
                                    ? "internalAgentName"
                                    : "internalAgentNameNotAvail"
                                  }`}
                              ></span>
                            </div>
                            <div className="chat-name w-100">
                              <div className="d-flex">
                                <div className="w-100">
                                  <p className="space-member__displayname">
                                    {client ? client.username : ""}
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })} */}
                  </div>
                )}
                {props.agentList
                  .filter((val) => {
                    if (searchterm == "") {
                      return val;
                    } else if (
                      val.reciverDetails[0].username 
                        .toLowerCase()
                        .includes(searchterm.toLowerCase()) && val.reciverDetails[0].username != localStorage.getItem('NameDisplay')
                    ) {
                      return val;
                    } else if (
                      val.senderDetails[0].username
                        .toLowerCase()
                        .includes(searchterm.toLowerCase()) &&  val.reciverDetails[0].username != localStorage.getItem('NameDisplay')
                    ) {
                      return val;
                    }
                  })
                  .map((val, index) => {
                    console.log('agenList',val)
                    return (
                      <div
                        className={
                          chatid === val.chat_session_id
                            ? "chat-contact d-flex p-2 justify-content-evenly align-items-center border-bottom chat-active"
                            : "chat-contact d-flex p-2 justify-content-evenly align-items-center border-bottom"
                        }
                      >
                        <div
                          className="d-flex justify-content-between w-100"
                          onClick={() => {
                            setOldId(val.id);
                            updateCount1(val.id);
                            props.setchatdata(val);
                            props.setinternalchatnotify(false);
                            props.setchatid(val.chat_session_id);
                            props.setrefreshtogglechat(true);
                            props.setselectedmobile(val.phonenumber);
                            props.setselectedemail(val.email);
                            props.setselectedusername(
                              val.reciverDetails[0]._id != agentId
                                ? val.reciverDetails[0].username
                                : val.senderDetails[0].username
                            );

                            props.setSelectedchannel("webchat");
                            props.setchattype("WEBCHAT");
                            props.setSelectedColor(randomColors[index]);
                            props.setloadmore(false);
                            // setInternalspinner(true)
                          }}
                        >
                          <div className="d-flex me-1">
                            <div
                              style={{
                                width: "40px",
                                height: "40px",
                                background: randomColors[index],
                                borderRadius: "50%",
                              }}
                            >
                              <b className="d-flex justify-content-center align-items-center h-100">
                                {val.reciverDetails[0]._id != agentId
                                  ? val.reciverDetails[0].username
                                      .slice(0, 2)
                                      .toUpperCase()
                                  : val.senderDetails[0].username
                                      .slice(0, 2)
                                      .toUpperCase()}

                                {/* {val.reciverDetails[0] ? val.reciverDetails[0].username.slice(0, 2).toUpperCase():""} */}

                                {/* {val.reciverDetails[0] ? val.reciverDetails[0].username.slice(0, 2).toUpperCase():""} */}
                              </b>
                            </div>

                            <div className="chat-content-main d-flex">
                              <div className="chat-details me-1">
                                <div className="d-flex align-items-center">
                                  <div
                                    className="name d-inline-block text-truncate me-1 my-2 mx-1"
                                    style={{ width: "115px" }}
                                  >
                                    {val.reciverDetails[0]._id != agentId
                                      ? val.reciverDetails[0].username
                                      : val.senderDetails[0].username}
                                  </div>

                                  <div
                                    className={`${
                                      val.reciverDetails[0]._id != agentId
                                        ? val.reciverDetails[0].agent_status ==
                                          "Available"
                                          ? "online"
                                          : val.reciverDetails[0]
                                              .agent_status == "Not Available"
                                          ? "offline"
                                          : "busy"
                                        : val.senderDetails[0].agent_status ==
                                          "Available"
                                        ? "online"
                                        : val.senderDetails[0].agent_status ==
                                          "Not Available"
                                        ? "offline"
                                        : "busy"
                                    }`}
                                  ></div>
                                </div>
                                <div className="d-flex align-items-center">
                                  <p
                                    className="d-inline-block text-truncate mb-0"
                                    style={{ width: "120px" }}
                                  >
                                    {val.lastmessage}
                                  </p>

                                  {val.unreadcount != 0 && (
                                    <Badge
                                      variant="danger"
                                      className="position-relative "
                                      pill
                                    >
                                      {val.unreadcount}
                                    </Badge>
                                  )}
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="chat-time d-flex justify-content-start flex-column align-items-end text-wrap text-break ">
                            <p
                              className="mb-0 text-wrap text-break"
                              style={{ width: "123px" }}
                            >
                              {" "}
                              {val.lastmessagetime}
                            </p>
                          </div>
                        </div>
                      </div>
                    );
                  })}

                <div style={{ marginBottom: "15vh" }}>
                  {nonActiveAgents
                    .filter((client) => {
                      if (searchterm == "") {
                        return client;
                      } else if (
                        client.username
                          .toLowerCase()
                          .includes(searchterm.toLowerCase())
                      ) {
                        return client;
                      }
                    })
                    .map((client, index) => {
                      // .slice(0)
                      // .reverse()
                      // .map((client, index) => {
                      return (
                        <div
                          className="chat-contact d-flex p-2 justify-content-evenly align-items-center border-bottom"
                          onClick={() => {
                            setInternalspinner(true);
                            setTimeout(() => {
                              createInternalChat(client.user_id, client.id);
                            }, 2000);
                          }}
                        >
                          <div className="d-flex justify-content-between w-100">
                            <div className="d-flex me-1">
                              <div
                                style={{
                                  width: "40px",
                                  height: "40px",
                                  background: randomColors[index],
                                  borderRadius: "50%",
                                }}
                              >
                                <b className="d-flex justify-content-center align-items-center h-100">
                                  {client
                                    ? client.username.slice(0, 2).toUpperCase()
                                    : ""}
                                </b>
                              </div>

                              <div className="chat-content-main d-flex">
                                <div className="chat-details me-1">
                                  <div className="d-flex align-items-center">
                                    <div className="name d-inline-block text-truncate me-1 my-2 mx-1">
                                      {client ? client.username : ""}
                                    </div>
                                    <div
                                      className={`${
                                        client.agent_status == "Available"
                                          ? "online"
                                          : client.agent_status ==
                                            "Not Available"
                                          ? "offline"
                                          : "busy"
                                      }`}
                                    ></div>
                                  </div>
                                  <p className="d-inline-block text-truncate mb-0"></p>
                                </div>
                              </div>
                            </div>

                            <div className="chat-time d-flex justify-content-start flex-column align-items-end"></div>
                          </div>
                        </div>
                      );
                    })}
                </div>
              </div>
            </div>
          )}
        </div>
      )}

      {/* For Email  */}

      <Modal
        show={escalationModal}
        className="transferCall-modal"
        onHide={handelEscalationModalClose}
        size="md"
      >
        <Modal.Header
          style={{
            padding: "6px 12px",
            margin: 0,
            fontSize: "12px",
            height: "45px",
            backgroundColor: "#294e9f",
            color: "white",
          }}
        >
          <div className="d-flex justify-content-between align-items-center w-100">
            <div>
              <Modal.Title
                style={{
                  fontSize: 15,
                  margin: "6px 0 0 0",
                  textTransform: "capitalize",
                }}
              >
                Reassign To Supervisor
              </Modal.Title>
            </div>
            <div>
              <AiOutlineCloseCircle
                onClick={handelEscalationModalClose}
                style={{ cursor: "pointer" }}
              />
            </div>
          </div>
        </Modal.Header>
        <Modal.Body>
          {" "}
          <div className="d-flex justify-content-between"></div>
          <select
            className="form-select form-select-sm mt-2"
            name="availableAgent"
            // value={selectedValue}
            // onChange={this.handleChangeSupervisorList}
          >
            <option value="" selected disabled>
              Available Supervisor
            </option>

            {/* {supervisorList.map((item) => {
                                                return (
                                                    <option id={item.id} value={item.user_id}  >
                                                        {item.username}
                                                    </option>
                                                );
                                            })} */}
          </select>
        </Modal.Body>

        <div className="transferOkDiv">
          <Button variant="primary" className="transferOkBtn">
            Reassign To Supervisor
          </Button>
        </div>
      </Modal>

      {/* For Email */}

      <Modal
        show={show}
        onHide={handleClose}
        centered
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header style={{ height: "3rem", backgroundColor: "#294e9f" }}>
          <div className="d-flex justify-content-center align-items-center color-white fw-bold">
            <span>Alert</span>
          </div>
        </Modal.Header>
        <Modal.Body className="fw-bold">
          Session Expired Please Login Again !
        </Modal.Body>

        <button className="btn btn-danger w-25 btn-sm ms-auto" onClick={logout}>
          Logout
        </button>
      </Modal>

      {/* transfer call */}
      <Modal show={showTransferMod} onHide={handleTransferClose}>
        <Modal.Header
          closeButton
          style={{
            padding: "6px 12px",
            margin: 0,
            fontSize: "12px",
            height: "45px",
          }}
        >
          <Modal.Title style={{ fontSize: 20 }}>Transfer Agent</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {" "}
          <select
            className="clientChatLogin"
            name="availableAgent"
            onChange={(e) => setAgentTransferId(e.target.value)}
          >
            <option value="agent" selected>
              Available Agent
            </option>
            {availAgent.map((agents) => {
              return <option value={agents.id}>{agents.username}</option>;
            })}
          </select>
        </Modal.Body>

        <Button
          variant="primary"
          className="transferOkBtn"
          onClick={transferAgentSubmit}
        >
          Transfer
        </Button>
      </Modal>
    </div>
  );
}
export default connect(mapStateToProps, {
  setselectedusername,
  setselectedwhatsapp,
  setselectedfacebook,
  setselectedtwitter,
  setselectedteams,
  setselectedcompany,
  setselectedadress,
  setselectedid,
  setselectedemail,
  setselectedmobile,
  setrefreshtogglechat,
  setselecteduserimage,
  setchatdata,
  setchatid,
  setchatduration,
  setchattype,
  setcontactlist,
  setcontactlist2,
  setonloadContactRefresh,
  setconfchatrefresh,
  setinternalexternal,
  settogglechat,
  setAgentList,
  setinternalchatrefresh,
  setinternalchatnotify,
  setSelectedColor,
  setloadmore,
  setSelectedchannel,
  setSelectedchanneldata,
  setUpdatechanneldata,
  setKnowledgeCategory,
})(Contacts);
