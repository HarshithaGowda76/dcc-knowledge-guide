import React, { useEffect, useRef, useState } from "react";
import "./Login.css";
import inaipi from "../../assets/img/Inaipi_logo.png";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { BaseUrl, errorApi } from "../../containers/Page/Constants/BaseUrl";
import { connect } from "react-redux";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import { browserName } from "react-device-detect";
import Cookies from "universal-cookie";
import { Button, Modal } from "react-bootstrap";
import { AiOutlineCloseCircle } from "react-icons/ai";
import {
  setselectedusername,
  setselecteduserimage,
  setchatdata,
  setchattype,
  setselectedemail,
  setselectedmobile,
  setchatid,
  setchatduration,
  setrefreshtogglechat,
  setcontactlist,
  setcontactlist2,
  setonloadContactRefresh,
  setconfchatrefresh,
  setinternalexternal,
  settogglechat,
  setAgentList,
  setinternalchatnotify,
  setinternalchatrefresh,
} from "../../redux/actions/spaceActions";
const mapStateToProps = (state) => {
  const { data } = state;
  return {
    contactList: data.contactList,
  };
};

const Login = (props) => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [emailval, setEmailval] = useState(false);
  const [passval, setPassval] = useState(false);
  const [isShown, setIsSHown] = useState(false);
  const [validate, setValidate] = useState(false);
  const [spinner, setSpinner] = useState(false);
  const [eye, setEye] = useState(false);
  const [showTransferMod, setShowTransferMod] = useState(false);
  const [userid, setuserid] = useState("");
  const ref = useRef(null);
  const cookies = new Cookies();

  window.onpopstate = () => {
    navigate('/dashboard');
  };

  const errorHandel = async (error,endpoint) => {
    const tenantID = localStorage.getItem("TenantId");
    try {
      const payload = {
        servicename: 'DCCCHAT',
        logs:error,
        description:endpoint

      };

      const header = {
        headers: {
          // Authorization: "Bearer " + token,
          // "Content-Type": "application/json",
          tenantid: tenantID,
        },
      };
      const { data } = await axios.post(
        errorApi + "/UIlogs/createLogs",
        payload,
        header
      );
      if (data.status) {
        console.log("from error api", data);
      }
    } catch (error) {
      console.log('error',error)
    }
  };


  const permission = [

   

    {

      "createdDate": "2023-01-10T11:29:48.343",

      "lastModifiedDate": "2023-01-10T11:29:48.343",

      "createdBy": "pragadeeswar.j@cognicx.com",

      "modifiedBy": "pragadeeswar.j@cognicx.com",

      "moduleScreenId": "63bd4c2c9a59460443db1179",

      "moduleId": "CHAT",

      "moduleName": "Chat",

      "screen": [


        {

          "screenId": "EXT_CHAT",

          "screenName": "External Chat",

          "read": true,

          "write": true

        },


        {

          "screenId": "IN_CHAT",

          "screenName": "Internal Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "TRANSFER",

          "screenName": "Transfer Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "CONF",

          "screenName": "Chat Conference",

          "read": true,

          "write": true

        },



      ],

      "appName": "Agent Desktop"

    },

    {

      "createdDate": "2023-01-10T11:29:48.343",

      "lastModifiedDate": "2023-01-10T11:29:48.343",

      "createdBy": "pragadeeswar.j@cognicx.com",

      "modifiedBy": "pragadeeswar.j@cognicx.com",

      "moduleScreenId": "63bd4c2c9a59460443db1179",

      "moduleId": "CALENDER",

      "moduleName": "Calender",

      "screen": [

        {

          "screenId": "IN_CHAT",

          "screenName": "Internal Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "EXT_CHAT",

          "screenName": "External Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "TRANSFER",

          "screenName": "Transfer Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "CONF",

          "screenName": "Chat Conference",

          "read": true,

          "write": true

        }

      ],

      "appName": "Agent Desktop"

    },

    {

      "createdDate": "2023-01-10T11:29:48.343",

      "lastModifiedDate": "2023-01-10T11:29:48.343",

      "createdBy": "pragadeeswar.j@cognicx.com",

      "modifiedBy": "pragadeeswar.j@cognicx.com",

      "moduleScreenId": "63bd4c2c9a59460443db1179",

      "moduleId": "DASHBOARD",

      "moduleName": "Dashboard",

      "screen": [

        {

          "screenId": "IN_CHAT",

          "screenName": "Internal Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "EXT_CHAT",

          "screenName": "External Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "TRANSFER",

          "screenName": "Transfer Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "CONF",

          "screenName": "Chat Conference",

          "read": true,

          "write": true

        }

      ],

      "appName": "Agent Desktop"

    },

    {

      "createdDate": "2023-01-10T11:29:48.343",

      "lastModifiedDate": "2023-01-10T11:29:48.343",

      "createdBy": "pragadeeswar.j@cognicx.com",

      "modifiedBy": "pragadeeswar.j@cognicx.com",

      "moduleScreenId": "63bd4c2c9a59460443db1179",

      "moduleId": "SESSIONREPORT",

      "moduleName": "Sessionreport",

      "screen": [

        {

          "screenId": "IN_CHAT",

          "screenName": "Internal Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "EXT_CHAT",

          "screenName": "External Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "TRANSFER",

          "screenName": "Transfer Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "CONF",

          "screenName": "Chat Conference",

          "read": true,

          "write": true

        }

      ],

      "appName": "Agent Desktop"

    },

    {

      "createdDate": "2023-01-10T11:29:48.343",

      "lastModifiedDate": "2023-01-10T11:29:48.343",

      "createdBy": "pragadeeswar.j@cognicx.com",

      "modifiedBy": "pragadeeswar.j@cognicx.com",

      "moduleScreenId": "63bd4c2c9a59460443db1179",

      "moduleId": "AGENTREPORT",

      "moduleName": "Agentreport",

      "screen": [

        {

          "screenId": "IN_CHAT",

          "screenName": "Internal Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "EXT_CHAT",

          "screenName": "External Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "TRANSFER",

          "screenName": "Transfer Chat",

          "read": true,

          "write": true

        },

        {

          "screenId": "CONF",

          "screenName": "Chat Conference",

          "read": true,

          "write": true

        }

      ],

      "appName": "Agent Desktop"

    },

  ]



  useEffect(() => {
    let tenantID = window.location.search
    const urlParams = new URLSearchParams(tenantID);
    const param1 = urlParams.get('tenantID');
    localStorage.setItem("TenantId", param1)
  }, [])

  const tenantId = localStorage.getItem('TenantId')

  const handleTransferClose = () => {
    setShowTransferMod(false);
  };
  const logout = async () => {

    try {
      
      const update = await axios.post(
        BaseUrl + "/users/logout/" + userid
      );
  
      localStorage.clear();
      window.location.reload();
    } catch (error) {
      errorHandel(error,'/users/logout/')
      
    }
    // Clear localStorage items

  };
  useEffect(async () => {
    var userData = cookies.get("username");
    var passwordData = cookies.get("password");

    if (userData && passwordData) {
      setEmail(userData);
      setPass(passwordData);

      // setChecked(true)
    } else {
      setEmail("");
      setPass("");
    }



   

    const queryParameters = new URLSearchParams(window.location.search);
    const email = queryParameters.get("email");
    const pass = queryParameters.get("password");
    const number = queryParameters.get("number");

    if (email && pass) {
      const data_num = await axios.post(BaseUrl + "/users/getInfoBasedOnNum", {
        number,
      });
      let details = data_num.data;
      if (details.data.length != 0) {
        let user_info = details.data[0];
        props.setinternalchatnotify(false);
        props.setchatdata(user_info);
        props.setchatid(user_info.chat_session_id);
        props.setrefreshtogglechat(true);
        props.setselectedmobile(user_info.unique_id.phonenumber);
        props.setselectedemail(user_info.unique_id.email);
        props.setselectedusername(user_info.unique_id.username);
        props.setchattype("WEBCHAT");
      }

      setSpinner(true);
      var datas = await axios.post(BaseUrl + "/users/login", {
        email,
        pass,
      });

      if (datas.data.status === true) {
        console.log(datas.data.status);
        localStorage.setItem("NameDisplay", datas.data.user.username);
        localStorage.setItem("NameDisplay", datas.data.user.username);
        localStorage.setItem("emailDisplay", datas.data.user.email);
        localStorage.setItem("tokenAgent", JSON.stringify(datas.data.user));
        setSpinner(false);
        setValidate(false);
        navigate('/dashboard');
        console.log(props.contactList);
        // return;
      } else {
        if (datas.data.user_id != undefined) {
          setValidate(true);
          setSpinner(false);
          setShowTransferMod(true);

          setuserid(datas.data.user_id);
        } else {
          cookies.remove("username");
          cookies.remove("password");
          cookies.remove("boolean");
        }
      }
    }
  }, []);

  





  const handleChange = () => {
    console.log("checkingcookie", ref.current.checked);
    if (ref.current.checked == false) {
      cookies.remove("username");
      cookies.remove("password");
      cookies.remove("boolean");
    }
  };

  const togglePassword = () => {
    setIsSHown((isShown) => !isShown);
    setEye(true);
  };


  const login = async (e) => {
    console.log('ref', ref)
    setSpinner(true);
    try {
      var datas = await axios.post(BaseUrl + "/users/login", {
        email,
        pass,
      }, {
        headers: {
          tenantId: tenantId
        }
      });


      if (datas.data.status === true) {
        localStorage.setItem("NameDisplay", datas.data.user.username);
        localStorage.setItem("emailDisplay", datas.data.user.email);
        localStorage.setItem("access_token", datas.data.access_token_cognicx);
        localStorage.setItem("tokenAgent", JSON.stringify(datas.data.user));
        localStorage.setItem("AvayaUsername", datas.data.user.voiceRecord.avayaUserName);
        localStorage.setItem("AvayaPassword", datas.data.user.voiceRecord.avayaPassword);
        localStorage.setItem("AvayaDomain", datas.data.user.voiceRecord.avayaDomain);
        localStorage.setItem("permission", JSON.stringify(permission));




        setSpinner(false);
        setValidate(false);

        if (ref.current.checked) {
          cookies.set("username", email, { path: "/" });
          cookies.set("password", pass, { path: "/" });
          cookies.set("boolean", true, { path: "/" });
        }

        if (permission.find((element) => element.moduleName === "Dashboard")) {

          navigate('/dashboard')
        } else if (permission.find((element) => element.moduleName === "Chat")) {
          navigate('/chat')

        } else if (permission.find((element) => element.moduleName === "Session Report")) {
          navigate('/sessionreport')

        } else if(permission.find((element) => element.moduleName === "Calender")) {
          navigate('/calender')
        }


        // navigate('/chat');


        window.location.reload();
      } else {

        if (datas.data.user_id != undefined) {
          setValidate(true);
          setSpinner(false);
          setShowTransferMod(true);
          setuserid(datas.data.user_id);
        } else {
          cookies.remove("username");
          cookies.remove("password");
          cookies.remove("boolean");
          setValidate(true);
          setSpinner(false);
          toast.error(datas.data.message, toastOptions);
        }

        // window.location.reload();
      }
    } catch (error) {
      
      errorHandel(error,'/users/login')

      toast.warn('Server is down,please try after some time', {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }

  };
  const toastOptions = {
    position: "top-right",
    autoClose: 5000,
    pauseOnHover: true,
    draggable: true,
    theme: "light",
  };
  // render(){
  const isLogin = true;

  const handleKeyPress = (event) => {
    if (event.keyCode === 13 || event.which === 13) {
      login();
    }
  };
  return (
    <>
      <ToastContainer />
      <section className="banner set-bg">
        <div className="login-welcome">
          <div className="container-fluid">
            <div className="row p-0 m-0">
              <div className="col-6 p-0 m-0 no-gutters">
                <div className="hero__caption inaipi-welcome">
                  <div className="logo">
                    <img src={inaipi} alt="logo" />
                  </div>
                  <div className="content-div" style={{ marginTop: "10px" }}>
                    <div className="welcome">
                      <p style={{ fontSize: 17, fontWeight: 600 }}>


                        <span
                          style={{
                            fontSize: 17,
                            fontWeight: 600,
                            display: "block",
                          }}
                        >
                          {" "}
                          INAIPI UCX LOGIN
                        </span>
                      </p>
                    </div>
                    <div className="form">
                      {/* <form> */}
                      <div className="form-group">
                        <input
                          type="text"
                          className="form-control"
                          name="username"
                          id="exampleFormControlInput1"
                          onChange={(e) => setEmail(e.target.value)}
                          // value={this.state.username}
                          placeholder="Enter your username"
                          value={email}
                          onKeyPress={handleKeyPress}

                        />

                        {emailval ? (
                          <span className="text-danger mx-3">
                            Username is Required *
                          </span>
                        ) : (
                          <span></span>
                        )}
                      </div>
                      <div
                        className="form-group input-pass"
                        style={{ marginTop: "20px" }}
                      >
                        <input
                          type={isShown ? "text" : "password"}
                          className="form-control"
                          name="password"
                          id="exampleFormControlInput1"
                          onChange={(e) => setPass(e.target.value)}
                          placeholder="Enter your password"
                          value={pass}
                          onKeyPress={handleKeyPress}

                        />
                        {browserName == "Chrome" ? (
                          <span className="eye_icon">
                            {eye ? (
                              <AiFillEye
                              style={{cursor:'pointer'}}
                                size={17}
                                onClick={() => {
                                  togglePassword();
                                  setEye(false);
                                }}
                              />
                            ) : (
                              <AiFillEyeInvisible
                                size={17}
                                onClick={() => togglePassword()}
                              />
                            )}
                          </span>
                        ) : (
                          ""
                        )}
                        {passval ? (
                          <span className="text-danger mx-3">
                            Password is Required *
                          </span>
                        ) : (
                          <span></span>
                        )}
                      </div>
                      <div className="d-flex align-items-center">
                        <div className="mx-1">
                          <input
                            type="checkbox"
                            onChange={handleChange}
                            ref={ref}
                            defaultChecked={cookies.get("boolean")}
                          //  checked={cookies.get('boolean')}
                          />
                        </div>
                        <div>Remember Me</div>
                      </div>
                      <div
                        className="form-group input-pass"
                        style={{ marginTop: "40px" }}
                      >
                        <button
                          variant="success"
                          id="connectSocket"
                          onClick={(e) => {
                            if (email == "") {
                              setEmailval(true);
                              setPassval(false);
                            } else if (pass == "") {
                              setEmailval(false);
                              setPassval(true);
                            } else {
                              login();
                            }
                          }}
                          className="btn form-control"
                          style={{ backgroundColor: "#00498f", color: "white" }}
                        >
                          {spinner ? (
                            <div className="spinner-border" role="status">
                              <span className="sr-only">Loading...</span>
                            </div>
                          ) : (
                            <div>Login</div>
                          )}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="img-banner-main col-6 p-0 m-0 no-gutters position-relative">
                <ul className="ul-list list-unstyled d-flex">
                  <li>Connect</li>
                  <li>Converse</li>
                  <li>Colloborate</li>
                </ul>

                <div className="banner-sub"></div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <Modal
        show={showTransferMod}
        className="transferCall-modal1"
        onHide={handleTransferClose}

      >
        <Modal.Header


          style={{
            padding: "6px 12px",
            margin: 0,
            fontSize: "12px",
            height: "45px",
            backgroundColor: '#294e9f',
            color: 'white'
          }}
        >
          <div className="d-flex justify-content-between align-items-center w-100">
            <div>
              <Modal.Title
                style={{
                  fontSize: 15,
                  margin: "6px 0 0 0",
                  textTransform: "capitalize",
                }}
              >
                Confirmation
              </Modal.Title>

            </div>
            <div>
              <AiOutlineCloseCircle onClick={handleTransferClose} />

            </div>

          </div>
        </Modal.Header>
        <Modal.Body>
          {"User is already logged in are you sure want to logout "}

        </Modal.Body>

        <div className="transferOkDiv" style={{ justifyContent: "flex-start" }}>

          <Button
            variant="primary"
            className="transferOkBtn"
            onClick={() => logout()}
          >
            Logout
          </Button>
        </div>
      </Modal>

    </>
  );
};

export default connect(mapStateToProps, {
  setselectedusername,
  setselectedemail,
  setselectedmobile,
  setrefreshtogglechat,
  setselecteduserimage,
  setchatdata,
  setchatid,
  setchatduration,
  setchattype,
  setcontactlist,
  setcontactlist2,
  setonloadContactRefresh,
  setconfchatrefresh,
  setinternalexternal,
  settogglechat,
  setAgentList,
  setinternalchatrefresh,
  setinternalchatnotify,
})(Login);
