import React, { useEffect, useState, useRef } from "react";
import { BiMessage, BiSend } from "react-icons/bi";
import { RiAttachment2 } from "react-icons/ri";
import { MdKeyboardVoice } from "react-icons/md";
import { BsMicMuteFill } from "react-icons/bs"
import {  AiFillDelete, AiOutlineCloseCircle, AiOutlinePlus } from 'react-icons/ai'
import { setchatdata, settogglechat } from "../redux/actions/spaceActions";
import { connect } from "react-redux";
import InputEmoji from "react-input-emoji";
import Button from "react-bootstrap/Button";
import './chatStyle.css'
import Modal from "react-bootstrap/Modal";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Card, Form } from "react-bootstrap";
import { ImCross } from "react-icons/im";
import { BaseUrl,TenantID, errorApi } from "../containers/Page/Constants/BaseUrl";
import listenForOutsideClick from "./Foroutsideclick";
import Foroutsideclick from "./Foroutsideclick";
import {  RecordRTCPromisesHandler } from "recordrtc";
import giff from "./../assets/img/giphy.gif"
// import clamScan from 'clamscan'

const mapStateToProps = (state) => {
  const { data } = state;
  return {
    contactList: data.contactList,
    chat: data.chat,
    togglechat: data.togglechat,
  };
};
function ChatInput(props) {
  const tenantId = localStorage.getItem("TenantId");

  const [msg, setMsg] = useState("");
  const [imagePreviewUrl, setImagePreviewUrl] = useState("");
  const [file, setFile] = useState("");
  const [show, setShow] = useState(false);
  const [fileTypeStore, setFileTypeStore] = useState("");
  const [fileUploadName, setfileUploadName] = useState("");
  const [fileSendName, setfileSendName] = useState("");
  const [loading, setLoading] = useState(false)
  const [cannedmessagediv, setCannedmessagediv] = useState(false)
  const [showcannedmodal, setShowcannedmodal] = useState(false)
  const [showMessage, setShowMessage] = useState([]);
  const [createMsg, setCreateMsg] = useState("");
  const [voiceRecord, setVoiceRecord] = useState(true)
  const [recordnum, setRecordnum] = useState('1')
  const [recorddata, setRecorddata] = useState('')
  const [preview, setPreview] = useState(true)
  const [src, setSrc] = useState("")
  const [cannemessagestore, setCannemessagestore] = useState('')
  const handleClosecannedmessage = () => setShowcannedmodal(false);
  const handleShowcannedmessage = () => setShowcannedmodal(true);
  const [recorder, setRecorder] = useState(null);
  const menuRef = useRef();




  useEffect(() => {
    retrieveScannedMsg();
  }, [props.disconnect])

  const showCannedmessage = () => {
    setCannedmessagediv(true);
    console.log('click')

  };
  const closeuserNotification = () => {
    setCannedmessagediv(false);
  };

  const errorHandel = async (error,endpoint) => {
    const tenantID = localStorage.getItem("TenantId");
    try {
      const payload = {
        servicename: 'DCCCHAT',
        logs:error,
        description:endpoint

      };

      const header = {
        headers: {
          // Authorization: "Bearer " + token,
          // "Content-Type": "application/json",
          tenantid: tenantID,
        },
      };
      const { data } = await axios.post(
        errorApi + "/UIlogs/createLogs",
        payload,
        header
      );
      if (data.status) {
        console.log("from error api", data);
      }
    } catch (error) {
      console.log('error',error)
   
    }
  };

  const handleClose = () => {
    setShow(false);
    setFileTypeStore("");
  };
  const handleShow = () => setShow(true);
  const sendChat = () => {
 if(props.data.chat.is_customer_disconnected){
  toast.error("Customer is disconnected you can't send message", toastOptions);
  // alert("Customer is disconnected");
 }else{
    if (!preview) {
      setVoiceRecord(true)
      setPreview(true)
      const fileUploaded = recorddata;
      let formData = new FormData();
      let fileName = `audio.wav`;
      let file = new File([fileUploaded], fileName);
      formData.append('file', file, fileName);

      const url = "https://qacc.inaipi.ae/v1/fileServer/uploadMedia";

      formData.append(
        "userID",
        props.data.chat.unique_id
          ? props.data.chat.unique_id.id
          : props.data.chat.senderDetails[0]._id
      );
      formData.append("clientApp", "InapiWebchat");
      formData.append("channel", "webchat");
      formData.append("sessionId", props.data.chat.chat_session_id);
      formData.append("sentBy", "Agent");
      const config = {
        headers: {
          "content-type": "multipart/form-data",
           tenantId: "123456",

        },
      };

      axios
        .post(url, formData, config)
        // setLoading(true)
        .then((response) => {
          if (response.data.status) {
            setLoading(false)
            let pic_url = response.data.data.signedUrl;
            let mediaType = response.data.data.mediaType.toUpperCase();
            props.handleSendMsg(pic_url, "AUDIO", "");
            setFile("");
            setFileTypeStore("");
          }
        })
        .catch((error) => {
          errorHandel(error,'/fileServer/uploadMedia')
          toast.error("Sorry,the file you are trying to upload is too big(maximum size is 3072KB)", toastOptions);
        });
    }
    if (msg.length > 0 && msg.length < 1000) {
      props.handleSendMsg(msg, "TEXT");
      // props.handleSendMsg(selectedcannedmsg ,"TEXT");
      // props.handleSendMsg(message, "TEXT");
      setMsg("");
    } else if (msg.length > 1000) {
      // props.handleSendMsg(message, "TEXT");
     // alert("You'll need to shorten your message to send it");
      toast.error("You'll need to shorten your message to send it", toastOptions);
    }
  }
  };

  const hiddenFileInput = React.useRef(null);

  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };
  const handleChange = (event) => {
    let reader = new FileReader();
    const fileUploaded = event.target.files[0];
    console.log('fileUploaded<<<<<<<<<<<>>>>>>>>',fileUploaded)
    const fileType = fileUploaded.type;
    console.log('fileUploaded', fileType)
    const allowedFormats = ['image/jpeg', 'image/png', 'video/mp4','video/webmp','audio/mp3','audio/.wav','application/pdf','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/.docx'];
    if (fileUploaded.size > 3000000) {

      toast.warn('Choose File Below 3mb', {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });

      // toast.warn('Choose File Below 5mb')
    } else if(allowedFormats.includes(fileType)){
      reader.onloadend = () => {
        setImagePreviewUrl(reader.result);
        setFile(fileUploaded);

      };


      reader.readAsDataURL(fileUploaded);
      let split_fileType = event.target.files[0].type.split("/");
      let fileTypeToShow;

      setFileTypeStore(split_fileType[0]);

      setfileUploadName(fileUploaded);
      setfileSendName(event.target.files[0].name);
      handleShow();
    }else{
      toast.warn('Invalid File Format')
    }

  };


  const retrieveScannedMsg = () => {
    let data = {
      offset: "0",
      limit: "10",
    };
    const access_token = localStorage.getItem("access_token");
    axios
      .post(BaseUrl + "/scannedMessage/listMessage", data, {
        headers: {
          'Authorization': 'Bearer ' + access_token,
          'Content-Type': 'application/json',
          tenantId: tenantId,

        }

      })
      .then((res) => {
        if (res.status) {
          if (res.data.data) {
            let resp = res.data.data;
            console.log(resp);
            setShowMessage(resp);
          }

        }
      })
      .catch((error) => {
        console.log("try after sometimes");
        errorHandel(error,'/scannedMessage/listMessage')
      });
  };


  const create_contact = () => {
    const access_token = localStorage.getItem("access_token");
    let data = {
      message: createMsg,
    };
    axios
      .post(BaseUrl + "/scannedMessage/createMessage", data, {
        headers: {
          'Authorization': 'Bearer ' + access_token,
          'Content-Type': 'application/json',
          tenantId: tenantId,

        }

      })
      .then((res) => {
        if (res.status) {
          toast.success('The message is successfully canned')

          setTimeout(() => {
            retrieveScannedMsg();
            setShowcannedmodal(false);

          }, 1000);
        }
      })
      .catch((error) => {
        console.log("try after sometimes");
        errorHandel(error,'/scannedMessage/createMessage')

      });
  };

  const sendFile = () => {
    let send_file;
    if (fileUploadName.name) {
      send_file = fileUploadName;
      setLoading(true)
    } else {
      send_file = file;
    }
    const url = "https://qacc.inaipi.ae/v1/fileServer/uploadMedia";
    const formData = new FormData();
    formData.append("file", send_file);
    formData.append(
      "userID",
      props.data.chat.unique_id
        ? props.data.chat.unique_id.id
        : props.data.chat.senderDetails[0]._id
    );
    formData.append("clientApp", "InapiWebchat");
    formData.append("channel", "webchat");
    formData.append("sessionId", props.data.chat.chat_session_id);
    formData.append("sentBy", "Agent");
    const config = {
      headers: {
        "content-type": "multipart/form-data",
         tenantId: "123456",

      },
    };

    axios
      .post(url, formData, config)
      // setLoading(true)
      .then((response) => {
        if (response.data.status) {
          setLoading(false)
          let pic_url = response.data.data.signedUrl;
          let mediaType = response.data.data.mediaType.toUpperCase();
          props.handleSendMsg(pic_url, mediaType, fileSendName);
          setFile("");
          setFileTypeStore("");
        }
      })
      .catch((error) => {
        errorHandel(error,'/fileServer/uploadMedia')
        toast.error("Sorry,the file you are trying to upload is too big(maximum size is 3072KB)", toastOptions);
      });

    handleClose();
  };

  const toastOptions = {
    position: "top-right",
    autoClose: 5000,
    pauseOnHover: true,
    draggable: true,
    theme: "light",
  };


  const getId = (item) => {
    console.log('selectedMessage', item.message);
    // setSelectedcannedmsg(item.message)
    // sendChat(setMsg(item.message))
    setMsg(item.message)
    setCannemessagestore(item.message)




  }




  const startRecording = async () => {
    let stream = await (navigator).mediaDevices.getUserMedia({

      audio: true,
    });
    let recorder1 = new RecordRTCPromisesHandler(stream, {
      type: "audio",
    });
    setRecorder(recorder1);
    setVoiceRecord(false)
    setRecordnum('2')
    recorder1.startRecording();
  };
  const stopRecording = async () => {
    // setVoiceRecord(true)
    setRecordnum('3')
    setPreview(false)
    console.log("stopped")
    await recorder.stopRecording();
    let blob = await recorder.getBlob();
    setRecorddata(blob);
    const url = URL.createObjectURL(blob);
    setSrc(url)
    console.log(url)
  };
  const deletebutton = () => {
    setRecorddata([]);
    setSrc("");
    setVoiceRecord(true);
    setPreview(true);
  }







  return (
    // <Container>
    <>
      <div></div>
      {props.togglechat ?

        <div className="d-flex chat_fix" style={{ backgroundColor: 'white', width: '69%', pointerEvents: props.data.chat.is_customer_disconnected ? 'none' : 'all' }} >
          
          <ToastContainer />
          <form className="chat-input-area col-10 m-0 p-0" id="quillForm" >
          {voiceRecord?
            <form className="chat-input-area col-10 m-0 p-0" id="quillForm">
              <div className="mb-3 pr-2 pl-2 " style={{ width: '65%',height:'', }}>
                <div className="newclass">
                <InputEmoji
                  value={msg}
                  cleanOnEnter
                  onChange={setMsg}
                  theme="light"
                  onEnter={sendChat}
                  onKeyDown={props.handleTyping}
                  className="newclass"
                  id="exampleFormControlInput1"
                  placeholder="Enter your message"
                 
                  
                
                />

                </div>
              </div>
            </form>
          :preview?<>
          <div  style={{position:'relative',top:'12px',left:'8rem'}}>
              <AiFillDelete size={16}  onClick={deletebutton} color="red"style={{
                      color: "red",
                     // background: "rgb(0, 73, 143)",
                       width: "32px",
                       padding:"3px",
                      height: "25px",
                      // borderRadius: "5px",
                      cursor: "pointer",
                      position:'relative',
                      bottom:'-7px'
                    }}/>
              

          </div>
              <img src={giff} style={{ position:'relative',width: '54%',height: '13%',paddingLeft:'10rem',paddingBottom:'38px' }} />
             
          </>
            :<> 
            
            <AiFillDelete size={26} onClick={deletebutton} color="red"style={{
                      color: "red",                   
                      padding: " 3px",
                      cursor: "pointer",
                      position:'relative',
                      bottom:'0',
                      left:"85px"

                    }}/>
            <audio src={src} style={{ width: '32%', height: '13%',paddingLeft:'',paddingRight:'',paddingBottom:'25px',position:'relative',bottom:'-14px ',left:'90px' }} controls/></>
            
          }

          </form>



          <div className="attachment_icon" style={{ marginLeft: '-12rem' }}>
            <div className="mx-1" style={{ position: "relative", left: '-69px', top: '10px' }}>
              <button
                type="button"
                onClick={handleClick}
                id="attach_files"
                style={{
                  padding: 0,
                  margin: 0,
                  border: 0,
                  height: 0,
                  background: "transparent",
                }}
              >
                <RiAttachment2
                  color="white"
                  style={{
                    color: "white",
                    background: "rgb(0, 73, 143)",
                    width: "29px",
                    padding: " 3px",
                    height: "29px",
                    borderRadius: "5px",
                  }}
                />
              </button>
              <input
                type="file"
                ref={hiddenFileInput}
                onChange={handleChange}
                style={{ display: "none" }}
                onClick={(event) => {
                  event.target.value = null;
                }}
              />
            </div>




            <div style={{ position: 'relative', left: '-30px', top: '-7px' }} className='mx-1'>





              {
                voiceRecord ? <button onClick={startRecording}> <MdKeyboardVoice color="white"
                  style={{
                    color: "white",
                    background: "rgb(0, 73, 143)",
                    width: "29px",
                    padding: " 3px",
                    height: "29px",
                    borderRadius: "5px",

                  }} /></button> : <button onClick={stopRecording}> <BsMicMuteFill MdKeyboardVoice color="white"
                    style={{
                      color: "white",
                      background: "rgb(0, 73, 143)",
                      width: "29px",
                      padding: " 3px",
                      height: "29px",
                      borderRadius: "5px",
                    }} /></button>
              }
            </div>
            <div className="mx-1"
              style={{
                position: 'relative',
                bottom: '50px',
                left: '53px'
              }}
            >
              <button >
                <BiMessage
                  size={24}
                  color='#294e9f'
                  onClick={showCannedmessage}
                />
              </button>

              
            </div>

            {/* {cannedmessagediv && (
              <div className="Menu_NOtification_Wrap bg-light" style={{ top: '8.6rem', right: '33rem',zIndex:0 }}>

                <div className="Notification_body">
                  <Foroutsideclick onClickOutside={closeuserNotification}>
                    <Card
                      style={{
                        height: "15rem",
                        zIndex: 0,
                        overflowY: "scroll",
                        paddingBottom: 35,
                      }}
                      className="mt-2"
                    >
                      <div className="d-flex justify-content-between mt-1 ml-2">
                        <h5 className="mt-2 text-center" style={{ fontSize: "1rem" }}>
                          Canned Message
                        </h5>
                        <Button
                          className="mr-2"
                          style={{ fontSize: "10px", backgroundColor: "#00498f" }}
                          onClick={handleShowcannedmessage}
                        >
                          <AiOutlinePlus />
                        </Button>
                      </div>


                      {showMessage.map((item) => (

                        <Card
                          onClick={() => getId(item)}
                          style={{
                            width: "95%",
                            height: "3rem",
                            marginLeft: "10px",
                            cursor: "pointer",

                            // marginBottom:'10rem'
                          }}
                          className="mt-2"

                        >

                          <div className="text-center d-flex justify-content-around my-auto"


                          >
                            <p style={{ fontWeight: "bold", padding: "10px 0px" }} >
                              {item.message}
                            </p>
                          </div>
                        </Card>
                      ))}



                    </Card>
                  </Foroutsideclick>






                </div>

              </div>
            )} */}

             <div className="mx-1"
              style={{
                // display: "inline-block",
                position: "relative",
                top: "-6.2rem",
                left: "9px",
                clear: "both",
              }}
            >
              <button
                type="button"
                onClick={sendChat}
                id="send_msg_to_client"
                style={{
                  padding: 0,
                  margin: 0,
                  border: 0,
                  height: 0,
                  background: "transparent",
                }}
              >
                <BiSend
                  color="#00498f"
                  className="btn btn-block btn-primary sendnew"
                  style={{ backgroundColor: "blue" }}
                />

              </button>

            </div>

            {cannedmessagediv && (
              <div className="Menu_NOtification_Wrap bg-light" style={{ top: '8.6rem', right:'32rem',zIndex:0 }}>

                <div className="Notification_body">
                  <Foroutsideclick onClickOutside={closeuserNotification}>
                    <Card
                      style={{
                        height: "15rem",
                        zIndex: 0,
                        overflowY: "scroll",
                        paddingBottom: 35,
                      }}
                      className="mt-2"
                    >
                      <div className="d-flex justify-content-between mt-1 ml-2">
                        <h5 className="mt-2 text-center" style={{ fontSize: "1rem" }}>
                          Canned Message
                        </h5>
                        <Button
                          className="mr-2"
                          style={{ fontSize: "10px", backgroundColor: "#00498f" }}
                          onClick={handleShowcannedmessage}
                        >
                          <AiOutlinePlus />
                        </Button>
                      </div>


                      {showMessage.map((item) => (

                        <Card
                          onClick={() => {getId(item);closeuserNotification()}}
                          style={{
                            width: "95%",
                            height: "3rem",
                            marginLeft: "10px",
                            cursor: "pointer",

                            // marginBottom:'10rem'
                          }}
                          className="mt-2"

                        >

                          <div className="text-center d-flex justify-content-around my-auto"


                          >
                            <p style={{ fontWeight: "bold", padding: "10px 0px" }} >
                              {item.message}
                            </p>
                          </div>
                        </Card>
                      ))}



                    </Card>
                  </Foroutsideclick>






                </div>

              </div>
            )}

          </div>

        </div> :
        <div className="d-flex chat_fix" style={{ backgroundColor: 'white', width: '100%', pointerEvents: props.data.chat.is_customer_disconnected ? 'none' : 'all' }} >
          <ToastContainer />
          {voiceRecord ?
            <form className="chat-input-area col-10 m-0 p-0" id="quillForm">
              <div className="mb-3 pr-2 pl-2 " style={{ width: '95%', height: '', }}>
                <div className="newclass">
                  <InputEmoji
                    value={msg}
                    cleanOnEnter
                    onChange={setMsg}
                    theme="light"
                    onEnter={sendChat}
                    onKeyDown={props.handleTyping}
                    className="newclass"
                    id="exampleFormControlInput1"
                    placeholder="Enter your message"
                    disabled ={props.data.chat.is_customer_disconnected?true:false}


                  />

                </div>
              </div>
            </form>
            : preview ? <>
              <div className="my-3" style={{ position: 'relative', top: '0', left: '32rem' }}>
                <AiFillDelete size={20} onClick={()=>{deletebutton()}} color="red" style={{
                  color: "red",
                  // background: "rgb(0, 73, 143)",
                  width: "32px",
                  padding: " 3px",
                  height: "35px",
                  borderRadius: "5px",
                  cursor: "pointer",
                 
                }} />


              </div>
              
              <img src={giff} style={{ width: '80%', height: '15%', paddingLeft: '33rem' }} />

            </>
              : <> 
              
              
              <div style={{ position: 'relative', top: '25px', left: '28rem' }}>
                <AiFillDelete size={20} onClick={()=>{deletebutton()}} color="red" style={{
                  color: "red",
                  width: "32px",
                  padding: "3px",
                  height: "35px",
                  borderRadius: "5px",
                  cursor: "pointer",
                  position:'relative',
                  bottom:'10px'
                 
                 
                  
                }} /></div>
                <audio src={src} style={{ width: '80%', height: '10%', paddingLeft: '30rem', paddingRight: '50px',marginTop:'6px' }} controls /></>

          }

          <div className="mx-1" style={{ width: '5%' }}>

            <div className="attachment_icon d-flex">
              <div style={{ display: "inline-block", position: "relative", left: '-40px', top: '7px' }}>
                <button
                  type="button"
                  onClick={handleClick}
                  id="attach_files"
                  style={{
                    padding: 0,
                    margin: 0,
                    border: 0,
                    height: 0,
                    background: "transparent",
                  }}
                >
                  <RiAttachment2
                    color="white"
                    style={{
                      color: "white",
                      background: "rgb(0, 73, 143)",
                      width: "29px",
                      padding: " 3px",
                      height: "29px",
                      borderRadius: "5px",
                    }}
                  />
                </button>
                <input
                  type="file"
                  ref={hiddenFileInput}
                  onChange={handleChange}
                  style={{ display: "none" }}
                  onClick={(event) => {
                    event.target.value = null;
                  }}
                />

              </div>
              <div style={{ position: 'relative', left: '-40px', top: '6px' }}>

                {
                  voiceRecord ? <button onClick={startRecording}> <MdKeyboardVoice color="white"
                    style={{
                      color: "white",
                      background: "rgb(0, 73, 143)",
                      width: "29px",
                      padding: " 3px",
                      height: "29px",
                      borderRadius: "5px",
                    }} /></button> : <button onClick={stopRecording}> <BsMicMuteFill MdKeyboardVoice color="white"
                      style={{
                        color: "white",
                        background: "rgb(0, 73, 143)",
                        width: "29px",
                        padding: " 3px",
                        height: "29px",
                        borderRadius: "5px",
                      }} /></button>
                }
              </div>


              <div
                style={{
                  position: "relative",
                  left: "-65px",
                  clear: "both",
                }}
              >
                <button
                  type="button"
                  onClick={sendChat}
                  id="send_msg_to_client"
                  style={{
                    padding: 0,
                    margin: 0,
                    border: 0,
                    height: 0,
                    background: "transparent",
                  }}
                >
                  <BiSend
                    color="#00498f"
                    className="btn btn-primary sendnew"
                    style={{ backgroundColor: "blue" }}
                  />
                </button>
              </div>
              <div>

              
                <div style={{ position: 'relative', left: '-6rem', top: '7px', zIndex: '111' }}>
                  <button
                    onClick={showCannedmessage}
                  >

                    <BiMessage
                      size={24}
                      color='#294e9f'
                    //showCannedmessage
                    />
                  </button>


                

                </div>
                {/* <button
                  onClick={showCannedmessage}
                >

                  <BiMessage
                    size={24}
                    color='#294e9f'
                
                  />
                </button> */}

                {cannedmessagediv && (
                  <div className="Menu_NOtification_Wrap bg-light" ref={menuRef} style={{ top: '8.7rem', right: '5rem',zIndex:9 }}>
                    <div className="Notification_body" style={{ width: '' }}>
                      <Foroutsideclick onClickOutside={closeuserNotification}>
                        <Card
                          style={{
                            height: "15rem",
                            // zIndex: 99999,
                            overflowY: "scroll",
                            paddingBottom: 35,
                          }}
                          className="mt-2"
                        >
                          <div className="d-flex justify-content-between mt-1 ml-2">
                            <h5 className="mt-2 text-center" style={{ fontSize: "1rem" }}>
                              Canned Message
                            </h5>
                            <Button
                              className="mr-2"
                              style={{ fontSize: "10px", backgroundColor: "#00498f" }}
                              onClick={handleShowcannedmessage}
                            >
                              <AiOutlinePlus />
                            </Button>
                          </div>


                          {showMessage.map((item) => (

                            <Card
                              onClick={() => {getId(item);closeuserNotification()}}

                              style={{
                                width: "95%",
                                height: "3rem",
                                marginLeft: "10px",
                                cursor: "pointer",
                                // marginBottom:'10rem'
                              }}
                              className="mt-2"

                            >

                              <div className="text-center d-flex justify-content-around my-auto"


                              >
                                <p style={{ fontWeight: "bold", padding: "10px 0px" }}>
                                  {item.message}
                                </p>
                              </div>






                            </Card>
                          ))}



                        </Card>
                      </Foroutsideclick>






                    </div>
                  </div>
                )}
              
              </div>




            </div>
          </div>
        </div>
      }


      {/* canned message modal  */}


      <Modal show={showcannedmodal} onHide={handleClosecannedmessage} className="cannded_msg">
        <Modal.Header
          style={{
            backgroundColor: "#294e9f",
            color: "white",
            marginTop: "-1px",
           
          }}
        >
          <Modal.Title>Canned Message</Modal.Title>
          <span className="mt-2">
            <ImCross onClick={handleClosecannedmessage} />
          </span>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Canned Message</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Your Message"
                onChange={(e) => setCreateMsg(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <div className="d-flex  justify-content-end">
          <Button variant="primary" onClick={create_contact}>
            Submit
          </Button>
        </div>
      </Modal>
      {/* canned message modal */}





      {/* </Container> */}
      <div>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header
            style={{ padding: "10px 0 0 10px", margin: 0, height: "42px" }}
          >
            <Modal.Title className="w-100">
              <div className="d-flex justify-content-between align-items-center text-dark">
                Preview
                <AiOutlineCloseCircle size={15} className="mr-3" style={{ cursor: 'pointer' }} onClick={handleClose} />
              </div>
            </Modal.Title>
          </Modal.Header>
          {loading ?
            <div className="spinner-border  d-flex justify-content-center align-items-center mx-auto" role="status">

              <span className="sr-only" >Loading...</span>


            </div> :
            <Modal.Body style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '14rem' }}>
              {" "}




              {fileTypeStore == "image" && <img style={{ width: '100%', height: '14rem' }} src={imagePreviewUrl} />}
              {fileTypeStore == "audio" && (
                <audio controls src={imagePreviewUrl}></audio>
              )}
              {fileTypeStore == "video" && (
                <video width="320" height='180' controls muted onloadstart={true}>
                  <source src={imagePreviewUrl} type="video/mp4"></source>
                </video>

              )}

              {fileTypeStore == "application" && (
                <div
                  style={{
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                    overflow: "hidden",
                    position: 'relative',
                  }}
                >
                  <a href={imagePreviewUrl} target="_blank">
                    {fileSendName}
                  </a>
                </div>
              )}
            </Modal.Body>}
          <Modal.Footer className="imgPreview" style={{ padding: 0 }}>
            <div>
              {" "}
              <button onClick={handleClose} className="closeBtnImg">
                Close
              </button>
              <button onClick={sendFile} className="saveButtonImg">
                Send
              </button>
            </div>
          </Modal.Footer>
        </Modal>
      </div>


      {/* Audio Modal */}


    </>
  );
}

export default connect(mapStateToProps, {
  setchatdata,
  settogglechat

})(ChatInput);
