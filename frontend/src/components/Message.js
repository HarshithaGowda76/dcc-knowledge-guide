import { connect } from "react-redux";
import React, { useState } from "react";
import './DirectMessage.scss'
import {
  sendMessage,
  setchatdata,
  settogglechat,
  setrefreshtogglechat,

} from "../redux/actions/spaceActions";
const mapStateToProps = (state) => {
  return {
    me: state.data.me,
    chat: state.data.chat,
    chatType: state.data.chatType,
    contactList: state.data.contactList,
    togglechat: state.data.togglechat,

  };
};

function Message(props) {
  const {
    sender,
    avatarURL,
    messageTxt,
    me,
    data,
    message,
    displayname,
    created,
    chatType,
    isSelf,
    message_sent_type,
    captions
  } = props;




  const createMarkup = (isSelf) => {
    let new_msg;
    // console.log("msg test", messageTxt);
    if (message_sent_type == "TEXT") {
      if (
        messageTxt.includes("http://")||
        messageTxt.includes("https://") ||
        messageTxt.includes("www.")


      ) {
        const splittedData = messageTxt.split(' ');
        for(let i=0; i< splittedData.length; i++){
          if(splittedData[i].includes('http://') || splittedData[i].includes('https://') || splittedData[i].includes('www.')){
            if(isSelf){
              splittedData[i] = "<a  href="+splittedData[i]+" style='color:white' target='_blank'>"+splittedData[i]+"</a>";
            }else{
              splittedData[i] = "<a  href="+splittedData[i]+" style='color:blue' target='_blank'>"+splittedData[i]+"</a>";
            }
         
          }
        } 
        new_msg = splittedData.join(' ');
      } else {
        new_msg = messageTxt;
      }
    } else if (message.msg_sent_type == "APPLICATION") {
      if(captions){
      if(isSelf){
      new_msg = `<a href='${messageTxt}' style='color:white' target='_blank'>${message.file_name!=undefined?message.file_name:messageTxt}</a>    <p>${captions}</p>`;
      }else{
        new_msg = `<a href='${messageTxt}' style='color:blue' target='_blank'>${message.file_name!=undefined?message.file_name:messageTxt}</a>    <p>${captions}</p>`;
      }
    }else{
      if(isSelf){
        new_msg = `<a href='${messageTxt}' style='color:white' target='_blank'>${message.file_name!=undefined?message.file_name:messageTxt}</a>`;
        }else{
          new_msg = `<a href='${messageTxt}' style='color:blue' target='_blank'>${message.file_name!=undefined?message.file_name:messageTxt}</a>`;
        }
    }
      // new_msg=`<div style="height:45;width:20rem;background-color:#9fa3d6;border-radius:10;border-radius:10px;overflow:hidden"><div style='display:flex;justify-content:center;align-items:center;padding-top:auto;height:100%;overflow:hidden'><a href='${messageTxt}' style='color:white;overflow:hidden' target='_blank'>${message.file_name}</a></div></div>`
    } 
    else if (message_sent_type == "AUDIO") {
      if(captions){
      new_msg = `<audio controls src="${messageTxt}"></audio>    <p>${captions}</p>`;
    }else{
      new_msg = `<audio controls src="${messageTxt}"></audio>`;
    }
    } else if (message_sent_type == "VIDEO") {
      if(captions){
      new_msg = `<video width="180" height="150" controls><source src="${messageTxt}" type="video/mp4"></video>    <p>${captions}</p>`;
      }else{
        new_msg = `<video width="180" height="150" controls><source src="${messageTxt}" type="video/mp4"></video>`;
      }
    }else if(message_sent_type == "IMAGE") {
   if(captions){
      new_msg =

        `<a href='${messageTxt}' target='_blank'><img style=' min-width: 180px;height: 120px;' alt='image expired' src='${messageTxt}' ></img></a>

        <p>${captions}</p>

        `      

    }else{

      new_msg =

      `<a href='${messageTxt}' target='_blank'><img style=' min-width: 180px;height: 120px;' alt='image expired' src='${messageTxt}' ></img></a>`

    }
  }
    return { __html: new_msg };
  };




  return (
    <div>

      {chatType == "WEBCHAT" ? (
        <div >
          {message_sent_type == "NOTIFICATIONS" && (
            <p className="msgNotify"><span style={{position:'relative',bottom:'8px',fontSize:'10px'}}>{messageTxt}</span></p>
          
          )}
          
          {/* width: props.togglechat ? "69%" : "100%", */}



          {message_sent_type == "whatsappImage" && (
            <div className={`chat-message ${isSelf && "chat-message--right"} `} >
              <span className="chat-message__avatar-frame">
                <img
                  src={"https://accounts.zang.io/norevimages/noimage.jpg"}
                  alt="avatar"
                  className="chat-message__avatar"
                />
              </span>

              <div>
                <div>
                  <span
                    className=""
                    style={{
                      fontSize: "8px",
                      color: "black",
                      position: "relative",
                      top: "20px",
                      left: "15px",
                      color: "rgb(197, 194, 194)",
                      fontWeight: "bold",
                      textTransform: "capitalize",
                    }}
                  >
                    {displayname}
                  </span>
                 
                  <p
                    className="chat-message__text "
                    style={{ padding: "22px 0 8px 10px" }}
                  >
                    <img
                      style={{
                        minWidth: " 180px",
                        height: "120px",
                        objectFit: "cover",
                      }}
                      alt="image expired"
                      src={require(`../whatsapp-media/${messageTxt}`)}
                    />
                  </p>
                  <span
                    style={{ fontSize: "10px", color: "black" }}
                    className="mx-3"
                  >
                    {created}
                  </span>
                </div>
              </div>
            </div>
          )}
          {message_sent_type == "whatsappVideo" && (
            <div className={`chat-message ${isSelf && "chat-message--right"} `}>
              <span className="chat-message__avatar-frame">
                <img
                  src={"https://accounts.zang.io/norevimages/noimage.jpg"}
                  alt="avatar"
                  className="chat-message__avatar"
                />
              </span>

              <div>
                <div>
                  <span
                    className=""
                    style={{
                      fontSize: "8px",
                      color: "black",
                      position: "relative",
                      top: "20px",
                      left: "15px",
                      color: "rgb(197, 194, 194)",
                      fontWeight: "bold",
                      textTransform: "capitalize",
                      maxWidth:'20px'
                    }}
                  >
                    {displayname}
                  </span>
                  <p
                    className="chat-message__text "
                    // style={{ padding: "22px 0 8px 10px" }}
                    // style={{marginTop: props.togglechat ? '20px' : "", }}
                  >
                    <video width="180" height="150" controls><source src={require(`../whatsapp-media/${messageTxt}`)} type="video/mp4"></source></video>
                  </p>
                  <span
                    style={{ fontSize: "10px", color: "black" }}
                    className="mx-3"
                  >
                    {created}
                  </span>
                </div>
              </div>
            </div>
          )}
          {message_sent_type == "whatsappAudio" && (
            <div className={`chat-message ${isSelf && "chat-message--right"} `}>
              <span className="chat-message__avatar-frame">
                <img
                  src={"https://accounts.zang.io/norevimages/noimage.jpg"}
                  alt="avatar"
                  className="chat-message__avatar"
                />
              </span>

              <div>
                <div>
                  <span
                    className=""
                    style={{
                      fontSize: "8px",
                      color: "black",
                      position: "relative",
                      top: "20px",
                      left: "15px",
                      color: "rgb(197, 194, 194)",
                      fontWeight: "bold",
                      textTransform: "capitalize",
                    }}
                  >
                    {displayname}
                  </span>
                  <p
                    className="chat-message__text "
                    style={{ padding: "22px 0 8px 10px" }}
                  >
                    <audio controls src={require(`../whatsapp-media/${messageTxt}`)} ></audio>
                  </p>
                  <span
                    style={{ fontSize: "10px", color: "black" }}
                    className="mx-3"
                  >
                    {created}
                  </span>
                </div>
              </div>
            </div>
          )}
          {message_sent_type == "whatsappDocument" && (
            <div className={`chat-message ${isSelf && "chat-message--right"} `}>
              <span className="chat-message__avatar-frame">
                <img
                  src={"https://accounts.zang.io/norevimages/noimage.jpg"}
                  alt="avatar"
                  className="chat-message__avatar"
                />
              </span>

              <div>
                <div>
                  <span
                    className=""
                    style={{
                      fontSize: "8px",
                      color: "black",
                      position: "relative",
                      top: "20px",
                      left: "15px",
                      color: "rgb(197, 194, 194)",
                      fontWeight: "bold",
                      textTransform: "capitalize",
                    }}
                  >
                    {displayname}
                  </span>
                  <p
                    className="chat-message__text "
                    style={{ padding: "22px 0 8px 10px" }}
                  >
                    <a href={require(`../whatsapp-media/${messageTxt}`)} target='_blank'>{messageTxt}</a>
                  </p>
                  <span
                    style={{ fontSize: "10px", color: "black" }}
                    className="mx-3"
                  >
                    {created}
                  </span>
                </div>
              </div>
            </div>
          )}


          {message_sent_type != "NOTIFICATIONS" &&
            message_sent_type != "whatsappImage" && message_sent_type != "whatsappVideo" && message_sent_type != "whatsappAudio" && message_sent_type != "whatsappDocument" && (
              <div
                className={`chat-message ${isSelf && "chat-message--right"} `}
              >
             



                <span className="d-flex message_container jutify-content-center align-items-center" style={{ backgroundColor: isSelf ? '#0b3363' : '#27b490' }}>
                  <b className="my-auto text-white d-flex" style={{fontSize:'12px'}}>{displayname?displayname.slice(0, 2).toUpperCase():""}</b>
                </span>

                {/* </span> */}

                <div className="mx-1">
                  {data.length ? (
                    <a href={data[0].path} download={data[0].name}>
                      <p className="chat-message__text">
                        <span></span>
                      </p>
                    </a>
                  ) : (
                    <>
                    <div>
                      <span
                        className="text-truncate w-25"
                        style={{
                          fontSize: "8px",
                          color: isSelf?"#E0E0E0":'#27b490',
                          position: "relative",
                          top: "20px",
                          left: "15px",
                          fontWeight: "bold",
                          textTransform: "capitalize",
                          width:'20px'
                        }}
                      >

                        {displayname}
                        
                      </span>
                      <p
                        className="chat-message__text "
                        style={{ padding: "22px 0 8px 10px" }}
                        dangerouslySetInnerHTML={createMarkup(isSelf)}
                      ></p>
                    </div>
                      <span
                        style={{ fontSize: "10px", color: "black" }}
                        className="mx-3"
                      >
                        {created}
                      </span>
                      </>
                  )}
                </div>
              </div>
            )}
        </div>
      ) : (
        <div
          className={`chat-messagess ${sender === me._id && "chat-message--right"
            } `}
        >
          <span className="chat-message__avatar-frame">
            <img
              src={avatarURL}
              alt="avatar"
              className="chat-message__avatar"
            />
          </span>

          <div>
            {data.length ? (
              <a href={data[0].path} download={data[0].name}>
                <p className="chat-message__text">
                  <span></span>
                </p>
              </a>
              
            ) : (
              <div>
                <span
                  className=""
                  style={{
                    fontSize: "8px",
                    color: "black",
                    position: "relative",
                    top: "20px",
                    left: "15px",
                    color: "rgb(197, 194, 194)",
                    fontWeight: "bold",
                    textTransform: "capitalize",
                  }}
                >
                  {displayname}
                </span>
                <p
                  className="chat-message__text "
                  style={{ padding: "22px 0 8px 10px" }}
                  dangerouslySetInnerHTML={createMarkup()}
                ></p>
                <span
                  style={{ fontSize: "10px", color: "black" }}
                  className="mx-3"
                >
                  {created}
                </span>
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
}

export default connect(mapStateToProps, {
  sendMessage,
  setchatdata,
  setrefreshtogglechat,
  settogglechat

})(Message);
