import axios from "axios";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import { FcDocument } from "react-icons/fc";
import { FiDownload } from "react-icons/fi";
import { connect } from "react-redux";
import { BaseUrl, baseUrl, newBaseUrl ,TenantID} from "../../containers/Page/Constants/BaseUrl";


const mapStateToProps = (state) => {
  return {
    chat: state.data.chat,
  };
};
const UserChart = (props) => {


  const [filename, setFilename] = useState([])



  useEffect(() => {
    // skillsetDropdown()
  }, [props.email])


  const skillsetDropdown = () => {

    let data = {
      userId: props.chat.unique_id.id
    };
    axios
      .post("https://qacc.inaipi.ae/v1/fileServer/getMediaUserId", data, {
        headers: {
          "TenantId": TenantID
        },
      })
      .then((res) => {
        console.log('resPdf', res.data.data)
        setFilename(res.data.data)
      })
      .catch((err) => {
        console.error(err);
      });
  }




  return (
    <div
      className="tab_content_userchart"
      style={{ backgroundColor: "#f8f9fd", height: '42vh', marginBottom: '20rem' }}
    >
      <div style={{ height: '42vh', overflowY: 'auto', marginBottom: '30rem' }}>

        <h5
          className="ml-1 mt-3 text-center"
          style={{ backgroundColor: "#f8f9fd", fontSize: "1rem" }}
        >
          Files
        </h5>

        {filename.map((item) => {
          return (
            <Card
              style={{ width: "96%", height: "auto", marginLeft: "10px",wordBreak:'break-all' }}
              className="mt-2 pl-3 file_card"
            >
              <div className="text-center d-flex my-3">

                <p>
                  <FcDocument size={25} />
                </p>
                <div className="w-75 d-flex justify-content-center align-items-center" style={{flexDirection:'column'}}>

                  <p style={{ fontWeight: "bold", paddingLeft: 10 }}>{item.path.split('/')}</p>

                  <div>

                    <span
                       style={{
                        fontSize:'10px'
                       }}
                    >
                      {moment(item.createdDate).format('l : hh:mm:s A')}
                    </span>
                  </div>
                </div>


                <p style={{ position: "absolute", right: "15px" }}>
                  <FiDownload size={22} />
                </p>
              </div>
            </Card>
            // console.log("path",item)
          )
        })}


        {/* <Card
        style={{ width: "96%", height: "4rem", marginLeft: "10px" }}
        className="mt-3 pl-3"
      >
        <div className="text-center d-flex mt-3 " style={{overflowY:'auto'}}>
          <p>
            <FcDocument size={25} />
          </p>
          <p style={{ fontWeight: "bold", paddingLeft: 10 }}>Emirtes.pdf</p>
          <span
            style={{
              fontSize: "10px",
              color: "#000",
              fontWeight: "400",
              position: "absolute",
              top: 31,
              left: "50px",
            }}
          >
            July 17th 2022, 4:28:30 pm
          </span>
          <p style={{ position: "absolute", right: "15px" }}>
            <FiDownload size={22} />
          </p>
        </div>
      </Card>

      <Card
        style={{ width: "96%", height: "4rem", marginLeft: "10px" }}
        className="mt-2 pl-3"
      >
        <div className="text-center d-flex mt-3">
          <p>
            <FcDocument size={25} />
          </p>
          <p style={{ fontWeight: "bold", paddingLeft: 10 }}>Inaipi.pdf</p>
          <span
            style={{
              fontSize: "10px",
              color: "#000",
              fontWeight: "400",
              position: "absolute",
              top: 31,
              left: "50px",
            }}
          >
            Aug 24th 2022, 5:12:30 pm
          </span>
          <p style={{ position: "absolute", right: "15px" }}>
            <FiDownload size={22} />
          </p>
        </div>
      </Card>

      <Card
        style={{ width: "96%", height: "4rem", marginLeft: "10px" }}
        className="mt-2 pl-3"
      >
        <div className="text-center d-flex  mt-3">
          <p>
            <FcDocument size={25} />
          </p>
          <p style={{ fontWeight: "bold", paddingLeft: 10 }}>Adharcard.pdf</p>
          <span
            style={{
              fontSize: "10px",
              color: "#000",
              fontWeight: "400",
              position: "absolute",
              top: 31,
              left: "50px",
            }}
          >
            Sep 14th 2022, 1:12:30 pm
          </span>
          <p style={{ position: "absolute", right: "15px" }}>
            <FiDownload size={22} />
          </p>
        </div>
      </Card>

      <Card
        style={{ width: "96%", height: "4rem", marginLeft: "10px" }}
        className="mt-2 pl-3"
      >
        <div className="text-center d-flex  mt-3">
          <p>
            <FcDocument size={25} />
          </p>
          <p style={{ fontWeight: "bold", paddingLeft: 10 }}>Pancard.pdf</p>
          <span
            style={{
              fontSize: "10px",
              color: "#000",
              fontWeight: "400",
              position: "absolute",
              top: 31,
              left: "50px",
            }}
          >
            Sep 15th 2022, 3:12:30 am
          </span>
          <p style={{ position: "absolute", right: "15px" }}>
            <FiDownload size={22} />
          </p>
        </div>
      </Card> */}
      </div>
    </div>
  );
};



export default connect(mapStateToProps, {})(UserChart);

