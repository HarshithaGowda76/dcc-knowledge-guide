const config = require("../config.json");

const mongoose = require("mongoose");

mongoose.connect(config.connectionString).then(() => {
  console.log("Database connected");
});

mongoose.Promise = global.Promise;

module.exports = {
  User_web: require("../src/web_users/users.model"),
  Message: require("../src/message/message.model"),
  Client_new: require("../src/web_users/client.model"),

  isValidId,
};

function isValidId(id) {
  return mongoose.Types.ObjectId.isValid(id);
}
