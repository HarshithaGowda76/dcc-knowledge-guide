const winston = require('winston');
const { combine, timestamp, printf, colorize, align,json} = winston.format;
require('winston-daily-rotate-file');
const path = require("path");
const logger = winston.createLogger({
    level : 'info',
    format:  combine(
      timestamp({
        format: 'YYYY-MM-DD hh:mm:ss.SSS A',
      }),
      printf((info) => `[${info.timestamp}] ${info.level}: ${info.message}`)
    ),
    // new winston.transports.Console()
    transports: [
      new winston.transports.DailyRotateFile({
      filename: 'log/info-%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      maxFiles: '1d',
      maxSize: '20mb',
      level : 'info'
      }),
      new winston.transports.DailyRotateFile({
      filename: 'log/error-%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      maxFiles: '1d',
      maxSize: '20mb',
      level : 'error',
     })
  ],
  exceptionHandlers: [
    new winston.transports.File({ filename: 'log/exception.log' }),
  ],
  rejectionHandlers: [
    new winston.transports.File({ filename: 'log/rejections.log' }),
  ],
  });


  module.exports = logger