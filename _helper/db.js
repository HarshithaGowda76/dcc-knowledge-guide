const config = require("../config.json");

const mongoose = require("mongoose");

mongoose.connect(config.connectionString).then(() => {
  console.log("Database connected");
});

mongoose.Promise = global.Promise;

module.exports = {
  User: require("../src/users/users.model"),
  Message: require("../src/message/message.model"),
  Client: require("../src/users/client.model"),
  Session: require("../src/users/session.model"),
  AgentSession: require("../src/users/agentsession.modal"),
  scannedMessage: require("../src/scannedMessage/scannedMessage.model"),
  fileServer:require("../src/fileUpload/fileUpload.model"),
  agentActivity:require("../src/users/agentActivity.model"),
  chatHistory:require("../src/users/chatHistory.model"),
  roleManage:require("../src/roleManage/roleManage.model"),
  wrapUp:require("../src/message/wrapUp.model"),
  totalTask:require("../src/dashboardCount/totaltask.model"),
  chatCount:require("../src/dashboardCount/chatcount.model"),
  activeTime:require("../src/dashboardCount/activeTime.model"),
  SocialMedia:require("../src/socialmedia/socialmedia.model"),
  agentlastupdate:require("../src/reports/agentlastupdate.model"),
  isValidId,
};

function isValidId(id) {
  return mongoose.Types.ObjectId.isValid(id);
}
