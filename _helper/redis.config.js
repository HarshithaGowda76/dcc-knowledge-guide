const redis = require("redis");
const config = require("../config.json");

module.exports = {
  redisconfig,
};

function redisconfig() {
    if(config.env==="Production"){
        var data= redis.createClient(
            {
        
              socket: {
        
                  host: config.redishostip
        
              }
        
          }
          );
    }else if(config.env==="Development"){
        var data= redis.createClient();
    }
  
  return data
}

