const cron = require("node-cron");
const db = require("../_helper/db");
const axios = require("axios");
const config_file = require("../config.json");
var moment = require("moment");
const { default: mongoose } = require("mongoose");
const { redisconfig } = require("../_helper/redis.config");
(async () => {
  redisClient = redisconfig();

  redisClient.on("error", async (error) => {
    // redisClient=undefined;
    lastredisclient = undefined;
    console.error(`Error : ${error}`);
  });
  redisClient.on("connect", async (data) => {
    console.error(`Redis Started`);
    lastredisclient = "Connected";
    let getAvailAgents = await db.User.find({
      is_available: true,
    });

    console.error(`Getting All Agent Information`);
    // var promises = getAvailAgents.map((element) => {
    //   return new Promise(async (resolve, reject) => {
    //     const newincomingrequest = await db.Session.find({
    //       agent: element._id,
    //       status: {
    //         $in: ["newjoin", "Accept"],
    //       },
    //     }).populate("unique_id", "username email phonenumber");
    //     // console.log(newincomingrequest);
    //     redisClient.set(
    //       JSON.stringify(element._id),
    //       JSON.stringify(newincomingrequest)
    //     );
    //     console.error(`Agent Incoming Request Updation : ${element._id}`);
    //     resolve(element);
    //   });
    // });

    // Promise.all(promises).then(function (values) {
    //   console.log("All data restored to redis");
    //   lastredisclient = "Connected";
    // });
  });

  await redisClient.connect();
})();
//require('events').EventEmitter.defaultMaxListeners = Infinity;
module.exports={
    roleManage,
    totalCompleted,
    totalTransfered,
    totalActiveChatCount,
    activeinternalChat,
    activeChat,
    activeTransferedChat,
    activeConfernceChat,
    chatcountDataCreate,
    totalBreakTime,
    totalAvaliblieTime,
    tranferChatIncount,
    tranferChatOutcount,
    totalLoginTime,
    agentstatusupdate,
    sessionUpdate
};
async function roleManage() {
    const job = cron.schedule("*/30 * * * *", async function () {
        const config = {
            headers: { TenantID:"a3dc14bd-fe70-4120-8572-461b0dc866b5" },
          };
          axios
          .get(config_file.loginbaseurl+"/usermodule/clientUser/role", config)
          .then(async (resp) => {
            if (resp.data.dataList) {
                var newArr1=resp.data.dataList;
               // console.log(newArr1)
               if(newArr1){
                await db.roleManage.deleteMany();
                var newdata = await db.roleManage.create(newArr1);
               }
            }
          })
    })}
async function totalCompleted(){
  const job = cron.schedule("*/5 * * * * *", async function () {
    todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var aggregateOpts=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          }, 
          'status': 'chatEnded',
          chat_type:'external'
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$agent'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'count': '$count',
          agent_id: "$agent_id",
        }
      }
    ]
    await db.Session.aggregate(aggregateOpts).exec()
    .then(async (data) => {
      // console.log(data)
      if (data.length > 0) {
        await db.totalTask.updateMany({created_at:start_date},{$set:{total_completed:0}})
        data.forEach(async (ele) => {
          var dashboarddata = await db.totalTask.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (!dashboarddata) {
            if (ele._id.agent_id) {
              await new db.totalTask({
                created_at: start_date,
                agent_id: ele._id.agent_id,
              }).save();
            }
          } else {
            var id = dashboarddata._id;
            var countValue = await db.totalTask.findByIdAndUpdate(
              { _id: id },
              {
                total_completed: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });
    var aggregateValue=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          }, 
          'status': 'chatEnded',
          chat_type:'external'
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$conference_agent'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'count': '$count',
          agent_id: "$agent_id",
        }
      }
    ]
    await db.Session.aggregate(aggregateValue).exec()
    .then(async (data) => {
      // console.log(data)
      if (data.length > 0) {
        await db.totalTask.updateMany({created_at:start_date},{$set:{total_completed_conference:0}})
        data.forEach(async (ele) => {
          var dashboarddata = await db.totalTask.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (dashboarddata) {
            var id = dashboarddata._id;
            var countValue = await db.totalTask.findByIdAndUpdate(
              { _id: id },
              {
                total_completed_conference: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });
  })
} 
async function totalTransfered(){
  const job = cron.schedule("*/7 * * * * *", async function () {
    todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var aggregateOpts=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          }, 
          'chat_type':'external',
          transferred:true
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$agent'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'count': '$count',
          agent_id: "$agent_id",
        }
      }
    ]
    await db.Session.aggregate(aggregateOpts).exec()
    .then(async (data) => {
      // console.log(data)
      if (data.length > 0) {
        await db.totalTask.updateMany({created_at:start_date},{$set:{total_transfered:0}})
        data.forEach(async (ele) => {
          var dashboarddata = await db.totalTask.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (!dashboarddata) {
            if (ele._id.agent_id) {
              await new db.totalTask({
                created_at: start_date,
                agent_id: ele._id.agent_id,
              }).save();
            }
          } else {
            var id = dashboarddata._id;
            var countValue = await db.totalTask.findByIdAndUpdate(
              { _id: id },
              {
                total_transfered: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });
  })
}   
async function totalActiveChatCount(){
  const job = cron.schedule("*/8 * * * * *", async function () {
    todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var aggregateOpts=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          }, 
          status:"Accept",
          'chat_type':'external',
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$agent'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          agent_id: "$agent_id",
          'created_at': '$createdAt', 
          'count': '$count'
        }
      }
    ]
    await db.Session.aggregate(aggregateOpts).exec()
    .then(async (data) => {
      // console.log(data)
      await db.totalTask.updateMany({created_at:start_date},{$set:{total_active_chat_count:0}})
      if (data.length > 0) {
        
        data.forEach(async (ele) => {
          var dashboarddata = await db.totalTask.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (!dashboarddata) {
            if (ele._id.agent_id) {
              await new db.totalTask({
                created_at: start_date,
                agent_id: ele._id.agent_id,
              }).save();
            }
          } else {
            var id = dashboarddata._id;
            var countValue = await db.totalTask.findByIdAndUpdate(
              { _id: id },
              {
                total_active_chat_count: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });

    var aggregateValue=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          }, 
          status:"Accept",
          'chat_type':'external',
          $expr:{$ne:[ "$conference_agent","$conference_left_id"]}
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$conference_agent'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          agent_id: "$agent_id",
          'created_at': '$createdAt', 
          'count': '$count'
        }
      }
    ]
    await db.Session.aggregate(aggregateValue).exec()
    .then(async (data) => {
      // console.log(data)
      await db.totalTask.updateMany({created_at:start_date},{$set:{total_active_chat_conference_count:0}})
      if (data.length > 0) {
        
        data.forEach(async (ele) => {
          var dashboarddata = await db.totalTask.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (dashboarddata) {
          
            var id = dashboarddata._id;
            var countValue = await db.totalTask.findByIdAndUpdate(
              { _id: id },
              {
                total_active_chat_conference_count: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });
  })
  
}
async function totalBreakTime(){
  const job = cron.schedule("*/12 * * * * *", async function () {
    todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var aggregateOpts=[
      {
        '$match': {
          'created_at': {
            '$gte': start_date, 
            '$lte': end_date
          }, 
        }
      }, {
        '$project': {
          'agent_id': "$agent_id",
          'created_at': '$created_at',
          'duration_break':'$duration_break'

        }
      }
    ]
    await db.activeTime.aggregate(aggregateOpts).exec()
    .then(async (data) => {
      // console.log(data)
      if (data.length > 0) {
        data.forEach(async (ele) => {
          var dashboarddata = await db.totalTask.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele.agent_id },
            ],
          });
          if (!dashboarddata) {
            if (ele.agent_id) {
              await new db.totalTask({
                created_at: start_date,
                agent_id: ele.agent_id,
              }).save();
            }
          } else {
            var id = dashboarddata._id;
            var countValue = await db.totalTask.findByIdAndUpdate(
              { _id: id },
              {
                total_break_time: ele.duration_break,
              },
              { new: true }
            );
          }
        });
      }
    });
  })

}
async function totalLoginTime(){
  const job = cron.schedule("*/5 * * * * *", async function () {
   var todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z"); 
    var aggregateOpts=[
      {
        '$match': {
          'created_at': {
            '$gte': start_date, 
            '$lte': end_date
          }, 
          'is_login':true
        }
      }, {
        '$project': {
          'agent_id': "$agent_id",
          'created_at': '$created_at',
          'duration_break':'$duration_break',
          'duration_login':'$duration_login',
          'loginDate':'$loginDate',
          'updateDate':'$updateDate'

        }
      }
    ]
    await db.activeTime.aggregate(aggregateOpts).exec()
    .then(async(data)=>{
      if(data.length>0){
        //console.log(data)
        data.forEach(async (ele) => {
       if(ele.agent_id){
        var id=ele._id;
        var login_time = ele.updateDate;
        var current_time = new Date();
        var logdates = moment(login_time, "YYYY-M-DD HH:mm:ss");
        var dates = moment(current_time, "YYYY-M-DD HH:mm:ss");
        var diff = dates.diff(logdates, "seconds");
        var duration = ele.duration_login + diff;
        var countValue = await db.activeTime.findByIdAndUpdate(
          { _id: id },
          {
            duration_login: duration,
            updateDate:new Date()
          },
          { new: true }
        );
       }
        })
      }
    })
  })
}
async function totalAvaliblieTime(){

  const job = cron.schedule("*/15 * * * * *", async function () {
    todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var aggregateOpts=[
      {
        '$match': {
          'created_at': {
            '$gte': start_date, 
            '$lte': end_date
          }, 
        }
      }, {
        '$project': {
          'agent_id': "$agent_id",
          'created_at': '$created_at',
          'duration_break':'$duration_break',
          'duration_login':'$duration_login',
          'loginDate':'$loginDate'

        }
      }
    ]
    await db.activeTime.aggregate(aggregateOpts).exec()
    .then(async (data) => {
      // console.log(data)
      if (data.length > 0) {
        data.forEach(async (ele) => {
          var dashboarddata = await db.totalTask.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele.agent_id },
            ],
          });
          if (!dashboarddata) {
            if (ele.agent_id) {
              await new db.totalTask({
                created_at: start_date,
                agent_id: ele.agent_id,
              }).save();
            }
          } else {
            var id = dashboarddata._id;
            var duration=ele.duration_login - ele.duration_break
            var countValue = await db.totalTask.findByIdAndUpdate(
              { _id: id },
              {
                total_active_time: duration,
              },
              { new: true }
            );
          }
        });
      }
    });
  })
}
async function activeinternalChat(){
  const job = cron.schedule("*/7 * * * * *", async function () {
    todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var aggregateOpts=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          }, 
          'chat_type': 'internal'
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$agent'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'agent_id':'$agent_id',
          'count': '$count'
        }
      }
    ]
    await db.Session.aggregate(aggregateOpts).exec()
    .then(async (data) => {
      // console.log(data)
      if (data.length > 0) {
        await db.chatCount.updateMany({created_at:start_date},{$set:{internal_chat:0}})
        data.forEach(async (ele) => {
          var dashboarddata = await db.chatCount.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (!dashboarddata) {
            if (ele._id.agent_id) {
              await new db.chatCount({
                created_at: start_date,
                agent_id: ele._id.agent_id,
              }).save();
            }
          } else {
            var id = dashboarddata._id;
            var countValue = await db.chatCount.findByIdAndUpdate(
              { _id: id },
              {
                internal_chat: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });
  })
}
async function activeChat(){
  const job = cron.schedule("*/8 * * * * *", async function () {
    todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var aggregateOpts=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          },  'chat_type':'external',
          'status': 'Accept',
          $or:[
            {'conference':false},
            {conference:true,
              $expr:{$ne:[ "$agent","$conference_left_id"]}
            }
            ]
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$agent'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'agent_id':'$agent_id',
          'count': '$count'
        }
      }
    ]
    await db.Session.aggregate(aggregateOpts).exec()
    .then(async (data) => {
       //console.log(data)
       await db.chatCount.updateMany({created_at:start_date},{$set:{active_chat:0}})
      if (data.length > 0) {
        
        data.forEach(async (ele) => {
          var dashboarddata = await db.chatCount.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (!dashboarddata) {
            if (ele._id.agent_id) {
              await new db.chatCount({
                created_at: start_date,
                agent_id: ele._id.agent_id,
              }).save();
            }
          } else {
            var id = dashboarddata._id;
            var countValue = await db.chatCount.findByIdAndUpdate(
              { _id: id },
              {
                active_chat: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });

    var aggregateValue=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          },  'chat_type':'external',
          'status': 'Accept',
          $expr:{$ne:[ "$conference_agent","$conference_left_id"]}
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$conference_agent'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'agent_id':'$agent_id',
          'count': '$count'
        }
      }
    ]
    await db.Session.aggregate(aggregateValue).exec()
    .then(async (data) => {
       //console.log(data)
       await db.chatCount.updateMany({created_at:start_date},{$set:{conference_basedactive_chat:0}})
      if (data.length > 0) {
        
        data.forEach(async (ele) => {
          var dashboarddata = await db.chatCount.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          
          if (dashboarddata) {
            var id = dashboarddata._id;
            
            var countValue = await db.chatCount.findByIdAndUpdate(
              { _id: id },
              {
                conference_basedactive_chat: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });

  })
}
async function activeTransferedChat(){
  const job = cron.schedule("*/9 * * * * *", async function () {
    todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var aggregateOpts=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          },  'chat_type':'external',
          'transferred': true
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$agent'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'agent_id':'$agent_id',
          'count': '$count'
        }
      }
    ]
    await db.Session.aggregate(aggregateOpts).exec()
    .then(async (data) => {
      // console.log(data)
      if (data.length > 0) {
        await db.chatCount.updateMany({created_at:start_date},{$set:{active_transfered_chat:0}})
        data.forEach(async (ele) => {
          var dashboarddata = await db.chatCount.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (!dashboarddata) {
            if (ele._id.agent_id) {
              await new db.chatCount({
                created_at: start_date,
                agent_id: ele._id.agent_id,
              }).save();
            }
          } else {
            var id = dashboarddata._id;
            var countValue = await db.chatCount.findByIdAndUpdate(
              { _id: id },
              {
                active_transfered_chat: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });

  })
}
async function activeConfernceChat(){
  const job = cron.schedule("*/10 * * * * *", async function () {
    todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var aggregateOpts=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          },  'chat_type':'external',
          'conference': true
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$agent'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'agent_id':'$agent_id',
          'count': '$count'
        }
      }
    ]
    await db.Session.aggregate(aggregateOpts).exec()
    .then(async (data) => {
      // console.log(data)
      if (data.length > 0) {
        await db.chatCount.updateMany({created_at:start_date},{$set:{active_conferece_chat:0}})
        data.forEach(async (ele) => {
          var dashboarddata = await db.chatCount.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (!dashboarddata) {
            if (ele._id.agent_id) {
              await new db.chatCount({
                created_at: start_date,
                agent_id: ele._id.agent_id,
              }).save();
            }
          } else {
            var id = dashboarddata._id;
            var countValue = await db.chatCount.findByIdAndUpdate(
              { _id: id },
              {
                active_conferece_chat: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });
    var aggregateValue=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          },  'chat_type':'external',
          'conference': true
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$conference_agent',
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'agent_id':'$agent_id',
          'count': '$count'
        }
      }
    ]
    await db.Session.aggregate(aggregateValue).exec()
    .then(async (data) => {
      // console.log(data)
      if (data.length > 0) {
        await db.chatCount.updateMany({created_at:start_date},{$set:{conference_based_conferenceagent:0}})
        data.forEach(async (ele) => {
          var dashboarddata = await db.chatCount.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (dashboarddata) {
            var id = dashboarddata._id;
            var countValue = await db.chatCount.findByIdAndUpdate(
              { _id: id },
              {
                conference_based_conferenceagent: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });
  })
}
async function chatcountDataCreate(){
  const job = cron.schedule("*/50 * * * *", async function () {
    console.log("chatcount cronjob")
    var agent = await db.roleManage
      .findOne({ $or: [{ roleName: "Agent" }, { roleName: "agent" }] })
      .select({ id: 1, _id: 0 });
      var listUser=await db.User.find({roles_array:agent.id}).select({_id:1});
      todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
      // var userarray = [];
      // listUser.forEach((el) => {
      //   userarray.push(el._id);
      // });
      listUser.forEach(async (ele) => {
        
        var dashboarddata = await db.chatCount.findOne({
          $and: [
            { created_at: start_date },
            { agent_id: ele._id },
          ],
        });
        if (!dashboarddata) {
          if (ele._id) {
            await new db.chatCount({
              created_at: start_date,
              agent_id: ele._id,
            }).save();
          }
        } 
      });
      listUser.forEach(async (ele) => {
        
        var dashboarddat = await db.totalTask.findOne({
          $and: [
            { created_at: start_date },
            { agent_id: ele._id },
          ],
        });
        if (!dashboarddat) {
          if (ele._id) {
            await new db.totalTask({
              created_at: start_date,
              agent_id: ele._id,
            }).save();
          }
        } 
      });

  })
}
async function tranferChatIncount(){
  const job = cron.schedule("*/10 * * * * *", async function () {
    todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    var aggregateOpts=[
      {
        '$match': {
          'createdAt': {
            '$gte': start_date, 
            '$lte': end_date
          }, 
          'transferred': true
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$createdAt'
              }
            }, 
            'agent_id': '$agent'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'agent_id':'$agent_id',
          'count': '$count'
        }
      }
    ]
    var aggregateOption=[
      {
        '$match': {
          'type': 'Transfer',
          'time': {
            '$gte': start_date, 
            '$lte': end_date
          },
        }
      },{
        '$lookup': {
          'from': 'sessions', 
          'localField': 'session_id', 
          'foreignField': 'chat_session_id', 
          'as': 'session'
        }
      }, {
        '$unwind': {
          'path': '$session'
        }
      }, {
        '$match': {
          'session.chat_type': 'external'
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$time'
              }
            }, 
            'agent_id': '$destinationagent_id'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'agent_id': '$agent_id', 
          'count': '$count'
        }
      }
    ]
    await db.chatHistory.aggregate(aggregateOption).exec()
    .then(async (data) => {
      // console.log(data)
      if (data.length > 0) {
        data.forEach(async (ele) => {
          var dashboarddata = await db.chatCount.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (!dashboarddata) {
            if (ele._id.agent_id) {
              await new db.chatCount({
                created_at: start_date,
                agent_id: ele._id.agent_id,
              }).save();
            }
          } else {
            var id = dashboarddata._id;
            var countValue = await db.chatCount.findByIdAndUpdate(
              { _id: id },
              {
                total_transferred_in: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });

  })
}
async function tranferChatOutcount(){
  const job = cron.schedule("*/12 * * * * *", async function () {
    todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var end_date = new Date(todays_date + "T23:59:59.999Z");
    
    var aggregateOption=[
      {
        '$match': {
          'type': 'Transfer',
          'time': {
            '$gte': start_date, 
            '$lte': end_date
          },
        }
      },{
        '$lookup': {
          'from': 'sessions', 
          'localField': 'session_id', 
          'foreignField': 'chat_session_id', 
          'as': 'session'
        }
      }, {
        '$unwind': {
          'path': '$session'
        }
      }, {
        '$match': {
          'session.chat_type': 'external'
        }
      }, {
        '$group': {
          '_id': {
            'createdAt': {
              '$dateToString': {
                'format': '%Y-%m-%d', 
                'date': '$time'
              }
            }, 
            'agent_id': '$agent_id'
          }, 
          'count': {
            '$count': {}
          }
        }
      }, {
        '$project': {
          'created_at': '$createdAt', 
          'agent_id': '$agent_id', 
          'count': '$count'
        }
      }
    ]
    await db.chatHistory.aggregate(aggregateOption).exec()
    .then(async (data) => {
      // console.log(data)
      if (data.length > 0) {
        data.forEach(async (ele) => {
          var dashboarddata = await db.chatCount.findOne({
            $and: [
              { created_at: start_date },
              { agent_id: ele._id.agent_id },
            ],
          });
          if (!dashboarddata) {
            if (ele._id.agent_id) {
              await new db.chatCount({
                created_at: start_date,
                agent_id: ele._id.agent_id,
              }).save();
            }
          } else {
            var id = dashboarddata._id;
            var countValue = await db.chatCount.findByIdAndUpdate(
              { _id: id },
              {
                total_transferred_out: ele.count,
              },
              { new: true }
            );
          }
        });
      }
    });

  })
}
async function agentstatusupdate(){
  const job = cron.schedule("*/30 * * * * *", async function () {
    var todays_date = new Date().toISOString();
    todays_date = todays_date.replace(/\T.*/, "");
    var start_date = new Date(todays_date);
    var agent = await db.roleManage
    .findOne({ $or: [{ roleName: "Agent" }, { roleName: "agent" }] })
    .select({ id: 1, _id: 0 });
    var userstatus=await db.User.find({roles_array:agent.id}).select({agent_status_real:1});
    userstatus.forEach(async(ele)=>{
      await db.chatCount.findOneAndUpdate({agent_id:ele._id,created_at:start_date},{ $set: { agent_status:ele.agent_status_real}})
    })
    //console.log(userstatus)

  })
}
async function sessionUpdate(){
  const job = cron.schedule("*/5 * * * *", async function () {
    //  var data=await db.Session.deleteMany({status:{$in:["newjoin","Accept"]},chat_type:"external",agent:{$eq:null}})
    //  var value=await db.Session.find({status:{$in:["newjoin","chatEnded"]},chat_type:"external",agent:{$ne:null}}).limit(50);
    //  if(value.length>0){
    //   value.forEach(async(data)=>{
    //     var agents=JSON.stringify(data.agent)
    //    // var id=JSON.stringify(data.id)
    //     //console.log("agents",agents,"id",data.id)
    //   var valuss=  await redisClient.hDel(JSON.stringify(agents), JSON.stringify(data.id))
    //   //console.log(valuss,"valu")
    //  // if(valuss){
    //      await db.Session.findByIdAndUpdate({_id:data.id},{status:"chatEnded"})
    //  // }
    //    })
    //  }
    
   var todays_date = new Date();
   console.log("date",todays_date)
   todays_date.setMinutes(todays_date.getMinutes() - 30);
   console.log("today",todays_date)
       var datavalue=await db.Session.find({status:{$eq:"newjoin"},chat_type:"external",agent:{$ne:null},updatedAt:{$lte:todays_date}}).limit(100);
       if(datavalue.length>0){
        datavalue.forEach(async(data)=>{
          var agents=JSON.stringify(data.agent)
         // var id=JSON.stringify(data.id)
          //console.log("agents",agents,"id",data.id)
          await axios.post(
            config_file.customurl+"/v1/message/addMessage",
            {
              from: data._id,
              to: data._id,
              message: "Please try again later",
              senderName: "Error Message",
              receiverName: "User",
              msg_sent_type: "TEXT",
              messageFrom: "fromAgent",
              userType: "external",
              file_name: "",
              chatdetails: data
            }
          );

        var valuss=  await redisClient.hDel(JSON.stringify(agents), JSON.stringify(data.id))
        console.log(valuss,"valu")
        if(valuss){
           await db.Session.findByIdAndUpdate({_id:data.id},{status:"chatEnded"})
        }
         })
       }
       var dataAccept=await db.Session.find({status:{$eq:"Accept"},chat_type:"external",agent:{$ne:null},lastmessageUpdatedat:{$lte:todays_date}}).limit(100);
       if(dataAccept.length>0){
        dataAccept.forEach(async(data)=>{
          var agents=JSON.stringify(data.agent)
         // var id=JSON.stringify(data.id)
          //console.log("agents",agents,"id",data.id)
          await axios.post(
            config_file.customurl+"/v1/message/addMessage",
            {
              from: data._id,
              to: data._id,
              message: "Please try again later",
              senderName: "Error Message",
              receiverName: "User",
              msg_sent_type: "TEXT",
              messageFrom: "fromAgent",
              userType: "external",
              file_name: "",
              chatdetails: data
            }
          );

        var valuss=  await redisClient.hDel(JSON.stringify(agents), JSON.stringify(data.id))
        console.log(valuss,"valu")
        if(valuss){
           await db.Session.findByIdAndUpdate({_id:data.id},{status:"chatEnded"})
        }
         })
       }
     //var result=  await redisClient.hDel(JSON.stringify(agents), JSON.stringify(findDetails._id))
  })
}
async function logoutupdate(){
  const job = cron.schedule("*/30 * * * *", async function () {
    var todays_date = new Date();
    console.log("date",todays_date)
    todays_date.setMinutes(todays_date.getMinutes() - 30);
    console.log("today",todays_date)
    var datavalue=await db.User.find({lastactive_time:{$lte:todays_date},is_loggedIn:true}).select({_id:1,agent_id:1}).limit(100);
    if(datavalue.length>0){
   var cacheResults = await redisClient.get("agentsDetails");
   if (cacheResults) {
    cache_resp = JSON.parse(cacheResults);
  }
  if (cache_resp.length != 0) {
    const missingObjects = cache_resp.filter(element => !datavalue.some(data => data._id === element.id));
     console.log(missingObjects);
     await redisClient.set("agentsDetails", JSON.stringify(missingObjects))
     datavalue.forEach(async(e)=>{
      await db.User.findByIdAndUpdate({_id:mongoose.Types.ObjectId(e._id)},{$set: {
        agent_status: "Not Available",
        agent_status_real: "Is LoggedOut",
        is_loggedIn: false,
      }})
     })
  }
    }

  })
}