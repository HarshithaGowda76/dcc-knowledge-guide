
module.exports = {
  EVENT_CUSTOMER_CONNECTED: 'customer connected',
  EVENT_CUSTOMER_DISCONNECTED: 'customer disconnected',
  EVENT_CUSTOMER_MESSAGE: 'customer message',
  EVENT_OPERATOR_MESSAGE: 'operator message',
  EVENT_OPERATOR_REQUESTED: 'operator requested',
  EVENT_AGENT_REQUESTED: 'Agent requested',
  EVENT_SYSTEM_ERROR: 'system error',
  EVENT_DISCONNECT: 'disconnect',

  CONTEXT_OPERATOR_REQUEST: 'operator_request',

  OPERATOR_GREETING: `Connecting to Agent please wait`
};
